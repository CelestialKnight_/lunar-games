function ad(callback) {
  loadAdSpace();
  generateAd(callback);
}
function loadAdSpace() {
  document.getElementById('video-ad-container').style.display = 'block';
}
function hideAdSpace() {
  document.getElementById('video-ad-container').style.display = 'none';
}
function generateAd(callback) {
  var player = new VASTPlayer(document.getElementById('video-ad'));
  player.callback = callback;
  player.once('AdStopped', function() {
    hideAdSpace();
    player.callback();
  });
  player.load('https://www.videosprofitnetwork.com/watch.xml?key=1a61be334c19b72e3d5b6c6ca97b8041').then(function startAd() {
    return player.startAd();
  }).catch(function(reason) {
    throw reason;
  });
}
