(function() {
  var betaLevel = '';
  /*document.getElementById('run-level').addEventListener('click', function() {
    betaLevel = document.getElementById('level-code').value;
    Game.level = 'beta-level';
    level('beta-level', null, true);
    document.removeEventListener('click', mainMenuSupport);
    document.removeEventListener('keydown', mainMenuSupport2);
  })*/
  window.oncontextmenu = function() {
    return false;
  }
  var pt = [];
  var pings = {
    client: [],
    server: [],
  };
  //var saveStatus = document.getElementById('saveStatus');
  saveStatus = {};
  var playerData;
  var socket, respawn;
  window.setInterval(function() {
    if (!window.navigator.onLine) {
      socket.close();
    }
  }, 100);
  var status_message = 'Playing Pixel Tanks';

  function socket_support() {
    socket = new WebSocket('wss://' + window.location.hostname + '/server');
    setInterval(function() {
      socket.send(JSON.stringify({
        username: sessionStorage.username,
        token: sessionStorage.token,
        operation: 'status',
        task: 'set',
        status: 'online',
        message: status_message,
      }));
    }, 1000);
    socket.onclose = function() {
      saveStatus.innerHTML = 'Disconnected reconnecting';
      setTimeout(function() {
        socket_support();
      }, 10000);
    };
    socket.onopen = function() {
      clearInterval(respawn);
      saveStatus.innerHTML = 'Connected';
      setTimeout(function() {
        saveStatus.innerHTML = '';
      }, 3000)
    }
  }
  socket_support();

  function get(username, callback) {
    socket.send(JSON.stringify({
      operation: 'database',
      task: 'get',
      username: sessionStorage.username,
      token: sessionStorage.token,
    }));
    socket.onmessage = function(data) {
      data = JSON.parse(data.data);
      userData = JSON.parse(data.data[0].playerdata)['increedible-tanks'];
      playerData = JSON.parse(data.data[0].playerdata);
      socket.onmessage = function() {};
      callback();
    };
  }

  function saveGame() {
    try {
      var t = playerData;
      t['increedible-tanks'] = userData;
    } catch (e) {
      saveStatus.innerHTML = 'Log in to save your progress';
      setTimeout(function() {
        saveStatus.innerHTML = '';
      }, 3000);
    }
    update(sessionStorage.username, 'playerdata', JSON.stringify(t));
  }

  function update(username, key, value) {
    try {
      saveStatus.innerHTML = 'Saving'
      socket.send(JSON.stringify({
        operation: 'database',
        token: sessionStorage.token,
        task: 'update',
        username: sessionStorage.username,
        key: key,
        value: value,
      }));
      socket.onmessage = function(data) {
        data = JSON.parse(data.data);
        if (data.success) {
          socket.onmessage = function() {}
          document.getElementById('saveStatus').innerHTML = 'Saved';
          setTimeout(function() {
            document.getElementById('saveStatus').innerHTML = '';
          }, 3000);
        }
      };
    } catch (e) {}
  }
  var interval, user, listener, interval2;
  // initialize music and sounds  
  /*
  var villageMusic = new sound('');
  var menuMusic = new sound('');
  var damageNoise = new sound('');
  var victoryNoise = new sound('');
  var shotNoise = new sound('');
  */
  // function constructor for making sounds
  function sound(src) {
    this.sound = document.createElement('audio');
    this.sound.src = src;
    this.sound.setAttribute('preload', 'auto');
    this.sound.setAttribute('controls', 'none');
    this.sound.style.display = 'none';
    document.body.appendChild(this.sound);
    this.play = function() {
      this.sound.play();
    }
    this.stop = function() {
      this.sound.pause();
    }
  }
  class Menu {
    constructor(draw, listeners, element, context) {
      this.draw = draw;
      this.element = element;
      this.listeners = listeners;
      if (context) {
        for (let property in this.listeners) {
          this.listeners[property] = this.listeners[property].bind(context);
        }
      } else {
        for (let property in this.listeners) {
          this.listeners[property] = this.listeners[property].bind(this);
        }
      }
    }
    addListeners() {
      for (let property in this.listeners) {
        this.element.addEventListener(property, this.listeners[property]);
      }
    }
    removeListeners() {
      for (let property in this.listeners) {
        this.element.removeEventListener(property, this.listeners[property]);
      }
    }
  }

  function ai_check(x, y, isBlock) {
    var t = 40;
    if (isBlock) t = 50;
    var l = 0;
    while (l < s.length) {
      if (s[l].x > x || s[l].x + 5 > x) {
        if (s[l].x < x + t || s[l].x + 5 < x + t) {
          if (s[l].y > y || s[l].y + 5 > y) {
            if (s[l].y < y + t || s[l].y + 5 < y + t) {
              var damage = s[l].damage;
              saveStatus.innerHTML = s[l].damage
              var team = s[l].team;
              var grappleData = {
                grappled: false,
              };
              if (s[l].type == 'grapple') {
                grappleData = {
                  grappled: true,
                  startx: s[l].startx + s[l].xm * 4,
                  starty: s[l].starty + s[l].ym * 4,
                };
              }
              delete s[l];
              s.splice(l, 1);
              return [true, damage, team, grappleData];
            }
          }
        }
      }
      if (s[l].x < Game.borders[0] || s[l].x > Game.borders[1] || s[l].y < Game.borders[2] || s[l].y > Game.borders[3]) {
        delete s[l];
        s.splice(l, 1);
      }
      l++;
    }
    return false;
  }

  function animate(fps, duration, value, start, end) {
    var frames = duration / (1000 / fps);
    var interval = Math.abs(start - end) / frames;
    var int = 0;
    var a = setInterval(function() {
      int++;
      eval(value + '+=' + interval);
      if (int == frames) clearInterval(a);
    }, 1000 / fps);
  }
  class Hitbox {
    constructor(owner, x, y, width, height) {
      this.owner = owner;
      this.width = width;
      this.x = x;
      this.y = y;
    }
    detect() {
      for (var l = 0; l < s[l].length; l++) {
        if ((this.x < s[l].x && this.x + this.width > s[l].x) && (this.y < s[l].y && this.y + this.height > s[l].y)) {
          delete s[l];
          s.splice(l, 1);
          this.owner.health -= s[l].damage;
        }
      }
    }
  }

  function generateRandomColor() {
    var red = Math.floor(Math.random() * 255);
    var green = Math.floor(Math.random() * 255);
    var blue = Math.floor(Math.random() * 255);
    return "rgb(" + red + "," + green + "," + blue + " )";
  }
  class Ai {
    constructor(type, fireType, isInvis, turretFireType, team) {
      if (type == 'turret') {
        this.turret = true;
      } else {
        this.turret = false;
      }
      this.team = team
      this.fireType = fireType;
      this.sawUser = false;
      this.instructions = {};
      this.type = type;
      this.rotation = 0;
      this.base = 0;
      this.pushback = 0;
      this.fire = this.fire.bind(this);
      if (!this.turret) {
        if (fireType == 0) {
          this.health = 60;
        } else if (fireType == 1) {
          this.health = 200;
        } else if (fireType == 2) {
          this.health = 320;
        } else if (fireType == 3) {
          this.health = 250;
        }
      } else { // Turrets have x3 health
        if (fireType == 1) {
          this.health = 600;
        }
      }
      this.maxHealth = this.health;
      this.inactive = false;
      this.update = true;
      this.leftright = false;
      this.canShoot = true;
      this.canTakeThermalDamage = true;
    }
    draw() {
      if (this.inactive != true) {
        if (this.leftright) {
          if (!this.turret) {
            draw.translate(this.x + 20, this.y + 20);
          } else {
            draw.translate(this.x + 25, this.y + 25);
          }
          draw.rotate(90 * Math.PI / 180);
          if (this.base == 1) {
            if (!this.turret) {
              if (this.fireType == 0) {
                draw.drawImage(stupid_tank_base, -20, -20)
              } else if (this.fireType == 1) {
                draw.drawImage(tank_base_png, -20, -20);
              } else if (this.fireType == 2) {
                draw.drawImage(area_tank_base, -20, -20);
              } else if (this.fireType == 3) {
                draw.drawImage(condensed_tank_base, -20, -20);
              }
            } else {
              draw.drawImage(ai_turret_base, -25, -25, 50, 50);
            }
          } else {
            if (!this.turret) {
              if (this.fireType == 0) {
                draw.drawImage(stupid_tank_base2, -20, -20);
              } else if (this.fireType == 1) {
                draw.drawImage(tank_base2, -20, -20);
              } else if (this.fireType == 2) {
                draw.drawImage(area_tank_base2, -20, -20);
              } else if (this.fireType == 3) {
                draw.drawImage(condensed_tank_base2, -20, -20);
              }
            } else {
              draw.drawImage(ai_turret_base, -25, -25, 50, 50);
            }
          }
          draw.rotate(-90 * Math.PI / 180);
          if (!this.turret) {
            draw.translate((-this.x - 20), (-this.y - 20));
          } else {
            draw.translate((-this.x - 25), (-this.y - 25));
          }
        } else {
          if (this.base == 1) {
            if (!this.turret) {
              if (this.fireType == 0) {
                draw.drawImage(stupid_tank_base, this.x, this.y);
              } else if (this.fireType == 1) {
                draw.drawImage(tank_base_png, this.x, this.y);
              } else if (this.fireType == 2) {
                draw.drawImage(area_tank_base, this.x, this.y);
              } else if (this.fireType == 3) {
                draw.drawImage(condensed_tank_base, this.x, this.y);
              }
            } else {
              draw.drawImage(ai_turret_base, this.x, this.y, 50, 50);
            }
          } else {
            if (!this.turret) {
              if (this.fireType == 0) {
                draw.drawImage(stupid_tank_base2, this.x, this.y);
              } else if (this.fireType == 1) {
                draw.drawImage(tank_base2, this.x, this.y);
              } else if (this.fireType == 2) {
                draw.drawImage(area_tank_base2, this.x, this.y);
              } else if (this.fireType == 3) {
                draw.drawImage(condensed_tank_base2, this.x, this.y);
              }
            } else {
              draw.drawImage(ai_turret_base, this.x, this.y, 50, 50);
            }
          }
        }
        if (!this.turret) {
          draw.translate(this.x + 20, this.y + 20);
        } else {
          draw.translate(this.x + 25, this.y + 25);
        }
        draw.rotate(this.rotation * Math.PI / 180);
        if (!this.turret) {
          if (this.fireType == 0) {
            draw.drawImage(stupid_tank_top, -20, -20 + this.pushback);
          } else if (this.fireType == 1) {
            draw.drawImage(ai_top, -20, -20 + this.pushback);;
          } else if (this.fireType == 2) {
            draw.drawImage(area_tank_top, -20, -20 + this.pushback);
          } else if (this.fireType == 3) {
            draw.drawImage(condensed_tank_top, -20, -20 + this.pushback);
          }
        } else {
          if (this.fireType == 1) {
            draw.drawImage(ai_top, -20, -20 + this.pushback);
          }
        }
        if (this.pushback != 0) {
          this.pushback += 1;
        }
        draw.rotate(-(this.rotation * Math.PI / 180));
        if (!this.turret) {
          draw.translate(-this.x - 20, -this.y - 20);
        } else {
          draw.translate(-this.x - 25, -this.y - 25);
        }
        draw.fillStyle = '#000000';
        draw.fillRect(this.x, this.y + 50, 40, 5);
        draw.fillStyle = '#FF0000';
        draw.fillRect(this.x + 2, this.y + 51, 36 * this.health / this.maxHealth, 3);
      }
    }
    move() {
      if (this.grappled) {
        if (this.x > this.grapplePos.x) {
          if (this.x - this.grapplePos.x < 20) {
            if (checker(this.x - (this.x - this.grapplePos.x), this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x -= this.x - this.grapplePos.x;
            } else {
              //this.grappled = false;
            }
          } else {
            if (checker(this.x - 20, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x -= 20;
            } else {
              //this.grappled = false;
            }
          }
        } else if (this.x < this.grapplePos.x) {
          if (this.grapplePos.x - this.x < 20) {
            if (checker(this.x + (this.grapplePos.x - this.x), this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x += this.grapplePos.x - this.x;
            } else {
              //this.grappled = false;
            }
          } else {
            if (checker(this.x + 20, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x += 20;
            } else {
              //this.grappled = false;
            }
          }
        }
        if (this.y > this.grapplePos.y) {
          if (this.y - this.grapplePos.y < 20) {
            if (checker(this.x, this.y - (this.y - this.grapplePos.y), {
                x: this.x,
                y: this.y
              })) {
              this.y -= this.y - this.grapplePos.y;
            } else {
              //this.grappled = false;
            }
          } else {
            if (checker(this.x, this.y - 20, {
                x: this.x,
                y: this.y
              })) {
              this.y -= 20;
            } else {
              //this.grappled = false;
            }
          }
        } else if (this.y < this.grapplePos.y) {
          if (this.grapplePos.y - this.y < 20) {
            if (checker(this.x, this.y + (this.grapplePos.y - this.y), {
                x: this.x,
                y: this.y
              })) {
              this.y += this.grapplePos.y - this.y;
            } else {
              //this.grappled = false;
            }
          } else {
            if (checker(this.x, this.y + 20, {
                x: this.x,
                y: this.y
              })) {
              this.y += 20;
            } else {
              //this.grappled = false;
            }
          }
        }
        if (this.x == this.grapplePos.x && this.y == this.grapplePos.y) {
          this.grappled = false;
        }
        setTimeout(function() {
          if (this.grappled) this.grappled = false;
        }.bind(this), 10000);
        return;
      }
      if (!this.inactive) {
        var remap = false;
        this.xdistance = this.seeUserX - this.x;
        this.ydistance = this.seeUserY - this.y;
        if (this.xdistance == 0) {
          this.xdistance = 1;
        }
        this.rotation = Math.round(Math.atan(this.ydistance / this.xdistance) * 180 / Math.PI);
        if (this.xdistance > 0) {
          this.rotation -= 90;
        } else {
          this.rotation += 90;
        }
        if (this.seeUser) {
          this.rotation += 1;
          this.seeUserX = user.tank.x;
          this.seeUserY = user.tank.y
        }
        if (this.instructions.movement) { // check if movement is a valid value
          // followpath
          // include block break checker
          if (this.instructions.movement[0] == 2) {
            if (checker(this.x, this.y - 5, {
                x: this.x,
                y: this.y
              })) {
              this.y -= 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 1) {
            if (checker(this.x, this.y + 5, {
                x: this.x,
                y: this.y
              })) {
              this.y += 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 3) {
            if (checker(this.x - 5, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x -= 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 4) {
            if (checker(this.x + 5, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x += 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 5) {
            if (checker(this.x - 5, this.y - 5, {
                x: this.x,
                y: this.y
              })) {
              this.x -= 5;
              this.y -= 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 6) {
            if (checker(this.x + 5, this.y - 5, {
                x: this.x,
                y: this.y
              })) {
              this.x += 5;
              this.y -= 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 7) {
            if (checker(this.x - 5, this.y + 5, {
                x: this.x,
                y: this.y
              })) {
              this.x -= 5;
              this.y += 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 8) {
            if (checker(this.x + 5, this.y + 5, {
                x: this.x,
                y: this.y
              })) {
              this.x += 5;
              this.y += 5;
            } else {
              remap = true;
            }
          }
          if (remap) {
            var AiPosX = [];
            AiPosX.push(((this.x) - ((this.x) % 50)) / 50);
            AiPosX.push(((this.x + 40) - ((this.x + 40) % 50)) / 50);
            var AiPosY = [];
            AiPosY.push(((this.y) - ((this.y) % 50)) / 50);
            AiPosY.push(((this.y + 40) - ((this.y + 40) % 50)) / 50);
            if (AiPosX[0] == 30) {
              AiPosX[0] = 29;
            }
            if (AiPosX[1] == 30) {
              AiPosX[1] = 29;
            }
            if (AiPosY[0] == 30) {
              AiPosX[0] = 29;
            }
            if (AiPosY[1] == 30) {
              AiPosX[1] = 29;
            }
            if (AiPosX[0] == -1) {
              AiPosX[0] = 0;
            }
            if (AiPosX[1] == -1) {
              AiPosX[1] = 0;
            }
            if (AiPosY[0] == -1) {
              AiPosX[0] = 0;
            }
            if (AiPosY[1] == -1) {
              AiPosX[1] = 0;
            }
            var pathfind = true;
            var l = 0;
            while (l < AiPosX.length) {
              if (AiPosX[l] == this.instructions.AiPosX - 1 && checker(this.x + 5, this.y, {
                  x: this.x,
                  y: this.y
                })) {
                this.x += 5;
              }
              if (AiPosX[l] == this.instructions.AiPosX + 1 && checker(this.x - 5, this.y, {
                  x: this.x,
                  y: this.y
                })) {
                this.x -= 5;
              }
              l++;
            }
            var l = 0;
            while (l < AiPosY.length) {
              if (AiPosY[l] == this.instructions.AiPosY - 1 && checker(this.x, this.y + 5, {
                  x: this.x,
                  y: this.y
                })) {
                this.y += 5;
              }
              if (AiPosY[l] == this.instructions.AiPosY + 1 && checker(this.x, this.y - 5, {
                  x: this.x,
                  y: this.y
                })) {
                this.y -= 5;
              }
              l++;
            }
          }
        }
        if (this.instructions.sawUser) {
          // followpath
          // include block breaker
        }
      }
    }
    pathfind() {
      if (!this.inactive) {
        this.instructions = this.moveCalc();
      }
    }
    shoot(instance) {
      if (!instance.inactive) {
        if (instance.seeUser) {
          instance.fire();
        }
      }
    }
    fire() {
      this.pushback = -3;
      var xd = (this.x + 20) - (user.tank.x + 20);
      var yd = (this.y + 20) - (user.tank.y + 20);
      if (xd < 0) {
        var neg = false;
      } else {
        var neg = true;
      }
      if (xd == 0) {
        xd = 1;
      }
      yd = yd / xd;
      xd = 1;
      if (neg) {
        xd = 0 - 1;
        yd = 0 - yd;
      }
      var resize = 0;
      if (this.turret) resize = 5;
      if (this.fireType == 1) {
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd, yd, 'bullet', this.team, this.rotation, 0));
      } else if (this.fireType == 2) {
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd, yd, 'shotgun_bullet', this.team, this.rotation, 0));
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd + xd * .6, yd, 'shotgun_bullet', this.team, this.rotation, 0));
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd, yd - yd * .6, 'shotgun_bullet', this.team, this.rotation, 0));
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd, yd + yd * .9, 'shotgun_bullet', this.team, this.rotation, 0));
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd - xd * .9, yd, 'shotgun_bullet', this.team, this.rotation, 0));
      } else if (this.fireType == 3) {
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd, yd, 'condensed_bullet', this.team, this.rotation, 0));
        setTimeout(function(x, y, team, rotation) {
          s.push(new Shot(x + 20 + resize, y + 20 + resize, s.length - 1, xd, yd, 'condensed_bullet', team, rotation, 0));
          setTimeout(function(x, y, team, rotation) {
            s.push(new Shot(x + 20 + resize, y + 20 + resize, s.length - 1, xd, yd, 'condensed_bullet', team, rotation, 0));
            setTimeout(function(x, y, team, rotation) {
              s.push(new Shot(x + 20 + resize, y + 20 + resize, s.length - 1, xd, yd, 'condensed_bullet', team, rotation, 0));
              setTimeout(function(x, y, team, rotation) {
                s.push(new Shot(x + 20 + resize, y + 20 + resize, s.length - 1, xd, yd, 'condensed_bullet', team, rotation, 0));
              }, 20, x, y, team, rotation);
            }, 20, x, y, team, rotation);
          }, 20, x, y, team, rotation);
        }, 20, this.x, this.y, this.team, this.rotation);
      }
    }
    check() {
      if (userData.kit == 'thermal') {
        if (thermal_check(this.x, this.y) && this.canTakeThermalDamage) {
          this.health -= 20;
          if (this.health <= 0) {
            var max, min;
            if (this.fireType == 0) {
              if (Math.random() < .1 || userData.cosmetic == 'MLG Glasses') {
                Game.xp += 1;
              }
              max = 50;
              min = 0;
            } else if (this.fireType == 1) {
              if (Math.random() < .1 || userData.cosmetic == 'MLG Glasses') {
                Game.xp += 2;
              }
              max = 200;
              min = 100;
            } else if (this.fireType == 2) {
              if (Math.random() < .1 || userData.cosmetic == 'MLG Glasses') {
                Game.xp += 5;
              }
              max = 500;
              min = 200;
            } else if (this.fireType == 3) {
              if (Math.random() < .1) {
                Game.xp += 5;
              }
              max = 500;
              min = 200;
            }
            if (userData.kit == 'scavenger') {
              Game.coins += 3 * (Math.floor(Math.random() * max) + min);
            } else {
              Game.coins += Math.floor(Math.random() * max) + min;
            }
            Game.foes--;
            this.x = undefined;
            this.y = undefined;
            this.inactive = true;
            if (Game.foes == 0) {
              document.exitFullscreen();
              if (Game.level < 10000) {
                setTimeout(victory, 200);
                Game.endGame();
              }
            }
          }
          this.canTakeThermalDamage = false;
          setTimeout(function(tank) {
            tank.canTakeThermalDamage = true;
          }, 1000, this);
        }
      }
      var results = ai_check(this.x, this.y, this.turret);
      if (results[0] && results[2] != this.team && !results[3].grappled) {
        if (this.inactive != true) {
          draw.fillStyle = '#FF0000';
          draw.fillRect(this.x, this.y, 40, 40);
          this.health -= results[1];
          this.sawUser = true; // detects user if shot
          if (this.health <= 0) {
            var max, min;
            if (this.fireType == 0) {
              if (Math.random() < .1) {
                Game.xp += 1;
              }
              max = 50;
              min = 0;
            } else if (this.fireType == 1) {
              if (Math.random() < .1) {
                Game.xp += 2;
              }
              max = 200;
              min = 100;
            } else if (this.fireType == 2) {
              if (Math.random() < .1) {
                Game.xp += 5;
              }
              max = 500;
              min = 200;
            } else if (this.fireType == 3) {
              if (Math.random() < .1) {
                Game.xp += 5;
              }
              max = 500;
              min = 200;
            }
            if (userData.kit == 'scavenger') {
              Game.coins += 2 * (Math.floor(Math.random() * max) + min);
            } else {
              Game.coins += Math.floor(Math.random() * max) + min;
            }
            Game.foes--;
            this.x = undefined;
            this.y = undefined;
            this.inactive = true;
            if (Game.foes == 0) {
              document.exitFullscreen();
              if (Game.level < 10000) {
                setTimeout(victory, 200);
                Game.endGame();
              }
            }
          }
        }
      } else if (results[0] && results[2] != this.team && results[3].grappled) {
        this.grappled = true;
        this.grapplePos = {
          startx: this.x,
          starty: this.y,
          x: results[3].startx - 20,
          y: results[3].starty - 20,
        }
      }
    }
    moveCalc() {
      this.seeUser = false;
      var AiPosX = ((this.x + 20) - ((this.x + 20) % 50)) / 50;
      var AiPosY = ((this.y + 20) - ((this.y + 20) % 50)) / 50;
      var PlayerPosX = [];
      PlayerPosX.push(((user.tank.x) - ((user.tank.x) % 50)) / 50);
      PlayerPosX.push(((user.tank.x) - ((user.tank.x) % 50)) / 50);
      PlayerPosX.push(((user.tank.x + 40) - ((user.tank.x + 40) % 50)) / 50);
      PlayerPosX.push(((user.tank.x + 40) - ((user.tank.x + 40) % 50)) / 50);
      var PlayerPosY = [];
      PlayerPosY.push(((user.tank.y) - ((user.tank.y) % 50)) / 50);
      PlayerPosY.push(((user.tank.y + 40) - ((user.tank.y + 40) % 50)) / 50);
      PlayerPosY.push(((user.tank.y) - ((user.tank.y) % 50)) / 50);
      PlayerPosY.push(((user.tank.y + 40) - ((user.tank.y + 40) % 50)) / 50);
      if (PlayerPosX[0] == 30) {
        PlayerPosX[0] = 29;
        PlayerPosX[1] = 29;
      }
      if (PlayerPosX[2] == 30) {
        PlayerPosX[2] = 29;
        PlayerPosX[3] = 29;
      }
      if (PlayerPosY[0] == 30) {
        PlayerPosY[0] = 29;
        PlayerPosY[2] = 29;
      }
      if (PlayerPosY[1] == 30) {
        PlayerPosY[1] = 29;
        PlayerPosY[3] = 29;
      }
      if (PlayerPosX[0] == -1) {
        PlayerPosX[0] = 0;
        PlayerPosX[1] = 0;
      }
      if (PlayerPosX[2] == -1) {
        PlayerPosX[2] = 0;
        PlayerPosX[3] = 0;
      }
      if (PlayerPosY[0] == -1) {
        PlayerPosY[0] = 0;
        PlayerPosY[2] = 0;
      }
      if (PlayerPosY[1] == -1) {
        PlayerPosY[1] = 0;
        PlayerPosY[3] = 0;
      }
      var seeUser;
      var k = 0;
      //var startSeeUser = new Date();
      while (k < 4) {
        seeUser = true;
        if (PlayerPosY[k] - AiPosY == 0) { // needs work 
          var slope = 0;
          var yInt = AiPosY;
        } else if (PlayerPosX[k] - AiPosX == 0) {
          var slope = 'v'; // needs work
        } else {
          var slope = (PlayerPosY[k] - AiPosY) / (PlayerPosX[k] - AiPosX);
          var yInt = -(slope * AiPosX - AiPosY);
        }
        if (slope == 'v') {
          if (PlayerPosY[k] > AiPosY) {
            var l = AiPosY;
            while (l < PlayerPosY[k]) {
              if (l >= 0) {
                if (l <= 30) {
                  if (Game.map[l][PlayerPosX[k]] == '1') {
                    seeUser = false;
                  }
                }
              }
              l++;
            }
          } else {
            var l = PlayerPosY[k];
            while (l < AiPosY) {
              if (l >= 0) {
                if (l <= 30) {
                  if (Game.map[l][AiPosX] == '1') {
                    seeUser = false;
                  }
                }
              }
              l++;
            }
          }
        } else if (slope == 0) {
          if (PlayerPosX[k] > AiPosX) {
            var l = AiPosX;
            while (l < PlayerPosX[k]) {
              if (l >= 0) {
                if (l <= 30) {
                  if (Game.map[AiPosY][l] == '1') {
                    seeUser = false;
                  }
                }
              }
              l++;
            }
          } else {
            var l = PlayerPosX[k];
            while (l < AiPosX) {
              if (l >= 0) {
                if (l <= 30) {
                  if (Game.map[PlayerPosY[k]][l] == '1') {
                    seeUser = false;
                  }
                }
              }
              l++;
            }
          }
        } else {
          if (PlayerPosX[k] > AiPosX) {
            var l = AiPosX;
            while (l < PlayerPosX[k]) {
              var y = slope * l + yInt;
              if (y >= 0 && l >= 0) {
                if (Math.round(l) < 30 && Math.round(y) < 30) {
                  if (Game.map[Math.round(y)][Math.round(l)] == '1') {
                    seeUser = false;
                  }
                }
              }
              l += .1;
            }
          } else {
            var l = PlayerPosX[k];
            while (l < AiPosX) {
              var y = slope * l + yInt;
              if (y >= 0 && l >= 0) {
                if (Math.round(l) < 30 && Math.round(y) < 30) {
                  if (Game.map[Math.round(y)][Math.round(l)] == '1') {
                    seeUser = false;
                  }
                }
              }
              l += .1;
            }
          }
        }
        if ((seeUser && !user.tank.invis) || (user.tank.invis && this.sawUser)) {
          this.seeUser = true;
        }
        seeUser = this.seeUser;
        if (seeUser) {
          this.sawUser = true;
        }
        k++
      }
      if (!this.turret) {
        PlayerPosX = ((user.tank.x + 20) - ((user.tank.x + 20) % 50)) / 50;
        PlayerPosY = ((user.tank.y + 20) - ((user.tank.y + 20) % 50)) / 50;
        var xdistance = Math.abs((user.tank.x + 20) - (this.x + 20));
        var ydistance = Math.abs((user.tank.y + 20) - (this.y + 20));
        var distance = Math.sqrt((xdistance * xdistance) + (ydistance * ydistance));
        if (seeUser || (this.sawUser && distance <= 600)) {
          var pathfind = [{
            x: AiPosX,
            y: AiPosY,
            route: [],
            old: [],
            complete: false,
            step: 0,
            color: generateRandomColor(),
          }];
          var complete = false;
          while (!complete) {
            complete = true;
            var l = 0;
            while (l < pathfind.length) {
              if (!pathfind[l].complete) {
                var action = false;
                var spawnNew = false;
                var down = true,
                  up = true,
                  left = true,
                  right = true;
                var obsticale = false;
                var current = JSON.parse(JSON.stringify(pathfind[l]));
                if (current.y + 1 < 31 && Math.abs(current.y + 1 - PlayerPosY) < Math.abs(current.y - PlayerPosY)) {
                  if (Game.map[current.y + 1][current.x] == '1') {
                    obsticale = true;
                  }
                }
                if (current.y - 1 > -1 && Math.abs(current.y - 1 - PlayerPosY) < Math.abs(current.y - PlayerPosY)) {
                  if (Game.map[current.y - 1][current.x] == '1') {
                    obsticale = true;
                  }
                }
                if (current.x + 1 < 31 && Math.abs(current.x + 1 - PlayerPosX) < Math.abs(current.x - PlayerPosX)) {
                  if (Game.map[current.y][current.x + 1] == '1') {
                    obsticale = true;
                  }
                }
                if (current.x - 1 > -1 && Math.abs(current.x - 1 - PlayerPosX) < Math.abs(current.x - PlayerPosX)) {
                  if (Game.map[current.y][current.x - 1] == '1') {
                    obsticale = true;
                  }
                }
                var q = 0;
                while (q < current.old.length) {
                  if (current.x == current.old[q].x) {
                    if (current.y + 1 == current.old[q].y) {
                      down = false;
                    }
                  }
                  q++;
                }
                var q = 0;
                while (q < current.old.length) {
                  if (current.x == current.old[q].x) {
                    if (current.y - 1 == current.old[q].y) {
                      up = false;
                    }
                  }
                  q++;
                }
                var q = 0;
                while (q < current.old.length) {
                  if (current.x - 1 == current.old[q].x) {
                    if (current.y == current.old[q].y) {
                      left = false;
                    }
                  }
                  q++;
                }
                var q = 0;
                while (q < current.old.length) {
                  if (current.x + 1 == current.old[q].x) {
                    if (current.y == current.old[q].y) {
                      right = false;
                    }
                  }
                  q++;
                }
                var optDown = false,
                  optUp = false,
                  optLeft = false,
                  optRight = false;
                var cantDownRight = false,
                  cantDownLeft = false,
                  cantUpRight = false,
                  cantUpLeft = false;
                if (current.y + 1 < 31 && (Math.abs(current.y + 1 - PlayerPosY) < Math.abs(current.y - PlayerPosY) || obsticale)) {
                  if (Game.map[current.y + 1][current.x] == '0' && down) {
                    optDown = true;
                  }
                }
                if (current.y - 1 > -1 && (Math.abs(current.y - 1 - PlayerPosY) < Math.abs(current.y - PlayerPosY) || obsticale)) {
                  if (Game.map[current.y - 1][current.x] == '0' && up) {
                    optUp = true;
                  }
                }
                if (current.x + 1 < 31 && (Math.abs(current.x + 1 - PlayerPosX) < Math.abs(current.x - PlayerPosX) || obsticale)) {
                  if (Game.map[current.y][current.x + 1] == '0' && right) {
                    optRight = true;
                  }
                }
                if (current.x - 1 > -1 && (Math.abs(current.x - 1 - PlayerPosX) < Math.abs(current.x - PlayerPosX) || obsticale)) {
                  if (Game.map[current.y][current.x - 1] == '0' && left) {
                    optLeft = true;
                  }
                }
                if (optDown && optRight && current.x + 1 < 31 && current.y + 1 < 31 && !obsticale) {
                  if (Game.map[current.y + 1][current.x + 1] == '0') {
                    if (spawnNew) {
                      var n = JSON.parse(JSON.stringify(current));
                      n.old.push({
                        x: n.x,
                        y: n.y
                      });
                      n.route.push(8);
                      n.x += 1;
                      n.y += 1;
                      n.color = generateRandomColor();
                      n.step++;
                      pathfind.push(n);
                      draw.beginPath();
                      draw.strokeStyle = n.color;
                      draw.moveTo((n.x - 1) * 50, (n.y - 1) * 50);
                      draw.lineTo(n.x * 50, n.y * 50);
                      draw.stroke();
                    } else {
                      pathfind[l].old.push({
                        x: pathfind[l].x,
                        y: pathfind[l].y
                      });
                      pathfind[l].route.push(8);
                      pathfind[l].x += 1;
                      pathfind[l].y += 1;
                      pathfind[l].step++;
                      spawnNew = true;
                      draw.beginPath();
                      draw.strokeStyle = pathfind[l].color;
                      draw.moveTo((pathfind[l].x - 1) * 50, (pathfind[l].y - 1) * 50);
                      draw.lineTo(pathfind[l].x * 50, pathfind[l].y * 50);
                      draw.stroke();
                    }
                    action = true;
                  } else {
                    cantDownRight = true;
                  }
                } else {
                  cantDownRight = true;
                }
                if (optLeft && optUp && current.y - 1 > -1 && current.x - 1 > -1 && !obsticale) {
                  if (Game.map[current.y - 1][current.x - 1] == '0') {
                    if (spawnNew) {
                      var n = JSON.parse(JSON.stringify(current));
                      n.old.push({
                        x: n.x,
                        y: n.y
                      });
                      n.route.push(5);
                      n.x -= 1;
                      n.y -= 1;
                      n.color = generateRandomColor();
                      n.step++;
                      pathfind.push(n);
                      draw.beginPath();
                      draw.strokeStyle = n.color;
                      draw.moveTo((n.x + 1) * 50, (n.y + 1) * 50);
                      draw.lineTo(n.x * 50, n.y * 50);
                      draw.stroke();
                    } else {
                      pathfind[l].old.push({
                        x: pathfind[l].x,
                        y: pathfind[l].y
                      });
                      pathfind[l].route.push(5);
                      pathfind[l].x -= 1;
                      pathfind[l].y -= 1;
                      pathfind[l].step++;
                      spawnNew = true;
                      draw.beginPath();
                      draw.strokeStyle = pathfind[l].color;
                      draw.moveTo((pathfind[l].x + 1) * 50, (pathfind[l].y + 1) * 50);
                      draw.lineTo(pathfind[l].x * 50, pathfind[l].y * 50);
                      draw.stroke();
                    }
                    action = true;
                  } else {
                    cantUpLeft = true;
                  }
                } else {
                  cantUpLeft = true;
                }
                if (optRight && optUp && current.y - 1 > -1 && current.x + 1 < 31 && !obsticale) {
                  if (Game.map[current.y - 1][current.x + 1] == '0') {
                    if (spawnNew) {
                      var n = JSON.parse(JSON.stringify(current));
                      n.old.push({
                        x: n.x,
                        y: n.y
                      });
                      n.route.push(6);
                      n.y -= 1;
                      n.x += 1;
                      n.color = generateRandomColor();
                      n.step++;
                      pathfind.push(n);
                      draw.beginPath();
                      draw.strokeStyle = n.color;
                      draw.moveTo((n.x - 1) * 50, (n.y + 1) * 50);
                      draw.lineTo(n.x * 50, n.y * 50);
                      draw.stroke();
                    } else {
                      pathfind[l].old.push({
                        x: pathfind[l].x,
                        y: pathfind[l].y
                      });
                      pathfind[l].route.push(6);
                      pathfind[l].y -= 1;
                      pathfind[l].x += 1;
                      pathfind[l].step++;
                      spawnNew = true;
                      draw.beginPath();
                      draw.strokeStyle = pathfind[l].color;
                      draw.moveTo((pathfind[l].x - 1) * 50, (pathfind[l].y + 1) * 50);
                      draw.lineTo(pathfind[l].x * 50, pathfind[l].y * 50);
                      draw.stroke();
                    }
                    action = true;
                  } else {
                    cantUpRight = true;
                  }
                } else {
                  cantUpRight = true;
                }
                if (optDown && optLeft && current.y + 1 < 31 && current.x - 1 > -1 && !obsticale) {
                  if (Game.map[current.y + 1][current.x - 1] == '0') {
                    if (spawnNew) {
                      var n = JSON.parse(JSON.stringify(current));
                      n.old.push({
                        x: n.x,
                        y: n.y
                      });
                      n.route.push(7);
                      n.y += 1;
                      n.x -= 1;
                      n.color = generateRandomColor();
                      n.step++;
                      pathfind.push(n);
                      draw.beginPath();
                      draw.strokeStyle = n.color;
                      draw.moveTo((n.x + 1) * 50, (n.y - 1) * 50);
                      draw.lineTo(n.x * 50, n.y * 50);
                      draw.stroke();
                    } else {
                      pathfind[l].old.push({
                        x: pathfind[l].x,
                        y: pathfind[l].y
                      });
                      pathfind[l].route.push(7);
                      pathfind[l].y += 1;
                      pathfind[l].x -= 1;
                      pathfind[l].step++;
                      spawnNew = true;
                      draw.beginPath();
                      draw.strokeStyle = pathfind[l].color;
                      draw.moveTo((pathfind[l].x + 1) * 50, (pathfind[l].y - 1) * 50);
                      draw.lineTo(pathfind[l].x * 50, pathfind[l].y * 50);
                      draw.stroke();
                    }
                    action = true;
                  } else {
                    cantDownLeft = true;
                  }
                } else {
                  cantDownLeft = true;
                }
                if ((optDown && !optRight && !optLeft) || (optDown && ((cantDownRight && cantDownLeft) || obsticale))) {
                  if (spawnNew) {
                    var n = JSON.parse(JSON.stringify(current));
                    n.old.push({
                      x: n.x,
                      y: n.y
                    });
                    n.route.push(1);
                    n.y++;
                    n.color = generateRandomColor();
                    n.step++;
                    pathfind.push(n);
                    draw.beginPath();
                    draw.strokeStyle = n.color;
                    draw.moveTo((n.x) * 50, (n.y - 1) * 50);
                    draw.lineTo(n.x * 50, n.y * 50);
                    draw.stroke();
                  } else {
                    pathfind[l].old.push({
                      x: pathfind[l].x,
                      y: pathfind[l].y
                    });
                    pathfind[l].route.push(1);
                    pathfind[l].y++;
                    pathfind[l].step++;
                    spawnNew = true;
                    draw.beginPath();
                    draw.strokeStyle = pathfind[l].color;
                    draw.moveTo((pathfind[l].x) * 50, (pathfind[l].y - 1) * 50);
                    draw.lineTo(pathfind[l].x * 50, pathfind[l].y * 50);
                    draw.stroke();
                  }
                  action = true;
                }
                if ((optUp && !optRight && !optLeft) || (optUp && ((cantUpRight && cantUpLeft) || obsticale))) {
                  if (spawnNew) {
                    var n = JSON.parse(JSON.stringify(current));
                    n.old.push({
                      x: n.x,
                      y: n.y
                    });
                    n.route.push(2);
                    n.y -= 1;
                    n.color = generateRandomColor();
                    n.step++;
                    pathfind.push(n);
                    draw.beginPath();
                    draw.strokeStyle = n.color;
                    draw.moveTo((n.x) * 50, (n.y + 1) * 50);
                    draw.lineTo(n.x * 50, n.y * 50);
                    draw.stroke();
                  } else {
                    pathfind[l].old.push({
                      x: pathfind[l].x,
                      y: pathfind[l].y
                    });
                    pathfind[l].route.push(2);
                    pathfind[l].y -= 1;
                    pathfind[l].step++;
                    spawnNew = true;
                    draw.beginPath();
                    draw.strokeStyle = pathfind[l].color;
                    draw.moveTo((pathfind[l].x) * 50, (pathfind[l].y + 1) * 50);
                    draw.lineTo(pathfind[l].x * 50, pathfind[l].y * 50);
                    draw.stroke();
                  }
                  action = true;
                }
                if ((optLeft && !optDown && !optUp) || (optLeft && ((cantDownLeft && cantUpLeft) || obsticale))) {
                  if (spawnNew) {
                    var n = JSON.parse(JSON.stringify(current));
                    n.old.push({
                      x: n.x,
                      y: n.y
                    });
                    n.route.push(3);
                    n.x -= 1;
                    n.color = generateRandomColor();
                    n.step++;
                    pathfind.push(n);
                    draw.beginPath();
                    draw.strokeStyle = n.color;
                    draw.moveTo((n.x + 1) * 50, (n.y) * 50);
                    draw.lineTo(n.x * 50, n.y * 50);
                    draw.stroke();
                  } else {
                    pathfind[l].old.push({
                      x: pathfind[l].x,
                      y: pathfind[l].y
                    });
                    pathfind[l].route.push(3);
                    pathfind[l].x -= 1;
                    pathfind[l].step++;
                    spawnNew = true;
                    draw.beginPath();
                    draw.strokeStyle = pathfind[l].color;
                    draw.moveTo((pathfind[l].x + 1) * 50, (pathfind[l].y) * 50);
                    draw.lineTo(pathfind[l].x * 50, pathfind[l].y * 50);
                    draw.stroke();
                  }
                  action = true;
                }
                if ((optRight && !optDown && !optUp) || (optRight && ((cantDownRight && cantUpRight) || obsticale))) {
                  if (spawnNew) {
                    var n = JSON.parse(JSON.stringify(current));
                    n.old.push({
                      x: n.x,
                      y: n.y
                    });
                    n.route.push(4);
                    n.x += 1;
                    n.color = generateRandomColor();
                    n.step++;
                    pathfind.push(n);
                    draw.beginPath();
                    draw.strokeStyle = n.color;
                    draw.moveTo((n.x - 1) * 50, (n.y) * 50);
                    draw.lineTo(n.x * 50, n.y * 50);
                    draw.stroke();
                  } else {
                    pathfind[l].old.push({
                      x: pathfind[l].x,
                      y: pathfind[l].y
                    });
                    pathfind[l].route.push(4);
                    pathfind[l].x += 1;
                    pathfind[l].step++;
                    spawnNew = true;
                    draw.beginPath();
                    draw.strokeStyle = pathfind[l].color;
                    draw.moveTo((pathfind[l].x - 1) * 50, (pathfind[l].y) * 50);
                    draw.lineTo(pathfind[l].x * 50, pathfind[l].y * 50);
                    draw.stroke();
                  }
                  action = true;
                }
                var q = 0;
                while (q < pathfind.length) {
                  if (pathfind[q].x == PlayerPosX && pathfind[q].y == PlayerPosY) {
                    pathfind[q].complete = true;
                  }
                  if (pathfind[q].step > 15) { // Limit of 15 blocks for pathfinding
                    delete pathfind[q];
                    pathfind.splice(q, 1);
                  } // pathfind length limit
                  q++;
                }
                if (!action) {
                  delete pathfind[l];
                  pathfind.splice(l, 1);
                }
              }
              l++;
            }
            var q = 0;
            while (q < pathfind.length) {
              if (!pathfind[q].complete) {
                complete = false;
              }
              q++;
            }
            /*if (doPathfindTracing) {
              var l = 0;
              while (l<pathfind.length) {
                var o = 1;
                draw.strokeStyle = "green";
                while (o<pathfind[l].old.length) {
                  draw.beginPath();
                  draw.lineWidth = '3';
                  draw.moveTo(pathfind[l].old[o-1].x*50+25, pathfind[l].old[o-1].y*50+25);
                  draw.lineTo(pathfind[l].old[o].x*50+25, pathfind[l].old[o].y*50+25);
                  draw.stroke();
                  draw.closePath();
                  o++;
                }
                draw.beginPath();
                draw.lineTo(pathfind[l].x+25, pathfind[l].y+25);
                draw.stroke();
                draw.closePath();
                l++;
              }
            //}*/
          }
          //var pathfindEndTime = new Date();
          //var seeUserTime = endSeeUser.getMilliseconds() - startSeeUser.getMilliseconds();
          //var pathfindTime = pathfindEndTime.getMilliseconds() - startPathfind.getMilliseconds();
          //saveStatus.innerHTML = 'SeeUser Time: '+seeUserTime+' pathfindTime: '+pathfindTime+' with '+iterations+' iterations';
          pathfind.sort(function(a, b) {
            return a.step - b.step;
          });
          if (pathfind.length != 0) {
            return {
              seeUser: seeUser,
              sawUser: this.sawUser,
              movement: pathfind[0].route,
              old: pathfind[0].old,
              AiPosX: AiPosX,
              AiPosY: AiPosY
            };
          } else {
            return {
              seeUser: seeUser,
              sawUser: this.sawUser,
              movement: [],
              old: [],
              AiPosX: AiPosX,
              AiPosY: AiPosY
            }
          }
        }
      }
      return {
        seeUser: seeUser,
        sawUser: this.sawUser,
        movement: []
      }
    }
  }
  class Shot {
    constructor(x, y, id, xm, ym, type, team, originalRotation, rank) {
      this.particleFrame = 0;
      this.rot = originalRotation;
      this.team = team;
      this.addx = x;
      this.addy = y;
      this.type = type;
      this.xm = xm;
      this.ym = ym;
      while (this.xm * this.xm + this.ym * this.ym > 1.2 || this.xm * this.xm + this.ym * this.ym < 1) {
        if (this.xm * this.xm + this.ym * this.ym > 1.1) {
          this.xm /= 1.01;
          this.ym /= 1.01;
        }
        if (this.xm * this.xm + this.ym * this.ym < 1.1) {
          this.xm *= 1.01;
          this.ym *= 1.01;
        }
      }
      this.xm = this.xm * 7;
      this.ym = this.ym * 7;
      this.x1 = 20; // get point (x1, y1) on line that intersects circle
      this.y1 = (this.ym / this.xm) * this.x1;
      this.x2 = -20; // get point (x2, y2) on line that intersects circle
      this.y2 = (this.ym / this.xm) * this.x2;
      this.dx = this.x2 - this.x1;
      this.dy = this.y2 - this.y1;
      this.dr = Math.sqrt(this.dx * this.dx + this.dy * this.dy);
      this.d = this.x1 * this.y2 - this.x2 * this.y1;
      if (this.dy < 0) {
        this.sgn = -1;
      } else {
        this.sgn = 1;
      }
      // evil math stuff:
      this.x_1 = (this.d * this.dy + this.sgn * this.dx * Math.sqrt(1225 * (this.dr * this.dr) - (this.d * this.d))) / (this.dr * this.dr);
      this.x_2 = (this.d * this.dy - this.sgn * this.dx * Math.sqrt(1225 * (this.dr * this.dr) - (this.d * this.d))) / (this.dr * this.dr);
      this.y_1 = (-this.d * this.dx + Math.abs(this.dy) * Math.sqrt(1225 * (this.dr * this.dr) - (this.d * this.d))) / (this.dr * this.dr);
      this.y_2 = (-this.d * this.dx - Math.abs(this.dy) * Math.sqrt(1225 * (this.dr * this.dr) - (this.d * this.d))) / (this.dr * this.dr);
      //this.y_1 *= -1; //convert from normal graph to canvas graph
      //this.y_2 *= -1;
      if (this.ym >= 0) {
        this.x = this.x_1 + this.addx;
        this.y = this.y_1 + this.addy;
      } else {
        this.x = this.x_2 + this.addx;
        this.y = this.y_2 + this.addy;
      }
      this.counter = 0;
      this.id = id;
      // for exact angles with bugs and infinite or zero slope
      if (xm == 0 && ym == 1) {
        this.x = 0 + this.addx;
        this.y = 35 + this.addy;
      } else if (xm == -1 && ym == 0) {
        this.x = -35 + this.addx;
        this.y = 0 + this.addy;
      } else if (xm == 0 && ym == -1) {
        this.x = 0 + this.addx;
        this.y = -35 + this.addy;
      } else if (xm == 1 && ym == 0) {
        this.x = 35 + this.addx;
        this.y = 0 + this.addy;
      }
      this.startx = this.x;
      this.starty = this.y;
      if (this.type == 'bullet') {
        this.damage = 20;
      } else if (this.type == 'powermissle') {
        this.xm *= 2;
        this.ym *= 2;
        this.damage = 100;
      } else if (this.type == 'megamissle') {
        this.xm *= 2;
        this.ym *= 2;
        this.damage = 200;
      } else if (this.type == 'shotgun_bullet') {
        this.damage = 25;
      } else if (this.type == 'condensed_bullet') {
        this.damage = 5;
      } else if (this.type == 'grapple') {
        this.damage = 25;
      }
      this.ownerRank = rank;
      this.damage *= 1 + .05 * rank;
    }
    draw() {
      if (this.type == 'bullet') {
        draw.fillStyle = '#000000';
        draw.fillRect(this.x, this.y, 5, 5);
      } else if (this.type == 'powermissle') {
        draw.translate(this.x + 2.5, this.y + 2.5);
        draw.rotate(this.rot * Math.PI / 180);
        draw.drawImage(powermissle, -2.5, 2.5, 10, 20);
        draw.rotate(-this.rot * Math.PI / 180);
        draw.translate(-this.x - 2.5, -this.y - 2.5);
      } else if (this.type == 'megamissle') {
        draw.translate(this.x + 2.5, this.y + 2.5);
        draw.rotate(this.rot * Math.PI / 180);
        draw.drawImage(megamissle, -2.5, 2.5, 10, 20);
        draw.rotate(-this.rot * Math.PI / 180);
        draw.translate(-this.x - 2.5, -this.y - 2.5);
      } else if (this.type == 'shotgun_bullet') {
        draw.drawImage(shotgun_bullet, this.x, this.y);
      } else if (this.type == 'condensed_bullet') {
        draw.drawImage(condensed_bullet, this.x, this.y);
      } else if (this.type == 'grapple') {
        var particleAngle = this.rot + 180;
        if (particleAngle > 360) this.rot -= 360;
        draw.translate(this.x + 2.5, this.y + 2.5);
        draw.rotate(particleAngle * Math.PI / 180);
        draw.drawImage(grapple, -22.5, -22.5);
        draw.rotate(-particleAngle * Math.PI / 180);
        draw.translate(-this.x - 2.5, -this.y - 2.5);
        draw.strokeStyle = 'black';
        draw.lineWidth = 5;
        draw.beginPath();
        draw.moveTo(this.x, this.y);
        draw.lineTo(this.startx, this.starty);
        draw.stroke();
      }
      var particleAngle = this.rot + 180;
      if (particleAngle > 360) this.rot -= 360;
      draw.translate(this.x + 2.5, this.y + 2.5);
      draw.rotate(particleAngle * Math.PI / 180);
      draw.drawImage(bullet_particle[this.particleFrame], -2.5, 2.5);
      draw.rotate(-particleAngle * Math.PI / 180);
      draw.translate(-this.x - 2.5, -this.y - 2.5);
    }
    update() {
      if (this.particleFrame == 4) {
        this.particleFrame = 0;
      } else {
        this.particleFrame++;
      }
      if (this.type == 'grapple') {
        this.x += this.xm * 2;
        this.y += this.ym * 2;
      } else {
        this.x += this.xm / 2;
        this.y += this.ym / 2;
      }
      if (this.type == 'shotgun_bullet') {
        this.distance = Math.sqrt(Math.abs(this.x - this.startx) * Math.abs(this.x - this.startx) + Math.abs(this.y - this.starty) * Math.abs(this.y - this.starty));
        this.damage = 25 - (this.distance / 200) * 25;
        this.damage *= 1 + .05 * this.ownerRank;
        if (this.distance >= 150) {
          var l = 0;
          while (l < s.length) {
            if (s[l].distance == this.distance) {
              s.splice(l, 1);
              delete this;
            }
            l++;
          }
        }
      }
      if (this.type == 'condensed_bullet') {
        var distance = Math.sqrt(Math.abs(this.x - this.startx) * Math.abs(this.x - this.startx) + Math.abs(this.y - this.starty) * Math.abs(this.y - this.starty));
        this.damage = 5 + (distance / 500) * 25;
        this.damage *= 1 + .05 * this.ownerRank
      }
    }
  }

  function clearShot() {
    if (user.tank != undefined) {
      level(Game.level, 'n', false);
    }
  }

  function pauseM() {
    cancelAnimationFrame(user.joiner.drawLoop);
    cancelAnimationFrame(user.joiner.outputLoop);
    user.joiner.socket.onmessage = function() {}
    canvas.removeEventListener('keydown', tank_M_listener1);
    canvas.removeEventListener('keyup', tank_M_listener2);
    document.removeEventListener('mousemove', tank_M_listener3);
    document.removeEventListener('mousedown', tank_M_listener4);
    document.removeEventListener('mouseup', tank_M_listener5);
  }

  function resumeM() {
    user.joiner.intervals();
    user.joiner.socket.onmessage = function(data) {
      data = JSON.parse(data.data);
      if (data.event == 'hostupdate') {
        pings.server.push(new Date(data.sendTime).getTime() - new Date(new Date().toISOString()).getTime());
        if (pings.server.length > 60) {
          pings.server.shift();
        }
        var tank = false,
          block = false,
          bullet = false;
        user.joiner.hostupdate.sendTime = data.sendTime;
        user.joiner.hostupdate.gameMessage = data.gameMessage;
        if (data.tanks) {
          tank = true;
          user.joiner.hostupdate.tanks = data.tanks;
          pt = user.joiner.hostupdate.tanks;
        }
        if (data.scaffolding) {
          user.joiner.hostupdate.scaffolding = data.scaffolding;
        }
        if (data.blocks) {
          block = true;
          user.joiner.hostupdate.blocks = data.blocks;
        }
        if (data.bullets) {
          bullet = true;
          user.joiner.hostupdate.bullets = data.bullets;
        }
      } else if (data.event == 'gameover') {
        gameover(data.data);
      } else if (data.event == 'override') {
        user.joiner.tank[data.data.key] = data.data.value;
        user.joiner.tank[data.data.key2] = data.data.value2;
      } else if (data.event == 'kill') {
        userData.crates += Math.floor(Math.random() * (2) + 1);
        userData.xp += 10;
        saveGame();
      }
    };
    canvas.addEventListener('keydown', tank_M_listener1, false);
    canvas.addEventListener('keyup', tank_M_listener2, false);
    document.addEventListener('mousemove', tank_M_listener3, false);
    document.addEventListener('mousedown', tank_M_listener4, false);
    document.addEventListener('mouseup', tank_M_listener5);
  }
  var pauseMultiplayerM = new Menu(function() {
    draw.setTransform(window.resizer, 0, 0, window.resizer, 0, 0);
    draw.drawImage(pause_menu, 0, 0);
  }, {
    click: function pauseMultiplayerOnClick(e) {
      e = e || event;
      if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
      } else {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }
      x -= canvas.offsetLeft;
      y -= canvas.offsetTop;
      x /= window.resizer;
      y /= window.resizer;
      if (x > 180) {
        if (x < 321) {
          if (y > 191) {
            if (y < 239) {
              this.removeListeners();
              user.joiner.paused = false;
              resumeM();
            }
          }
        }
      }
      if (x > 180) {
        if (x < 321) {
          if (y > 255) {
            if (y < 303) {
              this.removeListeners();
              endM();
              mainMenu();
            }
          }
        }
      }
    }
  }, document);

  function pauseMultiplayer() {
    pauseMultiplayerM.draw();
    pauseMultiplayerM.addListeners();
  }

  function endM() {
    cancelAnimationFrame(user.joiner.drawLoop);
    cancelAnimationFrame(user.joiner.outputLoop)
    user.joiner.socket.close();
    document.removeEventListener('keydown', pauseLM);
  }

  function pauseLM(e) {
    e = e || event;
    if (e.keyCode == 27 && user.joiner.canChangePaused) {
      user.joiner.canChangePaused = false;
      setTimeout(function() {
        user.joiner.canChangePaused = true;
      }, 1000);
      if (user.joiner.paused) {
        user.joiner.paused = false;
        resumeM();
      } else {
        user.joiner.paused = true;
        pauseM();
        pauseMultiplayer();
      }
    }
  }
  var mainMenuTimer;

  function victoryM(data, double, crates, coins) {
    if (double == undefined) {
      double = false;
    }
    draw.drawImage(tank_ui['victory'].image, 0, 0);
    draw.textAlign = 'center';
    draw.fillStyle = '#ffffff';
    draw.font = '15px starfont'
    draw.fillText(data.message, 250, 200);
    draw.font = '10px starfont';
    if (!double) {
      draw.fillText('Click anywhere to watch a', 250, 250);
      draw.fillText('video for double rewards!', 250, 275);
    }
    draw.fillText('Rewards', 250, 300);
    var q = Math.floor(Math.random() * (5) + 1);
    if (double) q = crates * 2;
    draw.fillText('You Earned ' + q + ' Crates!', 250, 325);
    var w = Math.floor(Math.random() * (1000 - 0 + 1)) + 0;
    if (double) w = coins * 2;
    draw.fillText('You Earned ' + w + ' Coins!', 250, 350);
    draw.fillText('You Earned 15 XP!', 250, 375);
    userData.xp += 15;
    userData.crates += q;
    userData.coins += w;
    saveGame();
    draw.textAlign = 'left';
    mainMenuTimer = setTimeout(mainMenu, 5000);
    if (!double) {
      //document.addEventListener('click', adOnClick);
    }
  }

  function adOnClick() {
    clearTimeout(mainMenuTimer);
    ad(function() {
      document.
      victoryM(data, true, q, w);
    });
    document.removeEventListener('click', adOnClick);
  }

  function defeatM(data) {
    draw.drawImage(tank_ui['defeat'].image, 0, 0);
    draw.textAlign = 'center';
    draw.fillStyle = '#ffffff';
    draw.font = '15px starfont'
    draw.fillText(data.message, 250, 200);
    draw.font = '10px starfont';
    draw.fillText('you lost', 250, 250);
    draw.fillText('you lost', 250, 300);
    draw.textAlign = 'left';
    setTimeout(mainMenu, 5000);
  }

  function gameover(data) {
    endM();
    if (data.winner == user.username) {
      setTimeout(victoryM, 3000, data);
    } else {
      setTimeout(defeatM, 3000, data);
    }
  }
  class Joiner {
    control(channelname, gamemode) {
      this.canUpdate = true;
      this.hostupdate = {};
      this.FPS = 60;
      this.gamemode = gamemode;
      this.canAd = true;
      this.canChangePaused = true;
      this.paused = false;
      document.addEventListener('keydown', pauseLM)
      this.lagometer = false;
      this.tank = {
        canGrapple: true,
        team: sessionStorage.username,
        invis: false,
        fireType: 1,
        canBlockShield: true,
        isBuilder: false,
        canTakeThermalDamage: true,
        isThermal: false,
        canFire: true,
        shields: 0,
        x: 730,
        y: 730,
        rotation: 0,
        health: userData.health * (1 + .02 * userData.rank),
        rank: userData.rank,
        leftright: true,
        username: user.username,
        speed: 2,
        count: 0,
        CanBoost: true,
        usingToolkit: false,
        CanToolkit: true,
        canPlaceScaffolding: true,
        placeScaffolding: false,
        pushback: 0,
        base: 0,
        shielded: false,
        canShield: true,
        flashbangFired: false,
        canFireFlashbang: true,
        immune: false,
        canInvis: true,
        invis: false,
        canChangeInvisStatus: true,
        class: userData.class,
        altAttack: true,
        megaAttack: true,
      };
      if (userData.health == 200) {
        user.joiner.tank.material = 'normal';
      } else if (userData.health == 300) {
        user.joiner.tank.material = 'iron';
      } else if (userData.health == 400) {
        user.joiner.tank.material = 'diamond';
      } else if (userData.health == 500) {
        user.joiner.tank.material = 'dark';
      } else if (userData.health == 600) {
        user.joiner.tank.material = 'light';
      }
      this.tank.helper = [];
      this.tank.intervals = [];
      this.frameOuput = 0;
      this.socket = new WebSocket('wss://' + window.location.hostname + '/server');
      this.channelname = channelname;
      this.drawMap = function() {
        if (!user.joiner.canUpdate) {
          user.joiner.drawLoop = requestAnimationFrame(user.joiner.drawMap);
          return;
        }
        //user.joiner.canUpdate = false;
        var l = 0;
        var len = user.joiner.hostupdate.tanks.length;
        while (l < len) {
          if (pt[l].username == user.username) {
            pings.client = pt[l].pings.slice(Math.max(pt[l].pings.length - 50, 0));
            var q = 0;
            var min = 100000000000;
            var max = 0;
            while (q < pings.client.length) {
              if (pings.client[q] < min) {
                min = pings.client[q];
              }
              if (pings.client[q] > max) {
                max = pings.client[q];
              }
              q++;
            }
            var smin = 100000000000;
            var smax = 0;
            while (q < pings.server.length) {
              if (pings.server[q] < smin) {
                smin = pings.server[q];
              }
              if (pings.server[q] > smax) {
                smax = pings.server[q];
              }
              q++;
            }
            if (pt[l].ded && user.joiner.canAd) {
              user.joiner.canAd = false;
              setTimeout(function() {
                user.joiner.canAd = true;
              }, 15000);
            }
            if (pt[l].health < 60 && userData.kit == 'autoheal') {
              if (user.joiner.tank.CanToolkit) {
                if (userData.toolkits > 0) {
                  toolkitAnimation = new Animation(toolkitOpt);
                  user.joiner.tank.toolkitAnimation = true;
                  toolkitAnimation.loopRun('toolkit-reset2');
                  userData.toolkits -= 1;
                  user.joiner.tank.usingToolkit = true;
                  user.joiner.tank.CanToolkit = false;
                  var cooldown = 40000;
                  if (userData.kit == 'cooldown') {
                    cooldown *= .9;
                  }
                  setTimeout(function() {
                    user.joiner.tank.CanToolkit = true;
                  }, cooldown)
                }
              }
            }
            draw.setTransform(window.resizer, 0, 0, window.resizer, window.resizer * (-pt[l].x + 230), window.resizer * (-pt[l].y + 230));
          }
          l++;
        }
        draw.clearRect(0, 0, 500, 500);
        draw.fillStyle = '#000000';
        draw.fillRect(-1000, -1000, 3500, 3500);
        draw.drawImage(floor, 0, 0, 1500, 1500);
        var m = 0;
        var len = user.joiner.hostupdate.tanks.length;
        while (m < len) {
          if (user.joiner.hostupdate.tanks[m].username == user.username) {
            draw.setTransform(window.resizer, 0, 0, window.resizer, window.resizer * (-pt[m].x + 230), window.resizer * (-pt[m].y + 230));
          }
          m++;
        }
        var l = 0;
        var len = user.joiner.hostupdate.bullets.length
        while (l < len) {
          if (user.joiner.hostupdate.bullets[l].type == 'bullet') {
            draw.fillStyle = '#000000';
            draw.fillRect(user.joiner.hostupdate.bullets[l].x, user.joiner.hostupdate.bullets[l].y, 5, 5);
          } else if (user.joiner.hostupdate.bullets[l].type == 'powermissle') {
            draw.translate(user.joiner.hostupdate.bullets[l].x + 2.5, user.joiner.hostupdate.bullets[l].y + 2.5);
            draw.rotate(user.joiner.hostupdate.bullets[l].rot * Math.PI / 180);
            draw.drawImage(powermissle, -5, -10, 10, 20);
            draw.rotate(-user.joiner.hostupdate.bullets[l].rot * Math.PI / 180);
            draw.translate(-user.joiner.hostupdate.bullets[l].x - 2.5, -user.joiner.hostupdate.bullets[l].y - 2.5);
          } else if (user.joiner.hostupdate.bullets[l].type == 'megamissle') {
            draw.translate(user.joiner.hostupdate.bullets[l].x + 2.5, user.joiner.hostupdate.bullets[l].y + 2.5);
            draw.rotate(user.joiner.hostupdate.bullets[l].rot * Math.PI / 180);
            draw.drawImage(megamissle, -5, -10, 10, 20);
            draw.rotate(-user.joiner.hostupdate.bullets[l].rot * Math.PI / 180);
            draw.translate(-user.joiner.hostupdate.bullets[l].x - 2.5, -user.joiner.hostupdate.bullets[l].y - 2.5);
          } else if (user.joiner.hostupdate.bullets[l].type == 'shotgun_bullet') {
            draw.drawImage(shotgun_bullet, user.joiner.hostupdate.bullets[l].x, user.joiner.hostupdate.bullets[l].y);
          } else if (user.joiner.hostupdate.bullets[l].type == 'condensed_bullet') {
            draw.drawImage(condensed_bullet, user.joiner.hostupdate.bullets[l].x, user.joiner.hostupdate.bullets[l].y);
          } else if (user.joiner.hostupdate.bullets[l].type == 'grapple') {
            var particleAngle = user.joiner.hostupdate.bullets[l].rot + 180;
            if (particleAngle > 360) user.joiner.hostupdate.bullets[l].rot -= 360;
            draw.translate(user.joiner.hostupdate.bullets[l].x + 2.5, user.joiner.hostupdate.bullets[l].y + 2.5);
            draw.rotate(particleAngle * Math.PI / 180);
            draw.drawImage(grapple, -22.5, -22.5);
            draw.rotate(-particleAngle * Math.PI / 180);
            draw.translate(-user.joiner.hostupdate.bullets[l].x - 2.5, -user.joiner.hostupdate.bullets[l].y - 2.5);
            draw.strokeStyle = 'darkgray';
            draw.lineWidth = 5;
            draw.beginPath();
            draw.moveTo(user.joiner.hostupdate.bullets[l].x, user.joiner.hostupdate.bullets[l].y);
            draw.lineTo(user.joiner.hostupdate.bullets[l].startx, user.joiner.hostupdate.bullets[l].starty);
            draw.stroke();
          }
          var particleAngle = user.joiner.hostupdate.bullets[l].rot + 180;
          if (particleAngle > 360) user.joiner.hostupdate.bullets[l].rot -= 360;
          draw.translate(user.joiner.hostupdate.bullets[l].x + 2.5, user.joiner.hostupdate.bullets[l].y + 2.5);
          draw.rotate(particleAngle * Math.PI / 180);
          draw.drawImage(bullet_particle[user.joiner.hostupdate.bullets[l].particleFrame], -2.5, 2.5);
          draw.rotate(-particleAngle * Math.PI / 180);
          draw.translate(-user.joiner.hostupdate.bullets[l].x - 2.5, -user.joiner.hostupdate.bullets[l].y - 2.5);
          l++;
        }
        /*var l = 0;
        var len = user.joiner.hostupdate.scaffolding.length;
        while (l < len) {
          if (user.joiner.hostupdate.scaffolding[l].type == 'weak') {
            draw.drawImage(weakI[0], user.joiner.hostupdate.scaffolding[l].x * 50, user.joiner.hostupdate.scaffolding[l].y * 50);
          } else {
            draw.drawImage(strongI[0], user.joiner.hostupdate.scaffolding[l].x * 50, user.joiner.hostupdate.scaffolding[l].y * 50);
          }
          l++;
        }*/
        var l = 0;
        var len = user.joiner.hostupdate.blocks.length;
        while (l < len) {
          if (user.joiner.hostupdate.blocks[l].type == 'strong') {
            var num = 6 - Math.ceil(user.joiner.hostupdate.blocks[l].health / 20);
            if (num < 6) {
              draw.drawImage(strongI[num], user.joiner.hostupdate.blocks[l].x * 50, user.joiner.hostupdate.blocks[l].y * 50);
            }
          } else if (user.joiner.hostupdate.blocks[l].type == 'gold') {
            var num = 30 - Math.ceil(user.joiner.hostupdate.blocks[l].health / 20);
            if (num < 30) {
              draw.drawImage(goldI[num], user.joiner.hostupdate.blocks[l].x * 50, user.joiner.hostupdate.blocks[l].y * 50);
            }
          } else if (user.joiner.hostupdate.blocks[l].type == 'wall') {
            draw.drawImage(wall_image, user.joiner.hostupdate.blocks[l].x * 50, user.joiner.hostupdate.blocks[l].y * 50);
          } else if (user.joiner.hostupdate.blocks[l].type == 'weak') {
            var num = 4 - Math.ceil(user.joiner.hostupdate.blocks[l].health / 20);
            if (num < 4) {
              draw.drawImage(weakI[num], user.joiner.hostupdate.blocks[l].x * 50, user.joiner.hostupdate.blocks[l].y * 50);
            }
          }
          l++;
        }
        var l = 0;
        var len = pt.length;
        while (l < len) {
          if (!pt[l].ded || pt[l].username == user.username) {
            var m = 0;
            var len = user.joiner.hostupdate.tanks.length
            while (m < len) {
              if (user.joiner.hostupdate.tanks[m].username == user.username) {
                draw.setTransform(window.resizer, 0, 0, window.resizer, window.resizer * (-pt[m].x + 230), window.resizer * (-pt[m].y + 230));
              }
              m++;
            }
            if (pt[l].invis) {
              if (pt[l].username != user.username) {
                draw.globalAlpha = 0;
              } else {
                draw.globalAlpha = .5;
              }
            }
            if (pt[l].leftright == true) {
              draw.translate(pt[l].x + 20, pt[l].y + 20)
              draw.rotate(90 * Math.PI / 180);
              if (pt[l].base > 2) {
                if (pt[l].material == 'normal') {
                  draw.drawImage(tank_base_png, -20, -20);
                } else if (pt[l].material == 'iron') {
                  draw.drawImage(iron_tank_base, -20, -20);
                } else if (pt[l].material == 'diamond') {
                  draw.drawImage(diamond_tank_base, -20, -20);
                } else if (pt[l].material == 'dark') {
                  draw.drawImage(dark_tank_base, -20, -20);
                } else if (pt[l].material == 'light') {
                  draw.drawImage(light_tank_base, -20, -20);
                }
              } else {
                if (pt[l].material == 'normal') {
                  draw.drawImage(tank_base2, -20, -20);
                } else if (pt[l].material == 'iron') {
                  draw.drawImage(iron_tank_base2, -20, -20);
                } else if (pt[l].material == 'diamond') {
                  draw.drawImage(diamond_tank_base2, -20, -20);
                } else if (pt[l].material == 'dark') {
                  draw.drawImage(dark_tank_base2, -20, -20);
                } else if (pt[l].material == 'light') {
                  draw.drawImage(light_tank_base2, -20, -20);
                }
              }
              draw.rotate(-90 * Math.PI / 180);
              draw.translate((-pt[l].x - 20), (-pt[l].y - 20));
            } else {
              if (pt[l].base > 2) {
                if (pt[l].material == 'normal') {
                  draw.drawImage(tank_base_png, pt[l].x, pt[l].y);
                } else if (pt[l].material == 'iron') {
                  draw.drawImage(iron_tank_base, pt[l].x, pt[l].y);
                } else if (pt[l].material == 'diamond') {
                  draw.drawImage(diamond_tank_base, pt[l].x, pt[l].y);
                } else if (pt[l].material == 'dark') {
                  draw.drawImage(dark_tank_base, pt[l].x, pt[l].y);
                } else if (pt[l].material == 'light') {
                  draw.drawImage(light_tank_base, pt[l].x, pt[l].y);
                }
              } else {
                if (pt[l].material == 'normal') {
                  draw.drawImage(tank_base2, pt[l].x, pt[l].y);
                } else if (pt[l].material == 'iron') {
                  draw.drawImage(iron_tank_base2, pt[l].x, pt[l].y);
                } else if (pt[l].material == 'diamond') {
                  draw.drawImage(diamond_tank_base2, pt[l].x, pt[l].y);
                } else if (pt[l].material == 'dark') {
                  draw.drawImage(dark_tank_base2, pt[l].x, pt[l].y);
                } else if (pt[l].material == 'light') {
                  draw.drawImage(light_tank_base2, pt[l].x, pt[l].y);
                }
              }
            }
            draw.translate(pt[l].x + 20, pt[l].y + 20);
            draw.rotate(pt[l].rotation * Math.PI / 180);
            if (pt[l].material == 'normal') {
              draw.drawImage(tank_top_png, -20, -20 + pt[l].pushback);
            } else if (pt[l].material == 'iron') {
              draw.drawImage(iron_tank_top, -20, -20 + pt[l].pushback);
            } else if (pt[l].material == 'diamond') {
              draw.drawImage(diamond_tank_top, -20, -20 + pt[l].pushback);
            } else if (pt[l].material == 'dark') {
              draw.drawImage(dark_tank_top, -20, -20 + pt[l].pushback);
            } else if (pt[l].material == 'light') {
              draw.drawImage(light_tank_top, -20, -20 + pt[l].pushback);
            }
            if (pt[l].cosmetic != undefined) {
              var q = 0;
              while (q < cosmetics.length) {
                if (cosmetics[q].name == pt[l].cosmetic) {
                  draw.drawImage(cosmetics[q].image, -20, -20 + pt[l].pushback);
                }
                q++;
              }
            }
            draw.rotate(-(pt[l].rotation * Math.PI / 180));
            draw.translate(-pt[l].x - 20, -pt[l].y - 20);
            // Health Bar!
            draw.fillStyle = '#000000';
            draw.fillRect(pt[l].x, pt[l].y + 50, 40, 5);
            draw.fillStyle = '#90EE90';
            draw.fillRect(pt[l].x + 2, pt[l].y + 51, 36 * pt[l].health / pt[l].maxHealth, 3);
            //username
            draw.font = '20px starfont';
            if (pt[l].team == 'blue') {
              draw.fillStyle = 'blue';
            } else if (pt[l] == 'red') {
              draw.fillStyle = 'red';
            } else {
              draw.fillStyle = '#FFDF00';
            }
            draw.fillText('[LVL:' + pt[l].rank + '] ' + pt[l].username, pt[l].x + 20 - pt[l].username.length / 2 * 13, pt[l].y - 25);
            if (pt[l].shields != 0) {
              draw.strokeStyle = '#7DF9FF';
              draw.globalAlpha = .2;
              draw.lineWidth = 5;
              draw.beginPath();
              draw.arc(pt[l].x + 20, pt[l].y + 20, 33, 0, Math.PI * 2);
              draw.fill();
              draw.globalAlpha = 1;
            }
            if (pt[l].invis) {
              draw.globalAlpha = 1;
            }
          }
          l++;
        }
        draw.font = '15px starfont';
        draw.globalAlpha = .4;
        draw.setTransform(window.resizer, 0, 0, window.resizer, 0, 0);
        draw.drawImage(tank_ui['tank_main_singleplayer'].image, 0, 0);
        draw.fillStyle = '#ffffff';
        if (user.joiner.tank.CanBoost) draw.globalAlpha = .8;
        draw.fillRect(100, 450, 50, 50);
        draw.globalAlpha = .4;
        if (user.joiner.tank.CanToolkit) draw.globalAlpha = .8;
        draw.fillRect(183, 450, 50, 50);
        draw.globalAlpha = .4;
        if (user.joiner.tank.canPlaceScaffolding) draw.globalAlpha = .8;
        draw.fillRect(266, 450, 50, 50);
        draw.globalAlpha = .4;
        if (user.joiner.tank.canFireFlashbang) draw.globalAlpha = .8;
        draw.fillRect(349, 450, 50, 50);
        draw.globalAlpha = 1;
        draw.drawImage(boost, 100, 450);
        draw.drawImage(toolkit, 183, 450);
        draw.drawImage(scaffolding, 266, 450);
        draw.drawImage(flashbang, 349, 450);
        draw.fillStyle = '#ffffff';
        draw.fillText(userData.boosts, 135, 470);
        draw.fillText(userData.toolkits, 183 + 35, 470);
        draw.fillText(userData.blocks, 266 + 35, 470);
        draw.fillText(userData.flashbangs, 349 + 35, 470);
        draw.textAlign = 'center';
        draw.fillText(user.joiner.hostupdate.gameMessage, 250, 25);
        draw.textAlign = 'left';
        if (user.joiner.tank.toolkitAnimation) {
          draw.globalAlpha = .5;
          toolkitAnimation.drawFrame();
          draw.globalAlpha = 1;
        }
        if (user.joiner.lagometer) {
          var g = 0,
            total = 0;
          while (g < pings.client.length) {
            total += pings.client[g];
            g++;
          }
          var g = 0;
          while (g < pings.client.length) {
            if (pings.client[g] < 60) {
              draw.fillStyle = 'green';
            } else if (pings.client[g] >= 60 && pings.client[g] < 100) {
              draw.fillStyle = 'yellow';
            } else if (pings.client[g] >= 100 && pings.client[g] <= 300) {
              draw.fillStyle = 'orange';
            } else if (pings.client[g] > 300) {
              draw.fillStyle = 'red';
            }
            draw.fillRect(0 + g * 5, 450 - pings.client[g] / 10, 5, pings.client[g] / 10);
            g++;
          }
          var average = total / pings.client.length;
          draw.fillStyle = '#000000';
          draw.fillRect(0, 450 - average / 10, 300, 5);
          var g = 0,
            total = 0;
          while (g < pings.server.length) {
            total += pings.server[g];
            g++;
          }
          var g = 0;
          while (g < pings.server.length) {
            if (pings.server[g] < 60) {
              draw.fillStyle = 'green';
            } else if (pings.server[g] >= 60 && pings.server[g] < 100) {
              draw.fillStyle = 'yellow';
            } else if (pings.server[g] >= 100 && pings.server[g] <= 300) {
              draw.fillStyle = 'orange';
            } else if (pings.server[g] > 300) {
              draw.fillStyle = 'red';
            }
            draw.fillRect(250 + g * 5, 450 - pings.server[g] / 10, 5, pings.server[g] / 10);
            g++;
          }
          var average = total / pings.server.length;
          draw.fillStyle = '#000000';
          draw.fillRect(250, 450 - average / 10, 300, 5);
        }
        user.joiner.canUpdate = true;
        user.joiner.drawLoop = requestAnimationFrame(user.joiner.drawMap);
      }
      this.socket.onmessage = function(data) {
        data = JSON.parse(data.data);
        if (data.event == 'hostupdate') {
          pings.server.push(new Date(data.sendTime).getTime() - new Date(new Date().toISOString()).getTime());
          if (pings.server.length > 60) {
            pings.server.shift();
          }
          if (data.tanks) {
            user.joiner.hostupdate.tanks = data.tanks;
            pt = user.joiner.hostupdate.tanks;
          }
          if (data.scaffolding) {
            user.joiner.hostupdate.scaffolding = data.scaffolding;
          }
          if (data.blocks) {
            user.joiner.hostupdate.blocks = data.blocks;
          }
          if (data.bullets) {
            user.joiner.hostupdate.bullets = data.bullets;
          }
        } else if (data.event == 'gameover') {
          gameover(data.data);
        } else if (data.event == 'override') {
          user.joiner.tank[data.data.key] = data.data.value;
          user.joiner.tank[data.data.key2] = data.data.value2;
        } else if (data.event == 'kill') {
          userData.crates += Math.floor(Math.random() * (2) + 1);
          userData.xp += 10;
          saveGame();
        }
      };
      this.intervals = function() {
        user.joiner.send();
        user.joiner.outputLoop = requestAnimationFrame(user.joiner.send);
        user.joiner.drawLoop = requestAnimationFrame(this.drawMap);
      }.bind(this);
      this.socket.onopen = function() {
        user.joiner.send();
        setTimeout(user.joiner.intervals, 500);
        load.stop();
        user.joiner.socket.send(JSON.stringify({
          operation: 'multiplayer',
          room: user.joiner.channelname,
          username: user.username,
          type: 'joiner',
          mode: channelname,
          task: 'pixel-tanks',
          gamemode: user.joiner.gamemode,
        }));
        var thermalStatus = false;
        if (userData.kit == 'thermal') {
          thermalStatus = true;
        }
        var builderStatus = false;
        if (userData.class == 'builder') {
          builderStatus = true;
        }
        user.joiner.socket.send(JSON.stringify({
          operation: 'multiplayer',
          username: sessionStorage.username,
          event: 'joinerjoin',
          task: 'pixel-tanks',
          gamemode: user.joiner.gamemode,
          data: {
            pings: [],
            team: user.joiner.tank.team,
            firedRecent: false,
            isBuilder: builderStatus,
            canTakeThermalDamage: user.joiner.tank.canTakeThermalDamage,
            isThermal: thermalStatus,
            username: user.username,
            health: userData.health * (1 + .02 * userData.rank),
            maxHealth: userData.health * (1 + .02 * userData.rank),
            ded: false,
            pushback: 0,
            base: 0,
            shielded: false,
            shields: 0,
            material: user.joiner.tank.material,
            class: user.joiner.tank.class,
            kills: 0,
            t: [],
            i: [],
            rank: userData.rank,
          },
        }));
      }
      canvas.addEventListener('keydown', tank_M_listener1, false);
      canvas.addEventListener('keyup', tank_M_listener2, false);
      document.addEventListener('mousemove', tank_M_listener3, false);
      document.addEventListener('mousedown', tank_M_listener4, false);
      document.addEventListener('mouseup', tank_M_listener5);
    }
    keyHandler(e) {
      e = e || event;
      switch (e.keyCode) {
        case 76:
          user.joiner.lagometer = !user.joiner.lagometer;
          break;
        case 68:
          if (!checkerM(user.joiner.tank.x + user.joiner.tank.speed, user.joiner.tank.y, user.joiner.hostupdate.blocks) && (!user.joiner.tank.invis || !user.joiner.tank.immune)) return;
          user.joiner.tank.x += user.joiner.tank.speed;
          user.joiner.tank.leftright = true;
          if (user.joiner.tank.base >= 5) {
            user.joiner.tank.base = 0;
          } else {
            user.joiner.tank.base += 1;
          }
          break;
        case 87:
          if (!checkerM(user.joiner.tank.x, user.joiner.tank.y - user.joiner.tank.speed, user.joiner.hostupdate.blocks) && (!user.joiner.tank.invis || !user.joiner.tank.immune)) return;
          user.joiner.tank.y -= user.joiner.tank.speed;
          user.joiner.tank.leftright = false;
          if (user.joiner.tank.base >= 5) {
            user.joiner.tank.base = 0;
          } else {
            user.joiner.tank.base += 1;
          }
          break;
        case 65:
          if (!checkerM(user.joiner.tank.x - user.joiner.tank.speed, user.joiner.tank.y, user.joiner.hostupdate.blocks) && (!user.joiner.tank.invis || !user.joiner.tank.immune)) return;
          user.joiner.tank.x -= user.joiner.tank.speed;
          user.joiner.tank.leftright = true;
          if (user.joiner.tank.base >= 5) {
            user.joiner.tank.base = 0;
          } else {
            user.joiner.tank.base += 1;
          }
          break;
        case 83:
          if (!checkerM(user.joiner.tank.x, user.joiner.tank.y + user.joiner.tank.speed, user.joiner.hostupdate.blocks) && (!user.joiner.tank.invis || !user.joiner.tank.immune)) return;
          user.joiner.tank.y += user.joiner.tank.speed;
          user.joiner.tank.leftright = false;
          if (user.joiner.tank.base >= 5) {
            user.joiner.tank.base = 0;
          } else {
            user.joiner.tank.base += 1;
          }
          break;
        case 16:
          if (userData.boosts > 0) {
            if (user.joiner.tank.CanBoost == true) {
              userData.boosts -= 1;
              user.joiner.tank.speed = 8;
              user.joiner.tank.CanBoost = false;
              user.joiner.tank.immune = true;
              setTimeout(function() {
                user.joiner.tank.speed = 2;
                user.joiner.tank.immune = false;
              }, 500);
              var cooldown = 5000;
              if (userData.kit == 'cooldown') {
                cooldown *= .9;
              }
              setTimeout(function() {
                user.joiner.tank.CanBoost = true;
              }, cooldown);
            }
          }
          break;
        case 81:
          var l = 0,
            health;
          var len = pt.length
          while (l < len) {
            if (pt[l].username == user.username) {
              health = pt[l].health;
            }
            l++;
          }
          if (health < userData.health * (1 + .02 * userData.rank)) {
            if (user.joiner.tank.CanToolkit) {
              if (userData.toolkits > 0) {
                toolkitAnimation = new Animation(toolkitOpt);
                user.joiner.tank.toolkitAnimation = true;
                toolkitAnimation.loopRun('toolkit-reset2');
                userData.toolkits -= 1;
                user.joiner.tank.usingToolkit = true;
                user.joiner.tank.CanToolkit = false;
                var cooldown = 40000;
                if (userData.kit == 'cooldown') {
                  cooldown *= .9;
                }
                setTimeout(function() {
                  user.joiner.tank.CanToolkit = true;
                }, cooldown);
                //user.joiner.send();
              }
            }
          }
          break;
        case 32:
          if (user.joiner.tank.canPlaceScaffolding) {
            if (userData.blocks > 0) {
              user.joiner.tank.canPlaceScaffolding = false;
              userData.blocks -= 1;
              user.joiner.tank.placeScaffolding = true;
              if (userData.class == 'builder') {
                user.joiner.tank.scaffoldingType = 'strong';
              } else {
                user.joiner.tank.scaffoldingType = 'weak';
              }
              var cooldown = 5000;
              if (userData.kit == 'cooldown') {
                cooldown *= .9;
              }
              setTimeout(function() {
                user.joiner.tank.canPlaceScaffolding = true;
              }, cooldown);
              //user.joiner.send();
            }
          }
          break;
        case 69:
          if (userData.flashbangs > 0) {
            if (user.joiner.tank.canFireFlashbang) {
              userData.flashbangs -= 1;
              user.joiner.tank.canFireFlashbang = false;
              user.joiner.tank.flashbangFired = true;
              var cooldown = 20000;
              if (userData.kit == 'cooldown') {
                cooldown *= .9;
              }
              setTimeout(function() {
                user.joiner.tank.canFireFlashbang = true;
              }, cooldown);
              //user.joiner.send();
            }
          }
          break;
        case 70:
          if (user.joiner.tank.class === 'stealth') {
            if (user.joiner.tank.canChangeInvisStatus) {
              if (user.joiner.tank.invis) {
                user.joiner.tank.canChangeInvisStatus = false;
                window.setTimeout(function() {
                  user.joiner.tank.canChangeInvisStatus = true;
                }, 500);
                user.joiner.tank.invis = false;
                var cooldown = 500;
                if (userData.kit == 'cooldown') {
                  cooldown *= .9;
                }
                window.setTimeout(function() {
                  user.joiner.tank.canInvis = true;
                }, cooldown);
              } else {
                if (user.joiner.tank.canInvis) {
                  user.joiner.tank.canChangeInvisStatus = false;
                  window.setTimeout(function() {
                    user.joiner.tank.canChangeInvisStatus = true;
                  }, 500);
                  user.joiner.tank.invis = true;
                  window.setTimeout(function() {
                    user.joiner.tank.canChangeInvisStatus = false;
                    user.joiner.tank.invis = false;
                    user.joiner.tank.canInvis = false;
                    window.setTimeout(function() {
                      user.joiner.tank.canChangeInvisStatus = true;
                      user.joiner.tank.canInvis = true;
                    }, 60000);
                  }, 20000);
                  user.joiner.tank.canInvis = false;
                }
              }
            }
          } else if (user.joiner.tank.class === 'normal') {
            if (user.joiner.tank.canShield) {
              user.joiner.tank.shielded = true;
              user.joiner.tank.canShield = false;
              var cooldown = 40000;
              if (userData.kit == 'cooldown') {
                cooldown *= .9;
              }
              setTimeout(function() {
                user.joiner.tank.canShield = true;
              }, cooldown);
            }
          } else if (user.joiner.tank.class == 'tactical') {
            if (user.joiner.tank.megaAttack) {
              tank_M_support('megaAttack');
              user.joiner.tank.megaAttack = false;
              var cooldown = 20000;
              if (userData.kit == 'cooldown') {
                cooldown *= .9
              }
              setTimeout(function() {
                user.joiner.tank.megaAttack = true;
              }, cooldown);
            }
          } else if (user.joiner.tank.class == 'builder') {
            if (user.joiner.tank.canBlockShield) {
              user.joiner.tank.blockShield = true;
              user.joiner.tank.canBlockShield = false;
              var cooldown = 30000;
              if (userData.kit == 'cooldown') {
                cooldown *= .9;
              }
              setTimeout(function() {
                user.joiner.tank.canBlockShield = true;
              }, cooldown);
            }
          }
          break;
        case 49:
          user.joiner.tank.fireType = 1;
          clearInterval(tankSupport);
          break;
        case 50:
          //if (userData.fire2) {
          user.joiner.tank.fireType = 2;
          clearInterval(tankSupport);
          //}
          break;
        case 51:
          //if (userData.fire3) {
          user.joiner.tank.fireType = 3;
          clearInterval(tankSupport);
          //}
          break;
        case 82:
          if (user.joiner.tank.canGrapple) {
            tank_M_support('grapple');
            user.joiner.tank.canGrapple = false;
            setTimeout(function() {
              this.canGrapple = true;
            }.bind(user.joiner.tank), 5000);
          }
          break;
      }
      e.preventDefault();
      e.stopImmediatePropagation();
    }
    send() {
      user.joiner.socket.send(JSON.stringify({
        operation: 'multiplayer',
        event: 'joinerupdate',
        task: 'pixel-tanks',
        gamemode: user.joiner.gamemode,
        sendTime: new Date().toISOString(),
        data: {
          rank: user.joiner.tank.rank,
          username: user.username,
          leftright: user.joiner.tank.leftright,
          x: user.joiner.tank.x,
          y: user.joiner.tank.y,
          rotation: user.joiner.tank.rotation,
          health: user.joiner.tank.health,
          fire: user.joiner.tank.fire,
          xd: user.joiner.tank.xd,
          yd: user.joiner.tank.yd,
          rotations: user.joiner.tank.rotations,
          usingToolkit: user.joiner.tank.usingToolkit,
          placeScaffolding: user.joiner.tank.placeScaffolding,
          scaffoldingType: user.joiner.tank.scaffoldingType,
          base: user.joiner.tank.base,
          shielded: user.joiner.tank.shielded,
          flashbangFired: user.joiner.tank.flashbangFired,
          invis: user.joiner.tank.invis,
          canInvis: user.joiner.tank.canInvis,
          fireType: user.joiner.tank.fireType,
          canChangeInvisStatus: user.joiner.tank.canChangeInvisStatus,
          immune: user.joiner.tank.immune,
          type: user.joiner.tank.type,
          blockShield: user.joiner.tank.blockShield,
          cosmetic: userData.cosmetic,
        }
      }));
      user.joiner.tank.scaffoldingType = null;
      user.joiner.tank.blockShield = false;
      user.joiner.tank.type = null;
      user.joiner.tank.fire = false;
      user.joiner.tank.usingToolkit = false;
      user.joiner.tank.placeScaffolding = false;
      user.joiner.tank.shielded = false;
      user.joiner.tank.flashbangFired = false;
      setTimeout(function() {
        requestAnimationFrame(user.joiner.send);
      })
    }
  }

  function tank_M_listener1(event) {
    event.preventDefault();
    if (event.keyCode == 87 || event.keyCode == 83 || event.keyCode == 68 || event.keyCode == 65) {}
    if (user.joiner.tank.helper[event.keyCode] != true) {
      var cooldown = 15;
      if (user.joiner.tank.invis) {
        cooldown = 12;
      }
      user.joiner.tank.intervals[event.keyCode] = window.setInterval(function() {
        user.joiner.keyHandler(event, user.joiner.tank.speed);
      }, cooldown);
    }
    user.joiner.tank.helper[event.keyCode] = true;
  }

  function tank_M_listener2(event) {
    event.preventDefault()
    if (event.keyCode == 87 || event.keyCode == 83 || event.keyCode == 68 || event.keyCode == 65) {
      if ((user.joiner.tank.helper[87] != true && user.joiner.tank.helper[83] != true) && (user.joiner.tank.helper[68] != true && user.joiner.tank.helper[65] != true)) {}
    }
    window.clearInterval(user.joiner.tank.intervals[event.keyCode]);
    user.joiner.tank.helper[event.keyCode] = false;
    user.joiner.tank.intervals[event.keyCode] = false;
  }

  function tank_M_listener3(e) {
    if (e.pageX || e.pageY) {
      x = e.pageX;
      y = e.pageY;
    } else {
      x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    x /= window.resizer;
    y /= window.resizer;
    var targetX = x - 250,
      targetY = y - 250;
    var rotation = 360 - Math.atan2(targetX, targetY) * (180 / Math.PI);
    rotation -= 360;
    if (rotation < 0) {
      rotation = 360 + rotation;
    }
    user.joiner.tank.rotation = Math.round(rotation);
    //user.joiner.send();
  }

  function tank_M_listener4(e) {
    user.joiner.tank.fire = false;
    clearInterval(tankSupport);
    var cooldown = 0;
    if (user.joiner.tank.fireType == 1) {
      cooldown = 200;
    }
    if (user.joiner.tank.fireType == 2) {
      cooldown = 600;
    }
    if (user.joiner.tank.fireType == 3) {
      cooldown = 400;
    }
    if (user.joiner.tank.canFire) {
      tank_M_support(e.button);
      user.joiner.tank.canFire = false;
      setTimeout(function() {
        user.joiner.tank.canFire = true;
      }, cooldown);
    }
    tankSupport = window.setInterval(tank_M_support, cooldown, e.button);
  }

  function tank_M_support(button) {
    user.joiner.tank.fire = true;
    if (button == 'grapple') {
      user.joiner.tank.type = 'grapple';
    } else if (user.joiner.tank.fireType == 1) {
      if (button != 'megaAttack') {
        if (button == 0) {
          user.joiner.tank.type = 'bullet';
        } else {
          if (user.joiner.tank.altAttack) {
            user.joiner.tank.type = 'powermissle';
            user.joiner.tank.altAttack = false;
            var cooldown = 10000;
            if (userData.kit == 'cooldown') {
              cooldown *= .9;
            }
            setTimeout(function() {
              user.joiner.tank.altAttack = true;
            }, cooldown);
          } else {
            user.joiner.tank.fire = false;
            return;
          }
        }
      } else {
        user.joiner.tank.type = 'megamissle';
      }
    } else if (user.joiner.tank.fireType == 2) {
      user.joiner.tank.type = 'shotgun_bullet';
    } else if (user.joiner.tank.fireType == 3) {
      user.joiner.tank.type = 'condensed_bullet';
    }
    user.joiner.tank.xd = [];
    user.joiner.tank.yd = [];
    user.joiner.tank.rotations = [];
    if (user.joiner.tank.fireType == 1) {
      var data = tank_M_calc(user.joiner.tank.rotation);
      user.joiner.tank.xd.push(data.xd);
      user.joiner.tank.yd.push(data.yd);
      user.joiner.tank.rotations.push(data.rotation);
    } else if (user.joiner.tank.fireType == 2) {
      var data = tank_M_calc(user.joiner.tank.rotation);
      user.joiner.tank.xd.push(data.xd);
      user.joiner.tank.yd.push(data.yd);
      user.joiner.tank.rotations.push(data.rotation);
      var data = tank_M_calc(user.joiner.tank.rotation - 5);
      user.joiner.tank.xd.push(data.xd);
      user.joiner.tank.yd.push(data.yd);
      user.joiner.tank.rotations.push(data.rotation);
      var data = tank_M_calc(user.joiner.tank.rotation + 5);
      user.joiner.tank.xd.push(data.xd);
      user.joiner.tank.yd.push(data.yd);
      user.joiner.tank.rotations.push(data.rotation);
      var data = tank_M_calc(user.joiner.tank.rotation + 10);
      user.joiner.tank.xd.push(data.xd);
      user.joiner.tank.yd.push(data.yd);
      user.joiner.tank.rotations.push(data.rotation);
      var data = tank_M_calc(user.joiner.tank.rotation - 10);
      user.joiner.tank.xd.push(data.xd);
      user.joiner.tank.yd.push(data.yd);
      user.joiner.tank.rotations.push(data.rotation);
    } else if (user.joiner.tank.fireType == 3) {
      var data = tank_M_calc(user.joiner.tank.rotation);
      user.joiner.tank.xd.push(data.xd);
      user.joiner.tank.yd.push(data.yd);
      user.joiner.tank.rotations.push(data.rotation);
    }
  }

  function tank_M_calc(rotation) {
    if (rotation < 0) {
      rotation = 360 + rotation;
    }
    if (rotation >= 360) {
      rotation = rotation - 360;
    }
    var xd = 1;
    var angle = rotation % 90;
    yd = Math.abs(((xd * angle) - (90 * xd)) / angle);
    if (rotation < 90 || rotation > 270) {
      yd = Math.abs(yd);
    } else {
      yd = 0 - Math.abs(yd);
    }
    if (this.rotation > 180) {
      xd = Math.abs(xd);
    } else {
      xd = 0 - Math.abs(xd);
    }
    if (rotation == 0) {
      xd = 0;
      yd = 1;
    } else if (rotation == 90) {
      xd = -1;
      user.joiner.tank.yd = 0;
    } else if (rotation == 180) {
      xd = 0;
      yd = -1;
    } else if (rotation == 270) {
      xd = 1;
      yd = 0;
    }
    if ((xd < 0 && yd < 0) || (xd > 0 && yd > 0)) {
      let place = xd;
      xd = yd;
      yd = place;
    }
    return {
      xd: xd,
      yd: yd,
      rotation: Math.round(rotation)
    };
  }

  function tank_M_listener5() {
    user.joiner.tank.fire = false;
    clearInterval(tankSupport);
  }
  const toolkitOpt = {
    totalFrames: 5,
    frameSpeed: 300,
    x: 230,
    y: 230,
    frameImages: ['data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAABBUlEQVRoQ+2Z3QqDMAyF9UbY+z/swJsNx4QitTkJa9eWz+skzflJqrgukzzrJDgWgPSmZJeKbM/tdSVqf+zFXgFS01ooUpPdSG0UUVjLsXTmWRvlrn6pptLTEXOeLW8tgBSoRZGEHKyVc0p02D1LwDrjr8MOkAwDcynyizVoXWCWx9P88CsKQCwZMvvekeIK/cxIb4q4EHyDAeJlzTPs3tpHPIp4WWuiiNJUje8R5Vw1psubXW0+jQNIykZt/ysKoQiKKD4JxGCtYa0VUFtOuV62kS0oW0vuKhAIkIQ0FAk46DZlSGtFP6utBdDcWgDp7ff0NIrkJn7IYQeIse+bby0UMRR5A5Bt5DPRqdnDAAAAAElFTkSuQmCC', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAABBklEQVRoQ+1ZXQuDMAzUF2H//8cOfNnYmBCkNpfDdG05n5O095FEcV0medZJcCwCkqnk9txe5/r7Y6+S3qUiApJpE6a2FGFYy8xJVaRU/ADjTZQr0LWaKFHH2fDUEpAKtVLEkCNrlZzCNntkCHhn/LXZBaTAwFyK3DEGvQXmedzm05tdQDwZCvM+kBIK/fZIb4qEEPyCBSTKWqTZo7U/8VIkyloTRZBLZXyPIOeiMV1udvTyNk5ALBvZ/kcUkiJSBPEJESNrDWstQm045bxsmSkIWwu+FREoIIY0KUI46DJlSGuxn9XeAGhuLQHp7ff0NIqUOn7IZhcQZ943n1pSZARF7nhLeAOe6uQzdBwSTgAAAABJRU5ErkJggg==', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAABCElEQVRoQ+1ZywrEIAxsL4X9/49d6GVLly2EkppJUDfK9Jxo5pGodF0m+dZJcCwpgWzv7XMneH/txVoJpKUlqUhLdiNrUxGENY2lK8+aKE/rl9ZEajpjrr3hqUUgBWqpiCCH1tKcEm12zxCw9vhrsxOIwsBcitQYg9YBZnlc5oevKARiyaDMe0eKK/TbI9kUcSH4BROIlzVPs3vXPuOpiJe1LoogRbV4jyD7ojEpT3a0eBlHIJKN1v5HFKIiVATxSSCG1hrWWgG14ZT7YRuZgrC14KoCgQQiSKMiAQc9pgxpreiz2hoA3a1FINl+T0+jiNbxQzY7gRjzvvvUoiIjKFLjlpDCWjWAHK1n5DOivZc3AAAAAElFTkSuQmCC', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAABBklEQVRoQ+1ZywqEMAzUi+D/f+yCF8VlhSCxmQTbTct4TtrMI2mL8zTIN2fEsXyW/V7Xtm7FWgmkppJUpCa7kbWpCMKaxtKVZ02Up/VLayI1nTHX3vDUIpACtVREkENraU6JNrtnCFh7/LXZCURhYCxF3hiD1gFmeVzmh68oBGLJoMx7R4or9Nsj2RRxIfgFE4iXNU+ze9c+46mIl7UmiiBF1XiPIPuiMSlPdrR4GUcgko3a/kcUoiJUBPFJIIbW6tZaAbXhlPthG5mCsLXgqgKBBCJIoyIBBz2mdGmt6LPaGgDNrUUg2X5PD6OI1vFdNjuBGPO++dSiIj0o8sYtIYW1CEQwcAC75OQzivgfuQAAAABJRU5ErkJggg==', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAA80lEQVRoQ+2Z2wqDMBBE64vQ///Ygi8tFJRU0uzuEM2ix9dczMyZXRWnR8Jrfs3v/bGW5zK1jtocHKURIaOc/3dfiNyKSA33aoDVUSIRipq63tvdtRDSsLhljpcMRGpO3b5GIk3AMmtosSOk4sC1iPRog1a7tDJerpfftRBiYSjGI0QC225TvzWSjQhCIBLIADXiNCvlA9F59p9pCCntODr/HkIQgYgnJ8IcokW0hNh4lrij5dlMnbN/11PaOUJU92vrIFK4QrTUaKkfcVYDOJ0IQrL9nr4MEdqv0V1OL3aIQER94g1al6JGemhHSA8Xe+7xAcph5DN9w3kfAAAAAElFTkSuQmCC', ]
  }
  const loadOpt = {
    totalFrames: 5,
    frameSpeed: 500,
    x: 0,
    y: 0,
    frameImages: ['data:text/html;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAgAElEQVR4Xu3dXXIkx5VEYfRStBU+c8zIxWgVWkzTTHrmVrQUjDWpJsFmVVfezIwMv5HfvI0YPx7Ho3BQWQD60/v7+/ub/0MAAQQQQACB1gQ+EXrr/oRHAAEEEEDgNwKE7iIggAACCCCwAAFCX6BER0AAAQQQQIDQ3QEEEEAAAQQWIEDoC5ToCAgggAACCBC6O4AAAggggMACBAh9gRIdAQEEEEAAAUJ3BxBAAAEEEFiAAKEvUKIjIIAAAgggQOjuAAIIIIAAAgsQIPQFSnQEBBBAAAEECN0dQAABBBBAYAEChL5AiY6AAAIIIIAAobsDCCCAAAIILECA0Bco0REQQAABBBAgdHcAAQQQQACBBQgQ+gIlOgICCCCAAAKE7g4ggAACCCCwAAFCX6BER0AAAQQQQIDQ3QEEEEAAAQQWIEDoC5ToCAgggAACCBC6O4AAAggggMACBAh9gRIdAQEEEEAAAUJ3BxBAAAEEEFiAAKEvUKIjIIAAAgggQOjuAAIIIIAAAgsQIPQFSnQEBBBAAAEECN0dQAABBBBAYAEChL5AiY6AAAIIIIAAobsDCCCAAAIILECA0Bco0REQQAABBBAgdHcAAQQQQACBBQgQ+gIlOgICCCCAAAKE7g4ggAACCCCwAAFCX6BER0AAAQQQQIDQ3QEEEEAAAQQWIEDoC5ToCAgggAACCBC6O4AAAggggMACBAh9gRIdAQEEEEAAAUJ3BxBAAAEEEFiAAKEvUKIjIIAAAgggQOjuAAIIIIAAAgsQIPQFSnQEBBBAAAEECN0dQAABBBBAYAECywv9/375eYGaHAEBBBBAYAaBf//0eca2u/Yk9F3YTEIAAQQQuBOBDmIn9DvdSGdFAAEEENhFgNB3YTt3kkfu5/K0GgIIIHBHAoQe0DqhB5QgAgIIINCcAKEHFEjoASWIgAACCDQnQOgBBRJ6QAkiIIAAAs0JEHpAgYQeUIIICCCAQHMChB5QIKEHlCACAggg0JwAoQcUSOgBJYiAAAIINCdA6AEFEnpACSIggAACzQkQekCBhB5QgggIIIBAcwKEHlAgoQeUIAICCCDQnAChBxRI6AEliIAAAgg0J0DoAQUSekAJIiCAAALNCRB6QIGEHlCCCAgggEBzAoQeUCChB5QgAgIIINCcAKEHFEjoASWIgAACCDQnQOgBBRJ6QAkiIIAAAs0JEHpAgYQeUIIICCCAQHMChB5QIKEHlCACAggg0JwAoQcUSOgBJYiAAAIINCdA6AEFEnpACSIggAACzQkQekCBhB5QgggIIIBAcwKEHlAgoQeUIAICCCDQnAChBxRI6AEliIAAAgg0J0DoAQUSekAJIiCAAALNCRB6QIGEHlCCCAh8Q+A/P/+ymcmPn3/aPNZABEYRIPRRZAvrEnoBlqEIXESA0C8CbZvTCBD6aSj3L0To+9mZicAoAoQ+iqx1RxEg9FFkC+sSegGWoQhcRIDQLwJtm9MIEPppKPcvROj72ZmJwFECW8T9j3/842/b/Pe///3jf/MZ+tEWzD+DAKGfQfHgGoR+EKDpCBwgQOgH4JkaRYDQA+og9IASRLgtAUK/bfXLHZzQAyol9IASRHjbIrZRmK56ZP3ojO9PDvXpw//+6pH7xyWuOsuoLqzblwChB3RH6AEliEDo39wBQvei6EaA0AMaI/SAEkQgdEL3KmhOgNADCiT0gBJuFqHy6Hkkmo/vgh/t8+zx9aP8H8c++/jg2eP1R3s/y/bq8btH7iNvjLW/R4DQA+4HoQeUcLMIhP66cEJ/zciILAKEHtAHoQeUcLMIhP66cEJ/zciILAKEHtAHoQeUcIMIHyVeefQ8E82rR/LPsp19vkoOj9xn3ph7703oAf0TekAJN4hA6PtLJvT97My8jgChX8f66U6EHlDCDSIQ+v6SCX0/OzOvI0Do17Em9ADWd4vQUeIfO/oq0rMfoW+9B89E7rH6VoLGXUmA0K+k/WQv79ADSlg0AqEfK5bQj/Ez+1oChH4t74e7EXpACYtGIPRjxRL6MX5mX0uA0K/lTegBvFeP0F3isx65P5K3R+urv1rWOh+hB/TpHXpACQtFIPR9ZRL6Pm5m5RAg9IAuCD2ghIUiEPq+Mgl9HzezcggQekAXhB5QQvMIK0l89CN3n4s3v+ziPyVA6AGXg9ADSmgegdC3F0jo21kZ2YsAoQf0RegBJTSPQOjbCyT07ayM7EWA0AP6IvSAEppHIPTHBfpcvPnFFr9EgNBLuMYMJvQxXO+0KqET+p3uu7M+JkDoATeD0ANKaB5hdaF/qWfPn3/1Dr35xRa/RIDQS7jGDCb0MVzvtOqqQv/S4UcpV6X+da4/EHOnV8N9z0roAd0TekAJzSOsLPQjUv/4zQCpN7/k4r8kQOgvEY0fQOjjGa++w+pCJ/XVb7DznUGA0M+geHANQj8I0PS3Owid1F10BL5PgNADbgihB5QwMMJH2Q7c5o+lq58zX5HpzD32fqbu8fuZLVgrkQChB7RC6AElDIxA6OfDffbHYSo7+Uy9QsvYDgQIPaAlQg8oYWAEQh8Dl9THcLVqXwKEHtAdoQeUcCDCK2Ff8Qh872PoA8duPdXj99b1Cf+EAKEHXA1CDyjhQARCPwBv4lS/oz4Rvq2HECD0IVhrixJ6jVfaaEJPa2RbHkLfxsmoPgQIPaArQg8oYUOEV+L+ssQVj9cfRfXIfUOB3wwh9DozM7IJEHpAP4QeUMKGCIS+AVKjIYTeqCxRNxEg9E2Yxg4i9LF8z1qd0M8imbEOoWf0IMV5BAj9PJa7VyL03eiGT3wm8VmP1p8d2CP3+lUg9DozM7IJEHpAP4QeUMKTCISe283RZIR+lKD5aQQIPaARQg8ogdBzSxiUjNAHgbXsNAKEPg39nxsTekAJb29/+QdOviZKe7Tukft5d4XQz2NppQwChB7QA6EHlEDoGSVcmKLyp2P93fcLi7HVbgKEvhvdeRMJ/TyWR1Z69Hm5d+hHiGbPrQj940nIPbvXO6cj9ID2CT2gBO/QM0q4MMVeoX+JSOoXFmWrzQQIfTOqcQMJfRzbVyt3flf+8Wxb5NTlacOrzo789z2/3veMLakfacLcEQQIfQTV4pqEXgR24nBCPxFmg6X2CP3Lsb73DROxNyj+JhEJPaBoQp9XAqHPYz9j571C/5qV2Ge0Zs+tBAh9K6mB4wh9INwHS3+U+J0eQz+T0eoMjkr80e30GP7a16zdthEg9G2cho4i9KF4/7Y4of8VCaHvv3+PxO4R/H6eZh4jQOjH+J0ym9BPwbh5EUIn9M2XZeNAYt8IyrChBAh9KN5tixP6Nk5HRt1V4luYbfkJ+a/rdHk3P+Ix+yuWpP6KkP8+mgChjya8YX1C3wDp4BBCfw6Q0A9erm+mf+Tp8fu5bK32fQKEHnBDCH18CYRO6ONv2e87EPpVpO3zLQFCD7gThD6+BEI/h3Hyu/kZj9kfUSX0c+6aVeoECL3O7PQZhH460r8tSOjnMCb01xwJ/TUjI8YQIPQxXEurEnoJ167BhL4L298mEfprjoT+mpERYwgQ+hiupVUJvYRr12BC34Xt0KSK/A9t9L/JKT+BT+hntGmNPQQIfQ+1k+cQ+slAHyxH6OMZf7sDoftX2a6/dffekdAD+if08SUQ+njGhP47Ae/Qr79rdvydAKEH3ARCH1/Co3+E5cuuKY9pxxOww1UECP0q0vb5lgChB9wJQh9fAqGPZ2wH79DdgbkECH0u/992J/TxJRD6eMZ2IHR3YC4BQp/Ln9An8Pd5+gToN9rSI/cblR12VEIPKMQ79GtLIPRred9tN0K/W+M55yX0gC4I/doSCP1a3nfbjdDv1njOeQk9oAtCv7YEQr+W9912I/S7NZ5zXkIP6ILQry3h2Q/IPUrh19qu7WaF3bYIvXIHr2Din3m9gvL4PQh9POOXOxD6S0SnDqh8MSX0U9HfYjFCv0XNkYck9IBaCP3aEgj9Wt53243Q79Z4znkJPaALQj9eQkXSx3fLWcEThJwuviZ5JvSPd/SHH36YHvzXX399mMHj9+nV7A5A6LvRnTeR0I+zJPTjDK1wDgFCP4ejVeoECL3O7PQZhH4cKaEfZ2iFcwgQ+jkcrVInQOh1ZqfPIPR9SJ9J/J///Oe+BR/M+te//rVrraseqT57bPo1tEfyu+r726Sr/ynYZ6mvuFcf75TH7+fcn6tWIfSrSH9nH0LfVwKhv70R+r67U51F6FVixs8gQOgzqH+zJ6HvK4HQCX3fzanPIvQ6MzOuJ0Do1zP/246Evr2EjxI/89H69gTbRu59VL9l9VePXV+9a/92jxUfy48U8Mx7t/devbozH++ER+5bXoWZYwg9oBdC314Cob+9vfriTOhvb4T+19fUqztD6Nu/BiWPJPSAdgh9ewmETuhbbguhE/qWe7LaGEIPaJTQt5fQRejbT1QfecZj1+q7+K0pr358/0zcMx+Lb2U1etyje7LlnbpH7qObGbc+oY9ju3llQt+M6o3Q394I/c/7QujPXzuEvv3ryiojCT2gSULfXgKhE/rH20LohL79q8f6Iwk9oGNC/34JV/x6WsA1GBJhy7v5LY9hvxdu1OP7LUA8Wt9C6fE3gc9698h9G9PEUYQe0AqhE/qoa0joo8j2Wrfy+J3Qe3X7MS2hB3RH6IQ+6hoS+iiyvdYl9F597U1L6HvJnTiP0LcL3SPWEy/e/5Z6Jf3KI/mRj991f7z7j1175H6cZ9oKhB7QCKET+sxrSOgz6V+7N6Ffy/vq3Qj9auIP9iN0Qp95DQl9Jv1r9yb0a3lfvRuhX02c0MvE/apaGVlpAqGXcLUeTOit63sZntBfIho/wDt079DH37L9O7wS/seVK5+3b0k08jP5Lft/b0zHz/QJ/Wjr2fMJPaAfQif0gGv4NAKhP0ZD6Mm39p7ZCD2gd0In9IBrSOjFEgi9CMzw4QQIfTji1xsQOqG/viW5Iyrv4Lec4uzH9lv2rIz5+jFAJ6F/7WgLW39YpnIbssYSekAfhE7oAddwdwRC343usomEfhnqqRsR+lT8v29O6IQecA13RyD03egum0jol6GeuhGhT8VP6Fvw+7W1LZR6jznjm4Itj5PPoOSR+xkUrTGCAKGPoFpc0zt079CLV2a54YQ+tlLv0MfyTVmd0AOaIHRCD7iGUyMQ+lj8hD6Wb8rqhB7QBKETesA1bB/h2TcFZz+KT37kvpfBsz/g8+Pnn9rfizsdgNAD2iZ0Qg+4hu0j7JVZ9eCEXiVm/FUECP0q0t/Zh9AJPeAato9A6G9vexl4h97++v92AEIP6JHQt5fgJ963s7rzyC1/s3wvnyveoZ/xMwV7z+cx+15y8+cR+vwO/B56oQNCL8C68VBC318+oe9nN3smoc9uwB+WKTVA6CVctx1M6PurJ/T97GbPJPTZDRB6qQFCL+G67eBEoVceo5Pqba/uoYMT+iF850z2Gfp2joS+ndWdRxL6ndu/79kJPaB7Qt9eAqFvZ3XnkYR+5/bve3ZCD+ie0LeX8FHoH2d1+qcst5/WyL0ErhB6NZvH6FVixlcJEHqV2IDxhL4dKqFvZ3XnkYR+5/bve3ZCD+ie0LeXQOjbWd15JKHfuf37np3QA7on9O0lEPp2VnceSeh3bv++Zyf0gO4J/XgJj0Tvc/XjXLuucIXQfSbe9Xasm5vQA7ol9OMlEPpxhiutQOgrteksWwkQ+lZSA8cR+nG4hH6c4UorEPpKbTrLVgKEvpXUwHGEfi7cZ5+zP9rFY/lz2aesRugpTchxJQFCv5L2k70I/dwSCP1cnh1XI/SOrcl8lAChHyV4wnxCPwHihyUI/VyeHVcj9I6tyXyUAKEfJXjCfEI/AeLOJV7J3yP5nWAnTyP0yQXYfgoBQp+C/a+bEvq8Egh9HvuROxP6SLrWTiVA6AHNEPq8Egh9HvuROxP6SLrWTiVA6AHNEHpACU8ivBL+t9M8os/o8myh//rrr387mD8sk9G1FH8SIPSA20DoASUQem4JO5IR+g5oprQnQOgBFRJ6QAmEnlvCjmSEvgOaKe0JEHpAhYQeUMKBCP5K3QF4g6aeIfRHj9m/xPWofVBplj1MgNAPIzy+AKEfZzhzBUKfSf/x3oSe14lE4wkQ+njGL3cg9JeIogcQel49hJ7XiUTjCRD6eMYvdyD0l4jaDaj+dPyrA/rp+VeE3t6qEn/2SP3RTh6zv+ZvxHwChD6/gzdCDyjh5AiEfjLQDcsR+gZIhixNgNAD6iX0gBJOjkDoJwPdsByhb4BkyNIECD2gXkIPKCE8whnfIKz42P6jxD9W+MMPP/zx//pp9fDLLd5pBAj9NJT7FyL0/ezuMpPQHzdN6Hd5BTjnFgKEvoXS4DGEPhjwAssTOqEvcI0dYTABQh8MeMvyhL6F0r3HEDqh3/sV4PRbCBD6FkqDxxD6YMCW/43AGd8UdETpV846tibzHgKEvofayXMI/WSglntIgNBdDATWJkDoAf0SekAJN4hA6Dco2RFvTYDQA+on9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKLAq9P/8/EtAahEQQAABBEYS+PHzT6XlCb2Ea8xgQh/D1aoIIIBAZwKE3rA9Qm9YmsgIIIDAYAKEPhjwiOUJfQRVayKAAAK9CRB6w/6OCv39/b3hqedH/vTp08MQeF7bzbc94H8t/6276Wkrqf3jvmVM6PtZTptJ6HPQE/oc7t/uShQZPbxKoadXhI7/d0I/znD6CoQ+pwJCn8Od0DO4V1MQepVYfTyh15nFzSD0OZUQ+hzuhJ7BvZqC0KvE6uMJvc4sbgahz6mE0OdwJ/QM7tUUhF4lVh9P6HVmcTMIfU4lhD6HO6FncK+mIPQqsfp4Qq8zi5tB6HMqIfQ53Ak9g3s1BaFXidXHE3qdWdwMQp9TCaHP4U7oGdyrKQi9Sqw+ntDrzOJmEPqcSgh9DndCz+BeTUHoVWL18YReZxY3g9DnVELoc7gTegb3agpCrxKrjyf0OrO4GYQ+pxJCn8Od0DO4V1MQepVYfTyh15nFzSD0OZUQ+hzuhJ7BvZqC0KvE6uMJvc4sbgahz6mE0OdwJ/QM7tUUhF4lVh9P6HVmcTMIfU4lhD6HO6FncK+mIPQqsfp4Qq8zi5tB6HMqIfQ53Ak9g3s1BaFXidXHE3qdWdwMQp9TCaHP4U7oGdyrKQi9Sqw+ntDrzOJmEPqcSgh9DndCz+BeTUHoVWL18YReZxY3g9DnVELoc7gTegb3agpCrxKrjyf0OrO4GYQ+pxJCn8Od0DO4V1MQepVYfTyh15nFzSD0OZUQ+hzuhJ7BvZqC0KvE6uMJvc4sbgahz6mE0OdwJ/QM7tUUhF4lVh9P6HVmcTMIfU4lhD6HO6FncK+mIPQqsfp4Qq8zi5tB6HMqIfQ53Ak9g3s1BaFXidXHE3qdWdwMQp9TCaHP4U7oGdyrKQi9Sqw+ntDrzOJmEPqcSgh9DndCz+BeTUHoVWL18YReZxY3g9DnVELoc7gTegb3agpCrxKrjyf0OrO4GYQ+pxJCn8Od0DO4V1MQepVYfTyh15nFzSD0OZUQ+hzuhJ7BvZqC0KvE6uMJvc4sbgahz6mE0OdwJ/QM7tUUhF4lVh9P6HVmcTMIfU4lhD6HO6FncK+mIPQqsfp4Qq8zi5tB6HMqIfQ53Ak9g3s1BaFXidXHE3qdWdwMQp9TCaHP4U7oGdyrKQi9Sqw+ntDrzOJmEPqcSgh9DndCz+BeTUHoVWL18YReZxY3g9DnVELoc7gTegb3agpCrxKrjyf0OrO4GYQ+pxJCn8Od0DO4V1MQepVYfTyh15nFzTgq9LgDCYQAAgggcJjAj59/Kq3x758+l8bPGPzp/f39fcbGV+1J6FeRtg8CCCDQhwCh9+nqj6SE3rA0kRFAAIHBBAh9MOARyxP6CKrWRAABBHoTIPSG/VWF3vCIIiOAAAIIDCbgM/TBgLcsT+hbKBmDAAIIIPA9AoQecD8IPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJ/e3tPz//8rCJHz//NKWhZ3mehTkr5+h9q+ufdd7R+551f0avcxbPKS8Km8YTIPSAigid0L9ew6r4qt9IVNc/S0Cj9x0t4tGcq+sHfNkSIZAAoQeUQuiETujbXojPxEfo2/gZtTYBQg/ol9AJndC3vRAJfRsno+5JgNADeid0Qif0bS9EQt/Gyah7EiD0gN4JndAJfdsLkdC3cTLqngQIPaB3Qp8n9Fmfvd5t32cvs9Ecqj/UV81THR/w5UaEhQkQekC5hE7oX69h9aedq0Kpjp8l4rM4EHrAFzgRLiNA6Jehfr4RoRM6of/19UHoAV+YRGhHgNADKiN0Qid0Qg/4UiRCcwKEHlAgoRM6oRN6wJciEZoTIPSAAgmd0Amd0AO+FInQnAChBxRI6IR+9jXs/utdZ32GXuUw64foqvuefV+stwYBQg/okdAJ/exrWBXZaIE+O9+sn7qv/m35s3iete/Z98V6axAg9IAeCZ3Qz76GZwkoTcRn5TlLrNVvSM7a9+z7Yr01CBB6QI+ETuhnX0NC/51olUP10Tehn31zrXeEAKEfoXfSXEIn9JOu0h/LVEXmkfv3GziLp3foZ990630kQOgB94HQCf3sa3iWgM56xJ22zlli9Q797JtrvSMECP0IvZPmEjqhf71Ko98pVwWUJuK0PGk8T/qSZJmmBAg9oDhCJ3RC/+sLcfQ3Nmd9Y0DoAV9ARfiDAKEHXAZCJ3RCJ/RHX4qq39gEfDkTYSIBQp8I/+vWhE7ohE7ohB7wxbh5BEIPKJDQCZ3QCZ3QA74YN49A6AEFEjqhEzqhE3rAF+PmEQg9oEBCfy70s+qpfhZ51q81nfXDV2etUz3Xs31H86zum/bDaVXOVZ5n3Yfq6+sszrPyV8/bbTyhBzRG6IQ++x169WVQFVBVcIRebeT38d2FOzr/Pqp9ZhF6QFeETuiEvu2F2P0P5lS/UdlG5c9Ro4XYff0qz27jCT2gMUIndELf9kIk9O9z6i7c0fm33bK+owg9oDtCJ3RC3/ZCJHRC33ZT7jmK0AN6J3RCJ/RtLxRnboMAAAYiSURBVERCJ/RtN+Weowg9oHdCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBBoS6CDxj1g/vb+/vzfkLDICCCCAAAIIfCBA6K4DAggggAACCxAg9AVKdAQEEEAAAQQI3R1AAAEEEEBgAQKEvkCJjoAAAggggAChuwMIIIAAAggsQIDQFyjRERBAAAEEECB0dwABBBBAAIEFCBD6AiU6AgIIIIAAAoTuDiCAAAIIILAAAUJfoERHQAABBBBAgNDdAQQQQAABBBYgQOgLlOgICCCAAAIIELo7gAACCCCAwAIECH2BEh0BAQQQQAABQncHEEAAAQQQWIAAoS9QoiMggAACCCBA6O4AAggggAACCxAg9AVKdAQEEEAAAQQI3R1AAAEEEEBgAQKEvkCJjoAAAggggAChuwMIIIAAAggsQIDQFyjRERBAAAEEECB0dwABBBBAAIEFCBD6AiU6AgIIIIAAAoTuDiCAAAIIILAAAUJfoERHQAABBBBAgNDdAQQQQAABBBYgQOgLlOgICCCAAAIIELo7gAACCCCAwAIECH2BEh0BAQQQQAABQncHEEAAAQQQWIAAoS9QoiMggAACCCBA6O4AAggggAACCxAg9AVKdAQEEEAAAQQI3R1AAAEEEEBgAQKEvkCJjoAAAggggAChuwMIIIAAAggsQIDQFyjRERBAAAEEECB0dwABBBBAAIEFCBD6AiU6AgIIIIAAAoTuDiCAAAIIILAAAUJfoERHQAABBBBAgNDdAQQQQAABBBYgQOgLlOgICCCAAAIIELo7gAACCCCAwAIECH2BEh0BAQQQQAABQncHEEAAAQQQWIAAoS9QoiMggAACCCBA6O4AAggggAACCxAg9AVKdAQEEEAAAQQI3R1AAAEEEEBgAQL/D2HtDX5HLFC8AAAAAElFTkSuQmCC', 'data:text/html;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAgAElEQVR4Xu3dTZIkt5VF4aylaN6roPWQbUYuRqvQYkgzadjGVfRcS8k2kioyqxhR4YADjvvgn2aS8HNxLiJPhkdm1qf39/f3N/9BAAEEEEAAgdIEPhF66f6ERwABBBBA4DcChO4iIIAAAgggsAEBQt+gREdAAAEEEECA0N0BBBBAAAEENiBA6BuU6AgIIIAAAggQujuAAAIIIIDABgQIfYMSHQEBBBBAAAFCdwcQQAABBBDYgAChb1CiIyCAAAIIIEDo7gACCCCAAAIbECD0DUp0BAQQQAABBAjdHUAAAQQQQGADAoS+QYmOgAACCCCAAKG7AwgggAACCGxAgNA3KNEREEAAAQQQIHR3AAEEEEAAgQ0IEPoGJToCAggggAAChO4OIIAAAgggsAEBQt+gREdAAAEEEECA0N0BBBBAAAEENiBA6BuU6AgIIIAAAggQujuAAAIIIIDABgQIfYMSHQEBBBBAAAFCdwcQQAABBBDYgAChb1CiIyCAAAIIIEDo7gACCCCAAAIbECD0DUp0BAQQQAABBAjdHUAAAQQQQGADAoS+QYmOgAACCCCAAKG7AwgggAACCGxAgNA3KNEREEAAAQQQIHR3AAEEEEAAgQ0IEPoGJToCAggggAAChO4OIIAAAgggsAEBQt+gREdAAAEEEECA0N0BBBBAAAEENiBA6BuU6AgIIIAAAggQujuAAAIIIIDABgQIfYMSHQEBBBBAAAFCdwcQQAABBBDYgAChb1CiIyCAAAIIIEDo7gACCCCAAAIbECD0DUp0BAQQQAABBAjdHUAAAQQQQGADAtsL/X9+/nGDmhwBAQQQQGAFgX/+8NOKbbv2JPQubCYhgAACCNyJQAWxE/qdbqSzIoAAAgh0ESD0LmxjJ3nkPpan1RBAAIE7EiD0gNYJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREDgKwL/+vHnw0y+/+mHw2MNRGAWAUKfRbZhXUJvgGUoAhcRIPSLQNtmGAFCH4ayfyFC72dnJgKzCBD6LLLWnUWA0GeRbViX0BtgGYrARQQI/SLQthlGgNCHoexfiND72ZmJwFkCR8T9t7/97S/b/Pvf//7jf/MZ+tkWzB9BgNBHUDy5BqGfBGg6AicIEPoJeKZGESD0gDoIPaAEEW5LgNBvW/12Byf0gEoJPaAEEd6OiG0WpqseWT864/uTQ3368L+/euT+cYmrzjKrC+vWJUDoAd0RekAJIhD6V3eA0L0oqhEg9IDGCD2gBBEIndC9CooTIPSAAgk9oISbRWh59DwTzcd3wY/2efb4+lH+j2OffXzw7PH6o72fZXv1+N0j95k3xtrfIkDoAfeD0ANKuFkEQn9dOKG/ZmREFgFCD+iD0ANKuFkEQn9dOKG/ZmREFgFCD+iD0ANKuEGEjxJvefS8Es2rR/LPso0+X0sOj9xX3ph7703oAf0TekAJN4hA6P0lE3o/OzOvI0Do17F+uhOhB5RwgwiE3l8yofezM/M6AoR+HWtCD2B9twgVJf6xo88iHf0I/eg9eCZyj9WPEjTuSgKEfiXtJ3t5hx5QwqYRCP1csYR+jp/Z1xIg9Gt5P9yN0ANK2DQCoZ8rltDP8TP7WgKEfi1vQg/gvXuE6hJf9cj9kbw9Wt/91bLX+Qg9oE/v0ANK2CgCofeVSeh93MzKIUDoAV0QekAJG0Ug9L4yCb2Pm1k5BAg9oAtCDyiheISdJD77kbvPxYtfdvGfEiD0gMtB6AElFI9A6McLJPTjrIysRYDQA/oi9IASikcg9OMFEvpxVkbWIkDoAX0RekAJxSMQ+uMCfS5e/GKL30SA0JtwzRlM6HO43mlVQif0O913Z31MgNADbgahB5RQPMLuQv+1np4//+odevGLLX4TAUJvwjVnMKHP4XqnVXcV+q8dfpRyq9Q/z/UHYu70arjvWQk9oHtCDyiheISdhX5G6h+/GSD14pdc/JcECP0lovkDCH0+49132F3opL77DXa+EQQIfQTFk2sQ+kmApr/dQeik7qIj8G0ChB5wQwg9oISJET7KduI2fyzd+jnzFZlG7tH7mbrH7yNbsFYiAUIPaIXQA0qYGIHQx8N99sdhWnbymXoLLWMrECD0gJYIPaCEiREIfQ5cUp/D1ap1CRB6QHeEHlDCiQivhH3FI/Dex9Anjl16qsfvpesT/gkBQg+4GoQeUMKJCIR+At7CqX5HfSF8W08hQOhTsLYtSuhtvNJGE3paI8fyEPoxTkbVIUDoAV0RekAJByK8EvevS1zxeP1RVI/cDxT41RBCb2dmRjYBQg/oh9ADSjgQgdAPQCo0hNALlSXqIQKEfgjT3EGEPpfvqNUJfRTJjHUIPaMHKcYRIPRxLLtXIvRudNMnPpP4qkfrzw7skXv7VSD0dmZmZBMg9IB+CD2ghCcRCD23m7PJCP0sQfPTCBB6QCOEHlACoeeWMCkZoU8Ca9llBAh9Gfo/Nyb0gBLe3r74B04+J0p7tO6R+7i7QujjWFopgwChB/RA6AElEHpGCRemaPnTsf7u+4XF2KqbAKF3oxs3kdDHsTyz0qPPy71DP0M0e26L0D+ehNyze71zOkIPaJ/QA0rwDj2jhAtT9Ar914ikfmFRtjpMgNAPo5o3kNDnsX21cuV35R/PdkROVZ42vOrszP/f8+t9z9iS+pkmzJ1BgNBnUG1ck9AbgQ0cTugDYRZYqkfovx7rW98wEXuB4m8SkdADiib0dSUQ+jr2K3buFfrnrMS+ojV7HiVA6EdJTRxH6BPhPlj6o8Tv9Bj6mYx2Z3BW4o9up8fw175m7XaMAKEf4zR1FKFPxfuXxQn9SySE3n//HondI/h+nmaeI0Do5/gNmU3oQzAeXoTQCf3wZTk4kNgPgjJsKgFCn4r32OKEfozTmVF3lfgRZkd+Qv7zOlXezc94zP6KJam/IuT/n02A0GcTPrA+oR+AdHIIoT8HSOgnL9dX0z/y9Ph9LFurfZsAoQfcEEKfXwKhE/r8W/b7DoR+FWn7fE2A0APuBKHPL4HQxzBOfje/4jH7I6qEPuauWaWdAKG3Mxs+g9CHI/3LgoQ+hjGhv+ZI6K8ZGTGHAKHP4dq0KqE34eoaTOhd2P4yidBfcyT014yMmEOA0OdwbVqV0JtwdQ0m9C5spya1yP/URv+ZnPIT+IQ+ok1r9BAg9B5qg+cQ+mCgD5Yj9PmMv96B0P2rbNffunvvSOgB/RP6/BIIfT5jQv+dgHfo1981O/5OgNADbgKhzy/h0T/C8uuuKY9p5xOww1UECP0q0vb5mgChB9wJQp9fAqHPZ2wH79DdgbUECH0t/992J/T5JRD6fMZ2IHR3YC0BQl/Ln9AX8Pd5+gLoN9rSI/cblR12VEIPKMQ79GtLIPRred9tN0K/W+M55yX0gC4I/doSCP1a3nfbjdDv1njOeQk9oAtCv7YEQr+W9912I/S7NZ5zXkIP6ILQry3h2Q/IPUrh19qu7WaH3Y4IveUOXsHEP/N6BeX5exD6fMYvdyD0l4iGDmj5YkroQ9HfYjFCv0XNkYck9IBaCP3aEgj9Wt53243Q79Z4znkJPaALQj9fQoukz++Ws4InCDldfE7yTOgf7+h33323PPgvv/zyMIPH78ur6Q5A6N3oxk0k9PMsCf08QyuMIUDoYzhapZ0AobczGz6D0M8jJfTzDK0whgChj+FolXYChN7ObPgMQu9D+kzif//73/sWfDDrH//4R9daVz1SffbY9HNoj+S76vvLpKv/Kdhnqa+4Vx/vlMfvY+7PVasQ+lWkv7EPofeVQOhvb4Ted3daZxF6KzHjVxAg9BXUv9qT0PtKIHRC77s57bMIvZ2ZGdcTIPTrmf9lR0I/XsJHiY98tH48wbGRvY/qj6z+6rHrq3ftX++x42P5mQJeee9679WrO/PxTnjkfuRVmDmG0AN6IfTjJRD629urL86E/vZG6F++pl7dGUI//jUoeSShB7RD6MdLIHRCP3JbCJ3Qj9yT3cYQekCjhH68hCpCP36i9pEjHru2vos/mvLqx/fPxL3ysfhRVrPHPbonR96pe+Q+u5l56xP6PLaHVyb0w6jeCP3tjdD/vC+E/vy1Q+jHv67sMpLQA5ok9OMlEDqhf7wthE7ox7967D+S0AM6JvRvl3DFr6cFXIMpEY68mz/yGPZb4WY9vj8CxKP1I5QefxP4rHeP3I8xTRxF6AGtEDqhz7qGhD6LbK11Wx6/E3qtbj+mJfSA7gid0GddQ0KfRbbWuoReq6/etITeS27gPEI/LnSPWAdevP8s9Ur6LY/kZz5+1/357j927ZH7eZ5pKxB6QCOETugrryGhr6R/7d6Efi3vq3cj9KuJP9iP0Al95TUk9JX0r92b0K/lffVuhH41cUJvJu5X1ZqRNU0g9CZcpQcTeun6XoYn9JeI5g/wDt079Pm3rH+HV8L/uHLL5+1HEs38TP7I/t8aU/EzfUI/23r2fEIP6IfQCT3gGj6NQOiP0RB68q29ZzZCD+id0Ak94BoSemMJhN4IzPDpBAh9OuLXGxA6ob++JbkjWt7BHznF6Mf2R/ZsGfP5Y4BKQv/c0RG2/rBMy23IGkvoAX0QOqEHXMPuCITeje6yiYR+GeqlGxH6Uvy/b07ohB5wDbsjEHo3ussmEvplqJduROhL8RP6Efx+be0IpdpjRnxTcORx8ghKHrmPoGiNGQQIfQbVxjW9Q/cOvfHKbDec0OdW6h36XL4pqxN6QBOETugB13BpBEKfi5/Q5/JNWZ3QA5ogdEIPuIblIzz7pmD0o/jkR+69DJ79AZ/vf/qh/L240wEIPaBtQif0gGtYPkKvzFoPTuitxIy/igChX0X6G/sQOqEHXMPyEQj97a2XgXfo5a//bwcg9IAeCf14CX7i/TirO4888jfLe/lc8Q59xM8U9J7PY/ZecuvnEfr6DvweekMHhN4A68ZDCb2/fELvZ7d6JqGvbsAflmlqgNCbcN12MKH3V0/o/exWzyT01Q0QelMDhN6E67aDE4Xe8hidVG97dU8dnNBP4Rsz2WfoxzkS+nFWdx5J6Hdu/75nJ/SA7gn9eAmEfpzVnUcS+p3bv+/ZCT2ge0I/XsJHoX+cVemfsjx+WiN7CVwh9NZsHqO3EjO+lQChtxKbMJ7Qj0Ml9OOs7jyS0O/c/n3PTugB3RP68RII/TirO48k9Du3f9+zE3pA94R+vARCP87qziMJ/c7t3/fshB7QPaGfL+GR6H2ufp5r1RWuELrPxKvejn1zE3pAt4R+vgRCP89wpxUIfac2neUoAUI/SmriOEI/D5fQzzPcaQVC36lNZzlKgNCPkpo4jtDHwn32OfujXTyWH8s+ZTVCT2lCjisJEPqVtJ/sRehjSyD0sTwrrkboFVuT+SwBQj9LcMB8Qh8A8cMShD6WZ8XVCL1iazKfJUDoZwkOmE/oAyB2LvFK/h7Jd4JdPI3QFxdg+yUECH0J9i83JfR1JRD6OvYzdyb0mXStnUqA0AOaIfR1JRD6OvYzdyb0mXStnUqA0AOaIfSAEp5EeCX8r6d5RJ/R5Wih//LLL385mD8sk9G1FH8SIPSA20DoASUQem4JHckIvQOaKeUJEHpAhYQeUAKh55bQkYzQO6CZUp4AoQdUSOgBJZyI4K/UnYA3aeoIoT96zP5rXI/aJ5Vm2dMECP00wvMLEPp5hitXIPSV9B/vTeh5nUg0nwChz2f8cgdCf4koegCh59VD6HmdSDSfAKHPZ/xyB0J/iajcgNafjn91QD89/4rQ21urxJ89Un+0k8fsr/kbsZ4Aoa/v4I3QA0oYHIHQBwM9sByhH4BkyNYECD2gXkIPKGFwBEIfDPTAcoR+AJIhWxMg9IB6CT2ghPAII75B2PGx/UeJf6zwu+++++O/+mn18Mst3jAChD4MZf9ChN7P7i4zCf1x04R+l1eAcx4hQOhHKE0eQ+iTAW+wPKET+gbX2BEmEyD0yYCPLE/oRyjdewyhE/q9XwFOf4QAoR+hNHkMoU8GbPnfCIz4pqAiSr9yVrE1mXsIEHoPtcFzCH0wUMs9JEDoLgYCexMg9IB+CT2ghBtEIPQblOyItyZA6AH1E3pACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxTYKvR//fhzQGoREEAAAQRmEvj+px+alif0JlxzBhP6HK5WRQABBCoTIPSC7RF6wdJERgABBCYTIPTJgGcsT+gzqFoTAQQQqE2A0Av2d1ro//t/BU8dEPm//+thiPf394Bw94nw6dOnLw6Lf2b3eprfy9eMCX0+8+E7EPpwpMcWJPRjnCaPIorJgActr6dBIL+xDKHPZzx9B0KfjvjxBoS+CPyX2xJFRA0vQ+jpJaLTAwj9NML1CxD6og4IfRF4Qo8A3xiC0BuBdQwn9A5oaVMIfVEjhL4IPKFHgG8MQeiNwDqGE3oHtLQphL6oEUJfBJ7QI8A3hiD0RmAdwwm9A1raFEJf1AihLwJP6BHgG0MQeiOwjuGE3gEtbQqhL2qE0BeBJ/QI8I0hCL0RWMdwQu+AljaF0Bc1QuiLwBN6BPjGEITeCKxjOKF3QEubQuiLGiH0ReAJPQJ8YwhCbwTWMZzQO6ClTSH0RY0Q+iLwhB4BvjEEoTcC6xhO6B3Q0qYQ+qJGCH0ReEKPAN8YgtAbgXUMJ/QOaGlTCH1RI4S+CDyhR4BvDEHojcA6hhN6B7S0KYS+qBFCXwSe0CPAN4Yg9EZgHcMJvQNa2hRCX9QIoS8CT+gR4BtDEHojsI7hhN4BLW0KoS9qhNAXgSf0CPCNIQi9EVjHcELvgJY2hdAXNULoi8ATegT4xhCE3gisYzihd0BLm0Loixoh9EXgCT0CfGMIQm8E1jGc0DugpU0h9EWNEPoi8IQeAb4xBKE3AusYTugd0NKmEPqiRgh9EXhCjwDfGILQG4F1DCf0DmhpUwh9USOEvgg8oUeAbwxB6I3AOoYTege0tCmEvqgRQl8EntAjwDeGIPRGYB3DCb0DWtoUQl/UCKEvAk/oEeAbQxB6I7CO4YTeAS1tCqEvaoTQF4En9AjwjSEIvRFYx3BC74CWNoXQFzVC6IvAE3oE+MYQhN4IrGM4oXdAS5tC6IsaIfRF4Ak9AnxjCEJvBNYxnNA7oKVNIfRFjRD6IvCEHgG+MQShNwLrGE7oHdDSphD6okYIfRF4Qo8A3xiC0BuBdQwn9A5oaVMIfVEjhL4IPKFHgG8MQeiNwDqGE3oHtLQphL6oEUJfBJ7QI8A3hiD0RmAdwwm9A1raFEJf1AihLwJP6BHgG0MQeiOwjuGE3gEtbQqhL2qE0BeBJ/QI8I0hCL0RWMdwQu+AljaF0Bc1QuiLwBN6BPjGEITeCKxjOKF3QEubQuiLGiH0ReAJPQJ8YwhCbwTWMZzQO6ClTSH0RY0Q+iLwhB4BvjEEoTcC6xhO6B3Q0qacFnrageRBAAEEEDhN4Puffmha458//NQ0fsXgT+/v7+8rNr5qT0K/irR9EEAAgToECL1OV38kJfSCpYmMAAIITCZA6JMBz1ie0GdQtSYCCCBQmwChF+yvVegFjygyAggggMBkAj5Dnwz4yPKEfoSSMQgggAAC3yJA6AH3g9ADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNDf3v71488Pm/j+px+WNPQsz7Mwo3LO3rd1/VHnnb3vqPsze51RPJe8KGwaT4DQAyoidEL/fA1bxdf6jUTr+qMENHvf2SKezbl1/YAvWyIEEiD0gFIIndAJ/dgL8Zn4CP0YP6P2JkDoAf0SOqET+rEXIqEf42TUPQkQekDvhE7ohH7shUjoxzgZdU8ChB7QO6ETOqEfeyES+jFORt2TAKEH9E7o64S+6rPXu+377GU2m0PrD/W15mkdH/DlRoSNCRB6QLmETuifr2HrTzu3CqV1/CoRj+JA6AFf4ES4jAChX4b6+UaETuiE/uXrg9ADvjCJUI4AoQdURuiETuiEHvClSITiBAg9oEBCJ3RCJ/SAL0UiFCdA6AEFEjqhEzqhB3wpEqE4AUIPKJDQCX30Naz+612jPkNv5bDqh+ha9x19X6y3BwFCD+iR0Al99DVsFdlsgT4736qfum/92/KjeI7ad/R9sd4eBAg9oEdCJ/TR13CUgNJEPCrPKLG2fkMyat/R98V6exAg9IAeCZ3QR19DQv+daCuH1kffhD765lrvDAFCP0Nv0FxCJ/RBV+mPZVpF5pH7txsYxdM79NE33XofCRB6wH0gdEIffQ1HCWjUI+60dUaJ1Tv00TfXemcIEPoZeoPmEjqhf75Ks98ptwooTcRpedJ4DvqSZJmiBAg9oDhCJ3RC//KFOPsbm1HfGBB6wBdQEf4gQOgBl4HQCZ3QCf3Rl6LWb2wCvpyJsJAAoS+E/3lrQid0Qid0Qg/4Ylw8AqEHFEjohE7ohE7oAV+Mi0cg9IACCZ3QCZ3QCT3gi3HxCIQeUCChPxf6qHpaP4sc9WtNo374atQ6red6tu9snq37pv1wWivnVp6j7kPr62sU51X5W89bbTyhBzRG6IS++h1668ugVUCtgiP01kZ+H19duLPz91GtM4vQA7oidEIn9GMvxOp/MKf1G5VjVP4cNVuI1ddv5VltPKEHNEbohE7ox16IhP5tTtWFOzv/sVtWdxShB3RH6IRO6CMKdmUAAAY4SURBVMdeiIRO6Mduyj1HEXpA74RO6IR+7IVI6IR+7KbccxShB/RO6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBBAhUk/hHrp/f39/eCnEVGAAEEEEAAgQ8ECN11QAABBBBAYAMChL5BiY6AAAIIIIAAobsDCCCAAAIIbECA0Dco0REQQAABBBAgdHcAAQQQQACBDQgQ+gYlOgICCCCAAAKE7g4ggAACCCCwAQFC36BER0AAAQQQQIDQ3QEEEEAAAQQ2IEDoG5ToCAgggAACCBC6O4AAAggggMAGBAh9gxIdAQEEEEAAAUJ3BxBAAAEEENiAAKFvUKIjIIAAAgggQOjuAAIIIIAAAhsQIPQNSnQEBBBAAAEECN0dQAABBBBAYAMChL5BiY6AAAIIIIAAobsDCCCAAAIIbECA0Dco0REQQAABBBAgdHcAAQQQQACBDQgQ+gYlOgICCCCAAAKE7g4ggAACCCCwAQFC36BER0AAAQQQQIDQ3QEEEEAAAQQ2IEDoG5ToCAgggAACCBC6O4AAAggggMAGBAh9gxIdAQEEEEAAAUJ3BxBAAAEEENiAAKFvUKIjIIAAAgggQOjuAAIIIIAAAhsQIPQNSnQEBBBAAAEECN0dQAABBBBAYAMChL5BiY6AAAIIIIAAobsDCCCAAAIIbECA0Dco0REQQAABBBAgdHcAAQQQQACBDQgQ+gYlOgICCCCAAAKE7g4ggAACCCCwAQFC36BER0AAAQQQQIDQ3QEEEEAAAQQ2IEDoG5ToCAgggAACCBC6O4AAAggggMAGBAh9gxIdAQEEEEAAAUJ3BxBAAAEEENiAAKFvUKIjIIAAAgggQOjuAAIIIIAAAhsQIPQNSnQEBBBAAAEECN0dQAABBBBAYAMChL5BiY6AAAIIIIAAobsDCCCAAAIIbEDg/wHl9+pvcaTI0QAAAABJRU5ErkJggg==', 'data:text/html;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAgAElEQVR4Xu3dTZIkt5VF4aylaN6roPWQbUYuRqvQYkgzadjGVfRcS8k2kioyWRVRAcAdjvvgn2aS8HNxLiJPhkdm1qf39/f3N/9BAAEEEEAAgdIEPhF66f6ERwABBBBA4DcChO4iIIAAAgggsAEBQt+gREdAAAEEEECA0N0BBBBAAAEENiBA6BuU6AgIIIAAAggQujuAAAIIIIDABgQIfYMSHQEBBBBAAAFCdwcQQAABBBDYgAChb1CiIyCAAAIIIEDo7gACCCCAAAIbECD0DUp0BAQQQAABBAjdHUAAAQQQQGADAoS+QYmOgAACCCCAAKG7AwgggAACCGxAgNA3KNEREEAAAQQQIHR3AAEEEEAAgQ0IEPoGJToCAggggAAChO4OIIAAAgggsAEBQt+gREdAAAEEEECA0N0BBBBAAAEENiBA6BuU6AgIIIAAAggQujuAAAIIIIDABgQIfYMSHQEBBBBAAAFCdwcQQAABBBDYgAChb1CiIyCAAAIIIEDo7gACCCCAAAIbECD0DUp0BAQQQAABBAjdHUAAAQQQQGADAoS+QYmOgAACCCCAAKG7AwgggAACCGxAgNA3KNEREEAAAQQQIHR3AAEEEEAAgQ0IEPoGJToCAggggAAChO4OIIAAAgggsAEBQt+gREdAAAEEEECA0N0BBBBAAAEENiBA6BuU6AgIIIAAAggQujuAAAIIIIDABgQIfYMSHQEBBBBAAAFCdwcQQAABBBDYgAChb1CiIyCAAAIIIEDo7gACCCCAAAIbECD0DUp0BAQQQAABBAjdHUAAAQQQQGADAtsL/X9+/nGDmhwBAQQQQGAFgX/+8NOKbYf2JPQhbCYhgAACCNyJQAWxE/qdbqSzIoAAAggMESD0IWznTvLI/VyeVkMAAQTuSIDQA1on9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAGBLwj868efm5l8/9MPzWMNRGAWAUKfRbZjXULvgGUoAhcRIPSLQNvmNAKEfhrK8YUIfZydmQjMIkDos8hadxYBQp9FtmNdQu+AZSgCFxEg9ItA2+Y0AoR+GsrxhQh9nJ2ZCBwl0CLuv/3tb19t8+9///uP/81n6EdbMP8MAoR+BsWDaxD6QYCmI3CAAKEfgGdqFAFCD6iD0ANKEOG2BAj9ttVvd3BCD6iU0ANKEOGtRWyzMF31yPrRGd+fHOrTh//91SP3j0tcdZZZXVi3LgFCD+iO0ANKEIHQv7gDhO5FUY0AoQc0RugBJYhA6ITuVVCcAKEHFEjoASXcLELPo+eZaD6+C360z7PH14/yfxz77OODZ4/XH+39LNurx+8euc+8Mdb+FgFCD7gfhB5Qws0iEPrrwgn9NSMjsggQekAfhB5Qws0iEPrrwgn9NSMjsggQekAfhB5Qwg0ifJR4z6PnlWhePZJ/lu3s8/Xk8Mh95Y25996EHtA/oQeUcIMIhD5eMqGPszPzOgKEfh3rpzsRekAJN4hA6OMlE/o4OzOvI0Do17Em9ADWd4tQUeIfO/os0rMfobfeg2ci91i9laBxVxIg9CtpP9nLO/SAEjaNQOjHiiX0Y/zMvpYAoV/L++FuhB5QwqYRCP1YsYR+jJ/Z1xIg9Gt5E3oA790jVJf4qkfuj+Tt0frur5a9zkfoAX16hx5QwkYRCH2sTEIf42ZWDgFCD+iC0ANK2CgCoY+VSehj3MzKIUDoAV0QekAJxSPsJPHZj9x9Ll78sov/lAChB1wOQg8ooXgEQm8vkNDbWRlZiwChB/RF6AElFI9A6O0FEno7KyNrESD0gL4IPaCE4hEI/XGBPhcvfrHF7yJA6F245gwm9Dlc77QqoRP6ne67sz4mQOgBN4PQA0ooHmF3of9az8iff/UOvfjFFr+LAKF34ZozmNDncL3TqrsK/dcOP0q5V+qf5/oDMXd6Ndz3rIQe0D2hB5RQPMLOQj8i9Y/fDJB68Usu/ksChP4S0fwBhD6f8e477C50Ut/9BjvfGQQI/QyKB9cg9IMATX+7g9BJ3UVH4NsECD3ghhB6QAkTI3yU7cRt/li693PmKzKducfoZ+oev5/ZgrUSCRB6QCuEHlDCxAiEfj7cZ38cpmcnn6n30DK2AgFCD2iJ0ANKmBiB0OfAJfU5XK1alwChB3RH6AElHIjwSthXPAIffQx94Nilp3r8Xro+4Z8QIPSAq0HoASUciEDoB+AtnOp31BfCt/UUAoQ+BWvfooTexyttNKGnNdKWh9DbOBlVhwChB3RF6AElNER4Je5fl7ji8fqjqB65NxT4xRBC72dmRjYBQg/oh9ADSmiIQOgNkAoNIfRCZYnaRIDQmzDNHUToc/metTqhn0UyYx1Cz+hBivMIEPp5LIdXIvRhdNMnPpP4qkfrzw7skXv/VSD0fmZmZBMg9IB+CD2ghCcRCD23m6PJCP0oQfPTCBB6QCOEHlACoeeWMCkZoU8Ca9llBAh9Gfo/Nyb0gBLe3v7yD5x8TpT2aN0j9/PuCqGfx9JKGQQIPaAHQg8ogdAzSrgwRc+fjvV33y8sxlbDBAh9GN15Ewn9PJZHVnr0ebl36EeIZs/tEfrHk5B7dq93TkfoAe0TekAJ3qFnlHBhilGh/xqR1C8sylbNBAi9GdW8gYQ+j+2rlSu/K/94thY5VXna8KqzI///yK/3PWNL6keaMHcGAUKfQbVzTULvBHbicEI/EWaBpUaE/uuxvvUNE7EXKP4mEQk9oGhCX1cCoa9jv2LnUaF/zkrsK1qzZysBQm8lNXEcoU+E+2DpjxK/02PoZzLancFRiT+6nR7DX/uatVsbAUJv4zR1FKFPxfvV4oT+VySEPn7/HondI/hxnmYeI0Dox/idMpvQT8HYvAihE3rzZWkcSOyNoAybSoDQp+JtW5zQ2zgdGXVXibcwa/kJ+c/rVHk3P+Mx+yuWpP6KkP9/NgFCn024YX1Cb4B0cAihPwdI6Acv1xfTP/L0+P1ctlb7NgFCD7ghhD6/BEIn9Pm37PcdCP0q0vb5kgChB9wJQp9fAqGfwzj53fyKx+yPqBL6OXfNKv0ECL2f2ekzCP10pF8tSOjnMCb01xwJ/TUjI+YQIPQ5XLtWJfQuXEODCX0I21eTCP01R0J/zciIOQQIfQ7XrlUJvQvX0GBCH8J2aFKP/A9t9J/JKT+BT+hntGmNEQKEPkLt5DmEfjLQB8sR+nzGX+5A6P5Vtutv3b13JPSA/gl9fgmEPp8xof9OwDv06++aHX8nQOgBN4HQ55fw6B9h+XXXlMe08wnY4SoChH4Vaft8SYDQA+4Eoc8vgdDnM7aDd+juwFoChL6W/2+7E/r8Egh9PmM7ELo7sJYAoa/lT+gL+Ps8fQH0G23pkfuNyg47KqEHFOId+rUlEPq1vO+2G6HfrfGc8xJ6QBeEfm0JhH4t77vtRuh3azznvIQe0AWhX1sCoV/L+267EfrdGs85L6EHdEHo15bw7AfkHqXwa23XdrPDbi1C77mDVzDxz7xeQXn+HoQ+n/HLHQj9JaJTB/R8MSX0U9HfYjFCv0XNkYck9IBaCP3aEgj9Wt53243Q79Z4znkJPaALQj9eQo+kj++Ws4InCDldfE7yTOgf7+h33323PPgvv/zyMIPH78urGQ5A6MPozptI6MdZEvpxhlY4hwChn8PRKv0ECL2f2ekzCP04UkI/ztAK5xAg9HM4WqWfAKH3Mzt9BqGPIX0m8b///e9jCz6Y9Y9//GNoraseqT57bPo5tEfyQ/V9Nenqfwr2Weor7tXHO+Xx+zn356pVCP0q0t/Yh9DHSiD0tzdCH7s7vbMIvZeY8SsIEPoK6l/sSehjJRA6oY/dnP5ZhN7PzIzrCRD69cy/2pHQ20v4KPEzH623J2gbOfqovmX1V49dX71r/3KPHR/LzxTwyns3eq9e3ZmPd8Ij95ZXYeYYQg/ohdDbSyD0t7dXX5wJ/e2N0P/6mnp1Zwi9/WtQ8khCD2iH0NtLIHRCb7kthE7oLfdktzGEHtAoobeXUEXo7SfqH3nGY9fed/GtKa9+fP9M3Csfi7eymj3u0T1peafukfvsZuatT+jz2DavTOjNqN4I/e2N0P+8L4T+/LVD6O1fV3YZSegBTRJ6ewmETugfbwuhE3r7V4/9RxJ6QMeE/u0Srvj1tIBrMCVCy7v5lsew3wo36/F9CxCP1lsoPf4m8FnvHrm3MU0cRegBrRA6oc+6hoQ+i2ytdXsevxN6rW4/piX0gO4IndBnXUNCn0W21rqEXquv0bSEPkruxHmE3i50j1hPvHj/WeqV9Hseyc98/K77491/7Noj9+M801Yg9IBGCJ3QV15DQl9J/9q9Cf1a3lfvRuhXE3+wH6ET+sprSOgr6V+7N6Ffy/vq3Qj9auKE3k3cr6p1I+uaQOhduEoPJvTS9b0MT+gvEc0f4B26d+jzb9n4Dq+E/3Hlns/bWxLN/Ey+Zf9vjan4mT6hH209ez6hB/RD6IQecA2fRiD0x2gIPfnW3jMboQf0TuiEHnANCb2zBELvBGb4dAKEPh3x6w0IndBf35LcET3v4FtOcfZj+5Y9e8Z8/higktA/d9TC1h+W6bkNWWMJPaAPQif0gGs4HIHQh9FdNpHQL0O9dCNCX4r/980JndADruFwBEIfRnfZREK/DPXSjQh9KX5Cb8Hv19ZaKNUec8Y3BS2Pk8+g5JH7GRStMYMAoc+g2rmmd+jeoXdeme2GE/rcSr1Dn8s3ZXVCD2iC0Ak94BoujUDoc/ET+ly+KasTekAThE7oAdewfIRn3xSc/Sg++ZH7KINnf8Dn+59+KH8v7nQAQg9om9AJPeAalo8wKrPegxN6LzHjryJA6FeR/sY+hE7oAdewfARCf3sbZeAdevnr/9sBCD2gR0JvL8FPvLezuvPIlr9ZPsrninfoZ/xMwej5PGYfJbd+HqGv78DvoXd0QOgdsG48lNDHyyf0cXarZxL66gb8YZmuBgi9C9dtBxP6ePWEPs5u9UxCX90AoXc1QOhduG47OFHoPY/RSfW2V/fQwQn9EL5zJvsMvZ0jobezuvNIQr9z+/c9O6EHdE/o7SUQejurO48k9Du3f9+zE3pA94TeXsJHoX+cVemfsmw/rZGjBK4Qem82j9F7iRnfS4DQe4lNGE/o7VAJvZ3VnUcS+p3bv+/ZCT2ge0JvL4HQ21ndeSSh37n9+56d0AO6J/T2Egi9ndWdRxL6ndu/79kJPaB7Qj9ewiPR+1z9ONeqK1whdJ+JV70d++Ym9IBuCf14CYR+nOFOKxD6Tm06SysBQm8lNXEcoR+HS+jHGe60AqHv1KaztBIg9FZSE8cR+rlwn33O/mgXj+XPZZ+yGqGnNCHHlQQI/UraT/Yi9HNLIPRzeVZcjdArtibzUQKEfpTgCfMJ/QSIH5Yg9HN5VlyN0Cu2JvNRAoR+lOAJ8wn9BIiDS7ySv0fyg2AXTyP0xQXYfgkBQl+C/a+bEvq6Egh9HfuZOxP6TLrWTiVA6AHNEPq6Egh9HfuZOxP6TLrWTiVA6AHNEHpACU8ivBL+l9M8os/o8myh//LLL18dzB+Wyehaij8JEHrAbSD0gBIIPbeEgWSEPgDNlPIECD2gQkIPKIHQc0sYSEboA9BMKU+A0AMqJPSAEg5E8FfqDsCbNPUMoT96zP5rXI/aJ5Vm2cMECP0wwuMLEPpxhitXIPSV9B/vTeh5nUg0nwChz2f8cgdCf4koegCh59VD6HmdSDSfAKHPZ/xyB0J/iajcgN6fjn91QD89/4rQ21uvxJ89Un+0k8fsr/kbsZ4Aoa/v4I3QA0o4OQKhnwy0YTlCb4BkyNYECD2gXkIPKOHkCIR+MtCG5Qi9AZIhWxMg9IB6CT2ghPAIZ3yDsONj+48S/1jhd99998d/9dPq4ZdbvNMIEPppKMcXIvRxdneZSeiPmyb0u7wCnLOFAKG3UJo8htAnA95geUIn9A2usSNMJkDokwG3LE/oLZTuPYbQCf3erwCnbyFA6C2UJo8h9MmALf8bgTO+KaiI0q+cVWxN5hEChD5C7eQ5hH4yUMs9JEDoLgYCexMg9IB+CT2ghBtEIPQblOyItyZA6AH1E3pACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxTYK/R//fhzQGoREEAAAQRmEvj+px+6lif0LlxzBhP6HK5WRQABBCoTIPSC7RF6wdJERgABBCYTIPTJgGcsT+gzqFoTAQQQqE2A0Av2d1jo//t/BU8dEPm//+txCDyvLeeLHt7f36/d325NBD59+vSXcXpqwtY16EvGhN6FL2MwoS/qgdAXgf9iW0LP6OFFCkKfXxOhz2c8fQdCn4748QaEvgg8oWeA70tB6H28RkYT+gi1sDmEvqgQQl8EntAzwPelIPQ+XiOjCX2EWtgcQl9UCKEvAk/oGeD7UhB6H6+R0YQ+Qi1sDqEvKoTQF4En9AzwfSkIvY/XyGhCH6EWNofQFxVC6IvAE3oG+L4UhN7Ha2Q0oY9QC5tD6IsKIfRF4Ak9A3xfCkLv4zUymtBHqIXNIfRFhRD6IvCEngG+LwWh9/EaGU3oI9TC5hD6okIIfRF4Qs8A35eC0Pt4jYwm9BFqYXMIfVEhhL4IPKFngO9LQeh9vEZGE/oItbA5hL6oEEJfBJ7QM8D3pSD0Pl4jowl9hFrYHEJfVAihLwJP6Bng+1IQeh+vkdGEPkItbA6hLyqE0BeBJ/QM8H0pCL2P18hoQh+hFjaH0BcVQuiLwBN6Bvi+FITex2tkNKGPUAubQ+iLCiH0ReAJPQN8XwpC7+M1MprQR6iFzSH0RYUQ+iLwhJ4Bvi8FoffxGhlN6CPUwuYQ+qJCCH0ReELPAN+XgtD7eI2MJvQRamFzCH1RIYS+CDyhZ4DvS0HofbxGRhP6CLWwOYS+qBBCXwSe0DPA96Ug9D5eI6MJfYRa2BxCX1QIoS8CT+gZ4PtSEHofr5HRhD5CLWwOoS8qhNAXgSf0DPB9KQi9j9fIaEIfoRY2h9AXFULoi8ATegb4vhSE3sdrZDShj1ALm0Poiwoh9EXgCT0DfF8KQu/jNTKa0Eeohc0h9EWFEPoi8ISeAb4vBaH38RoZTegj1MLmEPqiQgh9EXhCzwDfl4LQ+3iNjCb0EWphcwh9USGEvgg8oWeA70tB6H28RkYT+gi1sDmEvqgQQl8EntAzwPelIPQ+XiOjCX2EWtgcQl9UCKEvAk/oGeD7UhB6H6+R0YQ+Qi1sDqEvKoTQF4En9AzwfSkIvY/XyGhCH6EWNofQFxVC6IvAE3oG+L4UhN7Ha2Q0oY9QC5tD6IsKIfRF4Ak9A3xfCkLv4zUymtBHqIXNIfRFhRD6IvCEngG+LwWh9/EaGU3oI9TC5hD6okIIfRF4Qs8A35eC0Pt4jYwm9BFqYXMIfVEhhL4IPKFngO9LQeh9vEZGE/oItbA5h4Uedh5xEEAAAQSOE/j+px+6FvnnDz91jV8x+NP7+/v7io2v2pPQryJtHwQQQKAOAUKv09UfSQm9YGkiI4AAApMJEPpkwDOWJ/QZVK2JAAII1CZA6AX76xV6wSOKjAACCCAwmYDP0CcDblme0FsoGYMAAggg8C0ChB5wPwg9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgn97e1fP/78sInvf/phSUPP8jwLc1bO2fv2rn/WeWfve9b9mb3OWTyXvChsGk+A0AMqInRC/3wNe8XX+41E7/pnCWj2vrNFPJtz7/oBX7ZECCRA6AGlEDqhE3rbC/GZ+Ai9jZ9RexMg9IB+CZ3QCb3thUjobZyMuicBQg/ondAJndDbXoiE3sbJqHsSIPSA3gmd0Am97YVI6G2cjLonAUIP6J3Q1wl91Wevd9v32ctsNofeH+rrzdM7PuDLjQgbEyD0gHIJndA/X8Pen3buFUrv+FUiPosDoQd8gRPhMgKEfhnq5xsROqET+l9fH4Qe8IVJhHIECD2gMkIndEIn9IAvRSIUJ0DoAQUSOqETOqEHfCkSoTgBQg8okNAJndAJPeBLkQjFCRB6QIGETuhnX8Pqv9511mfovRxW/RBd775n3xfr7UGA0AN6JHRCP/sa9opstkCfnW/VT933/m35s3iete/Z98V6exAg9IAeCZ3Qz76GZwkoTcRn5TlLrL3fkJy179n3xXp7ECD0gB4JndDPvoaE/jvRXg69j74J/eyba70jBAj9CL2T5hI6oZ90lf5YpldkHrl/u4GzeHqHfvZNt95HAoQecB8IndDPvoZnCeisR9xp65wlVu/Qz7651jtCgNCP0DtpLqET+uerNPudcq+A0kSclieN50lfkixTlAChBxRH6IRO6H99Ic7+xuasbwwIPeALqAh/ECD0gMtA6IRO6IT+6EtR7zc2AV/ORFhIgNAXwv+8NaETOqETOqEHfDEuHoHQAwokdEIndEIn9IAvxsUjEHpAgYRO6IRO6IQe8MW4eARCDyiQ0J8L/ax6ej+LPOvXms764auz1uk917N9Z/Ps3Tfth9N6OffyPOs+9L6+zuK8Kn/veauNJ/SAxgid0Fe/Q+99GfQKqFdwhN7byO/jqwt3dv4xqnVmEXpAV4RO6ITe9kKs/gdzer9RaaPy56jZQqy+fi/PauMJPaAxQid0Qm97IRL6tzlVF+7s/G23rO4oQg/ojtAJndDbXvXz/iIAAAY1SURBVIiETuhtN+Weowg9oHdCJ3RCb3shEjqht92Ue44i9IDeCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoSKCCxD9i/fT+/v5ekLPICCCAAAIIIPCBAKG7DggggAACCGxAgNA3KNEREEAAAQQQIHR3AAEEEEAAgQ0IEPoGJToCAggggAAChO4OIIAAAgggsAEBQt+gREdAAAEEEECA0N0BBBBAAAEENiBA6BuU6AgIIIAAAggQujuAAAIIIIDABgQIfYMSHQEBBBBAAAFCdwcQQAABBBDYgAChb1CiIyCAAAIIIEDo7gACCCCAAAIbECD0DUp0BAQQQAABBAjdHUAAAQQQQGADAoS+QYmOgAACCCCAAKG7AwgggAACCGxAgNA3KNEREEAAAQQQIHR3AAEEEEAAgQ0IEPoGJToCAggggAAChO4OIIAAAgggsAEBQt+gREdAAAEEEECA0N0BBBBAAAEENiBA6BuU6AgIIIAAAggQujuAAAIIIIDABgQIfYMSHQEBBBBAAAFCdwcQQAABBBDYgAChb1CiIyCAAAIIIEDo7gACCCCAAAIbECD0DUp0BAQQQAABBAjdHUAAAQQQQGADAoS+QYmOgAACCCCAAKG7AwgggAACCGxAgNA3KNEREEAAAQQQIHR3AAEEEEAAgQ0IEPoGJToCAggggAAChO4OIIAAAgggsAEBQt+gREdAAAEEEECA0N0BBBBAAAEENiBA6BuU6AgIIIAAAggQujuAAAIIIIDABgQIfYMSHQEBBBBAAAFCdwcQQAABBBDYgAChb1CiIyCAAAIIIEDo7gACCCCAAAIbECD0DUp0BAQQQAABBAjdHUAAAQQQQGADAoS+QYmOgAACCCCAAKG7AwgggAACCGxAgNA3KNEREEAAAQQQIHR3AAEEEEAAgQ0I/D8aDcdvKWGEIgAAAABJRU5ErkJggg==', 'data:text/html;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAgAElEQVR4Xu3dTXIkx5VFYdRSNO9V0HrINiMXo1VoMaSZNGzjKnqupaCNpIoEqzIr40WEh9/n8fWsRf+5fq4nDjISQH16f39/f/N/CCCAAAIIINCawCdCb92f8AgggAACCPxGgNBdBAQQQAABBBYgQOgLlOgICCCAAAIIELo7gAACCCCAwAIECH2BEh0BAQQQQAABQncHEEAAAQQQWIAAoS9QoiMggAACCCBA6O4AAggggAACCxAg9AVKdAQEEEAAAQQI3R1AAAEEEEBgAQKEvkCJjoAAAggggAChuwMIIIAAAggsQIDQFyjRERBAAAEEECB0dwABBBBAAIEFCBD6AiU6AgIIIIAAAoTuDiCAAAIIILAAAUJfoERHQAABBBBAgNDdAQQQQAABBBYgQOgLlOgICCCAAAIIELo7gAACCCCAwAIECH2BEh0BAQQQQAABQncHEEAAAQQQWIAAoS9QoiMggAACCCBA6O4AAggggAACCxAg9AVKdAQEEEAAAQQI3R1AAAEEEEBgAQKEvkCJjoAAAggggAChuwMIIIAAAggsQIDQFyjRERBAAAEEECB0dwABBBBAAIEFCBD6AiU6AgIIIIAAAoTuDiCAAAIIILAAAUJfoERHQAABBBBAgNDdAQQQQAABBBYgQOgLlOgICCCAAAIIELo7gAACCCCAwAIECH2BEh0BAQQQQAABQncHEEAAAQQQWIAAoS9QoiMggAACCCBA6O4AAggggAACCxAg9AVKdAQEEEAAAQQI3R1AAAEEEEBgAQKEvkCJjoAAAggggAChuwMIIIAAAggsQGB5of/Pzz8uUJMjIIAAAgjMIPDPH36ase2uPQl9FzaTEEAAAQTuRKCD2An9TjfSWRFAAAEEdhEg9F3Yzp3kkfu5PK2GAAII3JEAoQe0TugBJYiAAAIINCdA6AEFEnpACSIggAACzQkQekCBhB5QgggIIIBAcwKEHlAgoQeUIAICCCDQnAChBxRI6AEliIAAAgg0J0DoAQUSekAJIiCAAALNCRB6QIGEHlCCCAgggEBzAoQeUCChB5QgAgIIINCcAKEHFEjoASWIgAACCDQnQOgBBRJ6QAkiIIAAAs0JEHpAgYQeUIIICCCAQHMChB5QIKEHlCACAggg0JwAoQcUSOgBJYiAAAIINCdA6AEFEnpACSIggAACzQkQekCBhB5QgggIIIBAcwKEHlAgoQeUIAICCCDQnAChBxRI6AEliIAAAgg0J0DoAQUSekAJIiCAAALNCRB6QIGEHlCCCAgggEBzAoQeUCChB5QgAgIIINCcAKEHFEjoASWIgAACCDQnQOgBBRJ6QAkiIIAAAs0JEHpAgYQeUIIICCCAQHMChB5QIKEHlCACAl8Q+NePP29m8v1PP2weayACowgQ+iiyhXUJvQDLUAQuIkDoF4G2zWkECP00lPsXIvT97MxEYBQBQh9F1rqjCBD6KLKFdQm9AMtQBC4iQOgXgbbNaQQI/TSU+xci9P3szETgKIEt4v7b3/721Tb//ve///jffIZ+tAXzzyBA6GdQPLgGoR8EaDoCBwgQ+gF4pkYRIPSAOgg9oAQRbkuA0G9b/XIHJ/SASgk9oAQR3raIbRSmqx5ZPzrj+5NDffrwv7965P5xiavOMqoL6/YlQOgB3RF6QAkiEPoXd4DQvSi6ESD0gMYIPaAEEQid0L0KmhMg9IACCT2ghJtFqDx6Honm47vgR/s8e3z9KP/Hsc8+Pnj2eP3R3s+yvXr87pH7yBtj7W8RIPSA+0HoASXcLAKhvy6c0F8zMiKLAKEH9EHoASXcLAKhvy6c0F8zMiKLAKEH9EHoASXcIMJHiVcePc9E8+qR/LNsZ5+vksMj95k35t57E3pA/4QeUMINIhD6/pIJfT87M68jQOjXsX66E6EHlHCDCIS+v2RC38/OzOsIEPp1rAk9gPXdInSU+MeOPov07EfoW+/BM5F7rL6VoHFXEiD0K2k/2cs79IASFo1A6MeKJfRj/My+lgChX8v74W6EHlDCohEI/VixhH6Mn9nXEiD0a3kTegDv1SN0l/isR+6P5O3R+uqvlrXOR+gBfXqHHlDCQhEIfV+ZhL6Pm1k5BAg9oAtCDyhhoQiEvq9MQt/HzawcAoQe0AWhB5TQPMJKEh/9yN3n4s0vu/hPCRB6wOUg9IASmkcg9O0FEvp2Vkb2IkDoAX0RekAJzSMQ+vYCCX07KyN7ESD0gL4IPaCE5hEI/XGBPhdvfrHFLxEg9BKuMYMJfQzXO61K6IR+p/vurI8JEHrAzSD0gBKaR1hd6L/Ws+fPv3qH3vxii18iQOglXGMGE/oYrndadVWh/9rhRylXpf55rj8Qc6dXw33PSugB3RN6QAnNI6ws9CNS//jNAKk3v+TivyRA6C8RjR9A6OMZr77D6kIn9dVvsPOdQYDQz6B4cA1CPwjQ9Lc7CJ3UXXQEvk2A0ANuCKEHlDAwwkfZDtzmj6WrnzNfkenMPfZ+pu7x+5ktWCuRAKEHtELoASUMjEDo58N99sdhKjv5TL1Cy9gOBAg9oCVCDyhhYARCHwOX1MdwtWpfAoQe0B2hB5RwIMIrYV/xCHzvY+gDx2491eP31vUJ/4QAoQdcDUIPKOFABEI/AG/iVL+jPhG+rYcQIPQhWGuLEnqNV9poQk9rZFseQt/Gyag+BAg9oCtCDyhhQ4RX4v51iSserz+K6pH7hgK/GELodWZmZBMg9IB+CD2ghA0RCH0DpEZDCL1RWaJuIkDomzCNHUToY/metTqhn0UyYx1Cz+hBivMIEPp5LHevROi70Q2f+Ezisx6tPzuwR+71q0DodWZmZBMg9IB+CD2ghCcRCD23m6PJCP0oQfPTCBB6QCOEHlACoeeWMCgZoQ8Ca9lpBAh9Gvo/Nyb0gBLe3v7yD5x8TpT2aN0j9/PuCqGfx9JKGQQIPaAHQg8ogdAzSrgwReVPx/q77xcWY6vdBAh9N7rzJhL6eSyPrPTo83Lv0I8QzZ5bEfrHk5B7dq93TkfoAe0TekAJ3qFnlHBhir1C/zUiqV9YlK02EyD0zajGDST0cWxfrdz5XfnHs22RU5enDa86O/Lf9/x63zO2pH6kCXNHECD0EVSLaxJ6EdiJwwn9RJgNltoj9F+P9a1vmIi9QfE3iUjoAUUT+rwSCH0e+xk77xX656zEPqM1e24lQOhbSQ0cR+gD4T5Y+qPE7/QY+pmMVmdwVOKPbqfH8Ne+Zu22jQChb+M0dBShD8X71eKE/lckhL7//j0Su0fw+3maeYwAoR/jd8psQj8F4+ZFCJ3QN1+WjQOJfSMow4YSIPSheLctTujbOB0ZdVeJb2G25SfkP6/T5d38iMfsr1iS+itC/vtoAoQ+mvCG9Ql9A6SDQwj9OUBCP3i5vpj+kafH7+eytdq3CRB6wA0h9PElEDqhj79lv+9A6FeRts+XBAg94E4Q+vgSCP0cxsnv5mc8Zn9EldDPuWtWqRMg9Dqz02cQ+ulIv1qQ0M9hTOivORL6a0ZGjCFA6GO4llYl9BKuXYMJfRe2ryYR+muOhP6akRFjCBD6GK6lVQm9hGvXYELfhe3QpIr8D230n8kpP4FP6Ge0aY09BAh9D7WT5xD6yUAfLEfo4xl/uQOh+1fZrr91996R0AP6J/TxJRD6eMaE/jsB79Cvv2t2/J0AoQfcBEIfX8Kjf4Tl111THtOOJ2CHqwgQ+lWk7fMlAUIPuBOEPr4EQh/P2A7eobsDcwkQ+lz+v+1O6ONLIPTxjO1A6O7AXAKEPpc/oU/g7/P0CdBvtKVH7jcqO+yohB5QiHfo15ZA6NfyvttuhH63xnPOS+gBXRD6tSUQ+rW877Ybod+t8ZzzEnpAF4R+bQmEfi3vu+1G6HdrPOe8hB7QBaFfW8KzH5B7lMKvtV3bzQq7bRF65Q5ewcQ/83oF5fF7EPp4xi93IPSXiE4dUPliSuinor/FYoR+i5ojD0noAbUQ+rUlEPq1vO+2G6HfrfGc8xJ6QBeEfryEiqSP75azgicIOV18TvJM6B/v6HfffTc9+C+//PIwg8fv06vZHYDQd6M7byKhH2dJ6McZWuEcAoR+Dker1AkQep3Z6TMI/ThSQj/O0ArnECD0czhapU6A0OvMTp9B6PuQPpP43//+930LPpj1j3/8Y9daVz1SffbY9HNoj+R31ffVpKv/Kdhnqa+4Vx/vlMfv59yfq1Yh9KtIf2MfQt9XAqG/vRH6vrtTnUXoVWLGzyBA6DOof7Enoe8rgdAJfd/Nqc8i9DozM64nQOjXM/9qR0LfXsJHiZ/5aH17gm0j9z6q37L6q8eur961f7nHio/lRwp45r3be69e3ZmPd8Ij9y2vwswxhB7QC6FvL4HQ395efXEm9Lc3Qv/ra+rVnSH07V+DkkcSekA7hL69BEIn9C23hdAJfcs9WW0MoQc0SujbS+gi9O0nqo8847Fr9V381pRXP75/Ju6Zj8W3sho97tE92fJO3SP30c2MW5/Qx7HdvDKhb0b1Ruhvb4T+530h9OevHULf/nVllZGEHtAkoW8vgdAJ/eNtIXRC3/7VY/2RhB7QMaF/u4Qrfj0t4BoMibDl3fyWx7DfCjfq8f0WIB6tb6H0+JvAZ7175L6NaeIoQg9ohdAJfdQ1JPRRZHutW3n8Tui9uv2YltADuiN0Qh91DQl9FNle6xJ6r772piX0veROnEfo24XuEeuJF+8/S72SfuWR/MjH77o/3v3Hrj1yP84zbQVCD2iE0Al95jUk9Jn0r92b0K/lffVuhH418Qf7ETqhz7yGhD6T/rV7E/q1vK/ejdCvJk7oZeJ+Va2MrDSB0Eu4Wg8m9Nb1vQxP6C8RjR/gHbp36ONv2f4dXgn/48qVz9u3JBr5mfyW/b81puNn+oR+tPXs+YQe0A+hE3rANXwagdAfoyH05Ft7z2yEHtA7oRN6wDUk9GIJhF4EZvhwAoQ+HPHrDQid0F/fktwRlXfwW05x9mP7LXtWxnz+GKCT0D93tIWtPyxTuQ1ZYwk9oA9CJ/SAa7g7AqHvRnfZREK/DPXUjQh9Kv7fNyd0Qg+4hrsjEPpudJdNJPTLUE/diNCn4if0Lfj92toWSr3HnPFNwZbHyWdQ8sj9DIrWGEGA0EdQLa7pHbp36MUrs9xwQh9bqXfoY/mmrE7oAU0QOqEHXMOpEQh9LH5CH8s3ZXVCD2iC0Ak94Bq2j/Dsm4KzH8UnP3Lfy+DZH/D5/qcf2t+LOx2A0APaJnRCD7iG7SPslVn14IReJWb8VQQI/SrS39iH0Ak94Bq2j0Dob297GXiH3v76/3YAQg/okdC3l+An3rezuvPILX+zfC+fK96hn/EzBXvP5zH7XnLz5xH6/A78HnqhA0IvwLrxUELfXz6h72c3eyahz27AH5YpNUDoJVy3HUzo+6sn9P3sZs8k9NkNEHqpAUIv4brt4EShVx6jk+ptr+6hgxP6IXznTPYZ+naOhL6d1Z1HEvqd27/v2Qk9oHtC314CoW9ndeeRhH7n9u97dkIP6J7Qt5fwUegfZ3X6pyy3n9bIvQSuEHo1m8foVWLGVwkQepXYgPGEvh0qoW9ndeeRhH7n9u97dkIP6J7Qt5dA6NtZ3Xkkod+5/fuendADuif07SUQ+nZWdx5J6Hdu/75nJ/SA7gn9eAmPRO9z9eNcu65whdB9Jt71dqybm9ADuiX04yUQ+nGGK61A6Cu16SxbCRD6VlIDxxH6cbiEfpzhSisQ+kptOstWAoS+ldTAcYR+Ltxnn7M/2sVj+XPZp6xG6ClNyHElAUK/kvaTvQj93BII/VyeHVcj9I6tyXyUAKEfJXjCfEI/AeKHJQj9XJ4dVyP0jq3JfJQAoR8leMJ8Qj8B4s4lXsnfI/mdYCdPI/TJBdh+CgFCn4L9r5sS+rwSCH0e+5E7E/pIutZOJUDoAc0Q+rwSCH0e+5E7E/pIutZOJUDoAc0QekAJTyK8Ev6X0zyiz+jybKH/8ssvXx3MH5bJ6FqKPwkQesBtIPSAEgg9t4QdyQh9BzRT2hMg9IAKCT2gBELPLWFHMkLfAc2U9gQIPaBCQg8o4UAEf6XuALxBU88Q+qPH7L/G9ah9UGmWPUyA0A8jPL4AoR9nOHMFQp9J//HehJ7XiUTjCRD6eMYvdyD0l4iiBxB6Xj2EnteJROMJEPp4xi93IPSXiNoNqP50/KsD+un5V4Te3qoSf/ZI/dFOHrO/5m/EfAKEPr+DN0IPKOHkCIR+MtANyxH6BkiGLE2A0APqJfSAEk6OQOgnA92wHKFvgGTI0gQIPaBeQg8oITzCGd8grPjY/qPEP1b43Xff/fH/+mn18Mst3mkECP00lPsXIvT97O4yk9AfN03od3kFOOcWAoS+hdLgMYQ+GPACyxM6oS9wjR1hMAFCHwx4y/KEvoXSvccQOqHf+xXg9FsIEPoWSoPHEPpgwJb/jcAZ3xR0ROlXzjq2JvMeAoS+h9rJcwj9ZKCWe0iA0F0MBNYmQOgB/RJ6QAk3iEDoNyjZEW9NgNAD6if0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8osCr0f/34c0BqERBAAAEERhL4/qcfSssTegnXmMGEPoarVRFAAIHOBAi9YXuE3rA0kRFAAIHBBAh9MOARyxP6CKrWRAABBHoTIPSG/R0W+v/+X8NTB0T+7/96HALPa8v5sgf8r+W/dbcvenp/f98607iNBD59+vSXkYS+EVzSMEKf1AahTwL/xbaEntHDqxSE/orQ4f9O6IcRzl+A0Cd1QOiTwBN6BvhiCkIvAqsPJ/Q6s7gZhD6pEkKfBJ7QM8AXUxB6EVh9OKHXmcXNIPRJlRD6JPCEngG+mILQi8Dqwwm9zixuBqFPqoTQJ4En9AzwxRSEXgRWH07odWZxMwh9UiWEPgk8oWeAL6Yg9CKw+nBCrzOLm0Hokyoh9EngCT0DfDEFoReB1YcTep1Z3AxCn1QJoU8CT+gZ4IspCL0IrD6c0OvM4mYQ+qRKCH0SeELPAF9MQehFYPXhhF5nFjeD0CdVQuiTwBN6BvhiCkIvAqsPJ/Q6s7gZhD6pEkKfBJ7QM8AXUxB6EVh9OKHXmcXNIPRJlRD6JPCEngG+mILQi8Dqwwm9zixuBqFPqoTQJ4En9AzwxRSEXgRWH07odWZxMwh9UiWEPgk8oWeAL6Yg9CKw+nBCrzOLm0Hokyoh9EngCT0DfDEFoReB1YcTep1Z3AxCn1QJoU8CT+gZ4IspCL0IrD6c0OvM4mYQ+qRKCH0SeELPAF9MQehFYPXhhF5nFjeD0CdVQuiTwBN6BvhiCkIvAqsPJ/Q6s7gZhD6pEkKfBJ7QM8AXUxB6EVh9OKHXmcXNIPRJlRD6JPCEngG+mILQi8Dqwwm9zixuBqFPqoTQJ4En9AzwxRSEXgRWH07odWZxMwh9UiWEPgk8oWeAL6Yg9CKw+nBCrzOLm0Hokyoh9EngCT0DfDEFoReB1YcTep1Z3AxCn1QJoU8CT+gZ4IspCL0IrD6c0OvM4mYQ+qRKCH0SeELPAF9MQehFYPXhhF5nFjeD0CdVQuiTwBN6BvhiCkIvAqsPJ/Q6s7gZhD6pEkKfBJ7QM8AXUxB6EVh9OKHXmcXNIPRJlRD6JPCEngG+mILQi8Dqwwm9zixuBqFPqoTQJ4En9AzwxRSEXgRWH07odWZxMwh9UiWEPgk8oWeAL6Yg9CKw+nBCrzOLm0Hokyoh9EngCT0DfDEFoReB1YcTep1Z3AxCn1QJoU8CT+gZ4IspCL0IrD6c0OvM4mYQ+qRKCH0SeELPAF9MQehFYPXhhF5nFjeD0CdVQuiTwBN6BvhiCkIvAqsPJ/Q6s7gZh4UedyKBEEAAAQSOEvj+px9KS/zzh59K42cM/vT+/v4+Y+Or9iT0q0jbBwEEEOhDgND7dPVHUkJvWJrICCCAwGAChD4Y8IjlCX0EVWsigAACvQkQesP+qkJveESREUAAAQQGE/AZ+mDAW5Yn9C2UjEEAAQQQ+BYBQg+4H4QeUIIICCCAQHMChB5QIKEHlCACAggg0JwAoQcUSOgBJYiAAAIINCdA6AEFEnpACSIggAACzQkQekCBhB5QgggIIIBAcwKEHlAgoQeUIAICCCDQnAChBxRI6AEliIAAAgg0J0DoAQUSekAJIiCAAALNCRB6QIGEHlCCCAgggEBzAoQeUCChB5QgAgIIINCcAKEHFEjoASWIgAACCDQnQOgBBRJ6QAkiIIAAAs0JEHpAgYQeUIIICCCAQHMChB5QIKEHlCACAggg0JwAoQcUSOgBJYiAAAIINCdA6AEFEnpACSIggAACzQkQekCBhB5QgggIIIBAcwKEHlAgoQeUIAICCCDQnAChBxRI6AEliIAAAgg0J0DoAQUSekAJIiCAAALNCRB6QIGEHlCCCAgggEBzAoQeUCChB5QgAgIIINCcAKEHFEjoASWIgAACCDQnQOgBBRJ6QAkiIIAAAs0JEHpAgYQeUIIICCCAQHMChB5QIKEHlCACAggg0JwAoQcUSOgBJYiAAAIINCdA6AEFEnpACSIggAACzQkQekCBhB5QgggIIIBAcwKEHlAgoQeUIAICCCDQnAChBxRI6AEliIAAAgg0J0DoAQUSekAJIiCAAALNCRB6QIGEHlCCCAgggEBzAoQeUCChB5QgAgIIINCcAKEHFEjoASWIgAACCDQnQOgBBRJ6QAkiIIAAAs0JEHpAgYQeUIIICCCAQHMChB5QIKEHlCACAggg0JwAoQcUSOgBJYiAAAIINCdA6AEFEnpACSIggAACzQkQekCBhB5QgggIIIBAcwKEHlAgoQeUIAICCCDQnAChBxRI6AEliIAAAgg0J0DoAQUSekAJIiCAAALNCRB6QIGEHlCCCAgggEBzAoQeUCChB5QgAgIIINCcAKEHFEjoASWIgAACCDQnQOgBBRJ6QAkiIIAAAs0JEHpAgYQeUIIICCCAQHMChB5QIKEHlCACAggg0JwAoQcUSOgBJYiAAAIINCdA6AEFEnpACSIggAACzQkQekCBhB5QgggIIIBAcwKEHlAgoQeUIAICCCDQnAChBxRI6AEliIAAAgg0J0DoAQUSekAJIiCAAALNCRB6QIGEHlCCCAgggEBzAoQeUCChB5QgAgIIINCcAKEHFEjoASWIgAACCDQnQOgBBRJ6QAkiIIAAAs0JEHpAgYT+9vavH39+2MT3P/0wpaFneZ6FOSvn6H2r65913tH7nnV/Rq9zFs8pLwqbxhMg9ICKCJ3QP1/Dqviq30hU1z9LQKP3HS3i0Zyr6wd82RIhkAChB5RC6IRO6NteiM/ER+jb+Bm1NgFCD+iX0Amd0Le9EAl9Gyej7kmA0AN6J3RCJ/RtL0RC38bJqHsSIPSA3gmd0Al92wuR0LdxMuqeBAg9oHdCnyf0WZ+93m3fZy+z0RyqP9RXzVMdH/DlRoSFCRB6QLmETuifr2H1p52rQqmOnyXiszgQesAXOBEuI0Dol6F+vhGhEzqh//X1QegBX5hEaEeA0AMqI3RCJ3RCD/hSJEJzAoQeUCChEzqhE3rAlyIRmhMg9IACCZ3QCZ3QA74UidCcAKEHFEjohH72Nez+611nfYZe5TDrh+iq+559X6y3BgFCD+iR0An97GtYFdlogT4736yfuq/+bfmzeJ6179n3xXprECD0gB4JndDPvoZnCShNxGflOUus1W9Iztr37PtivTUIEHpAj4RO6GdfQ0L/nWiVQ/XRN6GffXOtd4QAoR+hd9JcQif0k67SH8tUReaR+7cbOIund+hn33TrfSRA6AH3gdAJ/exreJaAznrEnbbOWWL1Dv3sm2u9IwQI/Qi9k+YSOqF/vkqj3ylXBZQm4rQ8aTxP+pJkmaYECD2gOEIndEL/6wtx9Dc2Z31jQOgBX0BF+IMAoQdcBkIndEIn9Edfiqrf2AR8ORNhIgFCnwj/89aETuiETuiEHvDFuHkEQg8okNAJndAJndADvhg3j0DoAQUSOqETOqETesAX4+YRCD2gQEJ/LvSz6ql+FnnWrzWd9cNXZ61TPdezfUfzrO6b9sNpVc5Vnmfdh+rr6yzOs/JXz9ttPKEHNEbohD77HXr1ZVAVUFVwhF5t5Pfx3YU7Ov8+qn1mEXpAV4RO6IS+7YXY/Q/mVL9R2Ublz1Gjhdh9/SrPbuMJPaAxQid0Qt/2QiT0b3PqLtzR+bfdsr6jCD2gO0IndELf9kIkdELfdidNJ+EAAAYwSURBVFPuOYrQA3ondEIn9G0vREIn9G035Z6jCD2gd0IPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEmhMg9IACCT2gBBEQQACB5gQIPaBAQg8oQQQEEECgOQFCDyiQ0ANKEAEBBBBoToDQAwok9IASREAAAQSaEyD0gAIJPaAEERBAAIHmBAg9oEBCDyhBBAQQQKA5AUIPKJDQA0oQAQEEEGhOgNADCiT0gBJEQAABBJoTIPSAAgk9oAQREEAAgeYECD2gQEIPKEEEBBBAoDkBQg8okNADShABAQQQaE6A0AMKJPSAEkRAAAEEGhLoIPGPWD+9v7+/N+QsMgIIIIAAAgh8IEDorgMCCCCAAAILECD0BUp0BAQQQAABBAjdHUAAAQQQQGABAoS+QImOgAACCCCAAKG7AwgggAACCCxAgNAXKNEREEAAAQQQIHR3AAEEEEAAgQUIEPoCJToCAggggAAChO4OIIAAAgggsAABQl+gREdAAAEEEECA0N0BBBBAAAEEFiBA6AuU6AgIIIAAAggQujuAAAIIIIDAAgQIfYESHQEBBBBAAAFCdwcQQAABBBBYgAChL1CiIyCAAAIIIEDo7gACCCCAAAILECD0BUp0BAQQQAABBAjdHUAAAQQQQGABAoS+QImOgAACCCCAAKG7AwgggAACCCxAgNAXKNEREEAAAQQQIHR3AAEEEEAAgQUIEPoCJToCAggggAAChO4OIIAAAgggsAABQl+gREdAAAEEEECA0N0BBBBAAAEEFiBA6AuU6AgIIIAAAggQujuAAAIIIIDAAgQIfYESHQEBBBBAAAFCdwcQQAABBBBYgAChL1CiIyCAAAIIIEDo7gACCCCAAAILECD0BUp0BAQQQAABBAjdHUAAAQQQQGABAoS+QImOgAACCCCAAKG7AwgggAACCCxAgNAXKNEREEAAAQQQIHR3AAEEEEAAgQUIEPoCJToCAggggAAChO4OIIAAAgggsAABQl+gREdAAAEEEECA0N0BBBBAAAEEFiBA6AuU6AgIIIAAAggQujuAAAIIIIDAAgQIfYESHQEBBBBAAAFCdwcQQAABBBBYgAChL1CiIyCAAAIIIEDo7gACCCCAAAILECD0BUp0BAQQQAABBAjdHUAAAQQQQGABAv8PfOqkb+xBgrMAAAAASUVORK5CYII=', 'data:text/html;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAgAElEQVR4Xu3dTZIkt5VF4eRSNO9V0HrINiMXo1VoMaSZNGzjKnqupVQbSRUZLEZUAHCH4z7417MW8XNxLiJPhkdm1jefPn369OH/EEAAAQQQQKA0gW8IvXR/wiOAAAIIIPArAUJ3ERBAAAEEENiAAKFvUKIjIIAAAgggQOjuAAIIIIAAAhsQIPQNSnQEBBBAAAEECN0dQAABBBBAYAMChL5BiY6AAAIIIIAAobsDCCCAAAIIbECA0Dco0REQQAABBBAgdHcAAQQQQACBDQgQ+gYlOgICCCCAAAKE7g4ggAACCCCwAQFC36BER0AAAQQQQIDQ3QEEEEAAAQQ2IEDoG5ToCAgggAACCBC6O4AAAggggMAGBAh9gxIdAQEEEEAAAUJ3BxBAAAEEENiAAKFvUKIjIIAAAgggQOjuAAIIIIAAAhsQIPQNSnQEBBBAAAEECN0dQAABBBBAYAMChL5BiY6AAAIIIIAAobsDCCCAAAIIbECA0Dco0REQQAABBBAgdHcAAQQQQACBDQgQ+gYlOgICCCCAAAKE7g4ggAACCCCwAQFC36BER0AAAQQQQIDQ3QEEEEAAAQQ2IEDoG5ToCAgggAACCBC6O4AAAggggMAGBAh9gxIdAQEEEEAAAUJ3BxBAAAEEENiAAKFvUKIjIIAAAgggQOjuAAIIIIAAAhsQIPQNSnQEBBBAAAEECN0dQAABBBBAYAMChL5BiY6AAAIIIIAAobsDCCCAAAIIbECA0Dco0REQQAABBBAgdHcAAQQQQACBDQgQ+gYlOgICCCCAAAKE7g4ggAACCCCwAYHthf4/P/2wQU2OgAACCCCwgsA/v/9xxbZDexL6EDaTEEAAAQTuRKCC2An9TjfSWRFAAAEEhggQ+hC2cyd55H4uT6shgAACdyRA6AGtE3pACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAwBcE/vXDT81Mvvvx++axBiIwiwChzyLbsS6hd8AyFIGLCBD6RaBtcxoBQj8N5fhChD7OzkwEZhEg9FlkrTuLAKHPItuxLqF3wDIUgYsIEPpFoG1zGgFCPw3l+EKEPs7OTASOEmgR99/+9re/bPPvf//79//NZ+hHWzD/DAKEfgbFg2sQ+kGApiNwgAChH4BnahQBQg+og9ADShDhtgQI/bbVb3dwQg+olNADShDho0VsszBd9cj62Rk/vTjUNw//+7tH7o9LXHWWWV1Yty4BQg/ojtADShCB0L+4A4TuRVGNAKEHNEboASWIQOiE7lVQnAChBxRI6AEl3CxCz6PnmWge3wU/2+fV4+tn+R/Hvvr44NXj9Wd7v8r27vG7R+4zb4y1v0aA0APuB6EHlHCzCIT+vnBCf8/IiCwChB7QB6EHlHCzCIT+vnBCf8/IiCwChB7QB6EHlHCDCI8S73n0vBLNu0fyr7Kdfb6eHB65r7wx996b0AP6J/SAEm4QgdDHSyb0cXZmXkeA0K9j/XInQg8o4QYRCH28ZEIfZ2fmdQQI/TrWhB7A+m4RKkr8saPPIj37EXrrPXglco/VWwkadyUBQr+S9ou9vEMPKGHTCIR+rFhCP8bP7GsJEPq1vJ/uRugBJWwagdCPFUvox/iZfS0BQr+WN6EH8N49QnWJr3rk/kzeHq3v/mrZ63yEHtCnd+gBJWwUgdDHyiT0MW5m5RAg9IAuCD2ghI0iEPpYmYQ+xs2sHAKEHtAFoQeUUDzCThKf/cjd5+LFL7v4LwkQesDlIPSAEopHIPT2Agm9nZWRtQgQekBfhB5QQvEIhN5eIKG3szKyFgFCD+iL0ANKKB6B0J8X6HPx4hdb/C4ChN6Fa85gQp/D9U6rEjqh3+m+O+tzAoQecDMIPaCE4hF2F/ov9Yz8+Vfv0ItfbPG7CBB6F645gwl9Dtc7rbqr0H/p8FHKvVL/PNcfiLnTq+G+ZyX0gO4JPaCE4hF2FvoRqT9+M0DqxS+5+G8JEPpbRPMHEPp8xrvvsLvQSX33G+x8ZxAg9DMoHlyD0A8CNP3jDkIndRcdga8TIPSAG0LoASVMjPAo24nb/L507+fMV2Q6c4/Rz9Q9fj+zBWslEiD0gFYIPaCEiREI/Xy4r/44TM9OPlPvoWVsBQKEHtASoQeUMDECoc+BS+pzuFq1LgFCD+iO0ANKOBDhnbCveAQ++hj6wLFLT/X4vXR9wr8gQOgBV4PQA0o4EIHQD8BbONXvqC+Eb+spBAh9Cta+RQm9j1faaEJPa6QtD6G3cTKqDgFCD+iK0ANKaIjwTty/LHHF4/VnUT1ybyjwiyGE3s/MjGwChB7QD6EHlNAQgdAbIBUaQuiFyhK1iQChN2GaO4jQ5/I9a3VCP4tkxjqEntGDFOcRIPTzWA6vROjD6KZPfCXxVY/WXx3YI/f+q0Do/czMyCZA6AH9EHpACS8iEHpuN0eTEfpRguanESD0gEYIPaAEQs8tYVIyQp8E1rLLCBD6MvR/bEzoASV8fPzpHzj5nCjt0bpH7ufdFUI/j6WVMggQekAPhB5QAqFnlHBhip4/Hevvvl9YjK2GCRD6MLrzJhL6eSyPrPTs83Lv0I8QzZ7bI/THk5B7dq93TkfoAe0TekAJ3qFnlHBhilGh/xKR1C8sylbNBAi9GdW8gYQ+j+27lSu/K388W4ucqjxteNfZkf8+8ut9r9iS+pEmzJ1BgNBnUO1ck9A7gZ04nNBPhFlgqRGh/3Ksr33DROwFir9JREIPKJrQ15VA6OvYr9h5VOifsxL7itbs2UqA0FtJTRxH6BPhPln6UeJ3egz9Ska7Mzgq8We302P4a1+zdmsjQOhtnKaOIvSpeP+yOKH/GQmhj9+/Z2L3CH6cp5nHCBD6MX6nzCb0UzA2L0LohN58WRoHEnsjKMOmEiD0qXjbFif0Nk5HRt1V4i3MWn5C/vM6Vd7Nz3jM/o4lqb8j5L/PJkDoswk3rE/oDZAODiH01wAJ/eDl+mL6I0+P389la7WvEyD0gBtC6PNLIHRCn3/LftuB0K8ibZ8vCRB6wJ0g9PklEPo5jJPfza94zP6MKqGfc9es0k+A0PuZnT6D0E9H+pcFCf0cxoT+niOhv2dkxBwChD6Ha9eqhN6Fa2gwoQ9h+8skQn/PkdDfMzJiDgFCn8O1a1VC78I1NJjQh7AdmtQj/0Mb/Wdyyk/gE/oZbVpjhAChj1A7eQ6hnwz0yXKEPp/xlzsQun+V7fpbd+8dCT2gf0KfXwKhz2dM6L8R8A79+rtmx98IEHrATSD0+SU8+0dYftk15THtfAJ2uIoAoV9F2j5fEiD0gDtB6PNLIPT5jO3gHbo7sJYAoa/l/+vuhD6/BEKfz9gOhO4OrCVA6Gv5E/oC/j5PXwD9Rlt65H6jssOOSugBhXiHfm0JhH4t77vtRuh3azznvIQe0AWhX1sCoV/L+267EfrdGs85L6EHdEHo15ZA6NfyvttuhH63xnPOS+gBXRD6tSW8+gG5Zyn8Wtu13eywW4vQe+7gFUz8M69XUJ6/B6HPZ/x2B0J/i+jUAT1fTAn9VPS3WIzQb1Fz5CEJPaAWQr+2BEK/lvfddiP0uzWec15CD+iC0I+X0CPp47vlrOAJQk4Xn5O8EvrjHf3222+XB//555+fZvD4fXk1wwEIfRjdeRMJ/ThLQj/O0ArnECD0czhapZ8AofczO30GoR9HSujHGVrhHAKEfg5Hq/QTIPR+ZqfPIPQxpK8k/ve//31swSez/vGPfwytddUj1VePTT+H9kh+qL6/TLr6n4J9lfqKe/V4pzx+P+f+XLUKoV9F+iv7EPpYCYT+8UHoY3endxah9xIzfgUBQl9B/Ys9CX2sBEIn9LGb0z+L0PuZmXE9AUK/nvlfdiT09hIeJX7mo/X2BG0jRx/Vt6z+7rHru3ftX+6x42P5mQJeee9G79W7O/N4Jzxyb3kVZo4h9IBeCL29BEL/+Hj3xZnQPz4I/c+vqXd3htDbvwYljyT0gHYIvb0EQif0lttC6ITeck92G0PoAY0SensJVYTefqL+kWc8du19F9+a8urH96/EvfKxeCur2eOe3ZOWd+oeuc9uZt76hD6PbfPKhN6M6oPQPz4I/Y/7QuivXzuE3v51ZZeRhB7QJKG3l0DohP54Wwid0Nu/euw/ktADOib0r5dwxa+nBVyDKRFa3s23PIb9WrhZj+9bgHi03kLp+TeBr3r3yL2NaeIoQg9ohdAJfdY1JPRZZGut2/P4ndBrdfuYltADuiN0Qp91DQl9Ftla6xJ6rb5G0xL6KLkT5xF6u9A9Yj3x4v1nqXfS73kkP/Pxu+6Pd//YtUfux3mmrUDoAY0QOqGvvIaEvpL+tXsT+rW8r96N0K8m/mQ/Qif0ldeQ0FfSv3ZvQr+W99W7EfrVxAm9m7hfVetG1jWB0LtwlR5M6KXrexue0N8imj/AO3Tv0OffsvEd3gn/ceWez9tbEs38TL5l/6+NqfiZPqEfbT17PqEH9EPohB5wDV9GIPTnaAg9+dbeMxuhB/RO6IQecA0JvbMEQu8EZvh0AoQ+HfH7DQid0N/fktwRPe/gW05x9mP7lj17xnz+GKCS0D931MLWH5bpuQ1ZYwk9oA9CJ/SAazgcgdCH0V02kdAvQ710I0Jfiv+3zQmd0AOu4XAEQh9Gd9lEQr8M9dKNCH0pfkJvwe/X1loo1R5zxjcFLY+Tz6DkkfsZFK0xgwChz6DauaZ36N6hd16Z7YYT+txKvUOfyzdldUIPaILQCT3gGi6NQOhz8RP6XL4pqxN6QBOETugB17B8hFffFJz9KD75kfsog1d/wOe7H78vfy/udABCD2ib0Ak94BqWjzAqs96DE3ovMeOvIkDoV5H+yj6ETugB17B8BEL/+Bhl4B16+ev/6wEIPaBHQm8vwU+8t7O688iWv1k+yueKd+hn/EzB6Pk8Zh8lt34eoa/vwO+hd3RA6B2wbjyU0MfLJ/RxdqtnEvrqBvxhma4GCL0L120HE/p49YQ+zm71TEJf3QChdzVA6F24bjs4Ueg9j9FJ9bZX99DBCf0QvnMm+wy9nSOht7O680hCv3P79z07oQd0T+jtJRB6O6s7jyT0O7d/37MTekD3hN5ewqPQH2dV+qcs209r5CiBK4Tem81j9F5ixvcSIPReYhPGE3o7VEJvZ3XnkYR+5/bve3ZCD+ie0NtLIPR2VnceSeh3bv++Zyf0gO4Jvb0EQm9ndeeRhH7n9u97dkIP6J7Qj5fwTPQ+Vz/OteoKVwjdZ+JVb8e+uQk9oFtCP14CoR9nuNMKhL5Tm87SSoDQW0lNHEfox+ES+nGGO61A6Du16SytBAi9ldTEcYR+LtxXn7M/28Vj+XPZp6xG6ClNyHElAUK/kvaLvQj93BII/VyeFVcj9IqtyXyUAKEfJXjCfEI/AeLDEoR+Ls+KqxF6xdZkPkqA0I8SPGE+oZ8AcXCJd/L3SH4Q7OJphL64ANsvIUDoS7D/eVNCX1cCoa9jP3NnQp9J19qpBAg9oBlCX1cCoa9jP3NnQp9J19qpBAg9oBlCDyjhRYR3wv9ymkf0GV2eLfSff/75Lwfzh2UyupbiDwKEHnAbCD2gBELPLWEgGaEPQDOlPAFCD6iQ0ANKIPTcEgaSEfoANFPKEyD0gAoJPaCEAxH8lboD8CZNPUPozx6z/xLXo/ZJpVn2MAFCP4zw+AKEfpzhyhUIfSX953sTel4nEs0nQOjzGb/dgdDfIooeQOh59RB6XicSzSdA6PMZv92B0N8iKjeg96fj3x3QT8+/I/Tx0SvxV4/Un+3kMft7/kasJ0Do6zv4IPSAEk6OQOgnA21YjtAbIBmyNQFCD6iX0ANKODkCoZ8MtGE5Qm+AZMjWBAg9oF5CDyghPMIZ3yDs+Nj+UeKPFX777be//79+Wj38cot3GgFCPw3l+EKEPs7uLjMJ/XnThH6XV4BzthAg9BZKk8cQ+mTAGyxP6IS+wTV2hMkECH0y4JblCb2F0r3HEDqh3/sV4PQtBAi9hdLkMYQ+GbDlfyVwxjcFFVH6lbOKrck8QoDQR6idPIfQTwZquacECN3FQGBvAoQe0C+hB5RwgwiEfoOSHfHWBAg9oH5CDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAnuF/q8ffgpILQICCCCAwEwC3/34fdfyhN6Fa85gQp/D1aoIIIBAZQKEXrA9Qi9YmsgIIIDAZAKEPhnwjOUJfQZVayKAAAK1CRB6wf4OC/1//6/gqQMi//d/PQ+B57XlfNkD/tfyb91NT62kxsd9wZjQx1Eum0noi9AT+iLwX2xLFBk9vEuhp3eEjv93Qj/OcPUKhL6oAUJfBJ7QM8B3piD0TmADwwl9AFrYFEJfVAihLwJP6BngO1MQeiewgeGEPgAtbAqhLyqE0BeBJ/QM8J0pCL0T2MBwQh+AFjaF0BcVQuiLwBN6BvjOFITeCWxgOKEPQAubQuiLCiH0ReAJPQN8ZwpC7wQ2MJzQB6CFTSH0RYUQ+iLwhJ4BvjMFoXcCGxhO6APQwqYQ+qJCCH0ReELPAN+ZgtA7gQ0MJ/QBaGFTCH1RIYS+CDyhZ4DvTEHoncAGhhP6ALSwKYS+qBBCXwSe0DPAd6Yg9E5gA8MJfQBa2BRCX1QIoS8CT+gZ4DtTEHonsIHhhD4ALWwKoS8qhNAXgSf0DPCdKQi9E9jAcEIfgBY2hdAXFULoi8ATegb4zhSE3glsYDihD0ALm0Loiwoh9EXgCT0DfGcKQu8ENjCc0AeghU0h9EWFEPoi8ISeAb4zBaF3AhsYTugD0MKmEPqiQgh9EXhCzwDfmYLQO4ENDCf0AWhhUwh9USGEvgg8oWeA70xB6J3ABoYT+gC0sCmEvqgQQl8EntAzwHemIPROYAPDCX0AWtgUQl9UCKEvAk/oGeA7UxB6J7CB4YQ+AC1sCqEvKoTQF4En9AzwnSkIvRPYwHBCH4AWNoXQFxVC6IvAE3oG+M4UhN4JbGA4oQ9AC5tC6IsKIfRF4Ak9A3xnCkLvBDYwnNAHoIVNIfRFhRD6IvCEngG+MwWhdwIbGE7oA9DCphD6okIIfRF4Qs8A35mC0DuBDQwn9AFoYVMIfVEhhL4IPKFngO9MQeidwAaGE/oAtLAphL6oEEJfBJ7QM8B3piD0TmADwwl9AFrYFEJfVAihLwJP6BngO1MQeiewgeGEPgAtbAqhLyqE0BeBJ/QM8J0pCL0T2MBwQh+AFjaF0BcVQuiLwBN6BvjOFITeCWxgOKEPQAubQuiLCiH0ReAJPQN8ZwpC7wQ2MJzQB6CFTSH0RYUQ+iLwhJ4BvjMFoXcCGxhO6APQwqYQ+qJCCH0ReELPAN+ZgtA7gQ0MJ/QBaGFTCH1RIYS+CDyhZ4DvTEHoncAGhhP6ALSwKYS+qBBCXwSe0DPAd6Yg9E5gA8MJfQBa2JTDQg87jzgIIIAAAscJfPfj912L/PP7H7vGrxj8zadPnz6t2PiqPQn9KtL2QQABBOoQIPQ6Xf2elNALliYyAgggMJkAoU8GPGN5Qp9B1ZoIIIBAbQKEXrC/XqEXPKLICCCAAAKTCfgMfTLgluUJvYWSMQgggAACXyNA6AH3g9ADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNADShABAQQQKE6A0AMKJPSAEkRAAAEEihMg9IACCT2gBBEQQACB4gQIPaBAQg8oQQQEEECgOAFCDyiQ0ANKEAEBBBAoToDQAwok9IASREAAAQSKEyD0gAIJPaAEERBAAIHiBAg9oEBCDyhBBAQQQKA4AUIPKJDQA0oQAQEEEChOgNADCiT0gBJEQAABBIoTIPSAAgk9oAQREEAAgeIECD2gQEIPKEEEBBBAoDgBQg8okNA/Pv71w09Pm/jux++XNPQqz6swZ+WcvW/v+medd/a+Z92f2eucxXPJi8Km8QQIPaAiQif0z9ewV3y930j0rn+WgGbvO1vEszn3rh/wZUuEQAKEHlAKoRM6obe9EF+Jj9Db+Bm1NwFCD+iX0Amd0NteiITexsmoexIg9IDeCZ3QCb3thUjobZyMuicBQg/ondAJndDbXoiE3sbJqHsSIPSA3gl9ndBXffZ6t31fvcxmc+j9ob7ePL3jA77ciLAxAUIPKJfQCf3zNez9aedeofSOXyXiszgQesAXOBEuI0Dol6F+vRGhEzqh//n1QegBX5hEKEeA0AMqI3RCJ3RCD/hSJEJxAoQeUCChEzqhE3rAlyIRihMg9IACCZ3QCZ3QA74UiVCcAKEHFEjohH72Naz+611nfYbey2HVD9H17nv2fbHeHgQIPaBHQif0s69hr8hmC/TV+Vb91H3v35Y/i+dZ+559X6y3BwFCD+iR0An97Gt4loDSRHxWnrPE2vsNyVn7nn1frLcHAUIP6JHQCf3sa0jovxHt5dD76JvQz7651jtCgNCP0DtpLqET+klX6fdlekXmkfvXGziLp3foZ9906z0SIPSA+0DohH72NTxLQGc94k5b5yyxeod+9s213hEChH6E3klzCZ3QP1+l2e+UewWUJuK0PGk8T/qSZJmiBAg9oDhCJ3RC//MLcfY3Nmd9Y0DoAV9ARfidAKEHXAZCJ3RCJ/RnX4p6v7EJ+HImwkIChL4Q/uetCZ3QCZ3QCT3gi3HxCIQeUCChEzqhEzqhB3wxLh6B0AMKJHRCJ3RCJ/SAL8bFIxB6QIGE/lroZ9XT+1nkWb/WdNYPX521Tu+5Xu07m2fvvmk/nNbLuZfnWfeh9/V1FudV+XvPW208oQc0RuiEvvodeu/LoFdAvYIj9N5GfhtfXbiz849RrTOL0AO6InRCJ/S2F2L1P5jT+41KG5U/Rs0WYvX1e3lWG0/oAY0ROqETetsLkdC/zqm6cGfnb7tldUcRekB3hE7ohN72QiR0Qm+7KfccRdEXHIAAAAYsSURBVOgBvRM6oRN62wuR0Am97abccxShB/RO6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBxAoQeUCChB5QgAgIIIFCcAKEHFEjoASWIgAACCBQnQOgBBRJ6QAkiIIAAAsUJEHpAgYQeUIIICCCAQHEChB5QIKEHlCACAgggUJwAoQcUSOgBJYiAAAIIFCdA6AEFEnpACSIggAACxQkQekCBhB5QgggIIIBAcQKEHlAgoQeUIAICCCBQnAChBxRI6AEliIAAAggUJ0DoAQUSekAJIiCAAALFCRB6QIGEHlCCCAgggEBBAhUk/oj1m0+fPn0qyFlkBBBAAAEEEHggQOiuAwIIIIAAAhsQIPQNSnQEBBBAAAEECN0dQAABBBBAYAMChL5BiY6AAAIIIIAAobsDCCCAAAIIbECA0Dco0REQQAABBBAgdHcAAQQQQACBDQgQ+gYlOgICCCCAAAKE7g4ggAACCCCwAQFC36BER0AAAQQQQIDQ3QEEEEAAAQQ2IEDoG5ToCAgggAACCBC6O4AAAggggMAGBAh9gxIdAQEEEEAAAUJ3BxBAAAEEENiAAKFvUKIjIIAAAgggQOjuAAIIIIAAAhsQIPQNSnQEBBBAAAEECN0dQAABBBBAYAMChL5BiY6AAAIIIIAAobsDCCCAAAIIbECA0Dco0REQQAABBBAgdHcAAQQQQACBDQgQ+gYlOgICCCCAAAKE7g4ggAACCCCwAQFC36BER0AAAQQQQIDQ3QEEEEAAAQQ2IEDoG5ToCAgggAACCBC6O4AAAggggMAGBAh9gxIdAQEEEEAAAUJ3BxBAAAEEENiAAKFvUKIjIIAAAgggQOjuAAIIIIAAAhsQIPQNSnQEBBBAAAEECN0dQAABBBBAYAMChL5BiY6AAAIIIIAAobsDCCCAAAIIbECA0Dco0REQQAABBBAgdHcAAQQQQACBDQgQ+gYlOgICCCCAAAKE7g4ggAACCCCwAQFC36BER0AAAQQQQIDQ3QEEEEAAAQQ2IEDoG5ToCAgggAACCBC6O4AAAggggMAGBAh9gxIdAQEEEEAAAUJ3BxBAAAEEENiAAKFvUKIjIIAAAgggQOjuAAIIIIAAAhsQIPQNSnQEBBBAAAEECN0dQAABBBBAYAMC/w/QxYFvBKcJvQAAAABJRU5ErkJggg==', ],
  }
  const boxOpenOpt = {
    totalFrames: 40,
    frameSpeed: 30,
    frameImages: ['data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAD6AAAABkCAYAAACSTs9JAAAAAXNSR0IArs4c6QAAIABJREFUeF7svQfUbstZ3/e991yl15XevUjiljhxoRpswHSwAdGrRRcWEuoVgXrvFQkQEkJCIJBAQgghkIzoQhQbbLBNgBASEkhCCAkh0T3nnqw5r+b75p1vZj9lnmdm3tn/by2to/fuafv5zVOm7sO99957+wJ/y0vgwZ/+cUu+44ve+q6zfK+cx5/5oA+6+O9/67eq7xKej/ij2pQ/X4lHSd6UPCwZaeraG49c3hqZ1ZhZlLV3Hi18LORP1b+6vZLaoy2Zp2VZ+SPoB49QjYsVh9gK8LjOg2OHOGl4pE9TgcdRHi3ybckL/3ElAUs51uyN1MfsTT88GFjGv3vh0ZMDZYO2dGZ1HiM5aLisymMrtrGOUaVxFHdME8pdZTxYktFoDhJ9iWlX5iHtxzOkB48ZKFy14Vx5zCVFtAYSgAQgAUgAEoAEIAFIABKABCABSAASgAQgAUgAEoAEIAFIABKABCABSAASgAQ8JYDzap7SlZcNHnKZeeYonR+k6vPYK9da5ir74SjZ43lZAoebN29eHkA/HA7TyWnGNnkK6fZtn/sAHvIZH+/Z7GFle2/AmoHH6I2jM23gvffeey/7mqVtyPWjJPNWZ6tREmmdvR36rVu3Ll/rrrvu0rxiMQ+HR55RKitOY1vL7M1jpH5Q8tTIUpNnqx3gUZeOxM6HUiz8EnhQWnN8vuoB9JntVUpGqhsxb6uOQD+uKGh8gSYP/IfOR0j7ugWbPeqHhdwor6OtY088tDKiZC+xP2lazpjVe75khD8fwUE79lxZP2bgIOWyKo+aDZH66BZbpbVjab5V7FUui1k4SPQlpF2Vh1c/9y73XHlYycVyDcCqTZ7leMVXVm0GDytJ2pSzZx42ErQtxXKNzLZlPqXBXvnIVVuq134GbXvyfHuzV+Bh1XNsyoG9spGjVSngYSVJm3LAw0aOVqWAh5UkbcpJediUaFvK3sYfXvsTragg3rWSpE05e+MB/2HTb6xKwXjQSpI25UA/bORoVQp4WEnSphwvHtR5Nayfl/mBx6lcqH3bPdfP77rL7nztgz/99DwnVx8s92hZlLXK/isba3pxsbfxx+H973//7fDSIfC1/jdCCZMeaWAd64nPR/2OsEfVX3p/rwmsh3/WJ1npyFTlvPAtP+banjCBZa0XobyH3fcTWe3mOhZWYQ2JKEcei+7BI9iToCeW/z70Mz/hRDq53C2crVb8krrztC/4gR+97L8e9tZrwiTXD0oPJDKScmgpO8/7/O9/52W/Tf1javdjv9Y89+KR+w+KRy5jSxmmZbe2w1s/rO1ULI+yV5Z9fKssqfypftGDRw9/3iqXVE6U7/Wsy5tHiK9u3LhxYf2vtX4EHhSHvG9bcOntz4N9mVk/JH5EkpZrL8Gj7Wv0Uc5WbPbKw0p+pX7fUvbeeLTIimtzWnRmLzx6cqBi2K3xyeo8RnLQcFmVh+eYTWq3JFzytN7jj17xbklmFuODVhaScWZIuzIPS1n2Kgs8ekmaV483D6/53fTtZloPDe1K5yPS37HNI9vr5T9Szlg/P+5P4PCGfvDsVK9U0I8r+8Xpv972rQePMJ9vtZ4Z5NGyHprmn/Hwldf+ktQOggff2nntv0J8xfPfuf3rYa8QX/HjK/DYnz+Hfsj0w3JfYrp/KPqQGeMYvoftm7KHvZr90IBV/Jz2P21832N8HvcTxfbO8ju0Y7a/HuMP+A+Z//DYf4XxoE7zoB+Id0euL8y+/uHlzznno7B+frRpaf/0infBQzd/1Us/JLpgsUfLoozQd0v7r8JB/XvvvZK35W8v/cB6rU4/Dn/6p3/K+uT2rIPs1dqV3ti3FTJLJ6Ee8dmfrIvAJ8/13De9g9VCbT/h8pCW/8jP+RSy3RKnQhZmkIBzAOs53/fDrJqk/TcW2otHKnsrZ8sSTCURtw15uue9+UdaqiXzjuCRN4orG/JlNhJo68jzce2VVj9u3rzJes1WeyWxTVrZpS9iUUYp4OXykMprpL1idYAkEUe2nDTSemfmIdW/PL6S6MeW3Dg+N+T3qs9bP7j2Ssojj6+85FNj51Ufl4dGF0Me7gYsqT205MGxRZw0GhntMb4qyalFvi15qdiPqx/S/hvr5eqHtG9p9MNSjrG9rWVq410tD4w/tn3/nni09l2pzpbi17QMzhfpve3VCP0YwYHyCzUuK+vHDBykXFbl4T1O0Ngu7vxKmm6V+cRWec2SHzxmIXFsx7n6815S1Mb53u2Tzi/F9nDnr2Z979Xa5RXveve/WP6sPKAfvXoArx5tP4F+8OQrTaXl4TWfKG2/Nr3WLmjr884H/fCRsLafcOMrbfk+bzt/qVp7Bf3wYQsePnLVlgoeWsn55PPmAf8h4+bNQ1u+7C3kqVdrF/y5vA9wcmj7CXhwpCtPAx5ymXnmAA9P6crLno0H53xUeEurfbZyiZ3m4O7T5q4PauNR7nyJlPfeeUjlFXuH1/yuZr9o2mNb9ga15M31TLvfZzb9aLUf3PzafsgtX5tO267Dn/zJn9z2urFPUq72xT3zxZtFrP+NbY7lpr89b1zivke8sYybvle6eJOzpF9xvkAb5V+6KTA4dI/3e/Tnfdpm150lsKIcRv782d/79mZ5bemHl0PPeUT5WzrbVlvFaUuexptHsFfW+hjKe9TnfuqJuGr6wJFJq9xjfk1dI3j0sFdc+6SRWY2XRVkjeIzUD6rvS2QqSUvVW9OpHvZqJv3YkhN3UiOWwdVJSZ2BR2v/nSG+spANxw9YT4rlfeBZb/whVx5e449afMW1E9I4tFZuaz8Y4T9mtVcaf6DJI7VXrfIaMT7X6oelPC3KysvgXkimtQOz2CsL2XnEvXuzV54cJD6IO15dNd7tyaGFywj9aI2nS/lr8yUjOWi4jODRGi+U8lPzu6lsWmNSre/kjmfS8lexV7nMRjPQ+P6QZ1UerX16VP4ePHr4j1Hys663Bw8P/xFu1OeWO+t6rVe7qPG5h35w1pHTeq37sUV53P4kTUfxkJZnnd6rH7a206td4HH84pyUz155eO2rsbDDFnZPUgb3AIikTGlaC7lJ/RUnfegnGr2i9HDLXnnt95Ewodo/6rmX3kbZ9NwPJ+lXXna6laNXu+DP4c+hH/V4Dvqh049R/mOG+Ebi/3ulbfU/tfzQD51+wJ/r5ObVj8EDPLzGg5L12l7+wKKec13/kPAYvXYr2acNHha9eruMPfLQ6IBmb5AmzxatEftFMf4oE/GK27bGH4c//uM/vvMF9NJh6PSz8tTzGBxuVSYpj6qP+zyks3g/bn2c9w9llSZ3Q3BlFWBZttdSfrPyqMkrLki1yjPXj8d94af7e+IBNTzju3+wWd+CrEfzCA7d2tla4KDalD+PPGr2ZmuxKdqikLf2F29c8tYPztfk0jZqAjIOH0r+eRk1Hi3y2vIf0V618A7vkDN/7Bf8nZNX48hXKisP+ffiMYO94sgvTaPho8kjGYBY+Y9a/+/lzzn6IZELl611vVY8avrhFe/m8VWrXLh6E9J51uXNYyb/QfV5yUSTFRcvf+7tP+Jic5SplX5I/IEkLcU+Ps/LfOb3vI2bdTOdNw9qPCixIRZytSgjCPRc9cOCh5UMub6G00f2yMODAzWO4I49z5WHxn/04NDK5Vz9h4THCA5aLl48ZhgPzsBByuVc7RXlz0vBH8efmgSXzEI445te48GW+co4n5iu/2H947iZLP6l8p1h/YPZRadPZqUfWK+t99fQCVrtgyT/CP2QtM9THrOun/eOr8CjzX+0Gu4w/kgP687yO7Rjxr/e84nQjzb9aJUf9sPx44Xo00p667Ve28rXKn98d4v9Gulaakt5Pfb75PNXLe2N40uL999695F+Bf6Db0884+9YNniAx4j97Pl80azjQW/9gP+QeSOMz2GvYK/mmW+3Gj+U1g8k8c9e/YdmfVBmcedIvdL5D6yfH+3XiPWP0n5R8KB5WO1vz63Jqvt3n/6Gt9551Zb5oJDXe/yB+V1+PL11Xu3wR3/0R5dRWYQ2279eNx+1vGca5LWUk99smH5xe8b3Du31ujmv5WYKr3Av8EjbZVXP47/4M62Kmqqcp77+B+4Y/3PTDwmP0RsVORsTY6dYhUcu85HBr6TuPO2qPHIjJJGR1IC1lL3Fo8X/1G6Qv+eeey6DYUt/ntsrqU2ylGHKr7Ude9GPrT7PYcNJI9WrkH6v9oorK67vlepBrf4SD89NEsFeRTto+cWLVnvFlU8tnSePfBORpR+Z1X+kcubYIk4arg5u1b1X/9Ei35a8VNy3Nx6estTEWXv155YcqD4u4bI3Hp4cLLjshUdPDi1cVucxkoOGy6o8vMcJmliWG1On6bzjq/SQmeU6FzXfbjVea+Ug0ZmQ1psH1qNkRMFDJi/v1IGH5/zVOayfW64DSL6MuGW/z2291uq9Qzl74uHlz/fAw8M2Bh5RL2fsh3vTD/iP+pdm4T/KXyCcUW/hz3X92NKPpfoCHvvggfhKxxn6oZPbOdurGfeRx318HrF+a5mW86+x3yDe1ekd7JVObudsr2aM8/c8Pt+6uExqa7/xS+4rzXIW6Z/yuu+/nG8/J/9R2y+K9fNtu+vlz8FD5+9689AYJY5OcdJY1O29Pojxua4fe8W7hz/8wz+8PIAeg8PYkbi/qaAylGe5Kb922Ev63zUK453H68aMvN0ewQjVD6jnXp2cqnfreXprGFcfcv0plf/E+32Od1caUn5wIFI9lKQfzWOWjXHcg3BhANLS/yk70YtHKnevYEiiMNw25OlW5CHZpGmlP1z5U23bAw9KBulzLh+t/Km27JFHKhOJXCVpubZtpL3itpGT7gl/97NPknH79VbZXJ8by/Coc+/6UeJDcbHgEOst6YckfpWOg9P39Rx/tMqIYlDTK+t696wfGn+gySOxkXvkYSFTizICp5H+fLS9spKhNj7j+Ko96IcHB2ocwR3T7Ek/enBo5bIHHiM4aLmsymMrhmmNSTnjU0kMtZV2Ff9ResfRHCQ6E9MGHp5/6fqgZ3zl+Q49y159PaqnLC3q6snDor21Mqh1sRHPQ1tH1MtdPwcP3eYfy/Vbr/Xa/LCddJ6zR3rP/qctO+WRH37h/qbmn7Vt65FvZntlGV/1OIzaaidm9x/gsR//MZtd8PyoSove9vLn4FG+fCKXSy8ePeIlKq7In/eIF6R1eM2XwJ/z9GGUfsBe8fik+sQdb8Q8XPsg1dme6WfrJ738x2zvjfiKp689ue1pPLjqeZye64OW/uNJX/a5J24A5z949iH1H5Z+dGs/NfYz1OdlvOKr3F61rpVj/+7F5Tm5yKw230jN14T8GJ/zrM/hD/7gD25zgpoZg5FZg9aWdqU3Zoy4cZZSLur5ason4SExOqsGvE/8ju/jWR7lZgkJD45di/05D3hLL9Hq5NmCYSbkBA1PeM33sjalaO17Lx5R9jMEuxEPpy15mtV45F2VIxNm9yaTaeqamUeL/+DaJo3MaiAsysrLWMV/cHmkstXIU5NnS7Fm1o8Wf67hwWWzJU/reoP/4NgJ0nhWEvT259p2Sti0Mtiqa3V/LuHDiUfT8iy4wF6dEpL4A0labj8Aj6OkWmTbkpeKx/dmrzxlqbFle9UPSw5UH5dw2RsPTw4WXPbCoyeHFi4z8+CMg+KmiNKC7UgG2jmVVedLavKwGCNwY1fJvEgt7SrxVf5+s3CQ2LKQdlUeFn16RBngMULq9TrPdT6R2odg9TxIThJncDflt6bT9iLJ/K62Du98s/KQrE/E/inhoSnfSg9q5QTWs7ZL00/uuece1vvM/N6z8tC0C/rhc+hc23/BAzw0enyO/kPzntAP6Iem30A/TvuN1j/tOd7V9DvYKz97hfEH79Ckpt9K9VxrTyT6oeHdOg9E5feei2kpX8NdwkNSPnX+A+sfZTvZi0cu/5Frt5K6S+vnXJ3R2BPwOJUu1W/Odf1j64IGbv+SrqV6rAm37C/R6Afmd/3iXYm/jfHb4fd///cvv4Cu7bit+eKB7dZyrPOPaFevL6BvyWrEe3PYjWgXeNTJrMTjqV/5BZtd8FwHIN/06jdyVEudxks/ch5B/pLAX/1CwoxUm/LnK/GQBLBe+kPJn2rjyjyod0+fa/lI5U+1aU88Ulm0yLElL3jQBp8jX04auqbrKfbkPyzkUytDa9+gHzIq3EPo4HEq11K8K5P8VWqOLeKk0dQPeyXjQNmX1hgNPNouA4jyt9KXPfOwkqFV3BzK2SMPDw5WdmxPPHpwaOWyOo8RDGpxFactq/LYijWtxgqaeLbkI7bKWWX+qvSOozlIbFlMuzIPbX8emQ88Rkr/et3nykMixRHr1Jz2jWiX13ot531jmhHvzWnfiHbdunWL0zTXNGEj34x/I3hAP+o9ATzm0hLwAA/YK9irXAIj7AJHE0e0C/oxl354faGT0/9mTwP9mIsQeIAH/Mdc/sOLB3X+I0gB6x/X+0IvHiXZc9ZLrS2YtM5V12vBg9ezvOLdp3zF5580wMo2Yf8uj2tLqhFxJae9I9p1+L3f+73LL6CnC0Sp4sST7VvPOS84Is2siys1WYQFKY68KT4jZM2pc0Qnb2kXeHCkZ5+m1k+8eDz9q7/I/iUmKPEbvu0NJq2YmYdV8KUVFDdoC+WvwoO6WSqVpTcfyaAwT7sqj7wvS2Qk1YOWsvfKI5Vxi/xiORZlhLLA47T3S+QqScvVMfCoS0rid60mkMGD7rkUF8t4ADzKPCgGNYqtbMAD/sNj/iofn0v7aYtvbslLxeF7GX9Yx6mlWFUz5tyrvbLs01Qfl3DZGw9PDhZc9sCjJ4Na3MVtw8o8uDKgRyB2Kag2rcrDa3xgRYY7vukRX1m9U1rOM77mizeLlca/Hm0slUlx6cGjx/ijlzy96wEPbwnLyj9XHrK39EuN/SV+stWUPCuP2ruEDdUe/kMjO488s/Lovb/EQ7aaMs+NR/jC2o0bNy7SizM0vzWy2nMe6Mdc9MEDPGaQwLn5D6/9uzOwCG2YlUftHETw5/FLklGGmt+zyP9c2gH/MRep3jxw3mCb/6w8Zpl3p+bZo3TPdT6Ru9+HWp+ztDKauvI8j3/Vd1s26VpZXvHVqjxW1w9NZ6Nsi6UN9Fo/34p3V57fPbfzzoff/d3fvfwC+qyDpz21K53YnfW9z62Ta4xwzHMOPGbtJx7t8uLxrK/90pZuMm3ex7zydZdtW5GHZTDUApEK2mLZj37Fd15W42FHe+lHKnfNwKxF1qW83Dbk6R77La+3bspJeSN45C/ElU2LILR15PlWsVdcu6SVW4sObHHeO49UNho2mjzgIbc8XH+bl8zVy1qLoB80K4pNK4MtHd2b/5DYDoqcBRfox3Upc3wCJw3FjxMT7Fk/NDLW5JHo5B55WMjUoozAac/2ykqGrTHz3v25BwerOYC96EcPBtwxRZqOuuhxNf8xkoNGZ1bVj60YxmKcoIllYx5qfJmW7a0ft2/f2/Iq1bzPvD+9HjWag0RfYlpvHr3m212gDygUPAYIfaPKc+VhKUWPdWqL9nm0y8teWbxvLMPjvS3a59Eu8NCTAQ+97DxygoeHVOcq0+sLa5Zv6dEPLdrn0S74Dz0Z8NDLziMneHhIVV8meOhl55ETPDykqi/znHjgvIGOs1d8JeExet59pvUPry+g5zy2ZN5jzVBbx8j1QcvzJc+8/5ecKCx48OyXl73i1c5LZdlPeDWOS3UOPDziGAuJe7Tr8Du/8zu3441Xnv9aCMC6jHgTwkz/pjeYeLYryNKz/DAxKi0/9D9NPmk9kvSBh6dehABuVgcgkVMvbl4B73MecD9r8zJFeY/65teK9VDC3ctecXiMHgjmgDkDw1V4RNlrB2YeysFpS57Gm0cve1XTBY5MrFho6urNo5e94tgmjbwoVq1l9uYxWj9q8myRY0teyqd426uZ9KOFjSWDtB299eMceGzJp8aQYx8pWxeeg8e2lDgxaSgBPE7nK/LxR6t8uBxymtb17t1/SPyCJC3HVsFeXUmpRbYteRFfnUrAU5ZpTVw7tld/bsmB6uMSLnvi4cmgZSyzt/HHCA5anVlZP2bgIOXSm0f4gpTH3/Me+OWsYrl+lVVYQyLu+OYxr7y6gLehumrWmzdvuax3UetRs3CQ6EtIu8p40KMvjSjTm8e9995yea3nPODLXModXag/D58LTEpyk6zr99jPMet+nx5yCnx61CPhCB6HqfafgQd4UPrrtT6Y1xvsVY99kHE/JPff0fFBXn9PHvAftH3w2l+C+Eq+vzv0V+iHTm6UH9A+Bw/wmMWP7CnepeYTZ4truO3xni/xslel/T5Y/xgXX0n3X3myaik7z5t+gJCrU5J0PfVjq10tMqPet6Xs0vqg57g2PfBMvVfr81n8eGzHrP7ckzfOc8ri6cNv//ZvX34BfUsBPE6/cxUuDq5i+vR3qnSl5+G/beX3en7jxo3L18s75dZv7oYG8DhcypfDNz3kzUkf5ct1IOAh4zGbfnA3/HBt1izpHvny77iQ2B9pu7n6Ib1k4blft72h4Vw3/DziZa9hiTi1JzPYq1w/gvxbBgIsISgSUW3KnwceHPnO5j9KPHJxUbJQiJfMIq0zTz+LvZL6cw6PVHhSOZGCTxK0lL0n/ajJtEV+sUyLMkJZe9UPCzZWDLb0dmX/IbE5pb5K5beI4WCvKClftyF5DgsONbsH/biSNveQRsxhwQX6cV0/OH6Bk4bWPLruPeuHRsaaPFucoB/b/oFrg6y4zMpDquvPf9BXnGThyNFKhpbjzBIPznxiOr/LmV/xnm/njs89GFiMZfYWX/Xk0DJvNqu9spq/GslBw2VVHlv+h+NbpP5Lkl4yrnn4S18tKVqclvtFyFb9KDVsNAeJvsS0s4w/pKDz+Eqaf9b0gYdnfMXdX3L33Vf7VziyWvUA+iNfzluv5ciolObWLd4BdKm90ranlG8rfk8388W8nP6brqd6p5fIgrufYVYes+6H814/l+4vkfQJKi3Vf6n8I5577y8BDxlV8JDJyzu1Nw/4D9l+UW//4d2ftOWHedQQk6TzqbP9lrwb4iuf8wha/eh5QYOkn3DT7jXehf+Yy39Ieax63mCW+cRWHqt8YM2bh5f/4K7Xpn7CY62qtcxZ9+9Kx+f5eRzOWker7EoxQGuZpfVBTqyx1/Eg5nc5vcM2zZ7OOx9+8zd/8/IL6LZi5JcmNYb8kvUpR91kkN78qG99W07wuJJf4BEHuW1S1eeWBtP6mvg5g5HUTnqEWmpGlnOTH7+V+0qpDZJaeHjpxwsf/FVLwnvYS779zntJNuWm6Sn9mIEHZ3DiCVeyIW4VHrnMWwdqLXwkdedpV+XhPVGS85Iw2GrbHnh4sdEyAI+y9dHIU5Nny/b1tlcXF6z74cTm+vkP+sqTPK0+W+Jz04qt64326ii3q0Uq/u+Yr/xvWECPk3JioW9keNFDvtqUB9emhXStDGCv+D2BqydWTHrbq17jj1b5cDnkZK3r9Y6vZvcfEv8sScvVyJp+8P1FrGnbb8TyZtWPFtm25KXGLf76we0psnSaA+ihBk9ZauKuun7I5MFPHQ7khLjNNu59/oNO5xO37Lglg9p7a+vYk73Syojf166nlNa5h/hKKpMW+bfqy8o8ZuBA+evcx/Tmcfu2j/94wdfz1qNaxwdWusMd3/jHVz48cn9uPU6z4iDRl5DWe/6q1/jDS369y33oi1/lWmW6nmtZEdbPj19iuRyd3+Z9meW4/+pwce+9tuMPCdtQ/2x/QR6n+8K08+tzzJdI5Iv9V1dfwvPyH+Cht1cS2e0prcclJ9SX2b3WByXcZt2/q72UMry7dr8o7FW554zavwsec/HgXtgnsT+rpB3hP8IFcT32l6zCyHv+ysteUePzWeYRtfNXnvvbPfQj55HKf4Z5d24beq/X9tIPrj5w5cSxfxZl5WXEC3ipS+tqzznjD4/zalv6sSVLCxnG8i3K6r0e5aUfnP4b02D+6nT+ysN/SHjMOj4f0a7Db/zGb9xOByG58aJ+p4KfsaPP2KYgs1q7vG6UkSiIZ9pz5LGyfowwOi39K+jHyjxm1Y9aPwkT7h48XvzQr2npJtPmffALv9Wkbb39B5cHd6BoIoSNQrgbsHrw6KEfq0yYPORF3+baNbz8R64fNT2wGERzBaSpK8+zon7U5KeRF8Witcw980hl2yLHlrzUBPye9KNFbywZbPWLr3/Bt1AqyXpe+wLUzZt94l2rOIobB3nVZ6UfdWhhI2S6WTQ/vEb9PpnBuvzxooecjj+s5BMqoJh41mXFY2v80TveZSl0IRHFIc9iwQX+/DoIjm/gpNH0g7xcb/8RNnKnXyG8++67Rb/Tdwx54592gbA1xrLmkpe3+niw1GctZGpRRslX2enHVd9NZRB0Ywb/YSU/a77X/YdNvHvjxl1F8xm+0Jl+pTOkk/xOC03r4MZXnhyosV36nDufs2p81ZNDC5fV46uRHDRcevPgfmFNGiu+5GH3Z2exGCewK2sc1zz0xTbrUaeX/V01KviKHv68JK/RHCT6EtPaxVdlf37z5jgeLX16VN5V/Pko+VnX24OHdZtnKm/W/SW95xNnYXJuPLziq1l4pIdkZ2nTVju89jPM8u7nph/gMUvPObYjjD08xh+zvOW57d8NLNI97vlhEOp3Kvdze/eRfQbx1UjpX68b+6lteFiNB2ut8fIfnP3U5zh/ZcWjt72i9u+OnHeX1N17vt3Ln7d8kEUir5reW5QRyu7Nw2v8QenHljW3kKVFGSvxsPGe7aVgfN4uQ8sSzo3H4dd//ddvx0PmISimvrDa+3mAE9s3278e8gobsGbnEScJZvzXUplDWefAYza98Oy/6Q1xs05A7YnHvffeSlQuHvZo//elD/9aa1WeorwHPu8Vrvbdy15xeJzjhEngkfox606SLtha2oWcR5S91UDNQg6ctuRpVtEzW193AAAgAElEQVSPki5w5GEh97QMaZ174tEiJwknKYOtdq2sH56TgS0MZuERNsdQN2Byn+cbqrW+WyJXSVqufuVlPuj5r7zMaulv47zHPffccxEPDaYHALntraWz4sG1aaV2aPvAVp3ePE7jq7DhJMwXtf/70of/vRMRWcgmFrilByGNZ10PfN43X8RDZMfNdrwvN3PT3XOPz/xVLd5t1TuKRSzfisle4yuKE5dDXk4rl/483n9xONy4uH371sVdd91tNt9upR8SHy1JS/Gv2cYQ78a/1O+GeQ6L3+9/f+BxvCHZcj6/lUeLbFvy5px660fv+ZKtfmkpR47v59iy3jxu3rzn4nAI64Lhy5DBbvG+JEmlk+iHBweqn6fPt7iUeMS8V3HW8dC+xe/j5SXH+fUjl348enBo5dJfP/rHuyM4aLmszGMGDlIuWzw8Nq8Ef94jvqr5cY5P5camLem445tV5netx2ktstfGeCFfTx6e87te8utdrvd84sj1896ytKjPe/3ci4dV3Bxk6LHOYNE+y/mM2B7waBtvWehcWkbpMOdM+zKt39eyPA+9hX7o9QP26upLfRb2v7V/g8dcPCzHB3GfSNjP4DE+t+q/e4yvPPajcvbvWvrWXmWtPj4/t/mrVcbn2L/Li+O84l3J+mDJ1rTMzbfkpebjU3vl4c/Tjxu0xn9p/j3wQLw7V7wLHuvzOPzar/1a+vkot7jRY3GztbGztCkO1sL7pLf1tb7fueWfkUevL9LP8u5pn5mlTal+7JnHLAfu4+RV6CthAsvj75WPeZBHscPLDAdAPP/SrxNZ1vPNj/q6zeJmmSihBoD58697rj2P1F553Rj+ikc/8ORVgvwtB85WfYdqU/7cg0f6Lj15pPVScrCSd+tkTA8eo/SjJuMebLR19ObhFV/l/oPrL7Rya9UDbl/xsFc94l0tj1abZskztGXv+sHtp1z/wtVLbr3pghS3DVS6ePgwpEsn3Kl8kucW+iG1QWn6Vg6xrB76kbZ7Nv/BYU4dDrBiAXu1TYPikOe24NJDP87Fn9dsRskuWfvxnvYqHP4PlwCEv5ntlUbGmjxbWrl3/bCUp0VZPXjM6M8tZMeNWSVxWA8e4fB/OGx+/PNZrn3ZIx5wIh7uF98lsuLEYmkaDfMePK4uYxrnPzSykcrfQl968BgZX43kkPPhtKU3j17z7Vt922Kc0KI7knHNKvOJJXmN5iDRl5jWm0fw7R5/L3/k9nqtR509yvTm0Ws82ENWPeoAjx5S5tdxrjzyN5xl71narlna1CPezXnMsveMrwljUnr5D+gHn+cI/ZjFNsBeHSUAHnV96bH/6hz8xyx9pIe9ovbv8q3rXCnTC6qtWpbub/eav5LwGD1vssf5K6x/8PyHV7xrsf+KMx8umRPU6GHv+Xbw2PYCPXikLfDigfEg39v3iK/AQ8fj8Ku/+qu3JTcm8qvpl9Lqpo/Y4lie9nfLADi9MZzzXv2kzK8p3OwS3qP131hjLEf7O+Q7fu1L/pfe8EPx8Lixw+ImOat2afUh16de+uFxwxD3C5NUOnlPnDPHCvZqTsnqWiWxFxK7kB941rVuvlxf+6yXXt4AWrNvveyVFw/NwNmSlGQCK/Dw/At2mfLjmhthSxcCxPfQTIRYy4DbhjydNw/JhUtcbiHdFo9Utly5WPDQ1NWbh0Q/JAs4+YUyHJukkRfFqbXMPP/9n/mSO1/OLH2JIG2LdgOHJL5q8R8cHlZ608pgqx2r+HMpDykbSwbgQVmd43NJHBTSt/SBWuwB/bjOiuJiwQE8bHXEikkpvqLmf89hPNgqH0onajSt6927vZL4aUlanjZe91l75GEhV4sySjHEnnhYyVAaK2/pysz+w2t87sEhl7G2jtL4/O67b1zcvHnrsor8d3gQ/pvm7557rr6ATs1PWY/PtTLSvKd2LnNm/bDiMYKDVl9W5jEDBymXmed3W/TDa3zQYru4fj9N1yO+4r5T9Fucf6kD6K3jNG6bpemocecDnvMyaZGi9CFO8NgPh/Xzg4hDTBzWP8CDL7oe9oqKcy3Wz/lvPHfKc+Uh2UfE7Q+1dJI4Y6tdsSdQ87fU8xnnd8FD7z+4/dOqH3Lr46azahf0w+aLguBBy3HleNcz4ui1Hy5/h3MdD55rfLXqB6Rm4oH1j4uLHvsTe4zPtfZJMicsScv1ASvPt3NlwJ13zRmDBx1n9RgXrhbvetgrjS5o83DHjVS6GcaDh1/5lV/xuVKfId2oPDFQSG8qCNlrv3soXYTndYg1iic/vNDrxowtPPGwRHoTVEhf+00dtrV8Hnh48K8po2RAyOjyoiTnoB9Wzik3luChCz568xB1aOPEuT0yLt6kOCoI0D7vrR/f+rgHm8hjtkLCAN3Cn8zKQztwt+ZEbSyJ9a3CI8rdYxCtZcNpS2kD7wr6UdIDjjy0sq7lk9ZZ4mHdprQ8r/FH7j8ouySVk0QmLWXvST+s+nCpnBYGWxOaq/kPSb8OaSVylaTltgP6UZYUN/7Jc1N2kuLSmwfVHu3zb3ns159kbZVLLIzLxau+vdsrqW8I6a1YlOwleFwR4epGzGHBpbe98prflca7W3aR46c5aTS2FzyOUmuRb0venNneeXjKMpU115btlYclB6qPS7jsjYcnBwsue+HRk0MLl9V5jOSg4bIqj61Yk+tbNfEqJ49kXLPKeLAkl9EcJPoS037NM17MQaxO02s8qG7gZBnDgQOL/V3xtfL9cOAhAw4eMnl5p/b2HxcXPtt3v+Wx2O+jOezuZa84/RT7RY9fvh7JIOWE/YnXeYxkc/t2uCzw7suvo8943qD3/l0vHtgvur1/O9qJ3F6N5HGO48EQX3n+9d4Ph/mrU/89Sj8oXfDk1FL2qvOJFI9zWT9fZXy+Cg/v8bmXP5f4vJnP164S73J4nMP4vPd558Mv//IvX85gUTcDbj0Pz6T5OdCoNNQgMuRPb8Kh0vd4vqV04cbX2uBEIl/wOEpRyjPvb+kARCL/2O+iAwIPPg+uflBfntx6rv0iJWWPqOfUpQ5U/hHPtw5Jw17d7u5ftoKEmzdvuviPVz3+oSO6nnudX/nUFzTz27JX99xzz+U73Oc+d1+EL+bEP8nvkDb9+9bHPYSUTcsgkSxckYCz8eern/5CRcl5lnBbcghrr//rZa9y/Qiyb5lIMhBCsQiqTflzb/3oySMVCCUHL/mHciV1nzOPNP4q6UdNxhL5aDlp6yjxSGNJKr6jnufvM0o/erDRMtjS41XtFaefa+SpybPVlnO2V+n4XmKvKDYcGXPSUPWUnp81jw9EceG9LHlEOXHi0VSmFvF0kUdSSYxa43/i/L4zHgxfiir822s8aCEbLhfPuvbsPzj2o2aDrJictb1KNj9a2yuprYqcWrmAx1WP1/hoTR7EV9uRjoVMLcoojeX35D+sZGg5L7NHe+XBIddAbR174qGVkWZcx4mVOZdurmivRnDQ6ss56wc1vzsDBymXVed3vcdtLTaMWhNJy+5pryz3l3zbN2yv17aO01rlr5nvD3m+6mnt67Uj9pdgP8OReGl/HHf/VWufC/OWcX3r27/xYa3FTZn/K57y/IvwnvEvvG/L7/wl0/ndVgGk+1Fe/U0Pby1uyvxf/uTnXaR7acJ+nJbf13n47N+FvWq3V9i/K98PLd0/HdJz9ye2Goi77rp9ce+9YY/X8Y/63VofN39oR/wL7Rv9+/jRtuPh+/zfXvtLuLKbPV3P8YelveL6j1nGIdz1rlV4pHKfYd6E24Zznr9q3e/DlZHEprWWuWceW3Lm2pO8jFZ7CB5lKnvhYXmBiXQ+UmJ3WtJa7n+m9kdLn+fv5RXvhnqoeK1FxjEvNR6L7eCmp8qzeL41n3j4xV/8xdtpJelkcP4Smi8hxjIoONaH21vq43aU9GA7Nw+VLigYeOhuMvTgEVic002GFvpmZUw9eEA/joQ1N3168UhtGvwHZeGvnnvwCKWHQ8/3uc99+A1hpFx5gZDx+uokub1SF5RlfM0THrFZVOvA2aqdeTnUwPPLnvRcr6ovy/WIryge6UuNZkMxSNsaFmzTmMIaTq4fcZBdGtRKvrSQ81hlgjdsaPD884qvtnik79M6ASuRjaauPI+3fniNP/INJhybpJEXxaO1zN488glFq3iXqx81ebbIsSUv5d/3pB8lNhLZStJSehWfj9CPNL7q4c+5suD6ma3yOHZyK3/OY3V/LmXDjU1bOcyiH7P4D0mfzdNasQjljrZXM/Pg6kbkY8EFPE57u8RHS9Jy7SR4HCXVItuWvKPj3dHjc+r9W+a5LLjsVT8sZNdjbOk9Hhw9PvfkYKF7e9GPnhxauKzOYyQHDZdVeXjOaXBjV41/yfP08B895q9KsrAYr7WykM6R9Z6/8ppP9JzT6MmkD497Lw6Hu+68ltd8iaXMRpbV216BxzbtlIdHvwjyD5cC3H336QcjWuvC/iudBHvtL9G1br5cffzH1Ud7rOxV637/NL/mY2Gt9XN6gsd+UegHR/JXac5VP7A/UcY5pvb6gi21/2rkvImk7lXnS7hjcYmsqB5oUdYIHul7Wfnz1v1wJVlj/fwoFc05TvCgtLf83Gs9qjXezONd6eH21vQcaXp8JDYcQPc4/2HNw7K8dJ4uyn2W84OH973vfVfXdXF6BTNNfnI+CiEN7NIBlcYops5Omz+fRNWWY5XP8saMFBV4HCc9pJzAI9wseNflIXzJITCPfOkNJkxTxEoG/dDphxePEjTOzcHxhuFe/7I6V8dEPXl0fC1RVeHwfTyEP/pfLx7f8cRHimRyLonv98TnqPw01697+fPXPulRLBFzJ7ZYhTUk4k7GrMIjyt1ikq9B7CdZOW3J06zGIxUIRx5Wso/lSOvcE49ebKQMttoV9MNj3BHHPV7+PPcfEj/RIj+tHtT0MG+L94Uyvfy5hEeLTC1YQj+2vQRHxpw0Gl+0V//BkRU3Jo1laXQybwd40GQoLhYcajbTO9712mCSj8+tZESxsNSNUBb047p+cHwDJw2teXTd3vqB+EoWS+yRh0Vftyhj7/bKSoaW4/49+g8PDlTcmj7fijX2ND7vwaGVyx70YwQHLZeVeczAQcplVR61qNJqnKgZW3D9fppulXi3JK9ZWHDmk2OaVXnMxoLL5H5PfPadw/q3bx8P7Vv/22t83mpPZsm/qn7MIl9pO/7uE55957B+PLTv8a+0TZz03P0+nLJmSgP9mInGxcWqPM7Xn6+xPzHIH+NB+jyI136f73zyo08MTUkfRvCR1pmn977gx4vHqvuvVvEfVv5i9Pr5nvfDlSKr0TzOVT9K59U4XyLvfX5vrmj64s45Ro8/nB/UnR88vPe97738Ajp1iCeAaz2Z7wHf42aw1nZq25TeSAYerRSu8oOHnSwtSgIPCynaldHCQ9IK+A+JtORpg8+g/EbpBqBYk5SPvIV0Dm1fpEvun8KLx+ue8pj+L9Ohxi/9pmexatHejhV4cBe5WA35QCLOgpTVJIqkXVtpOYP/sEDo+ecV7+b6scqEO1c/tDZUOkDn+osSj7RfSSe+LfukpO48rTeP9Avo1OSRRCacBZBYnkQ+kjZY8O/No6e94sjSko1FWXvnYaEzFhxq7fC2V7PpR65DEtlK0nJ0NaTprR8z+A+ObDixaKmc1pi6N4/Z9UOiL2naVg6wVxwtOaahdMWKRYu90o7P0xuqqXms0D7uTdzU+IMvfR6DWnmtbHL2K43PpQwoXchlvYI/HzU+p9i0yLYlL+Wv9hbvespS4+/3Gl9ZcqD6uITL3nh4crDgshcePTm0cFmdx0gOGi4jeFCxTvpcu/6xVUfrGEHS/lJaanyZ5lklvvIar7WyqM2L1Mr15tFrPrH0fqP1QmK/YlpvHr3mE/N3n40FV09W5WFlZ3qX04OH5J08/Lmk/tFpe/Cg5nU1+xP3vh+uZb/PKB6z+RBO3LuKfqzyQZbVeEhiTGv90cwNaMfnWj8n3U/da31QIztOzCpl3Ht9sNd4UCqHrf5F2XnPula3Vxq9Bo/j4d3wxx1/RDlrvmQtZaSNLaX1SNJr2+Q1XwIex/4r/Tv8zM/8zO3Sl8hjQfnJfs/fmi9UcwdP2nSpUejRvrABCzzqN1OBh+6mCW3/p/KBB3hInY5X+vCF9F5fPufW4/WutXLhP7b1cYS94txMFXnGQ4bU79c/9bG9u1aX+r748c9w/QK6l35819Metykfy0kNSxDU4P+LvuHpZ3lDNcUjleFoNhSDtK3e+hEuZ/D4++5nPP6k2FTmLZO3Vm3ltiFP9yXf+EyrJhTL6WWvajrAlYuFEDR15XmCfnh+AT3oRw9/zrFJGnlRnFrLLPHwnDcZrR8lebbKMJRpUUapHG//MSOPyEgjU02eLR0boR+z2KuaXDgy5qShbBtHV3v4D4/53Tze5fgPSl6SuDSU5VGnt72SHuikZBafe/Dg2DErDrW6vHkE/zG7veJyyPvKOerHzP6cw8F7o90Ifz6r/9D4aE0eSXy1+vicE9No5rqsuOxZP6xkmPJrLbM3jxniq1aZcWI+bR29eYyMr7Qy4si/ZWy5pV894t3e/nwEh5wPtw0j9KMXD64MWvq/NC/VplXXP7bkZDFuk3Lg+vy83B72qsf4vCav0Sw4Y8607avMJ5Z4zMJCwqSHfvTwHx5zSy02ShN/hTyr8vCQZY8ye8yXjPQfPWRoWcfq+jGLD+Guda3CY5X9Vyvy0I6VW+wONe7j+ndvHuE8isdfvp9aY5c0MtTk2Xr/VeZL4jtKzht67jOjzkHVnof38NyPmH+UKOxP9Bh/rMSjZz/xWj8HD925O/CY63zU4ad+6qfuePSWmxdCXml+jyCiVqb2tgTvNpbaFRQEPLwlXy4fPMbIXaK3VhsaYK/krKEfcpn1zhH1g3vzW2xfml779are7zpbfT31Iz/QOZsstO35wsc9TZv1Wr5SP7Y6YJvr1/c88xvN2j1TQZY8PPUj58HVD81Eowcf7gJIuBDA889q/JG3sXYA3XoCtkU2nLbkab7gsU9tqZLMGyfcpfFqmr40Pt+6ECA2iiMP8gWECaR15unPxV7lPDk8UlFK5STB0FK2J48tfz5CP0oybZFdXp5FWZ48PP15q35YytKCQ82m7sVe1eyPRLaStFx756kf52CvNFy2ZNsaT3vy8LRX1PijVS6cmCzl4lXfuYw/rP1Hq5548bD0H7BXXK9xla63vfJa/5COPzT6MOKL9Jb64ek/rO1VS6zUkpeKu/fGw1OWGn/f215ZzSe2xleWHKg+LuGSt8s7vhrtPzw5WHDx1I9SfBX1Qx55nOaQ+o+eHFq4ePIo+fPe+jGSg4ZLbx5W/oOjHzOx4Iz3Qzy9Kg/POaZWW7vVT/KyLePdkePzmsys5jV6MbHkMdJ/lOQ1CwuO7YppvHn08h85j9lYcJmsyqPVvozKb8ljRv8R5DpaV0b585nmE0fGvpK6V413KR2QyEhqq1rK7s3Dav/ufe5zn4t77rnnUlRvfNY3nYiN4nEu6x/hQgDPv8jDev8VVZ7nO+Vl4/wgfT4UPI4yyv+8xh/QD7rHgQcto54pSjwOP/ETP3F5pcyWoeXeRKK9KWTrBpGobDP9G8B5tCcNsMIXfWt/8Qu88XntN/dLvdx0PTustC4PHukCIfSj/mX6XH+99OMcePS88Si/Aan2W6pL3PTnwMPDLrT6uT3rx6w8PPQ29efwH+v6j3wCi2s/Z0/3eY9+cnFgPXu7ORcCaCcZvd6dsxDy+Y95ymX1HnbUy5+XJnhbJr5HMCht+En1w5vH1sUwMe6Lcqn9jv89148ehwO4zCT9Ik+7kn5YLHJwZZ6nkzBI8+b5VtWPkly1Mtti1FrmHvXDqi9v9ev0mSSOGKkfluMPiwVbTd/W5JHoV097NRuPKCeOjDlpNL6nt35YHTjI39VCPyR2rCRriV3ixho99aNnvKvpqxx9CWksONTq2kO8y2XDGT9r/faM+gH/cZ1KyX94Xqjac76Eqwccu1SzSdZ+vbc/T3nM4j8sZGpRRugXex4PWsnQclw4Uj9G+Q8PDpJYeSse25P/6MGhlcseeIzgoOWyOo8ZWEjit97+o+d8yUwsuExW5bE19rCc35COcUox7VYZPeevPOOr2juOZsHRk7TtgccK43Ov+VeNPnDnqkrpvPWjl//I320WvZDEXSGtN4+e81fw5/T+x176UdKHEXykdfaOr3rqB2XrpbKiyqNiJ47N7M3j3nuPH+g8/l0/8Hn1LBxrS5/Xfh//u+V6LYcTJw2HH+VPgv/Y+5/HvsxzOP9huf7BPWdEpZu5L3r0Ey//0dr/0vO8Hu/d2r471v1Ax0vSesBD9+V4Lx6H97znPbelEONX2dJGXYYEH+g0uaGZ8RaRGdsUZLtXHrM6p73ygH7M1SNn5BEkRAWdpedRstSXutN0c9GYszV79h8z6gd4zKUnXjze9NwnzfWiRq353Ef5vpfXhDvFgzO5aiRCUTHUJvr0wIGoYGZiqy+y5NV933OeyGyB7YENdqVJQopBWqa3fvSyV6k+eE2OS1hw25CnW5FHKjeuXCSyrqXV1DWCh8W7UvaK8hcaWXHb3VJ2bx5e48Hcn1M8omxbZJfzsSirN48R/oPTr1tk2ZKXYrq6/6DYSGQrSUvVW9PXvfPg2LEt2XLtJDcG8N7QEMaDHvPtWv/B6bfUeKGVwVb8B/04JUSxiKmtmMCflzWEyyHP3cqlN4/Z4l0qvkmf97h0rjcPxLvbHmvvPCxjVouy9srDQnbcmHXL5lH2cvX4ypMDJVsOl976Mcqf9+TQwqU3jxH+fCQLznh/a61mNXs1AwsJk7y9K82XzMSCy2RVHp5zfpw5r600krH/KvaqJo/W+YxWFhw9SetYmccsLCRMVvIf4b1n8iGctuwh3t1aX+CM0bQ2iiN/aqyymn5QstTIzGOepGbDevgPSkaa5/n+xFZfIYmBLHXserzrvX93v+fVPC9O0vThOHflsX6et2fGsxYztslr/go8dBoCHjq5eeU6vPvd7778AjpVSXojQUzL/TK6dfrUyAZHoDG6HjcstN4sITkAAh78GzJC/9PwBg/djRmUPoKHj1wpudeea3mkX3im/MeNGzcugj6Ff+PfqN+x3rx+6h16Ptd8kVvCA/4D/qMWv60aX3kdeH7z89a8afCzHv5NrLhJ6z9u3bqHbVIPhxsXt2/fugj/Xo0nyr/Bgy3Wk4QS/ZD4Dy6P1olf3Vtfz8WdQA76wfHT2nZ5jT9yHlHulgsc2neuLW6UJurz9vrbq/QG3u03bNGPVW6oXpmHpJ/20IVSHSX9WMlebcnVw5a1ltlirzjc8svevMaDNf/B6eetMgx1WJRRKmcP9qrGSCNTTR6Jzu6Zh8S/WHOo1e3NQzL+4NiamKbFXkn6ay2t1bimxX9o1j/OjQflG8JzKxaz+480Xoj9MsYH+W+PCxq4Y+jYFgsueZ2f/YgnsM2ERj96j8/ZL5Mk5PgGThqLuiX+Y5V4V+LHcxlbc+ntP2bUDwuZWpQxu/+wnr/y7tuU7+f4l5n1w8ufW/VlbZws+SL9yv68B4dWHWzRj3Py5yNYaOKEFh6aeHfEeHAkCymT1XnMwELCpDePnvHuTCy4TFblsRX7cOJOzdiam0cyDyMZn2v8Ry/98J6H5cq+lo7LZCYeVuOPGewWtw17nE/kyqZFB7R1rOo/JD5CK7uUl0UZe5+/ovo/18Z7rn9470/0Gg9SsrV8roljtOdsuPnC+2na5RVfRXl7nfdc9bwBePicc4N++MiVa5/ydFoeh3e9612XX0CXGhlOY2OZ0ZjWfsdD5Nz0VHn5c43DGnGjRnpDA3icUgOPq7sipP2fSg/9OB5G5dof2KujpGaxVyNsg0ZneuTRDhLgz+3pzKIfsFdz2atwCUc6eRMn92MPbP2t6ckjbvQLB8rvvffi4q67rlrM/R3Tlf59ywufphHB9Hnu+7BvcG7jXXf6ZdoXuL/zw2/pbw4PySS8sxDuFM+ZQL7vw77RtSle/iPnEWRvtShhKRCqTfnzlXikcqTkYCnzvCxJ3XvhEWUkkU0rI01de+PRg4uGQ61dq9qrWl9vkV2LXeK2Z288LPTFk+leeWi4WHKAvaK9NWd8kJZiMb4pbcCiW6pPkX6RPpbCnV/cmk8sjT/0rTzNSXGx4FDTj7Dhx/Pv3HhQLKKsrJiMiHc9eP/AC556UmyrfLgc8nexrhf+/Leq3WXEF+n3yqMlVmrJS41h9sbDU5aa2GuE/+DGU3n8ZRlfWXKg+riEy954eHKw4LInHj1ZaMb7Ic9eeIxgoWGyh/H5SBZSJqvzmIGFhMnK9momFlwmq/LYmpNpndNone+RzMWsOh4cqSuSunvrR+hb3P1WsR9u7buK+wil8+0SGUn1oaXs3jx67r+SyNFShpLxODWGXNVeSdikabm23sonraIf1Hml/Dn2Ux97ndf6oJQHzhsceXj5D/DQWWTwqMttxBm6wzvf+c7b3Js2YtNrnX/EC+SLMqXf3PfrmS60s1RfuDGD2w7w4H/BljpcCR7ncaMG9GMMJ+jHGLlLv1gP/RjDCfoxRu4a/eDexB/jq9ph8BEHtvNJ59pv3fDMN1ct3vXgwTnw7Pu2PqV/xkPsDqCHQ//54f2bN+91GX+89UVP3xSI1QSstdSpCeRPf/Dj2PLaGteFdpf0IHzBdrR+jGZDMUiZW+rHSHuVyrxlsclKH7htyNP14OExX5Lbq1W+SG9pr3rOX3F4xL7O7astuqGtI8+3Bx6eXLQcam3aEw9LLq0cRvHwusFdYq9qdkgjU02eLTvY217NzCOVkyQmTfO1xtLgse21uVxaOYyyV17zuxb2SmPHQh4rFqGs3vpxTjy4uhE5WnAp8eDOZ2ylq/W1wINbvmR+V7qBV+JTS/7B2o/X7NXK43NqfKeRsSaPpC9g/HEqLa4NsuKyZ4gf3UoAACAASURBVP9hJUNunMxhO4LHaP/hwSG3Qdo6RszvjuShlRPle0rPNXXtiYdGPhoOLfarN49R8yUjWGjmK/fgP0aykDJZXT9mYCFhMkI/eq3XzsSCy2RlHlYxaItPl/aJPfHowUcqf6pNlvMltf1XveyVtF9rZKnJM9v81cjxoJRRaU0iL4MzD8Ktt3d85bX+wX1fq3Rbl0By9b9nuvDePfdfWcmZWw54XP8YKs7X8s97Qj/O4zzO4R3veMdlT/fu4NybfkuHiuKNGl5GPhpG6maJ3IBayyydULQuO287eNDuEDxkXwaHfhzlBXtF65Y0BewVLTHYK9ir2Etq+oL46ighxFdXkxi1+Pdc/fnbXvJM2lieYYq//aDHnLTaug97+Q/w0HW20TwsJ8d1Ejjm4m6e76kflheQ/OCLn3Einih36wUjLwa19v6dr39sS5Vk3hDLePzl9qrH1+m47yHpE3nanvph6ZsoHlF2Etlw5V1Lp6lrbzx6cNFwqLVrdf3I+3KL7DzK2qt+WMrSk2muH5YxUJBB2NCQjkVbbXTMz/UfnPo48uWk4dRF9QvwuJIQd4wQc1iMbSh7Za0fXgcOLPWD6rP5cwsOo/z5qPGHxnZIxtFWTCj9sBwjhPfrNV/SKh+prbKyWeBxqjkSPy1Jy9XP3jxmtVctsm3JS/kq7/HgbDw8ZZnKmms/e+vHLP7DkgPVxyVc9sjDk0Xr/NjeePRg0cJkTzx6stAyoXhYj89HzV+NYKFhQvFYYTw4koWUCcXj3PVjBhYSJhQPa/0YPf7oyUdTV28es4w/JH1WO9chGXeMmt+djUfLWE7T/ym2I/XD2jfl7xrYp3XUfsf/nv9LyW6F5176UZINzn/QPQY85jr/4RVf0T1BlsLrXC11njZ/nrfaOt6FfsylH4e3v/3t7C+gazup9DBm7ITSzitNH9slUVWuE4xlStMHBdHKmZsPPA6XyCk+4CHb4E/Js/U5eIAHZefgP/g3BdW+IL313+E/4D+CjlHxFuIrnh7CXvHkRNn92nPYK7698powefvLni0ZZp1N2k/9ukedXGTRGt/m+b3iXQ4P7ibBXrA4m7Z78ODewCtJ90MvfdaJGIPsPRaMWllRbcqff9oDH91a5Wb+XvZqlS/SB/3gLKpFoXMX4WJ6yY3IEn+e26tVvki/kr2qKSplMywMhLaOPdirnly0HGobTLz1Y4T/oPp7qwxD+RZllMrZI4+Ul0SukrRUnxilH7XNNa3/vRTvcmVQSscZG5TytY5xevuPXuPBVrnU+muNsVd9K8e7Wn2hdMWKRc1/rDr+kPKgOOTlWXDJ69y7P+fGSh6+fER8NbP/0MhYk2dLT0v6sTd7ZSFTizJG+Q/JvC03DtbEu1Yy1I5fOHG29/wuV77SdFIeHiw4MTLl8/c4/vBk0cpkb/FVDxYtTHrzkNohbnqOverJQstkhL0a4c9HsNAw2QOPkSykTEbwkKzDcveLctZrpbKRzq9YxL0YD9qsJVnp4J78Obe/c2TLScOtb0uveowHPexVatdm3S9648aNkwtWObys9yP22p94Djywv523bzv2U+q8QOvzvfKIdoE7zok8YnrP3xwblaaR7jeUpvfanwh7daQo9TeHt73tbewTjda3EUg7Zym9pk1SIWnSc4OY3DhLvnCgeXcLmW+VoWmTRr5pPZz84MEnz5GnVP55evAAD74E/FLCXvnJVlMyeGik5pcHPPxkqykZPDRS88uzGg+vAznv+Obn+kEYWHLYwCudBJE0N71Rn8on6YsUD2rjE9UWr+fUpu1PecAjWZMgs40/KB6pPEezoRikbeXy4E6i5pOlvezVKl+kX8VerfJFeq5+zG6vajbJa+G75F80deV5uDy09sprfjf3H5SP0MiK69Nbyi5tMFkhvqJ4RNm2yC7nY1HW3nmkMtXIU5NnS8/A47p0ODLmpOHat60+sRf/wZGVZJwQyuPaSIl+cHmca3zF4SDxC1Ycav7Mm0fv8aBG/lyf4jHm18a7q+uH1FZFNq02q7c/l4w/JH1bOv6olS3x05K03HeBfhwl1SLblryUr/L2HxL9aJlv59oNT1lq/ItWP859vqRVJyzsHWfOi6sf587DUi8omyPRE61+nHN85cmidV6sd3zVa70W8+13Xa67p4f4Y3+JhytG+vMeetFqu7T26tz8xwgWGtvVm8cs8yWefFrK1vI4Z3/OiTNbYiJJXsq+cePd1XhQcuHOd3DHodzxC5fHbP6DK6/WdJI5g1gXzuO0Sr2eHzz8ZKspeTUeXvGVRraaPKvx8BoPamSrybMaj8Nb3/pW9y+gc26sCTA46bg3cFmk4wat2nbHDpje/MG9EdE7nUY5vPNo5czNBx4ygly5atPVeGjLs8wHe3W8t8TrRn2p/QYP8KD0e5Q/p9rV4zn0A/pB9TPoB+9GP0qOVs9X4vHOVz5fFlyeSepP/nuPcG2p1419q/L4pK99uNk8Qs/xB5dH64KRVWflbtj25uE1wZvzWOWL9KvYq1W+SO+tH17j85J+SBbFveyYdLNJnr4HD+5NxZJ0P/KK552If0u+UhlpfIa2jj3yiPLVymyLT2uZI3hYjZ/Scjj2ykuOrQzSdo3gIbFD3HUpib2y4mLJoaaze/AfHH/AHR/kZbXGBdCPbTpcLq0cRurHjP5Da8NCPisWoaycf4/x4Mz+Y8u3UnbOgssIezW7fnD8NCcNxa/0fIR+zMpDI2NNHolt3GN8ZSFTizJK/qMHj5n0w0qOJVmmesD1LXv2H5YsrOZg9srDg4UFkz3y8GTRymRvPHqwaGEygseo8WBPFlomI3jMEl958Gktc8R4cJR+UONljSw1eTAepEiUn3PndmNu7hhjNh4e9grnDW6T++ZiP8jPq4FHed/wSvt3oR/nrx8ecQV3vb+WTufp9Lm89lNDP3T6cXjLW95ymyO8kvMJ/y11RprT+fquxM85Y7tqbQqKCh58tlYpg3Eu/QWDBR5WUuaXs8WDX8p5poS9mosbeIAHJQH4c0pCfZ+DR195U7WBByWhvs+3ePRtSf/a4M8vLn7sW1/YX/AdavyEr3moSS21PhLGgpxJt9iIOOlX+p2OcX70W15AtttiwYisRJCAs+DlzcNrvkSiH6O5cDhErJ94/4cJCMuT9tKPVb5I760fvXnEHmO98UDSEyV152lX5TGCi4RDrX1WPGrzicF/9PDnlI/QyIqrEy1le+lH7/nd3J9TPFLZtsjPWu+8eIyOdyU8WmRqwXKrb+zFXm3ZHo6MOWm49g08eJKSjBNCiRqdzFviZa9m9+c8IsdUFBcLDt7x1Tn6jxIjikXMY8Wkt3547Wdoia9aOOR5W7l48ei9/mHFQ+KnJWm59rE3j1HzJZQ8WmTbkne0P5d8UZiSYfq8RT8s5GlRRil2sBp/nIu9spIjFYdx/Upve9Vr/YPz/pYsWsb4PcaDs+uHBwsLJnvUD08WrUx68xgdX/Vg0cLEi8e5zLdb8rEoy4vHuc6XSGQqScuN3b14nIt+UHLC/NXxMHD8S8+nhf+G82pUDyo/7x3v6lrpkwv7RX3kqi0VPLSS88kHHj5y1ZZ6TjwOb37zmy+9dXqTSe6sNb85AoyHe2Pa2u8oVG76vDzOIWLNTS6pXDT583aFCcXa31YwxeHD4cFNsxce6QII9EP+hVBr/djisXXYI7SDes7p+1z7Ewe0cXNrLJv7m7MpVnOjDecdJWlgr+ibX7b8Qk/96OE/uPqxF/8B/YB+pPYU+nHaHxBfnY9+UPET9ZwTV3D1Yy/xlZd+vPtVL+bgOLs0f+srv/7kJtdzia8oHpwNQCNgUQteJR6acUttPHTz5s3L17Ycn1M8YqWzcKE4xPauoh+rfJF+Ff1Y5Yv0K/KIuu+xIYTyOdI68/Q9eaR+iXov6vm7vu1FJ0lW+SL9x33Vg6+9ei9/To0vtp7n/pzrt6X9d6tfWJRV0g/Lfpu3v9f4g8vDwpZZcKi1w9tebR3IaZnf1epH3l8kspWkpeztKB69xh9S/ZBw2ZKtdb3e+nEuPDh2LOXSygH6wbUgYy8E6KkflnGDJN7lkuDOaVjOxeR1luJdi30+cZ571vgqZcTx05w0XO5bda80fyWVh0bGmjySMUxPe5XP744YD3L8NuWrLZlgPLjtrykWFjxnsVcz+HPPvq2Jh3v7c6/9PprxuSULKz3hzO9axlez8PBgYcGkN48Z4l1PFq1MODxWnm8P8rPgY1FGqS0h3rX0s+cy335O48HV9YMaN1LzJtyYmKpnJf3gvGtIw92fuJf97V7+HDx0+4LBQye3VK8txx/gAR6pLYP/OO0Phze96U1X18VwrX5DunwSWeLUY7VciJxD56HM0KaWzRuxjK2bd7giS8vg5mlJVxpMcQ/Jxnq56TmHakOZtduoWt5Tm3drAktb5lY+6Me2VHvrB3jMxQP2apsH7BX8OdcfS/13nh7+nI4A4D/m8h/gAR5b48K9jAd//NUvpY3XGab42K940J1WS8fz3Ff1Gn+Ax+nNxefAw3Ihj/u+W+mohceQdw/6MZoLh0PkuAqPVb5IvxqP2M+sNuRo7JSk7jztqjxGcJFwqLUv8pBu/uf2m17xFcdHaORFvWdrmXvTj5I8W2UYyrQoo1TO6vZKG3/W9M2KQ81ehQ2jnn9e8+35eJBjr2rvyZExJ41Gjr3tlaaNnDx//9tfcpKshQfH95fa5FHnnu2VRl9CHgsOVHx17vNXVjLijqW96vOOdzm2R5PG2l5xOeRtbeXS23/MNP5ojX09fDp4XFHRyFeTRxJz79Wft8i1JW/OBvphM7a2YlLjsYf5K8t5DquySgdsNfENN89M43OrPp2+e2uZe+XRKjeJT07TUvEw/MdRWi18WvLCn/MsK8aDPDlJU2F/4rbEvMbntVrBQ9qD95leOl8rTc+VKvRj3+c/YK/gP85pf/vhjW98423ujQ9h0oZzqJtbnkW60N0syim9V+zKubPgOIN4Aw4nbZom3JjBfR/w4EsXPNpuIoF+2MgP9spGjlZ+CDzAg+NF4D/s+8mI+EqyIMWNw3qmg72y74ct/MADPOA/joeHS3oE/eDpx3te8zJONzq7NB/9ZV/HarM2vvLy5xQPaiMB66UdElELw948vOavKB5RlLNwoTjE9q7CY5Uv0s/E48aNGxecS77CPPDWgTXLDTlak8VtQ57uY778gdoqWfl62auSXeLKhPUizETSOvP0PfRD8gUL5mtf5P6D8hNSOXHbEdK1lN2bR6/4iuIR5dsiu5yRRVm9eYy0V1t9vEWWLXkppnuxVzU2EtlK0nLtHfSjLCnu+CDPzbWT3P7A1Q/tpfDhi/Tc+VWreJfbN0vpuFxaOdT8mXe8O5s/57CimFixKMVmXP3Qzl/N6s9bdMNyLkbrP7T2Kv1iEdU3uXYtpJOOP1pjLQ9fDv24TkUiZ0laqu/V/Mee7ZVGvpo8Et3cK48WubbkHT0enDG+spCnRRnwH21zf1TfTp9zY2JtfLVCvGvVp1O5t5Y5gscM8+3S8caoCy1n8uej50uocbn13FXJf3jPl3iNz632r1PlBJlJxqlUeenzyLfn+Siv+Ery3i3yXI3HCvoh8X/c/RhpP80vu+KM7bXzV5L5dpwf5JA4ppkt3oW9OvDhJSlXsFct/oc6nJ76J4mAtfpx5wA6pzO3BBucF9G+AKdsbZoRbQoswKNMDDy0PdknH3j4yFVbKnhoJeeTDzx85KotFTy0kvPJN4JHeBPOJEJ8Y83kAUda2gkGTtnnlKb3jX3nJJsR+oHxR72HgMdc2rMSj5/6zlfMJVyj1vzN+z3AqKR6MR7+HDx02Lz8B4cHd9ON7s3kuTgL9N76MZJHlNhoLhwOsa2r8Fjli/Sr8Yj9rHWjmtwaXeWQ1J2nXZVHKk+JfLQctHVoNyhq2xn8R4/4iuMjtDLbevfWMveoH7k8W2UYyrMoo1TOHuxVrX9rZKrJI9Ev7gZerb2SbFCU1PGTr/3mk+Qce9XCxZpDLe7Ys35I7FiJZUsfGMUj1DuLP+fqHzV+s+Awikev8bmVjCgW1mP+3vHVuegHl0OuY639oDcPr/VB+HOu9T1NNzsPSdwkScuVFvRDN8/kNS/Wm8es/qOlr7fkpWLsPY4/LORpUQbmS+zmnazmsHrbq9nHH5J+Lkmr9eeYvzqVHHcc0jruWG18zu1/Md2IvU5UG0e1yWP+inrX/Dn27x4l4uU/pDxG9cWtdo5oE3jUiYCHVKt804OHr3ylpY/gcXjDG95w7QvoadCTTjjmN93kv+ONGtz8HAHFw9gxreZ3GixwDnd73TCQlhvep1RPuKEhv5Elvnt+GIrzmyNjSZoY/MU8rb8ldXunrfHI/zu3f0M/6l9GpPRwSz/AQy9XSu7SL1mWbpSBfvjzgX5cDYB7+GtKb8ADPKj4mBO/aOLbdOAQ8s8a75ZuGizd2Kf1H/FGWG683ItHaFceL3MmUb1uZtyaPIU/532xmvIH0ufwH/v2Hz/9uldyzNHZpfnIL7m/yQ3MvfUDPLbtYOBR8k+SL0hRnTmNpygeVgvcVJukz6kF+h764TGfyLkQIMhqFi4Uh8j1o770a6WIq+lL41LLA1Jb+rHKF+lX0Y9UDzw2Skk7LbcNeTpv/fBa/8jtVa8vqGxx4TJIyxjBw2O+PffnlJ/QyIqrEy1l53kt7ZX3fEm6XivlEWXbIrucj0VZnjx6rtdy7VWtj7fIsiUvxbSH/7CyV+l8olY/KHmkz3MbaMmhpq/BXlnMM9b64Wh/buEDtsqg/BZV/179OSUXjn/Z0h1u+ZR+evvzXvMlrf10KwbNZehZlyWPnvt9rPyHhIMHF8/4yjve9fDnUjtlzaTEw9ufzxxfBfly4iZOGo0PAY9TqUnkLEnLZTOCR4/5do2P1chXk0cy52U5HvT2HxbzJRx/QbG1ZOLpz89lvsRCnhZllHyXpX6MXo/i2mxqTFYa91nJn6r7nMYfnvGuZBxC2TNJvxjhz63iXZyP4u+LD32i5/g8rS/2x3w/L6efYv/u7UsxUfuhcT6Krw/5ftIt/fAYf3D6PidN63nBPD+nzl5pYK+OF1LgPM6xx8F/XNm3mr06vP71r7/yGB/Q1BEn4SkjkR+y8fjNUZ48OIntkDrbWucsbVAEj7pRq/HgHM5PZV370il4yJzKCP3AbVSU9bz+HPbqcCkUi0EjN+gaoR/wH3L/Af2Afsitql0Oj/g2to4T59b6v3W863Wjvh2Jq8FkLj/r3xwu8B/gAX8+lz/H+ENubWePr372u75V/lJnkOMjvuirTyZHrf2513zJ6jzSRSPLblTa4G7hPzg8LDceWMiEcwi9ph+zzydyeEQZjubC4RDbGnmcu36s8kX6VfSjpANem9i2bJe0zjz9yjyi3KQy0vgKbR1cHucW73J8hFZmlvqQl7VnHpb6YsWWy+Pc4iuOflB9M33e60IULo8V7VXN7nD6OieNhd/Zgz/nykkyTghlanSS0tFV5kssZMPxL1YcanWtNh7k6oLGdnH8i7R++I+6xGCvpL3pKn0+f9Vqr6QsrObGoB/X+wAnduKk0fQu8DiVmkTOkrRcNlweGA9+UFGk1ky4PFYfD7bItSWvdvyxB/1IZTObPz+39Siufeam28v+RK/9DFw5c9OBB1dSfdKBRx85c2sBD66k+qSbncfs8ZU1pdl5zD7+AI/jYfP4R/G6TPe6173uTi7uITzOIYmtw3ihLu5hvV7p4s1E3vXV4JQ6L/emkNYvKVorjkV53hy4yhH1AvpxV5ebTTj6EY3cnu1VvNnO4qbjLfsRedQuZ4B+HJ3uTP4D+gEevfwntx7oh/6mu9Z4u5R/7zy8/aamfIu42aKMOOG+5/hq7/rBteu90s3EI+rYnsfnGvummacYOf5473e/ysKcTlfGh3/hV6nn3zjjcy/9+Lk3fNt0srRokDcPr/EgpR+tG0gtZFsqg9rIsiqPKItZuFAcYntX4bHKF+m9efTyHz2+Nsu1YZINjHnaVXlE2Ulkw5V3LZ2mrr3x6MFFw6HWLm/96BVfcf12i+xyvbAoq7d+eF1omce7XB4W+mLBAfpRtvoS2UrScn1Rb/0YFV9x5cHRl62ypHpJ2by9+Q+KE3fc1sphtL2i5CB93uo/tuqjmFixCG3oba9mi68o7hQL63kY8KgT4bKwZAIe8jhrJX9+DvEVJ47lpKFsYel5b/2YnYdEzpK0XDa9eczszzXy1eSRxHIYf1xJC/78+keBWvcHBun22sfDrWem/T5e9qrGDTy2vxwLHtjfzrUjvdLNZK+84t3a/j1unNkzXS/u8Z3y84Tpu8JewV716o/cemayV4fXvva1176AThmLeFtC7ZBCa1Bcyl9T9lT5Lb7sQ7279Lm0TaUbl6g6wYOS0NVz6dfqSl+QomoDD0pCV8+hH3xZ9UgJHj2kzK8DPPiy6pESPHpImV8HePBl1SMlePSQMr8O8ODLqkdK8OghZX4d4MGXVY+UGJ/3kDK/jln0431vfDW/0WeU8kM//ytErZ1FP8DjiO2c9MNyI7qo01YSczYUSfVDysPrwNrPf8+3s0U0mguHQ3wZbx5e6x+5vUplbr2JkA0+SchtQ55OymNW/7HKF+m9eWj0g9Mfc3u1ZZO4fZVTby2Nto7e+jEDjyhDrcy2OLWW2cpD6s81PKJNrF0qF/77lv+g+nmrDEP5FmWUyvmwL/hKqvlNz3vFV5oYSiNTTR6Jfkn9Rw/94OxnaNGPXD4cGXPSaDpuq706p/hKKh/JOCGUrdFJqi/MqB/e/oPiRHGx4FCLMbz9h8af97ZXVJ/Nn3vymFE/RvKgdCOysWLS6j9W9Oec8UnJxlkwAY+y9+DqhbXtauWxYnzFiWU5aag4ofQcPE6lIpGzJC2XTSuPlf0HNQ+SytjCd5TqO9f4ins4iZsuyjo/fIfzOFdrwjgfRVu9WewVt99z00E/6uw540GunLnpwAM8aGtEp5hl/MHt99x00A/oB9376RRSf354zWtec5vbST3ThVfTls9RnrR8SXpK5NwvBcZyqKA0TLhr5WCZDzxu3+EAHlf3U5QGm9CPYz+RXLohsT/pBo6twX7UV/AAD2l/9EgP/wH/AXtV9wvQD+gH9AP6QY0HvTbwUnFi6bk2Tlgp3gUPjAdLegB/7uvPf+F7X6MxWdPn+ZDP+/KTNp7LfCJ4HLFxecV0XvOJFA+rDTnWCkVthFxdP2bhQnGI3PMNWLVDezE993lMFy581caZW/ly/Yhy99hEqNURTlvyNKvoxypfpPfm4TX+qOlH3pc5fVTb/y3qojbwcu0RZb9G2auabD25tJS9Vx6RU4vsLPSBKsPbXvWKd6VxVAuXlrwUj9XiK6kvkMhWkpbbjhHxVdjUWPvCkPa/W14IEGTHHR/kcpbqJaUfub3icuWmm9Veafx+mqeVQ82fefuP0fEut9+k6ShdsWJR0ktvHuemHxzbBR7X12G540GpflC6EcuzYjLCn/ecv5LKX2KnrH057FWdFlcvrJlAP64z4YwrOGk0ujmCxzmMPyR2y8p3cOyVhvFWHq/4SrI/PR4i0vqx+H6c/W3Y387brx5kCh44j0PpC2WPuPsVYjnU/kTYK+yHw364w6Xa5X4W+gH9gH7U9ePw6le/+rYkON0KLlPnRwWflKO0fC49lW9Zd6msIO/0Jo3UyQeDBR7eBE7LB48rA9FX8uXawAM84D/qmgj9gH5AP6AfNQkg3r2arK2NV3rGOeABHlv9bYQ/j4uOsV357576Ib1V0rtt+SGFtD6v8Tl41KmO4IH4CvHVCvFV8C2STew1f5Db6F/8vu/wNsNDyv9rn3M/k3rTmC+d3wUPmXi9eXj5819602vJF7XcKEVWxkjA2QRpwWNEvJvzCLL32kTIEHU1CdWm/PlKPKJQKBm0yJebl9uGPfAYyYXLodbGD/7cL+Mir6bbGn/08uerfJHewl4FULX4qpc/5/huad/ldNTWMvdkr2rybJVhKNeijFI5FvoxS3zF6c9pGo1MNXm22gX9uC4djow5aaT9wVM/bty4cdmc1L+HC7A89l+Vxh8aeXD1pVQ2x2dRbYJ+bEuIM4YOJViwKOnHHuJdqo/mzykmViy87NVWvDvD+AM8jofIwt+58aB0I7K10hEP/3Eu40FKT7gsLJl48Ngaf5yLfkhZWDHx4DFCP7B+Xtf2oAO9xx/gsc0D53Eo79Tv+Yj5K+gH9KMmAezfxf7dLesHe4XzOPAfOv9xeNWrXnV1RcEHytDetNOaL75CDjMfQFmEQtTNLtxFiNg2bnoqXekLICE4pvJ5PAePwwV4HHuBtn9BPyysVf1LW1Iu4AEerX6amx/+A/4j9gGpnUrjM25/46SDP4c/r/UT2CvYK9ir+o280A/oB/QD+rEVZyG+2kd89ctv/k6bgfRkpfzl+37JyU3zoXmWX4i4efPmtZvsLeZ3weNeFadRPKw2dFqrD7XxrqQfnHE3d/zv5T8k+jGaDcUgZb4Kj1W+SL8aj9jXvA6YceyXpO48rTePcHgt/7Owh7m9qtkkiWw4st5Ko6mLw+Mc4yvKR2hkxeXTUjaHh0X/jf5+tH7kMm2RnUdZvXn0iq8o/bCUpSdTb/8xKw+N37fkUKt/7zw4XLb8iFQvKT0FjysJScZtIVcri1AG/Md21MRlYsGCy8My3g3+Y+T4gxuzpukoJlYsRvDoNZ/oKaOcqWddq8y3W8mI0o3Ixqs+b38+23hQO99RymfBhOPPLf2Hl73izven6aJMcf7jcLLf3mJ9EDx4XxrP5eQ1PgcP8JDEzjivRvcX+A/sT4x9QGNf40UDlusf8B9HIrPw8Bp/aN4P9upwcecAegrF+0tos33tTBIE9EgbOnL431bntGzHbLe7hHfTtskqSMsHxeChu+EEPCw19aos6IePXLWlgodWcj75wMNHrtpSwUMry5lqJQAAIABJREFUOZ984OEjV22p4KGVnE8+8PCRq7ZU8NBKzicfePjIVVsqeGgl55MPPHzkqi11Dzx+5S3fpRXP1PnChrj4d07zieBxuomKu6gbWHvMt3N5WGwitFAo7kbUHvph8T55Gf/wB15/8p9W+SJ9Dx499CPVA4+DZdI+xW1DaQOvp/8Ids1j/Ty3VyW7xJWJVNZb6aV1bvGwbFfKuLd+5O8hlZFGDto69qQfPbhoOcS27VE/PLi0cuDw8Bp/aPSfylOKr6g8FlysOIBHnRZHxpw00v4Q0vf2H73Gg63jPu54LZe5db09xh+afkPlsbBXEvtVak8rixH6MXL8QTEtPefqiQULioem/VSedOxBpZU899CPmo+1tlFpefAfNHVKR6x0g9IPj3gX9mqb/yrjwdpbatfiaK3Rp9C2yUM/vOJd8JhrPQo8wENrsWCvtJLzyQcePnLVlgoeWsn55JuJh9f4oyY5nHfe7lOHV77ylbe5m4NiuvxGplhFfnOV9Den+9eC/tjJa89j2ZqbCqTySdOHeiU3lpVuJKPqBw9+MBt4UPJMn5duMKHygwd45PYm/z2zvaL6N/SD379b/Q3s1fHmM0qfWuXMzQ8e4IF4t27/oB/QD+gH9APx7tWNrRgP8uNl+A/4D/gP+A/4D5n/+NW3voEzfX52af7Sp3+haL5W6j9KNyJTQsrnx0r2Cjyu/NgM84kcHpYbO6k+xHlObUQNZcyoH/l6V3zX9HJrDo+YbzQXDofY1sBDst7H6QdpGq/1qJzHKl+kn1E/NP4j1wGvA2ac/iipO0+r4SHRp9IXIal3suAR65DIhmoX9VxTlwUPiT/vFV9RPkIjK0r+FswteEj0w2t/Sc1/UDK05GJRlgUPb/3QxFeUfuScWmTZkpdqh3d8pbFXPXhobI0lh1r9M/Kw9OeU/QrPOXLmpOHUxdEPif2Rzpdoxh83bty4c0lT/nGjrfGg1F5RcuHK1rpe6MeV5CXj6JCrlUVJN73HH5r4aoR+cPxJqjMWLGo8JPErV49jOo294qzXauNdTvspPbFiYaUfEn8DHts9wGL8IdEnjb3i6EcaV8Q3ps6DcHTjHM9/zKAf4KFfj5LoU4xzOX3ZI01e/+j2cN4R+oH97Rr7lPsTTl+D/7j+JXn4c+xPjLoD/YB+SPxxfm4q9CNJvHR4xStecedz2xpjzjH40jTa2xKk9aTpqUPrUT6UknIOsVGDsXiDInjUgzLwaOnt8rzQj7rMYK/uuA/4j41D0rBXcpvTkgP2CvaqNtmN+Or4hbmtSx1gr1qsjzwveMBeWSzOyXveOjmsLqnB+JzuE7BXsFeIr+p9APoB/YB+nJd+5JujaS+4neIfv+17WouYMn/YwOv5Fw+AWK9/UDwsN3VayofajBp4tHyJhRvvjtKPWbhQHKIcV9GPVb5Iv4p+rPJF+l764W2vVvki/co8ok32OvyX+nltHaUDB5bxQ15Wr/iK47e1MtuST2uZJR4rxFccHpb60sqh1pY92Kta/9bIVJNHol975hHkJJGvJC3XB/T2H15feP5HP/jdJ68ssVe5rDhy5qThMtiKA1Ycf1jIhVtGSz8o6ae3vToH/eD4+RKfVhY1Hp7xFbefSdNZ2ivuOCJNZ8FiBA+v/Vf5/K6VfCgfH5571tXLf1jPt0v1KZ2H1ubV5sN6bV1y2N+O/e0c/Ui/9Fo7BJ9eQlU7pBZ7Yu2yKi//obUd0A/oB0c/sD9Rq2HyfOABf479V/U+MJN+HF7+8peLv4Cen5CPN2ikgwiviZhYBzVpEdK1nOTnHCa3KD9vp+aGOPDg32BCcQWP6zeAWPRzSu615+ABHvIQlM4B/1HvV/Dnen8CewV7RVsfeQrYK9grjzgM9mp9exVv2K9N7sutEZ2DY68kN+VxFi88ysvfFOPzsfoCezVW/qUbL1O/BP0Yywf6MVb+0I+55D+KB3XgmY6g5kzxFz/t80zXVcJbpnGb5ossnPUP8ODpZS//weFhubHTQps4h9D3oB+juXA4RN4ePHrEu7UN1V4HZzT6wWlLnsaDxwj/scoX6VflEfszp49q+n4pj6auvehHLi+NrLicWsruwWOk/6Bk2CI7D8Y9eMzgPzy5eDJd3X9scdHIVZNH0oZz4FFaj7I60CmRryQtpZ+1eCPwsFyX6rUeZXmgkyNnThougzTdOfoPT/3gxMVbcm4d+5d4zK4fPXgEmUvmNUL6VhalOmGvTns/l4kFixoPy30/veZ3cf5jrvl28NDxiBf2aWKPmGfWj4tY2hXt+Q4qH+wVr99ScrR6Dh7g0WILa3k5+0Vhr47S63FZEHjgvIGHvuX+4/Cyl73sdt7ZPAyMtswRN8zEtm7dFNAKpxachjrBo9xbwOO6XKAfWsvikw88fOSqLRU8tJLzyQcePnLVlgoeWsn55AMPH7lqSwUPreR88oGHj1y1pYKHVnI++cDDR67aUsFDKzmffODhI1dtqeChlZxPPvDwkau2VPDQSu403z/54e+zKWiyUv7Cp37unRad23oUxcNqQ6c1LmpD6qo8ohxn4UJxiO1dhccqX6RfiUfsY16HZyS2i9uGPN2KPEZy4XKotXFlHj25SDnsmYcnFy0H8Liy/q0yDCVZlFEqZw/2quSHW+TZkjdvy578ORUPaeSqybPVDvA4lY5EvpK0VF8Y6T+4bZOky+dLWsbhHDlz0kjaP5KHx35qSx6Uf96Sc0s/gD/f7sHcOSbLubFV/IfGNpTyYP3DSpI25YCHjRytSkm/PG5VJrec2pfOufk16UKdHv5c0xbYq4s7LMCj3HtwXu26XOA/rCyNTTngYSNHq1L2xuPwkpe8pPkL6Pnmlwgj/e/hv1E3AXFudqA+H899HtLFL722bt6xzB8CLMvy0puvwEP+ZVvwmOvGH/AAj9y/1H6XggKuf4h5t24Cgv842lPwqPsV2CvYK+gH9CP3Jxh/YDzIGefBf8B/wH/Af8B/nB6yw3wizy7Cf/DkZHVDOFUOeIAH/Pn2PLzVYl5azj99x5s8ih1e5p/75M82Xy9KxyVe9uqf/cibh8vOowHePLy+ZMPRj9bNztby5mwQ9uYxg36M5sLhENmvwiPK3OughkZXOG3J06zGI8qNIwuNjDl5JHX35tHLf9RskkQ2HFlvpdHU1ZtHL/9B+QiNrLh8WsreKw8PO9bCodaeVf2HtV3Jy7NgEcrcu35Y6IkVC/A47eUauWrySHS1h72y/KJ3/AJrPl9C+fOaTCTylaTVxgJ//lM+h5tVlW50vMtpNEfOnDScuig/1EM/OPseqHWU9Hl4Jyv94PiTLTlr9XK1+Cq8D84b4LwBd7819sNhPxzHL3jNl8Be6dblwUMnN0l8w9GLWB54gEeMJanztqU4luuvY16cj7p+/ikeIqfkj/POh4vDi170ojsS5Agr7bAc4WkGxNI8PW4M0Cgl93BibizihBN41HsCePC1BPpxlBXs1eGy05T0B/aKr1PSlLBXfInBXsFecfQF9oqvU9KUHPmneiqZPEa8K6VxvGUylzf1G/ohlzM3B3hwJXWMu73/wIMvYfBAfMXRF/gPvk5JU3Lkj/jqSk+l8pWmBw++xOA/4D84+tLbf8RNv7We/Bvv/H5+Jz+jlGHDKPUlivy55PVu3bpVnD8vrU+l5VLz7avy+LOf9Fms8bpWP7Qbqlv1o3VjraTPSdJSh59X149ZuFAcIlNv/fBaP8/t1SpfpO/FI7c/pd8Svd86cOB1WEPSPm4b8nSBB+ePuynRar5d6j9KdokrE877c9NI6ywd6OwRX/XUj1x2UhlxZZ+m09ZxLvohjXc5flsrsy0+rWWWeMw4HvTgEeTaKj8Lndgqo5c/t96fWIqvuHbGgolFGaX+0YvHSP9R4tQiz5a8lG+bNb7yslet9saSRYt+3Lhx43KeK8ZjpX/j+0Z90I4HLeJdjv2SyFeSllN3iccq8yWc+KomI46cOWm4DODPZZKi5ppa2HP8xznGuzIJ61NjffAoO8qf6yUsywke4AF7VdcZ6Af0A/oB/WjdzyDzyvrUq9mrwwtf+ELzL6BzF8diurgZI2LxCN62bmqotZe7SUT6vlvpPW4wkbYPPK5uLgOP+o0y0A+/4A32in+TEewV7BXnRjHYK9gr+HP48zwehv+A/4D/oO0CxueHO5sgpONp6/SwV7BXsFewVxy7AntF9xOOHDn6xikHPMAD44/6l0GgH3Pph/bAM7XE+t/96A9QSc7y+X/xCZ/pOj7Y2nzd8uW1VS8E+C8/8b6u/cjLXnH0w3KjrYWQqI3BoQ5v/ZjBXo3mwuEQeXvz6KUfq3yRPvBoseOUfwgXynDGKdLxTm6vch3wOrDBsVuSuvO0q+kHdcAifW5txyQcYjtKPFbUj55cNBy2eHjYE88veoX2UvaqZFda5FazU61l7s1eeepJK4vQtr3zKMlA41MsWNR47MF/WOqJFQvoxykVjVw1ebZiZNgrPRNrFtAPejTHnduwGrusoh+S8Tz2l2B/Cae/YD819lN7ze9y+h/OD15fRwePudbPwQM8sN+nvt/n8IIXvODyG/ISo1+adA7umLoZkx5i2KbocWMAt8XpodLapH26gA4eXMnq0nF4pCVTi7vUc10r95kL+jGeO0c/YK/6cQKPPl9U5RIFD/BAvFvXFugH9AP6Af3Y8qcYnx8nZ+Kfx+V73Hgmzt9I0numhf+A/4D/gP+A/ygv4mC+fdv7wH/Af8B/zOU/4ubv33rXWz1Dx2FlhwNr2j/Ol+rjF71CHZbrg6vy+M8//jO0OE6+ZF87tOC1PvibP/aWzXZbbbBVC6eSkdog3MIjrbLX+qDUXs3CheIQZbmKvVrli/Qt+jEy3s39R6oHHgc1pHaL24Y83Yo8ouy4MpHKeiu9tM7ePHr58y0/IZWRho+2DkseUn+uec88T4wbtuxVqR6tvDhtbinbkofUf3DejUqj5RHKbZFbrV2tZY7k4Tke5Ma1rfJLuViU1ZvHDP4j79seckzr4PaNks62xFcc/zEjj1bbZcGzFgPulYeWiSWLEfrhtX9XGl9RcQLFp5RfYpe48UCLfkjjK0t/Ht8P+0uuSIMH1gexPli3vNAP6Af0A/qxFZth/y7279b6B8d/HJ773OeefAE9FOZ5o6k2qN5qVy24pownJxhPhRjqqQk1tqGWnvve+Q3V4HHavcGjvhmV2iQTn0eJSn+XDM0Wj7Q9MW++yYo6tJ8/v3nz5ol9gn5APzj+Cv6D/0V5jjy5/qyWDjzAI4+nEF9d3RQF/YB+QD/qN6dBP6Af0A/oB+ZLri4vkI7P0zgf81dX9hTziXW70jruw3jw9AuEmL/C/BVnvgXxLuJdxLuId73i3d9+9w9u7TM422d/5mP/9p31otx+zh7v7oHHOa0PcnhYbHi2VDTOIfQ96MdoLhwOkfsqPFb5Iv1qPGI/sz44I7FbkrrztKvyGMFFwqHWvpRHL39u8QVjyQEpjZwk+hDSauso6UeQj9dfafzRm0eLvDhy0bIotSvoR28enPkkah43j3c5MVSL3GpcWsvcm//I5dgqv7Q8i7K2ePTyH6P0w9LHW7Co2as4X7IXHq3+xIoFeNS9s2QMnZbC8VtbMcEq/oPy99LnWI/CehTWo7Ae5bUeJbVHVHrYK9gr2CvYK9irufaL3jmAzpmU20qzdQg1Vfo4sN1yFrEe6eFxzmHy2jvMdosDeBw3jMzwl94Qp20P9EMruev5wGO+W5la6UI/WiV4lR/6Af1AfFXXJ+gH9AP6Af3Y8rgYD9YnKzE+b54uObnEbmtSFPMldFwMfw5/Dn8Ofw5/fjVnGvUB/gP+g5YA/Af8B/zH3v3H//Cet3NMxdmlCQdAPP+8xh/goaM2ikfrRmfd29K5qI3bq+vHLFwoDpHkKjxW+SL9SjxiH7M8MENboHIKbhtKB0C0dXLyWfiPUj2/8+M/dPKfS3aJKxPOe3DTSOs8Rx6l/SV5fFXzE1L5cOVeSqepa288gtw0cpJy0daxRx7eTLQsSu1a0Z/X+naL3LzKPEf90PpzKxvvxWLv+mEZD1vp2jnqR0t8xfHLHNly0nDqytOABy01am7Dcg5mFR7pHjOs19J9LKawGJ/jvAFf3lRK8MD6OdbP61oC/YB+QD/q+nF49rOfLd5RvfXlJOomEq/n4RUtbraTtI9yzrFNnHSx3vDFZ+kfeBxv9uD8cQ+XgEf7jTngobtxxsuOgQd4lPoA/Af8BxX3IL46+sN0IszLTnPKBQ/wgD+HP4c/pw/Lb+kJxoPt4zyOv8L4fFvO8Ofw5/Dn8Ofw5/DnXH+az9to8lHjfs7iGeKr63YL/hz+HP4c/lzrz1c9gP6ffvSnXq7Tn5N+gMeRlnS+hDvu5/SFNA2Hh+XGZ2n7Sumpjdohj7d+jOQRZTKaC4dDbOsqPFb5Ir03D83+q60vhYbywnPqALrXARqO3ZLUnaf9zz7m0zhVqNP0slerXAjgrR8jeYROJOmr6k73gYyauvI8q/PoxUTDotS2PfDwZqJlMYLHKH8ujf+1MXELixKPlf25N5NWFiP0Y7Q/p3y0RKaStFS98Xlvf66xV5z9u9wLl7hyqcmnll9r3/LyVuHRsu6mWeeLc3c96+X0Jel8opd+9JRLfGcNx5Z2ggfWo0p9gOM/Wvqdpp/DXmH9HPZqfXt1eNaznsU7ucvpDQZpuAFJqarazTYa4xnLb2mPRhxeA0JNW6IT0OYFD63k6vla+iN4gIe1BGCvykEC/MdRArBXU4VX4MG8qMbaTtbKg35AP0rjE/gP+I8gAcRXiK+2fBH8B/wH/AfmS2oSgP+A/4D/qEsA+gH9gH5AP6SbJDA+x/jcc3z+P/7kO3pNQXat5z/5m5/StT6ryn73J37YqqipyvHm4RVfUfphteHZGhZ1+DkckPL802zg5bSH4hHLmIULxSG2dxUeq3yRfiUesY95HJzh6GyahtuGPN0q/iO3S1x5SOXMSS+pu3Qgh1OHNk0v/7HShQBaWXPyjeQR2ifpq5z32UqjqWtP+tHTn2hYlPrLiv681Ie18uLoTEvZe/HnvZi0sNizfljxaZV/3o49+g+OzaF8f6kMi7H/KvaKK+M8Hfb7YL8P9vvUtQf6Af2AfkA/ahLwWo+CP9dJ4PCMZzzjtnTzBSd9aE5MF5tG/S69Qu3QbEwrOVQbbqLVHEbnvK+23FROoZ5bt265fMkdPHhfegMPnpy0/V2aDzzAA/6DfxMO/MdYfYG9Giv/3L+AB3jAf8B/lOJOjAeP/QL6Af2Afpz6ScyX8OIGxFc8OUnnPbTpwQM84M/hz+HP4c8161bwH/Af8B/wH/Afp3bgf/qpH9HtsJg813/0UZ905wu98Uu9rf/mr+u1HrUyD43fro0XA4+U782bN132l3B4WGxAt1QnziH0oB/ePCz1L+ov90KAIM/RXDgcIvf/+G98smUXuFPWCP1Y5Yv0HvqR6lsv/5HqgPUhJk2H5bYhT+fBY6R+RNlx5aGRNZVHUvdeeASZSeRCyZj7XFrnnnj0ZiJlUWrfqvYq7c8aOXH1odU+9tCP0f68JEtPJi1l9+Axgz+n+relDNO6pGMd8CiTkozZWuSf196DRw97la6zYL4d8+2Yb8d6rWaeEeu1WK+F/4D/OFf/cXj6059+u3aIO94mIjnkzVWG/DA6NSBJy+Wm9UpXkkcY1MU/ibxypxPygoeMHHhcyavlBiCZ1OupwQM8Qj+E/yjrCPQD+gH9gP/g+Fv48+NXoFM5bB3OwvhDJi+MPzhauJ0G/hz+HP4c/pxjSeDPZf4J/lwmr5I/r/VLjM8xPqdsFuyVTP9gr2TywviD0kD6OcYf+xp/wJ/TOpGmgH7sSz881s//55/5UVmnO5PU/+FHfmJxftVyP4OHvQKPw7X9JpxLy7z2l1A8pBv/e6kPtZG+h36MtFezcKE4xP5Q43Hjxo3LLhMP4cf/ILlUo5d+rPJFem/9CAxrPFtsxO/99DtPsq9yIUDgUforrZ/GdBy/ke7X7MkjtLHlMFpLH0nzctuQp1uRx2gmXBaldq7KYxQTCYsRPHr685KtkcqnxV5p6irZK8/5kt7+fBQTDYsR+jEDj5yRVnYaf03pW29/PtpeUfLg+hkLhhzd9R5/nAMPif6EtJZj/t7+w4sH9XFUnFcrH/IED47FPE3jGV+BB3gECcBewV7JNaGcw9NeHZ72tKfd+QSa5JCH9KaOWb80ZwXIWn7W5eW8wKN+Y0ZpMh482jXF0r6AB3hIFs2k/oqTvp0APSjhtAP26vRwqhUX2Ku2m8WsOMRywAM8tPYQ8S7iXSpegL1qsy+UfKXPwQM8uPYe48F2bUF81aZv7QR8xoOx1JZN2ZwvtFm//6rlBT2DvWqnC3u1pr2CfrTrhrV9sS4P61FX663acRH8uY2etJYCe9UqwWN++PM1/bnX+IM68GzTK/uX8h/89U9wrTTaK+tK/pef/THrIqcor8ZjdnvF4WG5Ed0CFucQ+so8ogxHc+FwiG1dhcdKFwKUdNFyvGAdr4XxYG6vVr8Q4Fx5BPZeh8skPoTbBu4BwnPmMZoJl0WpnbULAc6dxygmEhalNq7kz0v2RCofiU3K02rq2ou9qslVIzPPsvbOo6VPW7KM7cjLXN1eSe0Pd+xmNcZchUe6DoP9otgvSq3LSfWSSj/7fCL0Q78+4zFfAh7gwd0f6/ERESt7dXjKU55y3BFH/Hm8BFVn/rx2Ep9yFrVDeqF87U0RW21vkVWYeOH8tdTBKZ+TBjyupAQenB5zTNMiK+jHcXBg+Qce8mAmyh/+Q98T4T/gPzS9B/YK9krTb1rzwF7BXmn6EOwV7JWm37Tmgb2CvdL0Idgr2CtNv2nNU7JX4ctmki+YxcPysS3579Y2hvzp1yGl5d26dYuVpUUHWRUwEsF/wH8wusm1JC19F/O7mN/V9LmQB/ZqfnsFf67t3e35oB/z64d28+Pe1qN+/+fe1a4QE5bw7334x5Gt6jH+kNbBOfBMvtiECf79j/h411ZZr+XHxlL6YbUB3Vo41AZ6jn60jD9G8YhynIULxSG215uH13iQ0o+0X49mwmUR2uzNo5d+rHIhgDePXvqxyoUAq/EIOu9x0E/q17ltyNOtyGM0Ey6LUjtX5TGKiYTF3njkNkYqK46Nai1zL/bKS5at8qf6yMr2isMkTSMZJ4R8FuOaVfRDKmtOesy3Y76d00/yNC3zV17jQc17UHmgH9APqo+UnkM/1tmfeHjyk598W3KSXnKYOz+cV/uddqjayfrYEa1O3nMOrWsXg6XyjO8W8gUHIs2fp0/LSyeMwYN3sw948OQE/ah/uQH2iu8kYa94+lbzC7BXPPnBXsFeIb6q22XoB/QD+gH9yOMxjM+PEsH4fDvOgv+A/4D/gP+A/zi1kxifY3we44fSfDziK8RXWI+6fhl0Ot9JzadTzznyla67Id5FvOt1IEezMWO2PFJ90qavrR/NJo/R7dHKV5oP8e7+4t3/9ef//uju7VL/v/thf+vOhWOaS9Bq+WJDw/NwIVlr+Wl5cQPqqjw4Bw5aOoIFZy0PiwMBLe+e5+UcaOjBQ+p/OPvhuPoxCxMOi8Av2KtWeW3Jb6R+xP45mgmXRWjvKvqxyoUAPfSjVf9K45ncXq1yIYA3D6/x+R+8990n7jK1SdaHLjU+nduGPJ03D6/1wZp+RNlx5aGRNZVHUvdeeEhizVZ/L5E/1S5v/Rhhr6j+G55byjCtT8p2r/pBMZLEpC3yH60fXv4jvBfOR/HmLTG/y5MT1gexPgh7hf1w+Xge+32OEglyODzpSU+69jld7SHw1IlznXlpsoUKtiTPW25LqNXTIp9QZsgfnVP+/qUBSEt9nMl4TvAlkflWWvCob06VXBYAHlcSgH5s94YW+cBeHaxU7bIc8NCLFP4D/oOjP4iv9DqW5+TImxpUgQd4SOLbfPxoJT34D/gPjj2DvbLSOP0kdGwB5kvsWMTxHOWvqefQDzsmHHsEHmV5w5/Dn3P0B/YK9grjj2Mf2Lp5HutR7XrCsUfw5/Dn6fgm9of8MGJME/8793d7Lz6WIP0SMqfeeIgppm39ndZZ+gJIiz6Cx8Xl4VsuL08e2M/Am0/pHe/+b+/7cY7qn12af+dDP7YYL1H+O38OHjborXjcuHHjjl3L/anX/itKP6QHMWykSZdCHWxYlUeQzGxMKBahzVY8ZrVXszDhsLDkIbFXtFbXU8T4irJXaQmjmXBZWPIYrR+rXAhwbvYqjgdz/VjpQoDW8Xiev8d4sMYj1N1yeLbFlqZ5uW3I043Qj5Z3rvmPLR/BlU1Lu7R1zMDDYv5qSz8ouWplp+n/0raECwFWs1eUDPLnHD6cNNJ6S7Z1hL1q0Q/MJ845n4jzg1fnTrBee2WZsN8H+3049r73+Bz2SmevDk94whNODqBzFbzWCTy+NBDb1HqzoDQ/FZBxZRXK4colX0Dn1gEexxsVuH/gwf9CdU1vKFmDB++mJKldAg+5XENftZIztxzoR50TeGz3Dvhz+HOunbFKB3sFe4XxRz0uhn5AP6Af0A+Nv0W8i3gX/gP+A/4D/gP+o24HoB/QD+gH9IO7PpemQ3yF+Ar+A/4D/gP+41z8R+nAM+XHOM//9194DyfZ2aX5tz74oy/bfE77S8DjtKtJ9NOjk1I8Rh+qrb0zddh2Zf2YjQnFIjDszYN7YVTt0Fb879wD6LMw4bAIPP7tD/kYD3NyWabX+IOyV+lLjWbCZTFCP7gxA7X/KuexyoUA3vaqV7y7yoUA3jxye+XlP2o2yetgbMnIa+rK86ziP7g+QiMzTeyqbU9v/fD0H5rARMJHkpbblrzMc+UhGY9y5hex3wf7fSgd4tqSUA63f3qNP7j1c9NBP6Af0A+s1945gL71ZQiqk0ieSwyupFxu2ppx5BjDWEd+0wG3bkk68KAPi4KHpEfx0kI/TuXaujOBAAAgAElEQVQEe8XvNz30ETzAI5UA7BXsFU8jTlMhvkJ8pek3rXlgr2CvNH0I9gr2StNvWvPAXsFeafoQ7BXslabftOaBvYK90vQh2CvYK02/ac0DewV7pelDsFewV5p+05oH9gr2StOHYK9grzT9pjXPOdirsKZd+hJ367v/H7/0k61FTJk/3eDu0cDQZzzs1co8vA6VBb7QD1kvX1U/uAeUZNJqT00dtg08PPUjvMEoezUbE4pFkFU4QJgf0mjvBacljOIRWjELEw6L0N6V9SP2itFMuCx68Ojlz1e8EMDaTkXf0cNelXTA40AsJSNpnaUD6Cv4D45NksqKkn143lpm6cDzCvEVh0cuX40sNXm2uIIH3esp/6thX6t1FR6l98P5D7qveY0HwYMn+1Iqj/gKPMCDksBZrH88/vGPv8250SV9mfh5e2k+bfoo6Pzwd/o7/H/O4fD4HrFM7m/uzR55ujiJzs1/69Yt8Zd6wYP/RWTwuL1pt7j6kOsPt39DP47yh72qf6lB6yeoL8PDf5TtJPwH/EfqF0vxFuwV7BXHLiO+QnzF6SfaeHFrMgfjwaMfgz+HP4c/r/tr6Af0A/oB/YiLdS3xCuJdxLst/YcTB2P9Y9tfwZ/Dn8Ofw5/Dn5ftAPwH/AfWa+kvw2N9EOuDnHh8K97f83gwxOHU4YwR+0tiuzj/3rx58878efwSMPdfajPgqgfQ/82/+jeoV7/2XDJe9toPtyqPf+OvfJRo/2CAI+nv0A9Zd19VPywPzcgkup2aOuyj0Q+pvZLoU/4l+trbcezVbEwoFuFdV9WPyHEWJhwWoc099EOiT9z9DBz9mIUJl4WWh8T+BH/eg0euB9YHYSU+RFJ3nnYV/aDskkRGEtmHtC1l75VHLmNLGaZlU/2Casde9IPb57m2Xir3Wv2r6EfrPFyaH+sfWP/A+gfWP0Kc6zWfCHt17F/S9Y9DOIDOdaZe6dJFiXQwlBsNjhHhOJtUSJx36nnzivdtTpz3BY8rKYEH3WOgH1fGN0irtshaclKwV3T/olLAXsFecS6fSXWN6lNWz+E/aEnCf8B/0L3ELwX8B/wH/Eddv6Af0A/oB/SD44ER79JSQryLeJfuJX4p4M/hz+HP4c85Fgb+nJYS/Dn8Od1L/FLAn8Ofw5/Dn3MsDPw5LSX4c/hzupfIUvyf/+CnZRnOJLXmAKHk1agLDSRlpWlX5REOgHj+efmPVXlAPzx7o7zsVfXD6gCTXKLbOagDWCvrx2xMKBaB5Kr6EXvpLEw4LFbikcq95fCslX3itqF0oNOqDaVyesVXW3rAlU2LHLR17JFHLmet7NJyLMoI5YFHXQu4Nt7SN63CQ2JbsP6B9Q+sf2D9g2MzvOIrTt0xDezVlbQOj3vc48RfQKduzOJ+kUICLU8rOeSpOZlveaMB98bk8I5hwp2Sr/Q5eOi+EAIefLl56UtgkAYX0I9+X+It2RnwGCv/XM/AAzxq8QDiq9MbmeDP4c9LNyNq9ATjD77dxfiDr3fw5/x+JR0Ha9KDB3ggvqrbL+gH9AP6Af3YmpfQ+F3MJ26PSjD+4NtdjD8w/qDsSRrHwF5tf0kb8yVH+WA+kW9XKP3TPsf4g+8He9g18AAPjAcxHsR48MoOYPzBjxOC/4hf8IzxleSLntwv1q964Plf/8sfqQnPN/P02H+1Ko9/7b/966b7OaEfbd0b+tEmP+vc3vrhceAg2EPKXs1y0DnnRR2K8+CR+o9RPIIcZmNCsQht9ubBjZek6XL9iLK3OvhqYYc4bcnTrOI/arrAkYmF7EMZmrryPD30w3reLIxnavpByVYjs1qZFmXtnYdGtluMW33UKjw46wGUrmw9x/o5f54a81ey+ase8yXQD17/xXoUT06c/mQRB+U8Do997GOLX0C3uAVYauS3hBCdiUW7WhxXKa+2TaX3rQ3QtXWk7QWPbfLgYa0Zx/K0fRc8wCPtOy1OEv7jeBi35Q/+A/6jpf9o88J/aCXnkw88fOSqLRU8tJLzyQcePnLVlgoeWsn55AMPH7lqSwUPreR88oGHj1y1pYKHVnI++cDDR67aUsFDKzmffODhI1dtqeChlZxPPvDwkau2VPDQSs4nH3j4yFVbKnhoJeeTDzx85KotFTy0kvPJBx4+ctWWGjaQa/5Kh8689if+X7/ys5omTp/nX/1vPuJaG89BP/bEA/oxTo1G6YeWeZTUH//DnxknNMeaw4FOz7/gU0p/3jxaD/Z5yYQ6hD6Kh9ZHRTnl/iPI3+LAqzUHqk3585K90vbdkfFVSR8oWVjLPpQnrZPDQ9t3e543KOkHJV+prKjyNPLPy9wzj5p8OZw4aTj8VuUR3gvnDbZ7QE97BR60NoIHLSNNinPw59APmmzx/N5jHvOY5i+gx6rTmx/Cf6v9Lh0qjGXUnE7+vOUwYmxX/i/3po9afu771+SVduKW9wOP0xtLpPKgVWmfKah+z30OHjb9hytvKl0LjxY7pbWX8B9HYrn84D9O7T78eX0QT8VbFno9Y3xlY3nXKIXyC9zn8B/8G/K29Ar2CvbK44bwNazVVbzDtUu1dC32ahVZWrxHKwdt/G/R9hXLAI+5qM7AwyKOl9pLjM8xPuf0uxni3bksxtjWwF5h/JGPPzh6TPUb+A+sR3lYNqrfcZ9L+6fHu6xQJlfeVDrwsOkNlJy5z8EDPGwkMFcp3P5PpYN+2HCl5Mx9Dh7r8MD44+oLRjPMl4DHXDxsNP16Kf/3r/6cV9FDy/1X/tKHk1+kb/EfXvrxJ//ovUPl5lU5eHhJVldu4BH8zNYXr2PJMR33d0gH/ZBx8eYhaw0/tcR/jL4cgLoEIH1rb3vFl7AsZc4jl7nXgVhOKyV152m9efSyV5QOSGTEkXmapqXs3jyk78ZNT+kHtxyJLCVptfWfq36E963tD8P+kmNvwPmP7X3XmC/BfgbsZ6h/AX0G/Tg8+tGPLn4BfcvhxaAsOoKWQ+PcyR6uA06dEzdPazrO+6e3ZKVBbf7+mgMI4HFKcBYekXmczImtrP0uTfpIJnc4/Vh7Wxun7Foa7vtz6qjdoMixV+BxlNIsPOA/jjxmsVfgAR6UDd5zfAX/Af8B/bguAYw/MP6g9CIdx3DTWqWbJd6F/5jLf4AHeFA2Zs/zJRgPYjxI6QfGg/z5NMzvUr3pKEvpH8Yfc44/4D/m8h+IdxHvUrZ1z/Eu9AP6Af2oz+9CP6Af0A/oB9UHZplvx/hjrvEHeMzFA/7c3p9r9u/m9rJkX1c9gP4v/9cfRrmT4vPSIaRU9vFwjoYHZz7x//nHP69q9+yZSjws9ydq3h/6IZdaD/3Y8h9c/aAO3srfXJeDewg9HOjM/2aJdyU8Url7HISVUuC2oXTgeVYeW/Furh9besCVjVTmaXptHXk+b//R4s+1+sGVq0aGmjxb7dmjfnD5xHSUrbf0SeekHxifzzU+Bw/woGzbue+/OjzqUY9q/gJ66SYOSnDp8/Rm2/jfLW4e4t6Yy01H3RgQ266Rh8f7ShiAx6m0wKN+c0ZNX27cuKG6OZHTT8EDPGr9BP7D5mbRXK85ellKAx7gobHXiK9svqTNjWepdPDnc/GAfoCHxq5qxqPnOB6EvZpLP8ADPGCv6uN26Mdc+oH4Cjxgr2CvMH+F+SuNHYD/mMt/IL4CD40eY77k+AW8+Jd/6Y6zLtQi99q8NKfec16Pgr2CvWrRG+jHUQKwV330CPaqj5ypddr4HOMP8ID/GD9/RX1Bmvtc68/T8mMZW1+05rbHI532Hal86fty7SeVLq3zT3/tfVQTzvL5v/RffejllzVr+wE14+PULln1oz3w0F7QQHW+GfTD8qAf9b6c59TBxFBG0I9a/515viS3V1H21gdgOXKupeG0JU+zGo9cNhyZtMg8zaupq8SDmr9r9R+Un+Y+3/IfUtukkV2svyUv1V9W1w9p3+fY+FCmlD/XpvWIr7j9n0onlW1Mj/VzrJ9r5iEwf7W/+avDIx/5SPEX0KOhyW9GC/+dMmpUcKI1erV8PW8IqN0UV1LGXNlSmWplAB6nkgMPuidBP3RGn5asLgV4gEfuX+E/rnQJ+gH9gH7UfQv0A/oB/YB+BAlgPIjxoHQUAv8B/wH/Af8B/3G9D2A+kfYm8B/wH/Af8B/wH/AftLe4ngL+A/4D/gP+A/4D/gP+Y1sCmN/F/K5URxBfIb5CfIX4CvEV4iup7wjpZ/Uf+WUoqY3TvCf047z04//99V/QYp4637/4Fz+k2j7JepSHfpQaFs93UDysDvhZw6MOKFrx6H3+I+cR5G958NWKA9Wm/Lk3j5b3ipc8lMqoXQiQpqVk0dK2Wl5pnRIekvbWLj/RfAE9H++k7ajZK4l9ksqsJAeLMkK5Eh4S/zGDvZL0H6mMS+klfYCrT972yks/pOc6W1nV8s86/uitH+CxfcgfPLw08KpcC/9xePjDH37yBfQ4ybB1g0FsQn6TDfW7NKivvUQaNFA3I0iVsTV9bFtsF1cenPcPQWt+gwjVXm79pZuHOPJPjX5IDx7bixjgcZQA9EO32EXpu/Q57NVdl4exNDfzSOVNpQcP8EB8Vb+pGvoB/YB+QD9q4yWMPzD+4NgHjD8w/ij1E4zPMT6fYRyYz8d52KvSghR3fErN55bmEzXT3lR7ej/H+APjD058odEPzLcfxzXp+k76G+sf9XFfzQ7CXsFewV5hvgTzJRd3vtyN9XPZuB/+A/4D/gP+A/4D/kMzL+bpPzB/hfHg1nxBaT4F64NYH+TEM5hvl40TvObhPf0HxoPwH/Af2/Pt6Re9Net3pTzUAXSrenqX8y/8hQ++rFKz/kO118te/X//5Bepqi+fWxy8ZFdWSEhdApBm+ef//F+7/Onhz0s8Un2Jh8zTf1N/Fv576XfOI5e51YFkDQdJ3XnanIe0/vzQfv771q1bLvO7FI/ae0hkRcnCoiyKRzqepuxXbT6ox3qtloeG0xaXVlu4Co8gI5wfPI6X0j9qP4eXPwcPXX8Ej7nGg3cOoFOOkXpOKWH6PJ/EiGVTztBqs2P+Lqkzpd6z9pz7/pLN0Nq2cIIG8KAXGy37G7d/pE5Fqg+W7U37HvTjdJIW9uooAcv+Bv3Y3pTLWUwBj6t+2eI7Jf4T/pyWNPwH/EeqJzX/Sfekegr4D/iPlv6DePeq/2B8zrPXLf0N9gr2qqX/wF7BXlGHEfNDOC39jVoU3rrpvaXeFfOWNlRL3xP+A/5D2mdq6TE+58V71Hw8eGB8jvXBq6Vky/loxLuId7nxroUdRnyF+MqiH8V10tayuP0R61G0pBHvIt7FelRdT2bTD4v2YP6KtovcFJi/ujrMx5VZng7+XCu56/ks7IMlD4v2wF7Z9Q/YK9grTryH+V29zpXs5z3/7Jf1BU6cMz2AHpvJtdeSw8otImjh0XrgsqXdaV7uIfT0wHPMb+nPrd4nL+f9//SXTv5TKneLg8it7ea2YeuA7aw8OPrB0QOujCQsWsvcM4+anDky5aSRcIxpV+FRu8QH+0V586maviO1n3G9AfHu1fnOKEPL9WhufAEe18/Z5jwOD33oQ0++gL512C0aodpiMPXfLZSw1r4SbKo9PZ6n7eK8f35DA3jIb2zY4goetvKk+if1HDzAg2uHOfazlkbSD7nt6ZEO+gH94PYz6ActAcRXvvoEe+UrX8qP5c/BAzzgP+o3/UM/oB/QD+jHlt/k9o8e6XrYK+qG99pzOvqup6DimpBT2y7PfNJ3zm9w57y3tl9J25am92yX9n04+XroB6cdW4eBNFzAoxyntKz7aDjki8JYjzrlAh6yeBr2SiYvyg62PgcP8OD6d/gPWgKYb/fVJ9grX/lK/Ql4gAf8x9j5RO18D+3NMH+F+Stf+wb/4SvfGf057NVdF6XDmSW5SG007JWvPsFe+crX215pLmjgxLf5AVup3s6a/j5/9q+cfIGZ4pOuW3LeyWu+hMODc+iW8w5WaTiH0DU8JIfRuH5Jegl77QC610FYDRNOW/I0/9yf+6viqqQ8OPYnPSTISb91IUDphTiyEQviAxlays7z9tAPjny9edRkLZGlJC2X7So8sJ/h+iXQnD7g5c/BAzwkdpeKU62eS8eDdw6gcxTJI03Lpk+P9tTKtLjVMJRduzmhdGNFz/eLdYFH/cYG8KhLAPpBT0L17D/gAR49+xv8x1EC1oOCVobw5/Dnmj4E/wH/oek3rXlgr2CvNH0I9gr2StNvWvPAXsFeafoQ7BXslabftOaBvYK90vQh2CvYK02/ac0DewV7pelDsFewV5p+05oH9gr2StOHYK9grzT9pjUP7BXslaYPwV7BXmn6TWse2CvYK00fgr2CvdL0m9Y8sFewV5o+1NNeWdWVv+eqX0APBzo9/zQXAnDac/M3/sFmstkOn8fGUofQW3iMPI+T8wjy9zgAy+kbW2moNlkcQI/154f4S4f/W9+nlr/Eo5aWkolFG7V1lA48a9szm35I30MjQ00eif6cq73iyB7xLuJdTj/J01jFoCPtFee9oR+FeYgHP/jBoi+gc07Kh5vrttJFWPnNN5KbcGIZtU5Xej7LjQHp+6cdN7RPemMfeNCTaxR38DjcEQElp17PwQM84D/qdg36Af2AfkA/gh3Yin97+WuqHtgr2CvYK9gr2Kvyl2EwX1L/Yk66IJtPVGK+ZFtulF/WPoc/hz+HP4c/hz+HP+esP+R+Bv4D/gP+A/4D/gP+A/7jdOsK9jPQ6/mYL8F8CWf/j3Z+wzof4t3/v73zanLlNqLwpdbZfnUOJVlyKrvs36SrUMo/1uVsy5Kcw6uzr69csywssSAwfRpokEPux5d7ucRggD7dp7sxaAzxLvEu8S7xLvEu8S7xrje+IN4l3iXebccP2Me27OPm5ubR2pujky4vuC3tWt89RT2KfaT7nfvffL7lHGfVf1gF6Iqst9jm5oUfDNctnCM/9+Bx7sMBrEMAcr2Yicdit0+ePBnGu4y/bvstDmhoyTy6QHnNpnruVV4zE4+Z+69UPFry65Fd6mvk2nI814IH61esX7F+xfpV9PrV7p133qm+AT3qVIKeoHWkqCYPJsvNy7NOlxqVVT7fPCHLZTd6jx4clKTOWkwDjxHJ16/FPo7lgn3oejYqK/hKl7XSEjy04B5/rmiT3Qb/gf+wtaTdAr6Cr0b0x3stfAVfeXUmMncm3h2Rfrzuggd4eCSA/4i3QY/8y7bgAR4j+kP+Qf4xoj/ea+Er+MqrM+QfWtEZzwdHNIvng4sEyAdjdYj4ivgqVqPWeyO+Ir4a0Tf4Cr4a0R/vtfAVfOXVGfJB8sERnRm5Fr6Cr0b0JzK+mrUHf2R+a9eyv32WZNv9PmS+mmUfT9/94emBPMEdn3n++0d3uQS+UvE4d/F5Eq5ahH4teNTkHlmYrJqG955l+2vGoyVDr8xq/UT0sfR7ajyoH1Qtq90u0n+AB3hYEnjI8W6zAL2n0t1K1tLv1ollaj+tN2tYgJcPrdP3WePyzqc2fvCwT0b3ylltDx77TR7YR+wb5xSeSm1qTgo8wKPmF+Ar+GpGvABf9fHNLJ4GD/A4lZ3PuA/5oKa/+HP8+Qz7w39o9lc7MRk87E2vuX7NkBf+Q9Nf/Af+Y4b94T80+8N/+OQEX8FX8BXxVS1+zJ+HzFpXU58LpnbwFXwFX8FX8FXfG0K8fLvGu62chOfnB/uc5TfJB315DutXmryIr4iviK+Ir4iviK96eAD/sR3/0YoR196YHfHGak9sOqNtml/qu/w+455Kn62C5x47U/M48o923DvNPh5QAbqi96NthvlKwGMrxedJVkoReq3geVTWyvXReJSyjypIVuZStvHcWyl47hmD95rZeLTG45GVNaeIvk6NB/58jyr1avH1kpa9kJ+Tn3vzht3bb7999wZ0z8WLsi0JR8/DII8ie9uunV7hmd9IcXttzOqpGrkD8YwXPHyaAh7H8vLoG/bh07dWa/hq77S38gEP8FB0Ef+B/1D0ZHYb+Aq+UnQMvoKvFD2Z3Qa+gq8UHYOv4CtFT2a3ga/gK0XH4Cv4StGT2W3gK/hK0TH4Cr5S9GR2G/gKvlJ0DL6CrxQ9md0GvoKvFB2Dr+ArRU9mt4Gv4CtFx+Ar+ErRk9lt4Cv4StEx+Oph8lUqRld05BRtlvG0PiNFeKNjXxtX3nf+hk7P/u+lD+o/dJROxVdN+3jvR/pgL6nlc9/rGu2p7KMXj60Vnychm0XoV4JHLv+IQuQuJc0uUsdw1O4K8WjJUpWRB4vRPi8Nj2Y9Z+E/IvnJ4pSp9+q0j1P5c+prNWsFj8vMB3dvvfXWh2Uxaau4tDSGmnGsvU5+EVHr9zL5SQmOJymy5tH6PUFnza+m5Op8I+ehjndN3qkP8Bg/KQQ89hLAPrQTnXt5SuGnvJgbPMCj5nfgK/iK+Mo+UdziW+IrPZ4fiX/hK/gKvoKvLD4m3iXeJd49HOZVs5dymZL1q/H1n971UvgKvoKv4KvaeiDxLvEu8e5hfYHng3qcwnoJ6yX4D/wH/gP/0bPujv/Af+A/8B/4D/wH/uOYB1T/yPou67us77K+y/qujwdUfmX/FfuveuITntfu7rYBUP+hr6uP1g/lfLV7/8daZd2ltSoKOi9lf0mJRyp6HS1AjoRPGYtV8HzpeLTkqcimF4uRvh8qHqqsreLz1E9UEfq14GHVb5a/8/xc9/PkH6d7/rF78803w18/m5Q9LcCtBbkq2JEnQainJbRIdMSJt+a7nJS09DvjbcDg4X+YBB5qCHHcDvvYF+NH2TJ8tbffWlCF/9hLAL6Cr7CP9sMu7AP7wD6wj5oVEF8RX1mb7fAf+A/8B/4D/3HY9ML6rr45Ff+B/8B/4D/wH/iPtU2TPB88thDyc/Jz8vN2/IR9YB/YB/ZR5hfk5+TnStaN/8B/4D/wH/iPvQTIzxWvcWiD/8B/4D/wH/gP/IfPc+xbP0T/cfObn/aIavPXfPjsd5svGa09/1QPg5j9/LzEYyl4HSk8ngWUNaby92vCoyVTSyYRWPTe4yHiocpbLT5P/UUUoV8LHgtvsr7L+q5ia1uOr3avv/76vTegp4Aw4mSp1olB3pOElE075xhvOY8UJI3M7+nTp3fFq0lxvHKMat/aDAMevhP9wGMvL+zjYDnwlX4ijWo/8NWjR/iPOL2Cr+CrmTwNX8FXkfoFX8FXkfqkPhwgHyQf7NE7+Aq+6tEb8sH7h9utHXZHPkg+OLIenecouZ+fabeqfeM/8B8z9ZD8nPw8Ur/gK/gqUp/Iz49XHoh3iXeJd9uHn2Mf2Af2gX3MjEOSfhHvXl+8W3uphrpeE92O/Jz8PJLH4Kvr4yviXeJd4l3i3Ug/0dIn/Af+Q9WzZz74iVI7dnFt/ve179yrX8rlsWX78OARUQg7AqyncHc2HrPywRKPlsx7C8N75N9zr/Kaa8dDlatHh/M+R23v1HiQf5B/kH+084/dG2+88aESNKnEstZutBLfGoMyj3KzZgqKrL5P+bsyj4jxgIcmRfA4vLlGk9jcVuABHjM0TNEr/MdB8vgPTQsVvdJ6Wm8FHpoUwQP/oWmKr5WiV/gP/IdPq9ZP5F8rPvTeB/+hSUyxc60n/HmEnMADfx6hR2Ufil7hz/HnXt1T9MrbZ609/lyTInjgPzRN8bVS9Ar/gf/waRX5YG3Tk1eGke0VO4+4H/5ckyJ44M81TfG1UvQKf44/92kV/hx/7tUYrT18pckptSK+8snrIbaOfN4WJT/FziPuhX1oUgQP8g9NU3ytFL0i/yD/8GkV+Qf5h1djtPbwVVtOH/ntzzQhXlirpcB29kfRK+8YVDxGC2C942q1Vwt4T4FH1JzyfmpvpC/v01MQPjpW7z1rBc+jYzjH9Qoe6rgUGSpt1Pvl7c6Bxwy+qs2d/FzTCPDYTn6+e+211+69AT0PxstkVlHwBG5ShdZ3RQk8J2Yu91Pbp7HV5mclI575WSf+1MZbnpgBHuvGAh7aCSPYR905efRH5RelHXiAxyKBNf+I/9DeLIo/390ZU3RcleunwmtWvEJ8pflrC0f8B/4D/9H2D9gH9oF9YB9lPGItUZIPHuIT8g/yj9JesA/sIz0HIB889ibYB/aBfbSjLOwD+8A+sA9lfwD5B/kH+cfeX5T2svhR7AP7wD6wj1oezvMPnn/Mev6x+J1ln2L6N0m69t1ab0/9pHat7/n9yvv3fLfGFfW7d37e+7Jf9LA/VDkcwLP+QHxFfEV8RXxFfLW3Auo/2vt1yc/v72sk/yD/mJV/lHycJD1Sr6YUoG+l+DzNVylC/+9XvlVdP4zkqxn5oFXwPKtAWcm/PPcu2y54LPJS811lPHmbWp4ckZ9beKjjHJFdfo9eWzwHHjP4qrTfmj2r+3OUdvhz/PkMf35bgK6Sh9rOs8iTK3/ef09xuDo+b7sy8eqd32hxlnfcFklZpAUe4aZxK9Je/QEP8FA4AL46HEbSSk4VOUa1AQ/wiNIl/Mf6ia0te1ceTvZihD9vF9WDR69WHa7Df+A/xrXo0AN8BV9F6lPZF3wFX0XqF3wFX0XqE3x1f1PZIg/W2+M0DL6Cr+K06bgn4iv4KlK/4Cv4KlKfiK+Ir1hv1y0Kf44/17XFbok/x5/bWtLfAr6Cr/q15/jKCL6yiuEjx2v1lRcYLG29Rehqcf/SbsYnAg/WE+OQAQ/8eZw2sZ5Y2hPPP2K1C76Cr2I16n5v5B/kH5H6tfDVx37/i9UuewteI8dZ68sqQn/y1W931xuds16txCOXv6eIeZb81THU3rh9ifngGh6qjFWZ5f31XLM2nmvBo3YobZp36+V1uVzIz1WttdsR7/bHu7tXX3316A3oyokIa29o7Dk5sNafDf2hRUsJ8r+Pzmv0+jTa3PjLYLp2ouXofcGjfpIjeBzkki/CjOpb7/XgAR69J/vjP/bWg/+IecP0WnyzFrzjz08nf+VkMfAAj0iNgKYAAAwxSURBVNF4RMlDyD/aekb+Qf6BfWAfXh4mHyQfJB/0Le4S7xLvenm2p1gIf44/9+oZ/hx/jj/Hny88gP/Af+A/2iuL12If+SZGZR1VaePVm1Z75V6eNlHj6u1Hia/A43T5oYIHz2u3hQf2sS08sA/wUP2hx1dfS3yFfWAf2Md+XS3tYc15APvAPrAP7IP97eM8QHxV38el8ktqx364/v1ws/Lzj/7u5x71vpi2//nyN+8Ol/fqqbJ+dSo8UgF6dEHyCJDKWMo2s/GYFe+W9uE9kEGRVQuLkWvLPsHD1njrUAsv9mt3vBY8lHohi39tZA4trmX96rYAPU0rF6IVJNWck0eAvW3LDZZr/VggrSmNZ3yeMVn95g4dPA5JkyW3/Hfw0KSFfWhyGmnl0UXwGJG0di146Mm/JtGxVuABHpYGeXTE6ov4al3fLPmlhXylndIGPMBD0ZNWGw83EF+NSFq7Fjzw55ameHTE6gv/gf+wdGTtd48u4j9GJK1dCx74D0tTPDpi9YX/wH9YOoL/2EuA51E8j/Laioeria+80vW3Bw/iK0trPDpi9ZX/nr/Zs3xLqKcf2sZIADxi5DjSC/nHtvIP+GpEm+OvxT62ZR+tggN1c23PYYcjWuWJZcg/RiStXQse5B+Wpnh0xOoL/7Fd/8F6IuuJlv2Wv3u4AX/ula6/PXjgzy2t8eiI1des/Nx6A7pnXFtquxQ8n+oTuZ5Ye+N2ZCFylEysMdUKbKPuXetnVrz78T/88t7tPEXIlowUeUT0sdyn7OffX/rG0e0j+WqLeKzJ2yo+T9d68Pfc71LxUHS4p41HFy8x3t298sorw29AT4ItFxdbi425UFtCS31GnCxgLY6m8VjjVxZPtzAf8DiYOniMn3yGfezuFGoL+gQe4DHTL+I/8B/R+rVIlPiq/eYn4t3+OAW+gq/gK9+b9Wp8nNtRyUfW92j5l/3hP/YIkH/0+4nWOhD+A/8RzV/wFXylrDv36B18BV/16M2aPsJX8BV81X7DCfaBfWAf2EetAIv9DOtvhlqKzcvNiZ7vKdpL/bS+539P/0/3sb57xtMzn0PEOva/LcwHPA4YRuAx6lfIB2PzwR77zvkD+8A+rjU/H/Ne13V1xH6GUYlE6Bn+I9Z/4M/3+/Mi7GMLzzuxD+wjgmdzXsA+WE8c9RPsZ9ivO6198B8x+2W2xFcz8vNP/PFXo6H4Jq+3CtAj1q/OjUdUMWwvgGrR7tL/KfAY9Su1eLe0D1XmUYXjy5gi+lIK0HM92Kr/6MVjTcc9erz0o+qA5561AvRLwKPmH3M7Sv/fgj5tqX5w9/LLL99GcNZiQalEnsp8L7GPgtQzH+8Yve2Vh9TLvFNADR5eCfvag8f95E05XMEnYV9r8AAPn8a0W+M/8OdRutTqB76Cr6J0DL6Cr6J0Cb463gxLPhirXfAVfBWrUce9EV8RX0XpGHwFX0XpEvEV8VWKJ2fpFHwFX83SLc9DUZ5HzUbh0D/xLvFulLbhP/AfUbpEvFuPd9Vi+Nk4pP7zN8q3NpUuf7/W/SXgEadp+I/r8x/YB/axJgFv/uHVp9w/xSGx72m0iCJ6PKfsb5l7z/NN9u/6UPLaB/t3ffL1tgYP1ku8OtNqT7x7ffGul3+jdIn1Ep4P8nzQb03n9ufXWoD+ry++4Afj0aNH515PVPGIKILtElBxkVq8ey14KHKPKBgvsRnts7y+F4+t8ZWCx5qeq/pb9hF930vFY4RDHnL+cVuArp6Y4RHyyIlhi/Mbud57MpQyr2U8s8alyr9sp4w7tRmR56x5p7H1FD+DR8zJVrlegMf6Sf4tO8U+NLnBV3P8Gv7DPgmx1D38B/7Dshv4Cr4aiZtb8TrxrhYvJDkRX2nygq/gK/iqHddgH9gH9oF91HiAfJB8kHyw7SGxD+wD+8A+avET+Tn5uZVb4T/wH/gP/Af+o58HLI7Nfx9Z58Cf488tXcOf99vxpe9PVN94Z+lQFF/d3NzcFaGnYvSIf9P4ymJ7ZV6LfcwaVyl/K67qfd6s9lu2w3/gPywbwX88XP+h8oqlQ1H+A76Cryxdg6/gK4u3LB2CrzSeseSs/g4el7vf55N/eteEb7T41byBs4FSxPvPLzx/97JfVY9ntfNMr8TDkv1oofja2Eb6Lq99KHiMynNE5p57XyoerLf3xYe7l1566fYN6DM+1imLHtDS+HoW0/K5WWMakUPPfPJ5Lf9PJ1SPjKN1rTX3nvGDRz9S4KFvhk6ywj769a28ssfe4atdHABFT+DhF63Fof4eD1eAh1964NGWWY8+EV/5dbD0Ua0ewOMgGeKrfj2L1C/iK+KrSH3Cf/TbtRXL4D/wH5aO9Gvffi1QffhU3gd/PiL5+rXg4Zcp9kE+WLMb+GovAewD+8A+2jqAfWAf2Af2UZOAxQ098TrrJf4Yn/V2O08n3iXeha/Y72Oxq6Uj1vVrv/f4Q55H8TyK51HHvIU/x59bXN3Dt+Qf/R4OPIivLO2xdMS6nvjq/sue4Kt+jbF0Ef/B/hJLR/q1b9v7Sz7151+vTs0qgB6Ry8i1VhH6UmAbmU+pa68jc1qu9RSgzypWzufQe49aAfqobM5xvQePtfF55Ohpq8rk1HhQP6giY7friU9Kvto9fvz4rgC9FkyWDjDdNHXU+u4ZnLrJMm+33L8cr/V9uWbm+CNOUltO7sxBKg0GPOyHfUkPwGM9WazZO/ah65eHt+CrPavN1C/w2N2eBI3/ODw0HIlD8B/4D/iq7Q+xD+wD+8A+anEX8S7xrhWP4z/wH/gP/Af+4/hBu7W+Xv4+kudaPJ3WnK31ddYTfQ/1Fbm3cCW+Ir6y9If4iviK+Ir4iviK+KonPsR/4D/wH/gP/Af+A//h2xdEfk5+for8fOaGansL8liLnvXEsTuuX23hpfzO/qs9T0asT5N/kH+Qf5B/kH+Qf5B/kH8sPEB8ta34alb+YRWgz8wDZvb9j89/vdm9kl9YPJjbxxI/59+XGy9/yz/L7/nfWt/VgucZRcotgfXcq7zm75977nb+ad7lv6O6cG481sYfIb+8/55DIc6JB/XOY/uVIvLzewXoo8bm3TSX389ajIoam9KPWuS9djL7luajJvHgoWjHeBvwsA+PGJey3gN4gIeuLXZL/Me29Ak8wMO22v4W+I9t6Rd4gEe/NR9fif/Ylj6BB3hE2nfZF/5jW/oFHuARae/4j23pE3iAR6R9488Pm5WSLHgeFadh8BV8FadNxz0R725Lv8ADPCLtHf+xLX0CD/CItG/yD/KPmfoEX8FXeV4frWtqUUL0fa+hvxkFOeQf27J38ACPSK7Cn29Ln8ADPCLtm3yQfHCmPp2Tr8p7R8xzyT8+89f3I7raXB9rBegRg52Vf5R41IqNewqaR+fsvWftjdut4vO14vFacf/oXNTrF4wVPNb688ot72vk2nJM14JH7dC1NNcyX1z+vqX9GGf1Hy+++OLdG9BVAyjbRZzcYZ3skRNr7eSC2pjyv7UWDZT7Rs4vV76y3+W3CAcSOd41+eSLoNa4R5R85nzAY48i9uE7YUzhDexD8ygz7TvHCTzAQ7Fbjz7iP/AfHn3x6B98BV959EXRQ/gKvlL0pEfv4Cv4qkdv1vQRvoKv4Kv9mzBqcsA+sA/sA/vg+cdBArPsofa8pvYwsRYF8vwjfn0dPLR8g+e1bf9g5SvEV8RXs/wJ6yUaf82SP/5Dkz/+A//B/ivdVuAr3V6Ir4ivZtkL8ZXGWWtvgFvbnO+9Lo1G2byvvqkvcnzKfCyJlm8ctNq31oqsvDTid+xDQ2cWP5F/aPIn/9DjqZIXiK+uI77y7M+PeDOl4l8esv8AjznPc3r5Cjy2hYfHs9fi7mt9A/rfPvvs6hu3rXzGkuus/OPTf3nv3q3LAvTIomRrjuXvnnuXbRc8RuL7ZSytvHH5zaqLVOZaG5+Fx1q/Hnm1+onoY+n7WvBQ4iWvnj2E+GoXUYCuGBFtkAASQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIAAkggcuXwLW+AX0peL7Ez9obt6OKkUfkoo6hVvA8ct9zXdv7BnRVTsq8Ivq6FjwUedHmWAL/B/9g/k0R1ILBAAAAAElFTkSuQmCC', ],
  }
  class Animation {
    constructor(animation) {
      this.totalFrames = animation.totalFrames - 1;
      this.frameSpeed = animation.frameSpeed;
      this.frameImages = animation.frameImages;
      this.spriteSheet = animation.spriteSheet;
      this.frames = [];
      this.x = animation.x;
      this.y = animation.y
      this.currentFrame = 0;
      var l = 0;
      while (l < this.frameImages.length) {
        this.frames.push(new Image());
        this.frames[l].src = this.frameImages[l];
        l++;
      }
    }
    loopRunSpriteSheet(callback, param) {
      this.frame = 0;
      this.interval = setInterval(function(gif, callback, param) {
        draw.clearRect(0, 0, 500, 500);
        draw.drawImage(gif.frames[0], gif.frame * 100, 0, 100, 100, 0, 0, 500, 500);
        gif.frame++;
        if (gif.frame > gif.totalFrames) {
          clearInterval(gif.interval);
          callback(param);
        }
      }, this.frameSpeed, this, callback, param);
    }
    start() {
      this.interval = window.setInterval(function(gif) {
        draw.drawImage(gif.frames[gif.currentFrame], gif.x, gif.y);
        if (gif.currentFrame == gif.totalFrames) {
          gif.currentFrame = 0;
        } else {
          gif.currentFrame++;
        }
      }, this.frameSpeed, this);
    }
    loopRun(reset) {
      this.interval = window.setInterval(function(gif) {
        if (gif.currentFrame == gif.totalFrames) {
          if (reset == 'toolkit-reset') user.tank.toolkitAnimation = false;
          if (reset == 'toolkit-reset2') user.joiner.tank.toolkitAnimation = false;
          gif.endRun(gif);
        } else {
          gif.currentFrame++;
        }
      }, this.frameSpeed, this);
    }
    endRun(gif) {
      window.clearInterval(gif.interval);
    }
    drawFrame() {
      draw.drawImage(this.frames[this.currentFrame], this.x, this.y);
    }
    stop() {
      clearInterval(this.interval);
    }
  }
  class Tank {
    draw() {
      if (this.invis) {
        draw.globalAlpha = .5;
      }
      if (this.leftright == true) {
        draw.translate(this.x + 20, this.y + 20)
        draw.rotate(90 * Math.PI / 180);
        if (this.base > 2) {
          if (this.material == 'normal') {
            draw.drawImage(tank_base_png, -20, -20);
          } else if (this.material == 'iron') {
            draw.drawImage(iron_tank_base, -20, -20);
          } else if (this.material == 'diamond') {
            draw.drawImage(diamond_tank_base, -20, -20);
          } else if (this.material == 'dark') {
            draw.drawImage(dark_tank_base, -20, -20);
          } else if (this.material == 'light') {
            draw.drawImage(light_tank_base, -20, -20);
          }
        } else {
          if (this.material == 'normal') {
            draw.drawImage(tank_base2, -20, -20);
          } else if (this.material == 'iron') {
            draw.drawImage(iron_tank_base2, -20, -20);
          } else if (this.material == 'diamond') {
            draw.drawImage(diamond_tank_base2, -20, -20)
          } else if (this.material == 'dark') {
            draw.drawImage(dark_tank_base2, -20, -20);
          } else if (this.material == 'light') {
            draw.drawImage(light_tank_base2, -20, -20);
          }
        }
        draw.rotate(-90 * Math.PI / 180);
        draw.translate((-this.x - 20), (-this.y - 20));
      } else {
        if (this.base > 2) {
          if (this.material == 'normal') {
            draw.drawImage(tank_base_png, this.x, this.y);
          } else if (this.material == 'iron') {
            draw.drawImage(iron_tank_base, this.x, this.y);
          } else if (this.material == 'diamond') {
            draw.drawImage(diamond_tank_base, this.x, this.y)
          } else if (this.material == 'dark') {
            draw.drawImage(dark_tank_base, this.x, this.y);
          } else if (this.material == 'light') {
            draw.drawImage(light_tank_base, this.x, this.y);
          }
        } else {
          if (this.material == 'normal') {
            draw.drawImage(tank_base2, this.x, this.y);
          } else if (this.material == 'iron') {
            draw.drawImage(iron_tank_base2, this.x, this.y);
          } else if (this.material == 'diamond') {
            draw.drawImage(diamond_tank_base2, this.x, this.y)
          } else if (this.material == 'dark') {
            draw.drawImage(dark_tank_base2, this.x, this.y);
          } else if (this.material == 'light') {
            draw.drawImage(light_tank_base2, this.x, this.y)
          }
        }
      }
      draw.translate(this.x + 20, this.y + 20);
      draw.rotate(this.rotation * Math.PI / 180);
      if (this.material == 'normal') {
        draw.drawImage(tank_top_png, -20, -20 + this.pushback);
      } else if (this.material == 'iron') {
        draw.drawImage(iron_tank_top, -20, -20 + this.pushback);
      } else if (this.material == 'diamond') {
        draw.drawImage(diamond_tank_top, -20, -20 + this.pushback);
      } else if (this.material == 'dark') {
        draw.drawImage(dark_tank_top, -20, -20 + this.pushback);
      } else if (this.material == 'light') {
        draw.drawImage(light_tank_top, -20, -20 + this.pushback);
      }
      if (userData.cosmetic != undefined) {
        var l = 0;
        while (l < cosmetics.length) {
          if (cosmetics[l].name == userData.cosmetic) {
            draw.drawImage(cosmetics[l].image, -20, -20 + this.pushback);
          }
          l++;
        }
      }
      if (this.pushback != 0) {
        this.pushback += 1;
      }
      draw.rotate(-(this.rotation * Math.PI / 180));
      draw.translate(-this.x - 20, -this.y - 20);
      // Health Bar!
      draw.fillStyle = '#000000';
      draw.fillRect(this.x, this.y + 50, 40, 5);
      draw.fillStyle = '#90EE90';
      draw.fillRect(this.x + 2, this.y + 51, 36 * this.health / (userData.health * (1 + .02 * userData.rank)), 3);
      if (user.tank.shields != 0) {
        draw.strokeStyle = '#7DF9FF';
        draw.globalAlpha = .2;
        draw.lineWidth = 5;
        draw.beginPath();
        draw.arc(this.x + 20, this.y + 20, 33, 0, Math.PI * 2);
        draw.fill();
        draw.globalAlpha = 1;
      }
      if (this.invis) {
        draw.globalAlpha = 1;
      }
    }
    check() {
      var results = ai_check(this.x, this.y, false);
      if (results[0]) {
        console.log(results[2] + '/' + this.team)
      }
      if (results[0] && !user.tank.inactive && results[2] != this.team) {
        if (user.tank.shields > 0) {
          user.tank.shields -= 1;
          return;
        }
        if (user.tank.immune) {
          return;
        }
        draw.fillStyle = '#FF0000';
        draw.fillRect(this.x, this.y, 40, 40);
        //clearInterval(this.healInterval);
        this.health -= results[1];
        this.fullHealthBonus = false;
        if (user.tank.health < 60 && userData.kit == 'autoheal') {
          if (user.tank.CanToolkit) {
            if (user.tank.health < userData.health * (1 + .02 * userData.rank)) {
              if (userData.toolkits > 0) {
                toolkitAnimation = new Animation(toolkitOpt);
                user.tank.toolkitAnimation = true;
                toolkitAnimation.loopRun();
                user.tank.CanToolkit = false;
                userData.toolkits -= 1;
                user.tank.health = userData.health * .75;
                var cooldown = 60000;
                if (userData.kit == 'cooldown') {
                  cooldown *= .9;
                }
                window.setTimeout(function() {
                  user.tank.CanToolkit = true;
                }, cooldown);
              }
            }
          }
        }
        if (this.health <= 0) {
          if (Game.level == 'multiplayer') {
            if (user.tank.team == 'red') {
              user.tank.x = 50;
              user.tank.y = -500;
            } else {
              user.tank.x = -100;
              user.tank.y = -500;
            }
            user.tank.inactive = true;
            window.setTimeout(function() {
              user.tank.inactive = false;
              user.tank.health = userData.health;
            }, 10000)
          } else {
            this.inactive = true;
            window.clearInterval(endlessRunner);
            window.clearInterval(this.checkloop);
            window.clearInterval(this.interval);
            window.clearInterval(this.interval2)
            this.checkloop = undefined;
            this.interval = undefined;
            this.interval2 = undefined;
            setTimeout(defeat, 20);
            Game.endGame();
          }
        }
      }
    }
    initialize() {
      if (userData.health == 200) {
        this.material = 'normal';
      } else if (userData.health == 300) {
        this.material = 'iron';
      } else if (userData.health == 400) {
        this.material = 'diamond';
      } else if (userData.health == 500) {
        this.material = 'dark';
      } else if (userData.health == 600) {
        this.material = 'light';
      }
      this.canGrapple = true;
      this.fullHealthBonus = true;
      this.fireType = 1;
      this.team = 'Player';
      this.canBlockShield = true;
      this.toolkitAnimation = false;
      this.canFire = true;
      this.base = 0;
      this.pushback = 0;
      this.shields = 0;
      this.canShield = true;
      this.canFireFlashbang = true;
      this.scaffolding = [];
      this.canPlaceScaffolding = true;
      this.speed = 2;
      this.intervals = [];
      this.helper = [];
      this.count = 0;
      this.count2 = 0;
      this.health = userData.health * (1 + .02 * userData.rank);
      this.r = 0;
      this.x = 0;
      this.y = 0;
      this.u = true;
      this.mode = 0;
      this.color = '#00ff00';
      this.inactive = false;
      this.rotation = 0;
      this.CanBoost = true;
      this.CanToolkit = true;
      this.leftright = true;
      this.immune = false;
      canvas.focus();
      this.draw();
      this.invis = false;
      this.canInvis = true;
      this.canChangeInvisStatus = true;
      this.class = userData.class;
      this.altAttack = true;
      this.megaAttack = true;
      canvas.addEventListener('keydown', tank_listener1);
      canvas.addEventListener('keyup', tank_listener2);
      document.addEventListener('mousemove', tank_listener3);
      document.addEventListener('mousedown', tank_listener4);
      document.addEventListener('mouseup', tank_listener5);
      canvas.addEventListener('touchstart', tank_listener6);
      canvas.addEventListener('touchmove', tank_listener6);
      canvas.addEventListener('touchend', tank_listener7);
      canvas.addEventListener('touchcancel', tank_listener7);
    }
    move(e) {
      e = e || event;
      if (this.inactive == false) {
        switch (e.keyCode) {
          case 68:
            if (checker(this.x + this.speed, this.y) != false) {
              this.x += this.speed;
              this.leftright = true;
              if (this.base == 5) {
                this.base = 0;
              } else {
                this.base += 1;
              }
              if (userData.class == 'builder' && this.speed == 8) {
                if (userData.blocks > 0) {
                  if ((this.x % 50 < 10 && this.x > 0) || (Math.abs(this.x) % 50 > 40 && this.x < 0)) {
                    var amountX = 1;
                    if (this.x < 0) {
                      amountX = 2;
                    }
                    var amountY = 0;
                    if (this.y < 0) {
                      amountY = 1;
                    }
                    weak((this.x - this.x % 50) / 50 - amountX, (this.y - this.y % 50) / 50 - amountY, true);
                    user.tank.scaffolding.push({
                      x: (this.x - this.x % 50) / 50 - amountX,
                      y: (this.y - this.y % 50) / 50 - amountY,
                      type: 'weak'
                    });
                  }
                }
              }
              // CHASE put in tank_track.play(); here
            }
            break;
          case 87:
            if (checker(this.x, this.y - this.speed) != false) {
              this.y -= this.speed;
              this.leftright = false;
              if (this.base == 5) {
                this.base = 0;
              } else {
                this.base += 1;
              }
              if (userData.class == 'builder' && this.speed == 8) {
                if (userData.blocks > 0) {
                  if (((this.y + 40) % 50 > 40 && this.y > 0) || (Math.abs(this.y + 40) % 50 < 10 && this.y < 0)) {
                    var amountX = 0;
                    if (this.x < 0) {
                      amountX = -1;
                    }
                    var amountY = 1;
                    if (this.y < 0) {
                      amountY = 0;
                    }
                    weak((this.x - this.x % 50) / 50 + amountX, ((this.y + 40) - ((this.y + 40) % 50)) / 50 + amountY, true);
                    user.tank.scaffolding.push({
                      x: (this.x - this.x % 50) / 50 + amountX,
                      y: ((this.y + 40) - ((this.y + 40) % 50)) / 50 + amountY,
                      type: 'weak'
                    });
                  }
                }
              } // optifine2
              // CHASE put in tank_track.play(); here
            }
            break;
          case 65:
            if (checker(this.x - this.speed, this.y) != false) {
              this.x -= this.speed;
              this.leftright = true;
              if (this.base == 5) {
                this.base = 0;
              } else {
                this.base += 1;
              }
              if (userData.class == 'builder' && this.speed == 8) {
                if (userData.blocks > 0) {
                  if (((this.x + 40) % 50 > 40 && this.x > 0) || (Math.abs(this.x + 40) % 50 < 10 && this.x < 0)) {
                    var amountX = 1;
                    if (this.x < 0) {
                      amountX = 0;
                    }
                    var amountY = 0;
                    if (this.y < 0) {
                      amountY = -1;
                    }
                    weak(((this.x + 40) - ((this.x + 40) % 50)) / 50 + amountX, (this.y - (this.y % 50)) / 50 + amountY, true);
                    user.tank.scaffolding.push({
                      x: ((this.x + 40) - ((this.x + 40) % 50)) / 50 + amountX,
                      y: (this.y - (this.y % 50)) / 50 + amountY,
                      type: 'weak'
                    });
                  }
                }
              } // optifine2
              // CHASE put in tank_track.play(); here
            }
            break;
          case 83:
            if (checker(this.x, this.y + this.speed) != false) {
              this.y += this.speed;
              this.leftright = false;
              if (this.base == 5) {
                this.base = 0;
              } else {
                this.base += 1;
              }
              if (userData.class == 'builder' && this.speed == 8) {
                if (userData.blocks > 0) {
                  if ((this.y % 50 < 10 && this.y > 0) || (Math.abs(this.y) % 50 > 40 && this.y < 0)) {
                    var amountX = 0;
                    if (this.x < 0) {
                      amountX = 1;
                    }
                    var amountY = 1;
                    if (this.y < 0) {
                      amountY = 2;
                    }
                    weak((this.x - this.x % 50) / 50 - amountX, (this.y - this.y % 50) / 50 - amountY, true);
                    user.tank.scaffolding.push({
                      x: (this.x - this.x % 50) / 50 - amountX,
                      y: (this.y - (this.y % 50)) / 50 - amountY,
                      type: 'weak'
                    });
                  }
                }
              }
              // CHASE put in tank_track.play(); here
            }
            break;
          case 16:
            if (userData.boosts > 0) {
              if (this.CanBoost) {
                userData.boosts -= 1;
                this.speed = 8;
                this.CanBoost = false;
                user.tank.immune = true;
                setTimeout(function() {
                  user.tank.speed = 2;
                  user.tank.immune = false;
                }, 500);
                var cooldown = 5000;
                if (userData.kit == 'cooldown') {
                  cooldown *= .9;
                }
                setTimeout(function() {
                  user.tank.CanBoost = true;
                }, cooldown);
              }
            }
            break;
          case 81:
            if (user.tank.CanToolkit) {
              if (user.tank.health < userData.health) {
                if (userData.toolkits > 0) {
                  toolkitAnimation = new Animation(toolkitOpt);
                  user.tank.toolkitAnimation = true;
                  toolkitAnimation.loopRun('toolkit-reset');
                  user.tank.CanToolkit = false;
                  userData.toolkits -= 1;
                  user.tank.healInterval = setInterval(function() {
                    if (user.tank.health < userData.health * (1 + .02 * userData.rank)) {
                      user.tank.health += 1;
                    } else {
                      clearInterval(user.tank.healInterval);
                    }
                  }, 10);
                  /*setTimeout(function() {
                    clearInterval(user.tank.healInterval);
                  }, 20000)*/
                  var cooldown = 60000;
                  if (userData.kit == 'cooldown') {
                    cooldown *= .9;
                  }
                  window.setTimeout(function() {
                    user.tank.CanToolkit = true;
                  }, cooldown);
                }
              }
            }
            break;
          case 32:
            if (this.canPlaceScaffolding) {
              if (userData.blocks > 0) {
                this.canPlaceScaffolding = false;
                userData.blocks -= 1;
                if (this.rotation >= 337.5 || this.rotation < 22.5) {
                  if (userData.class == 'builder') {
                    gold((this.x - 5) / 50, (this.y + 40) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x - 5) / 50,
                      y: (this.y + 40) / 50,
                      type: 'strong'
                    });
                  } else {
                    weak((this.x - 5) / 50, (this.y + 40) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x - 5) / 50,
                      y: (this.y + 40) / 50,
                      type: 'weak'
                    });
                  }
                }
                if (this.rotation >= 22.5 && this.rotation < 67.5) {
                  if (userData.class == 'builder') {
                    gold((this.x - 50) / 50, (this.y + 40) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x - 50) / 50,
                      y: (this.y + 40) / 50,
                      type: 'strong'
                    });
                  } else {
                    weak((this.x - 50) / 50, (this.y + 40) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x - 50) / 50,
                      y: (this.y + 40) / 50,
                      type: 'weak'
                    });
                  }
                }
                if (this.rotation >= 67.5 && this.rotation < 112.5) {
                  if (userData.class == 'builder') {
                    gold((this.x - 50) / 50, (this.y - 5) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x - 50) / 50,
                      y: (this.y - 5) / 50,
                      type: 'strong'
                    })
                  } else {
                    weak((this.x - 50) / 50, (this.y - 5) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x - 50) / 50,
                      y: (this.y - 5) / 50,
                      type: 'weak'
                    })
                  }
                }
                if (this.rotation >= 112.5 && this.rotation < 157.5) {
                  if (userData.class == 'builder') {
                    gold((this.x - 50) / 50, (this.y - 50) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x - 50) / 50,
                      y: (this.y - 50) / 50,
                      type: 'strong'
                    });
                  } else {
                    weak((this.x - 50) / 50, (this.y - 50) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x - 50) / 50,
                      y: (this.y - 50) / 50,
                      type: 'weak'
                    });
                  }
                }
                if (this.rotation >= 157.5 && this.rotation < 202.5) {
                  if (userData.class == 'builder') {
                    gold((this.x - 5) / 50, (this.y - 50) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x - 5) / 50,
                      y: (this.y - 50) / 50,
                      type: 'strong'
                    });
                  } else {
                    weak((this.x - 5) / 50, (this.y - 50) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x - 5) / 50,
                      y: (this.y - 50) / 50,
                      type: 'weak'
                    })
                  }
                }
                if (this.rotation >= 202.5 && this.rotation < 247.5) {
                  if (userData.class == 'builder') {
                    gold((this.x + 40) / 50, (this.y - 50) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x + 40) / 50,
                      y: (this.y - 50) / 50,
                      type: 'strong'
                    });
                  } else {
                    weak((this.x + 40) / 50, (this.y - 50) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x + 40) / 50,
                      y: (this.y - 50) / 50,
                      type: 'weak'
                    });
                  }
                }
                if (this.rotation >= 247.5 && this.rotation < 292.5) {
                  if (userData.class == 'builder') {
                    gold((this.x + 40) / 50, (this.y - 5) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x + 40) / 50,
                      y: (this.y - 5) / 50,
                      type: 'strong'
                    })
                  } else {
                    weak((this.x + 40) / 50, (this.y - 5) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x + 40) / 50,
                      y: (this.y - 5) / 50,
                      type: 'weak'
                    });
                  }
                }
                if (this.rotation >= 292.5 && this.rotation < 337.5) {
                  if (userData.class == 'builder') {
                    gold((this.x + 40) / 50, (this.y + 40) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x + 40) / 50,
                      y: (this.y + 40) / 50,
                      type: 'strong'
                    });
                  } else {
                    weak((this.x + 40) / 50, (this.y + 40) / 50, true);
                    user.tank.scaffolding.push({
                      x: (this.x + 40) / 50,
                      y: (this.y + 40) / 50,
                      type: 'weak'
                    });
                  }
                }
                var cooldown = 5000;
                if (userData.kit == 'cooldown') {
                  cooldown *= .9;
                }
                setTimeout(function() {
                  user.tank.canPlaceScaffolding = true;
                }, cooldown);
              }
            }
            break;
          case 69:
            if (userData.flashbangs > 0) {
              if (this.canFireFlashbang) {
                userData.flashbangs -= 1;
                Game.flashbanged = true;
                this.canFireFlashbang = false;
                var blocks = checker2(user.tank.x, user.tank.y);
                var g = 0;
                while (g < blocks.length) {
                  let isScaffolding = false;
                  var q = 0;
                  var len = user.tank.scaffolding.length
                  while (q < len) {
                    if (b[blocks[g]].x == user.tank.scaffolding[q].x) {
                      if (b[blocks[g]].y == user.tank.scaffolding[q].y) {
                        isScaffolding = true;
                      }
                    }
                    q++;
                  }
                  if (isScaffolding) {
                    var q = 0;
                    while (q < user.tank.scaffolding.length) {
                      if (b[blocks[g]].y == user.tank.scaffolding[q].y) {
                        if (b[blocks[g]].x == user.tank.scaffolding[q].x) {
                          user.tank.scaffolding.splice(q, 1);
                        }
                      }
                      q++;
                    }
                  }
                  b[blocks[g]].destroy(l);
                  g++;
                }
                setTimeout(function() {
                  Game.flashbanged = false;
                }, 10000);
                var cooldown = 200;
                if (userData.kit == 'cooldown') {
                  cooldown *= .9;
                }
                setTimeout(function() {
                  user.tank.canFireFlashbang = true;
                }, cooldown);
              }
            }
            break;
          case 70:
            if (this.class === 'stealth') {
              if (this.canChangeInvisStatus) {
                if (this.invis) {
                  this.canChangeInvisStatus = false;
                  window.setTimeout(function() {
                    user.tank.canChangeInvisStatus = true;
                  }, 500);
                  this.invis = false;
                  var cooldown = 40000;
                  if (userData.kit == 'cooldown') {
                    cooldown *= .9;
                  }
                  window.setTimeout(function() {
                    user.tank.canInvis = true;
                  }, cooldown);
                } else {
                  if (this.canInvis) {
                    this.canChangeInvisStatus = false;
                    window.setTimeout(function() {
                      user.tank.canChangeInvisStatus = true;
                    }, 500);
                    this.invis = true;
                    this.canInvis = false;
                  }
                }
              }
            } else if (this.class === 'normal') {
              if (this.canShield) {
                user.tank.shields = 5;
                this.canShield = false;
                setTimeout(function() {
                  user.tank.shields = 0;
                }, 10000);
                var cooldown = 40000;
                if (userData.kit == 'cooldown') {
                  cooldown *= .9;
                }
                setTimeout(function() {
                  user.tank.canShield = true;
                }, cooldown);
              }
            } else if (this.class == 'tactical') {
              if (this.megaAttack) {
                user.tank.fire('megaAttack');
                this.megaAttack = false;
                var cooldown = 20000;
                if (userData.kit == 'cooldown') {
                  cooldown *= .9;
                }
                setTimeout(function() {
                  user.tank.megaAttack = true;
                }, cooldown);
              }
            } else if (this.class == 'builder') {
              if (this.canBlockShield) {
                if (userData.blocks >= 8) {
                  user.tank.canBlockShield = false;
                  this.shields += 10;
                  var cooldown = 30000;
                  if (userData.kit == 'cooldown') {
                    cooldown *= .9;
                  }
                  setTimeout(function() {
                    user.tank.canBlockShield = true;
                  }, cooldown)
                }
              }
            }
            break;
          case 76:
            if (Game.level >= 10000) {
              window.clearInterval(endlessRunner);
              Game.endGame();
              document.exitFullscreen();
              setTimeout(function() {
                Game.endGame();
                victory();
              }, 5000);
            }
            break;
          case 9:
            var button = 0;
            if (user.tank.altAttack) {
              button = 2;
            }
            clearInterval(tankSupport);
            var cooldown = 0;
            if (user.tank.fireType == 1) {
              cooldown = 200;
            }
            if (user.tank.fireType == 2) {
              cooldown = 600;
            }
            if (user.tank.fireType == 3) {
              cooldown = 400;
            }
            if (user.tank.canFire) {
              tank_support(button);
              user.tank.canFire = false;
              setTimeout(function() {
                user.tank.canFire = true;
              }, cooldown);
            }
            tankSupport = window.setInterval(tank_support, cooldown, button);
            break;
          case 49:
            this.fireType = 1;
            clearInterval(tankSupport);
            break;
          case 50:
            //if (userData.fire2) {
            this.fireType = 2;
            clearInterval(tankSupport);
            //}
            break;
          case 51:
            //if (userData.fire3) {
            this.fireType = 3;
            clearInterval(tankSupport);
            //}
            break;
          case 82:
            if (this.canGrapple) {
              this.fire('grapple');
              this.canGrapple = false;
              setTimeout(function() {
                this.canGrapple = true;
              }.bind(this), 5000);
            }
            break;
          case 17:
            window.oncontextmenu = function() {};
            Ai.move = function() {};
            Ai.pathfind = function() {};
            Ai.fire = function() {};
            Shot.update = function() {};
            var l = 0;
            while (l < ai.length) {
              ai[l].move = function() {};
              ai[l].pathfind = function() {};
              ai[l].fire = function() {};
              l++;
            }
            var l = 0;
            while (l < s.length) {
              s[l].update = function() {};
              l++;
            }
            break;
        }
      }
    }
    fireCalc(rotation) {
      if (rotation < 0) {
        rotation = 360 + rotation;
      }
      if (rotation >= 360) {
        rotation = rotation - 360;
      }
      var xd = 1;
      var angle = rotation % 90;
      var yd = Math.abs(((xd * angle) - (90 * xd)) / angle);
      if (rotation < 90 || rotation > 270) {
        yd = Math.abs(yd);
      } else {
        yd = 0 - Math.abs(yd);
      }
      if (rotation > 180) {
        xd = Math.abs(xd);
      } else {
        xd = 0 - Math.abs(xd);
      }
      if (rotation == 0) {
        xd = 0;
        yd = 1;
      } else if (rotation == 90) {
        xd = -1;
        yd = 0;
      } else if (rotation == 180) {
        xd = 0;
        yd = -1;
      } else if (rotation == 270) {
        xd = 1;
        yd = 0;
      }
      return {
        xd: xd,
        yd: yd
      };
    }
    fire(button) {
      if (!user.tank.inactive) {
        var type;
        if (button == 'grapple') {
          this.data = this.fireCalc(this.rotation);
          this.xd = this.data.xd;
          this.yd = this.data.yd;
          if ((this.xd < 0 && this.yd < 0) || (this.xd > 0 && this.yd > 0)) {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.yd, this.xd, 'grapple', this.team, this.rotation, userData.rank));
          } else {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.xd, this.yd, 'grapple', this.team, this.rotation, userData.rank));
          }
          return;
        }
        if (this.fireType == 1) {
          if (button != 'megaAttack') {
            if (button == 0) {
              type = 'bullet';
            } else {
              if (this.altAttack) {
                type = 'powermissle';
                this.altAttack = false;
                var cooldown = 10000;
                if (userData.kit == 'cooldown') {
                  cooldown *= .9;
                }
                setTimeout(function() {
                  user.tank.altAttack = true;
                }, cooldown);
              } else {
                return;
              }
            }
          } else {
            type = 'megamissle';
          }
        } else if (this.fireType == 2) {
          type = 'shotgun_bullet';
        } else if (this.fireType == 3) {
          type = 'condensed_bullet';
        }
        this.pushback = -3;
        if (this.fireType == 1) {
          this.data = this.fireCalc(this.rotation);
          this.xd = this.data.xd;
          this.yd = this.data.yd;
          if ((this.xd < 0 && this.yd < 0) || (this.xd > 0 && this.yd > 0)) {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.yd, this.xd, type, this.team, this.rotation, userData.rank));
          } else {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.xd, this.yd, type, this.team, this.rotation, userData.rank));
          }
        } else if (this.fireType == 2) {
          this.data = this.fireCalc(this.rotation);
          this.xd = this.data.xd;
          this.yd = this.data.yd;
          if ((this.xd < 0 && this.yd < 0) || (this.xd > 0 && this.yd > 0)) {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.yd, this.xd, type, this.team, this.rotation, userData.rank));
          } else {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.xd, this.yd, type, this.team, this.rotation, userData.rank));
          }
          this.data = this.fireCalc(this.rotation - 5);
          this.xd = this.data.xd;
          this.yd = this.data.yd;
          if ((this.xd < 0 && this.yd < 0) || (this.xd > 0 && this.yd > 0)) {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.yd, this.xd, type, this.team, this.rotation, userData.rank));
          } else {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.xd, this.yd, type, this.team, this.rotation, userData.rank));
          }
          this.data = this.fireCalc(this.rotation + 5);
          this.xd = this.data.xd;
          this.yd = this.data.yd;
          if ((this.xd < 0 && this.yd < 0) || (this.xd > 0 && this.yd > 0)) {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.yd, this.xd, type, this.team, this.rotation, userData.rank));
          } else {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.xd, this.yd, type, this.team, this.rotation, userData.rank));
          }
          this.data = this.fireCalc(this.rotation - 10);
          this.xd = this.data.xd;
          this.yd = this.data.yd;
          if ((this.xd < 0 && this.yd < 0) || (this.xd > 0 && this.yd > 0)) {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.yd, this.xd, type, this.team, this.rotation, userData.rank));
          } else {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.xd, this.yd, type, this.team, this.rotation, userData.rank));
          }
          this.data = this.fireCalc(this.rotation + 10);
          this.xd = this.data.xd;
          this.yd = this.data.yd;
          if ((this.xd < 0 && this.yd < 0) || (this.xd > 0 && this.yd > 0)) {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.yd, this.xd, type, this.team, this.rotation, userData.rank));
          } else {
            s.push(new Shot(this.x + 20, this.y + 20, s.length - 1, this.xd, this.yd, type, this.team, this.rotation, userData.rank));
          }
        } else if (this.fireType == 3) {
          this.data = this.fireCalc(this.rotation);
          this.xd = this.data.xd;
          this.yd = this.data.yd;
          var l = 0;
          setTimeout(function(a) {
            if ((a.xd < 0 && a.yd < 0) || (a.xd > 0 && a.yd > 0)) {
              s.push(new Shot(a.x + 20, a.y + 20, s.length - 1, a.yd, a.xd, type, a.team, a.rotation, userData.rank));
            } else {
              s.push(new Shot(a.x + 20, a.y + 20, s.length - 1, a.xd, a.yd, type, a.team, a.rotation, userData.rank));
            }
          }, 20, this);
          setTimeout(function(a) {
            if ((a.xd < 0 && a.yd < 0) || (a.xd > 0 && a.yd > 0)) {
              s.push(new Shot(a.x + 20, a.y + 20, s.length - 1, a.yd, a.xd, type, a.team, a.rotation, userData.rank));
            } else {
              s.push(new Shot(a.x + 20, a.y + 20, s.length - 1, a.xd, a.yd, type, a.team, a.rotation, userData.rank));
            }
          }, 40, this);
          setTimeout(function(a) {
            if ((a.xd < 0 && a.yd < 0) || (a.xd > 0 && a.yd > 0)) {
              s.push(new Shot(a.x + 20, a.y + 20, s.length - 1, a.yd, a.xd, type, a.team, a.rotation, userData.rank));
            } else {
              s.push(new Shot(a.x + 20, a.y + 20, s.length - 1, a.xd, a.yd, type, a.team, a.rotation, userData.rank));
            }
          }, 60, this);
          setTimeout(function(a) {
            if ((a.xd < 0 && a.yd < 0) || (a.xd > 0 && a.yd > 0)) {
              s.push(new Shot(a.x + 20, a.y + 20, s.length - 1, a.yd, a.xd, type, a.team, a.rotation, userData.rank));
            } else {
              s.push(new Shot(a.x + 20, a.y + 20, s.length - 1, a.xd, a.yd, type, a.team, a.rotation, userData.rank));
            }
          }, 80, this);
          setTimeout(function(a) {
            if ((a.xd < 0 && a.yd < 0) || (a.xd > 0 && a.yd > 0)) {
              s.push(new Shot(a.x + 20, a.y + 20, s.length - 1, a.yd, a.xd, type, a.team, a.rotation, userData.rank));
            } else {
              s.push(new Shot(a.x + 20, a.y + 20, s.length - 1, a.xd, a.yd, type, a.team, a.rotation, userData.rank));
            }
          }, 100, this);
        }
      }
    }
  }

  function tank_listener1(event) {
    event.preventDefault();
    if (event.keyCode == 87 || event.keyCode == 83 || event.keyCode == 68 || event.keyCode == 65) {}
    if (user.tank.helper[event.keyCode] != true) {
      user.tank.intervals[event.keyCode] = window.setInterval(function() {
        user.tank.move(event, this.speed);
      }, 15);
    }
    user.tank.helper[event.keyCode] = true;
  }

  function tank_listener2(event) {
    event.preventDefault();
    if (event.keyCode == 87 || event.keyCode == 83 || event.keyCode == 68 || event.keyCode == 65) {
      if ((user.tank.helper[87] != true && user.tank.helper[83] != true) && (user.tank.helper[68] != true && user.tank.helper[65] != true)) {}
    }
    window.clearInterval(user.tank.intervals[event.keyCode]);
    user.tank.helper[event.keyCode] = false;
    user.tank.intervals[event.keyCode] = false;
  }

  function tank_listener3(e) {
    if (e.pageX || e.pageY) {
      x = e.pageX;
      y = e.pageY;
    } else {
      x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    x /= window.resizer;
    y /= window.resizer;
    var targetX = x - window.resizer * 250,
      targetY = y - window.resizer * 250;
    var rotation = 360 - Math.atan2(targetX, targetY) * (180 / Math.PI);
    rotation -= 360;
    if (rotation < 0) {
      rotation = 360 + rotation;
    }
    user.tank.rotation = Math.round(rotation);
  }
  var tankSupport;

  function tank_listener4(e) {
    var mouseValue = e.button;
    clearInterval(tankSupport);
    var cooldown = 0;
    if (user.tank.fireType == 1) {
      cooldown = 200;
    } else if (user.tank.fireType == 2) {
      cooldown = 600;
    } else if (user.tank.fireType == 3) {
      cooldown = 400;
    }
    if (user.tank.canFire) {
      tank_support(mouseValue);
      user.tank.canFire = false;
      setTimeout(function() {
        user.tank.canFire = true;
      }, cooldown);
    }
    tankSupport = window.setInterval(tank_support, cooldown, mouseValue);
  }

  function tank_support(button) {
    user.tank.fire(button);
  }

  function tank_listener5() {
    clearInterval(tankSupport);
  }

  function tank_listener6(e) {
    e.preventDefault();
    if (e.touches[0].pageX - canvas.offsetLeft <= 250) {
      if (e.touches[0].x) {
        if (this.touches.length == 0) {
          this.leftTouches[0] = {
            x: e.touches[0].pageX - canvas.offsetLeft,
            y: e.touches[0].pageY - canvas.offsetTop,
          };
        } else {
          this.leftTouches[1] = {
            x: e.touches[0].pageX - canvas.offsetLeft,
            y: e.touches[0].pageY - canvas.offsetTop,
          }
        }
      } else {
        if (this.touches.length == 0) {
          this.rightTouches[0] = {
            x: e.touches[0].pageX - canvas.offsetLeft,
            y: e.touches[0].pageY - canvas.offsetTop,
          };
        } else {
          this.rightTouches[1] = {
            x: e.touches[0].pageX - canvas.offsetLeft,
            y: e.touches[0].pageY - canvas.offsetTop,
          }
        }
      }
      if (this.touches.length == 2) {
        var targetX = this.rightTouches[1].x - this.rightTouches[0].x,
          targetY = this.rightTouches[1].y - this.rightTouches[0].y;
        var rotation = 360 - Math.atan2(targetX, targetY) * (180 / Math.PI);
        rotation -= 360;
        if (rotation < 0) {
          rotation = 360 + rotation;
        }
        this.rotation = Math.round(rotation);
      }
    }
  }

  function tank_listener7(e) {
    if (e.changedTouches[0].pageX - canvas.offsetLeft >= 250) {
      this.rightTouches = [];
    } else {
      this.leftTouches = [];
    }
  }

  function thermal_check(x, y) {
    if ((x + 45 >= user.tank.x - 5 && x + 40 <= user.tank.x + 5 + 40) || (x >= user.tank.x - 5 && x <= user.tank.x + 5 + 40)) {
      if ((y + 40 >= user.tank.y - 5 && y + 40 <= user.tank.y + 5 + 40) || (y >= user.tank.y - 5 && y <= user.tank.y + 5 + 40)) {
        return true;
      }
    }
    return false;
  }

  function checkerM(x, y, bloks) {
    if (x < 0 || y < 0 || x + 40 > 1500 || y + 40 > 1500) {
      return false;
    }
    var l = 0;
    var len = bloks.length
    while (l < len) {
      if ((x + 40 > bloks[l].x * 50 && x + 40 < bloks[l].x * 50 + 50) || (x > bloks[l].x * 50 && x < bloks[l].x * 50 + 50)) {
        if ((y > bloks[l].y * 50 && y < bloks[l].y * 50 + 50) || (y + 40 > bloks[l].y * 50 && y + 40 < bloks[l].y * 50 + 50)) {
          return false;
        }
      }
      l++;
    }
    return true;
  }

  function checker(x, y, exclude) {
    if (Game.borders == undefined) Game.borders = [0, 1500, 0, 1500];
    if (x < Game.borders[0] || y < Game.borders[2] || x + 40 > Game.borders[1] || y + 40 > Game.borders[3]) {
      return false;
    }
    // if touching user tank then return 'i cant move'
    if (Game.level != 'multiplayer') {
      if ((x + 40 >= user.tank.x && x + 40 <= user.tank.x + 40) || (x >= user.tank.x && x <= user.tank.x + 40)) {
        if ((y + 40 >= user.tank.y && y + 40 <= user.tank.y + 40) || (y >= user.tank.y && y <= user.tank.y + 40)) {
          if ((user.tank.x - user.tank.speed == x || user.tank.x + user.tank.speed == x || user.tank.x == x) && (user.tank.y + user.tank.speed == y || user.tank.y - user.tank.speed == y || user.tank.y == y)) {} else {
            return false;
          }
        }
      }
    }
    // if touching Eval Tank then return ' i cant move'
    var l = 0,
      checkAi = [];
    while (l < ai.length) {
      if (exclude != undefined) {
        if (ai[l].x != exclude.x || ai[l].y != exclude.y) {
          checkAi.push(ai[l]);
        }
      } else {
        checkAi.push(ai[l]);
      }
      l++;
    }
    var l = 0;
    while (l < checkAi.length) {
      if ((x + 40 >= checkAi[l].x && x + 40 <= checkAi[l].x + 40) || (x >= checkAi[l].x && x <= checkAi[l].x + 40) && x != checkAi[l].x) {
        if ((y + 40 >= checkAi[l].y && y + 40 <= checkAi[l].y + 40) || (y >= checkAi[l].y && y <= checkAi[l].y + 40) && y != checkAi[l].y) {
          if ((x == checkAi[l].x - 5 || x == checkAi[l].x + 5 || checkAi[l].x == x) && (y == checkAi[l].y - 5 || y == checkAi[l].y + 5 || checkAi[l].y == y)) {} else {
            return false;
          }
        }
      }
      l++;
    }
    // if touching walls then return 'i cant move'
    var l = 0;
    while (l < b.length) {
      if ((x + 40 > b[l].x * 50 && x + 40 < b[l].x * 50 + 50) || (x > b[l].x * 50 && x < b[l].x * 50 + 50)) {
        if ((y > b[l].y * 50 && y < b[l].y * 50 + 50) || (y + 40 > b[l].y * 50 && y + 40 < b[l].y * 50 + 50)) {
          return false;
        }
      }
      l++;
    }
    return true;
  }

  function checker2(x, y) {
    var l = 0,
      targets = [];
    while (l < b.length) {
      if ((x + 40 > b[l].x * 50 && x + 40 < b[l].x * 50 + 50) || (x > b[l].x * 50 && x < b[l].x * 50 + 50)) {
        if ((y > b[l].y * 50 && y < b[l].y * 50 + 50) || (y + 40 > b[l].y * 50 && y + 40 < b[l].y * 50 + 50)) {
          targets.push(l);
        }
      }
      l++;
    }
    return targets;
  }

  function gameHelper() {
    var l = 0;
    while (l < i.length) {
      window.clearInterval(i[l]);
      l++;
    }
    b = [];
    ai = [];
    s = [];
  }

  function pauseOnClick(e) {
    if (e.pageX || e.pageY) {
      x = e.pageX;
      y = e.pageY;
    } else {
      x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    x /= window.resizer;
    y /= window.resizer;
    if (x > 180) {
      if (x < 321) {
        if (y > 191) {
          if (y < 239) {
            document.removeEventListener('click', pauseOnClick);
            Game.resumeGame();
          }
        }
      }
    }
    if (x > 180) {
      if (x < 321) {
        if (y > 255) {
          if (y < 303) {
            document.removeEventListener('click', pauseOnClick);
            Game.endGame();
            mainMenu();
          }
        }
      }
    }
  }

  function pause() {
    draw.setTransform(window.resizer, 0, 0, window.resizer, 0, 0);
    draw.drawImage(pause_menu, 0, 0);
    document.addEventListener('click', pauseOnClick);
  }

  function Game() {
    this.canChangePaused = true;
    this.flashbanged = false;
    this.coins = 0;
    this.foes = 0;
    this.paused = false;
    this.aiId = 0;
    this.blockId = 0;
    this.tabHandler = function(e) {
      if (document.visibilityState == 'visible') {
        status_message = 'Playing Pixel Tanks';
      } else {
        status_message = 'On Another Tab';
      }
    }
    this.resumeGame = function() {
      document.removeEventListener('click', pauseOnClick);
      canvas.addEventListener('keydown', tank_listener1, false);
      canvas.addEventListener('keyup', tank_listener2, false);
      document.addEventListener('mousemove', tank_listener3, false);
      document.addEventListener('mousedown', tank_listener4, false);
      document.addEventListener('mouseup', tank_listener5, false);
      if (Game.level >= 10000) {
        endlessRunner = window.setInterval(function() {
          if (Math.round(Math.random() * 3) >= 1) {
            var x = Math.round(Math.random() * 30);
            var y = Math.round(Math.random() * 30);
            weak(x, y, true);
            var map = Game.map[y].split('');
            map[x] = '1';
            Game.map[y] = map.join('');
            user.tank.scaffolding.push({
              x: x,
              y: y,
              type: 'weak'
            });
          } else {
            createAi((Math.round(Math.random() * 29)) * 50, (Math.round(Math.random() * 29)) * 50, false, Math.floor(Math.random() * 2) + 1, false, null, 'Eval');
          }
        }, 400);
      }
      this.i();
    }
    this.pauseGame = function() {
      canvas.removeEventListener('keydown', tank_listener1);
      canvas.removeEventListener('keyup', tank_listener2);
      document.removeEventListener('mousemove', tank_listener3);
      document.removeEventListener('mousedown', tank_listener4);
      document.removeEventListener('mouseup', tank_listener5);
      if (Game.level >= 10000) {
        clearInterval(endlessRunner);
      }
      var l = 0;
      while (l < user.tank.intervals.length) {
        if (user.tank.intervals[l]) {
          window.clearInterval(user.tank.intervals[l]);
        }
        l++;
      }
      var l = 0;
      while (l < i.length) {
        window.clearInterval(i[l]);
        l++;
      }
      setTimeout(function() {
        pause();
      }, 20);
    }
    this.leaveWindow = function() {
      if (!document.fullscreenElement) {
        Game.pauseGame();
      }
    }
    this.endGame = function() {
      window.removeEventListener('blur', Game.leaveWindow);
      document.removeEventListener('visibilitychange', Game.tabHandler);
      document.removeEventListener('click', pauseOnClick);
      document.removeEventListener('keydown', Game.pauseHandler);
      draw.setTransform(window.resizer, 0, 0, window.resizer, 0, 0);
      document.removeEventListener('click', levelSelectSupport);
      canvas.removeEventListener('keydown', tank_listener1);
      canvas.removeEventListener('keyup', tank_listener2);
      document.removeEventListener('movemouse', tank_listener3);
      document.removeEventListener('mousedown', tank_listener4);
      document.removeEventListener('mouseup', tank_listener5);
      var l = 0;
      while (l < user.tank.intervals.length) {
        if (user.tank.intervals[l]) {
          window.clearInterval(user.tank.intervals[l]);
        }
        l++;
      }
      gameHelper();
    }
    this.redraw = function() {
      for (var r = 1; r == s.length - 1; r++) {
        s[r].redraw();
      }
    }
    this.pauseHandler = function(e) {
      e = e || event;
      if (e.keyCode == 27 && Game.canChangePaused) {
        Game.canChangePaused = false;
        setTimeout(function() {
          Game.canChangePaused = true;
        }, 1000);
        if (!Game.paused) {
          Game.paused = true;
          Game.pauseGame();
        } else {
          Game.paused = false;
          Game.resumeGame();
        }
      }
    }
    this.control = function() {
      document.addEventListener('keydown', this.pauseHandler);
    }
    this.i = function() {
      i.push(window.setInterval(function() {
        clearShot();
      }, 30));
      i.push(window.setInterval(function() {
        var l = 0;
        while (l < s.length) {
          s[l].update();
          l++;
        }
      }, 14));
      i.push(window.setInterval(function() {
        var l = 0;
        while (l < ai.length) {
          ai[l].move();
          l++;
        }
      }, 60));
      i.push(window.setInterval(function() {
        ai.sort(function(a, b) {
          var distance = Math.abs(a.x - user.tank.x) * Math.abs(a.x - user.tank.x) + Math.abs(a.y - user.tank.y) * Math.abs(a.y - user.tank.y);
          var distance2 = Math.abs(b.x - user.tank.x) * Math.abs(b.x - user.tank.x) + Math.abs(b.y - user.tank.y) * Math.abs(b.y - user.tank.y);
          return distance - distance2;
        })
        var l = 0;
        var len = ai.length;
        while (l < len) {
          ai[l].pathfind();
          l++;
        }
      }, 500));
      i.push(window.setInterval(function() {
        var l = 0;
        while (l < ai.length) {
          if (ai[l].fireType == 1) {
            setTimeout(function(l) {
              ai[l].shoot(ai[l]);
            }, Math.floor(Math.random() * 200) + 200, l);
          }
          l++;
        }
      }, 500))
      i.push(window.setInterval(function() {
        var l = 0;
        while (l < ai.length) {
          if (ai[l].fireType == 2) {
            setTimeout(function(l) {
              ai[l].shoot(ai[l]);
            }, Math.floor(Math.random() * 200) + 200, l);
          }
          l++;
        }
      }, 800))
      i.push(window.setInterval(function() {
        var l = 0;
        while (l < ai.length) {
          if (ai[l].fireType == 3) {
            setTimeout(function(l) {
              ai[l].shoot(ai[l]);
            }, Math.floor(Math.random() * 200) + 200, l);
          }
          l++;
        }
      }, 800))
    }
  }

  function jquery_tank_support(e) {
    user.tank.move(e);
  }
  var toolkitAnimation;
  class Block {
    constructor(health, x, y, isInvincible, isLoot, isScaffolding, isSolid, type) {
      this.blockId = b.length - 1;
      this.x = x;
      this.y = y;
      this.health = health;
      this.type = type;
      this.isInvincible = isInvincible;
      this.isLoot = isLoot;
      this.isScaffolding = isScaffolding;
      this.isSolid = isSolid;
      if (this.type == 'void') {
        this.barrier();
      } else if (this.type == 'wall') {
        this.wall();
      } else if (this.type == 'gold') {
        this.gold();
      } else if (this.type == 'strong') {
        this.strong();
      } else if (this.type == 'weak') {
        this.weak();
      }
    }
    barrier() {
      this.draw = function() {
        draw.fillStyle = '#000000';
        draw.fillRect(this.x * 50, this.y * 50, 50, 50);
      }
    }
    wall() {
      this.draw = function() {
        draw.drawImage(wall_image, this.x * 50, this.y * 50);
      }
    }
    gold() {
      this.draw = function() {
        draw.drawImage(goldI[30 - Math.ceil(this.health / 20)], this.x * 50, this.y * 50);
      }
    }
    strong() {
      this.draw = function() {
        draw.drawImage(strongI[6 - Math.ceil(this.health / 20)], this.x * 50, this.y * 50);
      }
    }
    weak() {
      this.draw = function() {
        draw.drawImage(weakI[4 - Math.ceil(this.health / 20)], this.x * 50, this.y * 50);
      }
    }
    destroy(id) {
      if (!this.isScaffolding) {
        var array = Game.map[this.y].split('');
        array[this.x] = '0';
        Game.map[this.y] = array.join('');
      }
      b.splice(id, 1);
      delete this;
    }
  }
  var messages = document.getElementById('messages');
  var message = document.getElementById('message');
  var canvas = document.getElementById('canvas'); // getting the canvas element
  var draw = canvas.getContext('2d'); // getting the canvas to be able to draw
  // OPTIMIZATION FOR IMAGE RESIZING
  draw.webkitImageSmoothingEnabled = false;
  draw.mozImageSmoothingEnabled = false;
  draw.imageSmoothingEnabled = false;
  // END
  var endlessRunner;
  draw.font = '15px starfont';
  draw.fillText('abcdefghijklmnopqrstuvwxyz', 0, 0); // init font
  draw.clearRect(0, 0, 500, 500);
  window.resizer = 1;
  document.addEventListener('fullscreenchange', function() {
    if (window.resizer == 1) {
      //canvas.height = window.innerHeight;
      //canvas.width = window.innerHeight;
      window.resizer = window.innerHeight / 500;
    } else {
      window.resizer = 1;
      canvas.width = 500;
      canvas.height = 500;
    }
  });
  var totalImages = 0;
  class ImageLoader extends Image { // loads all images
    constructor() {
      super();
      super.onload = this.onload;
      super.onerror = this.onerror;
      totalImages++;
    }
    onload() {
      bootPercent += 100 * (1 / totalImages);
    }
    onerror() {
      alert(this.src);
    }
  }
  window.bootPercent = 0;
  window.onload = function() {
    canvas.width = window.innerHeight;
    canvas.height = window.innerHeight;
    window.resizer = window.innerHeight / 500;
    draw.setTransform(window.resizer, 0, 0, window.resizer, 0, 0);
    boot();
  }
  draw.fillStyle = '#000000';
  draw.fillRect(0, 0, 500, 500);
  draw.textAlign = 'center';
  draw.fillStyle = '#ffffff';
  draw.fillText('Loading 0%', 250, 250);

  function boot() {
    bootLoader = window.setInterval(function() {
      if (Math.round(new Number(bootPercent.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0])) >= 100) {
        clearInterval(bootLoader);
        setTimeout(startScreen, 500);
      }
      updateBootProgress();
    }, 1000 / 60); // 60 fps boot progress speed
    b = [], s = [], ai = [], l1 = 0, l2 = 0, i = [], Game = new Game(), button = new ImageLoader(), gameplay1 = new ImageLoader(), red_bullet = new ImageLoader(), ai_top = new ImageLoader(), ai_base = new ImageLoader(), tank_base_png = new ImageLoader(), tank_top_png = new ImageLoader(), start_screen = new ImageLoader(), tank_base2 = new ImageLoader(), coins = new ImageLoader(), toolkit = new ImageLoader(), scaffolding = new ImageLoader(), boost = new ImageLoader(), flashbang = new ImageLoader(), iron_tank_top = new ImageLoader(), iron_tank_base = new ImageLoader(), iron_tank_base2 = new ImageLoader(), diamond_tank_top = new ImageLoader(), diamond_tank_base = new ImageLoader(), diamond_tank_base2 = new ImageLoader(), dark_tank_base = new ImageLoader(), dark_tank_base2 = new ImageLoader(), dark_tank_top = new ImageLoader(), light_tank_base = new ImageLoader(), light_tank_base2 = new ImageLoader(), light_tank_top = new ImageLoader(), powermissle = new ImageLoader(), megamissle = new ImageLoader(), stupid_tank_base = new ImageLoader(), stupid_tank_base2 = new ImageLoader(), stupid_tank_top = new ImageLoader(), area_tank_base = new ImageLoader(), area_tank_base2 = new ImageLoader(), area_tank_top = new ImageLoader(), condensed_tank_base = new ImageLoader(), condensed_tank_base2 = new ImageLoader(), condensed_tank_top = new ImageLoader(), ai_turret_base = new ImageLoader(), pause_menu = new ImageLoader(), shotgun_bullet = new ImageLoader(), condensed_bullet = new ImageLoader(), wall_image = new ImageLoader(), shopTanksMenu = new ImageLoader(), shopKitsMenu = new ImageLoader(), shopItemsMenu = new ImageLoader(), shopClassesMenu = new ImageLoader(), floor = new ImageLoader(), grapple = new ImageLoader();
    grapple.src = '/assets/images/tanks/grapple.png';
    bullet_particle = [new ImageLoader(), new ImageLoader(), new ImageLoader(), new ImageLoader(), new ImageLoader()];
    var l = 0;
    while (l < bullet_particle.length) {
      bullet_particle[l].src = '/assets/images/tanks/animations/bullet-particle-' + l + '.png';
      l++;
    }
    goldI = [];
    var l = 0;
    while (l < 30) {
      goldI.push(new ImageLoader());
      l++;
    }
    var l = 0;
    while (l < goldI.length) {
      goldI[l].src = '/assets/images/tanks/blocks/gold_block-d' + l + '.png';
      l++;
    }
    strongI = [
      new ImageLoader(),
      new ImageLoader(),
      new ImageLoader(),
      new ImageLoader(),
      new ImageLoader(),
      new ImageLoader(),
    ];
    var l = 0;
    while (l < strongI.length) {
      strongI[l].src = '/assets/images/tanks/blocks/strong_block-d' + l + '.png';
      l++;
    }
    weakI = [
      new ImageLoader(), // d0
      new ImageLoader(), // d1
      new ImageLoader(), // d2
      new ImageLoader(), // d3
    ];
    var l = 0;
    while (l < weakI.length) {
      weakI[l].src = '/assets/images/tanks/blocks/weak_block-d' + l + '.png';
      l++;
    }
    floor.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABdwAAAXcCAYAAAA4NUxkAAAAAXNSR0IArs4c6QAAIABJREFUeF7snX1PHEfztXtzE26Mkc17CJb4RlGiRNHvj+fbI4HWvITEcAMGO350atTbvb3dM91rMLC+RopkZzw7M9t16pyqnTk1+v3337+sra25+/t7p215edl9+fKl+u+3t7duaWnJra6uNh3nz3Nzc+M+ffrkVlZW5jq/Pufy8tKO5T6cYz26+CWuAp7BR8hn4COPjx9//NFyaAsPEFfEVRov5F3ybk5HknfRJWldMQ9/pDw1b1z5zyFfka/IV+W6/6nw8VA4p88w28+ZJ++mfaF58y7rwXrEOuCl4nyoXgYf6N2c3h39v//3/758/PjRmtXarq6u3P7+vjs5ObG/7+7uuuPj4+L+//znP9bgvLu7azrOf64S8Gg0cp8/f646X+56/vvf/9qx3IdzrEcXr8RVwDP4CPkMfICPlM/AB/hI9Q78AX/k9DD8AX/AH9SDQ/Uy/AF/wB/lfhL4AB/gA3zU9JsXqT4f/d///d8XPV3+v//9z5rWr1+/tqbt33//bY3wt2/fWjO8tN8fq3/Xcpz/3H/++cfOqV/Sa86Xux79mqSN+xhNvgPWg7jyeAYfIZ+Rr7p8Tt4NfPcU+ND3rw0enNUXT7Eeqd4BH0+LD9ajrL+fEz78tQzVCeh26qi+OhIehAfpM5T7LOADfIAP8NHSp31OOpH+7mtzH7GG+/b2tjs6OrIGwLt379zp6ak1v7Xp6fWdnZ3iftnJKAhaj4s/Vw132crUnC93PWdnZ3Ys9+HM3of16OKVuOrwDD5CPgMfIZ+DD/CR8jb4AB+p3oM/4I9cPQB/wB/wR7leBh/gA3yAj6F+GvoKfYW+KvebFwkfZimjBu36+ro12S8uLtzBwYEbj8fWxN7b23OHh4fF/fIyUhP8+vq66Tj/ufJ+13m9F+zQ+XL7JWy0cR/OsR5dvBJXAc/gI+Qz8AE+Uj4DH+Aj1TvwB/yR08PwB/wBf1APDtXL8Af8AX+U+0ngA3y8FHyot6g3LumL1veF6e/O9s3VZ7CGu15X+uuvv6xpvbm5af7tcRNCvuml/XoyXQ13CZCW49Qc1+f6V4f1ZJ1vmvedzx8XX0/8ej73wXoQV9N4BR8hn5GvyLspv4AP8JHqHXRJ0IPgA3yAj/BQD/xBPThUZ8If8Eeun0L9Qf0Bf8Af8Ed9v3mR6g+zlJGYrvVsT70YZWivJrga9TUefakHkQJP59fA03k85HU98WdyH6yHZgkQV8EDFnwE7zfyVTejA3zMh4/Ue91ji7girtIZNuRd8i56tzwbKv5uSnl1aKYUeZe8+zV5Fz7vn9WGTpxPJ5b6EOQr8tXX5CviinxVO+uS+uP51R/WcNcT6rWe7amHugSxFlZTp/u83kte8Hd3d9Zw98Exjxd8fO3cB+shzzTiKnhigY8wg4J81Xkqgg/wkZuZAj7AR+o5Cn/AH+mMJ/gD/oA/yjPOwAf4AB/gY2gGYtp7o39F/4r6fHqG6CLVH2Yps7W1ZXYw2vS6y/HxsVtbW7Mm/OXlpdvf3y/ul7BQ0tDA0pbj/OfKEF/nkTdmzfly13N+fm7Xzn2MrJHGeuzboFDiqsMz+Aj5DHx0+Rx8BL4DH+Aj1TvgA3zk9DD88e34Q9//1dUV9UdP/UUdNVufwufwOXxe7t+AD/DxPeCjRj/QT6S/29fffmh9ZQ132bnUeranHup6PUYNXhUird7ravLryXg1RmVHM4+HvK5Hr2lp4z6c2UWwHsRVjGfwETzjwEc3o4O8Gzx5wQf4SD0VwQf4yM0Ugj/gj3RWFfwBf8AfZU9e8AE+wAf4qOnvoa/QV4usr8xSZmVlZcoHXQNMLy4urBG+sbHh7u/vi/v9sfJSbznOf6489NQcvLm5qTpf7npub2+t4c59jCbfAetBXPmZCOAj5DPyVZfPybvBW7iED711pULhMXhQXoza4MFZfUG+Il+l+pN8NZyvWvT3Q/Ggx+pQnYBup47qqyPhQXiQPkO5zwI+wAf4AB+xL/uQ3qOOen51lDXcZQcTezTqlaN///3XGg36T1Ytpf1adAWBvNdbjos/Vw13JZOa8+WuR69/a+M+RvajB+vRxStx5QyX4CPkM/AR8jn4eDp86C0kbfDgrL4gX5GvcvqTfPV0+aq0Hp8+fZrJY/ofNfUAOAfn4LxcZ4MP8AE+wEdNX4y6lro27dPCH8+PP8xSRg3a9fV1K/71ZNHBwYEbj8cmpPf29tzh4WFxv54CVPPg+vq66Tj/uaurq3ZeNdxrzpe7ntg+g/tgPRSvxFXAM/gI+Yx81eVz8AE+cnwPPsBHqvfgD/gjrQfgD/gD/ijXy+ADfIAP8FHTT0NfPW99pb6k3kimL1rfF6a/O9s3F86t4S77kdRjq9bTXU+4qOGuRnfqvVPj6a5AFjHpF7qcZ+bmZudp1Hc9et1YG/fhHOvxP5slQFwFzzjwEfIH+AAfKS+BD/CR6gv4A/7IeY7CH/AH/BE8qdP6DHyAD/ABPob6Segr9BX6yrlSfxN8LCY+zFJGxaZfYDWtNcA09grSIIPSfg1E0b9V4LQcp3Pqc5WY9WcNPPWe033n88fF1xNfK/fBehBX03gFHyGfka/Iuym/tOAjbs7HfEdcEVdfE1foq+7Bi5L+RCeG76clX72UuCrl1ZzeJ+/O1mfgYz58wOfk3T7eoc+Qjw/0LnoXvUtfdKjfii6Z1iXWcNcT6iIdbfrzzs7OlGf76elpcb8EsQqA5eXlpuPk8ajPvbu7s4a7LyL8/2+5nvjfthwX3yf3wXqkcU5chTwAPsAH+IAHh/gVPg86Cv6AP+LZR+jd6fwJPsAH+DiyujtX94IP8AE+wEcpP9C/mu1TUn9Qf+T62M+lfzX65ZdfvshvLp5oqwuu/bsa9Brs4V/Jrj1uZWVlYiHzww8/TBr6/v+3fI7847VxH90PJqyHsx9wiCvnhCfwEfIZ+CDvpvwGPsBHqjfgj1vTVPBHly99fMAf8Af8Ua4PwQf4AB/gY6h/g75CX3l9ib6azRfgYzHxMfrzzz+/yH9dwx28p5JEU+3f5b2uhoVM8luOkwWN/v379++tUS7vv3nOr8/Rawt6GoD7cOaFz3oQVzF+wUfIZ+Bj8fOueCT2kBzipRI+Wj6HuFr8uBqKo3Q/efdx824LPtGJHT69LiBfka9eSr4C59Tn5KvFz1fgHJyD88XH+XddR2loqn5NUbM6N1XbT5st7f/xxx9NyKvJ2zLFlym2+Sm2Ih3WwzniqosP/RglXN7f39sPUkN4zO1nCvrznoJO3oU/wHnQH+Qr8tV4PJ7iO/ABPnI8iU5EJ0onx/Up/AF/wB/T/Ak+8v0t+AP+GOIPCVF5lbf0N4kr4ioXVyM13DWoamiqdGk/U+mZSs9UeqbSD+UPpm4v5tRtiRH4w9kbWvrhWYXNycnJxGJNP56m+TG3H3yAD/+GXxwfxBX6Cn2FvkJfBX4Y4td4EOrQ90Zd2/+9okvQJegS5/SGHLq9v64h74bvB92Obs/pdhuaqicChqbNlvYzrZpp1UyrZlr1UP5gWvX0tGqJWPKucy+NP2JR+fnz58mw75d2H/B5wGO8jsKk+Ix8Rb7SE9X6MRGchzz3WPgo5VV/PvIV+SqHx1x86EfuWn0FnxNXjxFX5Cviiria1U/weVdf0C/pzw/x99PC588971rDXU/mxRPR4+nHuenpTEdmOnI6PT2On9KfiavZuBnCHVO3mbqt4lE5mrxL3iXvHlkzBV3yzp2eng7qNvgD/oA/ZvnT5w/wAT7AB/hI9XVOX1DXBv2txqkaYcvLy1N1yZAuI+/O6jbiirhK6zp0yWLqErOU2dracqnnWa2nuwJDZLW9vW2e07XHaXiTPJHOzs6saJTn0TxexvKsPj8/tyKc+xg51oO4Sr3GwEfIS+ADfICP4PGaznwAH+ADfICPoXrgW+j2Gu9U8hX5inxFvnoO+aqmf0G+Il+Rr75NvqrRD74PGecP+iX0S9I+9kPpXWu4f/z4sdlz1nvf6fUINdxFJDVetalnnn4hFVHptd2cV1iNB65e59fGfTh7XYX1OLFf3omrbgAv+AgesOCj8yIEH6+NM8DHtMc8+AAfqVcp/AF/oNvLXtfgA3yAD/BR079AX6Gv0FdlL3jwAT4WGR9mKbOysjLxotWrQktLS06//KhhubGx4e7v74v7/bHyumw5zn+uPPR0zpubm6rz5a7n9vbWmifcx2jyHbAexJX3sQQfIZ+Rr7p8Tt4NPq8lfOitKxXSj8GD8prTBg/O6gvyFfkq1Z/kq+F81aK/H4oHPVaH6gR0O3VUXx0JD8KD9BnKfRbwAT7AB/iI/cyH9B511POro6zhLjuY2ENIr1ToKWltajbIqqW0X4uuIJA3V8tx8eeq4a6pvjXny12PHvfXxn04+9GD9ejilbjqvI7BR8hn4CPkc/ABPlLeBh/gI9V78Af8kasH4A/4A/4o173gA3yAD/Ax1BdDX6Gv0FflfvMi4cMsZdSgbfVe955HegpQwXJ9fW2e7ENeaun+1dVVa+rr1zttqadszmMp9YqXsPH2ANwH66H4IK7WJzMRwEfIS+SrPZu1AT7AR85zFHyAD/RVeRYR+AAf4AN8DNWZ6Cv0FfpqZE4Jub4Q+AAfLwUf83jBoxPRiTmdaA132Y8oMfpX3PUafexLuLm5WdyvJ9PVcJcAaTlOfuv6XL1aL+DpyTrvw953Pn9cfL163Vgb9+HsTQHWg7gCH/l8Bj7Iuym/wB+B78EH+AAf6N2hegDdHvQF/AF/pPUy+AAfuX4K+gp9hb5CX6Gv6vvNi6SvzFJGT8B6gaCmtQaYxl5BGmRQ2i+hoX+rJNJynM6pz1WTXn9WI917Tvedzx8XX098rdwH60FcTeMVfIR8Rr4i76b80oKPmPxjviOuiKuviSv0VffgRUl/ohPD99OSr15KXJXyak7vk3dn6zPwMR8+4HPybh/v0GfIxwd6F72L3qUvOtRvRZdM6xJruOuJaJGONv15Z2dnyrP99PS0uF+CWAXA8vJy03HyNtPn3t3dWcPdFxH+/7dcT/xvW46L75P7YD3SOCeuQh4AH+ADfMCDQ/wKnwcdBX/AH/HsI/TudP4EH+ADfBxZ3Z2re8EH+AAf4KOUH+hfzfYpqT+oP3J97OfSvzJLGQ3JavVe9x52CnA16TWwNPWsqfF0lyG+hIU8j3KeTjWe7hpKoY37GNkPGKzHgQ0KJa66mQjgI3iOgo/OUxF8jCczQ8AH+Eg9ecEH+MjNFII/vh1/1Hinsh7fbj1q6jPWg/VI6370FfoKfRU828FHedbjQ/JHjX7IzRggX5GvHitfWcNddi7xYMXd3V2zetEmq5iTk5Pifr1WogavgNJynP9cPRkvIafXRGvOl7ue2D+P+2A9FK/EVRgkDD5CPiNfdfkcfIAPP2g85m3wAT5SvQd/wB9pPQB/wB/wh7M3s3N1L/gAH+ADfJTyQ9ynQl+hr9BX5X7zIuFj9Mcff3zR0+W3t7fW8H716pU11/0T7z///LO7vLws7l9bW7P9b968aTrOf+6HDx/s2KurKzv/0Ply+9Xs18Z9OMd6dPFKXAU8g4+Qz8BHHh96E0I5uIUHnmtc6T5ubm6q+ey53kcf78KD6JKa+FhkHgTn6PZ5+dzz3SLjoyY/xHz/XHkQnIPzeXFOn2FWJ4Fz6sG0vwcPtvVLhupl8hV9uFR/Ke+Ofvvtty+rq6uTJ8z1hJs2/8T50N/V2NCv+fqv5Tj/uboI/adGv7ah8+X2X19f27Hch7NGE+vhLKaIqw5P4CPkM/DR5Xfw0b1RBT6m+R58gI9Ux8Ef8EdaD8Af8EeuXoM/4A/4o9w/AR/gA3yAj6H+KvpqMfWVDU3VE+16ikDWLvqzmt/v37+3hsRPP/1kTdzS/nhadctx/nPTKbZD58vt9wNXuQ/nWI8uXomrgGfwEfIZ+AAfKZ+14CP+tzHfEVfE1dfEFfqqX3/C54vN56W8OlQPkHfJu1+Td+Fz8m5t36NFJ8LnxBVxRT9RFtj0RV872ZbX9JMXvb9rDXd5sMcT0ePpx7np6UxHZjpyOj09jp/Sn4mr2bgZwh1Tt5m6LaJSjibvknfJu0cm3NAl79zp6emgboM/4A/4Y5Y/ff4AH+ADfICPVF/n9AV1bdDferhRP0DobfqWup68O6vbiCviKq3r0CWLqUtsaOrW1tbEs13eRIeHYUptbopvvP8hpwor8Qydj6nCFy6dcs16zMbr2dmZNSPk609cnVuTCpyPzEpF4np7e7spz/m8Q1yNLZbEE0xzZ5r7Y01zJ1+tG2+Rrzq9Q979/vKucsDff/+N3j04GKzPwMf3h4+augb+gD/SfgG6Hd3+Pej2Gv1AP5F+Yl8/9aH7PtZw1+P+8dTk3NT10n554KqBJWJvOU6vWpycnNgvpBIO3tPI//+W61mkKbZKEqxH561MXIEPprm/tgZ3Ou0efMAf4k/wAT5y+QF91eUH8AE+wMesfsjVWdRRu2ZF6b3pqT+oP+AP+AP+gD/S+nuoT0l9Tn2eqz/MUmZlZcUaOtpEMAoWPVmiRvjbt2+tGV7a74/Vv2s5zn/uP//8Y+eUD1/N+XLXc3vbTRjmPkaT74D1IK48nsFHyGfkqy6fk3cD3z0FPvT9a4MHZ/XFU6xHqnfAx9Pig/Uo6+/nhA9/LUN1ArqdOqqvjoQH4UH6DOU+C/gAH+ADfLT0aZ+TTqS/+9ppPazhLnuFPm/YPo+upaUla8YPear27VfD/dOnT9aAqPFSS69Hr1Nq4z6cYz2C1zVx1Xkdg4/gdQw+wAf8UZ4lAT7AB/gAHzX1APoKfZXWdfAH/AF/wB/wR92sIerzaU97+AP+WGT+MEsZNcxTT6fxOHji9Xm6yyNbTfLr6+ter8WSN/vq6qo92X5/fz/xBm7xkNfnxq99cR+sh+KHuOo8gMHHtEcZ+aqb0QE+wEfOAxZ8gI9Uf6Gvgoc2+AAf4KM84wt8gA/wAT6G+jDUH9QfL6X+mMcLHh6EB3M8aA13va6kxpy2jY0N87GL/fzkV1TaryfT1XBXgm05Tj7l+lz/6rB+2fL+5X3n88fF1xO/ns99sB7E1TRewUfIZ+Qr8m7KL+ADfKR6B10S9CD4AB/g47/F+gR8gA/wAT5q+hfUH9Qf1B/0E4f6lNQfi1l/mKWMnmCq9WxPvRglNPSEvJJIjUdf6kGkJr3Or0b6PB7yup74M7kP1kOzBIir4AELPoL3G/mqm9EBPubDR+q97rFFXBFX6Qwb8i55F71bng0VfzelvDo0U4q8S979mrwLn/fPakMnzqcTS30I8hX56mvyFXFFvqqddUn98fzqD2u46wl1LeI8HuoSxFrY5eVl1+f1XvJmv7u7s4a7D455vODja+c+WI/T01NHXAU8g4/gqUi+6jzzwAf4yPE9+AAfyg8lzkBfoa/gD/BRygPwB/wBf0znh7gvAj7AB/gAH0M6mvp8Metzs5TZ2tpyJycn1nDf3d11x8fHbm1tzYquy8tLt7+/X9yvwFAzXQNLW47zn6uBjjqPPI9qzpe7nvPzc7t27mNkjTTWY98GhRJXHZ7BR8hn4KPL5+Aj8B34AB+p3gEf4COnh+GPb8cf+v6vrq6oP3rqL+qo2foUPofP4fNy/wZ8gI/vAR81+oF+Iv3dvv72Q+sra7jLziUejKUmnV4l0yarGDXjS/v1eowavCpEWo7zn6sn49UYlR1Nzfly1xP753EfrIfilbh6bXjSmyPgI+Qz8lWXz8EH+PD5IeZt8AE+Ur0Hf8AfaT0Af8Af8Eenr3N1L/gAH+ADfJTyQ9ynQl+hr9BX5X7zIuHDLGVWVlamfNBTj6k+b3Z/rLzUW47zXvDy0FPyubm5scZ76hE/5OGo/be3t9Zc5D5Gk++A9SCu/EwE8PGfyYwK8tVb+3GTvBu8hZ8CH6l3LOvxtOuRemOyHqxHbqYQ/DHNHz53otupP1S/gQ/0VdoveAp9BZ+Xva5ZD+pB8AE+avqt8PnD8rk13GUHc3R0ZE3rVg/1paUla9a3Hhd7uqtprund83jI67x6/Vsb9+Ec6/FlMkuAuOrwDD6CZxz4AB/prBHwAT5ynorwB/yR6lr4A/6AP8JMHPDReVLDH+UZcOgr9BX4AB81/T30FfpqkfWVWcqoYb6+vm6i4eLiwh0cHLjxeGxN7L29PXd4eFjcL+91Nc+vr6+bjvOfu7q6aue9v7+vOl/ueuLXc7gP1kPxSlwFPIOPkM/IV10+Bx/gI8f34AN8pHoP/oA/0noA/oA/4I9yvQw+wAf4AB81/TT01fPWV2pM6o0A+qL1fWH6u7N9c+F89Ouvv5qljBrm2iQU9CtT/MqJnj4v7VeBrleU3rx503ScXkHV53748MFeQ5QHvH/Foe98/rj4erwdDffRDZ9lPYgr8JHPZ+CDvJvyi/hD26tXr74ZD8aWMvDgtL6Az4P+Il+Rr3L5ytt3PLVu9w/K5HT5UB0BzsF5WmdSDwbdCj7AB/gI1h/PmQfpX61ObKHpw9GHo7/b1S0zfbjff//9iyYWe+GsYS96Yr3272ruSlirUd9ynD+PRIUuTCDV1np+/XtNktXGfThrtrMeXyz5E1cdnsBHyGfgo8vvKT7U2NNWm/eJq2meJK7ycQWfo0uUV8AH+Ejrg3l0ScpT88aV/xx0YvdmMXwOn+fq96fCx0PhnD7DbD9nnryb6rh58y7rwXrEeeal4nyoXgYf6N2c3jVLmY8fP1qzWtvV1ZXb3993Jycn9ndNYD8+Pi7u16BUfbB+4Ws5zn9uOs196Hy5/fEUW+6D9VC8ElcBz+Aj5DPyVZfPwQf4yPE9+AAfqd6DP+CPtB6AP+AP+KNcL4MP8AE+wEdNPw19hb5CX5X7zYuEDxua6ifRiiDkM6OiO51iLJ/33H6m2D7sFFt9x6zHaPId6NWUlnjUv//8+bOTZYNi2b+W6f9/KY5z+5nmzjR3prkzzf0xprnHljLkq882eN3rC/IueZe8+zLyrscq+grd7m2OlMvR7dQf8PlsP4V+CfW59D56l35iX7+V/tVi1oPWcN/e3nZHR0dW8KZT5/X0ejo1Np7KzlRhpgov8lRhYQJ8OLMpkkhozQ9x/tAPILL50TaUV3L7z87O7FjWg/UgrmZ5GXyc2hwYbeQrdAm6ZKeo68EH+AAf4KOm7ke3d30R9BX6yutL6g/qD/qiZf4EH3l8mKWMGmnr62GqONN4mcbrvZz9tOGW+GDq9vOeui3RUDM9Pc4D8ixTEtUQiJb8wLTq/LRqNQXJu86GPBNXhzYDRbgk717Ym0ngY2zfAfjo8if4CPocfKCvxuMuP6Cv0FfKj6X6DP6AP9L4gD/gD/hjmj/j/JniQzyrNy5b+h7kXfJuLu9aw12v//31118m4DY3N82/PQ46+aaX9uuJVTVMFLAtx6nZpM/1rw7riRtfZPedzx8XX0/8ej73wXoQV9N4BR8hn5GvyLspv4AP8JHqHXRJ0IPgA3yAj/AjIPxBPThUZ8If8Eeun0L9Qf0Bf8Af8Ed9v3mR6g+zlJGYrvVsT70aZWivJrga9ak3VY33tgJP59fg1nk85HU9sR8W98F6KO6Iq+ABCz6CZx75qpvRAT7mw0fqve6xRVwRV6neIe+Sd9OZMeTdfN4t5dWhmVLkXfLu1+Rd+Lw8KwKdOD3LDj6Hz+HzvPc6fbhuNgH14HR8oHen9a413PWEes6bqsbTXYJYQaap7H2eRiXP5ru7O2u4+0CdxyM6vnbug/XQjAHiqvMyTrENPsAH+AhelOBj2msOPn/nwAf4KPEk+AAf8Qwr+AP+yNW91B/UH7lZVfAH/AF/TOvLOH+CD/CxyPgwS5mtrS2zg9Gm112Oj4/d2tqaNeEvLy/d/v5+cb+EhUSnBhm2HOc/V4NIdB55HtWcL3c95+fndu3cx8gazazHvg24Ia46PIOPkM/AR5fPwUfgO/ABPlK9Az7AR04Pwx/fjj/0/V9dXVF/9NRf1FGz9Sl8Dp/D5+X+DfgAH98DPmr0A/1E+rt9/e2H1lfWcJedS61ne+qhrtfO1OBVIdLqva4mv56MV2NUr2PM4yGv69Frpdq4D2d2EawHcRXjGXwEzzjw0c3oIO8GT17wAT5ST0XwAT5yM4XgD/gjnVUFf8Af8EfZkxd8gA/wAT5q+nvoK/TVIusrs5RZWVmZ8kHXANOLiwtrhG9sbLj7+/vifn+sPJxajvOfKw89NQdvbm6qzpe7ntvbW2u4cx+jyXfAehBXfiYC+Aj5jHzV5XPybvCaK+FDb12pUHgMHtSsEW3w4Ky+IF+Rr1L9Sb4azlct+vuheNBjdahOQLdTR/XVkfAgPEifodxnAR/gA3yAj3iGwJDeo456fnWUNdxlB3N0dGQNAHmo65UjPSWtTc0GWbWU9mvRFQStx8Wfq4a7pnfXnC93PXr9Wxv34exHD9aji1fiqsMz+Aj5DHyEfA4+wEfK2+ADfKR6D/6AP3L1APwBf8Af5XoZfIAP8AE+hvpp6Cv0Ffqq3G9eJHyYpYwatOvr69a01pN3BwcHbjwe29/39vbc4eFhcb+eAlSwXF9fNx3nP3d1ddWa+vr1ruZ8ueuJ7TO4D9ZD8UpcBTyDj5DPyFddPgcf4CPH9+ADfKR6D/6AP9J6AP6AP+CPcr0MPsAH+AAfNf009BX6Cn1V7jcvEj6s4S77kdRjq9bTXU+mq+GuRnfqvVPj6a6EpIa7nqzLeWZubnaeRn3Xo9eNtXEfzt4UYD127Ycj4so54Qd8hPwBPv5nszbAR/BUBB/gI9UX4AN85DxH4Q/4I61r4A/4A/4IMz/AR/BsT/sX8Af8AT7Ax1C/lfpjMesPs5SRWPALrKa1BpjGXkEaZFDar4Eo+rcilpbjdE59rgJPf9bAU+853Xc+f1x8PfG1ch+sB3E1jVfwEfIZ+Yq8m/JLCz7i5krMd8QVcfU1cYW+6n4gL+lbu/UGAAAgAElEQVRPdGL4flry1UuJq1Jezel98u5sfQY+5sMHfE7e7eMd+gz5+EDvonfRu/RFh/qt6JJpXWINdz0RLdLRpj/v7OxMebafnp4W90sQqwBYXl5uOk7eZvrcu7s7a7j7IsL//5brif9ty3HxfXIfrEca58RVyAPgA3yAD3hwiF/h86Cj4A/4I559hN6dzp/gA3yAjzA7DX2FvkJf1feh4A/4A/6AP/zs0ZfQpx798ssvX+Q3F0+01Q3U/l0N+n///Xdi+VJ73MrKysRC5ocffpg09P3/b/kc+cdr4z66H0xYD2c/4BBXzglP4CPkM/BB3k35DXyAj1RvwB+3pqngjy5f+viAP+AP+KNcH4IP8AE+wMdQ/wZ9hb7y+hJ9NZsvwMdi4mP0559/fpH/+sXFhRUWsoaRaKr9u7zX1bDQMNOW4/x53r9/b41yeZvNc359jl5b0K+d3IczL3zW44sjrgKewUfIZ+Cjy++LjA/xSOyRN8RLJXy0fA5xtfhxNRRH6X7y7uPm3RZ8ohOndT35inz1UvIVOKc+J18tfr4C5+AcnC8+zr/rOkpDU/VriprValqr0X5wEKYGq5F+eHhY3P/jjz9aA0dN3pbj/Of2TXOvvZ50im3tcfH1ch/dOrMeAQfEVdv08D7cEVfEVS4+yLvk3VRfkHfJu+Px2B7AQCfO6m/wAT7Ax3R+iOtX8AE+wAf4iPVDqb9F/UH9MVR/KI7kVd7S3ySuiKtcXI3UcNegqqGpuaX9TN1m6jZTt5m6PZQ/mLq9mFO3JUbgD2dvaOmHZwn7k5OTicWafsxO82NuP/gAH/4Nvzg+iCv0FfoKfYW+CvwwxK/xINSh7426tv97RZegS9AlnfMDur2/riHvhu8H3Y5uz+l2G5qqJwKGps2W9jOtmmnVTKtmWvVQ/mBa9fS0aolY8q5zL40/YlH5+fPnybDvl3Yf8HnAY7yOwqT4jHxFvtIbOfoxEZyHPPdY+CjlVX8+8hX5KofHXHzoR+5afQWfE1ePEVfkK+KKuJrVT/B5V1/QL+nPD/H308Lnzz3vWsNdT+YNTccu7ReA9IUsLy+7nZ0wNfndu3eub2qs3393d2eNJ/+l1h5XmlDNfbAeijviKkx7Z5o709yZ5s40dzUhhvgVPu90C/wBfwgv0saxrgUf4COta9BX6Cv0FfoKfTWsL+n7zPbF4A/4A/74PvjDLGW2trZc6nlW6+muwlRFyfb2dq/Xe8kL/uzszJr98jyax3tdn3t+fm5PNHAfI2sUsB4HjrgKHn7gI8ygAB/djA7wAT5yHpfgA3ykXpXwB/yR1gPfgj9qvFPJV+Qr8lV55hr4AB/gA3wM9fe+BZ/X9PceMl/V6IfczEr0Lnr3sfSuNdw/fvzY7Dnrve/0eoQavAJKjVdt6pmnJ+MFRL22m/MKq/HA1ev82rgPZ6+rsB4n9sYFceXszRHwETxgwUfnRQg+XhtngI9pj3nwAT5Sr1L4A/5At5e9rsEH+AAf4KOmf4G+Ql+hr8pe8OADfCwyPsxSZmVlZWLpoldml5aWnH75UcNyY2PD3d/fF/f7Y+V12XKc/1x56OmcNzc3VefLXc/t7a01T7iP0eQ7YD2IK+9jCT5CPiNfdfmcvBt8Xkv40FtXKqQfgwflNacNHpzVF+Qr8lWqP8lXw/mqRX8/FA96rA7VCeh26qi+OhIehAfpM5T7LOADfIAP8BH7mQ/pPeqo51dHWcNddjCxh5BeqdBT0trUbJBVS2m/Fl1BIG+uluPiz1XDXVN9a86Xux69DqON+3D2owfr0cUrcdV56oGPkM/AR8jn4AN8pLwNPsBHqvfgD/gjVw/AH/AH/FGue8EH+AAf4GOoL4a+Ql+hr8r95kXCh1nKqEFb69meeh7pKUAFy/X1tXkDD3lFpftXV1etqa9f73KesjmPpcPD4DGk/RI23h6A+2A9FB/E1fpkJgL4CHmJfLVnszbAB/jIeSqCD/CBvprWl7GuBR/gA3yAj6E6E32FvkJfjcwpIdcXAh/g46XgYx4veHQiOjGnE63hLvsRJUb/irteo499CTc3N4v79WS6Gu4SIC3HyW9dn6tX6wU8PVnnfdj7zuePi69Xrxtr4z6cvSnAehBX4COfz8AHeTflF/gj8D34AB/gA707VA+g24O+gD/gj7ReBh/gI9dPQV+hr9BX6Cv0VX2/eZH0lVnK6AlYLxDUtNYA09grSIMMSvslNPRvlURajtM59blq0uvPaqR7z+m+8/nj4uuJr5X7YD2Iq2m8go+Qz8hX5N2UX1rwEZN/zHfEFXH1NXGFvuoevCjpT3Ri+H5a8tVLiatSXs3pffLubH0GPubDB3xO3u3jHfoM+fhA76J30bv0RYf6reiSaV1iDXc9ES3S0aY/7+zsTHm2n56eFvdLEKsAWF5ebjpO3mb63Lu7O2u4+yLC//+W64n/bctx8X1yH6xHGufEVcgD4AN8gA94cIhf4fOgo+AP+COefYTenc6f4AN8gI8jq7tzdS/4AB/gA3yU8gP9q9k+JfUH9Ueuj/1c+ldmKaMhWa3e697DTgGuJr0GlqaeNTWe7jLEl7CQ51HO02lvr/MC6vPM01AKbdzHyH7AYD0ObFAoceWc8AM+Qv4AH52nIvgYT2aGgA/wkeoL8AE+cjOF4I9vxx813qmsx7dbj5r6jPVgPdK6H32FvkJfBc928FGe9fiQ/FGjH3IzBshX5KvHylfWcJedSzxYcXd316xetMkq5uTkpLhfr5WowSugtBznP1dPxkvI6TXRmvPlrif2z+M+WA/FK3EVBgmDj5DPyFddPgcf4MMPGo95G3yAj1TvwR/wR1oPwB/wB/zh7M3sXN0LPsAH+AAfpfwQ96nQV+gr9FW537xI+Bj98ccfX/R0+e3trTW8X716Zc11/8T7zz//7C4vL4v719bWbP+bN2+ajvOf++HDBzv26urKzj90vtx+Nfu1cR/OsR5dvBJXAc/gI+Qz8JHHh96EUA5u4YHnGle6j5ubm2o+e6730ce78CC6pCY+FpkHwTm6fV4+93y3yPioyQ8x3z9XHgTn4HxenNNnmNVJ4Jx6MO3vwYNt/ZKhepl8RR8u1V/Ku6Pffvvty+rq6uQJcz3hps0/cT70dzU29Gu+/ms5zn+uLkL/qdGvbeh8uf3X19d2LPfhrNHEejiLKeKqwxP4CPkMfHT5HXx0b1SBj2m+Bx/gI9Vx8Af8kdYD8Af8kavX4A/4A/4o90/AB/gAH+BjqL+KvlpMfWVDU/VEu54ikLWL/qzm9/v3760h8dNPP1kTt7Q/nlbdcpz/3HSK7dD5cvv9wFXuwznWo4tX4irgGXyEfAY+wEfKZy34iP9tzHfEFXH1NXGFvurXn/D5YvN5Ka8O1QPkXfLu1+Rd+Jy8W9v3aNGJ8DlxRVzRT5QFNn3R10625TX95EXv71rDXR7s8UT0ePpxbno605GZjpxOT4/jp/Rn4mo2boZwx9Rtpm6LqJSjybvkXfLukQk3dMk7d3p6Oqjb4A/4A/6Y5U+fP8AH+AAf4CPV1zl9QV0b9LcebtQPEHqbvqWuJ+/O6jbiirhK6zp0yWLqEhuaurW1NfFslzfR4WGYUpub4hvvf8ipwko8Q+djqvCFS6dcsx6z8Xp2dmbNCPn6E1fn1qQC5yOzUpG43t7ebspzPu8QV2OLJfEE09yZ5v5Y09zJV+vGW+SrTu+Qd7+/vKsc8Pfff6N3Dw4G6zPw8f3ho6augT/gj7RfgG5Ht38Pur1GP9BPpJ/Y10996L6PNdz1uH88NTk3db20Xx64amCJ2FuO06sWJycn9guphIP3NPL/v+V6FmmKrZIE69F5KxNX4INp7q+twZ1Ouwcf8If4E3yAj1x+QF91+QF8gA/wMasfcnUWddSuWVF6b3rqD+oP+AP+gD/gj7T+HupTUp9Tn+fqD7OUWVlZsYaONhGMgkVPlqgR/vbtW2uGl/b7Y/XvWo7zn/vPP//YOeXDV3O+3PXc3nYThrmP0eQ7YD2IK49n8BHyGfmqy+fk3cB3T4EPff/a4MFZffEU65HqHfDxtPhgPcr6+znhw1/LUJ2AbqeO6qsj4UF4kD5Duc8CPsAH+AAfLX3a56QT6e++dloPa7jLXqHPG7bPo2tpacma8UOeqn371XD/9OmTNSBqvNTS69HrlNq4D+dYj+B1TVx1XsfgI3gdgw/wAX+UZ0mAD/ABPsBHTT2AvkJfpXUd/AF/wB/wB/xRN2uI+nza0x7+gD8WmT/MUkYN89TTaTwOnnh9nu7yyFaT/Pr6utdrseTNvrq6ak+239/fT7yBWzzk9bnxa1/cB+uh+CGuOg9g8DHtUUa+6mZ0gA/wkfOABR/gI9Vf6KvgoQ0+wAf4KM/4Ah/gA3yAj6E+DPUH9cdLqT/m8YKHB+HBHA9aw12vK6kxp21jY8N87GI/P/kVlfbryXQ13JVgW46TT7k+1786rF+2vH953/n8cfH1xK/ncx+sB3E1jVfwEfIZ+Yq8m/IL+AAfqd5BlwQ9CD7AB/j4b7E+AR/gA3yAj5r+BfUH9Qf1B/3EoT4l9cdi1h9mKaMnmGo921MvRgkNPSGvJFLj0Zd6EKlJr/OrkT6Ph7yuJ/5M7oP10CwB4ip4wIKP4P1GvupmdICP+fCReq97bBFXxFU6w4a8S95F75ZnQ8XfTSmvDs2UIu+Sd78m78Ln/bPa0Inz6cRSH4J8Rb76mnxFXJGvamddUn88v/rDGu56Ql2LOI+HugSxFnZ5edn1eb2XvNnv7u6s4e6DYx4v+PjauQ/W4/T01BFXAc/gI3gqkq86zzzwAT5yfA8+wIfyQ4kz0FfoK/gDfJTyAPwBf8Af0/kh7ouAD/ABPsDHkI6mPl/M+twsZba2ttzJyYk13Hd3d93x8bFbW1uzouvy8tLt7+8X9ysw1EzXwNKW4/znaqCjziPPo5rz5a7n/Pzcrp37GFkjjfXYt0GhxFWHZ/AR8hn46PI5+Ah8Bz7AR6p3wAf4yOlh+OPb8Ye+/6urK+qPnvqLOmq2PoXP4XP4vNy/AR/g43vAR41+oJ9If7evv/3Q+soa7rJziQdjqUmnV8m0ySpGzfjSfr0eowavCpGW4/zn6sl4NUZlR1Nzvtz1xP553AfroXglrl4bnvTmCPgI+Yx81eVz8AE+fH6IeRt8gI9U78Ef8EdaD8Af8Af80enrXN0LPsAH+AAfpfwQ96nQV+gr9FW537xI+DBLmZWVlSkf9NRjqs+b3R8rL/WW47wXvDz0lHxubm6s8Z56xA95OGr/7e2tNRe5j9HkO2A9iCs/EwF8/Gcyo4J89dZ+3CTvBm/hp8BH6h3LejzteqTemKwH65GbKQR/TPOHz53oduoP1W/gA32V9gueQl/B52Wva9aDehB8gI+afit8/rB8bg132cEcHR1Z07rVQ31pacma9a3HxZ7uapprevc8HvI6r17/1sZ9OMd6fJnMEiCuOjyDj+AZBz7ARzprBHyAj5ynIvwBf6S6Fv6AP+CPMBMHfHSe1PBHeQYc+gp9BT7AR01/D32FvlpkfWWWMmqYr6+vm2i4uLhwBwcHbjweWxN7b2/PHR4eFvfLe13N8+vr66bj/Oeurq7aee/v76vOl7ue+PUc7oP1ULwSVwHP4CPkM/JVl8/BB/jI8T34AB+p3oM/4I+0HoA/4A/4o1wvgw/wAT7AR00/DX31vPWVGpN6I4C+aH1fmP7ubN9cOB/9+uuvZimjhrk2CQX9yhS/cqKnz0v7VaDrFaU3b940HadXUPW5Hz58sNcQ5QHvX3HoO58/Lr4eb0fDfXTDZ1kP4gp85PMZ+CDvpvwi/tD26tWrb8aDsaUMPDitL+DzoL/IV+SrXL7y9h1Prdv9gzI5XT5UR4BzcJ7WmdSDQbeCD/ABPoL1x3PmQfpXqxNbaPpw9OHo73Z1y0wf7vfff/+iicVeOGvYi55Yr/27mrsS1mrUtxznzyNRoQsTSLW1nl//XpNktXEfzprtrMcXS/7EVYcn8BHyGfjo8nuKDzX2tNXmfeJqmieJq3xcwefoEuUV8AE+0vpgHl2S8tS8ceU/B53YvVkMn8Pnufr9qfDxUDinzzDbz5kn76Y6bt68y3qwHnGeeak4H6qXwQd6N6d3zVLm48eP1qzWdnV15fb3993JyYn9XRPYj4+Pi/s1KFUfrF/4Wo7zn5tOcx86X25/PMWW+2A9FK/EVcAz+Aj5jHzV5XPwAT5yfA8+wEeq9+AP+COtB+AP+AP+KNfL4AN8gA/wUdNPQ1+hr9BX5X7zIuHDhqb6SbQiCPnMqOhOpxjL5z23nym2DzvFVt8x6zGafAd6NaUlHvXvP3/+7GTZoFj2r2X6/1+K49x+prkzzZ1p7kxzf4xp7rGlDPnqsw1e9/qCvEveJe++jLzrsYq+Qrd7myPlcnQ79Qd8PttPoV9CfS69j96ln9jXb6V/tZj1oDXct7e33dHRkRW86dR5Pb2eTo2Np7IzVZipwos8VViYAB/ObIokElrzQ5w/9AOIbH60DeWV3P6zszM7lvVgPYirWV4GH6c2B0Yb+Qpdgi7ZKep68AE+wAf4qKn70e1dXwR9hb7y+pL6g/qDvmiZP8FHHh9mKaNG2vp6mCrONF6m8XovZz9tuCU+mLr9vKduSzTUTE+P84A8y5RENQSiJT8wrTo/rVpNQfKusyHPxNWhzUARLsm7F/ZmEvgY23cAPrr8CT6CPgcf6KvxuMsP6Cv0lfJjqT6DP+CPND7gD/gD/pjmzzh/pvgQz+qNy5a+B3mXvJvLu9Zw1+t/f/31lwm4zc1N82+Pg06+6aX9emJVDRMFbMtxajbpc/2rw3rixhfZfefzx8XXE7+ez32wHsTVNF7BR8hn5Cvybsov4AN8pHoHXRL0IPgAH+Aj/AgIf1APDtWZ8Af8keunUH9Qf8Af8Af8Ud9vXqT6wyxlJKZrPdtTr0YZ2qsJrkZ96k1V472twNP5Nbh1Hg95XU/sh8V9sB6KO+IqeMCCj+CZR77qZnSAj/nwkXqve2wRV8RVqnfIu+TddGYMeTefd0t5dWimFHmXvPs1eRc+L8+KQCdOz7KDz+Fz+DzvvU4frptNQD04HR/o3Wm9aw13PaGe86aq8XSXIFaQaSp7n6dRybP57u7OGu4+UOfxiI6vnftgPTRjgLjqvIxTbIMP8AE+ghcl+Jj2moPP3znwAT5KPAk+wEc8wwr+gD9ydS/1B/VHblYV/AF/wB/T+jLOn+ADfCwyPsxSZmtry+xgtOl1l+PjY7e2tmZN+MvLS7e/v1/cL2Eh0alBhi3H+c/VIBKdR55HNefLXc/5+bldO/cxskYz67FvA26Iqw7P4CPkM/DR5XPwEfgOfICPVO+AD/CR08Pwx7fjD33/V1dX1B899Rd11Gx9Cp/D5/B5uX8DPsDH94CPGv1AP5H+bl9/+6H1lTXcZedS69meeqjrtTM1eFWItHqvq8mvJ+PVGNXrGPN4yOt69FqpNu7DmV0E60FcxXgGH8EzDnx0MzrIu8GTF3yAj9RTEXyAj9xMIfgD/khnVcEf8Af8UfbkBR/gA3yAj5r+HvoKfbXI+sosZVZWVqZ80DXA9OLiwhrhGxsb7v7+vrjfHysPp5bj/OfKQ0/NwZubm6rz5a7n9vbWGu7cx2jyHbAexJWfiQA+Qj4jX3X5nLwbvOZK+NBbVyoUHoMHNWtEGzw4qy/IV+SrVH+Sr4bzVYv+fige9FgdqhPQ7dRRfXUkPAgP0mco91nAB/gAH+AjniEwpPeoo55fHWUNd9nBHB0dWQNAHup65UhPSWtTs0FWLaX9WnQFQetx8eeq4a7p3TXny12PXv/Wxn04+9GD9ejilbjq8Aw+Qj4DHyGfgw/wkfI2+AAfqd6DP+CPXD0Af8Af8Ee5XgYf4AN8gI+hfhr6Cn2Fvir3mxcJH2Ypowbt+vq6Ndf1ZNHBwYEbj8fWxN7b23OHh4fF/XoKUMFyfX3ddJz/3NXVVTuvfr2rOV/uemL7DO6D9VC8ElcBz+Aj5DPyVZfPwQf4yPE9+AAfqd6DP+CPtB6AP+AP+KNcL4MP8AE+wEdNPw199bz1lfqSeiOZvmh9X5j+7mzfXDi3hrvsR1KPrVpPdz2Zroa7Gt2p906Np7sCWcSkJ+tynpmbm52nUd/16HVjbdyHszcFWI9dS5DElXPCD/gI+QN8/M9mbYCP4KkIPsBHqi/AB/jIeY7CH/BHWtfAH/AH/BFmfoCP4Nme9i/gD/gDfICPoX4r9cdi1h9mKSOx4BdYTWsNMI29gjTIoLRfA1H0b0UsLcfpnPpcBZ7+rIGn3nO673z+uPh64mvlPlgP4moar+Aj5DPyFXk35ZcWfMTNlZjviCvi6mviCn3V/UBe0p/oxPD9tOSrlxJXpbya0/vk3dn6DHzMhw/4nLzbxzv0GfLxgd5F76J36YsO9VvRJdO6xBrueiJapKNNf97Z2ZnybD89PS3ulyBWAbC8vNx0nLzN9Ll3d3fWcPdFhP//LdcT/9uW4+L75D5YjzTOiauQB8AH+AAf8OAQv8LnQUfBH/BHPPsIvTudP8EH+AAfYXYa+gp9hb6q70PBH/AH/AF/+NmjL6FPPfrll1++yG8unmirG6j9uxr0//7778Typfa4lZWViYXMDz/8MGno+//f8jnyj9fGfXQ/mLAezn7AIa6cE57AR8hn4IO8m/Ib+AAfqd6AP25NU8EfXb708QF/wB/wR7k+BB/gA3yAj6H+DfoKfeX1JfpqNl+Aj8XEx+jPP//8Iv91DXfwnpUSTbV/l/e6GhYyyW85ThY0+vfv37+3Rrm8zeY5vz5Hry3o107uw5kXPutBXMX4BR8hn4GPxc+74pHYI2+Il0r4aPkc4mrx42oojtL95N3Hzbst+EQndvj0uoB8Rb56KfkKnFOfk68WP1+Bc3AOzhcf5991HaWhqfo1Rc3q3FRtP222tP/HH380Ia8mb8sUX6bY5qfY+sGxrAdxdXh4aD9GCZf39/f2g9QQHnP7mYL+vKegk3fhD3Ae9Af5inw1Ho+n+A58gI8cT1J/dHUE+AAf4GNkPyjm+hDgA3yAD/BRyg9xvy2tPyRE5VXe0t9El6BLpMvSuBqp4a5BVUNTc0v7mbrN1G2mbjN1eyh/MHV7MaduS4zAH87e0NIPzyLYk5OTicWafsxO82NuP/gAH/4Nvzg+iCv0FfoKfYW+CvwwxK/xINSh7426tv97RZegS9AlzukNOXR7f11D3g3fD7od3Z7T7TY0Vb/oDE2bLe1nWjXTqplWzbTqofzBtOrpadUSseRd514af8Si8vPnz5Nh3y/tPuDzgMd4HYVJ8Rn5inylJ+L0YyI4D3nusfBRyqv+fOQr8lUOj7n40I/ctfoKPieuHiOuyFfEFXE1q5/g866+oF/Snx/i76eFz5973rWGu57MG5qOXdovAOkLWV5edjs7YWryu3fvXN/UWL//7u7OGk/+S609rjShmvtgPRR3xFWY9s40d6a5M82dae5qQgzxK3ze6Rb4A/4QXqSNY10LPsBHWtegr9BX6Cv0FfpqWF/S95nti8Ef8Af88X3wh1nKbG1tudQzs9ZDXIWpipLt7W3zEqw9znspnZ2dWbNfnkfzeBnLs/r8/NyeaOA+RtYoYD0OHHEVPHDBR8hL4KPzuAQf4CM3EwJ8gI/UqxL+gD9SXf8t+KPGO5V8Rb4iX4WZH+kMJ/ABPsAH+Bjq730LPq/p7z1kvqrRDzlPd/Quevex9K413D9+/NjsOeu97/R6hBq8AkqNV23qmacn4wVEvbab8wqr8cDV6/zauA9nr6uwHif2xgVx5ezNEfARPGDBR+dFCD5eG2eAj2mPefABPlKvUvgD/kC3l72uwQf4AB/go6Z/gb5CX6Gvyl7w4AN8LDI+zFJmZWVlYumiV2aXlpZs2rcalhsbG+7+/r643x8rr8uW4/znykNP57y5uak6X+56bm9vrXnCfYwm3wHrQVx5H0vwEfIZ+arL5+Td4PNawofeulIh/Rg8KK85bfDgrL4gX5GvUv1JvhrOVy36+6F40GN1qE5At1NH9dWR8CA8SJ+h3GcBH+ADfICP2M98SO9RRz2/Osoa7rKDiT2E9EqFnpLWpmaDrFpK+7XoCgJ5c7UcF3+uGu6a6ltzvtz16HUYbdyHsx89WI8uXomrzlMPfIR8Bj5CPgcf4CPlbfABPlK9B3/AH7l6AP6AP+CPct0LPsAH+AAfQ30x9BX6Cn1V7jcvEj7MUkYN2lbvde8JpacAFSzX19fmDTzkFZXuX11dtaa+fr3LecrmPJZSr3gJG28PwH2wHooP4mp9MhMBfIS8RL7as1kb4AN85DwVwQf4QF+VZxGBD/ABPsDHUJ2JvkJfoa9G5pSQ6wuBD/DxUvAxjxc8OhGdmNOJ1nCX/YgSo3/FXa/Rx76Em5ubxf16Ml0NdwmQluPkt67P1av1Ap6erPM+7H3n88fF16vXjbVxH87eFGA9iCvwkc9n4IO8m/IL/BH4HnyAD/CB3h2qB9DtQV/AH/BHWi+DD/CR66egr9BX6Cv0Ffqqvt+8SPrKLGX0BKwXCGpaa4Bp7BWkQQal/RIa+rdKIi3H6Zz6XDXp9Wc10r3ndN/5/HHx9cTXyn2wHsTVNF7BR8hn5CvybsovLfiIyT/mO+KKuPqauEJfdQ9elPQnOjF8Py356qXEVSmv5vQ+eXe2PgMf8+EDPifv9vEOfYZ8fKB30bvoXfqiQ/1WdMm0LrGGu56IFulo0593dnamPNtPT0+L+yWIVQAsLy83HSdvM33u3d2dNdx9EeH/f8v1xP+25bj4PrkP1iONc+Iq5AHwAT7ABzw4xK/wedBR8Af8Ec8+Qu9O50/wAT7Ax5HV3bm6F3yAD/ABPkr5gf7VbJ+S+oP6I9fHfi79K7OU0ZCsVu9172GnAFeTXgNLU8+aGk93GeJLWMjzKOfptLfXeQH1eZDuMCYAACAASURBVOZpKIU27mNkP2CwHgc2KJS4ck74AR8hf4CPzlMRfIwnM0PAB/hI9QX4AB+5mULwx7fjjxrvVNbj261HTX3GerAead2PvkJfoa+CZzv4KM96fEj+qNEPuRkD5Cvy1WPlK2u4y84lHqy4u7trVi/aZBVzcnJS3K/XStTgFVBajvOfqyfjJeT0mmjN+XLXE/vncR+sh+KVuAqDhMFHyGfkqy6fgw/w4QeNx7wNPsBHqvfgD/gjrQfgD/gD/nD2Znau7gUf4AN8gI9Sfoj7VOgr9BX6qtxvXiR8jP74448verr89vbWGt6vXr2y5rp/4v3nn392l5eXxf1ra2u2/82bN03H+c/98OGDHXt1dWXnHzpfbr+a/dq4D+dYjy5eiauAZ/AR8hn4yONDb0IoB7fwwHONK93Hzc1NNZ891/vo4114EF1SEx+LzIPgHN0+L597vltkfNTkh5jvnysPgnNwPi/O6TPM6iRwTj2Y9vfgwbZ+yVC9TL6iD5fqL+Xd0W+//fZldXV18oS5nnDT5p84H/q7Ghv6NV//tRznP1cXof/U6Nc2dL7c/uvrazuW+3DWaGI9nMUUcdXhCXyEfAY+uvwOPro3qsDHNN+DD/CR6jj4A/5I6wH4A/7I1WvwB/wBf5T7J+ADfIAP8DHUX0VfLaa+sqGpeqJdTxHI2kV/VvP7/fv31pD46aefrIlb2h9Pq245zn9uOsV26Hy5/X7gKvfhHOvRxStxFfAMPkI+Ax/gI+WzFnzE/zbmO+KKuPqauEJf9etP+Hyx+byUV4fqAfIuefdr8i58Tt6t7Xu06ET4nLgirugnygKbvuhrJ9vymn7yovd3reEuD/Z4Ino8/Tg3PZ3pyExHTqenx/FT+jNxNRs3Q7hj6jZTt0VUytHkXfIueffIhBu65J07PT0d1G3wB/wBf8zyp88f4AN8gA/wkerrnL6grg36Ww836gcIvU3fUteTd2d1G3FFXKV1HbpkMXWJDU3d2tqaeLbLm+jwsJtSq+3vv/926VTleP/9/b01g7a3t5uO8597dnZmRaP812vOl7ueoanCNZ/LfXTrzHqMLQ6FA+Iq5AHwAT5SHgAf4CPVCfAH/OH5E504q6PBB/gAH52+ztWZ4AN8gA/wUdOHov6g/qD+KPdpwcfzw4c13PW4fzw1OTd1vbRfHrhquOsXmZbj9KrFycmJ/UKqhrv3NPL/v+V6FmmKrcQG69F5KxNX4INp7q/tB6B02j34gD/En+ADfOTyA/qqyw/gA3yAj1n9kKuzqKN2zYrSe9NTf1B/wB/wB/wBf6T191Cfkvqc+jxXf5ilzMrKijV0tIlgFCx6klyN8Ldv31ozvLTfH6t/13Kc/9x//vnHzikfvprz5a7n9rabMMx9jCbfAetBXHk8g4+Qz8hXXT4n7wa+ewp86PvXBg/O6ounWI9U74CPp8UH61HW388JH/5ahuoEdDt1VF8dCQ/Cg/QZyn0W8AE+wAf4aOnTPiedSH/3tdN6WMNddjB93rB9Hl1LS0vWjB/yVO3br4b7p0+frAFR46WWXo9eQ9TGfTjHegSva+Kq8zoGH8HrGHyAD/ijPEsCfIAP8AE+auoB9BX6Kq3r4A/4A/6AP+CPullD1OfTnvbwB/yxyPxhljJqmMsLSU+YX1xcFD3bc/vlva4m+fX1ddNx3qNrdXXVziuP6NS7rfZ64te+uA/WQ96QxFXAM/g4mMyoIF913qngA3zA52W9Az7AB/gAHzX1EPoKfTUeB+919FXnnevzJ/gAH+BjOj+Aj3y/8TnW50MzINGJ6MRanWgNd72upAO0bWxsmI9d7Ocnv6LSfj2Zroa7EkjLcfIp1+f6V4f1y5b3L+87nz8uvp749Xzug/UgrqbxCj5CPiNfkXdTfgEf4CPVO+iSoAfBB/gAH/8t1ifgA3yAD/BR07+g/qD+oP6gnzjUp6T+WMz6wyxl9At0rWd76sUooaEn5JVEajz6Ug8iNel1fjXS5/GQ1/XEn8l9sB6aJUBcBQ9Y8BG838hX3YwO8DEfPlLvdY8t4oq4SmfYkHfJu+jd8myo+Lsp5dWhmVLkXfLu1+Rd+Lx/Vhs6cT6dWOpDkK/IV1+Tr4gr8lXtrEvqj+dXf1jDXU+oaxHn8VCXINbCLi8vuz6v95I3+93dnTXcfXDM4wUfXzv3wXqcnp464irgGXwET0XyVeeZBz7AR47vwQf4UH4ocQb6Cn0Ff4CPUh6AP+AP+GM6P8R9EfABPsAH+BjS0dTni1mfm6XM1taWOzk5sYb77u6uOz4+dmtra1Z0XV5euv39/eJ+BYaa6RpY2nKc/1wNdNR55N1Uc77c9Zyfn9u1cx8ja6SxHvs2KJS46vAMPkI+Ax9dPgcfge/AB/hI9Q74AB85PQx/fDv+0Pd/dXVF/dFTf1FHzdan8Dl8Dp+X+zfgA3x8D/io0Q/0E+nv9vW3H1pfWcNddi7xYBM16fQqmTZZxagZX9qv12PU4FUh0nKc/1w9Ga/GqOxoas6Xu57YP4/7YD0Ur8TVa8OT3hwBHyGfka+6fA4+wIfPDzFvgw/wkeo9+AP+SOsB+AP+gD86fZ2re8EH+AAf4KOUH+I+FfoKfYW+KvebFwkfZimzsrIy5YOeekz1ebP7Y+Wl3nKc94KXh56Sz83NjTXeU4/4IQ9H7b+9vbXmIvcxmnwHrAdx5WcigI//TGZUkK/e2o+b5N3gLfwU+Ei9Y1mPp12P1BuT9WA9cjOF4I9p/vC5E91O/aH6DXygr9J+wVPoK/i87HXNelAPgg/wUdNvhc8fls+t4S47mKOjI2tat3qoLy0tWbO+9bjY011Nc03vnsdDXufV69/auA/nWI8vk1kCxFWHZ/ARPOPAB/hIZ42AD/CR81SEP+CPVNfCH/AH/BFm4oCPzpMa/ijPgENfoa/AB/io6e+hr9BXi6yvzFJGDfP19XUTDRcXF+7g4MCNx2NrYu/t7bnDw8Pifnmvq3l+fX3ddJz/3NXVVTvv/f191fly1xO/nsN9sB6KV+Iq4Bl8hHxGvuryOfgAHzm+Bx/gI9V78Af8kdYD8Af8AX+U62XwAT7AB/io6aehr563vlJjUm8E0Bet7wvT353tmwvno19//dUsZdQw1yahoF+Z4ldO9PR5ab8KdL2i9ObNm6bj9AqqPvfDhw/2GqI84P0rDn3n88fF1+PtaLiPbvgs60FcgY98PgMf5N2UX8Qf2l69evXNeDC2lIEHp/UFfB70F/mKfJXLV96+46l1u39QJqfLh+oIcA7O0zqTejDoVvABPsBHsP54zjxI/2p1YgtNH44+HP3drm6Z6cP9/vvvXzSx2AtnDXvRE+u1f1dzV8JajfqW4/x5JCp0YQKpttbz699rkqw27sNZs531+GLJn7jq8AQ+Qj4DH11+T/Ghxp622rxPXE3zJHGVjyv4HF2ivAI+wEdaH8yjS1Kemjeu/OegE7s3i+Fz+DxXvz8VPh4K5/QZZvs58+TdVMfNm3dZD9YjzjMvFedD9TL4QO/m9K5Zynz8+NGa1dqurq7c/v6+Ozk5sb9rAvvx8XFxvwal6oP1C1/Lcf5z02nuQ+fL7Y+n2HIfrIfilbgKeAYfIZ+Rr7p8Dj7AR47vwQf4SPUe/AF/pPUA/AF/wB/lehl8gA/wAT5q+mnoK/QV+qrcb14kfNjQVD+JVgQhnxkV3ekUY/m85/YzxfZhp9jqO2Y9RpPvQK+mtMSj/v3nz5+dLBsUy/61TP//S3Gc2880d6a5M82dae6PMc09tpQhX322weteX5B3ybvk3ZeRdz1W0Vfodm9zpFyObqf+gM9n+yn0S6jPpffRu/QT+/qt9K8Wsx60hvv29rY7OjqygjedOq+n19OpsfFUdqYKM1V4kacKCxPgw5lNkURCa36I84d+AJHNj7ahvJLbf3Z2ZseyHqwHcTXLy+Dj1ObAaCNfoUvQJTtFXQ8+wAf4AB81dT+6veuLoK/QV15fUn9Qf9AXLfMn+Mjjwyxl1EhbXw9TxZnGyzRe7+Xspw23xAdTt5/31G2Jhprp6XEekGeZkqiGQLTkB6ZV56dVqylI3nU25Jm4OrQZKMIleffC3kwCH2P7DsBHlz/BR9Dn4AN9NR53+QF9hb5SfizVZ/AH/JHGB/wBf8Af0/wZ588UH+JZvXHZ0vcg75J3c3nXGu56/e+vv/4yAbe5uWn+7XHQyTe9tF9PrKphooBtOU7NJn2uf3VYT9z4IrvvfP64+Hri1/O5D9aDuJrGK/gI+Yx8Rd5N+QV8gI9U76BLgh4EH+ADfIQfAeEP6sGhOhP+gD9y/RTqD+oP+AP+gD/q+82LVH+YpYzEdK1ne+rVKEN7NcHVqE+9qWq8txV4Or8Gt87jIa/rif2wuA/WQ3FHXAUPWPARPPPIV92MDvAxHz5S73WPLeKKuEr1DnmXvJvOjCHv5vNuKa8OzZQi75J3vybvwuflWRHoxOlZdvA5fA6f573X6cN1swmoB6fjA707rXet4a4n1HPeVDWe7hLECjJNZe/zNCp5Nt/d3VnD3QfqPB7R8bVzH6yHZgwQV52XcYpt8AE+wEfwogQf015z8Pk7Bz7AR4knwQf4iGdYwR/wR67upf6g/sjNqoI/4A/4Y1pfxvkTfICPRcaHWcpsbW2ZHYw2ve5yfHzs1tbWrAl/eXnp9vf3i/slLCQ6Nciw5Tj/uRpEovPI86jmfLnrOT8/t2vnPkbWaGY99m3ADXHV4Rl8hHwGPrp8Dj4C34EP8JHqHfABPnJ6GP74dvyh7//q6or6o6f+oo6arU/hc/gcPi/3b8AH+Pge8FGjH+gn0t/t628/tL6yhrvsXGo921MPdb12pgavCpFW73U1+fVkvBqjeh1jHg95XY9eK9XGfTizi2A9iKsYz+AjeMaBj25GB3k3ePKCD/CReiqCD/CRmykEf8Af6awq+AP+gD/KnrzgA3yAD/BR099DX6GvFllfmaXMysrKlA+6BpheXFxYI3xjY8Pd398X9/tj5eHUcpz/XHnoqTl4c3NTdb7c9dze3lrDnfsYTb4D1oO48jMRwEfIZ+SrLp+Td4PXXAkfeutKhcJj8KBmjWiDB2f1BfmKfJXqT/LVcL5q0d8PxYMeq0N1ArqdOqqvjoQH4UH6DOU+C/gAH+ADfMQzBIb0HnXU86ujrOEuO5ijoyNrAMhDXa8c6SlpbWo2yKqltF+LriBoPS7+XDXcNb275ny569Hr39q4D2c/erAeXbwSVx2ewUfIZ+Aj5HPwAT5S3gYf4CPVe/AH/JGrB+AP+AP+KNfL4AN8gA/wMdRPQ1+hr9BX5X7zIuHDLGXUoF1fX7fmup4sOjg4cOPx2JrYe3t77vDwsLhfTwEqWK6vr5uO85+7urpq59WvdzXny11PbJ/BfbAeilfiKuAZfIR8Rr7q8jn4AB85vgcf4CPVe/AH/JHWA/AH/AF/lOtl8AE+wAf4qOmnoa+et75SX1JvJNMXre8L09+d7ZsL59Zwl/1I6rFV6+muJ9PVcFejO/XeqfF0VyCLmPRkXc4zc3Oz8zTqux69bqyN+3D2pgDrsWsJkrhyTvgBHyF/gI//2awN8BE8FcEH+Ej1BfgAHznPUfgD/kjrGvgD/oA/wswP8BE829P+BfwBf4AP8DHUb6X+WMz6wyxlJBb8AqtprQGmsVeQBhmU9msgiv6tiKXlOJ1Tn6vA05818NR7Tvedzx8XX098rdwH60FcTeMVfIR8Rr4i76b80oKPuLkS8x1xRVx9TVyhr7ofyEv6E50Yvp+WfPVS4qqUV3N6n7w7W5+Bj/nwAZ+Td/t4hz5DPj7Qu+hd9C590aF+K7pkWpdYw11PRIt0tOnPOzs7U57tp6enxf0SxCoAlpeXm46Tt5k+9+7uzhruvojw/7/leuJ/23JcfJ/cB+uRxjlxFfIA+AAf4AMeHOJX+DzoKPgD/ohnH6F3p/Mn+AAf4CPMTkNfoa/QV/V9KPgD/oA/4A8/e/Ql9KlHv/zyyxf5zcUTbXUDtX9Xg/7ff/+dWL7UHreysjKxkPnhhx8mDX3//1s+R/7x2riP7gcT1sPZDzjElXPCE/gI+Qx8kHdTfgMf4CPVG/DHrWkq+KPLlz4+4A/4A/4o14fgA3yAD/Ax1L9BX6GvvL5EX83mC/CxmPgY/fnnn1/kv67hDt6zUqKp9u/yXlfDQib5LcfJgkb//v3799Yol7fZPOfX5+i1Bf3ayX0488JnPYirGL/gI+Qz8LH4eVc8EnvkDfFSCR8tn0NcLX5cDcVRup+8+7h5twWf6MQOn14XkK/IVy8lX4Fz6nPy1eLnK3AOzsH54uP8u66jNDRVv6aoWZ2bqu2nzZb2//jjjybk1eRtmeLLFNv8FFs/OJb1IK4ODw/txyjh8v7+3n6QGsJjbj9T0J/3FHTyLvwBzoP+IF+Rr8bj8RTfgQ/wkeNJ6o+ujgAf4AN8jOwHxVwfAnyAD/ABPkr5Ie63pfWHhKi8ylv6m+gSdIl0WRpXIzXcNahqaGpuaT9Tt5m6zdRtpm4P5Q+mbi/m1G2JEfjD2Rta+uFZBHtycjKxWNOP2Wl+zO0HH+DDv+EXxwdxhb5CX6Gv0FeBH4b4NR6EOvS9Udf2f6/oEnQJusQ5vSGHbu+va8i74ftBt6Pbc7rdhqbqF52habOl/UyrZlo106qZVj2UP5hWPT2tWiKWvOvcS+OPWFR+/vx5Muz7pd0HfB7wGK+jMCk+I1+Rr/REnH5MBOchzz0WPkp51Z+PfEW+yuExFx/6kbtWX8HnxNVjxBX5irgirmb1E3ze1Rf0S/rzQ/z9tPD5c8+71nDXk3lD07FL+wUgfSHLy8tuZydMTX737p3rmxrr99/d3VnjyX+ptceVJlRzH6yH4o64CtPemebONHemuTPNXU2IIX6FzzvdAn/AH8KLtHGsa8EH+EjrGvQV+gp9hb5CXw3rS/o+s30x+AP+gD++D/4wS5mtrS2XembWeoirMFVRsr29bV6Ctcd5L6WzszNr9svzaB4vY3lWn5+f2xMN3MfIGgWsx4EjroIHLvgIeQl8dB6X4AN85GZCgA/wkXpVwh/wR6rrvwV/1Hinkq/IV+SrMPMjneEEPsAH+AAfQ/29b8HnNf29h8xXNfoh5+mO3kXvPpbetYb7x48fmz1nvfedXo9Qg1dAqfGqTT3z9GS8gKjXdnNeYTUeuHqdXxv34ex1FdbjxN64IK6cvTkCPoIHLPjovAjBx2vjDPAx7TEPPsBH6lUKf8Af6Pay1zX4AB/gA3zU9C/QV+gr9FXZCx58gI9FxodZyqysrEwsXfTK7NLSkk37VsNyY2PD3d/fF/f7Y+V12XKc/1x56OmcNzc3VefLXc/t7a01T7iP0eQ7YD2IK+9jCT5CPiNfdfmcvBt8Xkv40FtXKqQfgwflNacNHpzVF+Qr8lWqP8lXw/mqRX8/FA96rA7VCeh26qi+OhIehAfpM5T7LOADfIAP8BH7mQ/pPeqo51dHWcNddjCxh5BeqdBT0trUbJBVS2m/Fl1BIG+uluPiz1XDXVN9a86Xux69DqON+3D2owfr0cUrcdV56oGPkM/AR8jn4AN8pLwNPsBHqvfgD/gjVw/AH/AH/FGue8EH+AAf4GOoL4a+Ql+hr8r95kXCh1nKqEHb6r3uPaH0FKCC5fr62ryBh7yi0v2rq6vW1NevdzlP2ZzHUuoVL2Hj7QG4D9ZD8UFcrU9mIoCPkJfIV3s2awN8gI+cpyL4AB/oq/IsIvABPsAH+BiqM9FX6Cv01cicEnJ9IfABPl4KPubxgkcnohNzOtEa7rIfUWL0r7jrNfrYl3Bzc7O4X0+mq+EuAdJynPzW9bl6tV7A05N13oe973z+uPh69bqxNu7D2ZsCrAdxBT7y+Qx8kHdTfoE/At+DD/ABPtC7Q/UAuj3oC/gD/kjrZfABPnL9FPQV+gp9hb5CX9X3mxdJX5mljJ6A9QJBTWsNMI29gjTIoLRfQkP/Vkmk5TidU5+rJr3+rEa695zuO58/Lr6e+Fq5D9aDuJrGK/gI+Yx8Rd5N+aUFHzH5x3xHXBFXXxNX6KvuwYuS/kQnhu+nJV+9lLgq5dWc3ifvztZn4GM+fMDn5N0+3qHPkI8P9C56F71LX3So34oumdYl1nDXE9EiHW36887OzpRn++npaXG/BLEKgOXl5abj5G2mz727u7OGuy8i/P9vuZ7437YcF98n98F6pHFOXIU8AD7AB/iAB4f4FT4POgr+gD/i2Ufo3en8CT7AB/g4sro7V/eCD/ABPsBHKT/Qv5rtU1J/UH/k+tjPpX9lljIaktXqve497BTgatJrYGnqWVPj6S5DfAkLeR7lPJ329jovoD7PPA2l0MZ9jOwHDNbjwAaFElfOCT/gI+QP8NF5KoKP8WRmCPgAH6m+AB/gIzdTCP74dvxR453Keny79aipz1gP1iOt+9FX6Cv0VfBsBx/lWY8PyR81+iE3Y4B8Rb56rHxlDXfZucSDFXd3d83qRZusYk5OTor79VqJGrwCSstx/nP1ZLyEnF4TrTlf7npi/zzug/VQvBJXYZAw+Aj5jHzV5XPwAT78oPGYt8EH+Ej1HvwBf6T1APwBf8Afzt7MztW94AN8gA/wUcoPcZ8KfYW+Ql+V+82LhI/RH3/88UVPl9/e3lrD+9WrV9Zc90+8//zzz+7y8rK4f21tzfa/efOm6Tj/uR8+fLBjr66u7PxD58vtV7NfG/fhHOvRxStxFfAMPkI+Ax95fOhNCOXgFh54rnGl+7i5uanms+d6H328Cw+iS2riY5F5EJyj2+flc893i4yPmvwQ8/1z5UFwDs7nxTl9hlmdBM6pB9P+HjzY1i8ZqpfJV/ThUv2lvDv67bffvqyurk6eMNcTbtr8E+dDf1djQ7/m67+W4/zn6iL0nxr92obOl9t/fX1tx3IfzhpNrIezmCKuOjyBj5DPwEeX38FH90YV+Jjme/ABPlIdB3/AH2k9AH/AH7l6Df6AP+CPcv8EfIAP8AE+hvqr6KvF1Fc2NFVPtOspAlm76M9qfr9//94aEj/99JM1cUv742nVLcf5z02n2A6dL7ffD1zlPpxjPbp4Ja4CnsFHyGfgA3ykfNaCj/jfxnxHXBFXXxNX6Kt+/QmfLzafl/LqUD1A3iXvfk3ehc/Ju7V9jxadCJ8TV8QV/URZYNMXfe1kW17TT170/q413OXBHk9Ej6cf56anMx2Z6cjp9PQ4fkp/Jq5m42YId0zdZuq2iEo5mrxL3iXvHplwQ5e8c6enp4O6Df6AP+CPWf70+QN8gA/wAT5SfZ3TF9S1QX/r4Ub9AKG36VvqevLurG4jroirtK5DlyymLrGhqVtbWxPPdnkTHR6GKbW5Kb7x/oecKqzEM3Q+pgpfuHTKNesxG69nZ2fWjJCvP3F1bk0qcD4yKxWJ6+3t7aY85/MOcTW2WBJPMM2dae6PNc2dfLVuvEW+6vQOeff7y7vKAX///Td69+BgsD4DH98fPmrqGvgD/kj7Beh2dPv3oNtr9AP9RPqJff3Uh+77WMNdj/vHU5NzU9dL++WBqwaWiL3lOL1qcXJyYr+QSjh4TyP//1uuZ5Gm2CpJsB6dtzJxBT6Y5v7aGtzptHvwAX+IP8EH+MjlB/RVlx/AB/gAH7P6IVdnUUftmhWl96an/qD+gD/gD/gD/kjr76E+JfU59Xmu/jBLmZWVFWvoaBPBKFj0ZIka4W/fvrVmeGm/P1b/ruU4/7n//POPnVM+fDXny13P7W03YZj7GE2+A9aDuPJ4Bh8hn5GvunxO3g189xT40PevDR6c1RdPsR6p3gEfT4sP1qOsv58TPvy1DNUJ6HbqqL46Eh6EB+kzlPss4AN8gA/w0dKnfU46kf7ua6f1sIa77BX6vGH7PLqWlpasGT/kqdq3Xw33T58+WQOixkstvR69TqmN+3CO9Qhe18RV53UMPoLXMfgAH/BHeZYE+AAf4AN81NQD6Cv0VVrXwR/wB/wBf8AfdbOGqM+nPe3hD/hjkfnDLGXUME89ncbj4InX5+kuj2w1ya+vr3u9Fkve7Kurq/Zk+/39/cQbuMVDXp8bv/bFfbAeih/iqvMABh/THmXkq25GB/gAHzkPWPABPlL9hb4KHtrgA3yAj/KML/ABPsAH+Bjqw1B/UH+8lPpjHi94eBAezPGgNdz1upIac9o2NjbMxy7285NfUWm/nkxXw10JtuU4+ZTrc/2rw/ply/uX953PHxdfT/x6PvfBehBX03gFHyGfka/Iuym/gA/wkeoddEnQg+ADfICP/xbrE/ABPsAH+KjpX1B/UH9Qf9BPHOpTUn8sZv1hljJ6gqnWsz31YpTQ0BPySiI1Hn2pB5Ga9Dq/GunzeMjreuLP5D5YD80SIK6CByz4CN5v5KtuRgf4mA8fqfe6xxZxRVylM2zIu+Rd9G55NlT83ZTy6tBMKfIuefdr8i583j+rDZ04n04s9SHIV+Srr8lXxBX5qnbWJfXH86s/rOGuJ9S1iPN4qEsQa2GXl5ddn9d7yZv97u7OGu4+OObxgo+vnftgPU5PTx1xFfAMPoKnIvmq88wDH+Ajx/fgA3woP5Q4A32FvoI/wEcpD8Af8Af8MZ0f4r4I+AAf4AN8DOlo6vPFrM/NUmZra8udnJxYw313d9cdHx+7tbU1K7ouLy/d/v5+cb8CQ810DSxtOc5/rgY66jzyPKo5X+56zs/P7dq5j5E10liPfRsUSlx1eAYfIZ+Bjy6fg4/Ad+ADfKR6B3yAj5wehj++HX/o+7+6uqL+6Km/qKNm61P4HD6Hz8v9G/ABPr4HfNToB/qJ9Hf7+tsPra+s4S47l3gwlpp0epVMm6xi1Iwv7dfrMWrwqhBpOc5/rp6MV2NUdjQ158tdT+yfx32wHopX4uq14UlvjoCPkM/IV10+Bx/gw+eHmLfBB/hI9R78AX+k9QD8AX/AH52+ztW94AN8gA/w9SvUBgAAIABJREFUUcoPcZ8KfYW+Ql+V+82LhA+zlFlZWZnyQU89pvq82f2x8lJvOc57wctDT8nn5ubGGu+pR/yQh6P2397eWnOR+xhNvgPWg7jyMxHAx38mMyrIV2/tx03ybvAWfgp8pN6xrMfTrkfqjcl6sB65mULwxzR/+NyJbqf+UP0GPtBXab/gKfQVfF72umY9qAfBB/io6bfC5w/L59Zwlx3M0dGRNa1bPdSXlpasWd96XOzprqa5pnfP4yGv8+r1b23ch3Osx5fJLAHiqsMz+AieceADfKSzRsAH+Mh5KsIf8Eeqa+EP+AP+CDNxwEfnSQ1/lGfAoa/QV+ADfNT099BX6KtF1ldmKaOG+fr6uomGi4sLd3Bw4MbjsTWx9/b23OHhYXG/vNfVPL++vm46zn/u6uqqnff+/r7qfLnriV/P4T5YD8UrcRXwDD5CPiNfdfkcfICPHN+DD/CR6j34A/5I6wH4A/6AP8r1MvgAH+ADfNT009BXz1tfqTGpNwLoi9b3henvzvbNhfPRr7/+apYyaphrk1DQr0zxKyd6+ry0XwW6XlF68+ZN03F6BVWf++HDB3sNUR7w/hWHvvP54+Lr8XY03Ec3fJb1IK7ARz6fgQ/ybsov4g9tr169+mY8GFvKwIPT+gI+D/qLfEW+yuUrb9/x1LrdPyiT0+VDdQQ4B+dpnUk9GHQr+AAf4CNYfzxnHqR/tTqxhaYPRx+O/m5Xt8z04X7//fcvmljshbOGveiJ9dq/q7krYa1Gfctx/jwSFbowgVRb6/n17zVJVhv34azZznp8seRPXHV4Ah8hn4GPLr+n+FBjT1tt3ieupnmSuMrHFXyOLlFeAR/gI60P5tElKU/NG1f+c9CJ3ZvF8Dl8nqvfnwofD4Vz+gyz/Zx58m6q4+bNu6wH6xHnmZeK86F6GXygd3N61yxlPn78aM1qbVdXV25/f9+dnJzY3zWB/fj4uLhfg1L1wfqFr+U4/7npNPeh8+X2x1NsuQ/WQ/FKXAU8g4+Qz8hXXT4HH+Ajx/fgA3ykeg/+gD/SegD+gD/gj3K9DD7AB/gAHzX9NPQV+gp9Ve43LxI+bGiqn0QrgpDPjIrudIqxfN5z+5li+7BTbPUdsx6jyXegV1Na4lH//vPnz06WDYpl/1qm//+lOM7tZ5o709yZ5s4098eY5h5bypCvPtvgda8vyLvkXfLuy8i7HqvoK3S7tzlSLke3U3/A57P9FPol1OfS++hd+ol9/Vb6V4tZD1rDfXt72x0dHVnBm06d19Pr6dTYeCo7U4WZKrzIU4WFCfDhzKZIIqE1P8T5Qz+AyOZH21Beye0/OzuzY1kP1oO4muVl8HFqc2C0ka/QJeiSnaKuBx/gA3yAj5q6H93e9UXQV+grry+pP6g/6IuW+RN85PFhljJqpK2vh6niTONlGq/3cvbThlvig6nbz3vqtkRDzfT0OA/Is0xJVEMgWvID06rz06rVFCTvOhvyTFwd2gwU4ZK8e2FvJoGPsX0H4KPLn+Aj6HPwgb4aj7v8gL5CXyk/luoz+AP+SOMD/oA/4I9p/ozzZ4oP8azeuGzpe5B3ybu5vGsNd73+99dff5mA29zcNP/2OOjkm17arydW1TBRwLYcp2aTPte/OqwnbnyR3Xc+f1x8PfHr+dwH60FcTeMVfIR8Rr4i76b8Aj7AR6p30CVBD4IP8AE+wo+A8Af14FCdCX/AH7l+CvUH9Qf8AX/AH/X95kWqP8xSRmK61rM99WqUob2a4GrUp95UNd7bCjydX4Nb5/GQ1/XEfljcB+uhuCOuggcs+AieeeSrbkYH+JgPH6n3uscWcUVcpXqHvEveTWfGkHfzebeUV4dmSpF3ybtfk3fh8/KsCHTi9Cw7+Bw+h8/z3uv04brZBNSD0/GB3p3Wu9Zw1xPqOW+qGk93CWIFmaay93kalTyb7+7urOHuA3Uej+j42rkP1kMzBoirzss4xTb4AB/gI3hRgo9przn4/J0DH+CjxJPgA3zEM6zgD/gjV/dSf1B/5GZVwR/wB/wxrS/j/Ak+wMci48MsZba2tswORptedzk+PnZra2vWhL+8vHT7+/vF/RIWEp0aZNhynP9cDSLReeR5VHO+3PWcn5/btXMfI2s0sx77NuCGuOrwDD5CPgMfXT4HH4HvwAf4SPUO+AAfOT0Mf3w7/tD3f3V1Rf3RU39RR83Wp/A5fA6fl/s34AN8fA/4qNEP9BPp7/b1tx9aX1nDXXYutZ7tqYe6XjtTg1eFSKv3upr8ejJejVG9jjGPh7yuR6+VauM+nNlFsB7EVYxn8BE848BHN6ODvBs8ecEH+Eg9FcEH+MjNFII/4I90VhX8AX/AH2VPXvABPsAH+Kjp76Gv0FeLrK/MUmZlZWXKB10DTC8uLqwRvrGx4e7v74v7/bHycGo5zn+uPPTUHLy5uak6X+56bm9vreHOfYwm3wHrQVz5mQjgI+Qz8lWXz8m7wWuuhA+9daVC4TF4ULNGtMGDs/qCfEW+SvUn+Wo4X7Xo74fiQY/VoToB3U4d1VdHwoPwIH2Gcp8FfIAP8AE+4hkCQ3qPOur51VHWcJcdzNHRkTUA5KGuV470lLQ2NRtk1VLar0VXELQeF3+uGu6a3l1zvtz16PVvbdyHsx89WI8uXomrDs/gI+Qz8BHyOfgAHylvgw/wkeo9+AP+yNUD8Af8AX+U62XwAT7AB/gY6qehr9BX6Ktyv3mR8GGWMmrQrq+vW3NdTxYdHBy48XhsTey9vT13eHhY3K+nABUs19fXTcf5z11dXbXz6te7mvPlrie2z+A+WA/FK3EV8Aw+Qj4jX3X5HHyAjxzfgw/wkeo9+AP+SOsB+AP+gD/K9TL4AB/gA3zU9NPQV89bX6kvqTeS6YvW94Xp7872zYVza7jLfiT12Kr1dNeT6Wq4q9Gdeu/UeLorkEVMerIu55m5udl5GvVdj1431sZ9OHtTgPXYtQRJXDkn/ICPkD/Ax/9s1gb4CJ6K4AN8pPoCfICPnOco/AF/pHUN/AF/wB9h5gf4CJ7taf8C/oA/wAf4GOq3Un8sZv1hljISC36B1bTWANPYK0iDDEr7NRBF/1bE0nKczqnPVeDpzxp46j2n+87nj4uvJ75W7oP1IK6m8Qo+Qj4jX5F3U35pwUfcXIn5jrgirr4mrtBX3Q/kJf2JTgzfT0u+eilxVcqrOb1P3p2tz8DHfPiAz8m7fbxDnyEfH+hd9C56l77oUL8VXTKtS6zhrieiRTra9OednZ0pz/bT09PifgliFQDLy8tNx8nbTJ97d3dnDXdfRPj/33I98b9tOS6+T+6D9UjjnLgKeQB8gA/wAQ8O8St8HnQU/AF/xLOP0LvT+RN8gA/wEWanoa/QV+ir+j4U/AF/wB/wh589+hL61KNffvnli/zm4om2uoHav6tB/++//04sX2qPW1lZmVjI/PDDD5OGvv//LZ8j/3ht3Ef3gwnr4ewHHOLKOeEJfIR8Bj7Iuym/gQ/wkeoN+OPWNBX80eVLHx/wB/wBf5TrQ/ABPsAH+Bjq36Cv0FdeX6KvZvMF+FhMfIz+/PPPL/Jf13AH71kp0VT7d3mvq2Ehk/yW42RBo3///v17a5TL22ye8+tz9NqCfu3kPpx54bMexFWMX/AR8hn4WPy8Kx6JPfKGeKmEj5bPIa4WP66G4ijdT9593Lzbgk90YodPrwvIV+Srl5KvwDn1Oflq8fMVOAfn4Hzxcf5d11EamqpfU9Sszk3V9tNmS/t//PFHE/Jq8rZM8WWKbX6KrR8cy3oQV4eHh/ZjlHB5f39vP0gN4TG3nynoz3sKOnkX/gDnQX+Qr8hX4/F4iu/AB/jI8ST1R1dHgA/wAT5G9oNirg8BPsAH+AAfpfwQ99vS+kNCVF7lLf1NdAm6RLosjauRGu4aVDU0Nbe0n6nbTN1m6jZTt4fyB1O3F3PqtsQI/OHsDS398CyCPTk5mVis6cfsND/m9oMP8OHf8Ivjg7hCX6Gv0Ffoq8APQ/waD0Id+t6oa/u/V3QJugRd4pzekEO399c15N3w/aDb0e053W5DU/WLztC02dJ+plUzrZpp1UyrHsofTKuenlYtEUvede6l8UcsKj9//jwZ9v3S7gM+D3iM11GYFJ+Rr8hXeiJOPyaC85DnHgsfpbzqz0e+Il/l8JiLD/3IXauv4HPi6jHiinxFXBFXs/oJPu/qC/ol/fkh/n5a+Py5511ruOvJvKHp2KX9ApC+kOXlZbezE6Ymv3v3zvVNjfX77+7urPHkv9Ta40oTqrkP1kNxR1yFae9Mc2eaO9PcmeauJsQQv8LnnW6BP+AP4UXaONa14AN8pHUN+gp9hb5CX6GvhvUlfZ/Zvhj8AX/AH98Hf5ilzNbWlks9M2s9xFWYqijZ3t42L8Ha47yX0tnZmTX75Xk0j5exPKvPz8/tiQbuY2SNAtbjwBFXwQMXfIS8BD46j0vwAT5yMyHAB/hIvSrhD/gj1fXfgj9qvFPJV+Qr8lWY+ZHOcAIf4AN8gI+h/t634POa/t5D5qsa/ZDzdEfvoncfS+9aw/3jx4/NnrPe+06vR6jBK6DUeNWmnnl6Ml5A1Gu7Oa+wGg9cvc6vjftw9roK63Fib1wQV87eHAEfwQMWfHRehODjtXEG+Jj2mAcf4CP1KoU/4A90e9nrGnyAD/ABPmr6F+gr9BX6quwFDz7AxyLjwyxlVlZWJpYuemV2aWnJpn2rYbmxseHu7++L+/2x8rpsOc5/rjz0dM6bm5uq8+Wu5/b21pon3Mdo8h2wHsSV97EEHyGfka+6fE7eDT6vJXzorSsV0o/Bg/Ka0wYPzuoL8hX5KtWf5KvhfNWivx+KBz1Wh+oEdDt1VF8dCQ/Cg/QZyn0W8AE+wAf4iP3Mh/QeddTzq6Os4S47mNhDSK9U6ClpbWo2yKqltF+LriCQN1fLcfHnquGuqb4158tdj16H0cZ9OPvRg/Xo4pW46jz1wEfIZ+Aj5HPwAT5S3gYf4CPVe/AH/JGrB+AP+AP+KNe94AN8gA/wMdQXQ1+hr9BX5X7zIuHDLGXUoG31XveeUHoKUMFyfX1t3sBDXlHp/tXVVWvq69e7nKdszmMp9YqXsPH2ANwH66H4IK7WJzMRwEfIS+SrPZu1AT7AR85TEXyAD/RVeRYR+AAf4AN8DNWZ6Cv0FfpqZE4Jub4Q+AAfLwUf83jBoxPRiTmdaA132Y8oMfpX3PUafexLuLm5WdyvJ9PVcJcAaTlOfuv6XL1aL+DpyTrvw953Pn9cfL163Vgb9+HsTQHWg7gCH/l8Bj7Iuym/wB+B78EH+AAf6N2hegDdHvQF/AF/pPUy+AAfuX4K+gp9hb5CX6Gv6vvNi6SvzFJGT8B6gaCmtQaYxl5BGmRQ2i+hoX+rJNJynM6pz1WTXn9WI917Tvedzx8XX098rdwH60FcTeMVfIR8Rr4i76b80oKPmPxjviOuiKuviSv0VffgRUl/ohPD99OSr15KXJXyak7vk3dn6zPwMR8+4HPybh/v0GfIxwd6F72L3qUvOtRvRZdM6xJruOuJaJGONv15Z2dnyrP99PS0uF+CWAXA8vJy03HyNtPn3t3dWcPdFxH+/7dcT/xvW46L75P7YD3SOCeuQh4AH+ADfMCDQ/wKnwcdBX/AH/HsI/TudP4EH+ADfBxZ3Z2re8EH+AAf4KOUH+hfzfYpqT+oP3J97OfSvzJLGQ3JavVe9x52CnA16TWwNPWsqfF0lyG+hIU8j3KeTnt7nRdQn2eehlJo4z5G9gMG63Fgg0KJK+eEH/AR8gf46DwVwcd4MjMEfICPVF+AD/CRmykEf3w7/qjxTmU9vt161NRnrAfrkdb96Cv0FfoqeLaDj/Ksx4fkjxr9kJsxQL4iXz1WvrKGu+xc4sGKu7u7ZvWiTVYxJycnxf16rUQNXgGl5Tj/uXoyXkJOr4nWnC93PbF/HvfBeiheiaswSBh8hHxGvuryOfgAH37QeMzb4AN8pHoP/oA/0noA/oA/4A9nb2bn6l7wAT7AB/go5Ye4T4W+Ql+hr8r95kXCx+iPP/74oqfLb29vreH96tUra677J95//vlnd3l5Wdy/trZm+9+8edN0nP/cDx8+2LFXV1d2/qHz5far2a+N+3CO9ejilbgKeAYfIZ+Bjzw+9CaEcnALDzzXuNJ93NzcVPPZc72PPt6FB9ElNfGxyDwIztHt8/K557tFxkdNfoj5/rnyIDgH5/PinD7DrE4C59SDaX8PHmzrlwzVy+Qr+nCp/lLeHf32229fVldXJ0+Y6wk3bf6J86G/q7GhX/P1X8tx/nN1EfpPjX5tQ+fL7b++vrZjuQ9njSbWw1lMEVcdnsBHyGfgo8vv4KN7owp8TPM9+AAfqY6DP+CPtB6AP+CPXL0Gf8Af8Ee5fwI+wAf4AB9D/VX01WLqKxuaqifa9RSBrF30ZzW/379/bw2Jn376yZq4pf3xtOqW4/znplNsh86X2+8HrnIfzrEeXbwSVwHP4CPkM/ABPlI+a8FH/G9jviOuiKuviSv0Vb/+hM8Xm89LeXWoHiDvkne/Ju/C5+Td2r5Hi06Ez4kr4op+oiyw6Yu+drItr+knL3p/1xru8mCPJ6LH049z09OZjsx05HR6ehw/pT8TV7NxM4Q7pm4zdVtEpRxN3iXvknePTLihS96509PTQd0Gf8Af8Mcsf/r8AT7AB/gAH6m+zukL6tqgv/Vwo36A0Nv0LXU9eXdWtxFXxFVa16FLFlOX2NDUra2tiWe7vIkOD8OU2twU33j/Q04VVuIZOh9ThS9cOuWa9ZiN17OzM2tGyNefuDq3JhU4H5mVisT19vZ2U57zeYe4GlssiSeY5s4098ea5k6+WjfeIl91eoe8+/3lXeWAv//+G717cDBYn4GP7w8fNXUN/AF/pP0CdDu6/XvQ7TX6gX4i/cS+fupD932s4a7H/eOpybmp66X98sBVA0vE3nKcXrU4OTmxX0glHLynkf//LdezSFNslSRYj85bmbgCH0xzf20N7nTaPfiAP8Sf4AN85PID+qrLD+ADfICPWf2Qq7Ooo3bNitJ701N/UH/AH/AH/AF/pPX3UJ+S+pz6PFd/mKXMysqKNXS0iWAULHqyRI3wt2/fWjO8tN8fq3/Xcpz/3H/++cfOKR++mvPlruf2tpswzH2MJt8B60FceTyDj5DPyFddPifvBr57Cnzo+9cGD87qi6dYj1TvgI+nxQfrUdbfzwkf/lqG6gR0O3VUXx0JD8KD9BnKfRbwAT7AB/ho6dM+J51If/e103pYw132Cn3esH0eXUtLS9aMH/JU7duvhvunT5+sAVHjpZZej16n1MZ9OMd6BK9r4qrzOgYfwesYfIAP+KM8SwJ8gA/wAT5q6gH0FfoqrevgD/gD/oA/4I+6WUPU59Oe9vAH/LHI/GGWMmqYp55O43HwxOvzdJdHtprk19fXvV6LJW/21dVVe7L9/v5+4g3c4iGvz41f++I+WA/FD3HVeQCDj2mPMvJVN6MDfICPnAcs+AAfqf5CXwUPbfABPsBHecYX+AAf4AN8DPVhqD+oP15K/TGPFzw8CA/meNAa7npdSY05bRsbG+ZjF/v5ya+otF9PpqvhrgTbcpx8yvW5/tVh/bLl/cv7zuePi68nfj2f+2A9iKtpvIKPkM/IV+TdlF/AB/hI9Q66JOhB8AE+wMd/i/UJ+AAf4AN81PQvqD+oP6g/6CcO9SmpPxaz/jBLGT3BVOvZnnoxSmjoCXklkRqPvtSDSE16nV+N9Hk85HU98WdyH6yHZgkQV8EDFnwE7zfyVTejA3zMh4/Ue91ji7girtIZNuRd8i56tzwbKv5uSnl1aKYUeZe8+zV5Fz7vn9WGTpxPJ5b6EOQr8tXX5CviinxVO+uS+uP51R/WcNcT6lrEeTzUJYi1sMvLy67P673kzX53d2cNdx8c83jBx9fOfbAep6enjrgKeAYfwVORfNV55oEP8JHje/ABPpQfSpyBvkJfwR/go5QH4A/4A/6Yzg9xXwR8gA/wAT6GdDT1+WLW52Yps7W15U5OTqzhvru7646Pj93a2poVXZeXl25/f7+4X4GhZroGlrYc5z9XAx11Hnke1Zwvdz3n5+d27dzHyBpprMe+DQolrjo8g4+Qz8BHl8/BR+A78AE+Ur0DPsBHTg/DH9+OP/T9X11dUX/01F/UUbP1KXwOn8Pn5f4N+AAf3wM+avQD/UT6u3397YfWV9Zwl51LPBhLTTq9SqZNVjFqxpf26/UYNXhViLQc5z9XT8arMSo7mprz5a4n9s/jPlgPxStx9drwpDdHwEfIZ+SrLp+DD/Dh80PM2+ADfKR6D/6AP9J6AP6AP+CPTl/n6l7wAT7AB/go5Ye4T4W+Ql+hr8r95kXCh1nKrKysTPmgpx5Tfd7s/lh5qbcc573g5aGn5HNzc2ON99QjfsjDUftvb2+tuch9jCbfAetBXPmZCODjP5MZFeSrt/bjJnk3eAs/BT5S71jW42nXI/XGZD1Yj9xMIfhjmj987kS3U3+ofgMf6Ku0X/AU+go+L3tdsx7Ug+ADfNT0W+Hzh+Vza7jLDubo6Mia1q0e6ktLS9asbz0u9nRX01zTu+fxkNd59fq3Nu7DOdbjy2SWAHHV4Rl8BM848AE+0lkj4AN85DwV4Q/4I9W18Af8AX+EmTjgo/Okhj/KM+DQV+gr8AE+avp76Cv01SLrK7OUUcN8fX3dRMPFxYU7ODhw4/HYmth7e3vu8PCwuF/e62qeX19fNx3nP3d1ddXOe39/X3W+3PXEr+dwH6yH4pW4CngGHyGfka+6fA4+wEeO78EH+Ej1HvwBf6T1APwBf8Af5XoZfIAP8AE+avpp6Kvnra/UmNQbAfRF6/vC9Hdn++bC+ejXX381Sxk1zLVJKOhXpviVEz19XtqvAl2vKL1586bpOL2Cqs/98OGDvYYoD3j/ikPf+fxx8fV4Oxruoxs+y3oQV+Ajn8/AB3k35Rfxh7ZXr159Mx6MLWXgwWl9AZ8H/UW+Il/l8pW373hq3e4flMnp8qE6ApyD87TOpB4MuhV8gA/wEaw/njMP0r9andhC04ejD0d/t6tbZvpwv//++xdNLPbCWcNe9MR67d/V3JWwVqO+5Th/HokKXZhAqq31/Pr3miSrjftw1mxnPb5Y8ieuOjyBj5DPwEeX31N8qLGnrTbvE1fTPElc5eMKPkeXKK+AD/CR1gfz6JKUp+aNK/856MTuzWL4HD7P1e9PhY+Hwjl9htl+zjx5N9Vx8+Zd1oP1iPPMS8X5UL0MPtC7Ob1rljIfP360ZrW2q6srt7+/705OTuzvmsB+fHxc3K9Bqfpg/cLXcpz/3HSa+9D5cvvjKbbcB+uheCWuAp7BR8hn5Ksun4MP8JHje/ABPlK9B3/AH2k9AH/AH/BHuV4GH+ADfICPmn4a+gp9hb4q95sXCR82NNVPohVByGdGRXc6xVg+77n9TLF92Cm2+o5Zj9HkO9CrKS3xqH//+fNnJ8sGxbJ/LdP//1Ic5/YzzZ1p7kxzZ5r7Y0xzjy1lyFefbfC61xfkXfIuefdl5F2PVfQVut3bHCmXo9upP+Dz2X4K/RLqc+l99C79xL5+K/2rxawHreG+vb3tjo6OrOBNp87r6fV0amw8lZ2pwkwVXuSpwsIE+HBmUySR0Jof4vyhH0Bk86NtKK/k9p+dndmxrAfrQVzN8jL4OLU5MNrIV+gSdMlOUdeDD/ABPsBHTd2Pbu/6Iugr9JXXl9Qf1B/0Rcv8CT7y+DBLGTXS1tfDVHGm8TKN13s5+2nDLfHB1O3nPXVboqFmenqcB+RZpiSqIRAt+YFp1flp1WoKknedDXkmrg5tBopwSd69sDeTwMfYvgPw0eVP8BH0OfhAX43HXX5AX6GvlB9L9Rn8AX+k8QF/wB/wxzR/xvkzxYd4Vm9ctvQ9yLvk3VzetYa7Xv/766+/TMBtbm6af3scdPJNL+3XE6tqmChgW45Ts0mf618d1hM3vsjuO58/Lr6e+PV87oP1IK6m8Qo+Qj4jX5F3U34BH+Aj1TvokqAHwQf4AB/hR0D4g3pwqM6EP+CPXD+F+oP6A/6AP+CP+n7zItUfZikjMZ16yqZe16X9MrTXv1WjPvWmkvf20Ocq8HR+DW6NPY2Gjov3x9faclx8vdxH593PevxjcagfoYir4DUHPsBHms/BB/hIdQL8AX94/kRfTXu1oq+m6wH4A/6AP6a9jKlrA3/CH/BHXx8K/oA/4A/4o6Zv/Fz6V9Zw1xPqOW+qGk93NcsV9JrK3udpVPJsvru7s4a7B848HtHxtXMfrIdmDBBXnZdxim3wAT7AR/CiBB/TXnPw+TsHPsBHiSfBB/iIZ1jBH/BHru6l/qD+yM2qgj/gD/hjWl/G+RN8gI9FxodZymxtbZkdjDa97nJ8fOzW1tasCX95een29/eL+yUsJDo1yLDlOP+5GkSi88jzqOZ8ues5Pz+3a+c+RtZoZj32bcANcdXhGXyEfAY+unwOPgLfgQ/wkeod8AE+cnoY/vh2/KHv/+rqivqjp/6ijpqtT+Fz+Bw+L/dvwAf4+B7wUaMf6CfS3+3rbz+0vrKGu+xcaj3bUw91vSarBq8KkVbvdTX59WS8GqN6fWweD3ldj14X0MZ9/H/2zoCnjaP7+uP+KQ8hvCkESHmIxDeqWrXq95eQQI6B0gYeIJA0r85djWc8ntmdcSAxzm+lSkk369313HPPude75zqzhWE9iKsYz+AjeMaBj25GB3k3ePKCD/CReiqCD/CRmykEf8Af6awq+AP+gD/KnrzgA3yAD/BR099DX6GvVllfmaXMxsbGjF+1BpheXl5aI3xnZ8c9PDwU9/tj5Xndcpz/XHnWqTl4e3tbdb7c9dzd3VnDnfsYTb8D1oO48t5W4CPkM/JVl8/Ju8H7roQPvXWlQuEpeFBD1bTBg/P6gnxFvkr1J/kyg0dwAAAgAElEQVRqOF+16O/H4kGP1aE6Ad1OHdVXR8KD8CB9hnKfBXyAD/ABPmLP+iG9Rx21fHWUNdxlB3NycmINAHmo65Wjf//91xoN+k9WLaX9WnQFQetx8eeq4a5kUnO+3PXo9W9t3MfIfvRgPbp4Ja46PIOPkM/AR8jn4OPb4UNvIWmDB+f1BfmKfJXTn+Srb5evSuvx8ePHuTxWW0eAc3AOzst1NvgAH+ADfNT0xahrqWvTPi38sXz8YZYyatBub29b8a8ni46Ojtx4PDYhfXBw4I6Pj4v79RSgmgc3NzdNx/nP3dzctPOq4V5zvtz1xPYZ3AfroXglrgKewUfIZ+SrLp+DD/CR43vwAT5SvQd/wB9pPQB/wB/wR7leBh/gA3yAj5p+GvpqufWV+pJ6I5m+aH1fmP7ufN9cOLeGu+xHUo+tWk93PeGihrsa3an3To2nuwJZxKRf6HKema9fd55Gfdej1421cR/OsR7/s1kCxFXwjAMfIX+AD/CR8hL4AB+pvoA/4I+c5yj8AX/AH8GTOq3PwAf4AB/gY6ifhL5CX6GvnCv1N8HHauLDLGVUbPoFVtNaA0xjryANMijt10AU/VsFTstxOqc+V4lZf9bAU+853Xc+f1x8PfG1ch+sB3E1i1fwEfIZ+Yq8m/JLCz7i5nzMd8QVcfUlcYW+6h68KOlPdGL4flry1XOJq1Jezel98u58fQY+FsMHfE7e7eMd+gz5+EDvonfRu/RFh/qt6JJZXWINdz2hLtLRpj/v7+/PeLafnZ0V90sQqwBYX19vOk7e0vrc+/t7a7j7IsL//5brif9ty3HxfXIfrEca58RVyAPgA3yAD3hwiF/h86Cj4A/4I559hN6dzZ/gA3yAjzA7DX2FvkJf1feh4A/4A/6AP/zMoOfQpx798ssvn+U3F0+01Q3U/l0Neg328K9k1x63sbExtZD54Ycfpg19//9bPkf+8dq4j+4HE9bD2Q84xJVzwhP4CPkMfJB3U34DH+Aj1Rvwx51pKvijy5c+PuAP+AP+KNeH4AN8gA/wMdS/QV+hr7y+RF/N5wvwsZr4GP3555+f5b+u4Q7eU0miqfbv8l5Xw0Im+S3HyYJG//7du3fWKJf33yLn1+fotQX92sl9OPPCZz2Iqxi/4CPkM/Cx+nlXPBJ7SA7xUgkfLZ9DXK1+XA3FUbqfvPu0ebcFn+jEDp9eF5CvyFfPJV+Bc+pz8tXq5ytwDs7B+erj/LuuozQ0Vb+mqFmdm6rtp82W9v/4448m5NXkbZniyxTb/BRbPziW9SCujo+P7cco4fLh4cF+kBrCY24/U9CXewo6eRf+AOdBf5CvyFfj8XiG78AH+MjxJPVHV0eAD/ABPkb2g2KuDwE+wAf4AB+l/BD329L6Q0JUXuUt/U10CbpEuiyNq5Ea7hpUNTRVurSfqfRMpWcqPVPph/IHU7dXc+q2xAj84ewNLf3wLIKdTCZTizX9mJ3mx9x+8AE+/Bt+cXwQV+gr9BX6Cn0V+GGIX+NBqEPfG3Vt//eKLkGXoEuc0xty6Pb+uoa8G74fdDu6PafbbWiqftEZmjZb2s+0aqZVM62aadVD+YNp1bPTqiViybvOPTf+iEXlp0+fpsO+n9t9wOcBj/E6CpPiM/IV+UpPxOnHRHAe8txT4aOUV/35yFfkqxwec/GhH7lr9RV8Tlw9RVyRr4gr4mpeP8HnXX1Bv6Q/P8TfTwufL3vetYa7nswbmo5d2i8A6QtZX193+/thavLbt29d39RYv//+/t4aT/5LrT2uNKGa+2A9FHfEVZj2zjR3prkzzZ1p7mpCDPErfN7pFvgD/hBepI1jXQs+wEda16Cv0FfoK/QV+mpYX9L3me+LwR/wB/zxffCHWcrs7u661DOz1kNchamKkr29PfMSrD3Oeymdn59bs1+eR4t4Gcuz+uLiwp5o4D5G1ihgPY4ccRU8cMFHyEvgo/O4BB/gIzcTAnyAj9SrEv6AP1Jd/zX4o8Y7lXxFviJfhZkf6Qwn8AE+wAf4GOrvfQ0+r+nvPWa+qtEPOU939C5696n0rjXcP3z40Ow5673v9HqEGrwCSo1XbeqZpyfjBUS9tpvzCqvxwNXr/Nq4D2evq7AeE3vjgrhy9uYI+AgesOCj8yIEHy+NM8DHrMc8+AAfqVcp/AF/oNvLXtfgA3yAD/BR079AX6Gv0FdlL3jwAT5WGR9mKbOxsTG1dNErs2trazbtWw3LnZ0d9/DwUNzvj5XXZctx/nPloadz3t7eVp0vdz13d3fWPOE+RtPvgPUgrryPJfgI+Yx81eVz8m7weS3hQ29dqZB+Ch6U15w2eHBeX5CvyFep/iRfDeerFv39WDzosTpUJ6DbqaP66kh4EB6kz1Dus4AP8AE+wEfsZz6k96ijlq+Osoa77GBiDyG9UqGnpLWp2SCrltJ+LbqCQN5cLcfFn6uGu6b61pwvdz16HUYb9+HsRw/Wo4tX4qrz1AMfIZ+Bj5DPwQf4SHkbfICPVO/BH/BHrh6AP+AP+KNc94IP8AE+wMdQXwx9hb5CX5X7zauED7OUUYO21Xvde0LpKUAFy83NjXkDD3lFpfs3Nzetqa9f73KesjmPpdQrXsLG2wNwH6yH4oO42p7ORAAfIS+Rrw5s1gb4AB85T0XwAT7QV+VZROADfIAP8DFUZ6Kv0Ffoq5E5JeT6QuADfDwXfCziBY9ORCfmdKI13GU/osToX3HXa/SxL+Hr16+L+/VkuhruEiAtx8lvXZ+rV+sFPD1Z533Y+87nj4uvV68ba+M+nL0pwHoQV+Ajn8/AB3k35Rf4I/A9+AAf4AO9O1QPoNuDvoA/4I+0XgYf4CPXT0Ffoa/QV+gr9FV9v3mV9JVZyugJWC8Q1LTWANPYK0iDDEr7JTT0b5VEWo7TOfW5atLrz2qke8/pvvP54+Lria+V+2A9iKtZvIKPkM/IV+TdlF9a8BGTf8x3xBVx9SVxhb7qHrwo6U90Yvh+WvLVc4mrUl7N6X3y7nx9Bj4Wwwd8Tt7t4x36DPn4QO+id9G79EWH+q3oklldYg332D9If97f35/xbD87O5v6q6f7JYhVAKyvrzcdJ28zfe79/b013H0R4f+/93OvuR4Rpjbuw02/S9aDuBIuhAnwEfIZ+Yq8m/JZCz7ifxvzJHFFXH1JXJX0DnFFXH0PcVXKq0P1APgAH1+CD/i8m9NWqrOpz8P306IT4XPiqg9XcXwQV9Tn8YxM+qKz/eZVwodZymhIVqv3uvewEyGLrDWwNPWsqfF0lyG+vlB5HuU8nQ4OOi+gPs88DaXQxn2M7AcM1uPIBoUSV84JP+Aj5A/w0Xkqgo/xdGYI+AAfqb4AH+AjN1MI/vh6/FHjncp6fL31qKnPWA/WI6370VfoK/RV8GwHH+VZj4/JHzX6ITdjgHxFvnqqfGUNd9m5xIMV37x5Y1Yv2mQVM5lMivv1WokavAJKy3H+c/UktoScXhOtOV/uemL/PO6D9VC8EldhkDD4CPmMfNXlc/ABPvyg8Zi3wQf4SPUe/AF/pPUA/AF/wB/O3szO1b3gA3yAD/BRyg9xnwp9hb5CX5X7zauEj9Eff/zxWU+X393dWcP7xYsX1lz3T7z/97//dVdXV8X9W1tbtv/Vq1dNx/nPff/+vR17fX1t5x86X26/mv3auA/nWI8uXomrgGfwEfIZ+MjjQ29CKAe38MCyxpXu4/b2tprPlvU++ngXHkSX1MTHKvMgOEe3L8rnnu9WGR81+SHm+2XlQXAOzhfFOX2GeZ0EzqkH0/4ePNjWLxmql8lX9OFS/aW8O/rtt98+b25uTp8w1xNu2vwT50N/V2NDv+brv5bj/OfqIvSfGv3ahs6X239zc2PHch/OGk2sh7OYIq46PIGPkM/AR5ffwUf3RhX4mOV78AE+Uh0Hf8AfaT0Af8AfuXoN/oA/4I9y/wR8gA/wAT6G+qvoq9XUVzY0VU+06ykCWbvoz2p+v3v3zhoSP//8szVxS/vjadUtx/nPTafYDp0vt98PXOU+nGM9unglrgKewUfIZ+ADfKR81oKP+N/GfEdcEVdfElfoq379CZ+vNp+X8upQPUDeJe9+Sd6Fz8m7tX2PFp0InxNXxBX9RFlg0xd96WRbXtNPXvX+rjXc5cFemtb+9u1bp6m5i05zH/rc0hT0oePi/aU/7++H6cfcR/86+u+H9RhZYkgx0RKPcdzpRyAJNb110BKPrMd8vILz+mnuQ/EKzsG5z3Pkq3mdAD7AB/jodFBOt4AP8AE+wEcpP1Cfh/yAvkJfnZycWF8h14eirqWuTeMDfbWa+sqGpu7u7k492+VNdHwcptTmpvjG+x9zqrASz9D5mCp86dIp16zHfLyen5/bj0Ty9SeuLozswfnIrFRUJOzt7TXlOZ93iKuxxZJ4gmnuTHN/qmnu5Ktt4y3yVad3yLvfX95VDvj777/Ru0dHg/UZ+Pj+8FFT18Af8EfaL0C3o9u/B91eox/oJ9JP7OunPnbfxxruetw/npqcm7pe2i8PXDWwROwtx+lVi8lkYk/+Sjh4TyP//1uuZ5Wm2CpJsB6dtzJxBT6Y5v7SGtzptHvwAX+IP8EH+MjlB/RVlx/AB/gAH/P6IVdnUUe9MStK701P/UH9AX/AH/AH/JHW30N9Supz6vNc/WGWMhsbG9bQ0SaCUbDoyRI1wn/66Sdrhpf2+2P171qO85/7zz//2Dnlw1dzvtz13N11E4a5j9H0O2A9iCuPZ/AR8hn5qsvn5N3Ad98CH/r+tcGD8/riW6xHqnfAx7fFB+tR1t/LhA9/LUN1ArqdOqqvjoQH4UH6DOU+C/gAH+ADfLT0aZdJJ9Lffem0HtZwl71Cn8dUn/f02tqaNeOHPNL79qvh/vHjx6l3dqvXtV6n1MZ9OMd6BM9R4qrzjAMfYQYF+AAfKb+AD/CRm3kAf8AfqW6FP+AP+KM8Gwt8gA/wAT5q+knoK/QV+qo8WxF8rB4+zFJGDfPU02k8Dp54fZ7u8sjWq3c3Nze9Xoslb/bNzU17sv3h4WHqDdziIa/PjV/74j5YD8UPcdV5AIOPWY8y8lU3owN8gI+cByz4AB+p/kJfBQ9t8AE+wEd5xhf4AB/gA3wM9WGoP6g/nkv9sYgXPDwID+Z40Bruel1JjTltOzs75mMX+/nJr6i0X0+mq+GuBNtynHzK9bn+1WE9GeH9y/vO54+Lryd+PZ/7YD2Iq1m8go+Qz8hX5N2UX8AH+Ej1Drok6EHwAT7Ax3+K9Qn4AB/gA3zU9C+oP6g/qD/oJw71Kak/VrP+MEsZPcFU69meejFKaOgJeSWRGo++1INITXqdX430RTzkdT3xZ3IfrIdmCRBXwQMWfATvN/JVN6MDfCyGj9R73WOLuCKu0hk25F3yLnq3PBsq/m5KeXVophR5l7z7JXkXPu+f1YZOXEwnlvoQ5Cvy1ZfkK+KKfFU765L6Y/nqD2u46wn1nIepGuBD3uwSxFrY9fV11+q9fnZ25u7v763h7oNj6Hy5/fG1cx+sB3EVPJlTbIMP8AE+wEcpD8DnnaciumQ0namDvgqevOADfCg/lDCBvkJfwR/gA30V9EOuL4S+Ql9JYKo/EccH+gp9tcr6yixldnd33WQysQLrzZs37vT01G1tbZmovLq6coeHh8X9SpwCjQaWthznP1cD63QeeR7VnC93PRcXF3bt3MfIGgWsx6ENCiWuOjyDj5DPwEeXz8FH4DvwAT5SvQM+wEdOD8MfX48/9P1fX19Tf/TUX9RR8/UpfA6fw+fl/g34AB/fAz5q9AP9RPq7ff3tx9ZX1nCXnUs8GEtNOr1Kpk1WMWrGl/br9Rg1eFWItBznP1dPxqsxKjuamvPlrif2z+M+WA/FK3H10vCkN0fAR8hn5Ksun4MP8OHzQ8zb4AN8pHoP/oA/0noA/oA/4I9OX+fqXvABPsAH+Cjlh7hPhb5CX6Gvyv3mVcKHWcpsbGzM+KCnHlN93uz+WHmptxznveDloafkc3t7a4331CN+yMNR++/u7qy5yH2Mpt8B60Fc+ZkI4OP/pjMqyFc/2Y+b5N3gLfwt8JF6x7Ie33Y9Um9M1oP1yM0Ugj9m+cPnTnQ79YfqN/CBvkr7Bd9CX8HnZa9r1oN6EHyAj5p+K3z+uHxuDXfZwZycnFjTutVDfW1tzZr1rcfF3k1qmmt6d87TqeZz9fq3Nu7DOdYjeIIRVx2ewUfwlAQf4CP1lAQf4CPnuQp/wB+p/oQ/4A/4I8w0AB+d5y78UfbsRl+hr8AH+Kjp76Gv0FerrK/MUkYN8+3tbRMNl5eX7ujoyI3HY2tiHxwcuOPj4+J+ea+reX5zc9N0nP/czc1NO+/Dw0PV+XLXE7+ew32wHopX4irgGXyEfEa+6vI5+AAfOb4HH+Aj1XvwB/yR1gPwB/wBf5TrZfABPsAH+Kjpp6GvlltfqTGpNwLoi9b3henvzvfNhfPRr7/+apYyaphrk1DQr0zxKyd6+ry0XwW6XlF69epV03F6BVWf+/79e3sNUR7w/hWHvvP54+Lr8XY03Ec3fJb1IK7ARz6fgQ/ybsov4g9tL168+Go8GFvKwIOz+gI+D/qLfEW+yuUrb9/xrXW7f1Amp8uH6ghwDs7TOpN6MOhW8AE+wEew/lhmHqR/tTm1haYPRx+O/m5Xt8z14X7//ffPmljshbOGveiJ9dq/q7krYa1Gfctx/jwSFbowgVRb6/n17zVJVhv34azZznp8tuRPXHV4Ah8hn4GPLr+n+FBjT1tt3ieuZnmSuMrHFXyOLlFeAR/gI60PFtElKU8tGlf+c9CJ3ZvF8Dl8nqvfvxU+Hgvn9Bnm+zmL5N1Uxy2ad1kP1iPOM88V50P1MvhA7+b0rlnKfPjwwZrV2q6vr93h4aGbTCb2d01gPz09Le7XoFR9sH7haznOf246zX3ofLn98RRb7oP1ULwSVwHP4CPkM/JVl8/BB/jI8T34AB+p3oM/4I+0HoA/4A/4o1wvgw/wAT7AR00/DX2FvkJflfvNq4QPG5rqJ9GKIOQzo6I7nWIsn/fcfqbYPu4UW33HrMdo+h3o1ZSWeNS///Tpk5Nlg2LZv5bp/38pjnP7mebONHemuTPN/SmmuceWMuSrTzZ43esL8i55l7z7PPKuxyr6Ct3ubY6Uy9Ht1B/w+Xw/hX4J9bn0PnqXfmJfv5X+1WrWg9Zw39vbcycnJ1bwplPn9fR6OjU2nsrOVGGmCq/yVGFhAnw4symSSGjND3H+0A8gsvnRNpRXcvvPz8/tWNaD9SCu5nkZfJzZHBht5Ct0Cbpkv6jrwQf4AB/go6buR7d3fRH0FfrK60vqD+oP+qJl/gQfeXyYpYwaadvbYao403iZxuu9nP204Zb4YOr2ck/dlmiomZ4e5wF5limJaghES35gWnV+WrWaguRdZ0Oeiatjm4EiXJJ3L+3NJPAxtu8AfHT5E3wEfQ4+0FfjcZcf0FfoK+XHUn0Gf8AfaXzAH/AH/DHLn3H+TPEhntUbly19D/IueTeXd63hrtf//vrrLxNwr1+/Nv/2OOjkm17arydW1TBRwLYcp2aTPte/OqwnbnyR3Xc+f1x8PfHr+dwH60FczeIVfIR8Rr4i76b8Aj7AR6p30CVBD4IP8AE+wo+A8Af14FCdCX/AH7l+CvUH9Qf8AX/AH/X95lWqP8xSRmK61rM99WqUob2a4GrUp95UNd7bCjydX4NbF/GQ1/XEfljcB+uhuCOuggcs+AieeeSrbkYH+FgMH6n3uscWcUVcpXqHvEveTWfGkHfzebeUV4dmSpF3ybtfknfh8/KsCHTi7Cw7+Bw+h8/z3uv04brZBNSDs/GB3p3Vu9Zw1xPqOW+qGk93CWIFmaay93kalTyb7+/vreHuA3URj+j42rkP1kMzBoirzss4xTb4AB/gI3hRgo9Zrzn4/K0DH+CjxJPgA3zEM6zgD/gjV/dSf1B/5GZVwR/wB/wxqy/j/Ak+wMcq48MsZXZ3d80ORptedzk9PXVbW1vWhL+6unKHh4fF/RIWEp0aZNhynP9cDSLReeR5VHO+3PVcXFzYtXMfI2s0sx6HNuCGuOrwDD5CPgMfXT4HH4HvwAf4SPUO+AAfOT0Mf3w9/tD3f319Tf3RU39RR83Xp/A5fA6fl/s34AN8fA/4qNEP9BPp7/b1tx9bX1nDXXYutZ7tqYe6XjtTg1eFSKv3upr8ejJejVG9jrGIh7yuR6+VauM+nNlFsB7EVYxn8BE848BHN6ODvBs8ecEH+Eg9FcEH+MjNFII/4I90VhX8AX/AH2VPXvABPsAH+Kjp76Gv0FerrK/MUmZjY2PGB10DTC8vL60RvrOz4x4eHor7/bHycGo5zn+uPPTUHLy9va06X+567u7urOHOfYym3wHrQVz5mQjgI+Qz8lWXz8m7wWuuhA+9daVC4Sl4ULNGtMGD8/qCfEW+SvUn+Wo4X7Xo78fiQY/VoToB3U4d1VdHwoPwIH2Gcp8FfIAP8AE+4hkCQ3qPOmr56ihruMsO5uTkxBoA8lDXK0d6Slqbmg2yaint16IrCFqPiz9XDXdN7645X+569Pq3Nu7D2Y8erEcXr8RVh2fwEfIZ+Aj5HHyAj5S3wQf4SPUe/AF/5OoB+AP+gD/K9TL4AB/gA3wM9dPQV+gr9FW537xK+DBLGTVot7e3rbmuJ4uOjo7ceDy2JvbBwYE7Pj4u7tdTgAqWm5ubpuP8525ubtp59etdzfly1xPbZ3AfrIfilbgKeAYfIZ+Rr7p8Dj7AR47vwQf4SPUe/AF/pPUA/AF/wB/lehl8gA/wAT5q+mnoq+XWV+pL6o1k+qL1fWH6u/N9c+HcGu6yH0k9tmo93fVkuhruanSn3js1nu4KZBGTnqzLeWa+ft15GvVdj1431sZ9OHtTgPV4YwmSuHJO+AEfIX+Aj//ZrA3wETwVwQf4SPUF+AAfOc9R+AP+SOsa+AP+gD/CzA/wETzb0/4F/AF/gA/wMdRvpf5YzfrDLGUkFvwCq2mtAaaxV5AGGZT2ayCK/q2IpeU4nVOfq8DTnzXw1HtO953PHxdfT3yt3AfrQVzN4hV8hHxGviLvpvzSgo+4uRLzHXFFXH1JXKGvuh/IS/oTnRi+n5Z89VziqpRXc3qfvDtfn4GPxfABn5N3+3iHPkM+PtC76F30Ln3RoX4rumRWl1jDXU9Ei3S06c/7+/sznu1nZ2fF/RLEKgDW19ebjpO3mT73/v7eGu6+iPD/v+V64n/bclx8n9wH65HGOXEV8gD4AB/gAx4c4lf4POgo+AP+iGcfoXdn8yf4AB/gI8xOQ1+hr9BX9X0o+AP+gD/gDz979Dn0qUe//PLLZ/nNxRNtdQO1f1eD/t9//51avtQet7GxMbWQ+eGHH6YNff//Wz5H/vHauI/uBxPWw9kPOMSVc8IT+Aj5DHyQd1N+Ax/gI9Ub8MedaSr4o8uXPj7gD/gD/ijXh+ADfIAP8DHUv0Ffoa+8vkRfzecL8LGa+Bj9+eefn+W/ruEO3rNSoqn27/JeV8NCJvktx8mCRv/+3bt31iiXt9ki59fn6LUF/drJfTjzwmc9iKsYv+Aj5DPwsfp5VzwSe+QN8VIJHy2fQ1ytflwNxVG6n7z7tHm3BZ/oxA6fXheQr8hXzyVfgXPqc/LV6ucrcA7Owfnq4/y7rqM0NFW/pqhZnZuq7afNlvb/+OOPJuTV5G2Z4ssU2/wUWz84lvUgro6Pj+3HKOHy4eHBfpAawmNuP1PQl3sKOnkX/gDnQX+Qr8hX4/F4hu/AB/jI8ST1R1dHgA/wAT5G9oNirg8BPsAH+AAfpfwQ99vS+kNCVF7lLf1NdAm6RLosjauRGu4aVDU0Nbe0n6nbTN1m6jZTt4fyB1O3V3PqtsQI/OHsDS398CyCnUwmU4s1/Zid5sfcfvABPvwbfnF8EFfoK/QV+gp9FfhhiF/jQahD3xt1bf/3ii5Bl6BLnNMbcuj2/rqGvBu+H3Q7uj2n221oqn7RGZo2W9rPtGqmVTOtmmnVQ/mDadWz06olYsm7zj03/ohF5adPn6bDvp/bfcDnAY/xOgqT4jPyFflKT8Tpx0RwHvLcU+GjlFf9+chX5KscHnPxoR+5a/UVfE5cPUVcka+IK+JqXj/B5119Qb+kPz/E308Lny973rWGu57MG5qOXdovAOkLWV9fd/v7YWry27dvXd/UWL///v7eGk/+S609rjShmvtgPRR3xFWY9s40d6a5M82dae5qQgzxK3ze6Rb4A/4QXqSNY10LPsBHWtegr9BX6Cv0FfpqWF/S95nvi8Ef8Af88X3wh1nK7O7uutQzs9ZDXIWpipK9vT3zEqw9znspnZ+fW7NfnkeLeBnLs/ri4sKeaOA+RtYoYD2OHHEVPHDBR8hL4KPzuAQf4CM3EwJ8gI/UqxL+gD9SXf81+KPGO5V8Rb4iX4WZH+kMJ/ABPsAH+Bjq730NPq/p7z1mvqrRDzlPd/Quevep9K413D98+NDsOeu97/R6hBq8AkqNV23qmacn4wVEvbab8wqr8cDV6/zauA9nr6uwHhN744K4cvbmCPgIHrDgo/MiBB8vjTPAx6zHPPgAH6lXKfwBf6Dby17X4AN8gA/wUdO/QF+hr9BXZS948AE+VhkfZimzsbExtXTRK7Nra2s27VsNy52dHffw8FDc74+V12XLcf5z5aGnc97e3ladL3c9d3d31jzhPkbT74D1IK68jyX4CPmMfNXlc/Ju8Hkt4UNvXamQfgoelNecNnhwXl+Qr8hXqf4kXw3nqxb9/Vg86LE6VCeg26mj+upIeBAepM9Q7rOAD/ABPsBH7Gc+pPeoo5avjrKGu+xgYg8hvVKhp6S1qdkgq5bSfi26gkDeXC3HxZ+rhrum+tacL3c9eh1GG/fh7EcP1qOLV+Kq89QDHyGfgY+Qz8EH+Eh5G3yAj1TvwR/wR64egD/gD/ijXPeCD/ABPpEIyDgAACAASURBVMDHUF8MfYW+Ql+V+82rhA+zlFGDttV73XtC6SlABcvNzY15Aw95RaX7Nzc3ramvX+9ynrI5j6XUK17CxtsDcB+sh+KDuNqezkQAHyEvka8ObNYG+AAfOU9F8AE+0FflWUTgA3yAD/AxVGeir9BX6KuROSXk+kLgA3w8F3ws4gWPTkQn5nSiNdxlP6LE6F9x12v0sS/h69evi/v1ZLoa7hIgLcfJb12fq1frBTw9Wed92PvO54+Lr1evG2vjPpy9KcB6EFfgI5/PwAd5N+UX+CPwPfgAH+ADvTtUD6Dbg76AP+CPtF4GH+Aj109BX6Gv0FfoK/RVfb95lfSVWcroCVgvENS01gDT2CtIgwxK+yU09G+VRFqO0zn1uWrS689qpHvP6b7z+ePi64mvlftgPYirWbyCj5DPyFfk3ZRfWvARk3/Md8QVcfUlcYW+6h68KOlPdGL4flry1XOJq1Jezel98u58fQY+FsMHfE7e7eMd+gz5+EDvonfRu/RFh/qt6JJZXWINdz0RLdLRpj/v7+/PeLafnZ0V90sQqwBYX19vOk7eZvrc+/t7a7j7IsL//5brif9ty3HxfXIfrEca58RVyAPgA3yAD3hwiF/h86Cj4A/4I559hN6dzZ/gA3yAjxOru3N1L/gAH+ADfJTyA/2r+T4l9Qf1R66PvSz9K7OU0ZCsVu9172GnAFeTXgNLU8+aGk93GeJLWMjzKOfpdHDQeQH1eeZpKIU27mNkP2CwHkc2KJS4ck74AR8hf4CPzlMRfIynM0PAB/hI9QX4AB+5mULwx9fjjxrvVNbj661HTX3GerAead2PvkJfoa+CZzv4KM96fEz+qNEPuRkD5Cvy1VPlK2u4y84lHqz45s0bs3rRJquYyWRS3K/XStTgFVBajvOfqyfjJeT0mmjN+XLXE/vncR+sh+KVuAqDhMFHyGfkqy6fgw/w4QeNx7wNPsBHqvfgD/gjrQfgD/gD/nD2Znau7gUf4AN8gI9Sfoj7VOgr9BX6qtxvXiV8jP7444/Perr87u7OGt4vXryw5rp/4v2///2vu7q6Ku7f2tqy/a9evWo6zn/u+/fv7djr62s7/9D5cvvV7NfGfTjHenTxSlwFPIOPkM/ARx4fehNCObiFB5Y1rnQft7e31Xy2rPfRx7vwILqkJj5WmQfBObp9UT73fLfK+KjJDzHfLysPgnNwvijO6TPM6yRwTj2Y9vfgwbZ+yVC9TL6iD5fqL+Xd0W+//fZ5c3Nz+oS5nnDT5p84H/q7Ghv6NV//tRznP1cXof/U6Nc2dL7c/pubGzuW+3DWaGI9nMUUcdXhCXyEfAY+uvwOPro3qsDHLN+DD/CR6jj4A/5I6wH4A/7I1WvwB/wBf5T7J+ADfIAP8DHUX0Vfraa+sqGpeqJdTxHI2kV/VvP73bt31pD4+eefrYlb2h9Pq245zn9uOsV26Hy5/X7gKvfhHOvRxStxFfAMPkI+Ax/gI+WzFnzE/zbmO+KKuPqSuEJf9etP+Hy1+byUV4fqAfIuefdL8i58Tt6t7Xu06ET4nLgirugnygKbvuhLJ9vymn7yqvd3reEuD/Z4Ino8/Tg3PZ3pyExHTqenx/FT+jNxNR83Q7hj6jZTt0VUytHkXfIueffEhBu65K07Ozsb1G3wB/wBf8zzp88f4AN8gA/wkerrnL6grg36Ww836gcIvU3fUteTd+d1G3FFXKV1HbpkNXWJDU3d3d2derbLm+j4OEypzU3xjfc/5lRhJZ6h8zFV+NKlU65Zj/l4PT8/t2aEfP2JqwtrUoHzkVmpSFzv7e015Tmfd4irscWSeIJp7kxzf6pp7uSrbeMt8lWnd8i731/eVQ74+++/0btHR4P1Gfj4/vBRU9fAH/BH2i9At6PbvwfdXqMf6CfST+zrpz5238ca7nrcP56anJu6XtovD1w1sETsLcfpVYvJZGK/kEo4eE8j//9brmeVptgqSbAenbcycQU+mOb+0hrc6bR78AF/iD/BB/jI5Qf0VZcfwAf4AB/z+iFXZ1FHvTErSu9NT/1B/QF/wB/wB/yR1t9DfUrqc+rzXP1hljIbGxvW0NEmglGw6MkSNcJ/+ukna4aX9vtj9e9ajvOf+88//9g55cNXc77c9dzddROGuY/R9DtgPYgrj2fwEfIZ+arL5+TdwHffAh/6/rXBg/P64lusR6p3wMe3xQfrUdbfy4QPfy1DdQK6nTqqr46EB+FB+gzlPgv4AB/gA3y09GmXSSfS333ptB7WcJe9Qp83bJ9H19ramjXjhzxV+/ar4f7x40drQNR4qaXXo9cptXEfzrEeweuauOq8jsFH8DoGH+AD/ijPkgAf4AN8gI+aegB9hb5K6zr4A/6AP+AP+KNu1hD1+aynPfwBf6wyf5iljBrmqafTeBw88fo83eWRrSb5zc1Nr9diyZt9c3PTnmx/eHiYegO3eMjrc+PXvrgP1kPxQ1x1HsDgY9ajjHzVzegAH+Aj5wELPsBHqr/QV8FDG3yAD/BRnvEFPsAH+AAfQ30Y6g/qj+dSfyziBQ8PwoM5HrSGu15XUmNO287OjvnYxX5+8isq7deT6Wq4K8G2HCefcn2uf3VYv2x5//K+8/nj4uuJX8/nPlgP4moWr+Aj5DPyFXk35RfwAT5SvYMuCXoQfIAP8PGfYn0CPsAH+AAfNf0L6g/qD+oP+olDfUrqj9WsP8xSRk8w1Xq2p16MEhp6Ql5JpMajL/UgUpNe51cjfREPeV1P/JncB+uhWQLEVfCABR/B+4181c3oAB+L4SP1XvfYIq6Iq3SGDXmXvIveLc+Gir+bUl4dmilF3iXvfknehc/7Z7WhExfTiaU+BPmKfPUl+Yq4Il/Vzrqk/li++sMa7npCXYu4iIe6BLEWdn193fV5vZe82e/v763h7oNjES/4+Nq5D9bj7OzMEVcBz+AjeCqSrzrPPPABPnJ8Dz7Ah/JDiTPQV+gr+AN8lPIA/AF/wB+z+SHui4AP8AE+wMeQjqY+X8363Cxldnd33WQysYb7mzdv3Onpqdva2rKi6+rqyh0eHhb3KzDUTNfA0pbj/OdqoKPOI8+jmvPlrufi4sKunfsYWSON9Ti0QaHEVYdn8BHyGfjo8jn4CHwHPsBHqnfAB/jI6WH44+vxh77/6+tr6o+e+os6ar4+hc/hc/i83L8BH+Dje8BHjX6gn0h/t6+//dj6yhrusnOJB2OpSadXybTJKkbN+NJ+vR6jBq8KkZbj/OfqyXg1RmVHU3O+3PXE/nncB+uheCWuXhqe9OYI+Aj5jHzV5XPwAT58foh5G3yAj1TvwR/wR1oPwB/wB/zR6etc3Qs+wAf4AB+l/BD3qdBX6Cv0VbnfvEr4MEuZjY2NGR/01GOqz5vdHysv9ZbjvBe8PPSUfG5vb63xnnrED3k4av/d3Z01F7mP0fQ7YD2IKz8TAXz833RGBfnqJ/txk7wbvIW/BT5S71jW49uuR+qNyXqwHrmZQvDHLH/43Ilup/5Q/QY+0Fdpv+Bb6Cv4vOx1zXpQD4IP8FHTb4XPH5fPreEuO5iTkxNrWrd6qK+trVmzvvW42NNdTXNN717EQ17n1evf2rgP51iPz9NZAsRVh2fwETzjwAf4SGeNgA/wkfNUhD/gj1TXwh/wB/wRZuKAj86TGv4oz4BDX6GvwAf4qOnvoa/QV6usr8xSRg3z7e1tEw2Xl5fu6OjIjcdja2IfHBy44+Pj4n55r6t5fnNz03Sc/9zNzU0778PDQ9X5ctcTv57DfbAeilfiKuAZfIR8Rr7q8jn4AB85vgcf4CPVe/AH/JHWA/AH/AF/lOtl8AE+wAf4qOmnoa+WW1+pMak3AuiL1veF6e/O982F89Gvv/5qljJqmGuTUNCvTPErJ3r6vLRfBbpeUXr16lXTcXoFVZ/7/v17ew1RHvD+FYe+8/nj4uvxdjTcRzd8lvUgrsBHPp+BD/Juyi/iD20vXrz4ajwYW8rAg7P6Aj4P+ot8Rb7K5Stv3/Gtdbt/UCany4fqCHAOztM6k3ow6FbwAT7AR7D+WGYepH+1ObWFpg9HH47+ble3zPXhfv/998+aWOyFs4a96In12r+ruSthrUZ9y3H+PBIVujCBVFvr+fXvNUlWG/fhrNnOeny25E9cdXgCHyGfgY8uv6f4UGNPW23eJ65meZK4yscVfI4uUV4BH+AjrQ8W0SUpTy0aV/5z0Indm8XwOXyeq9+/FT4eC+f0Geb7OYvk3VTHLZp3WQ/WI84zzxXnQ/Uy+EDv5vSuWcp8+PDBmtXarq+v3eHhoZtMJvZ3TWA/PT0t7tegVH2wfuFrOc5/bjrNfeh8uf3xFFvug/VQvBJXAc/gI+Qz8lWXz8EH+MjxPfgAH6negz/gj7QegD/gD/ijXC+DD/ABPsBHTT8NfYW+Ql+V+82rhA8bmuon0Yog5DOjojudYiyf99x+ptg+7hRbfcesx2j6HejVlJZ41L//9OmTk2WDYtm/lun/fymOc/uZ5s40d6a5M839Kaa5x5Yy5KtPNnjd6wvyLnmXvPs88q7HKvoK3e5tjpTL0e3UH/D5fD+Ffgn1ufQ+epd+Yl+/lf7VataD1nDf29tzJycnVvCmU+f19Ho6NTaeys5UYaYKr/JUYWECfDizKZJIaM0Pcf7QDyCy+dE2lFdy+8/Pz+1Y1oP1IK7meRl8nNkcGG3kK3QJumS/qOvBB/gAH+Cjpu5Ht3d9EfQV+srrS+oP6g/6omX+BB95fJiljBpp29thqjjTeJnG672c/bThlvhg6vZyT92WaKiZnh7nAXmWKYlqCERLfmBadX5atZqC5F1nQ56Jq2ObgSJckncv7c0k8DG27wB8dPkTfAR9Dj7QV+Nxlx/QV+gr5cdSfQZ/wB9pfMAf8Af8Mcufcf5M8SGe1RuXLX0P8i55N5d3reGu1//++usvE3CvX782//Y46OSbXtqvJ1bVMFHAthynZpM+1786rCdufJHddz5/XHw98ev53AfrQVzN4hV8hHxGviLvpvwCPsBHqnfQJUEPgg/wAT7Cj4DwB/XgUJ0Jf8AfuX4K9Qf1B/wBf8Af9f3mVao/zFJGYrrWsz31apShvZrgatSn3lQ13tsKPJ1fg1sX8ZDX9cR+WNwH66G4I66CByz4CJ555KtuRgf4WAwfqfe6xxZxRVyleoe8S95NZ8aQd/N5t5RXh2ZKkXfJu1+Sd+Hz8qwIdOLsLDv4HD6Hz/Pe6/ThutkE1IOz8YHendW71nDXE+o5b6oaT3cJYgWZprL3eRqVPJvv7++t4e4DdRGP6PjauQ/WQzMGiKvOyzjFNvgAH+AjeFGCj1mvOfj8rQMf4KPEk+ADfMQzrOAP+CNX91J/UH/kZlXBH/AH/DGrL+P8CT7Axyrjwyxldnd3zQ5Gm153OT09dVtbW9aEv7q6coeHh8X9EhYSnRpk2HKc/1wNItF55HlUc77c9VxcXNi1cx8jazSzHoc24Ia46vAMPkI+Ax9dPgcfge/AB/hI9Q74AB85PQx/fD3+0Pd/fX1N/dFTf1FHzden8Dl8Dp+X+zfgA3x8D/io0Q/0E+nv9vW3H1tfWcNddi61nu2ph7peO1ODV4VIq/e6mvx6Ml6NUb2OsYiHvK5Hr5Vq4z6c2UWwHsRVjGfwETzjwEc3o4O8Gzx5wQf4SD0VwQf4yM0Ugj/gj3RWFfwBf8AfZU9e8AE+wAf4qOnvoa/QV6usr8xSZmNjY8YHXQNMLy8vrRG+s7PjHh4eivv9sfJwajnOf6489NQcvL29rTpf7nru7u6s4c59jKbfAetBXPmZCOAj5DPyVZfPybvBa66ED711pULhKXhQs0a0wYPz+oJ8Rb5K9Sf5ajhftejvx+JBj9WhOgHdTh3VV0fCg/AgfYZynwV8gA/wAT7iGQJDeo86avnqKGu4yw7m5OTEGgDyUNcrR3pKWpuaDbJqKe3XoisIWo+LP1cNd03vrjlf7nr0+rc27sPZjx6sRxevxFWHZ/AR8hn4CPkcfICPlLfBB/hI9R78AX/k6gH4A/6AP8r1MvgAH+ADfAz109BX6Cv0VbnfvEr4MEsZNWi3t7etua4ni46Ojtx4PLYm9sHBgTs+Pi7u11OACpabm5um4/znbm5u2nn1613N+XLXE9tncB+sh+KVuAp4Bh8hn5GvunwOPsBHju/BB/hI9R78AX+k9QD8AX/AH+V6GXyAD/ABPmr6aeir5dZX6kvqjWT6ovV9Yfq7831z4dwa7rIfST22aj3d9WS6Gu5qdKfeOzWe7gpkEZOerMt5Zr5+3Xka9V2PXjfWxn04e1OA9XhjCZK4ck74AR8hf4CP/9msDfARPBXBB/hI9QX4AB85z1H4A/5I6xr4A/6AP8LMD/ARPNvT/gX8AX+AD/Ax1G+l/ljN+sMsZSQW/AKraa0BprFXkAYZlPZrIIr+rYil5TidU5+rwNOfNfDUe073nc8fF19PfK3cB+tBXM3iFXyEfEa+Iu+m/NKCj7i5EvMdcUVcfUlcoa+6H8hL+hOdGL6flnz1XOKqlFdzep+8O1+fgY/F8AGfk3f7eIc+Qz4+0LvoXfQufdGhfiu6ZFaXWMNdT0SLdLTpz/v7+zOe7WdnZ8X9EsQqANbX15uOk7eZPvf+/t4a7r6I8P+/5Xrif9tyXHyf3AfrkcY5cRXyAPgAH+ADHhziV/g86Cj4A/6IZx+hd2fzJ/gAH+AjzE5DX6Gv0Ff1fSj4A/6AP+APP3v0OfSpR7/88stn+c3FE211A7V/V4P+33//nVq+1B63sbExtZD54Ycfpg19//9bPkf+8dq4j+4HE9bD2Q84xJVzwhP4CPkMfJB3U34DH+Aj1Rvwx51pKvijy5c+PuAP+AP+KNeH4AN8gA/wMdS/QV+hr7y+RF/N5wvwsZr4GP3555+f5b+u4Q7es1Kiqfbv8l5Xw0Im+S3HyYJG//7du3fWKJe32SLn1+fotQX92sl9OPPCZz2Iqxi/4CPkM/Cx+nlXPBJ75A3xUgkfLZ9DXK1+XA3FUbqfvPu0ebcFn+jEDp9eF5CvyFfPJV+Bc+pz8tXq5ytwDs7B+erj/LuuozQ0Vb+mqFmdm6rtp82W9v/4448m5NXkbZniyxTb/BRbPziW9SCujo+P7cco4fLh4cF+kBrCY24/U9CXewo6eRf+AOdBf5CvyFfj8XiG78AH+MjxJPVHV0eAD/ABPkb2g2KuDwE+wAf4AB+l/BD329L6Q0JUXuUt/U10CbpEuiyNq5Ea7hpUNTQ1t7SfqdtM3WbqNlO3h/IHU7dXc+q2xAj84ewNLf3wLIKdTCZTizX9mJ3mx9x+8AE+/Bt+cXwQV+gr9BX6Cn0V+GGIX+NBqEPfG3Vt//eKLkGXoEuc0xty6Pb+uoa8G74fdDu6PafbbWiqftEZmjZb2s+0aqZVM62aadVD+YNp1bPTqiViybvOPTf+iEXlp0+fpsO+n9t9wOcBj/E6CpPiM/IV+UpPxOnHRHAe8txT4aOUV/35yFfkqxwec/GhH7lr9RV8Tlw9RVyRr4gr4mpeP8HnXX1Bv6Q/P8TfTwufL3vetYa7nswbmo5d2i8A6QtZX193+/thavLbt29d39RYv//+/t4aT/5LrT2uNKGa+2A9FHfEVZj2zjR3prkzzZ1p7mpCDPErfN7pFvgD/hBepI1jXQs+wEda16Cv0FfoK/QV+mpYX9L3me+LwR/wB/zxffCHWcrs7u661DOz1kNchamKkr29PfMSrD3Oeymdn59bs1+eR4t4Gcuz+uLiwp5o4D5G1ihgPY4ccRU8cMFHyEvgo/O4BB/gIzcTAnyAj9SrEv6AP1Jd/zX4o8Y7lXxFviJfhZkf6Qwn8AE+wAf4GOrvfQ0+r+nvPWa+qtEPOU939C5696n0rjXcP3z40Ow5673v9HqEGrwCSo1XbeqZpyfjBUS9tpvzCqvxwNXr/Nq4D2evq7AeE3vjgrhy9uYI+AgesOCj8yIEHy+NM8DHrMc8+AAfqVcp/AF/oNvLXtfgA3yAD/BR079AX6Gv0FdlL3jwAT5WGR9mKbOxsTG1dNErs2trazbtWw3LnZ0d9/DwUNzvj5XXZctx/nPloadz3t7eVp0vdz13d3fWPOE+RtPvgPUgrryPJfgI+Yx81eVz8m7weS3hQ29dqZB+Ch6U15w2eHBeX5CvyFep/iRfDeerFv39WDzosTpUJ6DbqaP66kh4EB6kz1Dus4AP8AE+wEfsZz6k96ijlq+Osoa77GBiDyG9UqGnpLWp2SCrltJ+LbqCQN5cLcfFn6uGu6b61pwvdz16HUYb9+HsRw/Wo4tX4qrz1AMfIZ+Bj5DPwQf4SHkbfICPVO/BH/BHrh6AP+AP+KNc94IP8AE+wMdQXwx9hb5CX5X7zauED7OUUYO21Xvde0LpKUAFy83NjXkDD3lFpfs3Nzetqa9f73KesjmPpdQrXsLG2wNwH6yH4oO42p7ORAAfIS+Rrw5s1gb4AB85T0XwAT7QV+VZROADfIAP8DFUZ6Kv0Ffoq5E5JeT6QuADfDwXfCziBY9ORCfmdKI13GU/osToX3HXa/SxL+Hr16+L+/VkuhruEiAtx8lvXZ+rV+sFPD1Z533Y+87nj4uvV68ba+M+nL0pwHoQV+Ajn8/AB3k35Rf4I/A9+AAf4AO9O1QPoNuDvoA/4I+0XgYf4CPXT0Ffoa/QV+gr9FV9v3mV9JVZyugJWC8Q1LTWANPYK0iDDEr7JTT0b5VEWo7TOfW5atLrz2qke8/pvvP54+Lria+V+2A9iKtZvIKPkM/IV+TdlF9a8BGTf8x3xBVx9SVxhb7qHrwo6U90Yvh+WvLVc4mrUl7N6X3y7nx9Bj4Wwwd8Tt7t4x36DPn4QO+id9G79EWH+q3oklldYg13PREt0tGmP+/v7894tp+dnRX3SxCrAFhfX286Tt5m+tz7+3truPsiwv//luuJ/23LcfF9ch+sRxrnxFXIA+ADfIAPeHCIX+HzoKPgD/gjnn2E3p3Nn+ADfICPE6u7c3Uv+AAf4AN8lPID/av5PiX1B/VHro+9LP0rs5TRkKxW73XvYacAV5NeA0tTz5oaT3cZ4ktYyPMo5+l0cNB5AfV55mkohTbuY2Q/YLAeRzYolLhyTvgBHyF/gI/OUxF8jKczQ8AH+Ej1BfgAH7mZQvDH1+OPGu9U1uPrrUdNfcZ6sB5p3Y++Ql+hr4JnO/goz3p8TP6o0Q+5GQPkK/LVU+Ura7jLziUerPjmzRuzetEmq5jJZFLcr9dK1OAVUFqO85+rJ+Ml5PSaaM35ctcT++dxH6yH4pW4CoOEwUfIZ+SrLp+DD/DhB43HvA0+wEeq9+AP+COtB+AP+AP+cPZmdq7uBR/gA3yAj1J+iPtU6Cv0Ffqq3G9eJXyM/vjjj896uvzu7s4a3i9evLDmun/i/b///a+7uroq7t/a2rL9r169ajrOf+779+/t2Ovrazv/0Ply+9Xs18Z9OMd6dPFKXAU8g4+Qz8BHHh96E0I5uIUHljWudB+3t7fVfLas99HHu/AguqQmPlaZB8E5un1RPvd8t8r4qMkPMd8vKw+Cc3C+KM7pM8zrJHBOPZj29+DBtn7JUL1MvqIPl+ov5d3Rb7/99nlzc3P6hLmecNPmnzgf+rsaG/o1X/+1HOc/Vxeh/9To1zZ0vtz+m5sbO5b7cNZoYj2cxRRx1eEJfIR8Bj66/A4+ujeqwMcs34MP8JHqOPgD/kjrAfgD/sjVa/AH/AF/lPsn4AN8gA/wMdRfRV+tpr6yoal6ol1PEcjaRX9W8/vdu3fWkPj555+tiVvaH0+rbjnOf246xXbofLn9fuAq9+Ec69HFK3EV8Aw+Qj4DH+Aj5bMWfMT/NuY74oq4+pK4Ql/160/4fLX5vJRXh+oB8i5590vyLnxO3q3te7ToRPicuCKu6CfKApu+6Esn2/KafvKq93et4S4P9ngiejz9ODc9nenITEdOp6fH8VP6M3E1HzdDuGPqNlO3RVTK0eRd8i5598SEG7rkrTs7OxvUbfAH/AF/zPOnzx/gA3yAD/CR6uucvqCuDfpbDzfqBwi9Td9S15N353UbcUVcpXUdumQ1dYkNTd3d3Z16tsub6Pi4m1Kr7e+//3bpVOV4/8PDgzWD9vb2mo7zn3t+fm5Fo/zXa86Xu56hqcI1n8t9dOvMeowtDoUD4irkAfABPlIeAB/gI9UJ8Af84fkTnTivo8EH+AAfnb7O1ZngA3yAD/BR04ei/qD+oP4o92nBx/Lhwxruetw/npqcm7pe2i8PXDXc9YtMy3F61WIymdgvpGq4e08j//9brmeVpthKbLAenbcycQU+mOb+0n4ASqfdgw/4Q/wJPsBHLj+gr7r8AD7AB/iY1w+5Oos66o1ZUXpveuoP6g/4A/6AP+CPtP4e6lNSn1Of5+oPs5TZ2Niwho42EYyCRU+SqxH+008/WTO8tN8fq3/Xcpz/3H/++cfOKR++mvPlrufurpswzH2Mpt8B60FceTyDj5DPyFddPifvBr77FvjQ968NHpzXF99iPVK9Az6+LT5Yj7L+XiZ8+GsZqhPQ7dRRfXUkPAgP0mco91nAB/gAH+CjpU+7TDqR/u5Lp/WwhrvsYPq8Yfs8utbW1qwZP+Sp2rdfDfePHz9aA6LGSy29Hr2GqI37cI71CF7XxFXndQw+gtcx+AAf8Ed5lgT4AB/gA3zU1APoK/RVWtfBH/AH/AF/wB91s4aoz2c97eEP+GOV+cMsZdQwlxeSnjC/vLwserbn9st7XU3ym5ubpuO8R9fm5qadVx7RqXdb7fXEr31xH6yHvCGJq4Bn8HE0nVFBvuq8U8EH+IDPy3oHfIAP8AE+auoh9BX6ajwO3uvoq8471+dP8AE+wMdsfgAf+X7jMtbnQzMg0YnoxFqdaA13va6kA7Tt7OyYj13s5ye/otJ+PZmuhrsSSMtx8inX5/pXh/XLlvcvXZYEdgAAIABJREFU7zufPy6+nvj1fO6D9SCuZvEKPkI+I1+Rd1N+AR/gI9U76JKgB8EH+AAf/ynWJ+ADfIAP8FHTv6D+oP6g/qCfONSnpP5YzfrDLGX0C3StZ3vqxSihoSfklURqPPpSDyI16XV+NdIX8ZDX9cSfyX2wHpolQFwFD1jwEbzfyFfdjA7wsRg+Uu91jy3iirhKZ9iQd8m76N3ybKj4uynl1aGZUuRd8u6X5F34vH9WGzpxMZ1Y6kOQr8hXX5KviCvyVe2sS+qP5as/rOGuJ9S1iIt4qEsQa2HX19ddn9d7yZv9/v7eGu4+OBbxgo+vnftgPc7OzhxxFfAMPoKnIvmq88wDH+Ajx/fgA3woP5Q4A32FvoI/wEcpD8Af8Af8MZsf4r4I+AAf4AN8DOlo6vPVrM/NUmZ3d9dNJhNruL9588adnp66ra0tK7qurq7c4eFhcb8CQ810DSxtOc5/rgY66jzybqo5X+56Li4u7Nq5j5E10liPQxsUSlx1eAYfIZ+Bjy6fg4/Ad+ADfKR6B3yAj5wehj++Hn/o+7++vqb+6Km/qKPm61P4HD6Hz8v9G/ABPr4HfNToB/qJ9Hf7+tuPra+s4S47l3iwiZp0epVMm6xi1Iwv7dfrMWrwqhBpOc5/rp6MV2NUdjQ158tdT+yfx32wHopX4uql4UlvjoCPkM/IV10+Bx/gw+eHmLfBB/hI9R78AX+k9QD8AX/AH52+ztW94AN8gA/wUcoPcZ8KfYW+Ql+V+82rhA+zlNnY2JjxQU89pvq82f2x8lJvOc57wctDT8nn9vbWGu+pR/yQh6P2393dWXOR+xhNvwPWg7jyMxHAx/9NZ1SQr36yHzfJu8Fb+FvgI/WOZT2+7Xqk3pisB+uRmykEf8zyh8+d6HbqD9Vv4AN9lfYLvoW+gs/LXtesB/Ug+AAfNf1W+Pxx+dwa7rKDOTk5saZ1q4f62tqaNetbj4s93dU01/TuRTzkdV69/q2N+3CO9fg8nSVAXHV4Bh/BMw58gI901gj4AB85T0X4A/5IdS38AX/AH2EmDvjoPKnhj/IMOPQV+gp8gI+a/h76Cn21yvrKLGXUMN/e3jbRcHl56Y6Ojtx4PLYm9sHBgTs+Pi7ul/e6muc3NzdNx/nP3dzctPM+PDxUnS93PfHrOdwH66F4Ja4CnsFHyGfkqy6fgw/wkeN78AE+Ur0Hf8AfaT0Af8Af8Ee5XgYf4AN8gI+afhr6arn1lRqTeiOAvmh9X5j+7nzfXDgf/frrr2Ypo4a5NgkF/coUv3Kip89L+1Wg6xWlV69eNR2nV1D1ue/fv7fXEOUB719x6DufPy6+Hm9Hw310w2dZD+IKfOTzGfgg76b8Iv7Q9uLFi6/Gg7GlDDw4qy/g86C/yFfkq1y+8vYd31q3+wdlcrp8qI4A5+A8rTOpB4NuBR/gA3wE649l5kH6V5tTW2j6cPTh6O92dctcH+7333//rInFXjhr2IueWK/9u5q7EtZq1Lcc588jUaELE0i1tZ5f/16TZLVxH86a7azHZ0v+xFWHJ/AR8hn46PJ7ig819rTV5n3iapYniat8XMHn6BLlFfABPtL6YBFdkvLUonHlPwed2L1ZDJ/D57n6/Vvh47FwTp9hvp+zSN5NddyieZf1YD3iPPNccT5UL4MP9G5O75qlzIcPH6xZre36+todHh66yWRif9cE9tPT0+J+DUrVB+sXvpbj/Oem09yHzpfbH0+x5T5YD8UrcRXwDD5CPiNfdfkcfICPHN+DD/CR6j34A/5I6wH4A/6AP8r1MvgAH+ADfNT009BX6Cv0VbnfvEr4sKGpfhKtCEI+Myq60ynG8nnP7WeK7eNOsdV3zHqMpt+BXk1piUf9+0+fPjlZNiiW/WuZ/v+X4ji3n2nuTHNnmjvT3J9imntsKUO++mSD172+IO+Sd8m7zyPveqyir9Dt3uZIuRzdTv0Bn8/3U+iXUJ9L76N36Sf29VvpX61mPWgN9729PXdycmIFbzp1Xk+vp1Nj46nsTBVmqvAqTxUWJsCHM5siiYTW/BDnD/0AIpsfbUN5Jbf//PzcjmU9WA/iap6XwceZzYHRRr5Cl6BL9ou6HnyAD/ABPmrqfnR71xdBX6GvvL6k/qD+oC9a5k/wkceHWcqokba9HaaKM42Xabzey9lPG26JD6ZuL/fUbYmGmunpcR6QZ5mSqIZAtOQHplXnp1WrKUjedTbkmbg6thkowiV599LeTAIfY/sOwEeXP8FH0OfgA301Hnf5AX2FvlJ+LNVn8Af8kcYH/AF/wB+z/BnnzxQf4lm9cdnS9yDvkndzedca7nr976+//jIB9/r1a/Nvj4NOvuml/XpiVQ0TBWzLcWo26XP9q8N64sYX2X3n88fF1xO/ns99sB7E1SxewUfIZ+Qr8m7KL+ADfKR6B10S9CD4AB/gI/wICH9QDw7VmfAH/JHrp1B/UH/AH/AH/FHfb16l+sMsZSSmaz3bU69GGdqrCa5GfepNVeO9rcDT+TW4dREPeV1P7IfFfbAeijviKnjAgo/gmUe+6mZ0gI/F8JF6r3tsEVfEVap3yLvk3XRmDHk3n3dLeXVophR5l7z7JXkXPi/PikAnzs6yg8/hc/g8771OH66bTUA9OBsf6N1ZvWsNdz2hnvOmqvF0lyBWkGkqe5+nUcmz+f7+3hruPlAX8YiOr537YD00Y4C46ryMU2yDD/ABPoIXJfiY9ZqDz9868AE+SjwJPsBHPMMK/oA/cnUv9Qf1R25WFfwBf8Afs/oyzp/gA3ysMj7MUmZ3d9fsYLTpdZfT01O3tbVlTfirqyt3eHhY3C9hIdGpQYYtx/nP1SASnUeeRzXny13PxcWFXTv3MbJGM+txaANuiKsOz+Aj5DPw0eVz8BH4DnyAj1TvgA/wkdPD8MfX4w99/9fX19QfPfUXddR8fQqfw+fwebl/Az7Ax/eAjxr9QD+R/m5ff/ux9ZU13GXnUuvZnnqo67UzNXhViLR6r6vJryfj1RjV6xiLeMjrevRaqTbuw5ldBOtBXMV4Bh/BMw58dDM6yLvBkxd8gI/UUxF8gI/cTCH4A/5IZ1XBH/AH/FH25AUf4AN8gI+a/h76Cn21yvrKLGU2NjZmfNA1wPTy8tIa4Ts7O+7h4aG43x8rD6eW4/znykNPzcHb29uq8+Wu5+7uzhru3Mdo+h2wHsSVn4kAPkI+I191+Zy8G7zmSvjQW1cqFJ6CBzVrRBs8OK8vyFfkq1R/kq+G81WL/n4sHvRYHaoT0O3UUX11JDwID9JnKPdZwAf4AB/gI54hMKT3qKOWr46yhrvsYE5OTqwBIA91vXKkp6S1qdkgq5bSfi26gqD1uPhz1XDX9O6a8+WuR69/a+M+nP3owXp08UpcdXgGHyGfgY+Qz8EH+Eh5G3yAj1TvwR/wR64egD/gD/ijXC+DD/ABPsDHUD8NfYW+Ql+V+82rhA+zlFGDdnt725rrerLo6OjIjcdja2IfHBy44+Pj4n49Bahgubm5aTrOf+7m5qadV7/e1Zwvdz2xfQb3wXooXomrgGfwEfIZ+arL5+ADfOT4HnyAj1TvwR/wR1oPwB/wB/xRrpfBB/gAH+Cjpp+GvlpufaW+pN5Ipi9a3xemvzvfNxfOreEu+5HUY6vW011PpqvhrkZ36r1T4+muQBYx6cm6nGfm69edp1Hf9eh1Y23ch7M3BViPN5YgiSvnhB/wEfIH+PifzdoAH8FTEXyAj1RfgA/wkfMchT/gj7SugT/gD/gjzPwAH8GzPe1fwB/wB/gAH0P9VuqP1aw/zFJGYsEvsJrWGmAaewVpkEFpvwai6N+KWFqO0zn1uQo8/VkDT73ndN/5/HHx9cTXyn2wHsTVLF7BR8hn5CvybsovLfiImysx3xFXxNWXxBX6qvuBvKQ/0Ynh+2nJV88lrkp5Naf3ybvz9Rn4WAwf8Dl5t4936DPk4wO9i95F79IXHeq3oktmdYk13PVEtEhHm/68v78/49l+dnZW3C9BrAJgfX296Th5m+lz7+/vreHuiwj//1uuJ/63LcfF98l9sB5pnBNXIQ+AD/ABPuDBIX6Fz4OOgj/gj3j2EXp3Nn+CD/ABPsLsNPQV+gp9Vd+Hgj/gD/gD/vCzR59Dn3r0yy+/fJbfXDzRVjdQ+3c16P/999+p5UvtcRsbG1MLmR9++GHa0Pf/v+Vz5B+vjfvofjBhPZz9gENcOSc8gY+Qz8AHeTflN/ABPlK9AX/cmaaCP7p86eMD/oA/4I9yfQg+wAf4AB9D/Rv0FfrK60v01Xy+AB+riY/Rn3/++Vn+6xru4D0rJZpq/y7vdTUsZJLfcpwsaPTv3717Z41yeZstcn59jl5b0K+d3IczL3zWg7iK8Qs+Qj4DH6ufd8UjsUfeEC+V8NHyOcTV6sfVUByl+8m7T5t3W/CJTuzw6XUB+Yp89VzyFTinPidfrX6+AufgHJyvPs6/6zpKQ1P1a4qa1bmp2n7abGn/jz/+aEJeTd6WKb5Msc1PsfWDY1kP4ur4+Nh+jBIuHx4e7AepITzm9jMFfbmnoJN34Q9wHvQH+Yp8NR6PZ/gOfICPHE9Sf3R1BPgAH+BjZD8o5voQ4AN8gA/wUcoPcb8trT8kROVV3tLfRJegS6TL0rgaqeGuQVVDU3NL+5m6zdRtpm4zdXsofzB1ezWnbkuMwB/O3tDSD88i2MlkMrVY04/ZaX7M7Qcf4MO/4RfHB3GFvkJfoa/QV4Efhvg1HoQ69L1R1/Z/r+gSdAm6xDm9IYdu769ryLvh+0G3o9tzut2GpuoXnaFps6X9TKtmWjXTqplWPZQ/mFY9O61aIpa869xz449YVH769Gk67Pu53Qd8HvAYr6MwKT4jX5Gv9EScfkwE5yHPPRU+SnnVn498Rb7K4TEXH/qRu1ZfwefE1VPEFfmKuCKu5vUTfN7VF/RL+vND/P208Pmy511ruOvJvKHp2KX9ApC+kPX1dbe/H6Ymv3371vVNjfX77+/vrfHkv9Ta40oTqrkP1kNxR1yFae9Mc2eaO9PcmeauJsQQv8LnnW6BP+AP4UXaONa14AN8pHUN+gp9hb5CX6GvhvUlfZ/5vhj8AX/AH98Hf5ilzO7urks9M2s9xFWYqijZ29szL8Ha47yX0vn5uTX75Xm0iJexPKsvLi7siQbuY2SNAtbjyBFXwQMXfIS8BD46j0vwAT5yMyHAB/hIvSrhD/gj1fVfgz9qvFPJV+Qr8lWY+ZHOcAIf4AN8gI+h/t7X4POa/t5j5qsa/ZDzdEfvonefSu9aw/3Dhw/NnrPe+06vR6jBK6DUeNWmnnl6Ml5A1Gu7Oa+wGg9cvc6vjftw9roK6zGxNy6IK2dvjoCP4AELPjovQvDx0jgDfMx6zIMP8JF6lcIf8Ae6vex1DT7AB/gAHzX9C/QV+gp9VfaCBx/gY5XxYZYyGxsbU0sXvTK7trZm077VsNzZ2XEPDw/F/f5YeV22HOc/Vx56Ouft7W3V+XLXc3d3Z80T7mM0/Q5YD+LK+1iCj5DPyFddPifvBp/XEj701pUK6afgQXnNaYMH5/UF+Yp8lepP8tVwvmrR34/Fgx6rQ3UCup06qq+OhAfhQfoM5T4L+AAf4AN8xH7mQ3qPOmr56ihruMsOJvYQ0isVekpam5oNsmop7deiKwjkzdVyXPy5arhrqm/N+XLXo9dhtHEfzn70YD26eCWuOk898BHyGfgI+Rx8gI+Ut8EH+Ej1HvwBf+TqAfgD/oA/ynUv+AAf4AN8DPXF0FfoK/RVud+8SvgwSxk1aFu9170nlJ4CVLDc3NyYN/CQV1S6f3Nz05r6+vUu5ymb81hKveIlbLw9APfBeig+iKvt6UwE8BHyEvnqwGZtgA/wkfNUBB/gA31VnkUEPsAH+AAfQ3Um+gp9hb4amVNCri8EPsDHc8HHIl7w6ER0Yk4nWsNd9iNKjP4Vd71GH/sSvn79urhfT6ar4S4B0nKc/Nb1uXq1XsDTk3Xeh73vfP64+Hr1urE27sPZmwKsB3EFPvL5DHyQd1N+gT8C34MP8AE+0LtD9QC6PegL+AP+SOtl8AE+cv0U9BX6Cn2FvkJf1febV0lfmaWMnoD1AkFNaw0wjb2CNMigtF9CQ/9WSaTlOJ1Tn6smvf6sRrr3nO47nz8uvp74WrkP1oO4msUr+Aj5jHxF3k35pQUfMfnHfEdcEVdfElfoq+7Bi5L+RCeG76clXz2XuCrl1ZzeJ+/O12fgYzF8wOfk3T7eoc+Qjw/0LnoXvUtfdKjfii6Z1SXWcNcT0SIdbfrz/v7+jGf72dlZcb8EsQqA9fX1puPkbabPvb+/t4a7LyL8/2+5nvjfthwX3yf3wXqkcU5chTwAPsAH+IAHh/gVPg86Cv6AP+LZR+jd2fwJPsAH+DixujtX94IP8AE+wEcpP9C/mu9TUn9Qf+T62MvSvzJLGQ3JavVe9x52CnA16TWwNPWsqfF0lyG+hIU8j3KeTgcHnRdQn2eehlJo4z5G9gMG63Fkg0KJK+eEH/AR8gf46DwVwcd4OjMEfICPVF+AD/CRmykEf3w9/qjxTmU9vt561NRnrAfrkdb96Cv0FfoqeLaDj/Ksx8fkjxr9kJsxQL4iXz1VvrKGu+xc4sGKb968MasXbbKKmUwmxf16rUQNXgGl5Tj/uXoyXkJOr4nWnC93PbF/HvfBeiheiaswSBh8hHxGvuryOfgAH37QeMzb4AN8pHoP/oA/0noA/oA/4A9nb2bn6l7wAT7AB/go5Ye4T4W+Ql+hr8r95lXCx+iPP/74rKfL7+7urOH94sULa67HT7xfX18X929tbbmrqyv36tWrpuP05K0+9/3793as/uyfKOo7nz8uvl41+7VxH86xHsRVih/wEfIZ+Fh9fEjk3t7eVvMZ+AAfqd5BlwQ9uKz4AOfodvgcPkfvUp8P9S/gc/g819+CP1afP+gnohOXBeej33777fPm5ub0CXM94abNP3E+9Hc1NvRrvv5rOc5/roo5/adGv7ah8+X239zc2LHch7NGE+vhLKaIqw5P4CPkM/DR5Xfw0b1RBT5m+R58gI9Ux8Ef8EdaD8Af8EeuXoM/4A/4o9w/AR/gA3yAj6H+KvpqNfWVDU3VE+36FUjWLvqzmt/v3r2zhsTPP/9sTdzS/nhadctx/nPTKbZD58vt9wNXuQ/nWI8uXomrgGfwEfIZ+AAfKZ+14CP+tzHfEVfE1ZfEFfqqX3/C56vN56W8OlQPkHfJu1+Sd+Fz8m5t36NFJ8LnxBVxRT9RFtj0RV862ZbX9JNXvb9rDXd5sMcT0ePpx7np6UxHZjpyOj09jp/Sn4mr+bgZwh1Tt5m6LaJSjibvknfJuycm3NAlb93Z2dmgboM/4A/4Y54/ff4AH+ADfICPVF/n9AV1bdDferhRP0DobfqWup68O6/biCviKq3r0CWrqUtsaOru7u6MZ/vxcZhSm5viG+9/zKnCSjxD52Oq8KVLp1yzHvPxen5+bs0I+foTVxfWpALnI7NSkbje29tzLXnO5x3iamyxpFkaTHNnmvtTTXMnX20bb5GvOr1D3v3+8q5ywN9//43ePToarM/Ax/eHj5q6Bv6AP9J+Abod3f496PYa/UA/kX5iXz/1sfs+1nDX4/7x1OTc1PXSfnngqoElYm85Tq9aTCYT+4VUwsF7Gvn/33I9qzTFVkmC9ei8lYkr8ME095fW4E6n3YMP+EP8CT7ARy4/oK+6/AA+wAf4mNcPuTqLOuqNWVF6b3rqD+oP+AP+gD/gj7T+HupTUp9Tn+fqD7OU2djYsIaONhGMgkVPlqgR/tNPP1kzvLTfH6t/13Kc/9x//vnHzikfvprz5a7HTyjnPkaO9ejilbgKeAYfIZ+BD/CR8tm3wIfykzZ4cF5ffIv1SPUO/AF/5PQw/DHLHx6rQ3UCup06qq+OhAfhQfoM5T4L+AAf4AN8tPRpqaOWr+9jDXfZK/R5w/Z5dK2trVkzfshTtW+/Gu4fP360BkSNl1p6PXqdUhv34RzrEbyuiavO6xh8BK9j8AE+4I/yLAnwAT7AB/ioqQfQV+irtK6DP+AP+AP+gD/qZg1Rn8962sMf8Mcq84dZyqhhnno6jcfBE6/P61ge2WqS39zc9HotlrzZNzc37cn2h4eHqTdwq7dy/NoX98F6KH6Iq84DWLgDH8EDlXx1YN714AN85DxgwQf4SPUX/AF/pPUA/AF/wB/lmWPgA3yAD/BRM5MQfbXc+moRL3jqKOqoXB1lDXe9rqTEoG1nZ8d87GI/P/kVlfbryXQ13NXobjlOPuX6XP8qt37Z8v7lfefzx8XXE7+ez32wHsTVLF7BR8hn5Cvybsov4AN8pHoHXRL0IPgAH+DjP8X6BHyAD/ABPmr6F9Qf1B/UH/QTh/qU1B+rWX+YpYx+Yav1bE+9GCU09IS8kkiNR1/qQaQmvc6vRvoiHvK6nvgzuQ/WQ7MEiKswgwF8BO838lU3owN8LIaP1HvdY4u4Iq7SGTbkXfIuerc8Gyr+bkp5dWimFHmXvPsleRc+75/Vhk5cTCeW+hDkK/LVl+Qr4op8VTvrkvpj+eoPa7jrCXUt4iIe6hLEWtj19XXX5/Ve8ma/v7+3hrsPjkW84ONr5z5Yj7OzM0dcBTyDj+CpSL7qPPPAB/jI8T34AB/KDyXOQF+hr+AP8FHKA/AH/AF/zOaHuC8CPsAH+AAfQzqa+nw163OzlNnd3XWTycQa7m/evHGnp6dua2vLiq6rqyt3eHhY3K/AUDNdA0tbjvOfq4GOOo88j2rOl7uei4sLu3buY2SNNNbj0AaFElcdnsFHyGfgo8vn4CPwHfgAH6neAR/gI6eH4Y+vxx/6/q+vr6k/euov6qj5+hQ+h8/h83L/BnyAj+8BHzX6gX4i/d2+/vZj6ytruMvOJR7coCadXiXTJqsYNeNL+/V6jBq8KkRajvOfqyfj1RiVHU3N+XLXE/vncR+sh+KVuHppeNKbI+Aj5DPyVZfPwQf48Pkh5m3wAT5SvQd/wB9pPQB/wB/wR6evc3Uv+AAf4AN8lPJD3KdCX6Gv0FflfvMq4cMsZTY2NmZ80FOPqT5vdn+svNRbjvNe8PLQU/K5vb21xnvqET/k4aj9d3d31lzkPkbT74D1IK78TATw8X/TGRXkq5/sx03ybvAW/hb4SL1jWY9vux6pNybrwXrkZgrBH7P84XMnup36Q/Ub+EBfpf2Cb6Gv4POy1zXrQT0IPsBHTb8VPn9cPreGu+xgTk5OrGnd6qG+trZmzfrW42JPdzXNNb17EQ95nVevf2vjPpxjPT5PZwkQVx2ewUfwjAMf4COdNQI+wEfOUxH+gD9SXQt/wB/wR5iJAz46T2r4ozwDDn2FvgIf4KOmv4e+Ql+tsr4ySxk1zLe3t000XF5euqOjIzcej62JfXBw4I6Pj4v75b2u5vnNzU3Tcf5zNzc37bwPDw9V58tdT/x6DvfBeiheiauAZ/AR8hn5qsvn4AN85PgefICPVO/BH/BHWg/AH/AH/FGul8EH+AAf4KOmn4a+Wm59pcak3gigL1rfF6a/O983F85Hv/76q1nKqGGuTUJBvzLFr5zo6fPSfhXoekXp1atXTcfpFVR97vv37+01RHnA+1cc+s7nj4uvx9vRcB/d8FnWg7gCH/l8Bj7Iuym/iD+0vXjx4qvxYGwpAw/O6gv4POgv8hX5KpevvH3Ht9bt/kGZnC4fqiPAOThP60zqwaBbwQf4AB/B+mOZeZD+1ebUFpo+HH04+rtd3TLXh/v9998//7//9/+s4a1Nw1601f5dzV0JazXqW47z59EF6cIE0kXOr8/RJFlt3IezZjvr4SzQiasOz+Aj5DPw0eV38BH4DnyAj1TvgA/wkdOj8Af8kdY58Af8AX+U+wfgA3yAD/BR099DX6GvVllfmaXMhw8f3NbWljVirq+v3eHhoZtMJvZ3TWA/PT0t7tegVFnKKKG2HOc/N53mPnS+3P54ii33wXooXomrgGfwEfIZ+arL5+ADfOT4HnyAj1TvwR/wR1oPwB/wB/xRrpfBB/gAH+Cjpp+GvkJfoa/K/eZVwocNTfWTaEUQ8plR0Z1OMZbPe24/U2wfd4qtvmPWYzT9DvRqSks86t9/+vTJybJBsexfy/T/vxTHuf1Mc2eaO9Pcmeb+FNPcY0sZ8tUnG7zu9QV5l7xL3n0eeddjFX2Fbvc2R8rl6HbqD/h8vp9Cv4T6XHofvUs/sa/fSv9qNetBa7jv7e25k5MTK3jTqfN6ej2dGhtPZWeqMFOFV3mqsDABPpzZFEkktOaHOH/oBxDZ/Ggbyiu5/efn53Ys68F6EFfzvAw+zmwOjDbyFboEXbJf1PXgA3yAD/BRU/ej27u+CPoKfeX1JfUH9Qd90TJ/go88PsxSRo207e0wVZxpvEzj9cOw/LThlvhg6vZyT92WaKiZnh7nAQ3vUxKVt3FLfmBadX5atZqC5F1nQ56Jq2ObgSJckncv7c0k8DG27wB8dPkTfAR9Dj7QV+Nxlx/QV+gr5cdSfQZ/wB9pfMAf8Af8Mcufcf5M8SGe1RuXLX0P8i55N5d3reGu1//++usvE3CvX782//Y46OSbXtqvJ1bVMFHAthynZpM+1786rCdufJHddz5/XHw98ev53AfrQVzN4hV8hHxGviLvpvwCPsBHqnfQJUEPgg/wAT7Cj4DwB/XgUJ0Jf8AfuX4K9Qf1B/wBf8Af9f3mVao/zFJGYrrWsz31apShvZrgatSn3lQ13tsKPJ1fg1sX8ZDX9cR+WNwH66G4I66CByz4CJ555KtuRgf4WAwfqfe6xxZxRVyleoe8S95NZ8aQd/N5t5RXh2ZKkXfJu1+Sd+Hz8qwIdOLsLDv4HD6Hz/Pe6/ThutkE1IOz8YHendW71nDXE+o5b6oaT3cJYgWZprL3eRqVPJvv7++t4e4DdRGP6PjauQ/WQzMGiKu/ohF3AAAgAElEQVTOyzjFNvgAH+AjeFGCj1mvOfj8rQMf4KPEk+ADfMQzrOAP+CNX91J/UH/kZlXBH/AH/DGrL+P8CT7Axyrjwyxldnd3zQ5Gm153OT09dVtbW9aEv7q6coeHh8X9EhYSnRpk2HKc/1wNItF55HlUc77c9VxcXNi1cx8jazSzHoc24Ia46vAMPkI+Ax9dPgcfge/AB/hI9Q74AB85PQx/fD3+0Pd/fX1N/dFTf1FHzden8Dl8Dp+X+zfgA3x8D/io0Q/0E+nv9vW3H1tfWcNddi61nu2ph7peO1ODV4VIq/e6mvx6Ml6NUb2OsYiHvK5Hr5Vq4z6c2UWwHsRVjGfwETzjwEc3o4O8Gzx5wQf4SD0VwQf4yM0Ugj/gj3RWFfwBf8AfZU9e8AE+wAf4qOnvoa/QV6usr8xSZmNjY8YHXQNM5YWubWdnxz08PBT3+2Pl4dRynP9ceeipOXh3d1d1vtz1+GO5D+dYjy5eiavgpQU+Qj4DH+Aj5bMSPvTW1eXlZRUvEVfEVW1ctegk4oq4Iq6oP2Lv4Fz+QO+id/0MtDg+4A/4A/6AP+CPwA9D+pt+Cf2StP/9WPrKGu6ygzk5ObHGgjzU9cqRnpLWpqfPZdVS2q/gFZhbj4s/Vw13Te+uOV/uevT6tzbuw9mPHqxHF6/EVYdn8BHyGfgI+Rx8gI+Ut8EH+Ej1HvwBf+TqAfgD/oA/yvUy+AAf4AN8DPXT0FfoK/RVud+8SvgwSxk1aLe3t625rif6jo6O3Hg8tib2wcGBOz4+Lu7XU4AKlpubm6bj/Odubm7aefUrbM35ctcT22dwH6yH4pW4CngGHyGfka+6fA4+wEeO78EH+Ej1HvwBf6T1APwBf8Af5XoZfIAP8AE+avpp6Kvl1lfqS+qJZ/qi9X1h+rvzfXPh3BrusoNJPbZqPd31ZLoa7mp0p947NZ7uCmQRk56sy3lmvn7deRr1XY8e99fGfTh7U4D1eGMJkrhyTvgBHyF/gI//2awN8BE8FcEH+Ej1BfgAHznPUfgD/kjrGvgD/oA/wswP8BE829P+BfwBf4AP8DHUb6X+WM36wyxlJBb8AqtprQGmseeTBhmU9msgiv6tiKXlOJ1Tn6vA05818NR70PWdzx8XX098rdwH60FczeIVfIR8Rr4i76b80oKPuLkS8x1xRVx9SVyhr7ofyEv6E50Yvp+WfPVc4qqUV3N6n7w7X5+Bj8XwAZ+Td/t4hz5DPj7Qu+hd9C590aF+K7pkVpdYw11PRIt0tOnP+/v7M57tZ2dnxf0SxCoA1tfXm46Tt5k+9/7+3hruvojw/7/leuJ/23JcfJ/cB+uRxjlxFfIA+AAf4AMeHOJX+DzoKPgD/ohnH6F3Z/Mn+AAf4CPMTkNfoa/QV/V9KPgD/oA/4A8/e/Q59KlHv/zyy2f5zcWTeXUDtX9Xg/7ff/+dWr7UHqfp6d5C5ocffpg29P3/b/kc+cdr4z66H0xYD2c/4BBXzglP4CPkM/BB3k35DXyAj1RvwB93pqngjy5f+viAP+AP+KNcH4IP8AE+wMdQ/wZ9hb7y+hJ9NZ8vwMdq4mP0559/fpb/uoY7eM9Kiabav8t7XQ0LmeS3HCcLGv37d+/eWaNc3maLnF+fo9cW9Gsn9+HMC5/1IK5i/IKPkM/Ax+rnXfFI7JE3xEslfLR8DnG1+nE1FEfpfvLu0+bdFnyiEzt8el1AviJfPZd8Bc6pz8lXq5+vwDk4B+erj/Pvuo7S0FT9mqJmdW6qtp82W9r/448/mpBXk7dlii9TbPNTbP1T/6wHcXV8fGw/RgmXDw8P9oPUEB5z+5mCvtxT0Mm78Ac4D/qDfEW+Go/HM3wHPsBHjiepP7o6AnyAD/Axsh8Uc30I8AE+wAf4KOWHuN+W1h8SovIqb+lvokvQJdJlaVyN1HDXoKqhqbml/UzdZuo2U7eZuj2UP5i6vZpTtyVG4A9nb2jph2cR7GQymVqs6cfsND/m9oMP8OHf8Ivjg7hCX6Gv0Ffoq8APQ/waD0Id+t6oa/u/V3QJugRd4pzekEO399c15N3w/aDb0e053W5DU/WLztC02dJ+plUzrZpp1UyrHsofTKuenVYtEUvede658UcsKj99+jQd9v3c7gM+D3iM11GYFJ+Rr8hXeiJOPyaC85Dnngofpbzqz0e+Il/l8JiLD/3IXauv4HPi6iniinxFXBFX8/oJPu/qC/ol/fkh/n5a+HzZ86413PVk3tB07NJ+AUhfyPr6utvfD1OT37596/qmxvr99/f31njyX2rtcaUJ1dwH66G4I67CtHemuTPNnWnuTHNXE2KIX+HzTrfAH/CH8CJtHOta8AE+0roGfYW+Ql+hr9BXw/qSvs98Xwz+gD/gj++DP8xSZnd316WembUe4ipMVZTs7e2Zl2Dtcd5L6fz83Jr98jxaxMtYntUXFxf2RAP3MbJGAetx5Iir4IELPkJeAh+dxyX4AB+5mRDgA3ykXpXwB/yR6vqvwR813qnkK/IV+SrM/EhnOIEP8AE+wMdQf+9r8HlNf+8x81WNfsh5uqN30btPpXet4f7hw4dmz1nvfafXI9TgFVBqvGpTzzw9GS8g6rXdnFdYjQeuXufXxn04e12F9ZjYGxfElbM3R8BH8IAFH50XIfh4aZwBPmY95sEH+Ei9SuEP+APdXva6Bh/gA3yAj5r+BfoKfYW+KnvBgw/wscr4MEuZjY2NqaWLXpldW1uzad9qWO7s7LiHh4fifn+svC5bjvOfKw89nfP29rbqfLnrubu7s+YJ9zGafgesB3HlfSzBR8hn5Ksun5N3g89rCR9660qF9FPwoLzmtMGD8/qCfEW+SvUn+Wo4X7Xo78fiQY/VoToB3U4d1VdHwoPwIH2Gcp8FfIAP8AE+Yj/zIb1HHbV8dZQ13GUHE3sI6ZUKPSWtTc0GWbWU9mvRFQTy5mo5Lv5cNdw11bfmfLnr0esw2rgPZz96sB5dvBJXnace+Aj5DHyEfA4+wEfK2+ADfKR6D/6AP3L1APwBf8Af5boXfIAP8AE+hvpi6Cv0Ffqq3G9eJXyYpYwatK3e694TSk8BKlhubm7MG3jIKyrdv7m5aU19/XqX85TNeSylXvESNt4egPtgPRQfxNX2dCYC+Ah5iXx1YLM2wAf4yHkqgg/wgb4qzyICH+ADfICPoToTfYW+Ql+NzCkh1xcCH+DjueBjES94dCI6MacTreEu+xElRv+Ku16jj30JX79+XdyvJ9PVcJcAaTlOfuv6XL1aL+DpyTrvw953Pn9cfL163Vgb9+HsTQHWg7gCH/l8Bj7Iuym/wB+B78EH+AAf6N2hegDdHvQF/AF/pPUy+AAfuX4K+gp9hb5CX6Gv6vvNq6SvzFJGT8DGN6UBprFXkAYZlPZLaOjfKom0HKdz6nPVpNef1Uj3ntN95/PHxdcTXyv3wXoQV7N4BR8hn5GvyLspv4AP8JHqHXRJeIgBfIAP8BE89OEP6sGhOhP+gD/8Q4BxX4T6g/oD/oA/4I/AD0N941WqP6zhriei9ZS5Nv15f39/xrP97OysuF8NcH0h6+vrTcfJ20yfe39/bw13/6X6/99yPfG/bTkuvk/ug/VI45y4CnkAfIAP8AEPDvErfB50FPwBf8Szj9C7s/kTfIAP8HFidXeu7gUf4AN8gI9SfqB/Nd+npP6g/sj1sZelf2WWMhqS1eq97j3sFOBq0mtgaepZU+PpLkN8CQt5HuU8nQ4OOi+gPs88DaXQxn2M7AcM1uPIBoUSV84JP+Aj5A/w0Xkqgo/xdGYI+AAfqb4AH+AjN1MI/vh6/FHjncp6fL31qKnPWA/WI6370VfoK/RV8GwHH+VZj4/JHzX6ITdjgHxFvnqqfGUNd9m5xIMV37x5Y1Yv2mQVM5lMivv1eowavAJKy3H+c/VkvIScXiuoOV/uemL/PO6D9VC8EldhkDD4CPmMfNXlc/ABPvyg8Zi3wQf4SPUe/AF/pPUA/AF/wB/O3szO1b3gA3yAD/BRyg9xnwp9hb5CX5X7zauEj9Eff/zxWU+X393dWcP7xYsX1lz3T7z/97//dVdXV8X9W1tbtv/Vq1dNx/nPff/+vR17fX1t5x86X26/mv3auA/nWI8uXomrgGfwEfIZ+MjjQ29CKAe38MCyxpXu4/b2tprPlvU++ngXHkSX1MTHKvMgOEe3L8rnnu9WGR81+SHm+2XlQXAOzhfFOX2GeZ0EzqkH0/4ePNjWLxmql8lX9OFS/aW8O/rtt98+b25uTp8w1xNu2vwT50N/V2NDv+brv5bj/OfqIvSfGv3ahs6X239zc2PHch/OGk2sh7OYIq46PIGPkM/AR5ffwUf3RhX4mOV78AE+Uh0Hf8AfaT0Af8AfuXoN/oA/4I9y/wR8gA/wAT6G+qvoq9XUVzY0VU+06ykCWbvoz2p+v3v3zhoSP//8szVxS/vjqdstx/nPTae5D50vtz+eYst9/Mdec5T1Duvx0skuqSaOiStw3pfnyFfz8fE95t34nuP8Cg928QGfBx31PeIDndivo8FHHh+lvDpUD5B3ybupbmvJu/A5+aq279ESV/AgcUVc0U+kD/eX9ZTpw720vqw13OXBHk9Ej6cf56anMx2Z6cjp9PQ4fkp/Jq7m42YId0zdZuq2fjBSjibvknfJuyf2Ayq65K07Ozsb1G3wB/wBf8zzp88f4AN8gA/wkerrnL6grg36W000NZD0Nn1LXU/enddtxBVxldZ16JLV1CU2NHV3d3fq2S5vouPjMKU2N8U33v+YU4WVeIbOx1ThS5dOuWY95uP1/PzcmhHy9SeuLqxJBc5HZqUicb23t9eU53zeIa7GFkviCaa5M839qaa5k6+2jbfIV53eIe9+f3lXOeDvv/9G7x4dDdZn4OP7w0dNXQN/wB9pvwDdjm7/HnR7jX6gn0g/sa+f+th9H2u463H/eGpybup6ab88cNXAErG3HKdXLSaTif1CKuHgPY38/2+5nlWaYqskwXp03srEFfhgmvtLa3Cn0+7BB/wh/gQf4COXH9BXXX4AH+ADfMzrh1ydRR31xizRvDc99Qf1B/wBf8Af8Edafw/1KanPqc9z9YdZymxsbFhDR5sIRsGiJ0vUCP/pp5+sGV7a74/Vv2s5zn/uP//8Y+eUD1/N+XLXc3fXTRjmPkbT74D1IK48nsFHyGfkqy6fk3cD330LfOj71wYPzuuLb7Eeqd4BH98WH6xHWX8vEz78tQzVCeh26qi+OhIehAfpM5T7LOADfIAP8NHSp10mnUh/96XTeljDXfYKfd6wfR5da2tr1owf8lTt26+G+8ePH60BUeOlll6PXqfUxn04x3oEr2viqvM6Bh/B6xh8gA/4ozxLAnyAD/ABPmrqAfQV+iqt6+AP+AP+gD/gj7pZQ9Tns5728Af8scr8YZYyapinnk7jcfDE6/N0l0e2muQ3Nze9Xoslb/bNzU17sv3h4WHqDdziIa/PjV/74j5YD8UPcdV5AIOPWY8y8lU3owN8gI+cByz4AB+p/kJfBQ9t8AE+wEd5xhf4AB/gA3wM9WGoP6g/nkv9sYgXPDwID+Z40Bruel1JjTltOzs75mMX+/nJr6i0X0+mq+GuBNtynHzK9bn+1WH9suX9y/vO54+Lryd+PZ/7YD2Iq1m8go+Qz8hX5N2UX8AH+Ej1Drok6EHwAT7Ax3+K9Qn4AB/gA3zU9C+oP6g/qD/oJw71Kak/VrP+MEsZPcFU69meejFKaOgJeSWRGo++1INITXqdX430RTzkdT3xZ3IfrIdmCRBXwQMWfATvN/JVN6MDfCyGj9R73WOLuCKu0hk25F3yLnq3PBsq/m5KeXVophR5l7z7JXkXPu+f1YZOXEwnlvoQ5Cvy1ZfkK+KKfFU765L6Y/nqD2u46wl1LeIiHuoSxFrY9fV11+f1XvJmv7+/t4a7D45FvODja+c+WI+zszNHXAU8g4/gqUi+6jzzwAf4yPE9+AAfyg8lzkBfoa/gD/BRygPwB/wBf8zmh7gvAj7AB/gAH0M6mvp8Netzs5TZ3d11k8nEGu5v3rxxp6enbmtry4quq6srd3h4WNyvwFAzXQNLW47zn6uBjjqPPI9qzpe7nouLC7t27mNkjTTW49AGhRJXHZ7BR8hn4KPL5+Aj8B34AB+p3gEf4COnh+GPr8cf+v6vr6+pP3rqL+qo+foUPofP4fNy/wZ8gI/vAR81+oF+Iv3dvv72Y+sra7jLziUejKUmnV4l0yarGDXjS/v1eowavCpEWo7zn6sn49UYlR1Nzfly1xP753EfrIfilbh6aXjSmyPgI+Qz8lWXz8EH+PD5IeZt8AE+Ur0Hf8AfaT0Af8Af8Eenr3N1L/gAH+ADfJTyQ9ynQl+hr9BX5X7zKuHDLGU2NjZmfNBVdA95Kvr9/lh5qbcc573g9TlKPnd3d9YgTD3ivYdj3/X4Y7kP51iPn+zHG+IqeKeCj5DPwAf4SGeNgA/wkeoL+AP+iGcKoXf/mdHn4AN8gA9ntWuu7gUf4AN8gA/6V/n8mM62QV+hr0r931Wqz63hLjuYk5MTE9StHupra2vWrG89LvZ0V1LS9O5FPOR1Xr3+rY37cI71+DydJUBcdXgGH8EzDnyAj3TWCPgAHzlPRfgD/kh1LfwBf8AfYSYO+Og8qeGP8gw49BX6CnyAj5r+HvoKfbXK+sosZdQw397eNtFweXnpjo6O3Hg8tib2wcGBOz4+Lu6X97qa5zc3N03H+c/d3Ny08z48PFSdL3c98es53AfroXglrgKewUfIZ+SrLp+DD/CR43vwAT5SvQd/wB9pPQB/wB/wR7leBh/gA3yAj5p+GvpqufWVGpN///13U3+TOoo6KldHjX799VezlFHDXJuEgn5lUoCJMGTxoqfPS/sVWHrk/9WrV03H+c99//692aDIA77mfLnrub29tWO5j274LOvx0RFXAc/gI+Qz8NHlc/Axiw9x34sXL74aD8YWJqzHrL4gX5GvUv1JvlpOPvcPygzVCeh26qi+OpJ6cD4+4EF4EB4s96HAB/gAH+Cjpm+8NH2f33///bMmFnvhrGEvemK99u9q7qpBr0Z9y3H+PEqaEmJqlmtrPb/+vSbJauM+nDXbWY/Pjrjq3hgBH7P5DHx030eKDxGSttq8T1wRVzm+J++Sd3M6jrybz7vo3TbdnvLUonHlP4d8Rb4iX5Xr7m+Fj8fCOX2G+X4O/ZLQ31qUP4irx4mr54rzoXqZuELvpvWx8q5Zynz48MGa1dqur6/d4eGhm0wm9ndNYD89PS3u18AYfbCeUG85zn9uOs196Hy5/fEUW+6D9VC8ElcBz+Aj5DPyVZfPwQf4yPE9+AAfqd6DP+CPtB6AP+AP+KNcL4MP8AE+wEdNPw19hb5CX5X7zauEDxuaqqfL06nB8asanz59Ku73x+qVQBXrtcfp3+tz/TR3/3qQ//8t19M3xbb2ergP1iONc+Iq4Bl8gA/w8Xg8GFvKwIOz3yt5l7yb6jZ04v/sARj5vS4TPvy1oNudvaXbUrfE9RL6Cn2Fvno8fQV/BKsJ4oq4GuIl9NVy6iv6osuld790Pazhvre3505OTkzQp1Pn9fR6OjU2nsrOVGGmCq/yVGFhAnw4sykSabfmhzh/qFkg+6iaaeW5vHN+fm7Hsh6sB3E1z8vg48xmuWgjX6FL0CX7RV0PPsAH+AAfNXU/ur3ri6Cv0FdeX1J/UH/QFy3zJ/jI48MsZdRI294OU8WPjsLU4IODbtpsaT/TeJnGm5vGq6YHceVsiK2Sj4YOt+DK406zEUTy3lt7CI+5/UxBX+4p6Frfmmn2cfwQV+Rd8i66ZEi3wR9B18KD8OB4PLYf5NBX83Ud+AAf4GM2P8T8Cj7AB/j4/vAhvaA3Zlr6N9Tn1Oe5+twa7nod9K+//jIh+vr1a/Nvj8lFvuml/XpiVQ1FEVPLcWrG6nP9q1964sY3afvO54+Lryd+PZ/7YD2Iq1m8go+Qz8hX5N2UX8AH+Ej1Drok6EHwAT7Ax8tifQI+wAf4AB81/QvqD+oP6g/6iUN9SuqP1aw/zFJGYqHW6zz1apShvZrgatT3eb2XPB4VeDq/Brd6j8pWL/jYn4r7YD0UP8RV8PADH8HDj3z10mZtgI/F8JF6r3tsEVfEVapbyLvk3dQ7lbybz7ulvKraoK8eIO+Sd78k78LnZa9vdOLsTDr4HD6Hz4PX+Zfk3VKfDj6Hz1c5rqzhrifUc95UNZ7uEsRKQprK3udpVPKCv7+/t4a7T2SLeETH1859sB6aMUBcdV7GKbbBB/gAH8GLEnzMes3B528d+AAfJZ4EH+AjnmEFf8AfubqX+oP6IzerCv6AP+CPWX0Z50/wAT5WGR9mKbO7u2t2MNr0usvp6anb2tqyJvzV1ZU7PDws7pewkOjUIMOW4/znahCJziPPo5rz5a7n4uLCrp37GFmjmfU4tAE3xFWHZ/AR8hn46PI5+Ah8Bz7AR6p3wAf4yOlh+OPr8Ye+/+vra+qPnvqLOmq+PoXP4XP4vNy/AR/g43vAR41+oJ9If7evv/3Y+soa7rJzqfVsTz3U9fi/GrwqRFq919Xk15PxaozKjmYRD3ldj15D0cZ9OHv9lvUgrmI8g4/gGQc+uhkd5N3gOQo+wEfqqQg+wEfOkxf+gD/SWVXwB/wBf5RnwIEP8AE+wEdNfw99hb5aZX1lljIbGxtTSxc16jTA9PLy0hrhOzs77uHhobjfHyuP9pbj/OfKQ0/nvL29rTpf7nru7u6s4c59jKbfAetBXPmZCOAj5DPyVZfPybvBi7CED711pULhKXhQHoba4MF5fUG+Il+l+pN8NZyvWvT3Y/Ggx+pQnYBup47qqyPhQXiQPkO5zwI+wAf4AB/xDIEhvUcdtXx1lDXcZQdzcnJiDQB5qOuVo3///dcaDfpPVi2l/Vp0BUHrcfHnquGuZFJzvtz16PVvbdzHyH70YD26eCWuOjyDj5DPwEfI5+Dj2+FDbyFpgwfn9QX5inyV05/kq2+Xr0rr8fHjx7k8VltHgHNwDs7LdTb4AB/gA3zU9MWoa6lr0z4t/LF8/GGWMmrQbm9vW/GvJ4uOjo7ceDw2IX1wcOCOj4+L+/UUoJoHNzc3Tcf5z93c3LTzquFec77c9cT2GdwH66F4Ja4CnsFHyGfkqy6fgw/wkeN78AE+Ur0Hf8AfaT0Af8Af8Ee5XgYf4AN8gI+afhr6arn1lfqSeiOZvmh9X5j+7nzfXDi3hrvsR1KPrVpPdz3hooa7Gt2p906Np7sCWcSkX+hynpmvX3eeRn3Xo9eNtXEfzrEe/7NZAsRV8IwDHyF/gA/wkfIS+AAfqb6AP+CPnOco/AF/wB/Bkzqtz8AH+AAf4GOon4S+Ql+hr5wr9TfBx2riwyxlVGz6BVbTWgNMY68gDTIo7ddAFP1bBU7LcTqnPleJWX/WwFPvOd13Pn9cfD3xtXIfrAdxNYtX8BHyGfmKvJvySws+4uZ8zHfEFXH1JXGFvuoevCjpT3Ri+H5a8tVziatSXs3pffLufH0GPhbDB3xO3u3jHfoM+fhA76J30bv0RYf6reiSWV1iDXc9oS7S0aY/7+/vz3i2n52dFfdLEKsAWF9fbzpO3tL63Pv7e2u4+yLC//+W64n/bctx8X1yH6xHGufEVcgD4AN8gA94cIhf4fOgo+AP+COefYTenc2f4AN8gI8wOw19hb5CX9X3oeAP+AP+gD/8zKDn0Kce/fLLL5/lNxdPtNUN1P5dDXoN9vCvZNcet7GxMbWQ+eGHH6YNff//Wz5H/vHauI/uBxPWw9kPOMSVc8IT+Aj5DHyQd1N+Ax/gI9Ub8MedaSr4o8uXPj7gD/gD/ijXh+ADfIAP8DHUv0Ffoa+8vkRfzecL8LGa+Bj9+eefn+W/ruEO3lNJoqn27/JeV8NCJvktx8mCRv/+3bt31iiX998i59fn6LUF/drJfTjzwmc9iKsYv+Aj5DPwsfp5VzwSe0gO8VIJHy2fQ1ytflwNxVG6n7z7tHm3BZ/oxA6fXheQr8hXzyVfgXPqc/LV6ucrcA7Owfnq4/y7rqM0NFW/pqhZnZuq7afNlvb/+OOPJuTV5G2Z4ssU2/wUWz84lvUgro6Pj+3HKOHy4eHBfpAawmNuP1PQl3sKOnkX/gDnQX+Qr8hX4/F4hu/AB/jI8ST1R1dHgA/wAT5G9oNirg8BPsAH+AAfpfwQ99vS+kNCVF7lLf1NdAm6RLosjauRGu4aVDU0Vbq0n6n0TKVnKj1T6YfyB1O3V3PqtsQI/OHsDS398CyCnUwmU4s1/Zid5sfcfvABPvwbfnF8EFfoK/QV+gp9FfhhiF/jQahD3xt1bf/3ii5Bl6BLnNMbcuj2/rqGvBu+H3Q7uj2n221oqn7RGZo2+//ZOxueOI5mC/fmEi5eI5tPh2CJfxQlSpT/LyGB8AJxYriAIY6vTo16u7e3e6Z7zef6GenVa2c8OzPbdeqcqp05VdrPtGqmVTOtmmnVQ/mDadWz06olYsm7zr00/ohF5ZcvX6bDvl/afcDnAY/xOgqT4jPyFflKT8Tpx0RwHvLcQ+GjlFf9+chX5KscHnPxoR+5a/UVfE5cPURcka+IK+JqXj/B5119Qb+kPz/E308Lnz/3vGsNdz2ZNzQdu7RfANIXsrq66nZ3w9Tk9+/fu76psX7/7e2tNZ78l1p7XGlCNffBeijuiKsw7Z1p7kxzZ5o709zVhBjiV/i80y3wB/whvEgbx7oWfICPtK5BX6Gv0FfoK/TVsL6k7zPfF4M/4A/44/vgD7OU2d7edqlnZq2HuApTFSU7OzvmJVh7nPdSOjs7s2a/PI8W8TKWZ/X5+bk90cB9jKxRwHocOOIqeOCCj5CXwEfncQk+wEduJgT4AB+pVyX8AX+kuv4x+KPGO5V8Rb4iX4WZH+kMJ/ABPsAH+Bjq7z0Gn9f09+4zX3DwLQYAACAASURBVNXoh5ynO3oXvftQetca7p8/f272nPXed3o9Qg1eAaXGqzb1zNOT8QKiXtvNeYXVeODqdX5t3Iez11VYj4m9cUFcOXtzBHwED1jw0XkRgo/XxhngY9ZjHnyAj9SrFP6AP9DtZa9r8AE+wAf4qOlfoK/QV+irshc8+AAfy4wPs5RZW1ubWrroldmVlRWb9q2G5ebmpru7uyvu98fK67LlOP+58tDTOa+vr6vOl7uem5sba55wH6Ppd8B6EFfexxJ8hHxGvuryOXk3+LyW8KG3rlRIPwQPymtOGzw4ry/IV+SrVH+Sr4bzVYv+vi8e9FgdqhPQ7dRRfXUkPAgP0mco91nAB/gAH+Aj9jMf0nvUUc+vjrKGu+xgYg8hvVKhp6S1qdkgq5bSfi26gkDeXC3HxZ+rhrum+tacL3c9eh1GG/fh7EcP1qOLV+Kq89QDHyGfgY+Qz8EH+Eh5G3yAj1TvwR/wR64egD/gD/ijXPeCD/ABPsDHUF8MfYW+Ql+V+83LhA+zlFGDttV73XtC6SlABcvV1ZV5Aw95RaX7x+OxNfX1613OUzbnsZR6xUvYeHsA7oP1UHwQVxvTmQjgI+Ql8tWezdoAH+Aj56kIPsAH+qo8iwh8gA/wAT6G6kz0FfoKfTUyp4RcXwh8gI+Xgo9FvODRiejEnE60hrvsR5QY/Svueo0+9iXc2toq7teT6Wq4S4C0HCe/dX2uXq0X8PRknfdh7zufPy6+Xr1urI37cPamAOtBXIGPfD4DH+TdlF/gj8D34AN8gA/07lA9gG4P+gL+gD/Sehl8gI9cPwV9hb5CX6Gv0Ff1/eZl0ldmKaMnYL1AUNNaA0xjryANMijtl9DQv1USaTlO59TnqkmvP6uR7j2n+87nj4uvJ75W7oP1IK5m8Qo+Qj4jX5F3U35pwUdM/jHfEVfE1bfEFfqqe/CipD/RieH7aclXLyWuSnk1p/fJu/P1GfhYDB/wOXm3j3foM+TjA72L3kXv0hcd6reiS2Z1iTXc9US0SEeb/ry7uzvj2X56elrcL0GsAmB1dbXpOHmb6XNvb2+t4e6LCP/fW64n/rctx8X3yX2wHmmcE1chD4AP8AE+4MEhfoXPg46CP+CPePYRenc2f4IP8AE+jqzuztW94AN8gA/wUcoP9K/m+5TUH9QfuT72c+lfmaWMhmS1eq97DzsFuJr0GliaetbUeLrLEF/CQp5HOU+nvb3OC6jPM09DKbRxHyP7AYP1OLBBocSVc8IP+Aj5A3x0norg42Q6MwR8gI9UX4AP8JGbKQR/PB5/1Hinsh6Ptx419RnrwXqkdT/6Cn2Fvgqe7eCjPOvxPvmjRj/kZgyQr8hXD5WvrOEuO5d4sOK7d+/M6kWbrGImk0lxv14rUYNXQGk5zn+unoyXkNNrojXny11P7J/HfbAeilfiKgwSBh8hn5GvunwOPsCHHzQe8zb4AB+p3oM/4I+0HoA/4A/4w9mb2bm6F3yAD/ABPkr5Ie5Toa/QV+ircr95mfAx+uOPP77q6fKbmxtreL969cqa6/6J959//tldXFwU96+vr9v+N2/eNB3nP/fTp0927OXlpZ1/6Hy5/Wr2a+M+nGM9unglrgKewUfIZ+Ajjw+9CaEc3MIDzzWudB/X19fVfPZc76OPd+FBdElNfCwzD4JzdPuifO75bpnxUZMfYr5/rjwIzsH5ojinzzCvk8A59WDa34MH2/olQ/Uy+Yo+XKq/lHdHv/3229fxeDx9wlxPuGnzT5wP/V2NDf2ar/+1HOc/Vxeh/6nRr23ofLn9V1dXdiz34azRxHo4iyniqsMT+Aj5DHx0+R18dG9UgY9Zvgcf4CPVcfAH/JHWA/AH/JGr1+AP+AP+KPdPwAf4AB/gY6i/ir5aTn1lQ1P1RLueIpC1i/6s5veHDx+sIfHTTz9ZE7e0P55W3XKc/9x0iu3Q+XL7/cBV7sM51qOLV+Iq4Bl8hHwGPsBHymct+Ij/bcx3xBVx9S1xhb7q15/w+XLzeSmvDtUD5F3y7rfkXficvFvb92jRifA5cUVc0U+UBTZ90ddOtuU1/eRl7+9aw10e7PFE9Hj6cW56OtORmY6cTk+P46f0Z+JqPm6GcMfUbaZui6iUo8m75F3y7pEJN3TJe3d6ejqo2+AP+AP+mOdPnz/AB/gAH+Aj1dc5fUFdG/S3Hm7UDxB6m76lrifvzus24oq4Sus6dMly6hIbmrq9vT31bJc30eFhmFKbm+Ib77/PqcJKPEPnY6rwR5dOuWY95uP17OzMmhHy9Seuzq1JBc5HZqUicb2zs9OU53zeIa5OLJbEE0xzZ5r7Q01zJ19tGG+Rrzq9Q979/vKucsDff/+N3j04GKzPwMf3h4+augb+gD/SfgG6Hd3+Pej2Gv1AP5F+Yl8/9b77PtZw1+P+8dTk3NT10n554KqBJWJvOU6vWkwmE/uFVMLBexr5/95yPcs0xVZJgvXovJWJK/DBNPfX1uBOp92DD/hD/Ak+wEcuP6CvuvwAPsAH+JjXD7k6izrqnVlRem966g/qD/gD/oA/4I+0/h7qU1KfU5/n6g+zlFlbW7OGjjYRjIJFT5aoEf727Vtrhpf2+2P171qO85/7zz//2Dnlw1dzvtz13Nx0E4a5j9H0O2A9iCuPZ/AR8hn5qsvn5N3Ad0+BD33/2uDBeX3xFOuR6h3w8bT4YD3K+vs54cNfy1CdgG6njuqrI+FBeJA+Q7nPAj7AB/gAHy192uekE+nvvnZaD2u4y16hzxu2z6NrZWXFmvFDnqp9+9Vw//fff60BUeOlll6PXqfUxn04x3oEr2viqvM6Bh/B6xh8gA/4ozxLAnyAD/ABPmrqAfQV+iqt6+AP+AP+gD/gj7pZQ9Tns5728Af8scz8YZYyapinnk4nJ8ETr8/TXR7ZapJfXV31ei2WvNnH47E92X53dzf1Bm7xkNfnxq99cR+sh+KHuOo8gMHHrEcZ+aqb0QE+wEfOAxZ8gI9Uf6Gvgoc2+AAf4KM84wt8gA/wAT6G+jDUH9QfL6X+WMQLHh6EB3M8aA13va6kxpy2zc1N87GL/fzkV1TaryfT1XBXgm05Tj7l+lz/6rB+2fL+5X3n88fF1xO/ns99sB7E1SxewUfIZ+Qr8m7KL+ADfKR6B10S9CD4AB/g43+L9Qn4AB/gA3zU9C+oP6g/qD/oJw71Kak/lrP+MEsZPcFU69meejFKaOgJeSWRGo++1INITXqdX430RTzkdT3xZ3IfrIdmCRBXwQMWfATvN/JVN6MDfCyGj9R73WOLuCKu0hk25F3yLnq3PBsq/m5KeXVophR5l7z7LXkXPu+f1YZOXEwnlvoQ5Cvy1bfkK+KKfFU765L64/nVH9Zw1xPqWsRFPNQliLWwq6urrs/rveTNfnt7aw13HxyLeMHH1859sB6np6eOuAp4Bh/BU5F81XnmgQ/wkeN78AE+lB9KnIG+Ql/BH+CjlAfgD/gD/pjND3FfBHyAD/ABPoZ0NPX5ctbnZimzvb3tJpOJNdzfvXvnjo+P3fr6uhVdFxcXbn9/v7hfgaFmugaWthznP1cDHXUeeR7VnC93Pefn53bt3MfIGmmsx74NCiWuOjyDj5DPwEeXz8FH4DvwAT5SvQM+wEdOD8Mfj8cf+v4vLy+pP3rqL+qo+foUPofP4fNy/wZ8gI/vAR81+oF+Iv3dvv72fesra7jLziUejKUmnV4l0yarGDXjS/v1eowavCpEWo7zn6sn49UYlR1Nzfly1xP753EfrIfilbh6bXjSmyPgI+Qz8lWXz8EH+PD5IeZt8AE+Ur0Hf8AfaT0Af8Af8Eenr3N1L/gAH+ADfJTyQ9ynQl+hr9BX5X7zMuHDLGXW1tZmfNBTj6k+b3Z/rLzUW47zXvDy0FPyub6+tsZ76hE/5OGo/Tc3N9Zc5D5G0++A9SCu/EwE8PE/0xkV5Ku39uMmeTd4Cz8FPlLvWNbjadcj9cZkPViP3Ewh+GOWP3zuRLdTf6h+Ax/oq7Rf8BT6Cj4ve12zHtSD4AN81PRb4fP75XNruMsO5ujoyJrWrR7qKysr1qxvPS72dFfTXNO7F/GQ13n1+rc27sM51uPrdJYAcdXhGXwEzzjwAT7SWSPgA3zkPBXhD/gj1bXwB/wBf4SZOOCj86SGP8oz4NBX6CvwAT5q+nvoK/TVMusrs5RRw3xjY8NEw8ePH93BwYE7OTmxJvbe3p47PDws7pf3uprnV1dXTcf5zx2Px3beu7u7qvPlrid+PYf7YD0Ur8RVwDP4CPmMfNXlc/ABPnJ8Dz7AR6r34A/4I60H4A/4A/4o18vgA3yAD/BR009DXz1vfaXGpN4IoC9a3xemvzvfNxfOR7/++qtZyqhhrk1CQb8yxa+c6Onz0n4V6HpF6c2bN03H6RVUfe6nT5/sNUR5wPtXHPrO54+Lr8fb0XAf3fBZ1oO4Ah/5fAY+yLspv4g/tL169erReDC2lIEHZ/UFfB70F/mKfJXLV96+46l1u39QJqfLh+oIcA7O0zqTejDoVvABPsBHsP54zjxI/2o8tYWmD0cfjv5uV7fM9eF+//33r5pY7IWzhr3oifXav6u5K2GtRn3Lcf48EhW6MIFUW+v59e81SVYb9+Gs2c56fLXkT1x1eAIfIZ+Bjy6/p/hQY09bbd4nrmZ5krjKxxV8ji5RXgEf4COtDxbRJSlPLRpX/nPQid2bxfA5fJ6r358KH/eFc/oM8/2cRfJuquMWzbusB+sR55mXivOhehl8oHdzetcsZT5//mzNam2Xl5duf3/fTSYT+7smsB8fHxf3a1CqPli/8LUc5z83neY+dL7c/niKLffBeiheiauAZ/AR8hn5qsvn4AN85PgefICPVO/BH/BHWg/AH/AH/FGul8EH+AAf4KOmn4a+Ql+hr8r95mXChw1N9ZNoRRDymVHRnU4xls97bj9TbO93iq2+Y9ZjNP0O9GpKSzzq33/58sXJskGx7F/L9P+9FMe5/UxzZ5o709yZ5v4Q09xjSxny1RcbvO71BXmXvEvefRl512MVfYVu9zZHyuXoduoP+Hy+n0K/hPpceh+9Sz+xr99K/2o560FruO/s7LijoyMreNOp83p6PZ0aG09lZ6owU4WXeaqwMAE+nNkUSSS05oc4f+gHENn8aBvKK7n9Z2dndizrwXoQV/O8DD5ObQ6MNvIVugRdslvU9eADfIAP8FFT96Pbu74I+gp95fUl9Qf1B33RMn+Cjzw+zFJGjbSNjTBVnGm8TOP1Xs5+2nBLfDB1+3lP3ZZoqJmeHucBeZYpiWoIREt+YFp1flq1moLkXWdDnomrQ5uBIlySdz/am0ng48S+A/DR5U/wEfQ5+EBfnZx0+QF9hb5SfizVZ/AH/JHGB/wBf8Afs/wZ588UH+JZvXHZ0vcg75J3c3nXGu56/e+vv/4yAbe1tWX+7XHQyTe9tF9PrKphooBtOU7NJn2uf3VYT9z4IrvvfP64+Hri1/O5D9aDuJrFK/gI+Yx8Rd5N+QV8gI9U76BLgh4EH+ADfIQfAeEP6sGhOhP+gD9y/RTqD+oP+AP+gD/q+83LVH+YpYzEdK1ne+rVKEN7NcHVqE+9qWq8txV4Or8Gty7iIa/rif2wuA/WQ3FHXAUPWPARPPPIV92MDvCxGD5S73WPLeKKuEr1DnmXvJvOjCHv5vNuKa8OzZQi75J3vyXvwuflWRHoxNlZdvA5fA6f573X6cN1swmoB2fjA707q3et4a4n1HPeVDWe7hLECjJNZe/zNCp5Nt/e3lrD3QfqIh7R8bVzH6yHZgwQV52XcYpt8AE+wEfwogQfs15z8Pl7Bz7AR4knwQf4iGdYwR/wR67upf6g/sjNqoI/4A/4Y1ZfxvkTfICPZcaHWcpsb2+bHYw2ve5yfHzs1tfXrQl/cXHh9vf3i/slLCQ6Nciw5Tj/uRpEovPI86jmfLnrOT8/t2vnPkbWaGY99m3ADXHV4Rl8hHwGPrp8Dj4C34EP8JHqHfABPnJ6GP54PP7Q9395eUn90VN/UUfN16fwOXwOn5f7N+ADfHwP+KjRD/QT6e/29bfvW19Zw112LrWe7amHul47U4NXhUir97qa/HoyXo1RvY6xiIe8rkevlWrjPpzZRbAexFWMZ/ARPOPARzejg7wbPHnBB/hIPRXBB/jIzRSCP+CPdFYV/AF/wB9lT17wAT7AB/io6e+hr9BXy6yvzFJmbW1txgddA0w/fvxojfDNzU13d3dX3O+PlYdTy3H+c+Whp+bg9fV11fly13Nzc2MNd+5jNP0OWA/iys9EAB8hn5GvunxO3g1ecyV86K0rFQoPwYOaNaINHpzXF+Qr8lWqP8lXw/mqRX/fFw96rA7VCeh26qi+OhIehAfpM5T7LOADfIAP8BHPEBjSe9RRz6+Osoa77GCOjo6sASAPdb1ypKektanZIKuW0n4tuoKg9bj4c9Vw1/TumvPlrkevf2vjPpz96MF6dPFKXHV4Bh8hn4GPkM/BB/hIeRt8gI9U78Ef8EeuHoA/4A/4o1wvgw/wAT7Ax1A/DX2FvkJflfvNy4QPs5RRg3ZjY8Oa63qy6ODgwJ2cnFgTe29vzx0eHhb36ylABcvV1VXTcf5zx+OxnVe/3tWcL3c9sX0G98F6KF6Jq4Bn8BHyGfmqy+fgA3zk+B58gI9U78Ef8EdaD8Af8Af8Ua6XwQf4AB/go6afhr563vpKfUm9kUxftL4vTH93vm8unFvDXfYjqcdWrae7nkxXw12N7tR7p8bTXYEsYtKTdTnPzK2tztOo73r0urE27sPZmwKsxztLkMSVc8IP+Aj5A3z8n83aAB/BUxF8gI9UX4AP8JHzHIU/4I+0roE/4A/4I8z8AB/Bsz3tX8Af8Af4AB9D/Vbqj+WsP8xSRmLBL7Ca1hpgGnsFaZBBab8GoujfilhajtM59bkKPP1ZA0+953Tf+fxx8fXE18p9sB7E1SxewUfIZ+Qr8m7KLy34iJsrMd8RV8TVt8QV+qr7gbykP9GJ4ftpyVcvJa5KeTWn98m78/UZ+FgMH/A5ebePd+gz5OMDvYveRe/SFx3qt6JLZnWJNdz1RLRIR5v+vLu7O+PZfnp6WtwvQawCYHV1tek4eZvpc29vb63h7osI/99brif+ty3HxffJfbAeaZwTVyEPgA/wAT7gwSF+hc+DjoI/4I949hF6dzZ/gg/wAT7C7DT0FfoKfVXfh4I/4A/4A/7ws0dfQp969Msvv3yV31w80VY3UPt3Nej/+++/qeVL7XFra2tTC5kffvhh2tD3/73lc+Qfr4376H4wYT2c/YBDXDknPIGPkM/AB3k35TfwAT5SvQF/3Jimgj+6fOnjA/6AP+CPcn0IPsAH+AAfQ/0b9BX6yutL9NV8vgAfy4mP0Z9//vlV/usa7uA9KyWaav8u73U1LGSS33KcLGj07z98+GCNcnmbLXJ+fY5eW9CvndyHMy981oO4ivELPkI+Ax/Ln3fFI7FH3hAvlfDR8jnE1fLH1VAcpfvJuw+bd1vwiU7s8Ol1AfmKfPVS8hU4pz4nXy1/vgLn4BycLz/Ov+s6SkNT9WuKmtW5qdp+2mxp/48//mhCXk3elim+TLHNT7H1g2NZD+Lq8PDQfowSLu/u7uwHqSE85vYzBf15T0En78If4DzoD/IV+erk5GSG78AH+MjxJPVHV0eAD/ABPkb2g2KuDwE+wAf4AB+l/BD329L6Q0JUXuUt/U10CbpEuiyNq5Ea7hpUNTQ1t7SfqdtM3WbqNlO3h/IHU7eXc+q2xAj84ewNLf3wLIKdTCZTizX9mJ3mx9x+8AE+/Bt+cXwQV+gr9BX6Cn0V+GGIX+NBqEPfG3Vt//eKLkGXoEuc0xty6Pb+uoa8G74fdDu6PafbbWiqftEZmjZb2s+0aqZVM62aadVD+YNp1bPTqiViybvOvTT+iEXlly9fpsO+X9p9wOcBj/E6CpPiM/IV+UpPxOnHRHAe8txD4aOUV/35yFfkqxwec/GhH7lr9RV8Tlw9RFyRr4gr4mpeP8HnXX1Bv6Q/P8TfTwufP/e8aw13PZk3NB27tF8A0heyurrqdnfD1OT379+7vqmxfv/t7a01nvyXWntcaUI198F6KO6IqzDtnWnuTHNnmjvT3NWEGOJX+LzTLfAH/CG8SBvHuhZ8gI+0rkFfoa/QV+gr9NWwvqTvM98Xgz/gD/jj++APs5TZ3t52qWdmrYe4ClMVJTs7O+YlWHuc91I6OzuzZr88jxbxMpZn9fn5uT3RwH2MrFHAehw44ip44IKPkJfAR+dxCT7AR24mBPgAH6lXJfwBf6S6/jH4o8Y7lXxFviJfhZkf6Qwn8AE+wAf4GOrvPQaf1/T37jNf1eiHnKc7ehe9+1B61xrunz9/bvac9d53ej1CDV4BpcarNvXM05PxAqJe2815hdV44Op1fm3ch7PXVViPib1xQVw5e3MEfAQPWPDReRGCj9fGGeBj1mMefICP1KsU/oA/0O1lr2vwAT7AB/io6V+gr9BX6KuyFzz4AB/LjA+zlFlbW5tauuiV2ZWVFZv2rYbl5uamu7u7K+73x8rrsuU4/7ny0NM5r6+vq86Xu56bmxtrnnAfo+l3wHoQV97HEnyEfEa+6vI5eTf4vJbwobeuVEg/BA/Ka04bPDivL8hX5KtUf5KvhvNVi/6+Lx70WB2qE9Dt1FF9dSQ8CA/SZyj3WcAH+AAf4CP2Mx/Se9RRz6+Osoa77GBiDyG9UqGnpLWp2SCrltJ+LbqCQN5cLcfFn6uGu6b61pwvdz16HUYb9+HsRw/Wo4tX4qrz1AMfIZ+Bj5DPwQf4SHkbfICPVO/BH/BHrh6AP+AP+KNc94IP8AE+wMdQXwx9hb5CX5X7zcuED7OUUYO21Xvde0LpKUAFy9XVlXkDD3lFpfvH47E19fXrXc5TNuexlHrFS9h4ewDug/VQfBBXG9OZCOAj5CXy1Z7N2gAf4CPnqQg+wAf6qjyLCHyAD/ABPobqTPQV+gp9NTKnhFxfCHyAj5eCj0W84NGJ6MScTrSGu+xHlBj9K+56jT72Jdza2iru15PparhLgLQcJ791fa5erRfw9GSd92HvO58/Lr5evW6sjftw9qYA60FcgY98PgMf5N2UX+CPwPfgA3yAD/TuUD2Abg/6Av6AP9J6GXyAj1w/BX2FvkJfoa/QV/X95mXSV2YpoydgvUBQ01oDTGOvIA0yKO2X0NC/VRJpOU7n1OeqSa8/q5HuPaf7zuePi68nvlbug/UgrmbxCj5CPiNfkXdTfmnBR0z+Md8RV8TVt8QV+qp78KKkP9GJ4ftpyVcvJa5KeTWn98m78/UZ+FgMH/A5ebePd+gz5OMDvYveRe/SFx3qt6JLZnWJNdz1RLRIR5v+vLu7O+PZfnp6WtwvQawCYHV1tek4eZvpc29vb63h7osI/99brif+ty3HxffJfbAeaZwTVyEPgA/wAT7gwSF+hc+DjoI/4I949hF6dzZ/gg/wAT6OrO7O1b3gA3yAD/BRyg/0r+b7lNQf1B+5PvZz6V+ZpYyGZLV6r3sPOwW4mvQaWJp61tR4ussQX8JCnkc5T6e9vc4LqM8zT0MptHEfI/sBg/U4sEGhxJVzwg/4CPkDfHSeiuDjZDozBHyAj1RfgA/wkZspBH88Hn/UeKeyHo+3HjX1GevBeqR1P/oKfYW+Cp7t4KM86/E++aNGP+RmDJCvyFcPla+s4S47l3iw4rt378zqRZusYiaTSXG/XitRg1dAaTnOf66ejJeQ02uiNefLXU/sn8d9sB6KV+IqDBIGHyGfka+6fA4+wIcfNB7zNvgAH6negz/gj7QegD/gD/jD2ZvZuboXfIAP8AE+Svkh7lOhr9BX6Ktyv3mZ8DH6448/vurp8pubG2t4v3r1yprr/on3n3/+2V1cXBT3r6+v2/43b940Hec/99OnT3bs5eWlnX/ofLn9avZr4z6cYz26eCWuAp7BR8hn4COPD70JoRzcwgPPNa50H9fX19V89lzvo4934UF0SU18LDMPgnN0+6J87vlumfFRkx9ivn+uPAjOwfmiOKfPMK+TwDn1YNrfgwfb+iVD9TL5ij5cqr+Ud0e//fbb1/F4PH3CXE+4afNPnA/9XY0N/Zqv/7Uc5z9XF6H/qdGvbeh8uf1XV1d2LPfhrNHEejiLKeKqwxP4CPkMfHT5HXx0b1SBj1m+Bx/gI9Vx8Af8kdYD8Af8kavX4A/4A/4o90/AB/gAH+BjqL+KvlpOfWVDU/VEu54ikLWL/qzm94cPH6wh8dNPP1kTt7Q/nlbdcpz/3HSK7dD5cvv9wFXuwznWo4tX4irgGXyEfAY+wEfKZy34iP9tzHfEFXH1LXGFvurXn/D5cvN5Ka8O1QPkXfLut+Rd+Jy8W9v3aNGJ8DlxRVzRT5QFNn3R10625TX95GXv71rDXR7s2pQg9Od4+rGfnl7aPzT9dehzS1OFh46L98fT3FuOa5nyPPS53EeIH9YjTM8GH+/d6empPVHtvwthaSiv5PYTV8TV0dGRcZWPj8eOqzgG4Y8Qj0+1HuiSft322PhgPViP2joizqWlvDqkE9BX6Cvpy0XrQficfLVIvhrqB8CDxBVxRT9RbhMt/VTqqE7PLMrnzz3v2tDU7e3tqWe7vIkOD8OU2twU33j/fU4VVoIaOh9ThT+6dMo16zEfr2dnZ/YDknz9iatzS2DgfGSNf/2ouLOz05TnfN4hrk4slsQTTHNnmvtDTXMnX20Yb5GvOr1D3v3+8q5ywN9//43ePTgYrM/Ax/eHj5q6Bv6AP9J+Abod3f496PYa/UA/kX5iXz/1vvs+1nDX4/7x1OTc1PXSfnngqoElYm85Tq9aTCYT8xuXcPCeRv6/t1zPMk2xVZJgPTpvZeIKfDDN/bU1uNNp9+AD/hB/gg/wkcsP6KsuP4AP8AE+5vVDrs6ijnpnVpTa0Ffo/iwIBgAAIABJREFUK/hjlj/j/g74AB/gA3wM9Wnp7wb9LX1lljJra2vW0NGmL1DJVE+WqBH+9u1ba4aX9vtj9e9ajvOf+88//9g55cNXc77c9dzcdBOGuY/R9DtgPYgrj2fwEfIZ+arL5+TdwHdPgQ99/9rgwXl98RTrkeod8PG0+GA9yvr7OeHDX8tQnYBup47qqyPhQXiQPkO5zwI+wAf4AB8tfdrnpBPp7752Wg9ruMteIfXGHfJU9PtXVlasGe+9h2qPi73i1XD/999/rQFR8pDv+1y9TqmN+3CO9QgzCIirzusafJzaj3nawAf4SD31wAf4yOkL+AP+SHUt/AF/wB/zszvQV52+pK6dnwGHvkJfoa/m84PyBfV559kNf8Afpf7vMvGHWcqoYZ56Op2cBE+8Pk93eWRLZFxdXfV6LZa82cfjsYHt7u5u6g3c4iGvz41fa+A+WA/FD3HVeQCDj1mPMvJVN6MDfICPnAcs+AAfqf5CXwUPbfABPsBHecYX+AAf4AN8DPVhqD+oP15K/bGIFzw8CA/meNAa7npdSY05bZubm+ZjF/v5ye+vtF9PpqvhrgTbcpx8yvW5/tVhPTnk/cv7zuePi68nfj2f+2A9iKtZvIKPkM/IV+TdlF/AB/hI9Q66JOhB8AE+wMf/FusT8AE+wAf4qOlfUH9Qf1B/0E8c6lNSfyxn/WGWMnqCqdazPfVilNDQE/JKIjUefakHkZr0Or8a6Yt4yOt64s/kPlgPzRIgroIHLPgI3m/kq25GB/hYDB+p97rHFnFFXKUzbMi75F30bnk2VPzdlPLq0Ewp8i5591vyLnzeP6sNnbiYTiz1IchX5KtvyVfEFfmqdtYl9cfzqz+s4a4n1BfxXvcNci2sptGm3oaxN1PJm/329tYa7j44FvGCj6+d++i+S9aDuPKeWOAjeI76XAM+wAf4mJ+ZAj46T0l0SfCUhD/gj3TGE/gAHznPVfgD/kjrfvgD/oA/juyB0lx/C3yAD/DxfeDDLGW2t7fdZDKxhPDu3Tt3fHzs1tfXrQl/cXHh9vf3i/slvNVM18DSluP858oQX+eR51HN+XLXc35+btfOfYysUcB67NugUOKqwzP4CPkMfHT5HHwEvgMf4CPVO+ADfOT0MPzxePyh7//y8pL6o6f+oo6ar0/hc/gcPi/3b8AH+Pge8FGjH+gn0t/t62/ft76yhrvsXOLBWGrS6VUybbKKUTO+tF+vx6jBq0Kk5Tj/uXrSVI1R2dHUnC93PbF/HvfBeiheiavXhie97QA+Qj4jX3X5HHyAD58fYt4GH+Aj1XvwB/yR1gPwB/wBf3T6Olf3gg/wAT7ARyk/xH0q9BX6Cn1V7jcvEz7MUmZtbW3GBz31mOrzZvfHyku95TjvBS8PPSWf6+tra7ynHvFDHo7af3NzY81F7mM0/Q5YD+LKWz6Bj/+ZzqggX721HzfJu8Fb+CnwkXrHsh5Pux6pNybrwXrkZgrBH7P84XMnup36Q/Ub+EBfpf2Cp9BX8HnZ65r1oB4EH+Cjpt8Kn98vn1vDXXYwqYdQrRf6ysqKNesX8V73nu9qmmt6d84TsOZz9fq3Nu7DOdbj63SWAHHVecaBj9PpjArwAT7SWSPgA3zk9A78AX+k+hP+gD/gj+C5Cz46z3b4I8w0AB/go6afhL5CX8Ef8MfQrM9lqs/NUkYN842NDRMNHz9+dAcHB+7k5MSa2Ht7e+7w8LC4X97rspS5urpqOs5/7ng8tvPe3d1VnS93PfHrOdwH66F4Ja4CnsFHyGfkqy6fgw/wkeN78AE+Ur0Hf8AfaT0Af8Af8Ee5XgYf4AN8gI+afhr66nnrKzUm9UYAfdH6vjD93fm+uXA++vXXX81SRg1zbRIKeoonfuVET5+X9qtA1ytKb968aTpOr6Dqcz99+mSvIcoD3r/i0Hc+f1x8Pd6Ohvvohs+yHsQV+MjnM/BB3k35Rfyh7dWrV4/Gg7GlDDw4qy/g86C/yFfkq1y+8vYdT63b/YMyOV0+VEeAc3Ce1pnUg0G3gg/wAT6C9cdz5kH6V+OpLTR9OPpw9He7umWuD/f7779/1cRiL5w17EVPrNf+Xc1dCWs16luO8+eRqNCFCaTaWs+vf69Jstq4D2fNdtbjqyV/4qrDE/gI+Qx8dPk9xYcae9pq8z5xNcuTxFU+ruBzdInyCvgAH2l9sIguSXlq0bjyn4NO7N4shs/h81z9/lT4uC+c02eY7+cskndTHbdo3mU9WI84z7xUnA/Vy+ADvZvTu2Yp8/nzZ2tWa7u8vHT7+/tuMpnY3zWB/fj4uLhfg1L1wfqFr+U4/7npNPeh8+X2x1NsuQ/WQ/FKXAU8g4+Qz8hXXT4HH+Ajx/fgA3ykeg/+gD/SegD+gD/gj3K9DD7AB/gAHzX9NPQV+gp9Ve43LxM+bGiqn0QrgpDPjIrudIqxfN5z+5lie79TbPUdsx6j6XegV1Na4lH//suXL06WDYpl/1qm/++lOM7tZ5o709yZ5s4094eY5h5bypCvvtjgda8vyLvkXfLuy8i7HqvoK3S7tzlSLke3U3/A5/P9FPol1OfS++hd+ol9/Vb6V8tZD1rDfWdnx9VMlVaAMFWYqcLf01RhxTz4cGZTJJGQ4l9vt+zu7lblD6bSM5Ue/oA/4I9Tm1ejbSh/kncDv8Af8Af8AX/AH/BHLX/G/Ap/wB/wB/wBf8AfT8UfZimjRtrGRpgqzjRepvF6L2c/bbglPpi6/bynbivZ1ExPj/OAPMskXjUEoiU/MK06P61azTbyrrMhz8TVoc1AES7Jux/tzSTwcWLfAfjo8if4CPocfKCvTk66/IC+Ql8pP5bqM/gD/kjjA/6AP+CPWf6M82eKD/Gs3rhs6XuQd8m7ubxrDXe9/vfXX3+ZgNva2jL/9jjo5Jte2q/BlGqYKGBbjlOzSZ/rXx3Wk1y+yO47nz8uvp749Xzug/UgrmbxCj5CPiNfkXdTfgEf4CPVO+iSoAfBB/gAH+FHQPiDenCozoQ/4I9cP4X6g/oD/oA/4I/6fvMy1R9mKSMxXevZnno1ytBeTXA16lNvqhrvbQWezq/BrYt4yOt6Yj8s7oP1UNwRV8EDFnwEzzzyVTejA3wsho/Ue91ji7girlK9Q94l76YzY8i7+bxbyqtDM6XIu+Tdb8m78Hl5VgQ6cXaWHXwOn8Pnee91+nDdbALqwdn4QO/O6l1ruOsJ9UU8bXyDXEGmqey1Xs6xF/zt7a013H2gLuIRHV8799F9l6wHceU9gsFH8Jj3uQZ8gA/wMe8hDj46j0t0SfCYhz/gj3TGE/gAH7kZFPAH/JF6JMMf8Af8cWQPlOb6W+ADfICP7wMfZimzvb1tdjDa9LrL8fGxW19ft79fXl66/f394n55zqphr8GSLcf5zz07O7NmvzyPas6Xu57z83M7lvtw5gHMeuw74irgGXyEfAY+unwOPsBHju/BB/hI9R78AX+k9QD8AX/AH+V6GXyAD/ABPmr6aegr9BX6qtxvXiZ8WMNddi61nu2ph7peO1ODV0+8tHqvq8mvJ03VcNfrGIt4yOt69FqpNu7DmV0E60FcxXgGH8EzDnx0MzrIu8GTF3yAj9RTEXyAj9xMIfgD/khnVcEf8Af8UfbkBR/gA3yAj5r+HvoKfbXM+sosZdbW1mZ80DXA9OPHj9YI39zctKemY++qeL8/Vh5OLcf5z5WHnpqD19fXVefLXc/NzY013LmP0fQ7YD2IK2/5BD5CPiNfdfmcvBu85kr40FtXKhQeggc1a0QbPDivL8hX5KtUf5KvhvNVi/6+Lx70WB2qE9Dt1FF9dSQ8CA/SZyj3WcAH+AAf4KPUh0VfvQx9ZQ132cHEHkJ6hF9PSWtTs0FWLaX9EvkKAnlTtRwXf64a7preXXO+3PXo9T1t3IezHz1Yjy5eiavOMw58hHwGPkI+Bx/gI+Vt8AE+Ur0Hf8AfuXoA/oA/4I9y3Qs+wAf4AB9DfTH0FfoKfVXuNy8TPsxSRg3ajY0Na67ryaKDgwN3cnJiTey9vT13eHhY3K+nABUsV1dXTcf5zx2Px3Ze/XpXc77c9cT2GdwH66F4Ja4CnsFHyGfkqy6fgw/wkeN78AE+Ur0Hf8AfaT0Af8Af8Ee5XgYf4AN8gI+afhr66nnrK/Ul9UYyfdH6vjD93fm+uXBuDXfZj6QeW7We7noyXQ13NbpT750aT3cFsohJT9blPDO3tjpPo77r0evG2rgPZ28KsB7vLEESV84JP+Aj5A/w8X82awN8BE9F8AE+Un0BPsBHznMU/oA/0roG/oA/4I8w8wN8BM/2tH8Bf8Af4AN8DPVbqT+Ws/4wSxmJBb/AalprgGnsFaRBBqX9GoiifytiaTlO59TnKvD0Zw089Z7Tfefzx8XXE18r98F6EFezeAUfIZ+Rr8i7Kb+04CNursR8R1wRV98SV+ir7gfykv5EJ4bvpyVfvZS4KuXVnN4n787XZ+BjMXzA5+TdPt6hz5CPD/Quehe9S190qN+KLpnVJdZw1xPRIh1t+vPu7u6MZ/vp6WlxvwSxCoDV1dWm4+Rtps+9vb21hrsvIvx/b7me+N+2HBffJ/fBeqRxTlyFPAA+wAf4gAeH+BU+DzoK/oA/4tlH6N3Z/Ak+wAf4OLK6O1f3gg/wAT7ARyk/0L+a71NSf1B/5PrYz6V/Nfrll1++ym/u5ubGiH9tbc3+v/bvatD/999/U8uX2uP8edRo/+GHH6YN/dbz69/LP14b99H9YMJ6OPsBh7jq8Aw+Qj4DH11+Bx+B78AH+Eh1C/gAHzk9DH/AH2l9BH/AH/BHuX8APsAH+AAfNf1F9BX6apn11ejPP//8Kv91DXfwnpUK+tq/y3tdhCqT/JbjZEGjf//hwwdrlMvbbJHz63P02oKeBuA+nHnhsx7EVYxf8BHyGfhY/rwrHok98oZ4qYSPls8hrpY/robiKN1P3n3YvNuCT3Rih0+vC8hX5KuXkq/AOfU5+Wr58xU4B+fgfPlx/l3XURqaqqe51KzOTdX202ZL+3/88UcT8mrytkzxZYptfoqtf/qU9SCuDg8P7cco4fLu7s5+kBrCY24/U9Cf9xR08i78Ac6D/iBfka9OTk5m+A58gI8cT1J/dHUE+AAf4GNkPyjm+hDgA3yAD/BRyg9xvy2tPyRE5VXe0t9El6BLpMvSuBqp4a5BVUNTc0v7mbrN1G2mbjN1eyh/MHV7OaduS4zAH87e0NIPzyLYyWQytVjTj9lpfsztBx/gw7/hF8cHcYW+Ql+hr9BXgR+G+DUehDr0vVHX9n+v6BJ0CbrEOb0hh27vr2vIu+H7Qbej23O63Yam6hedoWmzpf1Mq2ZaNdOqmVY9lD+YVj07rVoilrzr3Evjj1hUfvnyZTrs+6XdB3we8BivozApPiNfka/0RJx+TATnIc89FD5KedWfj3xFvsrhMRcf+pG7Vl/B58TVQ8QV+Yq4Iq7m9RN83tUX9Ev680P8/bTw+XPPu9Zw15N58UT0ePpxbno605GZjpxOT4/jp/Rn4mo+boZwx9Rtpm6reFSOJu+Sd8m7R9ZMQZe8d6enp4O6Df6AP+CPef70+QN8gA/wAT5SfZ3TF9S1QX+rcapG2Orq6kxdMqTLyLvzuo24Iq7Sug5dspy6xCxltre3XeqZWeshrsAQWe3s7JiXYO1x3kvp7OzMikZ5Hi3iZSzP6vPzcyvCuY+RYz06Dz/iKnjggo+Ql8AH+Ei9+MAH+Eh1C/wBf+RmpsAfj8cfNd6prMfjrUdNfcZ6sB7oqzCDJZ2pBT7AB/h4HHzU6Iecpzv1IPXgQ9WD1nD//Plzs+es977T6xFquItIarxqU888/UIqIafXdnNeYTUeuHqdXxv34ex1FdZjYr+8E1fOnkIAH8EDFnx0XoTg47VxBviY9ZgHH+Aj9SqFP+APdHvZ6xp8gA/wAT5q+hfoK/QV+qrsBQ8+wMcy48MsZdbW1qZetHpVaGVlxaZ9q2G5ubnp7u7uivv9sfK6bDnOf6489HTO6+vrqvPlrufm5saaJ9zHaPodsB7ElfexBB8hn5GvunxO3g0+ryV86K0rFdIPwYPymtMGD87rC/IV+SrVn+Sr4XzVor/viwc9VofqBHQ7dVRfHQkPwoP0Gcp9FvABPsAH+Ij9zIf0HnXU86ujrOEuO5jYQ0ivVOgpaW1qNsiqpbRfi64gkDdXy3Hx56rhrqm+NefLXY9e/9bGfTj70YP16OKVuOq8jsFHyGfgI+Rz8AE+Ut4GH+Aj1XvwB/yRqwfgD/gD/ijXveADfIAP8DHUF0Nfoa/QV+V+8zLhwyxl1KBt9V73nu96ClDBcnV1Zd7ZrV7w4/HYmvr69S7nmZnzWEq94iVsvD0A98F6KD6Iq43pTATwEfIS+WrPZm2AD/CR8+QFH+ADfVWeRQQ+wAf4AB9DdSb6Cn2FvhqZU0KuLwQ+wMdLwcciXvDoRHRiTidaw132I0qM/hV3vUYf+xJubW0V9+vJdDXcJUBajpPfuj5Xr9YLeHqyzvuw953PHxdfr1431sZ9OHtTgPUgrsBHPp+BD/Juyi/wR+B78AE+wAd6d6geQLcHfQF/wB9pvQw+wEeun4K+Ql+hr9BX6Kv6fvMy6SuzlNETsF4gqGmtAaaxV5AGGZT2S2jo3yqJtBync+pz1aTXn9VI957Tfefzx8XXE18r98F6EFezeAUfIZ+Rr8i7Kb+04CMm/5jviCvi6lviCn3VPXhR0p/oxPD9tOSrlxJXpbya0/vk3fn6DHwshg/4nLzbxzv0GfLxgd5F76J36YsO9VvRJbO6xBrueiJapKNNf97d3Z3xbD89PS3ulyBWAbC6utp0nLzN9Lm3t7fWcPdFhP/vLdcT/9uW4+L75D5YjzTOiauQB8AH+AAf8OAQv8LnQUfBH/BHPPsIvTubP8EH+AAfR1Z35+pe8AE+wAf4KOUH+lfzfUrqD+qPXB/7ufSvzFJGQ7Javde9h50CXE16DSxNPWtqPN1liC9hIc+jnKfT3l7nBdTnmaehFNq4j5H9gMF6HNigUOLKOeEHfIT8AT46T0XwcTKdGQI+wEeqL8AH+MjNFII/Ho8/arxTWY/HW4+a+oz1YD3Suh99hb5CXwXPdvBRnvV4n/xRox9yMwbIV+Srh8pX1nCXnUs8WPHdu3dm9aJNVjGTyaS4X6+VqMEroLQc5z9XT8ZLyOk10Zrz5a4n9s/jPlgPxStxFQYJg4+Qz8hXXT4HH+DDDxqPeRt8gI9U78Ef8EdaD8Af8Af84ezN7FzdCz7AB/gAH6X8EPep0FfoK/RVud+8TPgY/fHHH1/1dPnNzY01vF+9emXNdf/E+88//+wuLi6K+9fX123/mzdvmo7zn/vp0yc79vLy0s4/dL7cfjX7tXEfzrEeXbwSVwHP4CPkM/CRx4fehFAObuGB5xpXuo/r6+tqPnuu99HHu/AguqQmPpaZB8E5un1RPvd8t8z4qMkPMd8/Vx4E5+B8UZzTZ5jXSeCcejDt78GDbf2SoXqZfEUfLtVfyruj33777et4PJ4+Ya4n3LT5J86H/q7Ghn7N1/9ajvOfq4vQ/9To1zZ0vtz+q6srO5b7cNZoYj2cxRRx1eEJfIR8Bj66/A4+ujeqwMcs34MP8JHqOPgD/kjrAfgD/sjVa/AH/AF/lPsn4AN8gA/wMdRfRV8tp76yoal6ol1PEcjaRX9W8/vDhw/WkPjpp5+siVvaH0+rbjnOf246xXbofLn9fuAq9+Ec69HFK3EV8Aw+Qj4DH+Aj5bMWfMT/NuY74oq4+pa4Ql/160/4fLn5vJRXh+oB8i5591vyLnxO3q3te7ToRPicuCKu6CfKApu+6Gsn2/KafvKy93et4S4P9ngiejz9ODc9nenITEdOp6fH8VP6M3E1HzdDuGPqNlO3RVTK0eRd8i5598iEG7rkvTs9PR3UbfAH/AF/zPOnzx/gA3yAD/CR6uucvqCuDfpbDzfqBwi9Td9S15N353UbcUVcpXUdumQ5dYkNTd3e3p56tsub6PAwTKnNTfGN99/nVGElnqHzMVX4o0unXLMe8/F6dnZmzQj5+hNX59akAucjs1KRuN7Z2WnKcz7vEFcnFkviCaa5M839oaa5k682jLfIV53eIe9+f3lXOeDvv/9G7x4cDNZn4OP7w0dNXQN/wB9pvwDdjm7/HnR7jX6gn0g/sa+fet99H2u463H/eGpybup6ab88cNXAErG3HKdXLSaTif1CKuHgPY38f2+5nmWaYqskwXp03srEFfhgmvtra3Cn0+7BB/wh/gQf4COXH9BXXX4AH+ADfMzrh1ydRR31zqwovTc99Qf1B/wBf8Af8Edafw/1KanPqc9z9YdZyqytrVlDR5sIRsGiJ0vUCH/79q01w0v7/bH6dy3H+c/9559/7Jzy4as5X+56bm66CcPcx2j6HbAexJXHM/gI+Yx81eVz8m7gu6fAh75/bfDgvL54ivVI9Q74eFp8sB5l/f2c8OGvZahOQLdTR/XVkfAgPEifodxnAR/gA3yAj5Y+7XPSifR3XzuthzXcZa/Q5w3b59G1srJizfghT9W+/Wq4//vvv9aAqPFSS69Hr1Nq4z6cYz2C1zVx1Xkdg4/gdQw+wAf8UZ4lAT7AB/gAHzX1APoKfZXWdfAH/AF/wB/wR92sIerzWU97+AP+WGb+MEsZNcxTT6eTk+CJ1+fpLo9sNcmvrq56vRZL3uzj8diebL+7u5t6A7d4yOtz49e+uA/WQ/FDXHUewOBj1qOMfNXN6AAf4CPnAQs+wEeqv9BXwUMbfIAP8FGe8QU+wAf4AB9DfRjqD+qPl1J/LOIFDw/CgzketIa7XldSY07b5uam+djFfn7yKyrt15PpargrwbYcJ59yfa5/dVi/bHn/8r7z+ePi64lfz+c+WA/iahav4CPkM/IVeTflF/ABPlK9gy4JehB8gA/w8b/F+gR8gA/wAT5q+hfUH9Qf1B/0E4f6lNQfy1l/mKWMnmCq9WxPvRglNPSEvJJIjUdf6kGkJr3Or0b6Ih7yup74M7kP1kOzBIir4AELPoL3G/mqm9EBPhbDR+q97rFFXBFX6Qwb8i55F71bng0VfzelvDo0U4q8S979lrwLn/fPakMnLqYTS30I8hX56lvyFXFFvqqddUn98fzqD2u46wl1LeIiHuoSxFrY1dVV1+f1XvJmv729tYa7D45FvODja+c+WI/T01NHXAU8g4/gqUi+6jzzwAf4yPE9+AAfyg8lzkBfoa/gD/BRygPwB/wBf8zmh7gvAj7AB/gAH0M6mvp8Oetzs5TZ3t52k8nEGu7v3r1zx8fHbn193Yqui4sLt7+/X9yvwFAzXQNLW47zn6uBjjqPPI9qzpe7nvPzc7t27mNkjTTWY98GhRJXHZ7BR8hn4KPL5+Aj8B34AB+p3gEf4COnh+GPx+MPff+Xl5fUHz31F3XUfH0Kn8Pn8Hm5fwM+wMf3gI8a/UA/kf5uX3/7vvWVNdxl5xIPxlKTTq+SaZNVjJrxpf16PUYNXhUiLcf5z9WT8WqMyo6m5ny564n987gP1kPxSly9NjzpzRHwEfIZ+arL5+ADfPj8EPM2+AAfqd6DP+CPtB6AP+AP+KPT17m6F3yAD/ABPkr5Ie5Toa/QV+ircr95mfBhljJra2szPuipx1SfN7s/Vl7qLcd5L3h56Cn5XF9fW+M99Ygf8nDU/pubG2such+j6XfAehBXfiYC+Pif6YwK8tVb+3GTvBu8hZ8CH6l3LOvxtOuRemOyHqxHbqYQ/DHLHz53otupP1S/gQ/0VdoveAp9BZ+Xva5ZD+pB8AE+avqt8Pn98rk13GUHc3R0ZE3rVg/1lZUVa9a3Hhd7uqtprundi3jI67x6/Vsb9+Ec6/F1OkuAuOrwDD6CZxz4AB/prBHwAT5ynorwB/yR6lr4A/6AP8JMHPDReVLDH+UZcOgr9BX4AB81/T30FfpqmfWVWcqoYb6xsWGi4ePHj+7g4MCdnJxYE3tvb88dHh4W98t7Xc3zq6urpuP8547HYzvv3d1d1fly1xO/nsN9sB6KV+Iq4Bl8hHxGvuryOfgAHzm+Bx/gI9V78Af8kdYD8Af8AX+U62XwAT7AB/io6aehr563vlJjUm8E0Bet7wvT353vmwvno19//dUsZdQw1yahoF+Z4ldO9PR5ab8KdL2i9ObNm6bj9AqqPvfTp0/2GqI84P0rDn3n88fF1+PtaLiPbvgs60FcgY98PgMf5N2UX8Qf2l69evVoPBhbysCDs/oCPg/6i3xFvsrlK2/f8dS63T8ok9PlQ3UEOAfnaZ1JPRh0K/gAH+AjWH88Zx6kfzWe2kLTh6MPR3+3q1vm+nC///77V00s9sJZw170xHrt39XclbBWo77lOH8eiQpdmECqrfX8+veaJKuN+3DWbGc9vlryJ646PIGPkM/AR5ffU3yosaetNu8TV7M8SVzl4wo+R5cor4AP8JHWB4vokpSnFo0r/znoxO7NYvgcPs/V70+Fj/vCOX2G+X7OInk31XGL5l3Wg/WI88xLxflQvQw+0Ls5vWuWMp8/f7ZmtbbLy0u3v7/vJpOJ/V0T2I+Pj4v7NShVH6xf+FqO85+bTnMfOl9ufzzFlvtgPRSvxFXAM/gI+Yx81eVz8AE+cnwPPsBHqvfgD/gjrQfgD/gD/ijXy+ADfIAP8FHTT0Nfoa/QV+V+8zLhw4am+km0Igj5zKjoTqcYy+c9t58ptvc7xVbfMesxmn4HejWlJR717798+eJk2aBY9q9l+v9eiuPcfqa5M82dae5Mc3+Iae6xpQz56osNXvf6grxL3iXvvoy867GKvkK3e5sj5XJ0O/UHfD7fT6FfQn0uvY/epZ/Y12+lf7Wc9aA13Hd2dtzR0ZEVvOnUeT29nk6NjaeyM1WYqcLLPFVYmAAfzmyKJBJa80OcP/QDiGx+tA3lldz+s7N11IYeAAAgAElEQVQzO5b1YD2Iq3leBh+nNgdGG/kKXYIu2S3qevABPsAH+Kip+9HtXV8EfYW+8vqS+oP6g75omT/BRx4fZimjRtrGRpgqzjRepvF6L2c/bbglPpi6/bynbks01ExPj/OAPMuURDUEoiU/MK06P61aTUHyrrMhz8TVoc1AES7Jux/tzSTwcWLfAfjo8if4CPocfKCvTk66/IC+Ql8pP5bqM/gD/kjjA/6AP+CPWf6M82eKD/Gs3rhs6XuQd8m7ubxrDXe9/vfXX3+ZgNva2jL/9jjo5Jte2q8nVtUwUcC2HKdmkz7XvzqsJ258kd13Pn9cfD3x6/ncB+tBXM3iFXyEfEa+Iu+m/AI+wEeqd9AlQQ+CD/ABPsKPgPAH9eBQnQl/wB+5fgr1B/UH/AF/wB/1/eZlqj/MUkZiutazPfVqlKG9muBq1KfeVDXe2wo8nV+DWxfxkNf1xH5Y3AfrobgjroIHLPgInnnkq25GB/hYDB+p97rHFnFFXKV6h7xL3k1nxpB383m3lFeHZkqRd8m735J34fPyrAh04uwsO/gcPofP897r9OG62QTUg7Pxgd6d1bvWcNcT6jlvqhpPdwliBZmmsvd5GpU8m29vb63h7gN1EY/o+Nq5D9ZDMwaIq87LOMU2+AAf4CN4UYKPWa85+Py9Ax/go8ST4AN8xDOs4A/4I1f3Un9Qf+RmVcEf8Af8Masv4/wJPsDHMuPDLGW2t7fNDkabXnc5Pj526+vr1oS/uLhw+/v7xf0SFhKdGmTYcpz/XA0i0XnkeVRzvtz1nJ+f27VzHyNrNLMe+zbghrjq8Aw+Qj4DH10+Bx+B78AH+Ej1DvgAHzk9DH88Hn/o+7+8vKT+6Km/qKPm61P4HD6Hz8v9G/ABPr4HfNToB/qJ9Hf7+tv3ra+s4S47l1rP9tRDXa+dqcGrQqTVe11Nfj0Zr8aoXsdYxENe16PXSrVxH87sIlgP4irGM/gInnHgo5vRQd4NnrzgA3yknorgA3zkZgrBH/BHOqsK/oA/4I+yJy/4AB/gA3zU9PfQV+irZdZXZimztrY244OuAaYfP360Rvjm5qa7u7sr7vfHysOp5Tj/ufLQU3Pw+vq66ny567m5ubGGO/cxmn4HrAdx5WcigI+Qz8hXXT4n7wavuRI+9NaVCoWH4EHNGtEGD87rC/IV+SrVn+Sr4XzVor/viwc9VofqBHQ7dVRfHQkPwoP0Gcp9FvABPsAH+IhnCAzpPeqo51dHWcNddjBHR0fWAJCHul450lPS2tRskFVLab8WXUHQelz8uWq4a3p3zfly16PXv7VxH85+9GA9unglrjo8g4+Qz8BHyOfgA3ykvA0+wEeq9+AP+CNXD8Af8Af8Ua6XwQf4AB/gY6ifhr5CX6Gvyv3mZcKHWcqoQbuxsWHNdT1ZdHBw4E5OTqyJvbe35w4PD4v79RSgguXq6qrpOP+54/HYzqtf72rOl7ue2D6D+2A9FK/EVcAz+Aj5jHzV5XPwAT5yfA8+wEeq9+AP+COtB+AP+AP+KNfL4AN8gA/wUdNPQ189b32lvqTeSKYvWt8Xpr873zcXzq3hLvuR1GOr1tNdT6ar4a5Gd+q9U+PprkAWMenJupxn5tZW52nUdz163Vgb9+HsTQHW450lSOLKOeEHfIT8AT7+z2ZtgI/gqQg+wEeqL8AH+Mh5jsIf8Eda18Af8Af8EWZ+gI/g2Z72L+AP+AN8gI+hfiv1x3LWH2YpI7HgF1hNaw0wjb2CNMigtF8DUfRvRSwtx+mc+lwFnv6sgafec7rvfP64+Hria+U+WA/iahav4CPkM/IVeTfllxZ8xM2VmO+IK+LqW+IKfdX9QF7Sn+jE8P205KuXElelvJrT++Td+foMfCyGD/icvNvHO/QZ8vGB3kXvonfpiw71W9Els7rEGu56Ilqko01/3t3dnfFsPz09Le6XIFYBsLq62nScvM30ube3t9Zw90WE/+8t1xP/25bj4vvkPliPNM6Jq5AHwAf4AB/w4BC/wudBR8Ef8Ec8+wi9O5s/wQf4AB9hdhr6Cn2FvqrvQ8Ef8Af8AX/42aMvoU89+uWXX77Kby6eaKsbqP27GvT//fff1PKl9ri1tbWphcwPP/wwbej7/97yOfKP18Z9dD+YsB7OfsAhrpwTnsBHyGfgg7yb8hv4AB+p3oA/bkxTwR9dvvTxAX/AH/BHuT4EH+ADfICPof4N+gp95fUl+mo+X4CP5cTH6M8///wq/3UNd/CelRJNtX+X97oaFjLJbzlOFjT69x8+fLBGubzNFjm/PkevLejXTu7DmRc+60FcxfgFHyGfgY/lz7vikdgjb4iXSvho+RziavnjaiiO0v3k3YfNuy34RCd2+PS6gHxFvnop+QqcU5+Tr5Y/X4FzcA7Olx/n33UdpaGp+jVFzercVG0/bba0/8cffzQhryZvyxRfptjmp9j6wbGsB3F1eHhoP0YJl3d3d/aD1BAec/uZgv68p6CTd+EPcB70B/mKfHVycjLDd+ADfOR4kvqjqyPAB/gAHyP7QTHXhwAf4AN8gI9Sfoj7bWn9ISEqr/KW/ia6BF0iXZbG1UgNdw2qGpqaW9rP1G2mbjN1m6nbQ/mDqdvLOXVbYgT+cPaGln54FsFOJpOpxZp+zE7zY24/+AAf/g2/OD6IK/QV+gp9hb4K/DDEr/Eg1KHvjbq2/3tFl6BL0CXO6Q05dHt/XUPeDd8Puh3dntPtNjRVv+gMTZst7WdaNdOqmVbNtOqh/MG06tlp1RKx5F3nXhp/xKLyy5cv02HfL+0+4POAx3gdhUnxGfmKfKUn4vRjIjgPee6h8FHKq/585CvyVQ6PufjQj9y1+go+J64eIq7IV8QVcTWvn+Dzrr6gX9KfH+Lvp4XPn3vetYa7nswbmo5d2i8A6QtZXV11u7thavL79+9d39RYv//29tYaT/5LrT2uNKGa+2A9FHfEVZj2zjR3prkzzZ1p7mpCDPErfN7pFvgD/hBepI1jXQs+wEda16Cv0FfoK/QV+mpYX9L3me+LwR/wB/zxffCHWcpsb2+71DOz1kNchamKkp2dHfMSrD3OeymdnZ1Zs1+eR4t4Gcuz+vz83J5o4D5G1ihgPQ4ccRU8cMFHyEvgo/O4BB/gIzcTAnyAj9SrEv6AP1Jd/xj8UeOdSr4iX5GvwsyPdIYT+AAf4AN8DPX3HoPPa/p795mvavRDztMdvYvefSi9aw33z58/N3vOeu87vR6hBq+AUuNVm3rm6cl4AVGv7ea8wmo8cPU6vzbuw9nrKqzHxN64IK6cvTkCPoIHLPjovAjBx2vjDPAx6zEPPsBH6lUKf8Af6Pay1zX4AB/gA3zU9C/QV+gr9FXZCx58gI9lxodZyqytrU0tXfTK7MrKik37VsNyc3PT3d3dFff7Y+V12XKc/1x56Omc19fXVefLXc/NzY01T7iP0fQ7YD2IK+9jCT5CPiNfdfmcvBt8Xkv40FtXKqQfggflNacNHpzXF+Qr8lWqP8lXw/mqRX/fFw96rA7VCeh26qi+OhIehAfpM5T7LOADfIAP8BH7mQ/pPeqo51dHWcNddjCxh5BeqdBT0trUbJBVS2m/Fl1BIG+uluPiz1XDXVN9a86Xux69DqON+3D2owfr0cUrcdV56oGPkM/AR8jn4AN8pLwNPsBHqvfgD/gjVw/AH/AH/FGue8EH+AAf4GOoL4a+Ql+hr8r95mXCh1nKqEHb6r3uPaH0FKCC5erqyryBh7yi0v3j8dia+vr1Lucpm/NYSr3iJWy8PQD3wXooPoirjelMBPAR8hL5as9mbYAP8JHzVAQf4AN9VZ5FBD7AB/gAH0N1JvoKfYW+GplTQq4vBD7Ax0vBxyJe8OhEdGJOJ1rDXfYjSoz+FXe9Rh/7Em5tbRX368l0NdwlQFqOk9+6Plev1gt4erLO+7D3nc8fF1+vXjfWxn04e1OA9SCuwEc+n4EP8m7KL/BH4HvwAT7AB3p3qB5Atwd9AX/AH2m9DD7AR66fgr5CX6Gv0Ffoq/p+8zLpK7OU0ROwXiCoaa0BprFXkAYZlPZLaOjfKom0HKdz6nPVpNef1Uj3ntN95/PHxdcTXyv3wXoQV7N4BR8hn5GvyLspv7TgIyb/mO+IK+LqW+IKfdU9eFHSn+jE8P205KuXElelvJrT++Td+foMfCyGD/icvNvHO/QZ8vGB3kXvonfpiw71W9Els7rEGu56Ilqko01/3t3dnfFsPz09Le6XIFYBsLq62nScvM30ube3t9Zw90WE/+8t1xP/25bj4vvkPliPNM6Jq5AHwAf4AB/w4BC/wudBR8Ef8Ec8+wi9O5s/wQf4AB9HVnfn6l7wAT7AB/go5Qf6V/N9SuoP6o9cH/u59K/MUkZDslq9172HnQJcTXoNLE09a2o83WWIL2Ehz6Ocp9PeXucF1OeZp6EU2riPkf2AwXoc2KBQ4so54Qd8hPwBPjpPRfBxMp0ZAj7AR6ovwAf4yM0Ugj8ejz9qvFNZj8dbj5r6jPVgPdK6H32FvkJfBc928FGe9Xif/FGjH3IzBshX5KuHylfWcJedSzxY8d27d2b1ok1WMZPJpLhfr5WowSugtBznP1dPxkvI6TXRmvPlrif2z+M+WA/FK3EVBgmDj5DPyFddPgcf4MMPGo95G3yAj1TvwR/wR1oPwB/wB/zh7M3sXN0LPsAH+AAfpfwQ96nQV+gr9FW537xM+Bj98ccfX/V0+c3NjTW8X716Zc11/8T7zz//7C4uLor719fXbf+bN2+ajvOf++nTJzv28vLSzj90vtx+Nfu1cR/OsR5dvBJXAc/gI+Qz8JHHh96EUA5u4YHnGle6j+vr62o+e6730ce78CC6pCY+lpkHwTm6fVE+93y3zPioyQ8x3z9XHgTn4HxRnNNnmNdJ4Jx6MO3vwYNt/ZKhepl8RR8u1V/Ku6Pffvvt63g8nj5hrifctPknzof+rsaGfs3X/1qO85+ri9D/1OjXNnS+3P6rqys7lvtw1mhiPZzFFHHV4Ql8hHwGPrr8Dj66N6rAxyzfgw/wkeo4+AP+SOsB+AP+yNVr8Af8AX+U+yfgA3yAD/Ax1F9FXy2nvrKhqXqiXU8RyNpFf1bz+8OHD9aQ+Omnn6yJW9ofT6tuOc5/bjrFduh8uf1+4Cr34Rzr0cUrcRXwDD5CPgMf4CPlsxZ8xP825jviirj6lrhCX/XrT/h8ufm8lFeH6gHyLnn3W/IufE7ere17tOhE+Jy4Iq7oJ8oCm77oayfb8pp+8rL3d63hLg/2eCJ6PP04Nz2d6chMR06np8fxU/ozcTUfN0O4Y+o2U7dFVMrR5F3yLnn3yIQbuuS9Oz09HdRt8Af8AX/M86fPH+ADfIAP8JHq65y+oK4N+lsPN+oHCL1N31LXk3fndRtxRVyldR26ZDl1iQ1N3d7ennq2y5vo8DBMqc1N8Y333+dUYSWeofMxVfijS6dcsx7z8Xp2dmbNCPn6E1fn1qQC5yOzUpG43tnZacpzPu8QVycWS+IJprkzzf2hprmTrzaMt8hXnd4h735/eVc54O+//0bvHhwM1mfg4/vDR01dA3/AH2m/AN2Obv8edHuNfqCfSD+xr596330fa7jrcf94anJu6nppvzxw1cASsbccp1ctJpOJ/UIq4eA9jfx/b7meZZpiqyTBenTeysQV+GCa+2trcKfT7sEH/CH+BB/gI5cf0FddfgAf4AN8zOuHXJ1FHfXOrCi9Nz31B/UH/AF/wB/wR1p/D/Upqc+pz3P1h1nKrK2tWUNHmwhGwaInS9QIf/v2rTXDS/v9sfp3Lcf5z/3nn3/snPLhqzlf7npubroJw9zHaPodsB7Elccz+Aj5jHzV5XPybuC7p8CHvn9t8OC8vniK9Uj1Dvh4WnywHmX9/Zzw4a9lqE5At1NH9dWR8CA8SJ+h3GcBH+ADfICPlj7tc9KJ9HdfO62HNdxlr9DnDdvn0bWysmLN+CFP1b79arj/+++/1oCo8VJLr0evU2rjPpxjPYLXNXHVeR2Dj+B1DD7AB/xRniUBPsAH+AAfNfUA+gp9ldZ18Af8AX/AH/BH3awh6vNZT3v4A/5YZv4wSxk1zFNPp5OT4InX5+kuj2w1ya+urnq9Fkve7OPx2J5sv7u7m3oDt3jI63Pj1764D9ZD8UNcdR7A4GPWo4x81c3oAB/gI+cBCz7AR6q/0FfBQxt8gA/wUZ7xBT7AB/gAH0N9GOoP6o+XUn8s4gUPD8KDOR60hrteV1JjTtvm5qb52MV+fvIrKu3Xk+lquCvBthwnn3J9rn91WL9sef/yvvP54+LriV/P5z5YD+JqFq/gI+Qz8hV5N+UX8AE+Ur2DLgl6EHyAD/Dxv8X6BHyAD/ABPmr6F9Qf1B/UH/QTh/qU1B/LWX+YpYyeYKr1bE+9GCU09IS8kkiNR1/qQaQmvc6vRvoiHvK6nvgzuQ/WQ7MEiKvgAQs+gvcb+aqb0QE+FsNH6r3usUVcEVfpDBvyLnkXvVueDRV/N6W8OjRTirxL3v2WvAuf989qQycuphNLfQjyFfnqW/IVcUW+qp11Sf3x/OoPa7jrCXUt4iIe6hLEWtjV1VXX5/Ve8ma/vb21hrsPjkW84ONr5z5Yj9PTU0dcBTyDj+CpSL7qPPPAB/jI8T34AB/KDyXOQF+hr+AP8FHKA/AH/AF/zOaHuC8CPsAH+AAfQzqa+nw563OzlNne3naTycQa7u/evXPHx8dufX3diq6Liwu3v79f3K/AUDNdA0tbjvOfq4GOOo88j2rOl7ue8/Nzu3buY2SNNNZj3waFElcdnsFHyGfgo8vn4CPwHfgAH6neAR/gI6eH4Y/H4w99/5eXl9QfPfUXddR8fQqfw+fwebl/Az7Ax/eAjxr9QD+R/m5ff/u+9ZU13GXnEg/GUpNOr5Jpk1WMmvGl/Xo9Rg1eFSItx/nP1ZPxaozKjqbmfLnrif3zuA/WQ/FKXL02POnNEfAR8hn5qsvn4AN8+PwQ8zb4AB+p3oM/4I+0HoA/4A/4o9PXuboXfIAP8AE+Svkh7lOhr9BX6Ktyv3mZ8GGWMmtrazM+6KnHVJ83uz9WXuotx3kveHnoKflcX19b4z31iB/ycNT+m5sbay5yH6Ppd8B6EFd+JgL4+J/pjAry1Vv7cZO8G7yFnwIfqXcs6/G065F6Y7IerEduphD8McsfPnei26k/VL+BD/RV2i94Cn0Fn5e9rlkP6kHwAT5q+q3w+f3yuTXcZQdzdHRkTetWD/WVlRVr1rceF3u6q2mu6d2LeMjrvHr9Wxv34Rzr8XU6S4C46vAMPoJnHPgAH+msEfABPnKeivAH/JHqWvgD/oA/wkwc8NF5UsMf5Rlw6Cv0FfgAHzX9PfQV+mqZ9ZVZyqhhvrGxYaLh48eP7uDgwJ2cnFgTe29vzx0eHhb3y3tdzfOrq6um4/znjsdjO+/d3V3V+XLXE7+ew32wHopX4irgGXyEfEa+6vI5+AAfOb4HH+Aj1XvwB/yR1gPwB/wBf5TrZfABPsAH+Kjpp6Gvnre+UmNSbwTQF63vC9Pfne+bC+ejX3/91Sxl1DDXJqGgX5niV0709Hlpvwp0vaL05s2bpuP0Cqo+99OnT/Yaojzg/SsOfefzx8XX4+1ouI9u+CzrQVyBj3w+Ax/k3ZRfxB/aXr169Wg8GFvKwIOz+gI+D/qLfEW+yuUrb9/x1LrdPyiT0+VDdQQ4B+dpnUk9GHQr+AAf4CNYfzxnHqR/NZ7aQtOHow9Hf7erW+b6cL///vtXTSz2wlnDXvTEeu3f1dyVsFajvuU4fx6JCl2YQKqt9fz695okq437cNZsZz2+WvInrjo8gY+Qz8BHl99TfKixp6027xNXszxJXOXjCj5HlyivgA/wkdYHi+iSlKcWjSv/OejE7s1i+Bw+z9XvT4WP+8I5fYb5fs4ieTfVcYvmXdaD9YjzzEvF+VC9DD7Quzm9a5Yynz9/tma1tsvLS7e/v+8mk4n9XRPYj4+Pi/s1KFUfrF/4Wo7zn5tOcx86X25/PMWW+2A9FK/EVcAz+Aj5jHzV5XPwAT5yfA8+wEeq9+AP+COtB+AP+AP+KNfL4AN8gA/wUdNPQ1+hr9BX5X7zMuHDhqb6SbQiCPnMqOhOpxjL5z23nym29zvFVt8x6zGafgd6NaUlHvXvv3z54mTZoFj2r2X6/16K49x+prkzzZ1p7kxzf4hp7rGlDPnqiw1e9/qCvEveJe++jLzrsYq+Qrd7myPlcnQ79Qd8Pt9PoV9CfS69j96ln9jXb6V/tZz1oDXcd3Z23NHRkRW86dR5Pb2eTo2Np7IzVZipwss8VViYAB/ObIokElrzQ5w/9AOIbH60DeWV3P6zszM7lvVgPYireV4GH6c2B0Yb+Qpdgi7ZLep68AE+wAf4qKn70e1dXwR9hb7y+pL6g/qDvmiZP8FHHh9mKaNG2sZGmCrONF6m8XovZz9tuCU+mLr9vKduSzTUTE+P84A8y5RENQSiJT8wrTo/rVpNQfKusyHPxNWhzUARLsm7H+3NJPBxYt8B+OjyJ/gI+hx8oK9OTrr8gL5CXyk/luoz+AP+SOMD/oA/4I9Z/ozzZ4oP8azeuGzpe5B3ybu5vGsNd73+99dff5mA29raMv/2OOjkm17arydW1TBRwLYcp2aTPte/OqwnbnyR3Xc+f1x8PfHr+dwH60FczeIVfIR8Rr4i76b8Aj7AR6p30CVBD4IP8AE+wo+A8Af14FCdCX/AH7l+CvUH9Qf8AX/AH/X95mWqP8xSRmK61rM99WqUob2a4GrUp95UNd7bCjydX4NbF/GQ1/XEfljcB+uhuCOuggcs+AieeeSrbkYH+FgMH6n3uscWcUVcpXqHvEveTWfGkHfzebeUV4dmSpF3ybvfknfh8/KsCHTi7Cw7+Bw+h8/z3uv04brZBNSDs/GB3p3Vu9Zw1xPqOW+qGk93CWIFmaay93kalTybb29vreHuA3URj+j42rkP1kMzBoirzss4xTb4AB/gI3hRgo9Zrzn4/L0DH+CjxJPgA3zEM6zgD/gjV/dSf1B/5GZVwR/wB/wxqy/j/Ak+wMcy48MsZba3t80ORptedzk+Pnbr6+vWhL+4uHD7+/vF/RIWEp0aZNhynP9cDSLReeR5VHO+3PWcn5/btXMfI2s0sx77NuCGuOrwDD5CPgMfXT4HH4HvwAf4SPUO+AAfOT0Mfzwef+j7v7y8pP7oqb+oo+brU/gcPofPy/0b8AE+vgd81OgH+on0d/v62/etr6zhLjuXWs/21ENdr52pwatCpNV7XU1+PRmvxqhex1jEQ17Xo9dKtXEfzuwiWA/iKsYz+AieceCjm9FB3g2evOADfKSeiuADfORmCsEf8Ec6qwr+gD/gj7InL/gAH+ADfNT099BX6Ktl1ldmKbO2tjbjg64Bph8/frRG+Obmpru7uyvu98fKw6nlOP+58tBTc/D6+rrqfLnrubm5sYY79zGafgesB3HlZyKAj5DPyFddPifvBq+5Ej701pUKhYfgQc0a0QYPzusL8hX5KtWf5KvhfNWiv++LBz1Wh+oEdDt1VF8dCQ/Cg/QZyn0W8AE+wAf4iGcIDOk96qjnV0dZw112MEdHR9YAkIe6XjnSU9La1GyQVUtpvxZdQdB6XPy5arhrenfN+XLXo9e/tXEfzn70YD26eCWuOjyDj5DPwEfI5+ADfKS8DT7AR6r34A/4I1cPwB/wB/xRrpfBB/gAH+BjqJ+GvkJfoa/K/eZlwodZyqhBu7GxYc11PVl0cHDgTk5OrIm9t7fnDg8Pi/v1FKCC5erqquk4/7nj8djOq1/vas6Xu57YPoP7YD0Ur8RVwDP4CPmMfNXlc/ABPnJ8Dz7AR6r34A/4I60H4A/4A/4o18vgA3yAD/BR009DXz1vfaW+pN5Ipi9a3xemvzvfNxfOreEu+5HUY6vW011PpqvhrkZ36r1T4+muQBYx6cm6nGfm1lbnadR3PXrdWBv34exNAdbjnSVI4so54Qd8hPwBPv7PZm2Aj+CpCD7AR6ovwAf4yHmOwh/wR1rXwB/wB/wRZn6Aj+DZnvYv4A/4A3yAj6F+K/XHctYfZikjseAXWE1rDTCNvYI0yKC0XwNR9G9FLC3H6Zz6XAWe/qyBp95zuu98/rj4euJr5T5YD+JqFq/gI+Qz8hV5N+WXFnzEzZWY74gr4upb4gp91f1AXtKf6MTw/bTkq5cSV6W8mtP75N35+gx8LIYP+Jy828c79Bny8YHeRe+id+mLDvVb0SWzusQa7noiWqSjTX/e3d2d8Ww/PT0t7pcgVgGwurradJy8zfS5t7e31nD3RYT/7y3XE//bluPi++Q+WI80zomrkAfAB/gAH/DgEL/C50FHwR/wRzz7CL07mz/BB/gAH2F2GvoKfYW+qu9DwR/wB/wBf/jZoy+hTz365ZdfvspvLp5oqxuo/bsa9P/999/U8qX2uLW1tamFzA8//DBt6Pv/3vI58o/Xxn10P5iwHs5+wCGunBOewEfIZ+CDvJvyG/gAH6negD9uTFPBH12+9PEBf8Af8Ee5PgQf4AN8gI+h/g36Cn3l9SX6aj5fgI/lxMfozz///Cr/dQ138J6VEk21f5f3uhoWMslvOU4WNPr3Hz58sEa5vM0WOb8+R68t6NdO7sOZFz7rQVzF+AUfIZ+Bj+XPu+KR2CNviJdK+Gj5HOJq+eNqKI7S/eTdh827LfhEJ3b49LqAfEW+ein5CpxTn5Ovlj9fgXNwDs6XH+ffdR2loan6NUXN6txUbT9ttrT/xx9/NCGvJm/LFF+m2Oan2PrBsawHcXV4eGg/RgmXd3d39oPUEB5z+5mC/rynoJN34Q9wHvQH+Yp8dXJyMsN34AN85HiS+qOrI8AH+AAfI/tBMdeHAB/gA3yAj1J+iPttaf0hISqv8pb+JroEXSJdlsbVSA13Daoamppb2hbEir4AACAASURBVM/UbaZuM3WbqdtD+YOp28s5dVtiBP5w9oaWfngWwU4mk6nFmn7MTvNjbj/4AB/+Db84Pogr9BX6Cn2Fvgr8MMSv8SDUoe+Nurb/e0WXoEvQJc7pDTl0e39dQ94N3w+6Hd2e0+02NFW/6AxNmy3tZ1o106qZVs206qH8wbTq2WnVErHkXedeGn/EovLLly/TYd8v7T7g84DHeB2FSfEZ+Yp8pSfi9GMiOA957qHwUcqr/nzkK/JVDo+5+NCP3LX6Cj4nrh4irshXxBVxNa+f4POuvqBf0p8f4u+nhc+fe961hruezBuajl3aLwDpC1ldXXW7u2Fq8vv3713f1Fi///b21hpP/kutPa40oZr7YD0Ud8RVmPbONHemuTPNnWnuakIM8St83ukW+AP+EF6kjWNdCz7AR1rXoK/QV+gr9BX6alhf0veZ74vBH/AH/PF98IdZymxvb7vUM7PWQ1yFqYqSnZ0d8xKsPc57KZ2dnVmzX55Hi3gZy7P6/PzcnmjgPkbWKGA9DhxxFTxwwUfIS+Cj87gEH+AjNxMCfICP1KsS/oA/Ul3/GPxR451KviJfka/CzI90hhP4AB/gA3wM9fceg89r+nv3ma9q9EPO0x29i959KL1rDffPnz83e8567zu9HqEGr4BS41WbeubpyXgBUa/t5rzCajxw9Tq/Nu7D2esqrMfE3rggrpy9OQI+ggcs+Oi8CMHHa+MM8DHrMQ8+wEfqVQp/wB/o9rLXNfgAH+ADfNT0L9BX6Cv0VdkLHnyAj2XGh1nKrK2tTS1d9MrsysqKTftWw3Jzc9Pd3d0V9/tj5XXZcpz/XHno6ZzX19dV58tdz83NjTVPuI/R9DtgPYgr72MJPkI+I191+Zy8G3xeS/jQW1cqpB+CB+U1pw0enNcX5CvyVao/yVfD+apFf98XD3qsDtUJ6HbqqL46Eh6EB+kzlPss4AN8gA/wEfuZD+k96qjnV0dZw112MLGHkF6p0FPS2tRskFVLab8WXUEgb66W4+LPVcNdU31rzpe7Hr0Oo437cPajB+vRxStx1XnqgY+Qz8BHyOfgA3ykvA0+wEeq9+AP+CNXD8Af8Af8Ua57wQf4AB/gY6gvhr5CX6Gvyv3mZcKHWcqoQdvqve49ofQUoILl6urKvIGHvKLS/ePx2Jr6+vUu5ymb81hKveIlbLw9APfBeig+iKuN6UwE8BHyEvlqz2ZtgA/wkfNUBB/gA31VnkUEPsAH+AAfQ3Um+gp9hb4amVNCri8EPsDHS8HHIl7w6ER0Yk4nWsNd9iNKjP4Vd71GH/sSbm1tFffryXQ13CVAWo6T37o+V6/WC3h6ss77sPedzx8XX69eN9bGfTh7U4D1IK7ARz6fgQ/ybsov8Efge/ABPsAHeneoHkC3B30Bf8Afab0MPsBHrp+CvkJfoa/QV+ir+n7zMukrs5TRE7BeIKhprQGmsVeQBhmU9kto6N8qibQcp3Pqc9Wk15/VSPee033n88fF1xNfK/fBehBXs3gFHyGfka/Iuym/tOAjJv+Y74gr4upb4gp91T14UdKf6MTw/bTkq5cSV6W8mtP75N35+gx8LIYP+Jy828c79Bny8YHeRe+id+mLDvVb0SWzusQa7noiWqSjTX/e3d2d8Ww/PT0t7pcgVgGwurradJy8zfS5t7e31nD3RYT/7y3XE//bluPi++Q+WI80zomrkAfAB/gAH/DgEL/C50FHwR/wRzz7CL07mz/BB/gAH0dWd+fqXvABPsAH+CjlB/pX831K6g/qj1wf+7n0r8xSRkOyWr3XvYedAlxNeg0sTT1rajzdZYgvYSHPo5yn095e5wXU55mnoRTauI+R/YDBehzYoFDiyjnhB3yE/AE+Ok9F8HEynRkCPsBHqi/AB/jIzRSCPx6PP2q8U1mPx1uPmvqM9WA90roffYW+Ql8Fz3bwUZ71eJ/8UaMfcjMGyFfkq4fKV9Zwl51LPFjx3bt3ZvWiTVYxk8mkuF+vlajBK6C0HOc/V0/GS8jpNdGa8+WuJ/bP4z5YD8UrcRUGCYOPkM/IV10+Bx/gww8aj3kbfICPVO/BH/BHWg/AH/AH/OHszexc3Qs+wAf4AB+l/BD3qdBX6Cv0VbnfvEz4GP3xxx9f9XT5zc2NNbxfvXplzXX/xPvPP//sLi4uivvX19dt/5s3b5qO85/76dMnO/by8tLOP3S+3H41+7VxH86xHl28ElcBz+Aj5DPwkceH3oRQDm7hgecaV7qP6+vraj57rvfRx7vwILqkJj6WmQfBObp9UT73fLfM+KjJDzHfP1ceBOfgfFGc02eY10ngnHow7e/Bg239kqF6mXxFHy7VX8q7o99+++3reDyePmGuJ9y0+SfOh/6uxoZ+zdf/Wo7zn6uL0P/U6Nc2dL7c/qurKzuW+3DWaGI9nMUUcdXhCXyEfAY+uvwOPro3qsDHLN+DD/CR6jj4A/5I6wH4A/7I1WvwB/wBf5T7J+ADfIAP8DHUX0VfLae+sqGpeqJdTxHI2kV/VvP7w4cP1pD46aefrIlb2h9Pq245zn9uOsV26Hy5/X7gKvfhHOvRxStxFfAMPkI+Ax/gI+WzFnzE/zbmO+KKuPqWuEJf9etP+Hy5+byUV4fqAfIuefdb8i58Tt6t7Xu06ET4nLgirugnygKbvuhrJ9vymn7ysvd3reEuD/Z4Ino8/Tg3PZ3pyExHTqenx/FT+jNxNR83Q7hj6jZTt0VUytHkXfIueffIhBu65L07PT0d1G3wB/wBf8zzp88f4AN8gA/wkerrnL6grg36Ww836gcIvU3fUteTd+d1G3FFXKV1HbpkOXWJDU3d3t6eerbLm+jwMEypzU3xjfff51RhJZ6h8zFV+KNLp1yzHvPxenZ2Zs0I+foTV+fWpALnI7NSkbje2dlpynM+7xBXJxZL4gmmuTPN/aGmuZOvNoy3yFed3iHvfn95Vzng77//Ru8eHAzWZ+Dj+8NHTV0Df8Afab8A3Y5u/x50e41+oJ9IP7Gvn3rffR9ruOtx/3hqcm7qemm/PHDVwBKxtxynVy0mk4n9Qirh4D2N/H9vuZ5lmmKrJMF6dN7KxBX4YJr7a2twp9PuwQf8If4EH+Ajlx/QV11+AB/gA3zM64dcnUUd9c6sKL03PfUH9Qf8AX/AH/BHWn8P9Smpz6nPc/WHWcqsra1ZQ0ebCEbBoidL1Ah/+/atNcNL+/2x+nctx/nP/eeff+yc8uGrOV/uem5uugnD3Mdo+h2wHsSVxzP4CPmMfNXlc/Ju4LunwIe+f23w4Ly+eIr1SPUO+HhafLAeZf39nPDhr2WoTkC3U0f11ZHwIDxIn6HcZwEf4AN8gI+WPu1z0on0d187rYc13GWv0OcN2+fRtbKyYs34IU/Vvv1quP/777/WgKjxUkuvR69TauM+nGM9gtc1cdV5HYOP4HUMPsAH/FGeJQE+wAf4AB819QD6Cn2V1nXwB/wBf8Af8EfdrCHq81lPe/gD/lhm/jBLGTXMU0+nk5Pgidfn6S6PbDXJr66uer0WS97s4/HYnmy/u7ubegO3eMjrc+PXvrgP1kPxQ1x1HsDgY9ajjHzVzegAH+Aj5wELPsBHqr/QV8FDG3yAD/BRnvEFPsAH+AAfQ30Y6g/qj5dSfyziBQ8PwoM5HrSGu15XUmNO2+bmpvnYxX5+8isq7deT6Wq4K8G2HCefcn2uf3VYv2x5//K+8/nj4uuJX8/nPlgP4moWr+Aj5DPyFXk35RfwAT5SvYMuCXoQfIAP8PG/xfoEfIAP8AE+avoX1B/UH9Qf9BOH+pTUH8tZf5iljJ5gqvVsT70YJTT0hLySSI1HX+pBpCa9zq9G+iIe8rqe+DO5D9ZDswSIq+ABCz6C9xv5qpvRAT4Ww0fqve6xRVwRV+kMG/IueRe9W54NFX83pbw6NFOKvEve/Za8C5/3z2pDJy6mE0t9CPIV+epb8hVxRb6qnXVJ/fH86g9ruOsJdS3iIh7qEsRa2NXVVdfn9V7yZr+9vbWGuw+ORbzg42vnPliP09NTR1wFPIOP4KlIvuo888AH+MjxPfgAH8oPJc5AX6Gv4A/wUcoD8Af8AX/M5oe4LwI+wAf4AB9DOpr6fDnrc7OU2d7edpPJxBru7969c8fHx259fd2KrouLC7e/v1/cr8BQM10DS1uO85+rgY46jzyPas6Xu57z83O7du5jZI001mPfBoUSVx2ewUfIZ+Cjy+fgI/Ad+AAfqd4BH+Ajp4fhj8fjD33/l5eX1B899Rd11Hx9Cp/D5/B5uX8DPsDH94CPGv1AP5H+bl9/+771lTXcZecSD8ZSk06vkmmTVYya8aX9ej1GDV4VIi3H+c/Vk/FqjMqOpuZ8ueuJ/fO4D9ZD8UpcvTY86c0R8BHyGfmqy+fgA3z4/BDzNvgAH6negz/gj7QegD/gD/ij09e5uhd8gA/wAT5K+SHuU6Gv0Ffoq3K/eZnwYZYya2trMz7oqcdUnze7P1Ze6i3HeS94eegp+VxfX1vjPfWIH/Jw1P6bmxtrLnIfo+l3wHoQV34mAvj4n+mMCvLVW/txk7wbvIWfAh+pdyzr8bTrkXpjsh6sR26mEPwxyx8+d6LbqT9Uv4EP9FXaL3gKfQWfl72uWQ/qQfABPmr6rfD5/fK5NdxlB3N0dGRN61YP9ZWVFWvWtx4Xe7qraa7p3Yt4yOu8ev1bG/fhHOvxdTpLgLjq8Aw+gmcc+AAf6awR8AE+cp6K8Af8kepa+AP+gD/CTBzw0XlSwx/lGXDoK/QV+AAfNf099BX6apn1lVnKqGG+sbFhouHjx4/u4ODAnZycWBN7b2/PHR4eFvfLe13N86urq6bj/OeOx2M7793dXdX5ctcTv57DfbAeilfiKuAZfIR8Rr7q8jn4AB85vgcf4CPVe/AH/JHWA/AH/AF/lOtl8AE+wAf4qOmnoa+et75SY1JvBNAXre8L09+d75sL56Nff/3VLGXUMNcmoaBfmeJXTvT0eWm/CnS9ovTmzZum4/QKqj7306dP9hqiPOD9Kw595/PHxdfj7Wi4j274LOtBXIGPfD4DH+TdlF/EH9pevXr1aDwYW8rAg7P6Aj4P+ot8Rb7K5Stv3/HUut0/KJPT5UN1BDgH52mdST0YdCv4AB/gI1h/PGcepH81ntpC04ejD0d/t6tb5vpwv//++1dNLPbCWcNe9MR67d/V3JWwVqO+5Th/HokKXZhAqq31/Pr3miSrjftw1mxnPb5a8ieuOjyBj5DPwEeX31N8qLGnrTbvE1ezPElc5eMKPkeXKK+AD/CR1geL6JKUpxaNK/856MTuzWL4HD7P1e9PhY/7wjl9hvl+ziJ5N9Vxi+Zd1oP1iPPMS8X5UL0MPtC7Ob1rljKfP3+2ZrW2y8tLt7+/7yaTif1dE9iPj4+L+zUoVR+sX/hajvOfm05zHzpfbn88xZb7YD0Ur8RVwDP4CPmMfNXlc/ABPnJ8Dz7AR6r34A/4I60H4A/4A/4o18vgA3yAD/BR009DX6Gv0FflfvMy4cOGpvpJtCII+cyo6E6nGMvnPbefKbb3O8VW3zHrMZp+B3o1pSUe9e+/fPniZNmgWPavZfr/Xorj3H6muTPNnWnuTHN/iGnusaUM+eqLDV73+oK8S94l776MvOuxir5Ct3ubI+VydDv1B3w+30+hX0J9Lr2P3qWf2NdvpX+1nPWgNdx3dnbc0dGRFbzp1Hk9vZ5OjY2nsjNVmKnCyzxVWJgAH85siiQSWvNDnD/0A4hsfrQN5ZXc/rOzMzuW9WA9iKt5XgYfpzYHRhv5Cl2CLtkt6nrwAT7AB/ioqfvR7V1fBH2FvvL6kvqD+oO+aJk/wUceH2Ypo0baxkaYKs40Xqbxei9nP224JT6Yuv28p25LNNRMT4/zgDzLlEQ1BKIlPzCtOj+tWk1B8q6zIc/E1aHNQBEuybsf7c0k8HFi3wH46PIn+Aj6HHygr05OuvyAvkJfKT+W6jP4A/5I4wP+gD/gj1n+jPNnig/xrN64bOl7kHfJu7m8aw13vf73119/mYDb2toy//Y46OSbXtqvJ1bVMFHAthynZpM+1786rCdufJHddz5/XHw98ev53AfrQVzN4hV8hHxGviLvpvwCPsBHqnfQJUEPgg/wAT7Cj4DwB/XgUJ0Jf8AfuX4K9Qf1B/wBf8Af9f3mZao/zFJGYrrWsz31apShvZrgatSn3lQ13tsKPJ1fg1sX8ZDX9cR+WNwH66G4I66CByz4CJ555KtuRgf4WAwfqfe6xxZxRVyleoe8S95NZ8aQd/N5t5RXh2ZKkXfJu9+Sd+Hz8qwIdOLsLDv4HD6Hz/Pe6/ThutkE1IOz8YHendW71nDXE+o5b6oaT3cJYgWZprL3eRqVPJtvb2+t4e4DdRGP6PjauQ/WQzMGiKvOyzjFNvgAH+AjeFGCj1mvOfj8vQMf4KPEk+ADfMQzrOAP+CNX91J/UH/kZlXBH/AH/DGrL+P8CT7AxzLjwyxltre3zQ5Gm153OT4+duvr69aEv7i4cPv7+8X9EhYSnRpk2HKc/1wNItF55HlUc77c9Zyfn9u1cx8jazSzHvs24Ia46vAMPkI+Ax9dPgcfge/AB/hI9Q74AB85PQx/PB5/6Pu/vLyk/uipv6ij5utT+Bw+h8/L/RvwAT6+B3zU6Af6ifR3+/rb962vrOEuO5daz/bUQ12vnanBq0Kk1XtdTX49Ga/GqF7HWMRDXtej10q1cR/O7CJYD+IqxjP4CJ5x4KOb0UHeDZ684AN8pJ6K4AN85GYKwR/wRzqrCv6AP+CPsicv+AAf4AN81PT30Ffoq2XWV2Yps7a2NuODrgGmHz9+tEb45uamu7u7K+73x8rDqeU4/7ny0FNz8Pr6uup8ueu5ubmxhjv3MZp+B6wHceVnIoCPkM/IV10+J+8Gr7kSPvTWlQqFh+BBzRrRBg/O6wvyFfkq1Z/kq+F81aK/74sHPVaH6gR0O3VUXx0JD8KD9BnKfRbwAT7AB/iIZwgM6T3qqOdXR1nDXXYwR0dH1gCQh7peOdJT0trUbJBVS2m/Fl1B0Hpc/LlquGt6d835ctej17+1cR/OfvRgPbp4Ja46PIOPkM/AR8jn4AN8pLwNPsBHqvfgD/gjVw/AH/AH/FGul8EH+AAf4GOon4a+Ql+hr8r95mXCh1nKqEG7sbFhzXU9WXRwcOBOTk6sib23t+cODw+L+/UUoILl6uqq6Tj/uePx2M6rX+9qzpe7ntg+g/tgPRSvxFXAM/gI+Yx81eVz8AE+cnwPPsBHqvfgD/gjrQfgD/gD/ijXy+ADfIAP8FHTT0NfPW99pb6k3kimL1rfF6a/O983F86t4S77kdRjq9bTXU+mq+GuRnfqvVPj6a5AFjHpybqcZ+bWVudp1Hc9et1YG/fh7E0B1uOdJUjiyjnhB3yE/AE+/s9mbYCP4KkIPsBHqi/AB/jIeY7CH/BHWtfAH/AH/BFmfoCP4Nme9i/gD/gDfICPoX4r9cdy1h9mKSOx4BdYTWsNMI29gjTIoLRfA1H0b0UsLcfpnPpcBZ7+rIGn3nO673z+uPh64mvlPlgP4moWr+Aj5DPyFXk35ZcWfMTNlZjviCvi6lviCn3V/UBe0p/oxPD9tOSrlxJXpbya0/vk3fn6DHwshg/4nLzbxzv0GfLxgd5F76J36YsO9VvRJbO6xBrueiJapKNNf97d3Z3xbD89PS3ulyBWAbC6utp0nLzN9Lm3t7fWcPdFhP/vLdcT/9uW4+L75D5YjzTOiauQB8AH+AAf8OAQv8LnQUfBH/BHPPsIvTubP8EH+AAfYXYa+gp9hb6q70PBH/AH/AF/+NmjL6FPPfrll1++ym8unmirG6j9uxr0//3339Typfa4tbW1qYXMDz/8MG3o+//e8jnyj9fGfXQ/mLAezn7AIa6cE57AR8hn4IO8m/Ib+AAfqd6AP25MU8EfXb708QF/wB/wR7k+BB/gA3yAj6H+DfoKfeX1JfpqPl+Aj+XEx+jPP//8Kv91DXfwnpUSTbV/l/e6GhYyyW85ThY0+vcfPnywRrm8zRY5vz5Hry3o107uw5kXPutBXMX4BR8hn4GP5c+74pHYI2+Il0r4aPkc4mr542oojtL95N2Hzbst+EQndvj0uoB8Rb56KfkKnFOfk6+WP1+Bc3AOzpcf5991HaWhqfo1Rc3q3FRtP222tP/HH380Ia8mb8sUX6bY5qfY+sGxrAdxdXh4aD9GCZd3d3f2g9QQHnP7mYL+vKegk3fhD3Ae9Af5inx1cnIyw3fgA3zkeJL6o6sjwAf4AB8j+0Ex14cAH+ADfICPUn6I+21p/SEhKq/ylv4mugRdIl2WxtVIDXcNqhqamlvaz9Rtpm4zdZup20P5g6nbyzl1W2IE/nD2hpZ+eBbBTiaTqcWafsxO82NuP/gAH/4Nvzg+iCv0FfoKfYW+CvwwxK/xINSh7426tv97RZegS9AlzukNOXR7f11D3g3fD7od3Z7T7TY0Vb/oDE2bLe1nWjXTqplWzbTqofzBtOrZadUSseRd514af8Si8suXL9Nh3y/tPuDzgMd4HYVJ8Rn5inylJ+L0YyI4D3nuofBRyqv+fOQr8lUOj7n40I/ctfoKPieuHiKuyFfEFXE1r5/g866+oF/Snx/i76eFz5973rWGu57MG5qOXdovAOkLWV1ddbu7YWry+/fvXd/UWL//9vbWGk/+S609rjShmvtgPRR3xFWY9s40d6a5M82dae5qQgzxK3ze6Rb4A/4QXqSNY10LPsBHWtegr9BX6Cv0FfpqWF/S95nvi8Ef8Af88X3wh1nKbG9vu9Qzs9ZDXIWpipKdnR3zEqw9znspnZ2dWbNfnkeLeBnLs/r8/NyeaOA+RtYoYD0OHHEVPHDBR8hL4KPzuAQf4CM3EwJ8gI/UqxL+gD9SXf8Y/FHjnUq+Il+Rr8LMj3SGE/gAH+ADfAz19x6Dz2v6e/eZr2r0Q87THb2L3n0ovWsN98+fPzd7znrvO70eoQavgFLjVZt65unJeAFRr+3mvMJqPHD1Or827sPZ6yqsx8TeuCCunL05Aj6CByz46LwIwcdr4wzwMesxDz7AR+pVCn/AH+j2stc1+AAf4AN81PQv0FfoK/RV2QsefICPZcaHWcqsra1NLV30yuzKyopN+1bDcnNz093d3RX3+2PlddlynP9ceejpnNfX11Xny13Pzc2NNU+4j9H0O2A9iCvvYwk+Qj4jX3X5nLwbfF5L+NBbVyqkH4IH5TWnDR6c1xfkK/JVqj/JV8P5qkV/3xcPeqwO1QnoduqovjoSHoQH6TOU+yzgA3yAD/AR+5kP6T3qqOdXR1nDXXYwsYeQXqnQU9La1GyQVUtpvxZdQSBvrpbj4s9Vw11TfWvOl7sevQ6jjftw9qMH69HFK3HVeeqBj5DPwEfI5+ADfKS8DT7AR6r34A/4I1cPwB/wB/xRrnvBB/gAH+BjqC+GvkJfoa/K/eZlwodZyqhB2+q97j2h9BSgguXq6sq8gYe8otL94/HYmvr69S7nKZvzWEq94iVsvD0A98F6KD6Iq43pTATwEfIS+WrPZm2AD/CR81QEH+ADfVWeRQQ+wAf4AB9DdSb6Cn2FvhqZU0KuLwQ+wMdLwcciXvDoRHRiTidaw132I0qM/hV3vUYf+xJubW0V9+vJdDXcJUBajpPfuj5Xr9YLeHqyzvuw953PHxdfr1431sZ9OHtTgPUgrsBHPp+BD/Juyi/wR+B78AE+wAd6d6geQLcHfQF/wB9pvQw+wEeun4K+Ql+hr9BX6Kv6fvMy6SuzlNETsF4gqGmtAaaxV5AGGZT2S2jo3yqJtBync+pz1aTXn9VI957Tfefzx8XXE18r98F6EFezeAUfIZ+Rr8i7Kb+04CMm/5jviCvi6lviCn3VPXhR0p/oxPD9tOSrlxJXpbya0/vk3fn6DHwshg/4nLzbxzv0GfLxgd5F76J36YsO9VvRJbO6xBrueiJapKNNf97d3Z3xbD89PS3ulyBWAbC6utp0nLzN9Lm3t7fWcPdFhP/vLdcT/9uW4+L75D5YjzTOiauQB8AH+AAf8OAQv8LnQUfBH/BHPPsIvTubP8EH+AAfR1Z35+pe8AE+wAf4KOUH+lfzfUrqD+qPXB/7ufSvzFJGQ7Javde9h50CXE16DSxNPWtqPN1liC9hIc+jnKfT3l7nBdTnmaehFNq4j5H9gMF6HNigUOLKOeEHfIT8AT46T0XwcTKdGQI+wEeqL8AH+MjNFII/Ho8/arxTWY/HW4+a+oz1YD3Suh99hb5CXwXPdvBRnvV4n/xRox9yMwbIV+Sr/2fvfHjaOLouvu5LeYmDEgh/SonEN6pater3l5BAjoGSBl7jQNK8Onc1nvF4ZnfGgQDmt9KjJ+lmvbuee+4593r33IfKV9Zwl51LOFhxf3/frF60ySpmPB5n9+u1EjV4BZSa49zn6sl4CTm9JlpyvtT1hP553AfroXglrvwgYfDh8xn5qs3n4AN8uEHjIW+DD/AR6z34A/6I6wH4A/6APxp7MztV94IP8AE+wEcuP4R9KvQV+gp9le83rxI+Bn/99dc3PV0+nU6t4f3q1Strrrsn3n/99dfm6uoqu39zc9P2v3nzpuo497mfPn2yY6+vr+38fedL7VezXxv30TSsRxuvxJXHM/jw+Qx8pPGhNyGUg2t44KnGle7j5uammM+e6n108S48iC4piY9V5kFwjm5fls8d360yPkryQ8j3T5UHwTk4Xxbn9BkWdRI4px6M+3vwYF2/pK9eJl/Rh4v1l/Lu4I8//vg2HA5nT5jrCTdt7onzvr+rsaFf8/W/muPc5+oi9D81+rX1nS+1fzKZ2LHcR2ONJtajsZgirlo8gQ+fz8BHm9/BR/tGFfiY53vwAT5iHQd/wB9xPQB/wB+peg3+gD/gj3z/BHyAD/ABPvr6q+ir1dRXftJmnAAAIABJREFUNjRVT7TrKQJZu+jPan5/+PDBGhK//PKLNXFz+8Np1TXHuc+Np9j2nS+13w1c5T6ahvVo45W48ngGHz6fgQ/wEfNZDT7CfxvyHXFFXH1PXKGvuvUnfL7afJ7Lq331AHmXvPs9eRc+J++W9j1qdCJ8TlwRV/QTZYFNX/R1I9vykn7yqvd3reEuD/ZwIno4/Tg1PZ3pyExHjqenh/GT+zNxtRg3fbhj6jZTt0VUytHkXfIueffEhBu65H1zdnbWq9vgD/gD/ljkT5c/wAf4AB/gI9bXKX1BXev1tx5u1A8Qepu+pq4n7y7qNuKKuIrrOnTJauoSG5q6s7Mz82yXN9HxsZ9Sm5riG+6/z6nCSjx952Oq8GUTT7lmPRbj9fz83JoR8vUnri6sSQXOB2alInG9u7tbledc3iGuRhZL4gmmuTPN/aGmuZOvtoy3yFet3iHvvry8qxzw8eNH9O7RUW99Bj5eHj5K6hr4A/6I+wXodnT7S9DtJfqBfiL9xK5+6n33fazhrsf9w6nJqanruf3ywFUDS8Rec5xetRiPx/YLqYSD8zRy/73melZpiq2SBOvReisTV+CDae6vrcEdT7sHH/CH+BN8gI9UfkBftfkBfIAP8LGoH1J1FnXUvllROm966g/qD/gD/oA/4I+4/u7rU1KfU5+n6g+zlNnY2LCGjjYRjIJFT5aoEf727Vtrhuf2u2P172qOc5/777//2jnlw1dyvtT1TKfthGHuYzD7DlgP4srhGXz4fEa+avM5edfz3WPgQ9+/NnhwUV88xnrEegd8PC4+WI+8/n5K+HDX0lcnoNupo7rqSHgQHqTPkO+zgA/wAT7AR02f9inpRPq7rxuthzXcZa/Q5Q3b5dG1trZmzfg+T9Wu/Wq4f/nyxRoQJV5q8fXodUpt3EfTsB7e65q4ar2OwYf3OgYf4AP+yM+SAB/gA3yAj5J6AH2FvorrOvgD/oA/4A/4o2zWEPX5vKc9/AF/rDJ/mKWMGuaxp9No5D3xujzd5ZGtJvlkMun0Wsx5sw+HQ3uy/e7ubuYNXOMhr88NX/viPlgPxQ9x1XoAg495jzLyVTujA3yAj5QHLPgAH7H+Ql95D23wAT7AR37GF/gAH+ADfPT1Yag/qD+eS/2xjBc8PAgPpnjQGu56XUmNOW3b29vmYxf6+cmvKLdfT6ar4a4EW3OcfMr1ue7VYf2y5fzLu87njguvJ3w9n/tgPYirebyCD5/PyFfk3ZhfwAf4iPUOusTrQfABPsDH/2brE/ABPsAH+CjpX1B/UH9Qf9BP7OtTUn+sZv1hljJ6gqnUsz32YpTQ0BPySiIlHn2xB5Ga9Dq/GunLeMjresLP5D5YD80SIK68Byz48N5v5Kt2Rgf4WA4fsfe6wxZxRVzFM2zIu+Rd9G5+NlT43eTyat9MKfIuefd78i583j2rDZ24nE7M9SHIV+Sr78lXxBX5qnTWJfXH06s/rOGuJ9S1iMt4qEsQa2HX19ebLq/3nDf77e2tNdxdcCzjBR9eO/fBepydnTXElccz+PCeiuSr1jMPfICPFN+DD/Ch/JDjDPQV+gr+AB+5PAB/wB/wx3x+CPsi4AN8gA/w0aejqc9Xsz43S5mdnZ1mPB5bw31/f785PT1tNjc3rei6urpqDg8Ps/sVGGqma2BpzXHuczXQUeeR51HJ+VLXc3FxYdfOfQyskcZ6HNqgUOKqxTP48PkMfLT5HHx4vgMf4CPWO+ADfKT0MPzx4/hD3//19TX1R0f9RR21WJ/C5/A5fJ7v34AP8PES8FGiH+gn0t/t6m/ft76yhrvsXMLBWGrS6VUybbKKUTM+t1+vx6jBq0Kk5jj3uXoyXo1R2dGUnC91PaF/HvfBeiheiavXhie9OQI+fD4jX7X5HHyAD5cfQt4GH+Aj1nvwB/wR1wPwB/wBf7T6OlX3gg/wAT7ARy4/hH0q9BX6Cn2V7zevEj7MUmZjY2POBz32mOryZnfHyku95jjnBS8PPSWfm5sba7zHHvF9Ho7aP51OrbnIfQxm3wHrQVy5mQjg439mMyrIV2/tx03yrvcWfgx8xN6xrMfjrkfsjcl6sB6pmULwxzx/uNyJbqf+UP0GPtBXcb/gMfQVfJ73umY9qAfBB/go6bfC5/fL59Zwlx3MycmJNa1rPdTX1tasWV97XOjprqa5pncv4yGv8+r1b23cR9OwHt9mswSIqxbP4MN7xoEP8BHPGgEf4CPlqQh/wB+xroU/4A/4w8/EAR+tJzX8kZ8Bh75CX4EP8FHS30Nfoa9WWV+ZpYwa5ltbWyYaLi8vm6Ojo2Y0GlkT++DgoDk+Ps7ul/e6mueTyaTqOPe5w+HQznt3d1d0vtT1hK/ncB+sh+KVuPJ4Bh8+n5Gv2nwOPsBHiu/BB/iI9R78AX/E9QD8AX/AH/l6GXyAD/ABPkr6aeirp62v1JjUGwH0Rcv7wvR3F/vmwvng999/N0sZNcy1SSjoV6bwlRM9fZ7brwJdryi9efOm6ji9gqrP/fTpk72GKA9494pD1/ncceH1ODsa7qMdPst6EFfgI53PwAd5N+YX8Ye2V69e/TAeDC1l4MF5fQGfe/1FviJfpfKVs+94bN3uHpRJ6fK+OgKcg/O4zqQe9LoVfIAP8OGtP54yD9K/Gs5soenD0Yejv9vWLQt9uD///PObJhY74axhL3pivfTvau5KWKtRX3OcO49EhS5MINVWe379e02S1cZ9NNZsZz2+WfInrlo8gQ+fz8BHm99jfKixp6007xNX8zxJXKXjCj5HlyivgA/wEdcHy+iSmKeWjSv3OejE9s1i+Bw+T9Xvj4WP+8I5fYbFfs4yeTfWccvmXdaD9QjzzHPFeV+9DD7Quym9a5Yynz9/tma1tuvr6+bw8LAZj8f2d01gPz09ze7XoFR9sH7hqznOfW48zb3vfKn94RRb7oP1ULwSVx7P4MPnM/JVm8/BB/hI8T34AB+x3oM/4I+4HoA/4A/4I18vgw/wAT7AR0k/DX2FvkJf5fvNq4QPG5rqJtGKIOQzo6I7fOU9njoe7o+n2JYep1cO9Ln69zqnm5zt/rt85Uuvp2vqdun1cB+sRxznxJXPA+ADfICPrzYgPMVL4AN8gA/w0adb0bs+f6Kv0FdxfQY+wAf6Kt+HAR/gA3yAj5I+Lfrq6ekra7jv7u42Jycn1kiIp87r6fV4amw4lZ2pwkwVXuWpwsIE+GjMpkjNhNr8EOYPkYRsfrT15ZXU/vPzczuW9WA9iKtFXgYfZzYHRhv5Cl2CLtnL6nrwAT7AB/goqfvR7W1fBH2FvnL6kvqD+oO+aJ4/wUcaH2Ypo0ba1pafKs40XqbxOi9nN224Jj6Yuv20p25LNJRMTw/zgDzLlEQ1BKImPzCtOj2tWk1B8m5jQ56Jq2ObgSJckncv7Y038DGy7wB8tPkTfHh9Dj7QV6NRmx/QV+gr5cdcfQZ/wB9xfMAf8Af8Mc+fYf6M8SGe1fDimr4HeZe8m8q71nCXjcs///xjAu7du3fm3x4GnXzTc/v1xKoaJgrYmuPUbNLnuinceuLGFdld53PHhdcTvpbIfbAexNU8XsGHz2fkK/JuzC/gA3zEegdd4vUg+AAf4MP/CAh/UA/21ZnwB/yR6qdQf1B/wB/wB/xR3m9epfrDLGUkpp1AyHmo5/bL0F5NcDXqU17vfZ+rwNP5Nbg19KbqOy7cH/pm1hwXXi/30Xr3sx4f7UlT4YC48p684AN8KD+89LybmwkCPsAH+JjPD+irRU979FVaX/XNWqL+8N9bqs4irpbT7fA5ceXqvb7+BfUg9WA8o4W8u1zehc/Juy8x71rDXU+oa9MXkPNWzu1Xs1xJSFPZU55GfZ97e3trDXeXyJxHdN9x4f7QV6vmuPB6uY/3jbz5WQ+PA+LKe3SBD/Ch/PDS827Kw9H9UAwPwh8vHR/oxG4djb5K66tcXu2rB9Al6JLv0SXwOfmqtO9BPUg9GM88gM/pl6TyB7oEXZLSJWYps7OzY3Yw2vS6y+npabO5uWkN+Kurq+bw8DC7XwlHTXoNMqw5zn2uBpHoPPI8Kjlf6nouLi7s2rmPgTXMWY9DG3BDXLV4Bh8+n4GPNp+DD8934AN8xHoHfICPlB6GP34cf+j7v76+pv7oqL+ooxbrU/gcPofP8/0b8AE+XgI+SvQD/UT6u1397fvWV9Zwl51LqWd77KGu16jV4FUhUuu9ria/noxXY1Svcy3jIa/r0ev82riPxmxhWA/iKsQz+PCeceCjndFB3vWevOADfMSeiuADfKRmCsEf8Ec8qwr+gD/gj7wnL/gAH+ADfJT099BX6KtV1ldmKbOxsTHnV60BppeXl9YI397ebu7u7rL73bHyvK45zn2uPPTUHLy5uSk6X+p6ptOpNdy5j8HsO2A9iCtndQE+fD4jX7X5nLz7f7OZITl86K0rFQoPwYPyMNQGDy7qC/IV+SrWn+Sr/nxVo7/viwcdVvvqBHQ7dVRXHQkPwoP0GfJ9FvABPsAH+AhnCPTpPeqop1dHWcNddjChN5VeOQp93WXVktuvRVcQyGux5jg1MdznquGu6d3O/7PrfOFx+vc6r17/1sZ9NPajB+vRxitxBT7ivAQ+yLsxv8Afnu/BB/gAH+jdknoAfYW+Ql/l617wAT7AB/jo64tRf1B/pPqt8Mfq8YdZyqhBu7W1ZU1rPXl3dHTUjEYj+/vBwUFzfHyc3a+nABUsk8mk6jj3ucPh0J4g1K93JedLXU9on8F9sB6KV+LK4xl8+HxGvmrzOfgAHym+Bx/gI9Z78Af8EdcD8Af8AX/k62XwAT7AB/go6aehr9BX6Kt8v3mV8GENd9mPxB5bpZ7uejJdDXc1umPvnRJPdyUkNdz1ZF3KM/Pdu9bTqOt69LqxNu6jsTcFWI99++GIuGoa4Qd8+PwBPv7PZm2AD++pCD7AR6wvwAf4SHmOwh/wR1zXwB/wB/zhZ36AD+/ZHvcv4A/4A3yAj75+K/XHatYfZikjseAWWE1rDTANvYI0yCC3XwNR9G9FLDXH6Zz6XAWe/qyBp85zuut87rjwesJr5T5YD+JqHq/gw+cz8hV5N+aXGnyEzZWQ74gr4up74gp91f5AntOf6ET//dTkq+cSV7m8mtL75N3F+gx8LIcP+Jy828U79BnS8YHeRe+id+mL9vVb0SXzusQa7noiWqSjTX/e29ub82w/OzvL7pcgVgGwvr5edZy8zfS5t7e31nB3RYT77zXXE/7bmuPC++Q+WI84zokrnwfAB/gAH/BgH7/C515HwR/wR+iFjt6dz5/gA3yAjxOru1N1L/gAH+ADfOTyA/2rxT4l9Qf1R6qP/VT6V4Pffvvtm/zmwom2uuDSv6tB/99//80sX0qP29jYmFnI/PTTT7OGvvvvNZ8j/3ht3Ef7gwnr0dgPOMRV0whP4MPnM/BB3o35DXyAj1hvwB9T01TwR5svXXzAH/AH/JGvD8EH+AAf4KOvf4O+Ql85fYm+WswX4GM18TH4+++/v8l//fLy0goLWcNINJX+Xd7ralhomGnNce48Hz58sEa5vM2WOb8+R68t6GkA7qMxL3zW41tDXHk8gw+fz8BHm99XGR/ikdAjr4+Xcvio+RziavXjqi+O4v3k3YfNuzX4RCfO63ryFfnqueQrcE59Tr5a/XwFzsE5OF99nL/oOkpDU/VriprValqr0X505KcGq5F+fHyc3f/zzz9bA0dN3prj3Od2TXMvvZ54im3pceH1ch/tOrMeHgfEVd308C7cEVfEVSo+yLvk3VhfkHfJu6PRyB7AQCcu6m/wAT7Ax3x+COtX8AE+wAf4CPVDrr9F/UH90Vd/KI7kVV7T3ySuiKtUXA3UcNegqr6pubn9TN1m6jZTt5m63Zc/mLq9mlO3JUbgj8be0NIPzxL24/F4ZrGmH7Pj/JjaDz7Ah3vDL4wP4gp9hb5CX6GvPD/08Ws4CLXve6Ou7f5e0SXoEnRJ6/yAbu+ua8i7/vtBt6PbU7rdhqbqiYC+abO5/UyrZlo106qZVt2XP5hWPT+tWiKWvNs0z40/QlH59evX2bDv53Yf8LnHY7iOwqT4jHxFvtIbOfoxEZz7PPdQ+MjlVXc+8hX5KoXHVHzoR+5SfQWfE1cPEVfkK+KKuFrUT/B5W1/QL+nOD+H3U8PnTz3vWsNdT+aFE9HD6cep6elMR2Y6cjw9PYyf3J+Jq8W46cMdU7eZuq3iUTmavEveJe+eWDMFXfK+OTs769Vt8Af8AX8s8qfLH+ADfIAP8BHr65S+oK71+luNUzXC1tfX5+qSPl1G3l3UbcQVcRXXdeiS1dQlZimzs7PTxJ5npZ7uCgyR1e7ubqfXe84L/vz83IpGeR4t472uz724uLAinPsYNKxHO4OAuPIefuDDz6AAH+Aj9uIDH+Aj1jvwB/yR8oCFP34cf5R4p7IeP249Suoz1oP1QF/lZ+CBD/ABPn4MPkr0Q2pmJfUg9eBD1YPWcP/8+XO156zzvtPrEWq4i0hKvGpjzzz9Qiohp9d2U15hJR64ep1fG/fR2OsqrMfYfnknrhp7CgF8eA9Y8NF6EYKP18YZ4GPeYx58gI/YqxT+gD/Q7Xmva/ABPsAH+CjpX6Cv0Ffoq7wXPPgAH6uMD7OU2djYmHnR6lWhtbW1Rr/8qGG5vb3d3N3dZfe7Y+V1WXOc+1x56OmcNzc3RedLXc90OrXmCfcxmH0HrAdx5XwswYfPZ+SrNp+Td73Paw4feutKhfRD8KC85rTBg4v6gnxFvor1J/mqP1/V6O/74kGH1b46Ad1OHdVVR8KD8CB9hnyfBXyAD/ABPkI/8z69Rx319Oooa7jLDib0ENIrFXpKWpuaDbJqye3XoisI5M1Vc1z4uWq4a6pvyflS16PXv7VxH4396MF6tPFKXLVex+DD5zPw4fM5+AAfMW+DD/AR6z34A/5I1QPwB/wBf+TrXvABPsAH+Ojri6Gv0Ffoq3y/eZXwYZYyatCWerbHnkd6ClDBMplMzDu71gt+OBxaU1+/3qU8M1MeS8fH3mNI+yVsnD0A98F6KD6Iq63ZTATw4fMS+erAZm2AD/CR8uQFH+ADfTWvL0NdCz7AB/gAH311JvoKfYW+GphTQqovBD7Ax3PBxzJe8OhEdGJKJ1rDXfYjSozuFXe9Rh/6Er579y67X0+mq+EuAVJznPzW9bl6tV7A05N1zoe963zuuPB69bqxNu6jsTcFWA/iCnyk8xn4IO/G/AJ/eL4HH+ADfKB3++oBdLvXF/AH/BHXy+ADfKT6Kegr9BX6Cn2FvirvN6+SvjJLGT0B6wSCmtYaYBp6BWmQQW6/hIb+rZJIzXE6pz5XTXr9WY105znddT53XHg94bVyH6wHcTWPV/Dh8xn5irwb80sNPkLyD/mOuCKuvieu0Fftgxc5/YlO9N9PTb56LnGVy6spvU/eXazPwMdy+IDPybtdvEOfIR0f6F30LnqXvmhfvxVdMq9LrOGuJ6JFOtr05729vTnP9rOzs+x+CWIVAOvr61XHydtMn3t7e2sNd1dEuP9ecz3hv605LrxP7oP1iOOcuPJ5AHyAD/ABD/bxK3zudRT8AX+Es4/Qu/P5E3yAD/BxYnV3qu4FH+ADfICPXH6gf7XYp6T+oP5I9bGfSv/KLGU0JCvlvd7nXaT98l5Xk14DS51nTclx+mVE3l4yxJewkOdRzXHh9WoohTbug/UgrlpcgY/RwkwI8lVjbyqRd8FHju/hc/ABf6RnEcEf8IfjT/QV+iqeORbWr9S1Pj6oz9uZB/AH/AF/LPYnwr4h9Qf1xyrXH9Zwl51LOFhxf3/frF60ySpmPB5n9+u1EjXc9ctSzXHuc/VkvBruek205Hyp6wn987gP1kPxSlz5QcLgw+cz8lWbz8EH+BDf6s2ykLfBB/iI9R78AX/E9QD8AX/AH4v8SV272C+AP+AP+CPfTwMf4AN8vAx8DP76669verp8Op1aw/vVq1fWXHdPcPz666/N1dVVdv/m5qbtf/PmTdVx7nM/ffpkx15fX9v5+86X2q9mvzbuo2lYjzZeiSuPZ/Dh8xn4SOPj4ODAcnANDzzVuNJ93NzcFPPZU72PLt6FB9ElJfGxyjwIztHty/K547tVxkdJfgj5/qnyIDgH58vinD7Dok4C59SDcX8PHqzrl/TVy+Qr+nCx/lLeHfzxxx/fhsPh7AlzPeGmzT1x3vd3NTb0tIv+V3Oc+1xdhP6nRr+2vvOl9k8mEzuW+2is0cR6NBZTxFWLJ/Dh8xn4aPM7+GjfqAIf83wPPsBHrOPgD/gjrgfgD/gjVa/BH/AH/JHvn4AP8AE+wEdffxV9tZr6yoam6ol2PUUgaxf9Wc3vDx8+WEPil19+sSZubn84rbrmOPe58RTbvvOl9ruBq9xH07AebbwSVx7P4MPnM/ABPmI+q8FH+G9DviOuiKvviSv0Vbf+hM9Xm89zebWvHiDvkne/J+/C5+Td0r5HjU6Ez4kr4op+oizG6Iu+bmRbXtJPXvX+rjXc5cGuTQlCfw6nH7vp6bn9fdNf+z43N1W477hwfzjNvea4minPfZ/Lffj4YT389Gzw8b45OzuzJ6rddyEs9eWV1H7iirg6OTkxrnLx8aPjKoxB+MPH42OtB7qkW7f9aHywHqxHaR0R5tJcXu3TCegr9JX05bL1IHxOvlomX/X1A+BB4oq4op8ot4mafip1VKtnluXzp553bWjqzs7OzLNd3kRuarASxuXlZRNPjQ33q6BTk353d7fqOPe54TT3kvOlrqdvCnrJ53If7TqzHiMDu3BAXB03W1tb9kMc+AAftdPDybv9/AkPLuoL8i551/EO+Hi5+JAO+/jxI/XH0VFvfYZuR7e7uoX63PMn/PFy+YP642XXHyX6gX4i/d2u/vZ984c13PW4v54U0abXpvb3/dRgN3U9t18euGq4qyFXcxzT3JnmPh6Ps3FHXLXxoV9IJRyc51cfHlP7mYLOFHSmoL+MKejicPi89eZHl8AfoW6FB+FBeBAeFEf26Wj4g/ojrk/hD/gD/oA/4I9+/qS/m+7vmqXMxsaGNdq1qUCR2NCTJWr0vX371pp9uf3uWP27muPc5/777792TvnwlZwvdT3TaTthmPsYzL4D1oO4cngGHz6fka/afE7e9Xz3GPjQ968NHlzUF4+xHrHeAR+Piw/WI6+/nxI+3LX01QnoduqorjoSHoQH6TPk+yzgA3yAD/BR06d9SjqR/u7rRuthDXfZwcTeuH2eim7/2tqaNeOd91DpcaFXvBruX758sQZEzkO+63P1OqU27qNpWA8/g4C4ar2uwceZ/ZinDXyAj9hTD3yAj5S+gD/gj1jXwh/wB/yxOLsDfdXqS+raxRlw6Cv0FfpqMT8oX1Cft57d8Af8kev/rhJ/mKWMGuaxZ+Zo5D3xujzdf/75ZxMZk8mk02sx5wU/HA4NbHd3dzPv7BoPeX1u+Now98F6KH6Iq9Z7HXzMe5SRr9oZHeADfKQ8LsEH+Ij1F/rKe2iDD/ABPhY9sl29CD7AB/gAH319GOoP6o/nUn8s4wUPD8KDKR60hrteV1JjTtv29nYjn6rQr0x+PLn9ejJdDXcl2Jrj5DOrz3WvDuvJIec/23U+d1x4PeHr+dwH60FczeMVfPh8Rr4i78b8Aj7AR6x30CVeD4IP8AE+/jdbn4AP8AE+wEdJ/4L6g/qD+oN+Yl+fkvpjNesPs5TRE0ylnu2xF6OEhp6QVxIp8eiLPYjUpNf51UhfxkNe1xN+JvfBemiWAHHlPWDBh/d+I1+1MzrAx3L4iL3XHbaIK+IqnmFD3iXvonfzs6HC7yaXV/tmSpF3ybvfk3fh8+5ZbejE5XRirg9BviJffU++Iq7IV6WzLqk/nl79YQ13PaG+jPe6a5BrYdfX15vY2zD0Zsp5s9/e3lrD3QXHMl7w4bVzH+13yXoQV84TC3x4z1GXa8AH+AAfizNTwEfrKYku8Z6S8Af8Ec94Ah/gI+W5Cn/AH3HdD3/AH/DHiT1QmupvgQ/wAT5eBj7MUmZnZ6cZj8eWEPb395vT09Nmc3PTmvBXV1fN4eFhdr+Et5rpGlhac5z7XBni6zzyPCo5X+p6Li4u7Nq5j4E1CliPQxsUSly1eAYfPp+Bjzafgw/Pd+ADfMR6B3yAj5Qehj9+HH/o+7++vqb+6Ki/qKMW61P4HD6Hz/P9G/ABPl4CPkr0A/1E+rtd/e371lfWcJedSzgYS006vUqmTVYxasbn9uv1GDV4VYjUHOc+V0+aqjEqO5qS86WuJ/TP4z5YD8UrcfXa8KS3HcCHz2fkqzafgw/w4fJDyNvgA3zEeg/+gD/iegD+gD/gj1Zfp+pe8AE+wAf4yOWHsE+FvkJfoa/y/eZVwodZymxsbMz5oMceU13e7O5YeanXHOe84OWhp+Rzc3NjjffYI77Pw1H7p9OpNRe5j8HsO2A9iCtn+QQ+/mc2o4J89dZ+3CTvem/hx8BH7B3LejzuesTemKwH65GaKQR/zPOHy53oduoP1W/gA30V9wseQ1/B53mva9aDehB8gI+Sfit8fr98bg132cHEHkKlXuhra2vWrF/Ge915vqtprundKU/Aks/V69/auI+mYT2+zWYJEFetZxz4OJvNqAAf4COeNQI+wEdK78Af8EesP+EP+AP+8J674KP1bIc//EwD8AE+SvpJ6Cv0FfwBf/TN+lyl+twsZdQw39rasqa1fvk6OjpqRqOR/f3g4KA5Pj7O7pf3uixlJpNJ1XHuc4fDoYmVu7u7ovOlrid8PYf7YD0Ur8SVxzP48PmMfNXmc/ABPlJ8Dz7AR6z34A/4I64H4A/4A/7I18vgA3yAD/BR0k9DX6Gv0Ff5fvMq4WPw+++/m6WMGubaJBT0FE/4yomePs/tV4GuV5TevHlTdZxeQdXnfvr0yV5DlAe8e8Wh63zuuPB6nB1ikG05AAAgAElEQVQN99EOn2U9iCvwkc5n4IO8G/OL+EPbq1evfhgPhpYy8OC8voDPvf4iX5GvUvnK2Xc8tm53D8qkdHlfHQHOwXlcZ1IPet0KPsAH+PDWH0+ZB+lfDWe20PTh6MPR323rloU+3J9//vlNE4udcNawFz2xXvp3NXclrNWorznOnUeiQhcmkGqrPb/+vSbJauM+Gmu2sx7fLPkTVy2ewIfPZ+Cjze8xPtTY01aa94mreZ4krtJxBZ+jS5RXwAf4iOuDZXRJzFPLxpX7HHRi+2YxfA6fp+r3x8LHfeGcPsNiP2eZvBvruGXzLuvBeoR55rnivK9eBh/o3ZTeNUuZz58/W7Na2/X1dXN4eNiMx2P7uyawn56eZvdrUKo+WL/w1RznPjee5t53vtT+cIot98F6KF6JK49n8OHzGfmqzefgA3yk+B58gI9Y78Ef8EdcD8Af8Af8ka+XwQf4AB/go6Sfhr5CX6Gv8v3mVcKHDU11k2hFEPLLUdEdTzGWz3tqP1Ns73eKrb5j1mMw+w70akpNPOrff/36tZFlg2LZvZbp/nsujlP7mebONHemuTPN/SGmuYeWMuSrrzZ43ekL8i55l7z7PPKuwyr6Ct3ubI6Uy9Ht1B/w+WI/hX4J9bn0PnqXfmJXv5X+1WrWg9Zw393dbeKp0goGCSg9vR5PHddUWbdf9iVKHm7acOlx4eeqMSorg5Lzpa6na4pt6fVwH36dWY/+6eHEVVl+AOeL+ZN8BX+I67SBD/CBvtor0p/okqenS2TbF+cx/b2kHoAH4cGUjgbnTw/n1OfvG/IV+Yp8la/7wQf4AB/d+DBLGTXMt7a2rOF9eXnZHB35qcEHBwfN8fFxdr+8jNQ0kDl8zXHuc7umuZdeTzzFtvS48Hq5j3adWQ+PA+Kqbnp4F+6IK+IqFR/kXfJurC/Iu+Td0WhkjVx04qL+Bh/gA3zM54ewfgUf4AN8gI9QP+T6W9Qf1B999YfiSG9c1vQ3iSviKhVX1nDX63///POPFTjv3r0z//ZQtMg3PbdfT7io4a6EVnOcmvz6XPfqsJ4w1+b+e831hK/n1xwXXi/3wXrEcU5c+TwAPsAH+IAH+/gVPvc6Cv6AP2IdDT7AR6rOQl+hr9BX6Cv0VXkfCn2FvkJfvc72TcHH08OHWcooaEs922OvRhnaq0muRn3sTVXivS2C0fk1uLXL06jLezvcx32wHoo74sp7wIIP75lHvmpndICP5fARe687bBFXxFWsd8i75N1Yt5J303k3l1f7ZkqRd8m735N34fP8rAh04vwsO/gcPofP097r6guCD/ABPrrxYQ13PaGe8pQNvRhz+yWI9SVrKnvKi7Tvc29vb63h7hbKeT/2HRfuz/255nq4j/eNvPlZD++tTFx5b13wAT6UH8i7i97r7odieBD+AB/go8vTHH2FvkrFB/oKfYW+mteXYf0OPsAH+AAffX1B9BX66inrK7OU2dnZMTsYbXqt7fT0tNnc3LS/X19fN4eHh9n9Gqaihr0Gr9Yc5z5XgxYEInkelZwvdT0XFxd2LPfR2PBZ1uPQBtwQVy2ewYfPZ+Cjzefgw/Md+AAfsd4BH+AjpYfhD/gjrofgD/gD/sj3C8AH+AAf4KOkv4e+Ql+tsr6yhrvsXEo922OPdb12pgavflnq8nrPebPryXg1RmVHs4yHvD5Xr5Vq4z4as4tgPcb2xgVx1c5EAB/eGxJ8tDM6wIf3vgMf4CP2TgUf4CM1Uwj+gD/iWVXwB/wBf+S9t8EH+AAf4KOkv4e+Ql+tsr4yS5mNjY05/yUNML28vLSG5fb2tj01HXrzhPvdsfJwqjnOfa489NTsv7m5KTpf6nqm06k13LmPwew7YD2IK2d1AT58PiNftfmcvOu91nL40FtXKhQeggc1a0QbPLioL8hX5KtYf5Kv+vNVjf6+Lx50WO2rE9Dt1FFddSQ8CA/SZ8j3WcAH+AAf4CPXh0VfPQ99ZQ132cGcnJxYA0Ae6noFTE9Ja1OzQVYtuf0S+QqC2uPCz1XD/cuXL0XnS12PXv/Wxn009qMH69HGK3HV4hl8+HwGPnw+Bx/gI+Zt8AE+Yr0Hf8AfqXoA/oA/4I98vQw+wAf4AB99/TT0FfoKfZXvN68SPsxSRg3ara0ta67ryaKjo6NmNBpZE/vg4KA5Pj7O7tdTgAqWyWRSdZz73OFwaOfVr3cl50tdT2iHw32wHopX4srjGXz4fEa+avM5+AAfKb4HH+Aj1nvwB/wR1wPwB/wBf+TrZfABPsAH+Cjpp6Gvnra+Ul9SbyTTFy3vC9PfXeybC+fWcJf9SOyxVerprifT1XBXozv23inxdFcgi5j0ZF3KM/Pdu9bTqOt69LqxNu6jsTcFWI99S5DEVdMIP+DD5w/w8X82awN8eE9F8AE+Yn0BPsBHynMU/oA/4roG/oA/4A8/8wN8eM/2uH8Bf8Af4AN89PVbqT9Ws/4wSxmJBbfAalprgGnoFaRBBrn9GoiifytiqTlO59TnKvD0Zw08dZ7TXedzx4XXE14r98F6EFfzeAUfPp+Rr8i7Mb/U4CNsroR8R1wRV98TV+ir9gfynP5EJ/rvpyZfPZe4yuXVlN4n7y7WZ+BjOXzA5+TdLt6hz5COD/Quehe9S1+0r9+KLpnXJdZw1xPRIh1t+vPe3t6cZ/vZ2Vl2vwSxCoD19fWq4+Rtps+9vb21hrsrItx/r7me8N/WHBfeJ/fBesRxTlz5PAA+wAf4gAf7+BU+9zoK/oA/wtlH6N35/Ak+wAf48LPT0FfoK/RVeR8K/oA/4A/4w80efQ596sFvv/32TX5z0+nUGu4bGxv2/6V/V4P+v//+m1m+lB7nzqNG+08//TRr6NeeX/9e/vHauI/2BxPWo7EfcIirFs/gw+cz8NHmd/Dh+Q58gI9Yt4AP8JHSw/AH/BHXR/AH/AF/5PsH4AN8gA/wUdJfRF+hr1ZZXw3+/vvvb/Jf13AH51mpoC/9u7zXRagyya85ThY0+vcfPnywRrm8zZY5vz5Hry3o107uozEvfNaDuArxCz58PgMfq593xSOhR14fL+XwUfM5xNXqx1VfHMX7ybsPm3dr8IlObPHpdAH5inz1XPIVOKc+J1+tfr4C5+AcnK8+zl90HaWhqXqaS83q1FRtN202t//nn382Ia8mb80UX6bYpqfYuqdPWQ/i6vj42H6MEi7v7u7sB6k+PKb2MwX9aU9BJ+/CH+Dc6w/yFflqNBrN8R34AB8pnqT+aOsI8AE+wMfAflBM9SHAB/gAH+Ajlx/Cfltcf0iIyqu8pr+JLkGXSJfFcTVQw12Dqvqm5ub2M3WbqdtM3Wbqdl/+YOr2ak7dlhiBPxp7Q0s/PItgx+PxzGJNP2bH+TG1H3yAD/eGXxgfxBX6Cn2FvkJfeX7o49dwEGrf90Zd2/29okvQJeiSptEbcuj27rqGvOu/H3Q7uj2l221oqn7R6Zs2m9vPtGqmVTOtmmnVffmDadXz06olYsm7TfPc+CMUlV+/fp0N+35u9wGfezyG6yhMis/IV+QrPRGnHxPBuc9zD4WPXF515yNfka9SeEzFh37kLtVX8Dlx9RBxRb4iroirRf0En7f1Bf2S7vwQfj81fP7U86413PVkXt907Nx+AUhfyPr6erO356cmv3//vumaGuv2397eWuPJfamlx+UmVHMfrIfijrjy096Z5s40d6a5M81dTYg+foXPW90Cf8Afwou0cahrwQf4iOsa9BX6Cn2FvkJf9etL+j6LfTH4A/6AP14Gf5ilzM7OThN7ZpZ6iKswVVGyu7trXoKlxzkvpfPzc2v2y/NoGS9jeVZfXFzYEw3cx8AaBazHUUNceQ9c8OHzEvhoPS7BB/hIzYQAH+Aj9qqEP+CPWNf/CP4o8U4lX5GvyFd+5kc8wwl8gA/wAT76+ns/gs9L+nv3ma9K9EPK0x29i959KL1rDffPnz9Xe8467zu9HqEGr4BS4lUbe+bpyXgBUa/tprzCSjxw9Tq/Nu6jsddVWI+xvXFBXDX25gj48B6w4KP1IgQfr40zwMe8xzz4AB+xVyn8AX+g2/Ne1+ADfIAP8FHSv0Bfoa/QV3kvePABPlYZH2Yps7GxMbN00Suza2trNu1bDcvt7e3m7u4uu98dK6/LmuPc58pDT+e8ubkpOl/qeqbTqTVPuI/B7DtgPYgr52MJPnw+I1+1+Zy8631ec/jQW1cqpB+CB+U1pw0eXNQX5CvyVaw/yVf9+apGf98XDzqs9tUJ6HbqqK46Eh6EB+kz5Pss4AN8gA/wEfqZ9+k96qinV0dZw112MKGHkF6p0FPS2tRskFVLbr8WXUEgb66a48LPVcNdU31Lzpe6Hr0Oo437aOxHD9ajjVfiqvXUAx8+n4EPn8/BB/iIeRt8gI9Y78Ef8EeqHoA/4A/4I1/3gg/wAT7AR19fDH2FvkJf5fvNq4QPs5RRg7bWe915QukpQAXLZDIxb+A+r6h4/3A4tKa+fr1LecqmPJZir3gJG2cPwH2wHooP4mprNhMBfPi8RL46sFkb4AN8pDwVwQf4QF/lZxGBD/ABPsBHX52JvkJfoa8G5pSQ6guBD/DxXPCxjBc8OhGdmNKJ1nCX/YgSo3vFXa/Rh76E7969y+7Xk+lquEuA1Bwnv3V9rl6tF/D0ZJ3zYe86nzsuvF69bqyN+2jsTQHWg7gCH+l8Bj7IuzG/wB+e78EH+AAf6N2+egDd7vUF/AF/xPUy+AAfqX4K+gp9hb5CX6GvyvvNq6SvzFJGT8A6gaCmtQaYhl5BGmSQ2y+hoX+rJFJznM6pz1WTXn9WI915Tnedzx0XXk94rdwH60FczeMVfPh8Rr4i78b8UoOPkPxDviOuiKvviSv0VfvgRU5/ohP991OTr55LXOXyakrvk3cX6zPwsRw+4HPybhfv0GdIxwd6F72L3qUv2tdvRZfM6xJruOuJaJGONv15b29vzrP97Owsu1+CWAXA+vp61XHyNtPn3t7eWsPdFRHuv9dcT/hva44L75P7YD3iOCeufB4AH+ADfMCDffwKn3sdBX/AH+HsI/TufP4EH+ADfJxY3Z2qe8EH+AAf4COXH+hfLfYpqT+oP1J97KfSvzJLGQ3JqvVedx52CnA16TWwNPasKfF0lyG+hIU8j1KeTgcHrRdQl2eehlJo4z4G9gMG63Fkg0KJq6YRfsCHzx/go/VUBB+j2cwQ8AE+Yn0BPsBHaqYQ/PHj+KPEO5X1+HHrUVKfsR6sR1z3o6/QV+gr79kOPvKzHu+TP0r0Q2rGAPmKfPVQ+coa7rJzCQcr7u/vm9WLNlnFjMfj7H69VqIGr4BSc5z7XD0ZLyGn10RLzpe6ntA/j/tgPRSvxJUfJAw+fD4jX7X5HHyADzdoPORt8AE+Yr0Hf8AfcT0Af8Af8Edjb2an6l7wAT7AB/jI5YewT4W+Ql+hr/L95lXCx+Cvv/76pqfLp9OpNbxfvXplzXX3xPuvv/7aXF1dZfdvbm7a/jdv3lQd5z7306dPduz19bWdv+98qf1q9mvjPpqG9WjjlbjyeAYfPp+BjzQ+9CaEcnANDzzVuNJ93NzcFPPZU72PLt6FB9ElJfGxyjwIztHty/K547tVxkdJfgj5/qnyIDgH58vinD7Dok4C59SDcX8PHqzrl/TVy+Qr+nCx/lLeHfzxxx/fhsPh7AlzPeGmzT1x3vd3NTb0a77+V3Oc+1xdhP6nRr+2vvOl9k8mEzuW+2is0cR6NBZTxFWLJ/Dh8xn4aPM7+GjfqAIf83wPPsBHrOPgD/gjrgfgD/gjVa/BH/AH/JHvn4AP8AE+wEdffxV9tZr6yoam6ol2PUUgaxf9Wc3vDx8+WEPil19+sSZubn84rbrmOPe58RTbvvOl9ruBq9xH07AebbwSVx7P4MPnM/ABPmI+q8FH+G9DviOuiKvviSv0Vbf+hM9Xm89zebWvHiDvkne/J+/C5+Td0r5HjU6Ez4kr4op+oiyw6Yu+bmRbXtJPXvX+rjXc5cEeTkQPpx+npqczHZnpyPH09DB+cn8mrhbjpg93TN1m6raISjmavEveJe+emHBDl7xvzs7OenUb/AF/wB+L/OnyB/gAH+ADfMT6OqUvqGu9/tbDjfoBQm/T19T15N1F3UZcEVdxXYcuWU1dYkNTd3Z2Zp7t8iY6PvZTalNTfMP99zlVWImn73xMFb5s4inXrMdivJ6fn1szQr7+xNWFNanA+cCsVCSud3d3q/KcyzvE1chiSTzBNHemuT/UNHfy1ZbxFvmq1Tvk3ZeXd5UDPn78iN49Ouqtz8DHy8NHSV0Df8Afcb8A3Y5ufwm6vUQ/0E+kn9jVT73vvo813PW4fzg1OTV1PbdfHrhqYInYa47Tqxbj8dh+IZVwcJ5G7r/XXM8qTbFVkmA9Wm9l4gp8MM39tTW442n34AP+EH+CD/CRyg/oqzY/gA/wAT4W9UOqzqKO2jcrSudNT/1B/QF/wB/wB/wR1999fUrqc+rzVP1hljIbGxvW0NEmglGw6MkSNcLfvn1rzfDcfnes/l3Nce5z//33XzunfPhKzpe6num0nTDMfQxm3wHrQVw5PIMPn8/IV20+J+96vnsMfOj71wYPLuqLx1iPWO+Aj8fFB+uR199PCR/uWvrqBHQ7dVRXHQkPwoP0GfJ9FvABPsAH+Kjp0z4lnUh/93Wj9bCGu+wVurxhuzy61tbWrBnf56natV8N9y9fvlgDosRLLb4evU6pjftoGtbDe10TV63XMfjwXsfgA3zAH/lZEuADfIAP8FFSD6Cv0FdxXQd/wB/wB/wBf5TNGqI+n/e0hz/gj1XmD7OUUcM89nQajbwnXpenuzyy1SSfTCadXos5b/bhcGhPtt/d3c28gWs85PW54Wtf3AfrofghrloPYPAx71FGvmpndIAP8JHygAUf4CPWX+gr76ENPsAH+MjP+AIf4AN8gI++Pgz1B/XHc6k/lvGChwfhwRQPWsNdryupMadte3vbfOxCPz/5FeX268l0NdyVYGuOk0+5Pte9Oqxftpx/edf53HHh9YSv53MfrAdxNY9X8OHzGfmKvBvzC/gAH7HeQZd4PQg+wAf4+N9sfQI+wAf4AB8l/QvqD+oP6g/6iX19SuqP1aw/zFJGTzCVerbHXowSGnpCXkmkxKMv9iBSk17nVyN9GQ95XU/4mdwH66FZAsSV94AFH977jXzVzugAH8vhI/Zed9giroireIYNeZe8i97Nz4YKv5tcXu2bKUXeJe9+T96Fz7tntaETl9OJuT4E+Yp89T35irgiX5XOuqT+eHr1hzXc9YS6FnEZD3UJYi3s+vp60+X1nvNmv729tYa7C45lvODDa+c+WI+zs7OGuPJ4Bh/eU5F81XrmgQ/wkeJ78AE+lB9ynIG+Ql/BH+AjlwfgD/gD/pjPD2FfBHyAD/ABPvp0NPX5atbnZimzs7PTjMdja7jv7+83p6enzebmphVdV1dXzeHhYXa/AkPNdA0srTnOfa4GOuo88jwqOV/qei4uLuzauY+BNdJYj0MbFEpctXgGHz6fgY82n4MPz3fgA3zEegd8gI+UHoY/fhx/6Pu/vr6m/uiov6ijFutT+Bw+h8/z/RvwAT5eAj5K9AP9RPq7Xf3t+9ZX1nCXnUs4GEtNOr1Kpk1WMWrG5/br9Rg1eFWI1BznPldPxqsxKjuakvOlrif0z+M+WA/FK3H12vCkN0fAh89n5Ks2n4MP8OHyQ8jb4AN8xHoP/oA/4noA/oA/4I9WX6fqXvABPsAH+Mjlh7BPhb5CX6Gv8v3mVcKHWcpsbGzM+aDHHlNd3uzuWHmp1xznvODloafkc3NzY4332CO+z8NR+6fTqTUXuY/B7DtgPYgrNxMBfPzPbEYF+eqt/bhJ3vXewo+Bj9g7lvV43PWIvTFZD9YjNVMI/pjnD5c70e3UH6rfwAf6Ku4XPIa+gs/zXtesB/Ug+AAfJf1W+Px++dwa7rKDOTk5saZ1rYf62tqaNetrjws93dU01/TuZTzkdV69/q2N+2ga1uPbbJYAcdXiGXx4zzjwAT7iWSPgA3ykPBXhD/gj1rXwB/wBf/iZOOCj9aSGP/Iz4NBX6CvwAT5K+nvoK/TVKusrs5RRw3xra8tEw+XlZXN0dNSMRiNrYh8cHDTHx8fZ/fJeV/N8MplUHec+dzgc2nnv7u6Kzpe6nvD1HO6D9VC8Elcez+DD5zPyVZvPwQf4SPE9+AAfsd6DP+CPuB6AP+AP+CNfL4MP8AE+wEdJPw199bT1lRqTeiOAvmh5X5j+7mLfXDgf/P7772Ypo4a5NgkF/coUvnKip89z+1Wg6xWlN2/eVB2nV1D1uZ8+fbLXEOUB715x6DqfOy68HmdHw320w2dZD+IKfKTzGfgg78b8Iv7Q9urVqx/Gg6GlDDw4ry/gc6+/yFfkq1S+cvYdj63b3YMyKV3eV0eAc3Ae15nUg163gg/wAT689cdT5kH6V8OZLTR9OPpw9HfbumWhD/fnn39+08RiJ5w17EVPrJf+Xc1dCWs16muOc+eRqNCFCaTaas+vf69Jstq4j8aa7azHN0v+xFWLJ/Dh8xn4aPN7jA819rSV5n3iap4niat0XMHn6BLlFfABPuL6YBldEvPUsnHlPged2L5ZDJ/D56n6/bHwcV84p8+w2M9ZJu/GOm7ZvMt6sB5hnnmuOO+rl8EHejeld81S5vPnz9as1nZ9fd0cHh424/HY/q4J7Kenp9n9GpSqD9YvfDXHuc+Np7n3nS+1P5xiy32wHopX4srjGXz4fEa+avM5+AAfKb4HH+Aj1nvwB/wR1wPwB/wBf+TrZfABPsAH+Cjpp6Gv0Ffoq3y/eZXwYUNT3SRaEYR8ZlR0x1OM5fOe2s8U2/udYqvvmPUYzL4DvZpSE4/691+/fm1k2aBYdq9luv+ei+PUfqa5M82dae5Mc3+Iae6hpQz56qsNXnf6grxL3iXvPo+867CKvkK3O5sj5XJ0O/UHfL7YT6FfQn0uvY/epZ/Y1W+lf7Wa9aA13Hd3d5uTkxMreOOp83p6PZ4aG05lZ6owU4VXeaqwMAE+GrMpkkiozQ9h/tAPILL50daXV1L7z8/P7VjWg/UgrhZ5GXyc2RwYbeQrdAm6ZC+r68EH+AAf4KOk7ke3t30R9BX6yulL6g/qD/qief4EH2l8mKWMGmlbW36qONN4mcbrvJzdtOGa+GDq9tOeui3RUDI9PcwD8ixTEtUQiJr8wLTq9LRqNQXJu40NeSaujm0GinBJ3r20N5PAx8i+A/DR5k/w4fU5+EBfjUZtfkBfoa+UH3P1GfwBf8TxAX/AH/DHPH+G+TPGh3hWb1zW9D3Iu+TdVN61hrte//vnn39MwL17987828Ogk296br+eWFXDRAFbc5yaTfpc9+qwnrhxRXbX+dxx4fWEr+dzH6wHcTWPV/Dh8xn5irwb8wv4AB+x3kGXeD0IPsAH+PA/AsIf1IN9dSb8AX+k+inUH9Qf8Af8AX+U95tXqf4wSxmJ6VLP9tirUYb2aoKrUR97U5V4byvwdH4Nbl3GQ17XE/phcR+sh+KOuPIesODDe+aRr9oZHeBjOXzE3usOW8QVcRXrHfIueTeeGUPeTefdXF7tmylF3iXvfk/ehc/zsyLQifOz7OBz+Bw+T3uv04drZxNQD87HB3p3Xu9aw11PqKe8qUo83SWIFWSayt7laZTzbL69vbWGuwvUZTyiw2vnPlgPzRggrlov4xjb4AN8gA/vRQk+5r3m4PP3DfgAHzmeBB/gI5xhBX/AH6m6l/qD+iM1qwr+gD/gj3l9GeZP8AE+VhkfZimzs7NjdjDa9LrL6elps7m5aU34q6ur5vDwMLtfwkKiU4MMa45zn6tBJDqPPI9Kzpe6nouLC7t27mNgjWbW49AG3BBXLZ7Bh89n4KPN5+DD8x34AB+x3gEf4COlh+GPH8cf+v6vr6+pPzrqL+qoxfoUPofP4fN8/wZ8gI+XgI8S/UA/kf5uV3/7vvWVNdxl51Lq2R57qOu1MzV4VYjUeq+rya8n49UY1esYy3jI63r0Wqk27qMxuwjWg7gK8Qw+vGcc+GhndJB3vScv+AAfsaci+AAfqZlC8Af8Ec+qgj/gD/gj78kLPsAH+AAfJf099BX6apX1lVnKbGxszPmga4Dp5eWlNcK3t7ebu7u77H53rDycao5znysPPTUHb25uis6Xup7pdGoNd+5jMPsOWA/iys1EAB8+n5Gv2nxO3vVeczl86K0rFQoPwYOaNaINHlzUF+Qr8lWsP8lX/fmqRn/fFw86rPbVCeh26qiuOhIehAfpM+T7LOADfIAP8BHOEOjTe9RRT6+Osoa77GBOTk6sASAPdb1ypKektanZIKuW3H4tuoKg9rjwc9Vw1/TukvOlrkevf2vjPhr70YP1aOOVuGrxDD58PgMfPp+DD/AR8zb4AB+x3oM/4I9UPQB/wB/wR75eBh/gA3yAj75+GvoKfYW+yvebVwkfZimjBu3W1pY11/Vk0dHRUTMajayJfXBw0BwfH2f36ylABctkMqk6zn3ucDi08+rXu5Lzpa4ntM/gPlgPxStx5fEMPnw+I1+1+Rx8gI8U34MP8BHrPfgD/ojrAfgD/oA/8vUy+AAf4AN8lPTT0FdPW1+pL6k3kumLlveF6e8u9s2Fc2u4y34k9tgq9XTXk+lquKvRHXvvlHi6K5BFTHqyLuWZ+e5d62nUdT163Vgb99HYmwKsx74lSOKqaYQf8OHzB/j4P5u1AT68pyL4AB+xvgAf4CPlOQp/wB9xXQN/wB/wh5/5AT68Z3vcv4A/4A/wAT76+q3UH6tZf5iljMSCW2A1rTXANPQK0iCD3H4NRNG/FbHUHKdz6nMVePqzBp46z+mu87njwusJr5X7YD2Iq3m8gg+fz8hX5N2YX2rwETZXQr4jroir74kr9FX7A3lOf6IT/fdTk6+eS+nxvP8AACAASURBVFzl8mpK75N3F+sz8LEcPuBz8m4X79BnSMcHehe9i96lL9rXb0WXzOsSa7jriWiRjjb9eW9vb86z/ezsLLtfglgFwPr6etVx8jbT597e3lrD3RUR7r/XXE/4b2uOC++T+2A94jgnrnweAB/gA3zAg338Cp97HQV/wB/h7CP07nz+BB/gA3z42WnoK/QV+qq8DwV/wB/wB/zhZo8+hz714Lfffvsmv7lwoq1uoPTvatD/999/M8uX0uM2NjZmFjI//fTTrKHv/nvN58g/Xhv30f5gwno09gMOcdU0whP48PkMfJB3Y34DH+Aj1hvwx9Q0FfzR5ksXH/AH/AF/5OtD8AE+wAf46OvfoK/QV05foq8W8wX4WE18DP7+++9v8l/XcAfnWSnRVPp3ea+rYSGT/JrjZEGjf//hwwdrlMvbbJnz63P02oJ+7eQ+GvPCZz2IqxC/4MPnM/Cx+nlXPBJ65PXxUg4fNZ9DXK1+XPXFUbyfvPuwebcGn+jEFp9OF5CvyFfPJV+Bc+pz8tXq5ytwDs7B+erj/EXXURqaql9T1KxOTdV202Zz+3/++WcT8mry1kzxZYpteoqtGxzLehBXx8fH9mOUcHl3d2c/SPXhMbWfKehPewo6eRf+AOdef5CvyFej0WiO78AH+EjxJPVHW0eAD/ABPgb2g2KqDwE+wAf4AB+5/BD22+L6Q0JUXuU1/U10CbpEuiyOq4Ea7hpU1Tc1N7efqdtM3WbqNlO3+/IHU7dXc+q2xAj80dgbWvrhWQQ7Ho9nFmv6MTvOj6n94AN8uDf8wvggrtBX6Cv0FfrK80Mfv4aDUPu+N+ra7u8VXYIuQZc0jd6QQ7d31zXkXf/9oNvR7SndbkNT9YtO37TZ3H6mVTOtmmnVTKvuyx9Mq56fVi0RS95tmufGH6Go/Pr162zY93O7D/jc4zFcR2FSfEa+Il/piTj9mAjOfZ57KHzk8qo7H/mKfJXCYyo+9CN3qb6Cz4mrh4gr8hVxRVwt6if4vK0v6Jd054fw+6nh86eed63hrifz+qZj5/YLQPpC1tfXm709PzX5/fv3TdfUWLf/9vbWGk/uSy09LjehmvtgPRR3xJWf9s40d6a5M82dae5qQvTxK3ze6hb4A/4QXqSNQ10LPsBHXNegr9BX6Cv0FfqqX1/S91nsi8Ef8Af88TL4wyxldnZ2mtgzs9RDXIWpipLd3V3zEiw9znkpnZ+fW7NfnkfLeBnLs/ri4sKeaOA+BtYoYD2OGuLKe+CCD5+XwEfrcQk+wEdqJgT4AB+xVyX8AX/Euv5H8EeJdyr5inxFvvIzP+IZTuADfIAP8NHX3/sRfF7S37vPfFWiH1Ke7uhd9O5D6V1ruH/+/Lnac9Z53+n1CDV4BZQSr9rYM09PxguIem035RVW4oGr1/m1cR+Nva7CeoztjQviqrE3R8CH94AFH60XIfh4bZwBPuY95sEH+Ii9SuEP+APdnve6Bh/gA3yAj5L+BfoKfYW+ynvBgw/wscr4MEuZjY2NmaWLXpldW1uzad9qWG5vbzd3d3fZ/e5YeV3WHOc+Vx56OufNzU3R+VLXM51OrXnCfQxm3wHrQVw5H0vw4fMZ+arN5+Rd7/Oaw4feulIh/RA8KK85bfDgor4gX5GvYv1JvurPVzX6+7540GG1r05At1NHddWR8CA8SJ8h32cBH+ADfICP0M+8T+9RRz29Osoa7rKDCT2E9EqFnpLWpmaDrFpy+7XoCgJ5c9UcF36uGu6a6ltyvtT16HUYbdxHYz96sB5tvBJXrace+PD5DHz4fA4+wEfM2+ADfMR6D/6AP1L1APwBf8Af+boXfIAP8AE++vpi6Cv0Ffoq329eJXyYpYwatLXe684TSk8BKlgmk4l5A/d5RcX7h8OhNfX1613KUzblsRR7xUvYOHsA7oP1UHwQV1uzmQjgw+cl8tWBzdoAH+Aj5akIPsAH+io/iwh8gA/wAT766kz0FfoKfTUwp4RUXwh8gI/ngo9lvODRiejElE60hrvsR5QY3Svueo0+9CV89+5ddr+eTFfDXQKk5jj5retz9Wq9gKcn65wPe9f53HHh9ep1Y23cR2NvCrAexBX4SOcz8EHejfkF/vB8Dz7AB/hA7/bVA+h2ry/gD/gjrpfBB/hI9VPQV+gr9BX6Cn1V3m9eJX1lljJ6AtYJBDWtNcA09ArSIIPcfgkN/VslkZrjdE59rpr0+rMa6c5zuut87rjwesJr5T5YD+JqHq/gw+cz8hV5N+aXGnyE5B/yHXFFXH1PXKGv2gcvcvoTnei/n5p89VziKpdXU3qfvLtYn4GP5fABn5N3u3iHPkM6PtC76F30Ln3Rvn4rumRel1jDXU9Ei3S06c97e3tznu1nZ2fZ/RLEKgDW19erjpO3mT739vbWGu6uiHD/veZ6wn9bc1x4n9wH6xHHOXHl8wD4AB/gAx7s41f43Oso+AP+CGcfoXfn8yf4AB/g48Tq7lTdCz7AB/gAH7n8QP9qsU9J/UH9kepjP5X+lVnKaEhWrfe687BTgKtJr4GlsWdNiae7DPElLOR5lPJ0OjhovYC6PPM0lEIb9zGwHzBYjyMbFEpcNY3wAz58/gAfraci+BjNZoaAD/AR6wvwAT5SM4Xgjx/HHyXeqazHj1uPkvqM9WA94roffYW+Ql95z3bwkZ/1eJ/8UaIfUjMGyFfkq4fKV9Zwl51LOFhxf3/frF60ySpmPB5n9+u1EjV4BZSa49zn6sl4CTm9JlpyvtT1hP553AfroXglrvwgYfDh8xn5qs3n4AN8uEHjIW+DD/AR6z34A/6I6wH4A/6APxp7MztV94IP8AE+wEcuP4R9KvQV+gp9le83rxI+Bn/99dc3PV0+nU6t4f3q1Strrrsn3n/99dfm6uoqu39zc9P2v3nzpuo497mfPn2yY6+vr+38fedL7VezXxv30TSsRxuvxJXHM/jw+Qx8pPGhNyGUg2t44KnGle7j5uammM+e6n108S48iC4piY9V5kFwjm5fls8d360yPkryQ8j3T5UHwTk4Xxbn9BkWdRI4px6M+3vwYF2/pK9eJl/Rh4v1l/Lu4I8//vg2HA5nT5jrCTdt7onzvr+rsaFf8/W/muPc5+oi9D81+rX1nS+1fzKZ2LHcR2ONJtajsZgirlo8gQ+fz8BHm9/BR/tGFfiY53vwAT5iHQd/wB9xPQB/wB+peg3+gD/gj3z/BHyAD/ABPvr6q+ir1dRXNjRVT7TrKQJZu+jPan5/+PDBGhK//PKLNXFz+8Np1TXHuc+Np9j2nS+13w1c5T6ahvVo45W48ngGHz6fgQ/wEfNZDT7CfxvyHXFFXH1PXKGvuvUnfL7afJ7Lq331AHmXvPs9eRc+J++W9j1qdCJ8TlwRV/QTZYFNX/R1I9vykn7yqvd3reEuD/ZwIno4/Tg1PZ3pyExHjqenh/GT+zNxtRg3fbhj6jZTt0VUytHkXfIueffEhBu65H1zdnbWq9vgD/gD/ljkT5c/wAf4AB/gI9bXKX1BXev1tx5u1A8Qepu+pq4n7y7qNuKKuIrrOnTJauoSG5q6s7Mz82yXN9HxsZ9Sm5riG+6/z6nCSjx952Oq8GUTT7lmPRbj9fz83JoR8vUnri6sSQXOB2alInG9u7tbledc3iGuRhZL4gmmuTPN/aGmuZOvtoy3yFet3iHvvry8qxzw8eNH9O7RUW99Bj5eHj5K6hr4A/6I+wXodnT7S9DtJfqBfiL9xK5+6n33fazhrsf9w6nJqanruf3ywFUDS8Rec5xetRiPx/YLqYSD8zRy/73melZpiq2SBOvReisTV+CDae6vrcEdT7sHH/CH+BN8gI9UfkBftfkBfIAP8LGoH1J1FnXUvllROm966g/qD/gD/oA/4I+4/u7rU1KfU5+n6g+zlNnY2LCGjjYRjIJFT5aoEf727Vtrhuf2u2P172qOc5/777//2jnlw1dyvtT1TKfthGHuYzD7DlgP4srhGXz4fEa+avM5edfz3WPgQ9+/NnhwUV88xnrEegd8PC4+WI+8/n5K+HDX0lcnoNupo7rqSHgQHqTPkO+zgA/wAT7AR02f9inpRPq7rxuthzXcZa/Q5Q3b5dG1trZmzfg+T9Wu/Wq4f/nyxRoQJV5q8fXodUpt3EfTsB7e65q4ar2OwYf3OgYf4AP+yM+SAB/gA3yAj5J6AH2FvorrOvgD/oA/4A/4o2zWEPX5vKc9/AF/rDJ/mKWMGuaxp9No5D3xujzd5ZGtJvlkMun0Wsx5sw+HQ3uy/e7ubuYNXOMhr88NX/viPlgPxQ9x1XoAg495jzLyVTujA3yAj5QHLPgAH7H+Ql95D23wAT7AR37GF/gAH+ADfPT1Yag/qD+eS/2xjBc8PAgPpnjQGu56XUmNOW3b29vmYxf6+cmvKLdfT6ar4a4EW3OcfMr1ue7VYf2y5fzLu87njguvJ3w9n/tgPYirebyCD5/PyFfk3ZhfwAf4iPUOusTrQfABPsDH/2brE/ABPsAH+CjpX1B/UH9Qf9BP7OtTUn+sZv1hljJ6gqnUsz32YpTQ0BPySiIlHn2xB5Ga9Dq/GunLeMjresLP5D5YD80SIK68Byz48N5v5Kt2Rgf4WA4fsfe6wxZxRVzFM2zIu+Rd9G5+NlT43eTyat9MKfIuefd78i583j2rDZ24nE7M9SHIV+Sr78lXxBX5qnTWJfXH06s/rOGuJ9S1iMt4qEsQa2HX19ebLq/3nDf77e2tNdxdcCzjBR9eO/fBepydnTXElccz+PCeiuSr1jMPfICPFN+DD/Ch/JDjDPQV+gr+AB+5PAB/wB/wx3x+CPsi4AN8gA/w0aejqc9Xsz43S5mdnZ1mPB5bw31/f785PT1tNjc3rei6urpqDg8Ps/sVGGqma2BpzXHuczXQUeeR51HJ+VLXc3FxYdfOfQyskcZ6HNqgUOKqxTP48PkMfLT5HHx4vgMf4CPWO+ADfKT0MPzx4/hD3//19TX1R0f9RR21WJ/C5/A5fJ7v34AP8PES8FGiH+gn0t/t6m/ft76yhrvsXMLBWGrS6VUybbKKUTM+t1+vx6jBq0Kk5jj3uXoyXo1R2dGUnC91PaF/HvfBeiheiavXhie9OQI+fD4jX7X5HHyAD5cfQt4GH+Aj1nvwB/wR1wPwB/wBf7T6OlX3gg/wAT7ARy4/hH0q9BX6Cn2V7zevEj7MUmZjY2POBz32mOryZnfHyku95jjnBS8PPSWfm5sba7zHHvF9Ho7aP51OrbnIfQxm3wHrQVy5mQjg439mMyrIV2/tx03yrvcWfgx8xN6xrMfjrkfsjcl6sB6pmULwxzx/uNyJbqf+UP0GPtBXcb/gMfQVfJ73umY9qAfBB/go6bfC5/fL59Zwlx3MycmJNa1rPdTX1tasWV97XOjprqa5pncv4yGv8+r1b23cR9OwHt9mswSIqxbP4MN7xoEP8BHPGgEf4CPlqQh/wB+xroU/4A/4w8/EAR+tJzX8kZ8Bh75CX4EP8FHS30Nfoa9WWV+ZpYwa5ltbWyYaLi8vm6Ojo2Y0GlkT++DgoDk+Ps7ul/e6mueTyaTqOPe5w+HQznt3d1d0vtT1hK/ncB+sh+KVuPJ4Bh8+n5Gv2nwOPsBHiu/BB/iI9R78AX/E9QD8AX/AH/l6GXyAD/ABPkr6aeirp62v1JjUGwH0Rcv7wvR3F/vmwvng999/N0sZNcy1SSjoV6bwlRM9fZ7brwJdryi9efOm6ji9gqrP/fTpk72GKA9494pD1/ncceH1ODsa7qMdPst6EFfgI53PwAd5N+YX8Ye2V69e/TAeDC1l4MF5fQGfe/1FviJfpfKVs+94bN3uHpRJ6fK+OgKcg/O4zqQe9LoVfIAP8OGtP54yD9K/Gs5soenD0Yejv9vWLQt9uD///PObJhY74axhL3pivfTvau5KWKtRX3OcO49EhS5MINVWe379e02S1cZ9NNZsZz2+WfInrlo8gQ+fz8BHm99jfKixp6007xNX8zxJXKXjCj5HlyivgA/wEdcHy+iSmKeWjSv3OejE9s1i+Bw+T9Xvj4WP+8I5fYbFfs4yeTfWccvmXdaD9QjzzHPFeV+9DD7Quym9a5Yynz9/tma1tuvr6+bw8LAZj8f2d01gPz09ze7XoFR9sH7hqznOfW48zb3vfKn94RRb7oP1ULwSVx7P4MPnM/JVm8/BB/hI8T34AB+x3oM/4I+4HoA/4A/4I18vgw/wAT7AR0k/DX2FvkJf5fvNq4QPG5rqJtGKIOQzo6I7nmIsn/fUfqbY3u8UW33HrMdg9h3o1ZSaeNS///r1ayPLBsWyey3T/fdcHKf2M82dae5Mc2ea+0NMcw8tZchXX23wutMX5F3yLnn3eeRdh1X0Fbrd2Rwpl6PbqT/g88V+Cv0S6nPpffQu/cSufiv9q9WsB63hvru725ycnFjBG0+d19Pr8dTYcCo7U4WZKrzKU4WFCfDRmE2RREJtfgjzh34Akc2Ptr68ktp/fn5ux7IerAdxtcjL4OPM5sBoI1+hS9Ale1ldDz7AB/gAHyV1P7q97Yugr9BXTl9Sf1B/0BfN8yf4SOPDLGXUSNva8lPFmcbLNF7n5eymDdfEB1O3n/bUbYmGkunpYR6QZ5mSqIZA1OQHplWnp1WrKUjebWzIM3F1bDNQhEvy7qW9mQQ+RvYdgI82f4IPr8/BB/pqNGrzA/oKfaX8mKvP4A/4I44P+AP+gD/m+TPMnzE+xLN647Km70HeJe+m8q413PX63z///GMC7t27d+bfHgadfNNz+/XEqhomCtia49Rs0ue6V4f1xI0rsrvO544Lryd8PZ/7YD2Iq3m8gg+fz8hX5N2YX8AH+Ij1DrrE60HwAT7Ah/8REP6gHuyrM+EP+CPVT6H+oP6AP+AP+KO837xK9YdZykhMl3q2x16NMrRXE1yN+tibqsR7W4Gn82tw6zIe8rqe0A+L+2A9FHfElfeABR/eM4981c7oAB/L4SP2XnfYIq6Iq1jvkHfJu/HMGPJuOu/m8mrfTCnyLnn3e/IufJ6fFYFOnJ9lB5/D5/B52nudPlw7m4B6cD4+0Lvzetca7npCPeVNVeLpLkGsINNU9i5Po5xn8+3trTXcXaAu4xEdXjv3wXpoxgBx1XoZx9gGH+ADfHgvSvAx7zUHn79vwAf4yPEk+AAf4Qwr+AP+SNW91B/UH6lZVfAH/AF/zOvLMH+CD/CxyvgwS5mdnR2zg9Gm111OT0+bzc1Na8JfXV01h4eH2f0SFhKdGmRYc5z7XA0i0XnkeVRyvtT1XFxc2LVzHwNrNLMehzbghrhq8Qw+fD4DH20+Bx+e78AH+Ij1DvgAHyk9DH/8OP7Q9399fU390VF/UUct1qfwOXwOn+f7N+ADfLwEfJToB/qJ9He7+tv3ra+s4S47l1LP9thDXa+dqcGrQqTWe11Nfj0Zr8aoXsdYxkNe16PXSrVxH43ZRbAexFWIZ/DhPePARzujg7zrPXnBB/iIPRXBB/hIzRSCP+CPeFYV/AF/wB95T17wAT7AB/go6e+hr9BXq6yvzFJmY2NjzgddA0zlha5te3u7ubu7y+53x8rDqeY497ny0FNzcDqdFp0vdT3uWO6jaViPNl6JK++lBT58PgMf4CPmsxw+9NbV5eVlES8RV8RVaVzV6CTiirgirqg/Qu/gVP5A76J33Qy0MD7gD/gD/oA/4A/PD336m34J/ZK4/31f+soa7rKDOTk5scaCPNT1ypGektamp89l1ZLbr+AVmGuPCz9XDXdN7y45X+p69Pq3Nu6jsR89WI82XomrFs/gw+cz8OHzOfgAHzFvgw/wEes9+AP+SNUD8Af8AX/k62XwAT7AB/jo66ehr9BX6Kt8v3mV8GGWMmrQbm1tWXNdT/QdHR01o9HImtgHBwfN8fFxdr+eAlSwTCaTquPc5w6HQzuvfoUtOV/qekL7DO6D9VC8Elcez+DD5zPyVZvPwQf4SPE9+AAfsd6DP+CPuB6AP+AP+CNfL4MP8AE+wEdJPw199bT1lfqSeuKZvmh5X5j+7mLfXDi3hrvsYGKPrVJPdz2Zroa7Gt2x906Jp7sCWcSkJ+tSnpnv3rWeRl3Xo8f9tXEfjb0pwHrsW4IkrppG+AEfPn+Aj/+zWRvgw3sqgg/wEesL8AE+Up6j8Af8Edc18Af8AX/4mR/gw3u2x/0L+AP+AB/go6/fSv2xmvWHWcpILLgFVtNaA0xDzycNMsjt10AU/VsRS81xOqc+V4GnP2vgqfOg6zqfOy68nvBauQ/Wg7iaxyv48PmMfEXejfmlBh9hcyXkO+KKuPqeuEJftT+Q5/QnOtF/PzX56rnEVS6vpvQ+eXexPgMfy+EDPifvdvEOfYZ0fKB30bvoXfqiff1WdMm8LrGGu56IFulo05/39vbmPNvPzs6y+yWIVQCsr69XHSdvM33u7e2tNdxdEeH+e831hP+25rjwPrkP1iOOc+LK5wHwAT7ABzzYx6/wuddR8Af8Ec4+Qu/O50/wAT7Ah5+dhr5CX6GvyvtQ8Af8AX/AH2726HPoUw9+++23b/KbCyfz6gZK/64G/X///TezfCk9TtPTnYXMTz/9NGvou/9e8znyj9fGfbQ/mLAejf2AQ1w1jfAEPnw+Ax/k3ZjfwAf4iPUG/DE1TQV/tPnSxQf8AX/AH/n6EHyAD/ABPvr6N+gr9JXTl+irxXwBPlYTH4O///77m/zXNdzBeVZKNJX+Xd7raljIJL/mOFnQ6N9/+PDBGuXyNlvm/PocvbagXzu5j8a88FkP4irEL/jw+Qx8rH7eFY+EHnl9vJTDR83nEFerH1d9cRTvJ+8+bN6twSc6scWn0wXkK/LVc8lX4Jz6nHy1+vkKnINzcL76OH/RdZSGpurXFDWrU1O13bTZ3P6ff/7ZhLyavDVTfJlim55i6576Zz2Iq+PjY/sxSri8u7uzH6T68JjazxT0pz0FnbwLf4Bzrz/IV+Sr0Wg0x3fgA3ykeJL6o60jwAf4AB8D+0Ex1YcAH+ADfICPXH4I+21x/SEhKq/ymv4mugRdIl0Wx9VADXcNquqbmpvbz9Rtpm4zdZup2335g6nbqzl1W2IE/mjsDS398CyCHY/HM4s1/Zgd58fUfvABPtwbfmF8EFfoK/QV+gp95fmhj1/DQah93xt1bff3ii5Bl6BLmkZvyKHbu+sa8q7/ftDt6PaUbrehqfpFp2/abG4/06qZVs20aqZV9+UPplXPT6uWiCXvNs1z449QVH79+nU27Pu53Qd87vEYrqMwKT4jX5Gv9EScfkwE5z7PPRQ+cnnVnY98Rb5K4TEVH/qRu1RfwefE1UPEFfmKuCKuFvUTfN7WF/RLuvND+P3U8PlTz7vWcNeTeX3TsXP7BSB9Ievr683enp+a/P79+6Zraqzbf3t7a40n96WWHpebUM19sB6KO+LKT3tnmjvT3JnmzjR3NSH6+BU+b3UL/AF/CC/SxqGuBR/gI65r0FfoK/QV+gp91a8v6fss9sXgD/gD/ngZ/GGWMjs7O03smVnqIa7CVEXJ7u6ueQmWHue8lM7Pz63ZL8+jZbyM5Vl9cXFhTzRwHwNrFLAeRw1x5T1wwYfPS+Cj9bgEH+AjNRMCfICP2KsS/oA/Yl3/I/ijxDuVfEW+Il/5mR/xDCfwAT7AB/jo6+/9CD4v6e/dZ74q0Q8pT3f0Lnr3ofSuNdw/f/5c7TnrvO/0eoQavAJKiVdt7JmnJ+MFRL22m/IKK/HA1ev82riPxl5XYT3G9sYFcdXYmyPgw3vAgo/WixB8vDbOAB/zHvPgA3zEXqXwB/yBbs97XYMP8AE+wEdJ/wJ9hb5CX+W94MEH+FhlfJilzMbGxszSRa/Mrq2t2bRvNSy3t7ebu7u77H53rLwua45znysPPZ3z5uam6Hyp65lOp9Y84T4Gs++A9SCunI8l+PD5jHzV5nPyrvd5zeFDb12pkH4IHpTXnDZ4cFFfkK/IV7H+JF/156sa/X1fPOiw2lcnoNupo7rqSHgQHqTPkO+zgA/wAT7AR+hn3qf3qKOeXh1lDXfZwYQeQnqlQk9Ja1OzQVYtuf1adAWBvLlqjgs/Vw13TfUtOV/qevQ6jDbuo7EfPViPNl6Jq9ZTD3z4fAY+fD4HH+Aj5m3wAT5ivQd/wB+pegD+gD/gj3zdCz7AB/gAH319MfQV+gp9le83rxI+zFJGDdpa73XnCaWnABUsk8nEvIH7vKLi/cPh0Jr6+vUu5Smb8liKveIlbJw9APfBeig+iKut2UwE8OHzEvnqwGZtgA/wkfJUBB/gA32Vn0UEPsAH+AAffXUm+gp9hb4amFNCqi8EPsDHc8HHMl7w6ER0YkonWsNd9iNKjO4Vd71GH/oSvnv3LrtfT6ar4S4BUnOc/Nb1uXq1XsDTk3XOh73rfO648Hr1urE27qOxNwVYD+IKfKTzGfgg78b8An94vgcf4AN8oHf76gF0u9cX8Af8EdfL4AN8pPop6Cv0FfoKfYW+Ku83r5K+MksZPQHrBIKa1hpgGnoFaZBBbr+Ehv6tkkjNcTqnPldNev1ZjXTnOd11PndceD3htXIfrAdxNY9X8OHzGfmKvBvzSw0+QvIP+Y64Iq6+J67QV+2DFzn9iU70309NvnoucZXLqym9T95drM/Ax3L4gM/Ju128Q58hHR/oXfQuepe+aF+/FV0yr0us4a4nokU62vTnvb29Oc/2s7Oz7H4JYhUA6+vrVcfJ20yfe3t7aw13V0S4/15zPeG/rTkuvE/ug/WI45y48nkAfIAP8AEP9vErfO51FPwBf4Szj9C78/kTfIAP8HFidXeq7gUf4AN8gI9cfqB/tdinpP6g/kj1sZ9K/8osZTQkq9Z73XnYKcDVpNfA0tizpsTTXYb4EhbyPEp5Oh0ctF5AXZ55GkqhjfsY2A8YrMeRDQolrppG+AEfPn+Aj9ZTEXyMRsc9jgAAIABJREFUZjNDwAf4iPUF+AAfqZlC8MeP448S71TW48etR0l9xnqwHnHdj75CX6GvvGc7+MjPerxP/ijRD6kZA+Qr8tVD5StruMvOJRysuL+/b1Yv2mQVMx6Ps/v1WokavAJKzXHuc/VkvIScXhMtOV/qekL/PO6D9VC8Eld+kDD48PmMfNXmc/ABPtyg8ZC3wQf4iPUe/AF/xPUA/AF/wB+NvZmdqnvBB/gAH+Ajlx/CPhX6Cn2Fvsr3m1cJH4O//vrrm54un06n1vB+9eqVNdfdE++//vprc3V1ld2/ublp+9+8eVN1nPvcT58+2bHX19d2/r7zpfar2a+N+2ga1qONV+LK4xl8+HwGPtL40JsQysE1PPBU40r3cXNzU8xnT/U+ungXHkSXlMTHKvMgOEe3L8vnju9WGR8l+SHk+6fKg+AcnC+Lc/oMizoJnFMPxv09eLCuX9JXL5Ov6MPF+kt5d/DHH398Gw6HsyfM9YSbNvfEed/f1djQr/n6X81x7nN1EfqfGv3a+s6X2j+ZTOxY7qOxRhPr0VhMEVctnsCHz2fgo83v4KN9owp8zPM9+AAfsY6DP+CPuB6AP+CPVL0Gf8Af8Ee+fwI+wAf4AB99/VX01WrqKxuaqifa9RSBrF30ZzW/P3z4YA2JX375xZq4uf3htOqa49znxlNs+86X2u8GrnIfTcN6tPFKXHk8gw+fz8AH+Ij5rAYf4b8N+Y64Iq6+J67QV936Ez5fbT7P5dW+eoC8S979nrwLn5N3S/seNToRPieuiCv6ibLApi/6upFteUk/edX7u9Zwlwd7OBE9nH6cmp7OdGSmI8fT08P4yf2ZuFqMmz7cMXWbqdsiKuVo8i55l7x7YsINXfK+OTs769Vt8Af8AX8s8qfLH+ADfIAP8BHr65S+oK71+lsPN+oHCL1NX1PXk3cXdRtxRVzFdR26ZDV1iQ1N3dnZmXm2y5vo+NhPqU1N8Q333+dUYSWevvMxVfiyiadcsx6L8Xp+fm7NCPn6E1cX1qQC5wOzUpG43t3drcpzLu8QVyOLJfEE09yZ5v5Q09zJV1vGW+SrVu+Qd19e3lUO+PjxI3r36Ki3PgMfLw8fJXUN/AF/xP0CdDu6/SXo9hL9QD+RfmJXP/W++z7WcNfj/uHU5NTU9dx+eeCqgSVirzlOr1qMx2P7hVTCwXkauf9ecz2rNMVWSYL1aL2ViSvwwTT319bgjqfdgw/4Q/wJPsBHKj+gr9r8AD7AB/hY1A+pOos6at+sKJ03PfUH9Qf8AX/AH/BHXH/39Smpz6nPU/WHWcpsbGxYQ0ebCEbBoidL1Ah/+/atNcNz+92x+nc1x7nP/ffff+2c8uErOV/qeqbTdsIw9zGYfQesB3Hl8Aw+fD4jX7X5nLzr+e4x8KHvXxs8uKgvHmM9Yr0DPh4XH6xHXn8/JXy4a+mrE9Dt1FFddSQ8CA/SZ8j3WcAH+AAf4KOmT/uUdCL93deN1sMa7rJX6PKG7fLoWltbs2Z8n6dq13413L98+WINiBIvtfh69DqlNu6jaVgP73VNXLVex+DDex2DD/ABf+RnSYAP8AE+wEdJPYC+Ql/FdR38AX/AH/AH/FE2a4j6fN7THv6AP1aZP8xSRg3z2NNpNPKeeF2e7vLIVpN8Mpl0ei3mvNmHw6E92X53dzfzBq7xkNfnhq99cR+sh+KHuGo9gMHHvEcZ+aqd0QE+wEfKAxZ8gI9Yf6GvvIc2+AAf4CM/4wt8gA/wAT76+jDUH9Qfz6X+WMYLHh6EB1M8aA13va6kxpy27e1t87EL/fzkV5TbryfT1XBXgq05Tj7l+lz36rB+2XL+5V3nc8eF1xO+ns99sB7E1TxewYfPZ+Qr8m7ML+ADfMR6B13i9SD4AB/g43+z9Qn4AB/gA3yU9C+oP6g/qD/oJ/b1Kak/VrP+MEsZPcFU6tkeezFKaOgJeSWREo++2INITXqdX430ZTzkdT3hZ3IfrIdmCRBX3gMWfHjvN/JVO6MDfCyHj9h73WGLuCKu4hk25F3yLno3Pxsq/G5yebVvphR5l7z7PXkXPu+e1YZOXE4n5voQ5Cvy1ffkK+KKfFU665L64+nVH9Zw1xPqWsRlPNQliLWw6+vrTZfXe86b/fb21hruLjiW8YIPr537YD3Ozs4a4srjGXx4T0XyVeuZBz7AR4rvwQf4UH7IcQb6Cn0Ff4CPXB6AP+AP+GM+P4R9EfABPsAH+OjT0dTnq1mfm6XMzs5OMx6PreG+v7/fnJ6eNpubm1Z0XV1dNYeHh9n9Cgw10zWwtOY497ka6KjzyPOo5Hyp67m4uLBr5z4G1khjPQ5tUChx1eIZfPh8Bj7afA4+PN+BD/AR6x3wAT5Sehj++HH8oe//+vqa+qOj/qKOWqxP4XP4HD7P92/AB/h4Cfgo0Q/0E+nvdvW371tfWcNddi7hYCw16fQqmTZZxagZn9uv12PU4FUhUnOc+1w9Ga/GqOxoSs6Xup7QP4/7YD0Ur8TVa8OT3hwBHz6fka/afA4+wIfLDyFvgw/wEes9+AP+iOsB+AP+gD9afZ2qe8EH+AAf4COXH8I+FfoKfYW+yvebVwkfZimzsbEx54Mee0x1ebO7Y+WlXnOc84KXh56Sz83NjTXeY4/4Pg9H7Z9Op9Zc5D4Gs++A9SCu3EwE8PE/sxkV5Ku39uMmedd7Cz8GPmLvWNbjcdcj9sZkPViP1Ewh+GOeP1zuRLdTf6h+Ax/oq7hf8Bj6Cj7Pe12zHtSD4AN8lPRb4fP75XNruMsO5uTkxJrWtR7qa2tr1qyvPS70dFfTXNO7l/GQ13n1+rc27qNpWI9vs1kCxFWLZ/DhPePAB/iIZ42AD/CR8lSEP+CPWNfCH/AH/OFn4oCP1pMa/sjPgENfoa/AB/go6e+hr9BXq6yvzFJGDfOtrS0TDZeXl83R0VEzGo2siX1wcNAcHx9n98t7Xc3zyWRSdZz73OFwaOe9u7srOl/qesLXc7gP1kPxSlx5PIMPn8/IV20+Bx/gI8X34AN8xHoP/oA/4noA/oA/4I98vQw+wAf4AB8l/TT01dPWV2pM6o0A+qLlfWH6u4t9c+F88Pvvv5uljBrm2iQU9CtT+MqJnj7P7VeBrleU3rx5U3WcXkHV53769MleQ5QHvHvFoet87rjwepwdDffRDp9lPYgr8JHOZ+CDvBvzi/hD26tXr34YD4aWMvDgvL6Az73+Il+Rr1L5ytl3PLZudw/KpHR5Xx0BzsF5XGdSD3rdCj7AB/jw1h9PmQfpXw1nttD04ejD0d9t65aFPtyff/75TROLnXDWsBc9sV76dzV3JazVqK85zp1HokIXJpBqqz2//r0myWrjPhprtrMe3yz5E1ctnsCHz2fgo83vMT7U2NNWmveJq3meJK7ScQWfo0uUV8AH+Ijrg2V0ScxTy8aV+xx0YvtmMXwOn6fq98fCx33hnD7DYj9nmbwb67hl8y7rwXqEeea54ryvXgYf6N2U3jVLmc+fP1uzWtv19XVzeHjYjMdj+7smsJ+enmb3a1CqPli/8NUc5z43nubed77U/nCKLffBeiheiSuPZ/Dh8xn5qs3n4AN8pPgefICPWO/BH/BHXA/AH/AH/JGvl8EH+AAf4KOkn4a+Ql+hr/L95lXChw1NdZNoRRDymVHRHU8xls97aj9TbO93iq2+Y9ZjMPsO9GpKTTzq33/9+rWRZYNi2b2W6f57Lo5T+5nmzjR3prkzzf0hprmHljLkq682eN3pC/IueZe8+zzyrsMq+grd7myOlMvR7dQf8PliP4V+CfW59D56l35iV7+V/tVq1oPWcN/d3W1OTk6s4I2nzuvp9XhqbDiVnanCTBVe5anCwgT4aMymSCKhNj+E+UM/gMjmR1tfXkntPz8/t2NZD9aDuFrkZfBxZnNgtJGv0CXokr2srgcf4AN8gI+Suh/d3vZF0FfoK6cvqT+oP+iL5vkTfKTxYZYyaqRtbfmp4kzjZRqv83J204Zr4oOp20976rZEQ8n09DAPyLNMSVRDIGryA9Oq09Oq1RQk7zY25Jm4OrYZKMIleffS3kwCHyP7DsBHmz/Bh9fn4AN9NRq1+QF9hb5SfszVZ/AH/BHHB/wBf8Af8/wZ5s8YH+JZvXFZ0/cg75J3U3nXGu56/e+ff/4xAffu3Tvzbw+DTr7puf16YlUNEwVszXFqNulz3avDeuLGFdld53PHhdcTvp7PfbAexNU8XsGHz2fkK/JuzC/gA3zEegdd4vUg+AAf4MP/CAh/UA/21ZnwB/yR6qdQf1B/wB/wB/xR3m9epfrDLGUkpks922OvRhnaqwmuRn3sTVXiva3A0/k1uHUZD3ldT+iHxX2wHoo74sp7wIIP75lHvmpndICP5fARe687bBFXxFWsd8i75N14Zgx5N513c3m1b6YUeZe8+z15Fz7Pz4pAJ87PsoPP4XP4PO29Th+unU1APTgfH+jdeb1rDXc9oZ7ypirxdJcgVpBpKnuXp1HOs/n29tYa7i5Ql/GIDq+d+2A9NGOAuGq9jGNsgw/wAT68FyX4mPeag8/fN+ADfOR4EnyAj3CGFfwBf6TqXuoP6o/UrCr4A/6AP+b1ZZg/wQf4WGV8mKXMzs6O2cFo0+sup6enzebmpjXhr66umsPDw+x+CQuJTg0yrDnOfa4Gkeg88jwqOV/qei4uLuzauY+BNZpZj0MbcENctXgGHz6fgY82n4MPz3fgA3zEegd8gI+UHoY/fhx/6Pu/vr6m/uiov6ijFutT+Bw+h8/z/RvwAT5eAj5K9AP9RPq7Xf3t+9ZX1nCXnUupZ3vsoa7XztTgVSFS672uJr+ejFdjVK9jLOMhr+vRa6XauI/G7CJYD+IqxDP48J5x4KOd0UHe9Z684AN8xJ6K4AN8pGYKwR/wRzyrCv6AP+CPvCcv+AAf4AN8lPT30Ffoq1XWV2Yps7GxMeeDrgGml5eX1gjf3t5u7u7usvvdsfJwqjnOfa489NQcvLm5KTpf6nqm06k13LmPwew7YD2IKzcTAXz4fEa+avM5edd7zeXwobeuVCg8BA9q1og2eHBRX5CvyFex/iRf9eerGv19XzzosNpXJ6DbqaO66kh4EB6kz5Dvs4AP8AE+wEc4Q6BP71FHPb06yhrusoM5OTmxBoA81PXKkZ6S1qZmg6xacvu16AqC2uPCz1XDXdO7S86Xuh69/q2N+2jsRw/Wo41X4qrFM/jw+Qx8+HwOPsBHzNvgA3zEeg/+gD9S9QD8AX/AH/l6GXyAD/ABPvr6aegr9BX6Kt9vXiV8mKWMGrRbW1vWXNeTRUdHR81oNLIm9sHBQXN8fJzdr6cAFSyTyaTqOPe5w+HQzqtf70rOl7qe0D6D+2A9FK/Elccz+PD5jHzV5nPwAT5SfA8+wEes9+AP+COuB+AP+AP+yNfL4AN8gA/wUdJPQ189bX2lvqTeSKYvWt4Xpr+72DcXzq3hLvuR2GOr1NNdT6ar4a5Gd+y9U+LprkAWMenJupRn5rt3radR1/XodWNt3EdjbwqwHvuWIImrphF+wIfPH+Dj/2zWBvjwnorgA3zE+gJ8gI+U5yj8AX/EdQ38AX/AH37mB/jwnu1x/wL+gD/AB/jo67dSf6xm/WGWMhILboHVtNYA09ArSIMMcvs1EEX/VsRSc5zOqc9V4OnPGnjqPKe7zueOC68nvFbug/UgrubxCj58PiNfkXdjfqnBR9hcCfmOuCKuvieu0FftD+Q5/YlO9N9PTb56LnGVy6spvU/eXazPwMdy+IDPybtdvEOfIR0f6F30LnqXvmhfvxVdMq9LrOGuJ6JFOtr05729vTnP9rOzs+x+CWIVAOvr61XHydtMn3t7e2sNd1dEuP9ecz3hv605LrxP7oP1iOOcuPJ5AHyAD/ABD/bxK3zudRT8AX+Es4/Qu/P5E3yAD/DhZ6ehr9BX6KvyPhT8AX/AH/CHmz36HPrUg99+++2b/ObCiba6gdK/q0H/33//zSxfSo/b2NiYWcj89NNPs4a+++81nyP/eG3cR/uDCevR2A84xFXTCE/gw+cz8EHejfkNfICPWG/AH1PTVPBHmy9dfMAf8Af8ka8PwQf4AB/go69/g75CXzl9ib5azBfgYzXxMfj777+/yX9dwx2cZ6VEU+nf5b2uhoVM8muOkwWN/v2HDx+sUS5vs2XOr8/Rawv6tZP7aMwLn/UgrkL8gg+fz8DH6udd8UjokdfHSzl81HwOcbX6cdUXR/F+8u7D5t0afKITW3w6XUC+Il89l3wFzqnPyVern6/AOTgH56uP8xddR2loqn5NUbM6NVXbTZvN7f/5559NyKvJWzPFlym26Sm2bnAs60FcHR8f249RwuXd3Z39INWHx9R+pqA/7Sno5F34A5x7/UG+Il+NRqM5vgMf4CPFk9QfbR0BPsAH+BjYD4qpPgT4AB/gA3zk8kPYb4vrDwlReZXX9DfRJegS6bI4rgZquGtQVd/U3Nx+pm4zdZup20zd7ssfTN1ezanbEiPwR2NvaOmHZxHseDyeWazpx+w4P6b2gw/w4d7wC+ODuEJfoa/QV+grzw99/BoOQu373qhru79XdAm6BF3SNHpDDt3eXdeQd/33g25Ht6d0uw1N1S86fdNmc/uZVs20aqZVM626L38wrXp+WrVELHm3aZ4bf4Si8uvXr7Nh38/tPuBzj8dwHYVJ8Rn5inylJ+L0YyI493nuofCRy6vufOQr8lUKj6n40I/cpfoKPieuHiKuyFfEFXG1qJ/g87a+oF/SnR/C76eGz5963rWGu57M65uOndsvAOkLWV9fb/b2/NTk9+/fN11TY93+29tbazy5L7X0uNyEau6D9VDcEVd+2jvT3JnmzjR3prmrCdHHr/B5q1vgD/hDeJE2DnUt+AAfcV2DvkJfoa/QV+irfn1J32exLwZ/wB/wx8vgD7OU2dnZaWLPzFIPcRWmKkp2d3fNS7D0OOeldH5+bs1+eR4t42Usz+qLiwt7ooH7GFijgPU4aogr74ELPnxeAh+txyX4AB+pmRDgA3zEXpXwB/wR6/ofwR8l3qnkK/IV+crP/IhnOIEP8AE+wEdff+9H8HlJf+8+81WJfkh5uqN30bsPpXet4f758+dqz1nnfafXI9TgFVBKvGpjzzw9GS8g6rXdlFdYiQeuXufXxn009roK6zG2Ny6Iq8beHAEf3gMWfLRehODjtXEG+Jj3mAcf4CP2KoU/4A90e97rGnyAD/ABPkr6F+gr9BX6Ku8FDz7AxyrjwyxlNjY2ZpYuemV2bW3Npn2rYbm9vd3c3d1l97tj5XVZc5z7XHno6Zw3NzdF50tdz3Q6teYJ9zGYfQesB3HlfCzBh89n5Ks2n5N3vc9rDh9660qF9EPwoLzmtMGDi/qCfEW+ivUn+ao/X9Xo7/viQYfVvjoB3U4d1VVHwoPwIH2GfJ8FfIAP8AE+Qj/zPr1HHfX06ihruMsOJvQQ0isVekpam5oNsmrJ7deiKwjkzVVzXPi5arhrqm/J+VLXo9dhtHEfjf3owXq08UpctZ564MPnM/Dh8zn4AB8xb4MP8BHrPfgD/kjVA/AH/AF/5Ote8AE+wAf46OuLoa/QV+irfL95lfBhljJq0NZ6rztPKD0FqGCZTCbmDdznFRXvHw6H1tTXr3cpT9mUx1LsFS9h4+wBuA/WQ/FBXG3NZiKAD5+XyFcHNmsDfICPlKci+AAf6Kv8LCLwAT7AB/joqzPRV+gr9NXAnBJSfSHwAT6eCz6W8YJHJ6ITUzrRGu6yH1FidK+46zX60Jfw3bt32f16Ml0NdwmQmuPkt67P1av1Ap6erHM+7F3nc8eF16vXjbVxH429KcB6EFfgI53PwAd5N+YX+MPzPfgAH+ADvdtXD6Dbvb6AP+CPuF4GH+Aj1U9BX6Gv0FfoK/RVeb95lfSVWcroCVgnENS01gDT0CtIgwxy+yU09G+VRGqO0zn1uWrS689qpDvP6a7zuePC6wmvlftgPYirebyCD5/PyFfk3ZhfavARkn/Id8QVcfU9cYW+ah+8yOlPdKL/fmry1XOJq1xeTel98u5ifQY+lsMHfE7e7eId+gzp+EDvonfRu/RF+/qt6JJ5XWINdz0RLdLRpj/v7e3NebafnZ1l90sQqwBYX1+vOk7eZvrc29tba7i7IsL995rrCf9tzXHhfXIfrEcc58SVzwPgA3yAD3iwj1/hc6+j4A/4I5x9hN6dz5/gA3yAjxOru1N1L/gAH+ADfOTyA/2rxT4l9Qf1R6qP/VT6V2YpoyFZtd7rzsNOAa4mvQaWxp41JZ7uMsSXsJDnUcrT6eCg9QLq8szTUApt3MfAfsBgPY5sUChx1TTCD/jw+QN8tJ6K4GM0mxkCPsBHrC/AB/hIzRSCP34cf5R4p7IeP249Suoz1oP1iOt+9BX6Cn3lPdvBR37W433yR4l+SM0YIF+Rrx4qX1nDXXYu4WDF/f19s3rRJquY8Xic3a/XStTgFVBqjnOfqyfjJeT0mmjJ+VLXE/rncR+sh+KVuPKDhMGHz2fkqzafgw/w4QaNh7wNPsBHrPfgD/gjrgfgD/gD/mjszexU3Qs+wAf4AB+5/BD2qdBX6Cv0Vb7fvEr4GPz111/f9HT5dDq1hverV6+sue6eeP/111+bq6ur7P7NzU3b/+bNm6rj3Od++vTJjr2+vrbz950vtV/Nfm3cR9OwHm28Elcez+DD5zPwkcaH3oRQDq7hgacaV7qPm5ubYj57qvfRxbvwILqkJD5WmQfBObp9WT53fLfK+CjJDyHfP1UeBOfgfFmc02dY1EngnHow7u/Bg3X9kr56mXxFHy7WX8q7gz/++OPbcDicPWGuJ9y0uSfO+/6uxoZ+zdf/ao5zn6uL0P/U6NfWd77U/slkYsdyH401mliPxmKKuGrxBD58PgMfbX4HH+0bVeBjnu/BB/iIdRz8AX/E9QD8AX+k6jX4A/6AP/L9E/ABPsAH+Ojrr6KvVlNf2dBUPdGupwhk7aI/q/n94cMHa0j88ssv1sTN7Q+nVdcc5z43nmLbd77UfjdwlftoGtajjVfiyuMZfPh8Bj7AR8xnNfgI/23Id8QVcfU9cYW+6taf8Plq83kur/bVA+Rd8u735F34nLxb2veo0YnwOXFFXNFPlAU2fdHXjWzLS/rJq97ftYa7PNjDiejh9OPU9HSmIzMdOZ6eHsZP7s/E1WLc9OGOqdtM3RZRKUeTd8m75N0TE27okvfN2dlZr26DP+AP+GORP13+AB/gA3yAj1hfp/QFda3X33q4UT9A6G36mrqevLuo24gr4iqu69Alq6lLbGjqzs7OzLNd3kTHx35KbWqKb7j/PqcKK/H0nY+pwpdNPOWa9ViM1/Pzc2tGyNefuLqwJhU4H5iVisT17u5uVZ5zeYe4GlksiSeY5s4094ea5k6+2jLeIl+1eoe8+/LyrnLAx48f0btHR731Gfh4efgoqWvgD/gj7heg29HtL0G3l+gH+on0E7v6qffd97GGux73D6cmp6au5/bLA1cNLBF7zXF61WI8HtsvpBIOztPI/fea61mlKbZKEqxH661MXIEPprm/tgZ3PO0efMAf4k/wAT5S+QF91eYH8AE+wMeifkjVWdRR+2ZF6bzpqT+oP+AP+AP+gD/i+ruvT0l9Tn2eqj/MUmZjY8MaOtpEMAoWPVmiRvjbt2+tGZ7b747Vv6s5zn3uv//+a+eUD1/J+VLXM522E4a5j8HsO2A9iCuHZ/Dh8xn5qs3n5F3Pd4+BD33/2uDBRX3xGOsR6x3w8bj4YD3y+vsp4cNdS1+dgG6njuqqI+FBeJA+Q77PAj7AB/gAHzV92qekE+nvvm60HtZwl71Clzdsl0fX2tqaNeP7PFW79qvh/uXLF2tAlHipxdej1ym1cR9Nw3p4r2viqvU6Bh/e6xh8gA/4Iz9LAnyAD/ABPkrqAfQV+iqu6+AP+AP+gD/gj7JZQ9Tn85728Af8scr8YZYyapjHnk6jkffE6/J0l0e2muSTyaTTazHnzT4cDu3J9ru7u5k3cI2HvD43fO2L+2A9FD/EVesBDD7mPcrIV+2MDvABPlIesOADfMT6C33lPbTBB/gAH/kZX+ADfIAP8NHXh6H+oP54LvXHMl7w8CA8mOJBa7jrdSU15rRt/z9758MTxxEs8bk8wsNnZIP5E4IlvlGUKFG+v4QEwgeExPAAQxw/Va/mZm5uZnfmDAbOv5Ui2Vnv7e5NV1d132715qb52MV+fvIrKu3Xk+lquCvBthwnn3J9rn91WL9sef/yvvP54+LriV/P5z5YD+JqFq/gI+Qz8hV5N+UX8AE+Ur2DLgl6EHyAD/Dxv8X6BHyAD/ABPmr6F9Qf1B/UH/QTh/qU1B/LWX+YpYyeYKr1bE+9GCU09IS8kkiNR1/qQaQmvc6vRvoiHvK6nvgzuQ/WQ7MEiKvgAQs+gvcb+aqb0QE+FsNH6r3usUVcEVfpDBvyLnkXvVueDRV/N6W8OjRTirxL3v2avAuf989qQycuphNLfQjyFfnqa/IVcUW+qp11Sf3x/OoPa7jrCXUt4iIe6hLEWtjV1VXX5/Ve8ma/u7uzhrsPjkW84ONr5z5Yj9PTU0dcBTyDj+CpSL7qPPPAB/jI8T34AB/KDyXOQF+hr+AP8FHKA/AH/AF/zOaHuC8CPsAH+AAfQzqa+nw563OzlNna2nKTycQa7ru7u+74+Nitr69b0XV5een29/eL+xUYaqZrYGnLcf5zNdBR55HnUc35ctdzfn5u1859jKyRxnrs26BQ4qrDM/gI+Qx8dPkcfAS+Ax/gI9U74AN85PQw/PHt+EPf/9XVFfVHT/1FHTVfn8Ln8Dl8Xu7fgA/w8T3go0Y/0E+kv9vX335ofWUNd9m5xIOx1KTTq2TaZBWjZnxpv16PUYNXhUjLcf5z9WS8GqOyo6k5X+56Yv887oP1ULwSV68NT3pXJJQhAAAgAElEQVRzBHyEfEa+6vI5+AAfPj/EvA0+wEeq9+AP+COtB+AP+AP+6PR1ru4FH+ADfICPUn6I+1ToK/QV+qrcb14mfJilzNra2owPeuox1efN7o+Vl3rLcd4LXh56Sj43NzfWeE894oc8HLX/9vbWmovcx2j6HbAexJWfiQA+/mc6o4J89dZ+3CTvBm/hp8BH6h3LejzteqTemKwH65GbKQR/zPKHz53oduoP1W/gA32V9gueQl/B52Wva9aDehB8gI+afit8/rB8bg132cEcHR1Z07rVQ31lZcWa9a3HxZ7uapprevciHvI6r17/1sZ9OMd6fJnOEiCuOjyDj+AZBz7ARzprBHyAj5ynIvwBf6S6Fv6AP+CPMBMHfHSe1PBHeQYc+gp9BT7AR01/D32FvlpmfWWWMmqYb2xsmGi4uLhwBwcH7uTkxJrYe3t77vDwsLhf3utqnl9fXzcd5z93PB7bee/v76vOl7ue+PUc7oP1ULwSVwHP4CPkM/JVl8/BB/jI8T34AB+p3oM/4I+0HoA/4A/4o1wvgw/wAT7AR00/DX31vPWVGpN6I4C+aH1fmP7ufN9cOB/9+uuvZimjhrk2CQX9yhS/cqKnz0v7VaDrFaU3b940HadXUPW5Hz9+tNcQ5QHvX3HoO58/Lr4eb0fDfXTDZ1kP4gp85PMZ+CDvpvwi/tD26tWrb8aDsaUMPDirL+DzoL/IV+SrXL7y9h1Prdv9gzI5XT5UR4BzcJ7WmdSDQbeCD/ABPoL1x3PmQfpX46ktNH04+nD0d7u6Za4P9/vvv3/RxGIvnDXsRU+s1/5dzV0JazXqW47z55Go0IUJpNpaz69/r0my2rgPZ8121uOLJX/iqsMT+Aj5DHx0+T3Fhxp72mrzPnE1y5PEVT6u4HN0ifIK+AAfaX2wiC5JeWrRuPKfg07s3iyGz+HzXP3+VPh4KJzTZ5jv5yySd1Mdt2jeZT1YjzjPvFScD9XL4AO9m9O7Zinz6dMna1Zru7q6cvv7+24ymdjfNYH9+Pi4uF+DUvXB+oWv5Tj/uek096Hz5fbHU2y5D9ZD8UpcBTyDj5DPyFddPgcf4CPH9+ADfKR6D/6AP9J6AP6AP+CPcr0MPsAH+AAfNf009BX6Cn1V7jcvEz5saKqfRCuCkM+Miu50irF83nP7mWL7sFNs9R2zHqPpd6BXU1riUf/+8+fPTpYNimX/Wqb//6U4zu1nmjvT3JnmzjT3x5jmHlvKkK8+2+B1ry/Iu+Rd8u7LyLseq+grdLu3OVIuR7dTf8Dn8/0U+iXU59L76F36iX39VvpXy1kPWsN9e3vbHR0dWcGbTp3X0+vp1Nh4KjtThZkqvMxThYUJ8OHMpkgioTU/xPlDP4DI5kfbUF7J7T87O7NjWQ/Wg7ia52XwcWpzYLSRr9Al6JKdoq4HH+ADfICPmrof3d71RdBX6CuvL6k/qD/oi5b5E3zk8WGWMmqkbWyEqeJM42Uar/dy9tOGW+KDqdvPe+q2REPN9PQ4D8izTElUQyBa8gPTqvPTqtUUJO86G/JMXB3aDBThkrx7YW8mgY8T+w7AR5c/wUfQ5+ADfXVy0uUH9BX6SvmxVJ/BH/BHGh/wB/wBf8zyZ5w/U3yIZ/XGZUvfg7xL3s3lXWu46/W/v/76ywTcu3fvzL89Djr5ppf264lVNUwUsC3Hqdmkz/WvDuuJG19k953PHxdfT/x6PvfBehBXs3gFHyGfka/Iuym/gA/wkeoddEnQg+ADfICP8CMg/EE9OFRnwh/wR66fQv1B/QF/wB/wR32/eZnqD7OUkZiu9WxPvRplaK8muBr1qTdVjfe2Ak/n1+DWRTzkdT2xHxb3wXoo7oir4AELPoJnHvmqm9EBPhbDR+q97rFFXBFXqd4h75J305kx5N183i3l1aGZUuRd8u7X5F34vDwrAp04O8sOPofP4fO89zp9uG42AfXgbHygd2f1rjXc9YR6zpuqxtNdglhBpqnsfZ5GJc/mu7s7a7j7QF3EIzq+du6D9dCMAeKq8zJOsQ0+wAf4CF6U4GPWaw4+f+/AB/go8ST4AB/xDCv4A/7I1b3UH9QfuVlV8Af8AX/M6ss4f4IP8LHM+DBLma2tLbOD0abXXY6Pj936+ro14S8vL93+/n5xv4SFRKcGGbYc5z9Xg0h0Hnke1Zwvdz3n5+d27dzHyBrNrMe+Dbghrjo8g4+Qz8BHl8/BR+A78AE+Ur0DPsBHTg/DH9+OP/T9X11dUX/01F/UUfP1KXwOn8Pn5f4N+AAf3wM+avQD/UT6u3397YfWV9Zwl51LrWd76qGu187U4FUh0uq9ria/noxXY1SvYyziIa/r0Wul2rgPZ3YRrAdxFeMZfATPOPDRzegg7wZPXvABPlJPRfABPnIzheAP+COdVQV/wB/wR9mTF3yAD/ABPmr6e+gr9NUy6yuzlFlbW5vxQdcA04uLC2uEb25uuvv7++J+f6w8nFqO858rDz01B29ubqrOl7ue29tba7hzH6Ppd8B6EFd+JgL4CPmMfNXlc/Ju8Jor4UNvXalQeAwe1KwRbfDgvL4gX5GvUv1JvhrOVy36+6F40GN1qE5At1NH9dWR8CA8SJ+h3GcBH+ADfICPeIbAkN6jjnp+dZQ13GUHc3R0ZA0AeajrlSM9Ja1NzQZZtZT2a9EVBK3HxZ+rhrumd9ecL3c9ev1bG/fh7EcP1qOLV+KqwzP4CPkMfIR8Dj7AR8rb4AN8pHoP/oA/cvUA/AF/wB/lehl8gA/wAT6G+mnoK/QV+qrcb14mfJiljBq0Gxsb1rTWk3cHBwfu5OTE/r63t+cODw+L+/UUoILl+vq66Tj/uePx2Jr6+vWu5ny564ntM7gP1kPxSlwFPIOPkM/IV10+Bx/gI8f34AN8pHoP/oA/0noA/oA/4I9yvQw+wAf4AB81/TT0FfoKfVXuNy8TPqzhLvuR1GOr1tNdT6ar4a5Gd+q9U+PproSkhruerMt5Zr5713ka9V2PXjfWxn04e1OA9di1H46IK+eEH/AR8gf4+D+btQE+gqci+AAfqb4AH+Aj5zkKf8AfaV0Df8Af8EeY+QE+gmd72r+AP+AP8AE+hvqt1B/LWX+YpYzEgl9gNa01wDT2CtIgg9J+DUTRvxWxtBync+pzFXj6swaees/pvvP54+Lria+V+2A9iKtZvIKPkM/IV+TdlF9a8BE3V2K+I66Iq6+JK/RV9wN5SX+iE8P305KvXkpclfJqTu+Td+frM/CxGD7gc/JuH+/QZ8jHB3oXvYvepS861G9Fl8zqEmu464lokY42/XlnZ2fGs/309LS4X4JYBcDq6mrTcfI20+fe3d1Zw90XEf7/t1xP/G9bjovvk/tgPdI4J65CHgAf4AN8wIND/AqfBx0Ff8Af8ewj9O5s/gQf4AN8hNlp6Cv0Ffqqvg8Ff8Af8Af84WePvoQ+9eiXX375Ir+5eKKtbqD272rQ//fff1PLl9rj1tbWphYyP/zww7Sh7/9/y+fIP14b99H9YMJ6OPsBh7hyTngCHyGfgQ/ybspv4AN8pHoD/rg1TQV/dPnSxwf8AX/AH+X6EHyAD/ABPob6N+gr9JXXl+ir+XwBPpYTH6M///zzi/zXLy4urLCQNYxEU+3f5b2uhoWGmbYc58/z4cMHa5TL22yR8+tz9NqCfu3kPpx54bMeXxxxFfAMPkI+Ax9dfl9mfIhHYo+8IV4q4aPlc4ir5Y+roThK95N3HzfvtuATnTir68lX5KuXkq/AOfU5+Wr58xU4B+fgfPlx/l3XURqaql9T1KxW01qN9oODMDVYjfTDw8Pi/h9//NEaOGrythznP7dvmnvt9aRTbGuPi6+X++jWmfUIOCCu2qaH9+GOuCKucvFB3iXvpvqCvEvePTk5sQcw0Inz+ht8gA/wMZsf4voVfIAP8AE+Yv1Q6m9Rf1B/DNUfiiN5lbf0N4kr4ioXVyM13DWoamhqbmk/U7eZus3UbaZuD+UPpm4v59RtiRH4w9kbWvrhWcJ+MplMLdb0Y3aaH3P7wQf48G/4xfFBXKGv0FfoK/RV4Ichfo0HoQ59b9S1/d8rugRdgi7pnB/Q7f11DXk3fD/odnR7Trfb0FQ9ETA0bba0n2nVTKtmWjXTqofyB9OqZ6dVS8SSd517afwRi8rPnz9Ph32/tPuAzwMe43UUJsVn5Cvyld7I0Y+J4DzkucfCRymv+vORr8hXOTzm4kM/ctfqK/icuHqMuCJfEVfE1bx+gs+7+oJ+SX9+iL+fFj5/7nnXGu56Mm9oOnZpvwCkL2R1ddXt7ISpye/fv3d9U2P9/ru7O2s8+S+19rjShGrug/VQ3BFXYdo709yZ5s40d6a5qwkxxK/weadb4A/4Q3iRNo51LfgAH2ldg75CX6Gv0Ffoq2F9Sd9nvi8Gf8Af8Mf3wR9mKbO1teVSz7NaT3cVpipKtre3e73eS17wZ2dn1uyX59Ei3uv63PPzc3uigfsYWaOA9ThwxFXw8AMfYQYF+OhmdIAP8JHzuAQf4CP1qoQ/4I+0HvgW/FHjnUq+Il+Rr8oz18AH+AAf4GOov/ct+Lymv/eQ+apGP+RmVqJ30buPpXet4f7p06dmz1nvfafXI9TgFVBqvGpTzzw9GS8g6rXdnFdYjQeuXufXxn04e12F9ZjYGxfElbM3R8BH8IAFH50XIfh4bZwBPmY95sEH+Ei9SuEP+APdXva6Bh/gA3yAj5r+BfoKfYW+KnvBgw/wscz4MEuZtbW1qaWLXpldWVlx+uVHDcvNzU13f39f3O+Plddly3H+c+Whp3Pe3NxUnS93Pbe3t9Y84T5G0++A9SCuvI8l+Aj5jHzV5XPybvB5LeFDb12pkH4MHpTXnDZ4cF5fkK/IV6n+JF8N56sW/f1QPOixOlQnoNupo/rqSHgQHqTPUO6zgA/wAT7AR+xnPqT3qKOeXx1lDXfZwcQeQnqlQk9Ja1OzQVYtpf1adAWBvLlajos/Vw13TfWtOV/uevQ6jDbuw9mPHqxHF6/EVeepBz5CPgMfIZ+DD/CR8jb4AB+p3oM/4I9cPQB/wB/wR7nuBR/gA3yAj6G+GPoKfYW+KveblwkfZimjBm2tZ3vqeaSnABUs19fX5g085BWV7h+Px9bU1693OU/ZnMfS4WHwGNJ+CRtvD8B9sB6KD+JqYzoTAXyEvES+2rNZG+ADfOQ8FcEH+EBfzerLWNeCD/ABPsDHUJ2JvkJfoa9G5pSQ6wuBD/DxUvCxiBc8OhGdmNOJ1nCX/YgSo3/FXa/Rx76E7969K+7Xk+lquEuAtBwnv3V9rl6tF/D0ZJ33Ye87nz8uvl69bqyN+3D2pgDrQVyBj3w+Ax/k3ZRf4I/A9+ADfIAP9O5QPYBuD/oC/oA/0noZfICPXD8FfYW+Ql+hr9BX9f3mZdJXZimjJ2C9QFDTWgNMY68gDTIo7ZfQ0L9VEmk5TufU56pJrz+rke49p/vO54+Lrye+Vu6D9SCuZvEKPkI+I1+Rd1N+acFHTP4x3xFXxNXXxBX6qnvwoqQ/0Ynh+2nJVy8lrkp5Naf3ybvz9Rn4WAwf8Dl5t4936DPk4wO9i95F79IXHeq3oktmdYk13PVEtEhHm/68s7Mz49l+enpa3C9BrAJgdXW16Th5m+lz7+7urOHuiwj//1uuJ/63LcfF98l9sB5pnBNXIQ+AD/ABPuDBIX6Fz4OOgj/gj3j2EXp3Nn+CD/ABPo6s7s7VveADfIAP8FHKD/Sv5vuU1B/UH7k+9nPpX5mljIZktXqvew87Bbia9BpYmnrW1Hi6yxBfwkKeRzlPp729zguozzNPQym0cR8j+wGD9TiwQaHElXPCD/gI+QN8dJ6K4ONkOjMEfICPVF+AD/CRmykEf3w7/qjxTmU9vt161NRnrAfrkdb96Cv0FfoqeLaDj/Ksx4fkjxr9kJsxQL4iXz1WvrKGu+xc4sGKu7u7ZvWiTVYxk8mkuF+vlajBK6C0HOc/V0/GS8jpNdGa8+WuJ/bP4z5YD8UrcRUGCYOPkM/IV10+Bx/gww8aj3kbfICPVO/BH/BHWg/AH/AH/OHszexc3Qs+wAf4AB+l/BD3qdBX6Cv0VbnfvEz4GP3xxx9f9HT57e2tNbxfvXplzXX/xPvPP//sLi8vi/vX19dt/5s3b5qO85/78eNHO/bq6srOP3S+3H41+7VxH86xHl28ElcBz+Aj5DPwkceH3oRQDm7hgecaV7qPm5ubaj57rvfRx7vwILqkJj6WmQfBObp9UT73fLfM+KjJDzHfP1ceBOfgfFGc02eY10ngnHow7e/Bg239kqF6mXxFHy7VX8q7o99+++3LeDyePmGuJ9y0+SfOh/6uxoZ+zdd/Lcf5z9VF6D81+rUNnS+3//r62o7lPpw1mlgPZzFFXHV4Ah8hn4GPLr+Dj+6NKvAxy/fgA3ykOg7+gD/SegD+gD9y9Rr8AX/AH+X+CfgAH+ADfAz1V9FXy6mvbGiqnmjXUwSydtGf1fz+8OGDNSR++ukna+KW9sfTqluO85+bTrEdOl9uvx+4yn04x3p08UpcBTyDj5DPwAf4SPmsBR/xv435jrgirr4mrtBX/foTPl9uPi/l1aF6gLxL3v2avAufk3dr+x4tOhE+J66IK/qJssCmL/rayba8pp+87P1da7jLgz2eiB5PP85NT2c6MtOR0+npcfyU/kxczcfNEO6Yus3UbRGVcjR5l7xL3j0y4YYuee9OT08HdRv8AX/AH/P86fMH+AAf4AN8pPo6py+oa4P+1sON+gFCb9O31PXk3XndRlwRV2ldhy5ZTl1iQ1O3tramnu3yJjo8DFNqc1N84/0POVVYiWfofEwVvnDplGvWYz5ez87OrBkhX3/i6tyaVOB8ZFYqEtfb29tNec7nHeLqxGJJPME0d6a5P9Y0d/LVhvEW+arTO+Td7y/vKgf8/fff6N2Dg8H6DHx8f/ioqWvgD/gj7Reg29Ht34Nur9EP9BPpJ/b1Ux+672MNdz3uH09Nzk1dL+2XB64aWCL2luP0qsVkMrFfSCUcvKeR//8t17NMU2yVJFiPzluZuAIfTHN/bQ3udNo9+IA/xJ/gA3zk8gP6qssP4AN8gI95/ZCrs6ijds2K0nvTU39Qf8Af8Af8AX+k9fdQn5L6nPo8V3+Ypcza2po1dLSJYBQserJEjfC3b99aM7y03x+rf9dynP/cf/75x84pH76a8+Wu5/a2mzDMfYym3wHrQVx5PIOPkM/IV10+J+8GvnsKfOj71wYPzuuLp1iPVO+Aj6fFB+tR1t/PCR/+WobqBHQ7dVRfHQkPwoP0Gcp9FvABPsAH+Gjp0z4nnUh/97XTeljDXfYKfd6wfR5dKysr1owf8lTt26+G+7///msNiBovtfR69DqlNu7DOdYjeF0TV53XMfgIXsfgA3zAH+VZEuADfIAP8FFTD6Cv0FdpXQd/wB/wB/wBf9TNGqI+n/W0hz/gj2XmD7OUUcM89XQ6OQmeeH2e7vLIVpP8+vq612ux5M0+Ho/tyfb7+/upN3CLh7w+N37ti/tgPRQ/xFXnAQw+Zj3KyFfdjA7wAT5yHrDgA3yk+gt9FTy0wQf4AB/lGV/gA3yAD/Ax1Ieh/qD+eCn1xyJe8PAgPJjjQWu463UlNea0bW5umo9d7Ocnv6LSfj2Zroa7EmzLcfIp1+f6V4f1y5b3L+87nz8uvp749Xzug/UgrmbxCj5CPiNfkXdTfgEf4CPVO+iSoAfBB/gAH/9brE/AB/gAH+Cjpn9B/UH9Qf1BP3GoT0n9sZz1h1nK6AmmWs/21ItRQkNPyCuJ1Hj0pR5EatLr/GqkL+Ihr+uJP5P7YD00S4C4Ch6w4CN4v5Gvuhkd4GMxfKTe6x5bxBVxlc6wIe+Sd9G75dlQ8XdTyqtDM6XIu+Tdr8m78Hn/rDZ04mI6sdSHIF+Rr74mXxFX5KvaWZfUH8+v/rCGu55Q1yIu4qEuQayFXV1ddX1e7yVv9ru7O2u4++BYxAs+vnbug/U4PT11xFXAM/gInorkq84zD3yAjxzfgw/wofxQ4gz0FfoK/gAfpTwAf8Af8Mdsfoj7IuADfIAP8DGko6nPl7M+N0uZra0tN5lMrOG+u7vrjo+P3fr6uhVdl5eXbn9/v7hfgaFmugaWthznP1cDHXUeeR7VnC93Pefn53bt3MfIGmmsx74NCiWuOjyDj5DPwEeXz8FH4DvwAT5SvQM+wEdOD8Mf344/9P1fXV1Rf/TUX9RR8/UpfA6fw+fl/g34AB/fAz5q9AP9RPq7ff3th9ZX1nCXnUs8GEtNOr1Kpk1WMWrGl/br9Rg1eFWItBznP1dPxqsxKjuamvPlrif2z+M+WA/FK3H12vCkN0fAR8hn5Ksun4MP8OHzQ8zb4AN8pHoP/oA/0noA/oA/4I9OX+fqXvABPsAH+Cjlh7hPhb5CX6Gvyv3mZcKHWcqsra3N+KCnHlN93uz+WHmptxznveDloafkc3NzY4331CN+yMNR+29vb625yH2Mpt8B60Fc+ZkI4ON/pjMqyFdv7cdN8m7wFn4KfKTesazH065H6o3JerAeuZlC8Mcsf/jciW6n/lD9Bj7QV2m/4Cn0FXxe9rpmPagHwQf4qOm3wucPy+fWcJcdzNHRkTWtWz3UV1ZWrFnfelzs6a6muaZ3L+Ihr/Pq9W9t3IdzrMeX6SwB4qrDM/gInnHgA3yks0bAB/jIeSrCH/BHqmvhD/gD/ggzccBH50kNf5RnwKGv0FfgA3zU9PfQV+irZdZXZimjhvnGxoaJhouLC3dwcOBOTk6sib23t+cODw+L++W9rub59fV103H+c8fjsZ33/v6+6ny564lfz+E+WA/FK3EV8Aw+Qj4jX3X5HHyAjxzfgw/wkeo9+AP+SOsB+AP+gD/K9TL4AB/gA3zU9NPQV89bX6kxqTcC6IvW94Xp7873zYXz0a+//mqWMmqYa5NQ0K9M8Ssnevq8tF8Ful5RevPmTdNxegVVn/vx40d7DVEe8P4Vh77z+ePi6/F2NNxHN3yW9SCuwEc+n4EP8m7KL+IPba9evfpmPBhbysCDs/oCPg/6i3xFvsrlK2/f8dS63T8ok9PlQ3UEOAfnaZ1JPRh0K/gAH+AjWH88Zx6kfzWe2kLTh6MPR3+3q1vm+nC///77F00s9sJZw170xHrt39XclbBWo77lOH8eiQpdmECqrfX8+veaJKuN+3DWbGc9vljyJ646PIGPkM/AR5ffU3yosaetNu8TV7M8SVzl4wo+R5cor4AP8JHWB4vokpSnFo0r/znoxO7NYvgcPs/V70+Fj4fCOX2G+X7OInk31XGL5l3Wg/WI88xLxflQvQw+0Ls5vWuWMp8+fbJmtbarqyu3v7/vJpOJ/V0T2I+Pj4v7NShVH6xf+FqO85+bTnMfOl9ufzzFlvtgPRSvxFXAM/gI+Yx81eVz8AE+cnwPPsBHqvfgD/gjrQfgD/gD/ijXy+ADfIAP8FHTT0Nfoa/QV+V+8zLhw4am+km0Igj5zKjoTqcYy+c9t58ptg87xVbfMesxmn4HejWlJR717z9//uxk2aBY9q9l+v9fiuPcfqa5M82dae5Mc3+Mae6xpQz56rMNXvf6grxL3iXvvoy867GKvkK3e5sj5XJ0O/UHfD7fT6FfQn0uvY/epZ/Y12+lf7Wc9aA13Le3t93R0ZEVvOnUeT29nk6NjaeyM1WYqcLLPFVYmAAfzmyKJBJa80OcP/QDiGx+tA3lldz+s7MzO5b1YD2Iq3leBh+nNgdGG/kKXYIu2SnqevABPsAH+Kip+9HtXV8EfYW+8vqS+oP6g75omT/BRx4fZimjRtrGRpgqzjRepvF6L2c/bbglPpi6/bynbks01ExPj/OAPMuURDUEoiU/MK06P61aTUHyrrMhz8TVoc1AES7Juxf2ZhL4OLHvAHx0+RN8BH0OPtBXJyddfkBfoa+UH0v1GfwBf6TxAX/AH/DHLH/G+TPFh3hWb1y29D3Iu+TdXN61hrte//vrr79MwL1798782+Ogk296ab+eWFXDRAHbcpyaTfpc/+qwnrjxRXbf+fxx8fXEr+dzH6wHcTWLV/AR8hn5iryb8gv4AB+p3kGXBD0IPsAH+Ag/AsIf1INDdSb8AX/k+inUH9Qf8Af8AX/U95uXqf4wSxmJ6VrP9tSrUYb2aoKrUZ96U9V4byvwdH4Nbl3EQ17XE/thcR+sh+KOuAoesOAjeOaRr7oZHeBjMXyk3useW8QVcZXqHfIueTedGUPezefdUl4dmilF3iXvfk3ehc/LsyLQibOz7OBz+Bw+z3uv04frZhNQD87GB3p3Vu9aw11PqOe8qWo83SWIFWSayt7naVTybL67u7OGuw/URTyi42vnPlgPzRggrjov4xTb4AN8gI/gRQk+Zr3m4PP3DnyAjxJPgg/wEc+wgj/gj1zdS/1B/ZGbVQV/wB/wx6y+jPMn+AAfy4wPs5TZ2toyOxhtet3l+PjYra+vWxP+8vLS7e/vF/dLWEh0apBhy3H+czWIROeR51HN+XLXc35+btfOfYys0cx67NuAG+KqwzP4CPkMfHT5HHwEvgMf4CPVO+ADfOT0MPzx7fhD3//V1RX1R0/9RR01X5/C5/A5fF7u34AP8PE94KNGP9BPpL/b199+aH1lDXfZudR6tqce6nrtTA1eFSKt3utq8uvJeDVG9TrGIh7yuh69VqqN+3BmF8F6EFcxnsFH8IwDH92MDvJu8OQFH+Aj9VQEH+AjN1MI/oA/0llV8Af8AX+UPXnBB/gAH+Cjpr+HvkJfLbO+MkuZtbW1GR90DTC9uLiwRvjm5qa7v78v7vfHysOp5Tj/ufLQU3Pw5uam6ny567m9vbWGO/cxmn4HrAdx5WcigI+Qz8hXXT4n7wavuRI+9NaVCoXH4EHNGtEGD87rC/IV+SrVn+Sr4XzVor8figc9VofqBHQ7dVRfHU8svQsAACAASURBVAkPwoP0Gcp9FvABPsAH+IhnCAzpPeqo51dHWcNddjBHR0fWAJCHul450lPS2tRskFVLab8WXUHQelz8uWq4a3p3zfly16PXv7VxH85+9GA9unglrjo8g4+Qz8BHyOfgA3ykvA0+wEeq9+AP+CNXD8Af8Af8Ua6XwQf4AB/gY6ifhr5CX6Gvyv3mZcKHWcqoQbuxsWHNdT1ZdHBw4E5OTqyJvbe35w4PD4v79RSgguX6+rrpOP+54/HYzqtf72rOl7ue2D6D+2A9FK/EVcAz+Aj5jHzV5XPwAT5yfA8+wEeq9+AP+COtB+AP+AP+KNfL4AN8gA/wUdNPQ189b32lvqTeSKYvWt8Xpr873zcXzq3hLvuR1GOr1tNdT6ar4a5Gd+q9U+PprkAWMenJupxn5rt3nadR3/XodWNt3IezNwVYj11LkMSVc8IP+Aj5A3z8n83aAB/BUxF8gI9UX4AP8JHzHIU/4I+0roE/4A/4I8z8AB/Bsz3tX8Af8Af4AB9D/Vbqj+WsP8xSRmLBL7Ca1hpgGnsFaZBBab8GoujfilhajtM59bkKPP1ZA0+953Tf+fxx8fXE18p9sB7E1SxewUfIZ+Qr8m7KLy34iJsrMd8RV8TV18QV+qr7gbykP9GJ4ftpyVcvJa5KeTWn98m78/UZ+FgMH/A5ebePd+gz5OMDvYveRe/SFx3qt6JLZnWJNdz1RLRIR5v+vLOzM+PZfnp6WtwvQawCYHV1tek4eZvpc+/u7qzh7osI//9brif+ty3HxffJfbAeaZwTVyEPgA/wAT7gwSF+hc+DjoI/4I949hF6dzZ/gg/wAT7C7DT0FfoKfVXfh4I/4A/4A/7ws0dfQp969Msvv3yR31w80VY3UPt3Nej/+++/qeVL7XFra2tTC5kffvhh2tD3/7/lc+Qfr4376H4wYT2c/YBDXDknPIGPkM/AB3k35TfwAT5SvQF/3Jqmgj+6fOnjA/6AP+CPcn0IPsAH+AAfQ/0b9BX6yutL9NV8vgAfy4mP0Z9//vlF/usa7uA9KyWaav8u73U1LGSS33KcLGj07z98+GCNcnmbLXJ+fY5eW9CvndyHMy981oO4ivELPkI+Ax/Ln3fFI7FH3hAvlfDR8jnE1fLH1VAcpfvJu4+bd1vwiU7s8Ol1AfmKfPVS8hU4pz4nXy1/vgLn4BycLz/Ov+s6SkNT9WuKmtW5qdp+2mxp/48//mhCXk3elim+TLHNT7H1g2NZD+Lq8PDQfowSLu/v7+0HqSE85vYzBf15T0En78If4DzoD/IV+erk5GSG78AH+MjxJPVHV0eAD/ABPkb2g2KuDwE+wAf4AB+l/BD329L6Q0JUXuUt/U10CbpEuiyNq5Ea7hpUNTQ1t7SfqdtM3WbqNlO3h/IHU7eXc+q2xAj84ewNLf3wLIKdTCZTizX9mJ3mx9x+8AE+/Bt+cXwQV+gr9BX6Cn0V+GGIX+NBqEPfG3Vt//eKLkGXoEuc0xty6Pb+uoa8G74fdDu6PafbbWiqftEZmjZb2s+0aqZVM62aadVD+YNp1bPTqiViybvOvTT+iEXl58+fp8O+X9p9wOcBj/E6CpPiM/IV+UpPxOnHRHAe8txj4aOUV/35yFfkqxwec/GhH7lr9RV8Tlw9RlyRr4gr4mpeP8HnXX1Bv6Q/P8TfTwufP/e8aw13PZk3NB27tF8A0heyurrqdnbC1OT379+7vqmxfv/d3Z01nvyXWntcaUI198F6KO6IqzDtnWnuTHNnmjvT3NWEGOJX+LzTLfAH/CG8SBvHuhZ8gI+0rkFfoa/QV+gr9NWwvqTvM98Xgz/gD/jj++APs5TZ2tpyqWdmrYe4ClMVJdvb2+YlWHuc91I6OzuzZr88jxbxMpZn9fn5uT3RwH2MrFHAehw44ip44IKPkJfAR+dxCT7AR24mBPgAH6lXJfwBf6S6/lvwR413KvmKfEW+CjM/0hlO4AN8gA/wMdTf+xZ8XtPfe8h8VaMfcp7u6F307mPpXWu4f/r0qdlz1nvf6fUINXgFlBqv2tQzT0/GC4h6bTfnFVbjgavX+bVxH85eV2E9JvbGBXHl7M0R8BE8YMFH50UIPl4bZ4CPWY958AE+Uq9S+AP+QLeXva7BB/gAH+Cjpn+BvkJfoa/KXvDgA3wsMz7MUmZtbW1q6aJXZldWVmzatxqWm5ub7v7+vrjfHyuvy5bj/OfKQ0/nvLm5qTpf7npub2+tecJ9jKbfAetBXHkfS/AR8hn5qsvn5N3g81rCh966UiH9GDworzlt8OC8viBfka9S/Um+Gs5XLfr7oXjQY3WoTkC3U0f11ZHwIDxIn6HcZwEf4AN8gI/Yz3xI71FHPb86yhrusoOJPYT0SoWektamZoOsWkr7tegKAnlztRwXf64a7prqW3O+3PXodRht3IezHz1Yjy5eiavOUw98hHwGPkI+Bx/gI+Vt8AE+Ur0Hf8AfuXoA/oA/4I9y3Qs+wAf4AB9DfTH0FfoKfVXuNy8TPsxSRg3aVu917wmlpwAVLNfX1+YNPOQVle4fj8fW1NevdzlP2ZzHUuoVL2Hj7QG4D9ZD8UFcbUxnIoCPkJfIV3s2awN8gI+cpyL4AB/oq/IsIvABPsAH+BiqM9FX6Cv01cicEnJ9IfABPl4KPhbxgkcnohNzOtEa7rIfUWL0r7jrNfrYl/Ddu3fF/XoyXQ13CZCW4+S3rs/Vq/UCnp6s8z7sfefzx8XXq9eNtXEfzt4UYD2IK/CRz2fgg7yb8gv8EfgefIAP8IHeHaoH0O1BX8Af8EdaL4MP8JHrp6Cv0FfoK/QV+qq+37xM+sosZfQErBcIalprgGnsFaRBBqX9Ehr6t0oiLcfpnPpcNen1ZzXSved03/n8cfH1xNfKfbAexNUsXsFHyGfkK/Juyi8t+IjJP+Y74oq4+pq4Ql91D16U9Cc6MXw/LfnqpcRVKa/m9D55d74+Ax+L4QM+J+/28Q59hnx8oHfRu+hd+qJD/VZ0yawusYa7nogW6WjTn3d2dmY8209PT4v7JYhVAKyurjYdJ28zfe7d3Z013H0R4f9/y/XE/7bluPg+uQ/WI41z4irkAfABPsAHPDjEr/B50FHwB/wRzz5C787mT/ABPsDHkdXduboXfIAP8AE+SvmB/tV8n5L6g/oj18d+Lv0rs5TRkKxW73XvYacAV5NeA0tTz5oaT3cZ4ktYyPMo5+m0t9d5AfV55mkohTbuY2Q/YLAeBzYolLhyTvgBHyF/gI/OUxF8nExnhoAP8JHqC/ABPnIzheCPb8cfNd6prMe3W4+a+oz1YD3Suh99hb5CXwXPdvBRnvX4kPxRox9yMwbIV+Srx8pX1nCXnUs8WHF3d9esXrTJKmYymRT367USNXgFlJbj/OfqyXgJOb0mWnO+3PXE/nncB+uheCWuwiBh8BHyGfmqy+fgA3z4QeMxb4MP8JHqPfgD/kjrAfgD/oA/nL2Znat7wQf4AB/go5Qf4j4V+gp9hb4q95uXCR+jP/7444ueLr+9vbWG96tXr6y57p94//nnn93l5WVx//r6uu1/8+ZN03H+cz9+/GjHXl1d2fmHzpfbr2a/Nu7DOdaji1fiKuAZfIR8Bj7y+NCbEMrBLTzwXONK93Fzc1PNZ8/1Pvp4Fx5El9TExzLzIDhHty/K557vlhkfNfkh5vvnyoPgHJwvinP6DPM6CZxTD6b9PXiwrV8yVC+Tr+jDpfpLeXf022+/fRmPx9MnzPWEmzb/xPnQ39XY0K/5+q/lOP+5ugj9p0a/tqHz5fZfX1/bsdyHs0YT6+EspoirDk/gI+Qz8NHld/DRvVEFPmb5HnyAj1THwR/wR1oPwB/wR65egz/gD/ij3D8BH+ADfICPof4q+mo59ZUNTdUT7XqKQNYu+rOa3x8+fLCGxE8//WRN3NL+eFp1y3H+c9MptkPny+33A1e5D+dYjy5eiauAZ/AR8hn4AB8pn7XgI/63Md8RV8TV18QV+qpff8Lny83npbw6VA+Qd8m7X5N34XPybm3fo0UnwufEFXFFP1EW2PRFXzvZltf0k5e9v2sNd3mwxxPR4+nHuenpTEdmOnI6PT2On9Kfiav5uBnCHVO3mbotolKOJu+Sd8m7Rybc0CXv3enp6aBugz/gD/hjnj99/gAf4AN8gI9UX+f0BXVt0N96uFE/QOht+pa6nrw7r9uIK+IqrevQJcupS2xo6tbW1tSzXd5Eh4fdlFptf//9t0unKsf77+/vrRm0vb3ddJz/3LOzMysa5b9ec77c9QxNFa75XO6jW2fW48TiUDggrkIeAB/gI+UB8AE+Up0Af8Afnj/RifM6GnyAD/DR6etcnQk+wAf4AB81fSjqD+oP6o9ynxZ8PD98WMNdj/vHU5NzU9dL++WBq4a7fpFpOU6vWkwmE/uFVA1372nk/3/L9SzTFFuJDdaj81YmrsAH09xf2w9A6bR78AF/iD/BB/jI5Qf0VZcfwAf4AB/z+iFXZ1FH7ZoVpfemp/6g/oA/4A/4A/5I6++hPiX1OfV5rv4wS5m1tTVr6GgTwShY9CS5GuFv3761Znhpvz9W/67lOP+5//zzj51TPnw158tdz+1tN2GY+xhNvwPWg7jyeAYfIZ+Rr7p8Tt4NfPcU+ND3rw0enNcXT7Eeqd4BH0+LD9ajrL+fEz78tQzVCeh26qi+OhIehAfpM5T7LOADfIAP8NHSp31OOpH+7mun9bCGu+xg+rxh+zy6VlZWrBk/5Knat18N93///dcaEDVeaun16DVEbdyHc6xH8LomrjqvY/ARvI7BB/iAP8qzJMAH+AAf4KOmHkBfoa/Sug7+gD/gD/gD/qibNUR9PutpD3/AH8vMH2Ypo4a5vJD0hPnFxUXRsz23X97rapJfX183Hec9usbjsZ1XHtGpd1vt9cSvfXEfrIe8IYmrgGfwcTCdUUG+6rxTwQf4gM/Legd8gA/wAT5q6iH0Ffrq5CR4r6OvOu9cnz/BB/gAH7P5AXzk+43PsT4fmgGJTkQn1upEa7jrdSUdoG1zc9N87GI/P/kVlfbryXQ13JVAWo6TT7k+1786rF+2vH953/n8cfH1xK/ncx+sB3E1i1fwEfIZ+Yq8m/IL+AAfqd5BlwQ9CD7AB/j432J9Aj7AB/gAHzX9C+oP6g/qD/qJQ31K6o/lrD/MUka/QNd6tqdejBIaekJeSaTGoy/1IFKTXudXI30RD3ldT/yZ3AfroVkCxFXwgAUfwfuNfNXN6AAfi+Ej9V732CKuiKt0hg15l7yL3i3Phoq/m1JeHZopRd4l735N3oXP+2e1oRMX04mlPgT5inz1NfmKuCJf1c66pP54fvWHNdz1hLoWcREPdQliLezq6qrr83ovebPf3d1Zw90HxyJe8PG1cx+sx+npqSOuAp7BR/BUJF91nnngA3zk+B58gA/lhxJnoK/QV/AH+CjlAfgD/oA/ZvND3BcBH+ADfICPIR1Nfb6c9blZymxtbbnJZGIN993dXXd8fOzW19et6Lq8vHT7+/vF/QoMNdM1sLTlOP+5Guio88i7qeZ8ues5Pz+3a+c+RtZIYz32bVAocdXhGXyEfAY+unwOPgLfgQ/wkeod8AE+cnoY/vh2/KHv/+rqivqjp/6ijpqvT+Fz+Bw+L/dvwAf4+B7wUaMf6CfS3+3rbz+0vrKGu+xc4sEmatLpVTJtsopRM760X6/HqMGrQqTlOP+5ejJejVHZ0dScL3c9sX8e98F6KF6Jq9eGJ705Aj5CPiNfdfkcfIAPnx9i3gYf4CPVe/AH/JHWA/AH/AF/dPo6V/eCD/ABPsBHKT/EfSr0FfoKfVXuNy8TPsxSZm1tbcYHPfWY6vNm98fKS73lOO8FLw89JZ+bmxtrvKce8UMejtp/e3trzUXuYzT9DlgP4srPRAAf/zOdUUG+ems/bpJ3g7fwU+Aj9Y5lPZ52PVJvTNaD9cjNFII/ZvnD5050O/WH6jfwgb5K+wVPoa/g87LXNetBPQg+wEdNvxU+f1g+t4a77GCOjo6sad3qob6ysmLN+tbjYk93Nc01vXsRD3mdV69/a+M+nGM9vkxnCRBXHZ7BR/CMAx/gI501Aj7AR85TEf6AP1JdC3/AH/BHmIkDPjpPavijPAMOfYW+Ah/go6a/h75CXy2zvjJLGTXMNzY2TDRcXFy4g4MDd3JyYk3svb09d3h4WNwv73U1z6+vr5uO8587Ho/tvPf391Xny11P/HoO98F6KF6Jq4Bn8BHyGfmqy+fgA3zk+B58gI9U78Ef8EdaD8Af8Af8Ua6XwQf4AB/go6afhr563vpKjUm9EUBftL4vTH93vm8unI9+/fVXs5RRw1ybhIJ+ZYpfOdHT56X9KtD1itKbN2+ajtMrqPrcjx8/2muI8oD3rzj0nc8fF1+Pt6PhPrrhs6wHcQU+8vkMfJB3U34Rf2h79erVN+PB2FIGHpzVF/B50F/kK/JVLl95+46n1u3+QZmcLh+qI8A5OE/rTOrBoFvBB/gAH8H64znzIP2r8dQWmj4cfTj6u13dMteH+/33379oYrEXzhr2oifWa/+u5q6EtRr1Lcf580hU6MIEUm2t59e/1yRZbdyHs2Y76/HFkj9x1eEJfIR8Bj66/J7iQ409bbV5n7ia5UniKh9X8Dm6RHkFfICPtD5YRJekPLVoXPnPQSd2bxbD5/B5rn5/Knw8FM7pM8z3cxbJu6mOWzTvsh6sR5xnXirOh+pl8IHezelds5T59OmTNau1XV1duf39fTeZTOzvmsB+fHxc3K9Bqfpg/cLXcpz/3HSa+9D5cvvjKbbcB+uheCWuAp7BR8hn5Ksun4MP8JHje/ABPlK9B3/AH2k9AH/AH/BHuV4GH+ADfICPmn4a+gp9hb4q95uXCR82NNVPohVByGdGRXc6xVg+77n9TLF92Cm2+o5Zj9H0O9CrKS3xqH//+fNnJ8sGxbJ/LdP//1Ic5/YzzZ1p7kxzZ5r7Y0xzjy1lyFefbfC61xfkXfIuefdl5F2PVfQVut3bHCmXo9upP+Dz+X4K/RLqc+l99C79xL5+K/2r5awHreG+vb3tjo6OrOBNp87r6fV0amw8lZ2pwkwVXuapwsIE+HBmUySR0Jof4vyhH0Bk86NtKK/k9p+dndmxrAfrQVzN8zL4OLU5MNrIV+gSdMlOUdeDD/ABPsBHTd2Pbu/6Iugr9JXXl9Qf1B/0Rcv8CT7y+DBLGTXSNjY2rFDtm8ab2y8vI325MocvTfHt+9y+ae6115NOea49Lr5e7qObqst6BBwQV23Tw8F5Xf4kroirk5MT49uaae61fEZcEVfE1SyuYl0LPsAH+AAfMe+W6l7qQepB1cPwR74vBD7AB/iYzQ/0E4O+pK7t8kPKH9Zw1+t/f/31lzUA3r17Z/7tcXEi3/TSfj2xqoa7PrjlODX59bn+1WE9caPN//+W64lfz285Lr5e7oP1SOOcuAp5AHyAD/ABDw7xK3wedBT8AX+kOhp8gI9cnYW+Ql+hr9BX6Kv6PhT6Cn2Fvnpd7JuCj+eHD7OUUdDWeranXo0ytFeTXI361JuqxntbBKPza3Brn6dRn/d2vI/7YD0Ud8RV8IAFH8Ezj3zVzegAH4vhI/Ve99giroirVO+Qd8m7qW4l7+bzbimvDs2UIu+Sd78m78Ln5VkR6MTZWXbwOXwOn+e919UXBB/gA3z048Ma7npCPedNVePpLkGsL1lT2fs8jUqezXd3d9Zw9wu1iEd0fO3cB+uhGQPEVedlnGIbfIAP8BG8KMHHrNccfP7egQ/wUeJJ8AE+4hlW8Af8kat7qT+oP3KzquAP+AP+mNWXcf4EH+BjmfFhljJbW1tmB6NNr7UdHx+79fV1a8JfXl66/f394n4JC4lODTJsOc5/rgaR6DzyBKs5X+56zs/P7dq5j5E1mlmPfRtwQ1x1eAYfIZ+Bjy6fg4/Ad+ADfKR6B3yAj5wehj++HX/o+7+6uqL+6Km/qKPm61P4HD6Hz8v9G/ABPr4HfNToB/qJ9Hf7+tsPra+s4S47l1rP9tRjXa+dqcGrQqTP673kza4n49UYlR3NIh7y+ly9VqqN+3BmF8F6TOyNC+Kqm4kAPoI3JPjoZnSAj+B9Bz7AR+qdCj7AR26mEPwBf6SzquAP+AP+KHtvgw/wAT7AR01/D32FvlpmfWWWMmtrazP+SxpgenFxYQ3Lzc1Nd39/X9zvj5WHU8tx/nPloadm/83NTdX5ctdze3trDXfuYzT9DlgP4srPRAAfIZ+Rr7p8Tt4NXmslfOitKxUKj8GDmjWiDR6c1xfkK/JVqj/JV8P5qkV/PxQPeqwO1QnoduqovjoSHoQH6TOU+yzgA3yAD/ARe6QP6T3qqOdXR1nDXXYwR0dH1gCQh7peOdJT0trUbJBVS2m/Fl1B0Hpc/LlquP/7779V58tdj17/1sZ9OPvRg/Xo4pW46vAMPkI+Ax8hn4MP8JHyNvgAH6negz/gj1w9AH/AH/BHuV4GH+ADfICPoX4a+gp9hb4q95uXCR9mKaMG7cbGhjXX9WTRwcGBOzk5sSb23t6eOzw8LO7XU4AKluvr66bj/OeOx2M7r369qzlf7npiOxzug/VQvBJXAc/gI+Qz8lWXz8EH+MjxPfgAH6negz/gj7QegD/gD/ijXC+DD/ABPsBHTT8NffW89ZX6knojmb5ofV+Y/u5831w4t4a77EdSj61aT3c9ma6GuxrdqfdOjae7AlnEpCfrcp6Z7951nkZ916PXjbVxH87eFGA9di1BElfOCT/gI+QP8PF/NmsDfARPRfABPlJ9AT7AR85zFP6AP9K6Bv6AP+CPMPMDfATP9rR/AX/AH+ADfAz1W6k/lrP+MEsZiQW/wGpaa4Bp7BWkQQal/RqIon8rYmk5TufU5yrw9GcNPPWe033n88fF1xNfK/fBehBXs3gFHyGfka/Iuym/tOAjbq7EfEdcEVdfE1foq+4H8pL+RCeG76clX72UuCrl1ZzeJ+/O12fgYzF8wOfk3T7eoc+Qjw/0LnoXvUtfdKjfii6Z1SXWcNcT0SIdbfrzzs7OjGf76elpcb8EsQqA1dXVpuPkbabPvbu7s4a7LyL8/2+5nvjfthwX3yf3wXqkcU5chTwAPsAH+IAHh/gVPg86Cv6AP+LZR+jd2fwJPsAH+Aiz09BX6Cv0VX0fCv6AP+AP+MPPHn0JferRL7/88kV+c/FEW91A7d/VoP/vv/+mli+1x62trU0tZH744YdpQ9///5bPkX+8Nu6j+8GE9XD2Aw5x5ZzwBD5CPgMf5N2U38AH+Ej1Bvxxa5oK/ujypY8P+AP+gD/K9SH4AB/gA3wM9W/QV+grry/RV/P5AnwsJz5Gf/755xf5r2u4g/eslGiq/bu819WwkEl+y3GyoNG///DhgzXK5W22yPn1OXptQb92ch/OvPBZD+Iqxi/4CPkMfCx/3hWPxB55Q7xUwkfL5xBXyx9XQ3GU7ifvPm7ebcEnOrHDp9cF5Cvy1UvJV+Cc+px8tfz5CpyDc3C+/Dj/rusoDU3VrylqVuemavtps6X9P/74owl5NXlbpvgyxTY/xdYPjmU9iKvDw0P7MUq4vL+/tx+khvCY288U9Oc9BZ28C3+A86A/yFfkq5OTkxm+Ax/gI8eT1B9dHQE+wAf4GNkPirk+BPgAH+ADfJTyQ9xvS+sPCVF5lbf0N9El6BLpsjSuRmq4a1DV0NTc0n6mbjN1m6nbTN0eyh9M3V7OqdsSI/CHsze09MOzCHYymUwt1vRjdpofc/vBB/jwb/jF8UFcoa/QV+gr9FXghyF+jQehDn1v1LX93yu6BF2CLnFOb8ih2/vrGvJu+H7Q7ej2nG63oan6RWdo2mxpP9OqmVbNtGqmVQ/lD6ZVz06rlogl7zr30vgjFpWfP3+eDvt+afcBnwc8xusoTIrPyFfkKz0Rpx8TwXnIc4+Fj1Je9ecjX5GvcnjMxYd+5K7VV/A5cfUYcUW+Iq6Iq3n9BJ939QX9kv78EH8/LXz+3POuNdz1ZN7QdOzSfgFIX8jq6qrb2QlTk9+/f+/6psb6/Xd3d9Z48l9q7XGlCdXcB+uhuCOuwrR3prkzzZ1p7kxzVxNiiF/h8063wB/wh/AibRzrWvABPtK6Bn2FvkJfoa/QV8P6kr7PfF8M/oA/4I/vgz/MUmZra8ulnpm1HuIqTFWUbG9vm5dg7XHeS+ns7Mya/fI8WsTLWJ7V5+fn9kQD9zGyRgHrceCIq+CBCz5CXgIfnccl+AAfuZkQ4AN8pF6V8Af8ker6b8EfNd6p5CvyFfkqzPxIZziBD/ABPsDHUH/vW/B5TX/vIfNVjX7Iebqjd9G7j6V3reH+6dOnZs9Z732n1yPU4BVQarxqU888PRkvIOq13ZxXWI0Hrl7n18Z9OHtdhfWY2BsXxJWzN0fAR/CABR+dFyH4eG2cAT5mPebBB/hIvUrhD/gD3V72ugYf4AN8gI+a/gX6Cn2Fvip7wYMP8LHM+DBLmbW1tamli16ZXVlZsWnfalhubm66+/v74n5/rLwuW47znysPPZ3z5uam6ny567m9vbXmCfcxmn4HrAdx5X0swUfIZ+SrLp+Td4PPawkfeutKhfRj8KC85rTBg/P6gnxFvkr1J/lqOF+16O+H4kGP1aE6Ad1OHdVXR8KD8CB9hnKfBXyAD/ABPmI/8yG9Rx31/Oooa7jLDib2ENIrFXpKWpuaDbJqKe3XoisI5M3Vclz8uWq4a6pvzfly16PXYbRxH85+9GA9unglrjpPPfAR8hn4CPkcfICPlLfBB/hI9R78AX/k6gH4A/6AP8p1L/gAH+ADfAz1xdBX6Cv0VbnfvEz4MEsZNWhbvde9J5SeAlSwXF9fmzfwkFdUun88HltTX7/e5Txlcx5LqVe8hI23B+A+WA/FB3G1MZ2JAD5CXiJf7dmsDfABPnKeiuADfKCvyrOIwAf4Ai4tRwAAIABJREFUAB/gY6jORF+hr9BXI3NKyPWFwAf4eCn4WMQLHp2ITszpRGu4y35EidG/4q7X6GNfwnfv3hX368l0NdwlQFqOk9+6Plev1gt4erLO+7D3nc8fF1+vXjfWxn04e1OA9SCuwEc+n4EP8m7KL/BH4HvwAT7AB3p3qB5Atwd9AX/AH2m9DD7AR66fgr5CX6Gv0Ffoq/p+8zLpK7OU0ROwXiCoaa0BprFXkAYZlPZLaOjfKom0HKdz6nPVpNef1Uj3ntN95/PHxdcTXyv3wXoQV7N4BR8hn5GvyLspv7TgIyb/mO+IK+Lqa+IKfdU9eFHSn+jE8P205KuXElelvJrT++Td+foMfCyGD/icvNvHO/QZ8vGB3kXvonfpiw71W9Els7rEGu56Ilqko01/3tnZmfFsPz09Le6XIFYBsLq62nScvM30uXd3d9Zw90WE//8t1xP/25bj4vvkPliPNM6Jq5AHwAf4AB/w4BC/wudBR8Ef8Ec8+wi9O5s/wQf4AB9HVnfn6l7wAT7AB/go5Qf6V/N9SuoP6o9cH/u59K/MUkZDslq9172HnQJcTXoNLE09a2o83WWIL2Ehz6Ocp9PeXucF1OeZp6EU2riPkf2AwXoc2KBQ4so54Qd8hPwBPjpPRfBxMp0ZAj7AR6ovwAf4yM0Ugj++HX/UeKeyHt9uPWrqM9aD9UjrfvQV+gp9FTzbwUd51uND8keNfsjNGCBfka8eK19Zw112LvFgxd3dXbN60SarmMlkUtyv10rU4BVQWo7zn6sn4yXk9Jpozfly1xP753EfrIfilbgKg4TBR8hn5Ksun4MP8OEHjce8DT7AR6r34A/4I60H4A/4A/5w9mZ2ru4FH+ADfICPUn6I+1ToK/QV+qrcb14mfIz++OOPL3q6/Pb21hrer169sua6f+L9559/dpeXl8X96+vrtv/NmzdNx/nP/fjxox17dXVl5x86X26/mv3auA/nWI8uXomrgGfwEfIZ+MjjQ29CKAe38MBzjSvdx83NTTWfPdf76ONdeBBdUhMfy8yD4Bzdviife75bZnzU5IeY758rD4JzcL4ozukzzOskcE49mPb34MG2fslQvUy+og+X6i/l3dFvv/32ZTweT58w1xNu2vwT50N/V2NDv+brv5bj/OfqIvSfGv3ahs6X2399fW3Hch/OGk2sh7OYIq46PIGPkM/AR5ffwUf3RhX4mOV78AE+Uh0Hf8AfaT0Af8AfuXoN/oA/4I9y/wR8gA/wAT6G+qvoq+XUVzY0VU+06ykCWbvoz2p+f/jwwRoSP/30kzVxS/vjadUtx/nPTafYDp0vt98PXOU+nGM9unglrgKewUfIZ+ADfKR81oKP+N/GfEdcEVdfE1foq379CZ8vN5+X8upQPUDeJe9+Td6Fz8m7tX2PFp0InxNXxBX9RFlg0xd97WRbXtNPXvb+rjXc5cEeT0SPpx/npqczHZnpyOn09Dh+Sn8mrubjZgh3TN1m6raISjmavEveJe8emXBDl7x3p6eng7oN/oA/4I95/vT5A3yAD/ABPlJ9ndMX1LVBf+vhRv0AobfpW+p68u68biOuiKu0rkOXLKcusaGpW1tbU892eRMdHoYptbkpvvH+h5wqrMQzdD6mCl+4dMo16zEfr2dnZ9aMkK8/cXVuTSpwPjIrFYnr7e3tpjzn8w5xdWKxJJ5gmjvT3B9rmjv5asN4i3zV6R3y7veXd5UD/v77b/TuwcFgfQY+vj981NQ18Af8kfYL0O3o9u9Bt9foB/qJ9BP7+qkP3fexhrse94+nJuemrpf2ywNXDSwRe8txetViMpnYL6QSDt7TyP//lutZpim2ShKsR+etTFyBD6a5v7YGdzrtHnzAH+JP8AE+cvkBfdXlB/ABPsDHvH7I1VnUUbtmRem96ak/qD/gD/gD/oA/0vp7qE9JfU59nqs/zFJmbW3NGjraRDAKFj1Zokb427dvrRle2u+P1b9rOc5/7j///GPnlA9fzfly13N7200Y5j5G0++A9SCuPJ7BR8hn5Ksun5N3A989BT70/WuDB+f1xVOsR6p3wMfT4oP1KOvv54QPfy1DdQK6nTqqr46EB+FB+gzlPgv4AB/gA3y09Gmfk06kv/vaaT2s4S57hT5v2D6PrpWVFWvGD3mq9u1Xw/3ff/+1BkSNl1p6PXqdUhv34RzrEbyuiavO6xh8BK9j8AE+4I/yLAnwAT7AB/ioqQfQV+irtK6DP+AP+AP+gD/qZg1Rn8962sMf8Mcy84dZyqhhnno6nZwET7w+T3d5ZKtJfn193eu1WPJmH4/H9mT7/f391Bu4xUNenxu/9sV9sB6KH+Kq8wAGH7MeZeSrbkYH+AAfOQ9Y8AE+Uv2Fvgoe2uADfICP8owv8AE+wAf4GOrDUH9Qf7yU+mMRL3h4EB7M8aA13PW6khpz2jY3N83HLvbzk19Rab+eTFfDXQm25Tj5lOtz/avD+mXL+5f3nc8fF19P/Ho+98F6EFezeAUfIZ+Rr8i7Kb+AD/CR6h10SdCD4AN8gI//LdYn4AN8gA/wUdO/oP6g/qD+oJ841Kek/ljO+sMsZfQEU61ne+rFKKGhJ+SVRGo8+lIPIjXpdX410hfxkNf1xJ/JfbAemiVAXAUPWPARvN/IV92MDvCxGD5S73WPLeKKuEpn2JB3ybvo3fJsqPi7KeXVoZlS5F3y7tfkXfi8f1YbOnExnVjqQ5CvyFdfk6+IK/JV7axL6o/nV39Yw11PqGsRF/FQlyDWwq6urro+r/eSN/vd3Z013H1wLOIFH18798F6nJ6eOuIq4Bl8BE9F8lXnmQc+wEeO78EH+FB+KHEG+gp9BX+Aj1IegD/gD/hjNj/EfRHwAT7AB/gY0tHU58tZn5ulzNbWlptMJtZw393ddcfHx259fd2KrsvLS7e/v1/cr8BQM10DS1uO85+rgY46jzyPas6Xu57z83O7du5jZI001mPfBoUSVx2ewUfIZ+Cjy+fgI/Ad+AAfqd4BH+Ajp4fhj2/HH/r+r66uqD966i/qqPn6FD6Hz+Hzcv8GfICP7wEfNfqBfiL93b7+9kPrK2u4y84lHoylJp1eJdMmqxg140v79XqMGrwqRFqO85+rJ+PVGJUdTc35ctcT++dxH6yH4pW4em140psj4CPkM/JVl8/BB/jw+SHmbfABPlK9B3/AH2k9AH/AH/BHp69zdS/4AB/gA3yU8kPcp0Jfoa/QV+V+8zLhwyxl1tbWZnzQU4+pPm92f6y81FuO817w8tBT8rm5ubHGe+oRP+ThqP23t7fWXOQ+RtPvgPUgrvxMBPDxP9MZFeSrt/bjJnk3eAs/BT5S71jW42nXI/XGZD1Yj9xMIfhjlj987kS3U3+ofgMf6Ku0X/AU+go+L3tdsx7Ug+ADfNT0W+Hzh+Vza7jLDubo6Mia1q0e6isrK9asbz0u9nRX01zTuxfxkNd59fq3Nu7DOdbjy3SWAHHV4Rl8BM848AE+0lkj4AN85DwV4Q/4I9W18Af8AX+EmTjgo/Okhj/KM+DQV+gr8AE+avp76Cv01TLrK7OUUcN8Y2PDRMPFxYU7ODhwJycn1sTe29tzh4eHxf3yXlfz/Pr6uuk4/7nj8djOe39/X3W+3PXEr+dwH6yH4pW4CngGHyGfka+6fA4+wEeO78EH+Ej1HvwBf6T1APwBf8Af5XoZfIAP8AE+avpp6Kvnra/UmNQbAfRF6/vC9Hfn++bC+ejXX381Sxk1zLVJKOhXpviVEz19XtqvAl2vKL1586bpOL2Cqs/9+PGjvYYoD3j/ikPf+fxx8fV4Oxruoxs+y3oQV+Ajn8/AB3k35Rfxh7ZXr159Mx6MLWXgwVl9AZ8H/UW+Il/l8pW373hq3e4flMnp8qE6ApyD87TOpB4MuhV8gA/wEaw/njMP0r8aT22h6cPRh6O/29Utc32433///YsmFnvhrGEvemK99u9q7kpYq1Hfcpw/j0SFLkwg1dZ6fv17TZLVxn04a7azHl8s+RNXHZ7AR8hn4KPL7yk+1NjTVpv3iatZniSu8nEFn6NLlFfAB/hI64NFdEnKU4vGlf8cdGL3ZjF8Dp/n6venwsdD4Zw+w3w/Z5G8m+q4RfMu68F6xHnmpeJ8qF4GH+jdnN41S5lPnz5Zs1rb1dWV29/fd5PJxP6uCezHx8fF/RqUqg/WL3wtx/nPTae5D50vtz+eYst9sB6KV+Iq4Bl8hHxGvuryOfgAHzm+Bx/gI9V78Af8kdYD8Af8AX+U62XwAT7AB/io6aehr9BX6Ktyv3mZ8GFDU/0kWhGEfGZUdKdTjOXzntvPFNuHnWKr75j1GE2/A72a0hKP+vefP392smxQLPvXMv3/L8Vxbj/T3JnmzjR3prk/xjT32FKGfPXZBq97fUHeJe+Sd19G3vVYRV+h273NkXI5up36Az6f76fQL6E+l95H79JP7Ou30r9aznrQGu7b29vu6OjICt506ryeXk+nxsZT2ZkqzFThZZ4qLEyAD2c2RRIJrfkhzh/6AUQ2P9qG8kpu/9nZmR3LerAexNU8L4OPU5sDo418hS5Bl+wUdT34AB/gA3zU1P3o9q4vgr5CX3l9Sf1B/UFftMyf4COPD7OUUSNtYyNMFWcaL9N4vZeznzbcEh9M3X7eU7clGmqmp8d5QJ5lSqIaAtGSH5hWnZ9WraYgedfZkGfi6tBmoAiX5N0LezMJfJzYdwA+uvwJPoI+Bx/oq5OTLj+gr9BXyo+l+gz+gD/S+IA/4A/4Y5Y/4/yZ4kM8qzcuW/oe5F3ybi7vWsNdr//99ddfJuDevXtn/u1x0Mk3vbRfT6yqYaKAbTlOzSZ9rn91WE/c+CK773z+uPh64tfzuQ/Wg7iaxSv4CPmMfEXeTfkFfICPVO+gS4IeBB/gA3yEHwHhD+rBoToT/oA/cv0U6g/qD/gD/oA/6vvNy1R/mKWMxHStZ3vq1ShDezXB1ahPvalqvLcVeDq/Brcu4iGv64n9sLgP1kNxR1wFD1jwETzzyFfdjA7wsRg+Uu91jy3iirhK9Q55l7ybzowh7+bzbimvDs2UIu+Sd78m78Ln5VkR6MTZWXbwOXwOn+e91+nDdbMJqAdn4wO9O6t3reGuJ9Rz3lQ1nu4SxAoyTWXv8zQqeTbf3d1Zw90H6iIe0fG1cx+sh2YMEFedl3GKbfABPsBH8KIEH7Nec/D5ewc+wEeJJ8EH+IhnWMEf8Eeu7qX+oP7IzaqCP+AP+GNWX8b5E3yAj2XGh1nKbG1tmR2MNr3ucnx87NbX160Jf3l56fb394v7JSwkOjXIsOU4/7kaRKLzyPOo5ny56zk/P7dr5z5G1mhmPfZtwA1x1eEZfIR8Bj66fA4+At+BD/CR6h3wAT5yehj++Hb8oe//6uqK+qOn/qKOmq9P4XP4HD4v92/AB/j4HvBRox/oJ9Lf7etvP7S+soa77FxqPdtTD3W9dqYGrwqRVu91Nfn1ZLwao3odYxEPeV2PXivVxn04s4tgPYirGM/gI3jGgY9uRgd5N3jygg/wkXoqgg/wkZspBH/AH+msKvgD/oA/yp684AN8gA/wUdPfQ1+hr5ZZX5mlzNra2owPugaYXlxcWCN8c3PT3d/fF/f7Y+Xh1HKc/1x56Kk5eHNzU3W+3PXc3t5aw537GE2/A9aDuPIzEcBHyGfkqy6fk3eD11wJH3rrSoXCY/CgZo1ogwfn9QX5inyV6k/y1XC+atHfD8WDHqtDdQK6nTqqr46EB+FB+gzlPgv4AB/gA3zEMwSG9B511POro6zhLjuYo6MjawDIQ12vHOkpaW1qNsiqpbRfi64gaD0u/lw13DW9u+Z8uevR69/auA9nP3qwHl28ElcdnsFHyGfgI+Rz8AE+Ut4GH+Aj1XvwB/yRqwfgD/gD/ijXy+ADfIAP8DHUT0Nfoa/QV+V+8zLhwyxl1KDd2Niw5rqeLDo4OHAnJyfWxN7b23OHh4fF/XoKUMFyfX3ddJz/3PF4bOfVr3c158tdT2yfwX2wHopX4irgGXyEfEa+6vI5+AAfOb4HH+Aj1XvwB/yR1gPwB/wBf5TrZfABPsAH+Kjpp6Gvnre+Ul9SbyTTF63vC9Pfne+bC+fWcJf9SOqxVevprifT1XBXozv13qnxdFcgi5j0ZF3OM/Pdu87TqO969LqxNu7D2ZsCrMeuJUjiyjnhB3yE/AE+/s9mbYCP4KkIPsBHqi/AB/jIeY7CH/BHWtfAH/AH/BFmfoCP4Nme9i/gD/gDfICPoX4r9cdy1h9mKSOx4BdYTWsNMI29gjTIoLRfA1H0b0UsLcfpnPpcBZ7+rIGn3nO673z+uPh64mvlPlgP4moWr+Aj5DPyFXk35ZcWfMTNlZjviCvi6mviCn3V/UBe0p/oxPD9tOSrlxJXpbya0/vk3fn6DHwshg/4nLzbxzv0GfLxgd5F76J36YsO9VvRJbO6xBrueiJapKNNf97Z2ZnxbD89PS3ulyBWAbC6utp0nLzN9Ll3d3fWcPdFhP//LdcT/9uW4+L75D5YjzTOiauQB8AH+AAf8OAQv8LnQUfBH/BHPPsIvTubP8EH+AAfYXYa+gp9hb6q70PBH/AH/AF/+NmjL6FPPfrll1++yG8unmirG6j9uxr0//3339Typfa4tbW1qYXMDz/8MG3o+//f8jnyj9fGfXQ/mLAezn7AIa6cE57AR8hn4IO8m/Ib+AAfqd6AP25NU8EfXb708QF/wB/wR7k+BB/gA3yAj6H+DfoKfeX1JfpqPl+Aj+XEx+jPP//8Iv91DXfwnpUSTbV/l/e6GhYyyW85ThY0+vcfPnywRrm8zRY5vz5Hry3o107uw5kXPutBXMX4BR8hn4GP5c+74pHYI2+Il0r4aPkc4mr542oojtL95N3Hzbst+EQndvj0uoB8Rb56KfkKnFOfk6+WP1+Bc3AOzpcf5991HaWhqfo1Rc3q3FRtP222tP/HH380Ia8mb8sUX6bY5qfY+sGxrAdxdXh4aD9GCZf39/f2g9QQHnP7mYL+vKegk3fhD3Ae9Af5inx1cnIyw3fgA3zkeJL6o6sjwAf4AB8j+0Ex14cAH+ADfICPUn6I+21p/SEhKq/ylv4mugRdIl2WxtVIDXcNqhqamlvaz9Rtpm4zdZup20P5g6nbyzl1W2IE/nD2hpZ+eBbBTiaTqcWafsxO82NuP/gAH/4Nvzg+iCv0FfoKfYW+CvwwxK/xINSh7426tv97RZegS9AlzukNOXR7f11D3g3fD7od3Z7T7TY0Vb/oDE2bLe1nWjXTqplWzbTqofzBtOrZadUSseRd514af8Si8vPnz9Nh3y/tPuDzgMd4HYVJ8Rn5inylJ+L0YyI4D3nusfBRyqv+fOQr8lUOj7n40I/ctfoKPieuHiOuyFfEFXE1r5/g866+oF/Snx/i76eFz5973rWGu57MG5qOXdovAOkLWV1ddTs7YWry+/fvXd/UWL//7u7OGk/+S609rjShmvtgPRR3xFWY9s40d6a5M82dae5qQgzxK3ze6Rb4A/4QXqSNY10LPsBHWtegr9BX6Cv0FfpqWF/S95nvi8Ef8Af88X3wh1nKbG1tudQzs9ZDXIWpipLt7W3zEqw9znspnZ2dWbNfnkeLeBnLs/r8/NyeaOA+RtYoYD0OHHEVPHDBR8hL4KPzuAQf4CM3EwJ8gI/UqxL+gD9SXf8t+KPGO5V8Rb4iX4WZH+kMJ/ABPsAH+Bjq730LPq/p7z1kvqrRDzlPd/Quevex9K413D99+tTsOeu97/R6hBq8AkqNV23qmacn4wVEvbab8wqr8cDV6/zauA9nr6uwHhN744K4cvbmCPgIHrDgo/MiBB+vjTPAx6zHPPgAH6lXKfwBf6Dby17X4AN8gA/wUdO/QF+hr9BXZS948AE+lhkfZimztrY2tXTRK7MrKys27VsNy83NTXd/f1/c74+V12XLcf5z5aGnc97c3FSdL3c9t7e31jzhPkbT74D1IK68jyX4CPmMfNXlc/Ju8Hkt4UNvXamQfgwelNecNnhwXl+Qr8hXqf4kXw3nqxb9/VA86LE6VCeg26mj+upIeBAepM9Q7rOAD/ABPsBH7Gc+pPeoo55fHWUNd9nBxB5CeqVCT0lrU7NBVi2l/Vp0BYG8uVqOiz9XDXdN9a05X+569DqMNu7D2Y8erEcXr8RV56kHPkI+Ax8hn4MP8JHyNvgAH6negz/gj1w9AH/AH/BHue4FH+ADfICPob4Y+gp9hb4q95uXCR9mKaMGbav3uveE0lOACpbr62vzBh7yikr3j8dja+rr17ucp2zOYyn1ipew8fYA3AfrofggrjamMxHAR8hL5Ks9m7UBPsBHzlMRfIAP9FV5FhH4AB/gA3wM1ZnoK/QV+mpkTgm5vhD4AB8vBR+LeMGjE9GJOZ1oDXfZjygx+lfc9Rp97Ev47t274n49ma6GuwRIy3HyW9fn6tV6AU9P1nkf9r7z+ePi69Xrxtq4D2dvCrAexBX4yOcz8EHeTfkF/gh8Dz7AB/hA7w7VA+j2oC/gD/gjrZfBB/jI9VPQV+gr9BX6Cn1V329eJn1lljJ6AtYLBDWtNcA09grSIIPSfgkN/VslkZbjdE59rpr0+rMa6d5zuu98/rj4euJr5T5YD+JqFq/gI+Qz8hV5N+WXFnzE5B/zHXFFXH1NXKGvugcvSvoTnRi+n5Z89VLiqpRXc3qfvDtfn4GPxfABn5N3+3iHPkM+PtC76F30Ln3RoX4rumRWl1jDXU9Ei3S06c87Ozsznu2np6fF/RLEKgBWV1ebjpO3mT737u7OGu6+iPD/v+V64n/bclx8n9wH65HGOXEV8gD4AB/gAx4c4lf4POgo+AP+iGcfoXdn8yf4AB/g48jq7lzdCz7AB/gAH6X8QP9qvk9J/UH9ketjP5f+lVnKaEhWq/e697BTgKtJr4GlqWdNjae7DPElLOR5lPN02tvrvID6PPM0lEIb9zGyHzBYjwMbFEpcOSf8gI+QP8BH56kIPk6mM0PAB/hI9QX4AB+5mULwx7fjjxrvVNbj261HTX3GerAead2PvkJfoa+CZzv4KM96fEj+qNEPuRkD5Cvy1WPlK2u4y84lHqy4u7trVi/aZBUzmUyK+/VaiRq8AkrLcf5z9WS8hJxeE605X+56Yv887oP1ULwSV2GQMPgI+Yx81eVz8AE+/KDxmLfBB/hI9R78AX+k9QD8AX/AH87ezM7VveADfIAP8FHKD3GfCn2FvkJflfvNy4SP0R9//PFFT5ff3t5aw/vVq1fWXPdPvP/888/u8vKyuH99fd32v3nzpuk4/7kfP360Y6+uruz8Q+fL7VezXxv34Rzr0cUrcRXwDD5CPgMfeXzoTQjl4BYeeK5xpfu4ubmp5rPneh99vAsPoktq4mOZeRCco9sX5XPPd8uMj5r8EPP9c+VBcA7OF8U5fYZ5nQTOqQfT/h482NYvGaqXyVf04VL9pbw7+u23376Mx+PpE+Z6wk2bf+J86O9qbOjXfP3Xcpz/XF2E/lOjX9vQ+XL7r6+v7Vjuw1mjifVwFlPEVYcn8BHyGfjo8jv46N6oAh+zfA8+wEeq4+AP+COtB+AP+CNXr8Ef8Af8Ue6fgA/wAT7Ax1B/FX21nPrKhqbqiXY9RSBrF/1Zze8PHz5YQ+Knn36yJm5pfzytuuU4/7npFNuh8+X2+4Gr3IdzrEcXr8RVwDP4CPkMfICPlM9a8BH/25jviCvi6mviCn3Vrz/h8+Xm81JeHaoHyLvk3a/Ju/A5ebe279GiE+Fz4oq4op8oC2z6oq+dbMtr+snL3t+1hrs82OOJ6PH049z0dKYjMx05nZ4ex0/pz8TVfNwM4Y6p20zdFlEpR5N3ybvk3SMTbuiS9+709HRQt8Ef8Af8Mc+fPn+AD/ABPsBHqq9z+oK6NuhvPdyoHyD0Nn1LXU/enddtxBVxldZ16JLl1CU2NHVra2vq2S5vosPDMKU2N8U33v+QU4WVeIbOx1ThC5dOuWY95uP17OzMmhHy9Seuzq1JBc5HZqUicb29vd2U53zeIa5OLJbEE0xzZ5r7Y01zJ19tGG+Rrzq9Q979/vKucsDff/+N3j04GKzPwMf3h4+augb+gD/SfgG6Hd3+Pej2Gv1AP5F+Yl8/9aH7PtZw1+P+8dTk3NT10n554KqBJWJvOU6vWkwmE/uFVMLBexr5/99yPcs0xVZJgvXovJWJK/DBNPfX1uBOp92DD/hD/Ak+wEcuP6CvuvwAPsAH+JjXD7k6izpq16wovTc99Qf1B/wBf8Af8Edafw/1KanPqc9z9YdZyqytrVlDR5sIRsGiJ0vUCH/79q01w0v7/bH6dy3H+c/9559/7Jzy4as5X+56bm+7CcPcx2j6HbAexJXHM/gI+Yx81eVz8m7gu6fAh75/bfDgvL54ivVI9Q74eFp8sB5l/f2c8OGvZahOQLdTR/XVkfAgPEifodxnAR/gA3yAj5Y+7XPSifR3XzuthzXcZa/Q5w3b59G1srJizfghT9W+/Wq4//vvv9aAqPFSS69Hr1Nq4z6cYz2C1zVx1Xkdg4/gdQw+wAf8UZ4lAT7AB/gAHzX1APoKfZXWdfAH/AF/wB/wR92sIerzWU97+AP+WGb+MEsZNcxTT6eTk+CJ1+fpLo9sNcmvr697vRZL3uzj8diebL+/v596A7d4yOtz49e+uA/WQ/FDXHUewOBj1qOMfNXN6AAf4CPnAQs+wEeqv9BXwUMbfIAP8FGe8QU+wAf4AB9DfRjqD+qPl1J/LOIFDw/CgzketIa7XldSY07b5uam+djFfn7yKyrt15PpargrwbYcJ59yfa5/dVi/bHn/8r7z+ePi64lfz+c+WA/iahav4CPkM/IVeTflF/ABPlK9gy4JehB8gA/w8b/F+gR8gA/wAT5q+hfUH9Qf1B/0E4f6lNQfy1l/mKWMnmCq9WxPvRglNPSEvJLmN1uAAAAgAElEQVRIjUdf6kGkJr3Or0b6Ih7yup74M7kP1kOzBIir4AELPoL3G/mqm9EBPhbDR+q97rFFXBFX6Qwb8i55F71bng0VfzelvDo0U4q8S979mrwLn/fPakMnLqYTS30I8hX56mvyFXFFvqqddUn98fzqD2u46wl1LeIiHuoSxFrY1dVV1+f1XvJmv7u7s4a7D45FvODja+c+WI/T01NHXAU8g4/gqUi+6jzzwAf4yPE9+AAfyg8lzkBfoa/gD/BRygPwB/wBf8zmh7gvAj7AB/gAH0M6mvp8Oetzs5TZ2tpyk8nEGu67u7vu+PjYra+vW9F1eXnp9vf3i/sVGGqma2Bpy3H+czXQUeeR51HN+XLXc35+btfOfYyskcZ67NugUOKqwzP4CPkMfHT5HHwEvgMf4CPVO+ADfOT0MPzx7fhD3//V1RX1R0/9RR01X5/C5/A5fF7u34AP8PE94KNGP9BPpL/b199+aH1lDXfZucSDsdSk06tk2mQVo2Z8ab9ej1GDV4VIy3H+c/VkvBqjsqOpOV/uemL/PO6D9VC8ElevDU96cwR8hHxGvuryOfgAHz4/xLwNPsBHqvfgD/gjrQfgD/gD/uj0da7uBR/gA3yAj1J+iPtU6Cv0Ffqq3G9eJnyYpcza2tqMD3rqMdXnze6PlZd6y3HeC14eeko+Nzc31nhPPeKHPBy1//b21pqL3Mdo+h2wHsSVn4kAPv5nOqOCfPXWftwk7wZv4afAR+ody3o87Xqk3pisB+uRmykEf8zyh8+d6HbqD9Vv4AN9lfYLnkJfwedlr2vWg3oQfICPmn4rfP6wfG4Nd9nBHB0dWdO61UN9ZWXFmvWtx8We7mqaa3r3Ih7yOq9e/9bGfTjHenyZzhIgrjo8g4/gGQc+wEc6awR8gI+cpyL8AX+kuhb+gD/gjzATB3x0ntTwR3kGHPoKfQU+wEdNfw99hb5aZn1lljJqmG9sbJhouLi4cAcHB+7k5MSa2Ht7e+7w8LC4X97rap5fX183Hec/dzwe23nv7++rzpe7nvj1HO6D9VC8ElcBz+Aj5DPyVZfPwQf4yPE9+AAfqd6DP+CPtB6AP+AP+KNcL4MP8AE+wEdNPw199bz1lRqTeiOAvmh9X5j+7nzfXDgf/frrr2Ypo4a5NgkF/coUv3Kip89L+1Wg6xWlN2/eNB2nV1D1uR8/frTXEOUB719x6DufPy6+Hm9Hw310w2dZD+IKfOTzGfgg76b8Iv7Q9urVq2/Gg7GlDDw4qy/g86C/yFfkq1y+8vYdT63b/YMyOV0+VEeAc3Ce1pnUg0G3gg/wAT6C9cdz5kH6V+OpLTR9OPpw9He7umWuD/f7779/0cRiL5w17EVPrNf+Xc1dCWs16luO8+eRqNCFCaTaWs+vf69Jstq4D2fNdtbjiyV/4qrDE/gI+Qx8dPk9xYcae9pq8z5xNcuTxFU+ruBzdInyCvgAH2l9sIguSXlq0bjyn4NO7N4shs/h81z9/lT4eCic02eY7+cskndTHbdo3mU9WI84z7xUnA/Vy+ADvZvTu2Yp8+nTJ2tWa7u6unL7+/tuMpnY3zWB/fj4uLhfg1L1wfqFr+U4/7npNPeh8+X2x1NsuQ/WQ/FKXAU8g4+Qz8hXXT4HH+Ajx/fgA3ykeg/+gD/SegD+gD/gj3K9DD7AB/gAHzX9NPQV+gp9Ve43LxM+bGiqn0QrgpDPjIrudIqxfN5z+5li+7BTbPUdsx6j6XegV1Na4lH//vPnz06WDYpl/1qm//+lOM7tZ5o709yZ5s4098eY5h5bypCvPtvgda8vyLvkXfLuy8i7HqvoK3S7tzlSLke3U3/A5/P9FPol1OfS++hd+ol9/Vb6V8tZD1rDfXt72x0dHVnBm06d19Pr6dTYeCo7U4WZKrzMU4WFCfDhzKZIIqE1P8T5Qz+AyOZH21Beye0/OzuzY1kP1oO4mudl8HFqc2C0ka/QJeiSnaKuBx/gA3yAj5q6H93e9UXQV+grry+pP6g/6IuW+RN85PFhljJqpG1shKniTONlGq/3cvbThlvig6nbz3vqtkRDzfT0OA/Is0xJVEMgWvID06rz06rVFCTvOhvyTFwd2gwU4ZK8e2FvJoGPE/sOwEeXP8FH0OfgA311ctLlB/QV+kr5sVSfwR/wRxof8Af8AX/M8mecP1N8iGf1xmVL34O8S97N5V1ruOv1v7/++ssE3Lt378y/PQ46+aaX9uuJVTVMFLAtx6nZpM/1rw7riRtfZPedzx8XX0/8ej73wXoQV7N4BR8hn5GvyLspv4AP8JHqHXRJ0IPgA3yAj/AjIPxBPThUZ8If8Eeun0L9Qf0Bf8Af8Ed9v3mZ6g+zlJGYrvVsT70aZWivJrga9ak3VY33tgJP59fg1kU85HU9sR8W98F6KO6Iq+ABCz6CZx75qpvRAT4Ww0fqve6xRVwRV6neIe+Sd9OZMeTdfN4t5dWhmVLkXfLu1+Rd+Lw8KwKdODvLDj6Hz+HzvPc6fbhuNgH14Gx8oHdn9a413PWEes6bqsbTXYJYQaap7H2eRiXP5ru7O2u4+0BdxCM6vnbug/XQjAHiqvMyTrENPsAH+AhelOBj1msOPn/vwAf4KPEk+AAf8Qwr+AP+yNW91B/UH7lZVfAH/AF/zOrLOH+CD/CxzPgwS5mtrS2zg9Gm112Oj4/d+vq6NeEvLy/d/v5+cb+EhUSnBhm2HOc/V4NIdB55HtWcL3c95+fndu3cx8gazazHvg24Ia46PIOPkM/AR5fPwUfgO/ABPlK9Az7AR04Pwx/fjj/0/V9dXVF/9NRf1FHz9Sl8Dp/D5+X+DfgAH98DPmr0A/1E+rt9/e2H1lfWcJedS61ne+qhrtfO1OBVIdLqva4mv56MV2NUr2Ms4iGv69Frpdq4D2d2EawHcRXjGXwEzzjw0c3oIO8GT17wAT5ST0XwAT5yM4XgD/gjnVUFf8Af8EfZkxd8gA/wAT5q+nvoK/TVMusrs5RZW1ub8UHXANOLiwtrhG9ubrr7+/vifn+sPJxajvOfKw89NQdvbm6qzpe7ntvbW2u4cx+j6XfAehBXfiYC+Aj5jHzV5XPybvCaK+FDb12pUHgMHtSsEW3w4Ly+IF+Rr1L9Sb4azlct+vuheNBjdahOQLdTR/XVkfAgPEifodxnAR/gA3yAj3iGwJDeo456fnWUNdxlB3N0dGQNAHmo65UjPSWtTc0GWbWU9mvRFQStx8Wfq4a7pnfXnC93PXr9Wxv34exHD9aji1fiqsMz+Aj5DHyEfA4+wEfK2+ADfKR6D/6AP3L1APwBf8Af5XoZfIAP8AE+hvpp6Cv0Ffqq3G9eJnyYpYwatBsbG9Zc15NFBwcH7uTkxJrYe3t77vDwsLhfTwEqWK6vr5uO8587Ho/tvPr1ruZ8ueuJ7TO4D9ZD8UpcBTyDj5DPyFddPgcf4CPH9+ADfKR6D/6AP9J6AP6AP+CPcr0MPsAH+AAfNf009NXz1lfqS+qNZPqi9X1h+rvzfXPh3Brush9JPbZqPd31ZLoa7mp0p947NZ7uCmQRk56sy3lmvnvXeRr1XY9eN9bGfTh7U4D12LUESVw5J/yAj5A/wMf/2awN8BE8FcEH+Ej1BfgAHznPUfgD/kjrGvgD/oA/wswP8BE829P+BfwBf4AP8DHUb6X+WM76wyxlJBb8AqtprQGmsVeQBhmU9msgiv6tiKXlOJ1Tn6vA05818NR7Tvedzx8XX098rdwH60FczeIVfIR8Rr4i76b80oKPuLkS8x1xRVx9TVyhr7ofyEv6E50Yvp+WfPVS4qqUV3N6n7w7X5+Bj8XwAZ+Td/t4hz5DPj7Qu+hd9C590aF+K7pkVpdYw11PRIt0tOnPOzs7M57tp6enxf0SxCoAVldXm46Tt5k+9+7uzhruvojw/7/leuJ/23JcfJ/cB+uRxjlxFfIA+AAf4AMeHOJX+DzoKPgD/ohnH6F3Z/Mn+AAf4CPMTkNfoa/QV/V9KPgD/oA/4A8/e/Ql9KlHv/zyyxf5zcUTbXUDtX9Xg/6///6bWr7UHre2tja1kPnhhx+mDX3//1s+R/7x2riP7gcT1sPZDzjElXPCE/gI+Qx8kHdTfgMf4CPVG/DHrWkq+KPLlz4+4A/4A/4o14fgA3yAD/Ax1L9BX6GvvL5EX83nC/CxnPgY/fnnn1/kv67hDt6zUqKp9u/yXlfDQib5LcfJgkb//sOHD9Yol7fZIufX5+i1Bf3ayX0488JnPYirGL/gI+Qz8LH8eVc8EnvkDfFSCR8tn0NcLX9cDcVRup+8+7h5twWf6MQOn14XkK/IVy8lX4Fz6nPy1fLnK3AOzsH58uP8u66jNDRVv6aoWZ2bqu2nzZb2//jjjybk1eRtmeLLFNv8FFs/OJb1IK4ODw/txyjh8v7+3n6QGsJjbj9T0J/3FHTyLvwBzoP+IF+Rr05OTmb4DnyAjxxPUn90dQT4AB/gY2Q/KOb6EOADfIAP8FHKD3G/La0/JETlVd7S30SXoEuky9K4GqnhrkFVQ1NzS/uZus3UbaZuM3V7KH8wdXs5p25LjMAfzt7Q0g/PItjJZDK1WNOP2Wl+zO0HH+DDv+EXxwdxhb5CX6Gv0FeBH4b4NR6EOvS9Udf2f6/oEnQJusQ5vSGHbu+va8i74ftBt6Pbc7rdhqbqF52habOl/UyrZlo106qZVj2UP5hWPTutWiKWvOvcS+OPWFR+/vx5Ouz7pd0HfB7wGK+jMCk+I1+Rr/REnH5MBOchzz0WPkp51Z+PfEW+yuExFx/6kbtWX8HnxNVjxBX5irgirub1E3ze1Rf0S/rzQ/z9tPD5c8+71nDXk3lD07FL+wUgfSGrq6tuZydMTX7//r3rmxrr99/d3VnjyX+ptceVJlRzH6yH4o64CtPemebONHemuTPNXU2IIX6FzzvdAn/AH8KLtHGsa8EH+EjrGvQV+gp9hb5CXw3rS/o+830x+AP+gD++D/4wS5mtrS2XembWeoirMFVRsr29bV6Ctcd5L6WzszNr9svzaBEvY3lWn5+f2xMN3MfIGgWsx4EjroIHLvgIeQl8dB6X4AN85GZCgA/wkXpVwh/wR6rrvwV/1Hinkq/IV+SrMPMjneEEPsAH+AAfQ/29b8HnNf29h8xXNfoh5+mO3kXvPpbetYb7p0+fmj1nvfedXo9Qg1dAqfGqTT3z9GS8gKjXdnNeYTUeuHqdXxv34ex1FdZjYm9cEFfO3hwBH8EDFnx0XoTg47VxBviY9ZgHH+Aj9SqFP+APdHvZ6xp8gA/wAT5q+hfoK/QV+qrsBQ8+wMcy48MsZdbW1qaWLnpldmVlxaZ9q2G5ubnp7u/vi/v9sfK6bDnOf6489HTOm5ubqvPlruf29taaJ9zHaPodsB7ElfexBB8hn5GvunxO3g0+ryV86K0rFdKPwYPymtMGD87rC/IV+SrVn+Sr4XzVor8figc9VofqBHQ7dVRfHQkPwoP0Gcp9FvABPsAH+Ij9zIf0HnXU86ujrOEuO5jYQ0ivVOgpaW1qNsiqpbRfi64gkDdXy3Hx56rhrqm+NefLXY9eh9HGfTj70YP16OKVuOo89cBHyGfgI+Rz8AE+Ut4GH+Aj1XvwB/yRqwfgD/gD/ijXveADfIAP8DHUF0Nfoa/QV+V+8zLhwyxl1KBt9V73nlB6ClDBcn19bd7AQ15R6f7xeGxNff16l/OUzXkspV7xEjbeHoD7YD0UH8TVxnQmAvgIeYl8tWezNsAH+Mh5KoIP8IG+Ks8iAh/gA3yAj6E6E32FvkJfjcwpIdcXAh/g46XgYxEveHQiOjGnE63hLvsRJUb/irteo499Cd+9e1fcryfT1XCXAGk5Tn7r+ly9Wi/g6ck678Pedz5/XHy9et1YG/fh7E0B1oO4Ah/5fAY+yLspv8Afge/BB/gAH+jdoXoA3R70BfwBf6T1MvgAH7l+CvoKfYW+Ql+hr+r7zcukr8xSRk/AeoGgprUGmMZeQRpkUNovoaF/qyTScpzOqc9Vk15/ViPde073nc8fF19PfK3cB+tBXM3iFXyEfEa+Iu+m/NKCj5j8Y74jroirr4kr9FX34EVJf6ITw/fTkq9eSlyV8mpO75N35+sz8LEYPuBz8m4f79BnyMcHehe9i96lLzrUb0WXzOoSa7jriWiRjjb9eWdnZ8az/fT0tLhfglgFwOrqatNx8jbT597d3VnD3RcR/v+3XE/8b1uOi++T+2A90jgnrkIeAB/gA3zAg0P8Cp8HHQV/wB/x7CP07mz+BB/gA3wcWd2dq3vBB/gAH+CjlB/oX833Kak/qD9yfezn0r8ySxkNyWr1XvcedgpwNek1sDT1rKnxdJchvoSFPI9ynk57e50XUJ9nnoZSaOM+RvYDButxYINCiSvnhB/wEfIH+Og8FcHHyXRmCPgAH6m+AB/gIzdTCP74dvxR453Keny79aipz1gP1iOt+9FX6Cv0VfBsBx/lWY8PyR81+iE3Y4B8Rb56rHxlDXfZucSDFXd3d83qRZusYiaTSXG/XitRg1dAaTnOf66ejJeQ02uiNefLXU/sn8d9sB6KV+IqDBIGHyGfka+6fA4+wIcfNB7zNvgAH6negz/gj7QegD/gD/jD2ZvZuboXfIAP8AE+Svkh7lOhr9BX6Ktyv3mZ8DH6448/vujp8tvbW2t4v3r1yprr/on3n3/+2V1eXhb3r6+v2/43b940Hec/9+PHj3bs1dWVnX/ofLn9avZr4z6cYz26eCWuAp7BR8hn4COPD70JoRzcwgPPNa50Hzc3N9V89lzvo4934UF0SU18LDMPgnN0+6J87vlumfFRkx9ivn+uPAjOwfmiOKfPMK+TwDn1YNrfgwfb+iVD9TL5ij5cqr+Ud0e//fbbl/F4PH3CXE+4afNPnA/9XY0N/Zqv/1qO85+ri9B/avRrGzpfbv/19bUdy304azSxHs5iirjq8AQ+Qj4DH11+Bx/dG1XgY5bvwQf4SHUc/AF/pPUA/AF/5Oo1+AP+gD/K/RPwAT7AB/gY6q+ir5ZTX9nQVD3RrqcIZO2iP6v5/eHDB2tI/PTTT9bELe2Pp1W3HOc/N51iO3S+3H4/cJX7cI716OKVuAp4Bh8hn4EP8JHyWQs+4n8b8x1xRVx9TVyhr/r1J3y+3HxeyqtD9QB5l7z7NXkXPifv1vY9WnQifE5cEVf0E2WBTV/0tZNteU0/edn7u9Zwlwd7PBE9nn6cm57OdGSmI6fT0+P4Kf2ZuJqPmyHcMXWbqdsiKuVo8i55l7x7ZMINXfLenZ6eDuo2+AP+gD/m+dPnD/ABPsAH+Ej1dU5fUNcG/a2HG/UDhN6mb6nrybvzuo24Iq7Sug5dspy6xIambm1tTT3b5U10eBim1Oam+Mb7H3KqsBLP0PmYKnzh0inXrMd8vJ6dnVkzQr7+xNW5NanA+cisVCSut7e3m/KczzvE1YnFkniCae5Mc3+sae7kqw3jLfJVp3fIu99f3lUO+Pvvv9G7BweD9Rn4+P7wUVPXwB/wR9ovQLej278H3V6jH+gn0k/s66c+dN/HGu563D+empybul7aLw9cNbBE7C3H6VWLyWRiv5BKOHhPI///W65nmabYKkmwHp23MnEFPpjm/toa3Om0e/ABf4g/wQf4yOUH9FWXH8AH+AAf8/ohV2dRR+2aFaX3pqf+oP6AP+AP+AP+SOvvoT4l9Tn1ea7+MEuZtbU1a+hoE8EoWPRkiRrhb9++tWZ4ab8/Vv+u5Tj/uf/884+dUz58NefLXc/tbTdhmPsYTb8D1oO48ngGHyGfka+6fE7eDXz3FPjQ968NHpzXF0+xHqneAR9Piw/Wo6y/nxM+/LUM1QnoduqovjoSHoQH6TOU+yzgA3yAD/DR0qd9TjqR/u5rp/WwhrvsFfq8Yfs8ulZWVqwZP+Sp2rdfDfd///3XGhA1Xmrp9eh1Sm3ch3OsR/C6Jq46r2PwEbyOwQf4gD/KsyTAB/gAH+Cjph5AX6Gv0roO/oA/4A/4A/6omzVEfT7raQ9/wB/LzB9mKaOGeerpdHISPPH6PN3lka0m+fX1da/XYsmbfTwe25Pt9/f3U2/gFg95fW782hf3wXoofoirzgMYfMx6lJGvuhkd4AN85DxgwQf4SPUX+ip4aIMP8AE+yjO+wAf4AB/gY6gPQ/1B/fFS6o9FvODhQXgwx4PWcNfrSmrMadvc3DQfu9jPT35Fpf16Ml0NdyXYluPkU67P9a8O65ct71/edz5/XHw98ev53AfrQVzN4hV8hHxGviLvpvwCPsBHqnfQJUEPgg/wAT7+t1ifgA/wAT7AR03/gvqD+oP6g37iUJ+S+mM56w+zlNETTLWe7akXo4SGnpBXEqnx6Es9iNSk1/nVSF/EQ17XE38m98F6aJYAcRU8YMFH8H4jX3UzOsDHYvhIvdc9togr4iqdYUPeJe+id8uzoeLvppRXh2ZKkXfJu1+Td+Hz/llt6MTFdGKpD0G+Il99Tb4irshXtbMuqT+eX/1hDXc9oa5FXMRDXYJYC7u6uur6vN5L3ux3d3fWcPfBsYgXfHzt3AfrcXp66oirgGfwETwVyVedZx74AB85vgcf4EP5ocQZ6Cv0FfwBPkp5AP6AP+CP2fwQ90XAB/gAH+BjSEdTny9nfW6WMltbW24ymVjDfXd31x0fH7v19XUrui4vL93+/n5xvwJDzXQNLG05zn+uBjrqPPI8qjlf7nrOz8/t2rmPkTXSWI99GxRKXHV4Bh8hn4GPLp+Dj8B34AN8pHoHfICPnB6GP74df+j7v7q6ov7oqb+oo+brU/gcPofPy/0b8AE+vgd81OgH+on0d/v62w+tr6zhLjuXeDCWmnR6lUybrGLUjC/t1+sxavCqEGk5zn+unoxXY1R2NDXny11P7J/HfbAeilfi6rXhSW+OgI+Qz8hXXT4HH+DD54eYt8EH+Ej1HvwBf6T1APwBf8Afnb7O1b3gA3yAD/BRyg9xnwp9hb5CX5X7zcuED7OUWVtbm/FBTz2m+rzZ/bHyUm85znvBy0NPyefm5sYa76lH/JCHo/bf3t5ac5H7GE2/A9aDuPIzEcDH/0xnVJCv3tqPm+Td4C38FPhIvWNZj6ddj9Qbk/VgPXIzheCPWf7wuRPdTv2h+g18oK/SfsFT6Cv4vOx1zXpQD4IP8FHTb4XPH5bPreEuO5ijoyNrWrd6qK+srFizvvW42NNdTXNN717EQ17n1evf2rgP51iPL9NZAsRVh2fwETzjwAf4SGeNgA/wkfNUhD/gj1TXwh/wB/wRZuKAj86TGv4oz4BDX6GvwAf4qOnvoa/QV8usr8xSRg3zjY0NEw0XFxfu4ODAnZycWBN7b2/PHR4eFvfLe13N8+vr66bj/OeOx2M77/39fdX5ctcTv57DfbAeilfiKuAZfIR8Rr7q8jn4AB85vgcf4CPVe/AH/JHWA/AH/AF/lOtl8AE+wAf4qOmnoa+et75SY1JvBNAXre8L09+d75sL56Nff/3VLGXUMNcmoaBfmeJXTvT0eWm/CnS9ovTmzZum4/QKqj7348eP9hqiPOD9Kw595/PHxdfj7Wi4j274LOtBXIGPfD4DH+TdlF/EH9pevXr1zXgwtpSBB2f1BXwe9Bf5inyVy1fevuOpdbt/UCany4fqCHAOztM6k3ow6FbwAT7AR7D+eM48SP9qPLWFpg9HH47+ble3zPXhfv/99y+aWOyFs4a96In12r+ruSthrUZ9y3H+PBIVujCBVFvr+fXvNUlWG/fhrNnOenyx5E9cdXgCHyGfgY8uv6f4UGNPW23eJ65meZK4yscVfI4uUV4BH+AjrQ8W0SUpTy0aV/5z0Indm8XwOXyeq9+fCh8PhXP6DPP9nEXybqrjFs27rAfrEeeZl4rzoXoZfKB3c3rXLGU+ffpkzWptV1dXbn9/3/1/e2fD27bRbGGq1/V1FCPxd10H8D8qWrTo/wcM2FBku25jX1uxm+biDLHa1WqX3FX8ISsPgRdvUoYiqZ0z58yIPDMej+3vmsB+dnaW3a9Bqfpg/cJXc5z73Hiae9/5UvvDKbbcB+uheCWuPJ7Bh89n5Ks2n4MP8JHie/ABPmK9B3/AH3E9AH/AH/BHvl4GH+ADfICPkn4a+gp9hb7K95tXCR82NNVNohVByGdGRXc8xVg+76n9TLF93Cm2+o5Zj8H0O9CrKTXxqH//5cuXRpYNimX3Wqb777k4Tu1nmjvT3JnmzjT3p5jmHlrKkK++2OB1py/Iu+Rd8u7ryLsOq+grdLuzOVIuR7dTf8Dn8/0U+iXU59L76F36iV39VvpXq1kPWsN9b2+vOT09tYI3njqvp9fjqbHhVHamCjNVeJWnCgsT4KMxmyKJhNr8EOYP/QAimx9tfXkltf/i4sKOZT1YD+JqnpfBx7nNgdFGvkKXoEv2s7oefIAP8AE+Sup+dHvbF0Ffoa+cvqT+oP6gL5rnT/CRxodZyqiRtrXlp4ozjZdpvM7L2U0brokPpm4v99RtiYaS6elhHpBnmZKohkDU5AemVaenVaspSN5tbMgzcXViM1CES/Lulb2ZBD5G9h2AjzZ/gg+vz8EH+mo0avMD+gp9pfyYq8/gD/gjjg/4A/6AP2b5M8yfMT7Es3rjsqbvQd4l76byrjXc9frfX3/9ZQJuZ2fH/NvDoJNvem6/nlhVw0QBW3Ocmk36XPfqsJ64cUV21/ncceH1hK/ncx+sB3E1i1fw4fMZ+Yq8G/ML+AAfsd5Bl3g9CD7AB/jwPwLCH9SDfXUm/AF/pPop1B/UH/AH/AF/lPebV6n+MEsZielSz/bYq1GG9mqCq1Efe1OVeG8r8HR+DW5dxENe1xP6YXEfrIfijrjyHrDgw3vmka/aGR3gYzF8xN7rDlvEFXEV6x3yLnk3nhlD3k3n3Vxe7ZspRd4l735L3oXP87Mi0Imzs+zgc/gcPk97r9OHa07J5KsAACAASURBVGcTUA/Oxgd6d1bvWsNdT6invKlKPN0liBVkmsre5WmU82y+v7+3hrsL1EU8osNr5z5YD80YIK5aL+MY2+ADfIAP70UJPma95uDzDw34AB85ngQf4COcYQV/wB+pupf6g/ojNasK/oA/4I9ZfRnmT/ABPlYZH2Yps7u7a3Yw2vS6y9nZWbO5uWlN+Ovr6+bo6Ci7X8JColODDGuOc5+rQSQ6jzyPSs6Xup7Ly0u7du5jYI1m1uPIBtwQVy2ewYfPZ+Cjzefgw/Md+AAfsd4BH+AjpYfhj+fjD33/Nzc31B8d9Rd11Hx9Cp/D5/B5vn8DPsDH94CPEv1AP5H+bld/+7H1lTXcZedS6tkee6jrtTM1eFWI1Hqvq8mvJ+PVGNXrGIt4yOt69FqpNu6jMbsI1oO4CvEMPrxnHPhoZ3SQd70nL/gAH7GnIvgAH6mZQvAH/BHPqoI/4A/4I+/JCz7AB/gAHyX9PfQV+mqV9ZVZymxsbMz4oGuA6dXVlTXCt7e3m4eHh+x+d6w8nGqOc58rDz01B+/u7orOl7qeyWRiDXfuYzD9DlgP4srNRAAfPp+Rr9p8Tt71XnM5fOitKxUKT8GDmjWiDR6c1xfkK/JVrD/JV/35qkZ/PxYPOqz21QnoduqorjoSHoQH6TPk+yzgA3yAD/ARzhDo03vUUctXR1nDXXYwp6en1gCQh7peOdJT0trUbJBVS26/Fl1BUHtc+LlquGt6d8n5Utej17+1cR+N/ejBerTxSly1eAYfPp+BD5/PwQf4iHkbfICPWO/BH/BHqh6AP+AP+CNfL4MP8AE+wEdfPw19hb5CX+X7zauED7OUUYN2a2vLmtZ68u74+LgZjUb298PDw+bk5CS7X08BKlhub2+rjnOfOxwOramvX+9Kzpe6ntA+g/tgPRSvxJXHM/jw+Yx81eZz8AE+UnwPPsBHrPfgD/gjrgfgD/gD/sjXy+ADfIAP8FHST0Nfoa/QV/l+8yrhwxrush+JPbZKPd31ZLoa7mp0x947JZ7uSkhquOvJupRn5s5O62nUdT163Vgb99HYmwKsx4H9cERcNY3wAz58/gAf/2ezNsCH91QEH+Aj1hfgA3ykPEfhD/gjrmvgD/gD/vAzP8CH92yP+xfwB/wBPsBHX7+V+mM16w+zlJFYcAusprUGmIZeQRpkkNuvgSj6tyKWmuN0Tn2uAk9/1sBT5znddT53XHg94bVyH6wHcTWLV/Dh8xn5irwb80sNPsLmSsh3xBVx9S1xhb5qfyDP6U90ov9+avLVa4mrXF5N6X3y7nx9Bj4Wwwd8Tt7t4h36DOn4QO+id9G79EX7+q3oklldYg13PREt0tGmP+/v7894tp+fn2f3SxCrAFhfX686Tt5m+tz7+3truLsiwv33musJ/23NceF9ch+sRxznxJXPA+ADfIAPeLCPX+Fzr6PgD/gjnH2E3p3Nn+ADfIAPPzsNfYW+Ql+V96HgD/gD/oA/3OzR19CnHvzyyy9f5TcXTrTVDZT+XQ36//77b2r5UnrcxsbG1ELmhx9+mDb03X+v+Rz5x2vjPtofTFiPxn7AIa6aRngCHz6fgQ/ybsxv4AN8xHoD/piYpoI/2nzp4gP+gD/gj3x9CD7AB/gAH339G/QV+srpS/TVfL4AH6uJj8Gff/75Vf7rV1dXVljIGkaiqfTv8l5Xw0LDTGuOc+f5+PGjNcrlbbbI+fU5em1Bv3ZyH4154bMeXxviyuMZfPh8Bj7a/L7K+BCPhB55fbyUw0fN5xBXqx9XfXEU7yfvPm3ercEnOnFW15OvyFevJV+Bc+pz8tXq5ytwDs7B+erj/LuuozQ0Vb+mqFmtprUa7cfHfmqwGuknJyfZ/T/++KM1cNTkrTnOfW7XNPfS64mn2JYeF14v99GuM+vhcUBc1U0P78IdcUVcpeKDvEvejfUFeZe8OxqN7AEMdOK8/gYf4AN8zOaHsH4FH+ADfICPUD/k+lvUH9QfffWH4khe5TX9TeKKuErF1UANdw2q6puam9vP1G2mbjN1m6nbffmDqdurOXVbYgT+aOwNLf3wLGE/Ho+nFmv6MTvOj6n94AN8uDf8wvggrtBX6Cv0FfrK80Mfv4aDUPu+N+ra7u8VXYIuQZe0zg/o9u66hrzrvx90O7o9pdttaKqeCOibNpvbz7RqplUzrZpp1X35g2nVs9OqJWLJu03z2vgjFJVfvnyZDvt+bfcBn3s8husoTIrPyFfkK72Rox8TwbnPc0+Fj1xedecjX5GvUnhMxYd+5C7VV/A5cfUUcUW+Iq6Iq3n9BJ+39QX9ku78EH4/NXy+7HnXGu56Mq9vOnZuvwCkL2R9fb3Z3/dTkz98+NB0TY11++/v763x5L7U0uNyE6q5D9ZDcUdc+WnvTHNnmjvT3JnmriZEH7/C561ugT/gD+FF2jjUteADfMR1DfoKfYW+Ql+hr/r1JX2f+b4Y/AF/wB/fB3+Ypczu7m4Te56VerqrMFVRsre31+n1nvOCv7i4sGa/PI8W8V7X515eXtoTDdzHwBoFrMdxQ1x5Dz/w4WdQgI92Rgf4AB8pj0vwAT5ir0r4A/6I64Hn4I8S71TyFfmKfJWfuQY+wAf4AB99/b3n4POS/t5j5qsS/ZCaWYneRe8+ld61hvvnz5+rPWed951ej1CDV0Ap8aqNPfP0ZLyAqNd2U15hJR64ep1fG/fR2OsqrMfY3rggrhp7cwR8eA9Y8NF6EYKPt8YZ4GPWYx58gI/YqxT+gD/Q7Xmva/ABPsAH+CjpX6Cv0Ffoq7wXPPgAH6uMD7OU2djYmFq66JXZtbW1Rr/8qGG5vb3dPDw8ZPe7Y+V1WXOc+1x56Omcd3d3RedLXc9kMrHmCfcxmH4HrAdx5XwswYfPZ+SrNp+Td73Paw4feutKhfRT8KC85rTBg/P6gnxFvor1J/mqP1/V6O/H4kGH1b46Ad1OHdVVR8KD8CB9hnyfBXyAD/ABPkI/8z69Rx21fHWUNdxlBxN6COmVCj0lrU3NBlm15PZr0RUE8uaqOS78XDXcNdW35Hyp69HrMNq4j8Z+9GA92nglrlpPPfDh8xn48PkcfICPmLfBB/iI9R78AX+k6gH4A/6AP/J1L/gAH+ADfPT1xdBX6Cv0Vb7fvEr4MEsZNWhLPdtjzyM9Bahgub29NW/gPq+oeP9wOLSmvn69S3nKpjyWTk68x5D2S9g4ewDug/VQfBBXW9OZCODD5yXy1aHN2gAf4CPlqQg+wAf6alZfhroWfIAP8AE++upM9BX6Cn01MKeEVF8IfICP14KPRbzg0YnoxJROtIa77EeUGN0r7nqNPvQl3NnZye7Xk+lquEuA1Bwnv3V9rl6tF/D0ZJ3zYe86nzsuvF69bqyN+2jsTQHWg7gCH+l8Bj7IuzG/wB+e78EH+AAf6N2+egDd7vUF/AF/xPUy+AAfqX4K+gp9hb5CX6GvyvvNq6SvzFJGT8A6gaCmtQaYhl5BGmSQ2y+hoX+rJFJznM6pz1WTXn9WI915Tnedzx0XXk94rdwH60FczeIVfPh8Rr4i78b8UoOPkPxDviOuiKtviSv0VfvgRU5/ohP991OTr15LXOXyakrvk3fn6zPwsRg+4HPybhfv0GdIxwd6F72L3qUv2tdvRZfM6hJruOuJaJGONv15f39/xrP9/Pw8u1+CWAXA+vp61XHyNtPn3t/fW8PdFRHuv9dcT/hva44L75P7YD3iOCeufB4AH+ADfMCDffwKn3sdBX/AH+HsI/TubP4EH+ADfJxa3Z2qe8EH+AAf4COXH+hfzfcpqT+oP1J97GXpX5mljIZk1XqvOw87Bbia9BpYGnvWlHi6yxBfwkKeRylPp8PD1guoyzNPQym0cR8D+wGD9Ti2QaHEVdMIP+DD5w/w0Xoqgo/RdGYI+AAfsb4AH+AjNVMI/ng+/ijxTmU9nm89Suoz1oP1iOt+9BX6Cn3lPdvBR37W42PyR4l+SM0YIF+Rr54qX1nDXXYu4WDFg4MDs3rRJquY8Xic3a/XStTgFVBqjnOfqyfjJeT0mmjJ+VLXE/rncR+sh+KVuPKDhMGHz2fkqzafgw/w4QaNh7wNPsBHrPfgD/gjrgfgD/gD/mjszexU3Qs+wAf4AB+5/BD2qdBX6Cv0Vb7fvEr4GPzxxx9f9XT5ZDKxhvebN2+sue6eeP/555+b6+vr7P7NzU3b/+7du6rj3Od++vTJjr25ubHz950vtV/Nfm3cR9OwHm28Elcez+DD5zPwkcaH3oRQDq7hgWWNK93H3d1dMZ8t63108S48iC4piY9V5kFwjm5flM8d360yPkryQ8j3y8qD4BycL4pz+gzzOgmcUw/G/T14sK5f0lcvk6/ow8X6S3l38Ntvv30dDofTJ8z1hJs298R539/V2NCv+fpfzXHuc3UR+p8a/dr6zpfaf3t7a8dyH401mliPxmKKuGrxBD58PgMfbX4HH+0bVeBjlu/BB/iIdRz8AX/E9QD8AX+k6jX4A/6AP/L9E/ABPsAH+Ojrr6KvVlNf2dBUPdGupwhk7aI/q/n98eNHa0j89NNP1sTN7Q+nVdcc5z43nmLbd77UfjdwlftoGtajjVfiyuMZfPh8Bj7AR8xnNfgI/23Id8QVcfUtcYW+6taf8Plq83kur/bVA+Rd8u635F34nLxb2veo0YnwOXFFXNFPlAU2fdG3jWzLS/rJq97ftYa7PNi1KUHoz+H0Yzc9Pbe/b/pr3+fmpgr3HRfuD6e51xxXM+W573O5Dx8/rIefng0+PjTn5+f2RLX7LoSlvryS2k9cEVenp6fGVS4+njuuwhiEP3w8vtR6oEu6ddtz44P1YD1K64gwl+byap9OQF+hr6QvF60H4XPy1SL5qq8fAA8SV8QV/US5TdT0U6mjWj2zKJ8ve961oam7u7tTz3Z5E52c+Cm1qSm+4f7HnCqsBNV3PqYKXzXxlGvWYz5eLy4u7Ack+foTV5eWwMD5wBr/+lFxb2+vKs+5vENcjSyWxBNMc2ea+1NNcydfbRlvka9avUPe/f7yrnLA33//jd49Pu6tz8DH94ePkroG/oA/4n4Buh3d/j3o9hL9QD+RfmJXP/Wx+z7WcNfj/uHU5NTU9dx+eeCqgSVirzlOr1qMx2PzG5dwcJ5G7r/XXM8qTbFVkmA9Wm9l4gp8MM39rTW442n34AP+EH+CD/CRyg/oqzY/gA/wAT7m9UOqzqKOOjArSm3oK/QV/DHLn2F/B3yAD/ABPvr6tPR3vf6WvjJLmY2NDWvoaNMXqGSqJ0vUCH///r01w3P73bH6dzXHuc/9559/7Jzy4Ss5X+p6JpN2wjD3MZh+B6wHceXwDD58PiNftfmcvOv57iXwoe9fGzw4ry9eYj1ivQM+XhYfrEdefy8TPty19NUJ6HbqqK46Eh6EB+kz5Pss4AN8gA/wUdOnXSadSH/3baP1sIa77BVib9w+T0W3f21tzZrxznuo9LjQK14N93///dcaEDkP+a7P1euU2riPpmE9/AwC4qr1ugYf5/ZjnjbwAT5iTz3wAT5S+gL+gD9iXQt/wB/wx/zsDvRVqy+pa+dnwKGv0Ffoq/n8oHxBfd56dsMf8Eeu/7tK/GGWMmqYx55Oo5H3xOvydJdHtkTG7e1tp9dizpt9OBwa2B4eHqbewDUe8vrc8LUG7oP1UPwQV60HMPiY9SgjX7UzOsAH+Eh5wIIP8BHrL/SV99AGH+ADfORnfIEP8AE+wEdfH4b6g/rjtdQfi3jBw4PwYIoHreGu15XUmNO2vb1tPnahn5/8/nL79WS6Gu5KsDXHyadcn+teHdaTQ86/vOt87rjwesLX87kP1oO4msUr+PD5jHxF3o35BXyAj1jvoEu8HgQf4AN8/G+2PgEf4AN8gI+S/gX1B/UH9Qf9xL4+JfXHatYfZimjJ5hKPdtjL0YJDT0hryRS4tEXexCpSa/zq5G+iIe8rif8TO6D9dAsAeLKe8CCD+/9Rr5qZ3SAj8XwEXuvO2wRV8RVPMOGvEveRe/mZ0OF300ur/bNlCLvkne/Je/C592z2tCJi+nEXB+CfEW++pZ8RVyRr0pnXVJ/LF/9YQ13PaG+iPe6a5BrYTWNNvY2DL2Zct7s9/f31nB3wbGIF3x47dxH+12yHsSV88QCH95z1OUa8AE+wMf8zBTw0XpKoku8pyT8AX/EM57AB/hIea7CH/BHXPfDH/AH/HFqD5Sm+lvgA3yAj+8DH2Yps7u724zHY0sIBwcHzdnZWbO5uWlN+Ovr6+bo6Ci7X8JbzXQNLK05zn2uDPF1HnkelZwvdT2Xl5d27dzHwBoFrMeRDQolrlo8gw+fz8BHm8/Bh+c78AE+Yr0DPsBHSg/DH8/HH/r+b25uqD866i/qqPn6FD6Hz+HzfP8GfICP7wEfJfqBfiL93a7+9mPrK2u4y84lHIylJp1eJdMmqxg143P79XqMGrwqRGqOc5+rJ03VGJUdTcn5UtcT+udxH6yH4pW4emt40tsO4MPnM/JVm8/BB/hw+SHkbfABPmK9B3/AH3E9AH/AH/BHq69TdS/4AB/gA3zk8kPYp0Jfoa/QV/l+8yrhwyxlNjY2ZnzQY4+pLm92d6y81GuOc17w8tBT8rm7u7PGe+wR3+fhqP2TycSai9zHYPodsB7ElbN8Ah//M51RQb56bz9ukne9t/BL4CP2jmU9XnY9Ym9M1oP1SM0Ugj9m+cPlTnQ79YfqN/CBvor7BS+hr+DzvNc160E9CD7AR0m/FT5/XD63hrvsYGIPoVIv9LW1NWvWL+K97jzf1TTX9O6UJ2DJ5+r1b23cR9OwHl+nswSIq9YzDnycT2dUgA/wEc8aAR/gI6V34A/4I9af8Af8AX94z13w0Xq2wx9+pgH4AB8l/ST0FfoK/oA/+mZ9rlJ9bpYyaphvbW2ZaLi6umqOj4+b0WhkTezDw8Pm5OQku1/e67KUub29rTrOfe5wOLTzPjw8FJ0vdT3h6zncB+uheCWuPJ7Bh89n5Ks2n4MP8JHie/ABPmK9B3/AH3E9AH/AH/BHvl4GH+ADfICPkn4a+mq59ZUak3ojgL5oeV+Y/u5831w4H/z6669mKaOGuTYJBT3FE75yoqfPc/tVoOsVpXfv3lUdp1dQ9bmfPn2y1xDlAe9eceg6nzsuvB5nR8N9tMNnWQ/iCnyk8xn4IO/G/CL+0PbmzZtn48HQUgYenNUX8LnXX+Qr8lUqXzn7jpfW7e5BmZQu76sjwDk4j+tM6kGvW8EH+AAf3vpjmXmQ/tVwagtNH44+HP3dtm6Z68P9/vvvXzWx2AlnDXvRE+ulf1dzV8Jajfqa49x5JCp0YQKpttrz699rkqw27qOxZjvr8dWSP3HV4gl8+HwGPtr8HuNDjT1tpXmfuJrlSeIqHVfwObpEeQV8gI+4PlhEl8Q8tWhcuc9BJ7ZvFsPn8Hmqfn8pfDwWzukzzPdzFsm7sY5bNO+yHqxHmGdeK8776mXwgd5N6V2zlPn8+bM1q7Xd3Nw0R0dHzXg8tr9rAvvZ2Vl2vwal6oP1C1/Nce5z42nufedL7Q+n2HIfrIfilbjyeAYfPp+Rr9p8Dj7AR4rvwQf4iPUe/AF/xPUA/AF/wB/5ehl8gA/wAT5K+mnoK/QV+irfb14lfNjQVDeJVgQhnxkV3fEUY/m8p/YzxfZxp9jqO2Y9BtPvQK+m1MSj/v2XL18aWTYolt1rme6/5+I4tZ9p7kxzZ5o709yfYpp7aClDvvpig9edviDvknfJu68j7zqsoq/Q7c7mSLkc3U79AZ/P91Pol1CfS++jd+kndvVb6V+tZj1oDfe9vb0mniqtYJCA0tPr8dRxTZV1+2VfouThpg2XHhd+rhqjsjIoOV/qerqm2JZeD/fh15n16J8eTlyV5QdwPp8/yVfwh7hOG/gAH+ir/SL9iS5ZPl0i2744j+nvJfUAPAgPpnQ0OF8+nFOff2jIV+Qr8lW+7gcf4AN8dOPDLGXUMN/a8lPFmcbLNF7n5eymDdfEB1O3l3vqtpp9JdPTwzwgzzI1BzUEoiY/MK06Pa1axETebWzIM3F1YjNQhEvy7pW9mQQ+RvYdgI82f4IPr8/BB/pqNGrzA/oKfaX8mKvP4A/4I44P+AP+gD9m+TPMnzE+xLN647Km70HeJe+m8q413PX6319//WUCbmdnx/zbw6CTb3puv55wUcNEAVtznJpN+lz36rCeMHdFdtf53HHh9YSv53MfrAdxNYtX8OHzGfmKvBvzC/gAH7HeQZd4PQg+wAf48D8Cwh/Ug311JvwBf6T6KdQf1B/wB/wBf5T3m1ep/jBLGYnpUs/22KtRhvZqgqtRH3tTlXhvK/B0fg1u7fI06vLeDvdxH6yH4o648h6w4MN75pGv2hkd4GMxfMTe6w5bxBVxFesd8i55N9at5N103s3l1b6ZUuRd8u635F34PD8rAp04O8sOPofP4fO097r6guADfICPbnxYw11PqKc8ZUMvxtx+CWJ9yZrKnvIi7fvc+/t7a7i7hXLej33Hhftzf665Hu7jQyNvftbDeysTV95bF3yAD+UH8u6897r7oRgehD/AB/jo8jRHX6GvUvGBvkJfoa9m9WVYv4MP8AE+wEdfXxB9hb5aZn1lljK7u7tmB6NNr7ucnZ01m5ub9vebm5vm6Ogou1+es2rYa/BqzXHuczVoQSCS51HJ+VLXc3l5acdyH415ALMeRzbghrhq8Qw+fD4DH20+Bx+e78AH+Ij1DvgAHyk9DH/AH3E9BH/AH/BHvl8APsAH+AAfJf099BX6apX1lTXcZedS6tkee6jrtTM1ePXLUq33upr8ejJejVHZ0SziIa/r0Wul2riPxuwiWA/iKsQz+PCeceCjndFB3vWevOADfMSeiuADfKRmCsEf8Ec8qwr+gD/gj7wnL/gAH+ADfJT099BX6KtV1ldmKbOxsTHjv6QBpldXV9YI397etqemQ2+ecL87Vh5ONce5z5WHnpqDd3d3RedLXc9kMrGGO/cxmH4HrAdx5awuwIfPZ+SrNp+Td73XWg4feutKhcJT8KBmjWiDB+f1BfmKfBXrT/JVf76q0d+PxYMOq311ArqdOqqrjoQH4UH6DPk+C/gAH+ADfOT6sOir16GvrOEuO5jT01NrAMhDXa+A6SlpbWo2yKolt18iX0FQe1z4uWq4a3p3yflS16PXv7VxH4396MF6tPFKXLV4Bh8+n4EPn8/BB/iIeRt8gI9Y78Ef8EeqHoA/4A/4I18vgw/wAT7AR18/DX2FvkJf5fvNq4QPs5RRg3Zra8ua63qy6Pj4uBmNRtbEPjw8bE5OTrL79RSgguX29rbqOPe5w+HQzqtf70rOl7qe0D6D+2A9FK/Elccz+PD5jHzV5nPwAT5SfA8+wEes9+AP+COuB+AP+AP+yNfL4AN8gA/wUdJPQ18tt75SX1JvJNMXLe8L09+d75sL59Zwl/1I7LFV6umuJ9PVcFejO/beKfF0VyCLmPRkXcozc2en9TTquh69bqyN+2jsTQHW48ASJHHVNMIP+PD5A3z8n83aAB/eUxF8gI9YX4AP8JHyHIU/4I+4roE/4A/4w8/8AB/esz3uX8Af8Af4AB99/Vbqj9WsP8xSRmLBLbCa1hpgGnoFaZBBbr8GoujfilhqjtM59bkKPP1ZA0+d53TX+dxx4fWE18p9sB7E1SxewYfPZ+Qr8m7MLzX4CJsrId8RV8TVt8QV+qr9gTynP9GJ/vupyVevJa5yeTWl98m78/UZ+FgMH/A5ebeLd+gzpOMDvYveRe/SF+3rt6JLZnWJNdz1RLRIR5v+vL+/P+PZfn5+nt0vQawCYH19veo4eZvpc+/v763h7ooI999rrif8tzXHhffJfbAecZwTVz4PgA/wAT7gwT5+hc+9joI/4I9w9hF6dzZ/gg/wAT787DT0FfoKfVXeh4I/4A/4A/5ws0dfQ5968Msvv3yV39xkMrGG+8bGhv1/6d/VoP/vv/+mli+lx7nzqNH+ww8/TBv6tefXv5d/vDbuo/3BhPVo7Acc4qrFM/jw+Qx8tPkdfHi+Ax/gI9Yt4AN8pPQw/AF/xPUR/AF/wB/5/gH4AB/gA3yU9BfRV+irVdZXgz///POr/Nc13MF5ViroS/8u73URqkzya46TBY3+/cePH61RLm+zRc6vz9FrC/q1k/tozAuf9SCuQvyCD5/PwMfq513xSOiR18dLOXzUfA5xtfpx1RdH8X7y7tPm3Rp8ohNbfDpdQL4iX72WfAXOqc/JV6ufr8A5OAfnq4/z77qO0tBUPc2lZnVqqrabNpvb/+OPP5qQV5O3ZoovU2zTU2zd06esB3F1cnJiP0YJlw8PD/aDVB8eU/uZgr7cU9DJu/AHOPf6g3xFvhqNRjN8Bz7AR4onqT/aOgJ8gA/wMbAfFFN9CPABPsAH+Mjlh7DfFtcfEqLyKq/pb6JL0CXSZXFcDdRw16Cqvqm5uf1M3WbqNlO3mbrdlz+Yur2aU7clRuCPxt7Q0g/PItjxeDy1WNOP2XF+TO0HH+DDveEXxgdxhb5CX6Gv0FeeH/r4NRyE2ve9Udd2f6/oEnQJuqRp9IYcur27riHv+u8H3Y5uT+l2G5qqX3T6ps3m9jOtmmnVTKtmWnVf/mBa9ey0aolY8m7TvDb+CEXlly9fpsO+X9t9wOcej+E6CpPiM/IV+UpPxOnHRHDu89xT4SOXV935yFfkqxQeU/GhH7lL9RV8Tlw9RVyRr4gr4mpeP8HnbX1Bv6Q7P4TfTw2fL3vetYa7nszrm46d2y8A6QtZX19v9vf91OQPHz40XVNj3f77+3trPLkvtfS43IRq7oP1UNwRV37aO9PcmebONHemuasJ0cev8HmrW+AP+EN4kTYOdS34AB9xXYO+Ql+hr9BX6Kt+fUnfZ74vBn/AH/DH98EfZimzu7vbxJ6ZpR7iKkxVlOzt7ZmXYOlxB36W+AAAIABJREFUzkvp4uLCmv3yPFrEy1ie1ZeXl/ZEA/cxsEYB63HcEFfeAxd8+LwEPlqPS/ABPlIzIcAH+Ii9KuEP+CPW9c/BHyXeqeQr8hX5ys/8iGc4gQ/wAT7AR19/7zn4vKS/95j5qkQ/pDzd0bvo3afSu9Zw//z5c7XnrPO+0+sRavAKKCVetbFnnp6MFxD12m7KK6zEA1ev82vjPhp7XYX1GNsbF8RVY2+OgA/vAQs+Wi9C8PHWOAN8zHrMgw/wEXuVwh/wB7o973UNPsAH+AAfJf0L9BX6Cn2V94IHH+BjlfFhljIbGxtTSxe9Mru2tmbTvtWw3N7ebh4eHrL73bHyuqw5zn2uPPR0zru7u6Lzpa5nMplY84T7GEy/A9aDuHI+luDD5zPyVZvPybve5zWHD711pUL6KXhQXnPa4MF5fUG+Il/F+pN81Z+vavT3Y/Ggw2pfnYBup47qqiPhQXiQPkO+zwI+wAf4AB+hn3mf3qOOWr46yhrusoMJPYT0SoWektamZoOsWnL7tegKAnlz1RwXfq4a7prqW3K+1PXodRht3EdjP3qwHm28Eletpx748PkMfPh8Dj7AR8zb4AN8xHoP/oA/UvUA/AF/wB/5uhd8gA/wAT76+mLoK/QV+irfb14lfJiljBq0td7rzhNKTwEqWG5vb80buM8rKt4/HA6tqa9f71KesimPpdgrXsLG2QNwH6yH4oO42prORAAfPi+Rrw5t1gb4AB8pT0XwAT7QV/lZROADfIAP8NFXZ6Kv0Ffoq4E5JaT6QuADfLwWfCziBY9ORCemdKI13GU/osToXnHXa/ShL+HOzk52v55MV8NdAqTmOPmt63P1ar2ApyfrnA971/ncceH16nVjbdxHY28KsB7EFfhI5zPwQd6N+QX+8HwPPsAH+EDv9tUD6HavL+AP+COul8EH+Ej1U9BX6Cv0FfoKfVXeb14lfWWWMnoC1gkENa01wDT0CtIgg9x+CQ39WyWRmuN0Tn2umvT6sxrpznO663zuuPB6wmvlPlgP4moWr+DD5zPyFXk35pcafITkH/IdcUVcfUtcoa/aBy9y+hOd6L+fmnz1WuIql1dTep+8O1+fgY/F8AGfk3e7eIc+Qzo+0LvoXfQufdG+fiu6ZFaXWMNdT0SLdLTpz/v7+zOe7efn59n9EsQqANbX16uOk7eZPvf+/t4a7q6IcP+95nrCf1tzXHif3AfrEcc5ceXzAPgAH+ADHuzjV/jc6yj4A/4IZx+hd2fzJ/gAH+Dj1OruVN0LPsAH+AAfufxA/2q+T0n9Qf2R6mMvS//KLGU0JKvWe9152CnA1aTXwNLYs6bE012G+BIW8jxKeTodHrZeQF2eeRpKoY37GNgPGKzHsQ0KJa6aRvgBHz5/gI/WUxF8jKYzQ8AH+Ij1BfgAH6mZQvDH8/FHiXcq6/F861FSn7EerEdc96Ov0FfoK+/ZDj7ysx4fkz9K9ENqxgD5inz1VPnKGu6ycwkHKx4cHJjVizZZxYzH4+x+vVaiBq+AUnOc+1w9GS8hp9dES86Xup7QP4/7YD0Ur8SVHyQMPnw+I1+1+Rx8gA83aDzkbfABPmK9B3/AH3E9AH/AH/BHY29mp+pe8AE+wAf4yOWHsE+FvkJfoa/y/eZVwsfgjz/++KqnyyeTiTW837x5Y81198T7zz//3FxfX2f3b25u2v53795VHec+99OnT3bszc2Nnb/vfKn9avZr4z6ahvVo45W48ngGHz6fgY80PvQmhHJwDQ8sa1zpPu7u7or5bFnvo4t34UF0SUl8rDIPgnN0+6J87vhulfFRkh9Cvl9WHgTn4HxRnNNnmNdJ4Jx6MO7vwYN1/ZK+epl8RR8u1l/Ku4Pffvvt63A4nD5hrifctLknzvv+rsaGfs3X/2qOc5+ri9D/1OjX1ne+1P7b21s7lvtorNHEejQWU8RViyfw4fMZ+GjzO/ho36gCH7N8Dz7AR6zj4A/4I64H4A/4I1WvwR/wB/yR75+AD/ABPsBHX38VfbWa+sqGpuqJdj1FIGsX/VnN748fP1pD4qeffrImbm5/OK265jj3ufEU277zpfa7gavcR9OwHm28Elcez+DD5zPwAT5iPqvBR/hvQ74jroirb4kr9FW3/oTPV5vPc3m1rx4g75J3vyXvwufk3dK+R41OhM+JK+KKfqIssOmLvm1kW17ST171/q413OXBHk5ED6cfp6anMx2Z6cjx9PQwfnJ/Jq7m46YPd0zdZuq2iEo5mrxL3iXvnppwQ5d8aM7Pz3t1G/wBf8Af8/zp8gf4AB/gA3zE+jqlL6hrvf7Ww436AUJv09fU9eTded1GXBFXcV2HLllNXWJDU3d3d6ee7fImOjnxU2pTU3zD/Y85VViJp+98TBW+auIp16zHfLxeXFxYM0K+/sTVpTWpwPnArFQkrvf29qrynMs7xNXIYkk8wTR3prk/1TR38tWW8Rb5qtU75N3vL+8qB/z999/o3ePj3voMfHx/+Cipa+AP+CPuF6Db0e3fg24v0Q/0E+kndvVTH7vvYw13Pe4fTk1OTV3P7ZcHrhpYIvaa4/SqxXg8tl9IJRycp5H77zXXs0pTbJUkWI/WW5m4Ah9Mc39rDe542j34gD/En+ADfKTyA/qqzQ/gA3yAj3n9kKqzqKMOzIrSedNTf1B/wB/wB/wBf8T1d1+fkvqc+jxVf5ilzMbGhjV0tIlgFCx6skSN8Pfv31szPLffHat/V3Oc+9x//vnHzikfvpLzpa5nMmknDHMfg+l3wHoQVw7P4MPnM/JVm8/Ju57vXgIf+v61wYPz+uIl1iPWO+DjZfHBeuT19zLhw11LX52AbqeO6qoj4UF4kD5Dvs8CPsAH+AAfNX3aZdKJ9HffNloPa7jLXqHLG7bLo2ttbc2a8X2eql371XD/999/rQFR4qUWX49ep9TGfTQN6+G9romr1usYfHivY/ABPuCP/CwJ8AE+wAf4KKkH0Ffoq7iugz/gD/gD/oA/ymYNUZ/PetrDH/DHKvOHWcqoYR57Oo1G3hOvy9NdHtlqkt/e3nZ6Lea82YfDoT3Z/vDwMPUGrvGQ1+eGr31xH6yH4oe4aj2AwcesRxn5qp3RAT7AR8oDFnyAj1h/oa+8hzb4AB/gIz/jC3yAD/ABPvr6MNQf1B+vpf5YxAseHoQHUzxoDXe9rqTGnLbt7W3zsQv9/ORXlNuvJ9PVcFeCrTlOPuX6XPfqsH7Zcv7lXedzx4XXE76ez32wHsTVLF7Bh89n5Cvybswv4AN8xHoHXeL1IPgAH+Djf7P1CfgAH+ADfJT0L6g/qD+oP+gn9vUpqT9Ws/4wSxk9wVTq2R57MUpo6Al5JZESj77Yg0hNep1fjfRFPOR1PeFnch+sh2YJEFfeAxZ8eO838lU7owN8LIaP2HvdYYu4Iq7iGTbkXfIuejc/Gyr8bnJ5tW+mFHmXvPsteRc+757Vhk5cTCfm+hDkK/LVt+Qr4op8VTrrkvpj+eoPa7jrCXUt4iIe6hLEWtj19fWmy+s9581+f39vDXcXHIt4wYfXzn2wHufn5w1x5fEMPrynIvmq9cwDH+AjxffgA3woP+Q4A32FvoI/wEcuD8Af8Af8MZsfwr4I+AAf4AN89Olo6vPVrM/NUmZ3d7cZj8fWcD84OGjOzs6azc1NK7qur6+bo6Oj7H4FhprpGlhac5z7XA101HnkeVRyvtT1XF5e2rVzHwNrpLEeRzYolLhq8Qw+fD4DH20+Bx+e78AH+Ij1DvgAHyk9DH88H3/o+7+5uaH+6Ki/qKPm61P4HD6Hz/P9G/ABPr4HfJToB/qJ9He7+tuPra+s4S47l3Awlpp0epVMm6xi1IzP7dfrMWrwqhCpOc59rp6MV2NUdjQl50tdT+ifx32wHopX4uqt4UlvjoAPn8/IV20+Bx/gw+WHkLfBB/iI9R78AX/E9QD8AX/AH62+TtW94AN8gA/wkcsPYZ8KfYW+Ql/l+82rhA+zlNnY2JjxQY89prq82d2x8lKvOc55wctDT8nn7u7OGu+xR3yfh6P2TyYTay5yH4Ppd8B6EFduJgL4+J/pjAry1Xv7cZO8672FXwIfsXcs6/Gy6xF7Y7IerEdqphD8McsfLnei26k/VL+BD/RV3C94CX0Fn+e9rlkP6kHwAT5K+q3w+ePyuTXcZQdzenpqTetaD/W1tTVr1tceF3q6q2mu6d2LeMjrvHr9Wxv30TSsx9fpLAHiqsUz+PCeceADfMSzRsAH+Eh5KsIf8Eesa+EP+AP+8DNxwEfrSQ1/5GfAoa/QV+ADfJT099BX6KtV1ldmKaOG+dbWlomGq6ur5vj4uBmNRtbEPjw8bE5OTrL75b2u5vnt7W3Vce5zh8Ohnffh4aHofKnrCV/P4T5YD8UrceXxDD58PiNftfkcfICPFN+DD/AR6z34A/6I6wH4A/6AP/L1MvgAH+ADfJT009BXy62v1JjUGwH0Rcv7wvR35/vmwvng119/NUsZNcy1SSjoV6bwlRM9fZ7brwJdryi9e/eu6ji9gqrP/fTpk72GKA9494pD1/ncceH1ODsa7qMdPst6EFfgI53PwAd5N+YX8Ye2N2/ePBsPhpYy8OCsvoDPvf4iX5GvUvnK2Xe8tG53D8qkdHlfHQHOwXlcZ1IPet0KPsAH+PDWH8vMg/SvhlNbaPpw9OHo77Z1y1wf7vfff/+qicVOOGvYi55YL/27mrsS1mrU1xznziNRoQsTSLXVnl//XpNktXEfjTXbWY+vlvyJqxZP4MPnM/DR5vcYH2rsaSvN+8TVLE8SV+m4gs/RJcor4AN8xPXBIrok5qlF48p9DjqxfbMYPofPU/X7S+HjsXBOn2G+n7NI3o113KJ5l/VgPcI881px3lcvgw/0bkrvmqXM58+frVmt7ebmpjk6OmrG47H9XRPYz87Osvs1KFUfrF/4ao5znxtPc+87X2p/OMWW+2A9FK/Elccz+PD5jHzV5nPwAT5SfA8+wEes9+AP+COuB+AP+AP+yNfL4AN8gA/wUdJPQ1+hr9BX+X7zKuHDhqa6SbQiCPnMqOiOpxjL5z21nym2jzvFVt8x6zGYfgd6NaUmHvXvv3z50siyQbHsXst0/z0Xx6n9THNnmjvT3Jnm/hTT3ENLGfLVFxu87vQFeZe8S959HXnXYRV9hW53NkfK5eh26g/4fL6fQr+E+lx6H71LP7Gr30r/ajXrQWu47+3tNaenp1bwxlPn9fR6PDU2nMrOVGGmCq/yVGFhAnw0ZlMkkVCbH8L8oR9AZPOjrS+vpPZfXFzYsawH60FczfMy+Di3OTDayFfoEnTJflbXgw/wAT7AR0ndj25v+yLoK/SV05fUH9Qf9EXz/Ak+0vgwSxk10ra2/FRxpvEyjdd5ObtpwzXxwdTt5Z66LdFQMj09zAPyLFMS1RCImvzAtOr0tGo1Bcm7jQ15Jq5ObAaKcEnevbI3k8DHyL4D8NHmT/Dh9Tn4QF+NRm1+QF+hr5Qfc/UZ/AF/xPEBf8Af8Mcsf4b5M8aHeFZvXNb0Pci75N1U3rWGu17/++uvv0zA7ezsmH97GHTyTc/t1xOrapgoYGuOU7NJn+teHdYTN67I7jqfOy68nvD1fO6D9SCuZvEKPnw+I1+Rd2N+AR/gI9Y76BKvB8EH+AAf/kdA+IN6sK/OhD/gj1Q/hfqD+gP+gD/gj/J+8yrVH2YpIzFd6tkeezXK0F5NcDXqY2+qEu9tBZ7Or8Gti3jI63pCPyzug/VQ3BFX3gMWfHjPPPJVO6MDfCyGj9h73WGLuCKuYr1D3iXvxjNjyLvpvJvLq30zpci75N1vybvweX5WBDpxdpYdfA6fw+dp73X6cO1sAurB2fhA787qXWu46wn1lDdViae7BLGCTFPZuzyNcp7N9/f31nB3gbqIR3R47dwH66EZA8RV62UcYxt8gA/w4b0owces1xx8/qEBH+Ajx5PgA3yEM6zgD/gjVfdSf1B/pGZVwR/wB/wxqy/D/Ak+wMcq48MsZXZ3d80ORptedzk7O2s2NzetCX99fd0cHR1l90tYSHRqkGHNce5zNYhE55HnUcn5UtdzeXlp1859DKzRzHoc2YAb4qrFM/jw+Qx8tPkcfHi+Ax/gI9Y74AN8pPQw/PF8/KHv/+bmhvqjo/6ijpqvT+Fz+Bw+z/dvwAf4+B7wUaIf6CfS3+3qbz+2vrKGu+xcSj3bYw91vXamBq8KkVrvdTX59WS8GqN6HWMRD3ldj14r1cZ9NGYXwXoQVyGewYf3jAMf7YwO8q735AUf4CP2VAQf4CM1Uwj+gD/iWVXwB/wBf+Q9ecEH+AAf4KOkv4e+Ql+tsr4yS5mNjY0ZH3QNML26urJG+Pb2dvPw8JDd746Vh1PNce5z5aGn5uDd3V3R+VLXM5lMrOHOfQym3wHrQVy5mQjgw+cz8lWbz8m73msuhw+9daVC4Sl4ULNGtMGD8/qCfEW+ivUn+ao/X9Xo78fiQYfVvjoB3U4d1VVHwoPwIH2GfJ8FfIAP8AE+whkCfXqPOmr56ihruMsO5vT01BoA8lDXK0d6Slqbmg2yasnt16IrCGqPCz9XDXdN7y45X+p69Pq3Nu6jsR89WI82XomrFs/gw+cz8OHzOfgAHzFvgw/wEes9+AP+SNUD8Af8AX/k62XwAT7AB/jo66ehr9BX6Kt8v3mV8GGWMmrQbm1tWXNdTxYdHx83o9HImtiHh4fNyclJdr+eAlSw3N7eVh3nPnc4HNp59etdyflS1xPaZ3AfrIfilbjyeAYfPp+Rr9p8Dj7AR4rvwQf4iPUe/AF/xPUA/AF/wB/5ehl8gA/wAT5K+mnoq+XWV+pL6o1k+qLlfWH6u/N9c+HcGu6yH4k9tko93fVkuhruanTH3jslnu4KZBGTnqxLeWbu7LSeRl3Xo9eNtXEfjb0pwHocWIIkrppG+AEfPn+Aj/+zWRvgw3sqgg/wEesL8AE+Up6j8Af8Edc18Af8AX/4mR/gw3u2x/0L+AP+AB/go6/fSv2xmvWHWcpILLgFVtNaA0xDryANMsjt10AU/VsRS81xOqc+V4GnP2vgqfOc7jqfOy68nvBauQ/Wg7iaxSv48PmMfEXejfmlBh9hcyXkO+KKuPqWuEJftT+Q5/QnOtF/PzX56rXEVS6vpvQ+eXe+PgMfi+EDPifvdvEOfYZ0fKB30bvoXfqiff1WdMmsLrGGu56IFulo05/39/dnPNvPz8+z+yWIVQCsr69XHSdvM33u/f29NdxdEeH+e831hP+25rjwPrkP1iOOc+LK5wHwAT7ABzzYx6/wuddR8Af8Ec4+Qu/O5k/wAT7Ah5+dhr5CX6GvyvtQ8Af8AX/AH2726GvoUw9++eWXr/KbCyfa6gZK/64G/X///Te1fCk9bmNjY2oh88MPP0wb+u6/13yO/OO1cR/tDyasR2M/4BBXTSM8gQ+fz8AHeTfmN/ABPmK9AX9MTFPBH22+dPEBf8Af8Ee+PgQf4AN8gI++/g36Cn3l9CX6aj5fgI/VxMfgzz///Cr/dQ13cJ6VEk2lf5f3uhoWMsmvOU4WNPr3Hz9+tEa5vM0WOb8+R68t6NdO7qMxL3zWg7gK8Qs+fD4DH6ufd8UjoUdeHy/l8FHzOcTV6sdVXxzF+8m7T5t3a/CJTmzx6XQB+Yp89VryFTinPidfrX6+AufgHJyvPs6/6zpKQ1P1a4qa1amp2m7abG7/jz/+aEJeTd6aKb5MsU1PsXWDY1kP4urk5MR+jBIuHx4e7AepPjym9jMFfbmnoJN34Q9w7vUH+Yp8NRqNZvgOfICPFE9Sf7R1BPgAH+BjYD8opvoQ4AN8gA/wkcsPYb8trj8kROVVXtPfRJegS6TL4rgaqOGuQVV9U3Nz+5m6zdRtpm4zdbsvfzB1ezWnbkuMwB+NvaGlH55FsOPxeGqxph+z4/yY2g8+wId7wy+MD+IKfYW+Ql+hrzw/9PFrOAi173ujru3+XtEl6BJ0SdPoDTl0e3ddQ9713w+6Hd2e0u02NFW/6PRNm83tZ1o106qZVs206r78wbTq2WnVErHk3aZ5bfwRisovX75Mh32/tvuAzz0ew3UUJsVn5CvylZ6I04+J4NznuafCRy6vuvORr8hXKTym4kM/cpfqK/icuHqKuCJfEVfE1bx+gs/b+oJ+SXd+CL+fGj5f9rxrDXc9mdc3HTu3XwDSF7K+vt7s7/upyR8+fGi6psa6/ff399Z4cl9q6XG5CdXcB+uhuCOu/LR3prkzzZ1p7kxzVxOij1/h81a3wB/wh/AibRzqWvABPuK6Bn2FvkJfoa/QV/36kr7PfF8M/oA/4I/vgz/MUmZ3d7eJPTNLPcRVmKoo2dvbMy/B0uOcl9LFxYU1++V5tIiXsTyrLy8v7YkG7mNgjQLW47ghrrwHLvjweQl8tB6X4AN8pGZCgA/wEXtVwh/wR6zrn4M/SrxTyVfkK/KVn/kRz3ACH+ADfICPvv7ec/B5SX/vMfNViX5Iebqjd9G7T6V3reH++fPnas9Z532n1yPU4BVQSrxqY888PRkvIOq13ZRXWIkHrl7n18Z9NPa6CusxtjcuiKvG3hwBH94DFny0XoTg461xBviY9ZgHH+Aj9iqFP+APdHve6xp8gA/wAT5K+hfoK/QV+irvBQ8+wMcq48MsZTY2NqaWLnpldm1tzaZ9q2G5vb3dPDw8ZPe7Y+V1WXOc+1x56Omcd3d3RedLXc9kMrHmCfcxmH4HrAdx5XwswYfPZ+SrNp+Td73Paw4feutKhfRT8KC85rTBg/P6gnxFvor1J/mqP1/V6O/H4kGH1b46Ad1OHdVVR8KD8CB9hnyfBXyAD/ABPkI/8z69Rx21fHWUNdxlBxN6COmVCj0lrU3NBlm15PZr0RUE8uaqOS78XDXcNdW35Hyp69HrMNq4j8Z+9GA92nglrlpPPfDh8xn48PkcfICPmLfBB/iI9R78AX+k6gH4A/6AP/J1L/gAH+ADfPT1xdBX6Cv0Vb7fvEr4MEsZNWhrvdedJ5SeAlSw3N7emjdwn1dUvH84HFpTX7/epTxlUx5LsVe8hI2zB+A+WA/FB3G1NZ2JAD58XiJfHdqsDfABPlKeiuADfKCv8rOIwAf4AB/go6/ORF+hr9BXA3NKSPWFwAf4eC34WMQLHp2ITkzpRGu4y35EidG94q7X6ENfwp2dnex+PZmuhrsESM1x8lvX5+rVegFPT9Y5H/au87njwuvV68bauI/G3hRgPYgr8JHOZ+CDvBvzC/zh+R58gA/wgd7tqwfQ7V5fwB/wR1wvgw/wkeqnoK/QV+gr9BX6qrzfvEr6yixl9ASsEwhqWmuAaegVpEEGuf0SGvq3SiI1x+mc+lw16fVnNdKd53TX+dxx4fWE18p9sB7E1SxewYfPZ+Qr8m7MLzX4CMk/5Dviirj6lrhCX7UPXuT0JzrRfz81+eq1xFUur6b0Pnl3vj4DH4vhAz4n73bxDn2GdHygd9G76F36on39VnTJrC6xhrueiBbpaNOf9/f3Zzzbz8/Ps/sliFUArK+vVx0nbzN97v39vTXcXRHh/nvN9YT/tua48D65D9YjjnPiyucB8AE+wAc82Mev8LnXUfAH/BHOPkLvzuZP8AE+wMep1d2puhd8gA/wAT5y+YH+1XyfkvqD+iPVx16W/pVZymhIVq33uvOwU4CrSa+BpbFnTYmnuwzxJSzkeZTydDo8bL2AujzzNJRCG/cxsB8wWI9jGxRKXDWN8AM+fP4AH62nIvgYTWeGgA/wEesL8AE+UjOF4I/n448S71TW4/nWo6Q+Yz1Yj7juR1+hr9BX3rMdfORnPT4mf5Toh9SMAfIV+eqp8pU13GXnEg5WPDg4MKsXbbKKGY/H2f16rUQNXgGl5jj3uXoyXkJOr4mWnC91PaF/HvfBeiheiSs/SBh8+HxGvmrzOfgAH27QeMjb4AN8xHoP/oA/4noA/oA/4I/G3sxO1b3gA3yAD/CRyw9hnwp9hb5CX+X7zauEj8Eff/zxVU+XTyYTa3i/efPGmuvuifeff/65ub6+zu7f3Ny0/e/evas6zn3up0+f7Nibmxs7f9/5UvvV7NfGfTQN69HGK3Hl8Qw+fD4DH2l86E0I5eAaHljWuNJ93N3dFfPZst5HF+/Cg+iSkvhYZR4E5+j2Rfnc8d0q46MkP4R8v6w8CM7B+aI4p88wr5PAOfVg3N+DB+v6JX31MvmKPlysv5R3B7/99tvX4XA4fcJcT7hpc0+c9/1djQ39mq//1RznPlcXof+p0a+t73yp/be3t3Ys99FYo4n1aCymiKsWT+DD5zPw0eZ38NG+UQU+ZvkefICPWMfBH/BHXA/AH/BHql6DP+AP+CPfPwEf4AN8gI++/ir6ajX1lQ1N1RPteopA1i76s5rfHz9+tIbETz/9ZE3c3P5wWnXNce5z4ym2fedL7XcDV7mPpmE92nglrjyewYfPZ+ADfMR8VoOP8N+GfEdcEVffElfoq279CZ+vNp/n8mpfPUDeJe9+S96Fz8m7pX2PGp0InxNXxBX9RFlg0xd928i2vKSfvOr9XWu4y4M9nIgeTj9OTU9nOjLTkePp6WH85P5MXM3HTR/umLrN1G0RlXI0eZe8S949NeGGLvnQnJ+f9+o2+AP+gD/m+dPlD/ABPsAH+Ij1dUpfUNd6/a2HG/UDhN6mr6nrybvzuo24Iq7iug5dspq6xIam7u7uTj3b5U10cuKn1Kam+Ib7H3OqsBJP3/mYKnzVxFOuWY/5eL24uLBmhHz9iatLa1KB84FZqUhc7+3tVeU5l3eIq5HFkniCae5Mc3+qae7kqy3jLfJVq3fIu99f3lUO+Pvvv9G7x8e99Rn4+P7wUVLXwB/wR9wvQLej278H3V6iH+gn0k/s6qc+dt/HGu563D+cmpyaup7bLw9cNbBE7DXH6VWL8Xhsv5BKODhPI/ffa65nlaapAxGiAAAelklEQVTYKkmwHq23MnEFPpjm/tYa3PG0e/ABf4g/wQf4SOUH9FWbH8AH+AAf8/ohVWdRRx2YFaXzpqf+oP6AP+AP+AP+iOvvvj4l9Tn1ear+MEuZjY0Na+hoE8EoWPRkiRrh79+/t2Z4br87Vv+u5jj3uf/884+dUz58JedLXc9k0k4Y5j4G0++A9SCuHJ7Bh89n5Ks2n5N3Pd+9BD70/WuDB+f1xUusR6x3wMfL4oP1yOvvZcKHu5a+OgHdTh3VVUfCg/AgfYZ8nwV8gA/wAT5q+rTLpBPp775ttB7WcJe9Qpc3bJdH19ramjXj+zxVu/ar4f7vv/9aA6LESy2+Hr1OqY37aBrWw3tdE1et1zH48F7H4AN8wB/5WRLgA3yAD/BRUg+gr9BXcV0Hf8Af8Af8AX+UzRqiPp/1tIc/4I9V5g+zlFHDPPZ0Go28J16Xp7s8stUkv7297fRazHmzD4dDe7L94eFh6g1c4yGvzw1f++I+WA/FD3HVegCDj1mPMvJVO6MDfICPlAcs+AAfsf5CX3kPbfABPsBHfsYX+AAf4AN89PVhqD+oP15L/bGIFzw8CA+meNAa7npdSY05bdvb2+ZjF/r5ya8ot19PpqvhrgRbc5x8yvW57tVh/bLl/Mu7zueOC68nfD2f+2A9iKtZvIIPn8/IV+TdmF/AB/iI9Q66xOtB8AE+wMf/ZusT8AE+wAf4KOlfUH9Qf1B/0E/s61NSf6xm/WGWMnqCqdSzPfZilNDQE/JKIiUefbEHkZr0Or8a6Yt4yOt6ws/kPlgPzRIgrrwHLPjw3m/kq3ZGB/hYDB+x97rDFnFFXMUzbMi75F30bn42VPjd5PJq30wp8i5591vyLnzePasNnbiYTsz1IchX5KtvyVfEFfmqdNYl9cfy1R/WcNcT6lrERTzUJYi1sOvr602X13vOm/3+/t4a7i44FvGCD6+d+2A9zs/PG+LK4xl8eE9F8lXrmQc+wEeK78EH+FB+yHEG+gp9BX+Aj1wegD/gD/hjNj+EfRHwAT7AB/jo09HU56tZn5ulzO7ubjMej63hfnBw0JydnTWbm5tWdF1fXzdHR0fZ/QoMNdM1sLTmOPe5Guio88jzqOR8qeu5vLy0a+c+BtZIYz2ObFAocdXiGXz4fAY+2nwOPjzfgQ/wEesd8AE+UnoY/ng+/tD3f3NzQ/3RUX9RR83Xp/A5fA6f5/s34AN8fA/4KNEP9BPp73b1tx9bX1nDXXYu4WAsNen0Kpk2WcWoGZ/br9dj1OBVIVJznPtcPRmvxqjsaErOl7qe0D+P+2A9FK/E1VvDk94cAR8+n5Gv2nwOPsCHyw8hb4MP8BHrPfgD/ojrAfgD/oA/Wn2dqnvBB/gAH+Ajlx/CPhX6Cn2Fvsr3m1cJH2Yps7GxMeODHntMdXmzu2PlpV5znPOCl4eeks/d3Z013mOP+D4PR+2fTCbWXOQ+BtPvgPUgrtxMBPDxP9MZFeSr9/bjJnnXewu/BD5i71jW42XXI/bGZD1Yj9RMIfhjlj9c7kS3U3+ofgMf6Ku4X/AS+go+z3tdsx7Ug+ADfJT0W+Hzx+Vza7jLDub09NSa1rUe6mtra9asrz0u9HRX01zTuxfxkNd59fq3Nu6jaViPr9NZAsRVi2fw4T3jwAf4iGeNgA/wkfJUhD/gj1jXwh/wB/zhZ+KAj9aTGv7Iz4BDX6GvwAf4KOnvoa/QV6usr8xSRg3zra0tEw1XV1fN8fFxMxqNrIl9eHjYnJycZPfLe13N89vb26rj3OcOh0M778PDQ9H5UtcTvp7DfbAeilfiyuMZfPh8Rr5q8zn4AB8pvgcf4CPWe/AH/BHXA/AH/AF/5Otl8AE+wAf4KOmnoa+WW1+pMak3AuiLlveF6e/O982F88Gvv/5qljJqmGuTUNCvTOErJ3r6PLdfBbpeUXr37l3VcXoFVZ/76dMnew1RHvDuFYeu87njwutxdjTcRzt8lvUgrsBHOp+BD/JuzC/iD21v3rx5Nh4MLWXgwVl9AZ97/UW+Il+l8pWz73hp3e4elEnp8r46ApyD87jOpB70uhV8gA/w4a0/lpkH6V8Np7bQ9OHow9HfbeuWuT7c77///lUTi51w1rAXPbFe+nc1dyWs1aivOc6dR6JCFyaQaqs9v/69Jslq4z4aa7azHl8t+RNXLZ7Ah89n4KPN7zE+1NjTVpr3iatZniSu0nEFn6NLlFfAB/iI64NFdEnMU4vGlfscdGL7ZjF8Dp+n6veXwsdj4Zw+w3w/Z5G8G+u4RfMu68F6hHnmteK8r14GH+jdlN41S5nPnz9bs1rbzc1Nc3R01IzHY/u7JrCfnZ1l92tQqj5Yv/DVHOc+N57m3ne+1P5wii33wXooXokrj2fw4fMZ+arN5+ADfKT4HnyAj1jvwR/wR1wPwB/wB/yRr5fBB/gAH+CjpJ+GvkJfoa/y/eZVwocNTXWTaEUQ8plR0R1PMZbPe2o/U2wfd4qtvmPWYzD9DvRqSk086t9/+fKlkWWDYtm9lun+ey6OU/uZ5s40d6a5M839Kaa5h5Yy5KsvNnjd6QvyLnmXvPs68q7DKvoK3e5sjpTL0e3UH/D5fD+Ffgn1ufQ+epd+Yle/lf7VataD1nDf29trTk9PreCNp87r6fV4amw4lZ2pwkwVXuWpwsIE+GjMpkgioTY/hPlDP4DI5kdbX15J7b+4uLBjWQ/Wg7ia52XwcW5zYLSRr9Al6JL9rK4HH+ADfICPkrof3d72RdBX6CunL6k/qD/oi+b5E3yk8WGWMmqkbW35qeJM42Uar/NydtOGa+KDqdvLPXVboqFkenqYB+RZpiSqIRA1+YFp1elp1WoKkncbG/JMXJ3YDBThkrx7ZW8mgY+RfQfgo82f4MPrc/CBvhqN2vyAvkJfKT/m6jP4A/6I4wP+gD/gj1n+DPNnjA/xrN64rOl7kHfJu6m8aw13vf73119/mYDb2dkx//Yw6OSbntuvJ1bVMFHA1hynZpM+1706rCduXJHddT53XHg94ev53AfrQVzN4hV8+HxGviLvxvwCPsBHrHfQJV4Pgg/wAT78j4DwB/VgX50Jf8AfqX4K9Qf1B/wBf8Af5f3mVao/zFJGYrrUsz32apShvZrgatTH3lQl3tsKPJ1fg1sX8ZDX9YR+WNwH66G4I668Byz48J555Kt2Rgf4WAwfsfe6wxZxRVzFeoe8S96NZ8aQd9N5N5dX+2ZKkXfJu9+Sd+Hz/KwIdOLsLDv4HD6Hz9Pe6/Th2tkE1IOz8YHendW71nDXE+opb6oST3cJYgWZprJ3eRrlPJvv7++t4e4CdRGP6PDauQ/WQzMGiKvWyzjGNvgAH+DDe1GCj1mvOfj8QwM+wEeOJ8EH+AhnWMEf8Eeq7qX+oP5IzaqCP+AP+GNWX4b5E3yAj1XGh1nK7O7umh2MNr3ucnZ21mxubloT/vr6ujk6Osrul7CQ6NQgw5rj3OdqEInOI8+jkvOlrufy8tKunfsYWKOZ9TiyATfEVYtn8OHzGfho8zn48HwHPsBHrHfAB/hI6WH44/n4Q9//zc0N9UdH/UUdNV+fwufwOXye79+AD/DxPeCjRD/QT6S/29Xffmx9ZQ132bmUerbHHup67UwNXhUitd7ravLryXg1RvU6xiIe8roevVaqjftozC6C9SCuQjyDD+8ZBz7aGR3kXe/JCz7AR+ypCD7AR2qmEPwBf8SzquAP+AP+yHvygg/wAT7AR0l/D32FvlplfWWWMhsbGzM+6BpgenV1ZY3w7e3t5uHhIbvfHSsPp5rj3OfKQ0/Nwbu7u6Lzpa5nMplYw537GEy/A9aDuHIzEcCHz2fkqzafk3e911wOH3rrSoXCU/CgZo1ogwfn9QX5inwV60/yVX++qtHfj8WDDqt9dQK6nTqqq46EB+FB+gz5Pgv4AB/gA3yEMwT69B511PLVUdZwlx3M6empNQDkoa5XjvSUtDY1G2TVktuvRVcQ1B4Xfq4a7preXXK+1PXo9W9t3EdjP3qwHm28ElctnsGHz2fgw+dz8AE+Yt4GH+Aj1nvwB/yRqgfgD/gD/sjXy+ADfIAP8NHXT0Nfoa/QV/l+8yrhwyxl1KDd2tqy5rqeLDo+Pm5Go5E1sQ8PD5uTk5Psfj0FqGC5vb2tOs597nA4tPPq17uS86WuJ7TP4D5YD8UrceXxDD58PiNftfkcfICPFN+DD/AR6z34A/6I6wH4A/6AP/L1MvgAH+ADfJT009BXy62v1JfUG8n0Rcv7wvR35/vmwrk13GU/EntslXq668l0NdzV6I69d0o83RXIIiY9WZfyzNzZaT2Nuq5Hrxtr4z4ae1OA9TiwBElcNY3wAz58/gAf/2ezNsCH91QEH+Aj1hfgA3ykPEfhD/gjrmvgD/gD/vAzP8CH92yP+xfwB/wBPsBHX7+V+mM16w+zlJFYcAusprUGmIZeQRpkkNuvgSj6tyKWmuN0Tn2uAk9/1sBT5znddT53XHg94bVyH6wHcTWLV/Dh8xn5irwb80sNPsLmSsh3xBVx9S1xhb5qfyDP6U90ov9+avLVa4mrXF5N6X3y7nx9Bj4Wwwd8Tt7t4h36DOn4QO+id9G79EX7+q3oklldYg13PREt0tGmP+/v7894tp+fn2f3SxCrAFhfX686Tt5m+tz7+3truLsiwv33musJ/23NceF9ch+sRxznxJXPA+ADfIAPeLCPX+Fzr6PgD/gjnH2E3p3Nn+ADfIAPPzsNfYW+Ql+V96HgD/gD/oA/3OzR19CnHvzyyy9f5TcXTrTVDZT+XQ36//77b2r5UnrcxsbG1ELmhx9+mDb03X+v+Rz5x2vjPtofTFiPxn7AIa6aRngCHz6fgQ/ybsxv4AN8xHoD/piYpoI/2nzp4gP+gD/gj3x9CD7AB/gAH339G/QV+srpS/TVfL4AH6uJj8Gff/75Vf7rGu7gPCslmkr/Lu91NSxkkl9znCxo9O8/fvxojXJ5my1yfn2OXlvQr53cR2Ne+KwHcRXiF3z4fAY+Vj/vikdCj7w+Xsrho+ZziKvVj6u+OIr3k3efNu/W4BOd2OLT6QLyFfnqteQrcE59Tr5a/XwFzsE5OF99nH/XdZSGpurXFDWrU1O13bTZ3P4ff/zRhLyavDVTfJlim55i6wbHsh7E1cnJif0YJVw+PDzYD1J9eEztZwr6ck9BJ+/CH+Dc6w/yFflqNBrN8B34AB8pnqT+aOsI8AE+wMfAflBM9SHAB/gAH+Ajlx/Cfltcf0iIyqu8pr+JLkGXSJfFcTVQw12Dqvqm5ub2M3WbqdtM3Wbqdl/+YOr2ak7dlhiBPxp7Q0s/PItgx+Px1GJNP2bH+TG1H3yAD/eGXxgfxBX6Cn2FvkJfeX7o49dwEGrf90Zd2/29okvQJeiSptEbcuj27rqGvOu/H3Q7uj2l221oqn7R6Zs2m9vPtGqmVTOtmmnVffmDadWz06olYsm7TfPa+CMUlV++fJkO+35t9wGfezyG6yhMis/IV+QrPRGnHxPBuc9zT4WPXF515yNfka9SeEzFh37kLtVX8Dlx9RRxRb4iroiref0En7f1Bf2S7vwQfj81fL7sedca7noyr286dm6/AKQvZH19vdnf91OTP3z40HRNjXX77+/vrfHkvtTS43ITqrkP1kNxR1z5ae9Mc2eaO9PcmeauJkQfv8LnrW6BP+AP4UXaONS14AN8xHUN+gp9hb5CX6Gv+vUlfZ/5vhj8AX/AH98Hf5ilzO7ubhN7ZpZ6iKswVVGyt7dnXoKlxzkvpYuLC2v2y/NoES9jeVZfXl7aEw3cx8AaBazHcUNceQ9c8OHzEvhoPS7BB/hIzYQAH+Aj9qqEP+CPWNc/B3+UeKeSr8hX5Cs/8yOe4QQ+wAf4AB99/b3n4POS/t5j5qsS/ZDydEfvonefSu9aw/3z58/VnrPO+06vR6jBK6CUeNXGnnl6Ml5A1Gu7Ka+wEg9cvc6vjfto7HUV1mNsb1wQV429OQI+vAcs+Gi9CMHHW+MM8DHrMQ8+wEfsVQp/wB/o9rzXNfgAH+ADfJT0L9BX6Cv0Vd4LHnyAj1XGh1nKbGxsTC1d9Mrs2tqaTftWw3J7e7t5eHjI7nfHyuuy5jj3ufLQ0znv7u6Kzpe6nslkYs0T7mMw/Q5YD+LK+ViCD5/PyFdtPifvep/XHD701pUK6afgQXnNaYMH5/UF+Yp8FetP8lV/vqrR34/Fgw6rfXUCup06qquOhAfhQfoM+T4L+AAf4AN8hH7mfXqPOmr56ihruMsOJvQQ0isVekpam5oNsmrJ7deiKwjkzVVzXPi5arhrqm/J+VLXo9dhtHEfjf3owXq08UpctZ564MPnM/Dh8zn4AB8xb4MP8BHrPfgD/kjVA/AH/AF/5Ote8AE+wAf46OuLoa/QV+irfL95lfBhljJq0NZ6rztPKD0FqGC5vb01b+A+r6h4/3A4tKa+fr1LecqmPJZir3gJG2cPwH2wHooP4mprOhMBfPi8RL46tFkb4AN8pDwVwQf4QF/lZxGBD/ABPsBHX52JvkJfoa8G5pSQ6guBD/DxWvCxiBc8OhGdmNKJ1nCX/YgSo3vFXa/Rh76EOzs72f16Ml0NdwmQmuPkt67P1av1Ap6erHM+7F3nc8eF16vXjbVxH429KcB6EFfgI53PwAd5N+YX+MPzPfgAH+ADvdtXD6Dbvb6AP+CPuF4GH+Aj1U9BX6Gv0FfoK/RVeb95lfSVWcroCVgnENS01gDT0CtIgwxy+yU09G+VRGqO0zn1uWrS689qpDvP6a7zuePC6wmvlftgPYirWbyCD5/PyFfk3ZhfavARkn/Id8QVcfUtcYW+ah+8yOlPdKL/fmry1WuJq1xeTel98u58fQY+FsMHfE7e7eId+gzp+EDvonfRu/RF+/qt6JJZXWINdz0RLdLRpj/v7+/PeLafn59n90sQqwBYX1+vOk7eZvrc+/t7a7i7IsL995rrCf9tzXHhfXIfrEcc58SVzwPgA3yAD3iwj1/hc6+j4A/4I5x9hN6dzZ/gA3yAj1Oru1N1L/gAH+ADfOTyA/2r+T4l9Qf1R6qPvSz9K7OU0ZCsWu9152GnAFeTXgNLY8+aEk93GeJLWMjzKOXpdHjYegF1eeZpKIU27mNgP2CwHsc2KJS4ahrhB3z4/AE+Wk9F8DGazgwBH+Aj1hfgA3ykZgrBH8/HHyXeqazH861HSX3GerAecd2PvkJfoa+8Zzv4yM96fEz+KNEPqRkD5Cvy1VPlK2u4y84lHKx4cHBgVi/aZBUzHo+z+/VaiRq8AkrNce5z9WS8hJxeEy05X+p6Qv887oP1ULwSV36QMPjw+Yx81eZz8AE+3KDxkLfBB/iI9R78AX/E9QD8AX/AH429mZ2qe8EH+AAf4COXH8I+FfoKfYW+yvebVwkfgz/++OOrni6fTCbW8H7z5o01190T7z///HNzfX2d3b+5uWn73717V3Wc+9xPnz7ZsTc3N3b+vvOl9qvZr437aBrWo41X4srjGXz4fAY+0vjQmxDKwTU8sKxxpfu4u7sr5rNlvY8u3oUH0SUl8bHKPAjO0e2L8rnju1XGR0l+CPl+WXkQnIPzRXFOn2FeJ4Fz6sG4vwcP1vVL+upl8hV9uFh/Ke8Ofvvtt6/D4XD6hLmecNPmnjjv+7saG/o1X/+rOc59ri5C/1OjX1vf+VL7b29v7Vjuo7FGE+vRWEwRVy2ewIfPZ+Cjze/go32jCnzM8j34AB+xjoM/4I+4HoA/4I9UvQZ/wB/wR75/Aj7AB/gAH339VfTVauorG5qqJ9r1FIGsXfRnNb8/fvxoDYmffvrJmri5/eG06prj3OfGU2z7zpfa7wauch9Nw3q08UpceTyDD5/PwAf4iPmsBh/hvw35jrgirr4lrtBX3foTPl9tPs/l1b56gLxL3v2WvAufk3dL+x41OhE+J66IK/qJssCmL/q2kW15ST951fu71nCXB3s4ET2cfpyans50ZKYjx9PTw/jJ/Zm4mo+bPtwxdZup2yIq5WjyLnmXvHtqwg1d8qE5Pz/v1W3wB/wBf8zzp8sf4AN8gA/wEevrlL6grvX6Ww836gcIvU1fU9eTd+d1G3FFXMV1HbpkNXWJDU3d3d2derbLm+jkxE+pTU3xDfc/5lRhJZ6+8zFV+KqJp1yzHvPxenFxYc0I+foTV5fWpALnA7NSkbje29urynMu7xBXI4sl8QTT3Jnm/lTT3MlXW8Zb5KtW75B3v7+8qxzw999/o3ePj3vrM/Dx/eGjpK6BP+CPuF+Abke3fw+6vUQ/0E+kn9jVT33svo813PW4fzg1OTV1PbdfHrhqYInYa47Tqxbj8dh+IZVwcJ5G7r/XXM8qTbFVkmA9Wm9l4gp8MM39rTW442n34AP+EH+CD/CRyg/oqzY/gA/wAT7m9UOqzqKOOjArSudNT/1B/QF/wB/wB/wR1999fUrqc+rzVP1hljIbGxvW0NEmglGw6MkSNcLfv39vzfDcfnes/l3Nce5z//nnHzunfPhKzpe6nsmknTDMfQym3wHrQVw5PIMPn8/IV20+J+96vnsJfOj71wYPzuuLl1iPWO+Aj5fFB+uR19/LhA93LX11ArqdOqqrjoQH4UH6DPk+C/gAH+ADfNT0aZdJJ9LffdtoPazhLnuFLm/YLo+utbU1a8b3eap27VfD/d9//7UGRImXWnw9ep1SG/fRNKyH97omrlqvY/DhvY7BB/iAP/KzJMAH+AAf4KOkHkBfoa/iug7+gD/gD/gD/iibNUR9PutpD3/AH6vMH2Ypo4Z57Ok0GnlPvC5Pd3lkq0l+e3vb6bWY82YfDof2ZPvDw8PUG7jGQ16fG772xX2wHoof4qr1AAYfsx5l5Kt2Rgf4AB8pD1jwAT5i/YW+8h7a4AN8gI/8jC/wAT7AB/jo68NQf1B/vJb6YxEveHgQHkzxoDXc9bqSGnPatre3zccu9POTX1Fuv55MV8NdCbbmOPmU63Pdq8P6Zcv5l3edzx0XXk/4ej73wXoQV7N4BR8+n5GvyLsxv4AP8BHrHXSJ14PgA3yAj//N1ifgA3yAD/BR0r+g/qD+oP6gn9jXp6T+WM36wyxl9ARTqWd77MUooaEn5JVESjz6Yg8iNel1fjXSF/GQ1/WEn8l9sB6aJUBceQ9Y8OG938hX7YwO8LEYPmLvdYct4oq4imfYkHfJu+jd/Gyo8LvJ5dW+mVLkXfLut+Rd+Lx7Vhs6cTGdmOtDkK/IV9+Sr4gr8lXprEvqj+WrP6zhrifUtYiLeKhLEGth19fXmy6v95w3+/39vTXcXXAs4gUfXjv3wXqcn583xJXHM/jwnorkq9YzD3yAjxTfgw/wofyQ4wz0FfoK/gAfuTwAf8Af8Mdsfgj7IuADfIAP8NGno6nPV7M+N0uZ3d3dZjweW8P94OCgOTs7azY3N63our6+bo6OjrL7FRhqpmtgac1x7nM10FHnkedRyflS13N5eWnXzn0MrJHGehzZoFDiqsUz+PD5DHy0+Rx8eL4DH+Aj1jvgA3yk9DD88Xz8oe//5uaG+qOj/qKOmq9P4XP4HD7P92/AB/j4HvBRoh/oJ9Lf7epvP7a+soa77FzCwVhq0ulVMm2yilEzPrdfr8eowatCpOY497l6Ml6NUdnRlJwvdT2hfx73wXooXomrt4YnvTkCPnw+I1+1+Rx8gA+XH0LeBh/gI9Z78Af8EdcD8Af8AX+0+jpV94IP8AE+wEcuP4R9KvQV+gp9le83rxI+zFJmY2Njxgc99pjq8mZ3x8pLveY45wUvDz0ln7u7O2u8xx7xfR6O2j+ZTKy5yH0Mpt8B60FcuZkI4ON/pjMqyFfv7cdN8q73Fn4JfMTesazHy65H7I3JerAeqZlC8Mcsf7jciW6n/lD9Bj7QV3G/4CX0FXye97pmPagHwQf4KOm3wuePy+fWcJcdzOnpqTWtaz3U19bWrFlfe1zo6a6muaZ3L+Ihr/Pq9W9t3EfTsB5fp7MEiKsWz+DDe8aBD/ARzxoBH+Aj5akIf8Afsa6FP+AP+MPPxAEfrSc1/JGfAYe+Ql+BD/BR0t9DX6GvVllfmaWMGuZbW1smGq6urprj4+NmNBpZE/vw8LA5OTnJ7pf3uprnt7e3Vce5zx0Oh3beh4eHovOlrid8PYf7YD0Ur8SVxzP48PmMfNXmc/ABPlJ8Dz7AR6z34A/4I64H4A/4A/7I18vgA3yAD/BR0k9DXy23vlJjUm8E0Bct7wvT353vmwvng19//dUsZdQw1yahoF+ZwldO9PR5br8KdL2i9O7du6rj9AqqPvfTp0/2GqI84N0rDl3nc8eF1+PsaLiPdvgs60FcgY90PgMf5N2YX8Qf2t68efNsPBhaysCDs/oCPvf6i3xFvkrlK2ff8dK63T0ok9LlfXUEOAfncZ1JPeh1K/gAH+DDW38sMw/SvxpObaHpw9GHo7/b1i1xH+7/ARXDaleuMbhYAAAAAElFTkSuQmCC'
    shopClassesMenu.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAgAElEQVR4Xu2dTcw2S1rX+37eIYBkAgyJhOhC3UhiYCZhgQtxyIAJyPl6Z2FMkHGlG10YEwwBAsyehBB0McoCBw2u5pw5c4ZZiAeGmMGFJBJjwsKoISQkGmHAxAjD+9ymurq6qquruqurP+6rr/7dEzjvc9/VVVf9r+r+1VVffbvf7/eGDwqgAAqgAAqgwKkVuAH0U/sP41EABVAABVCgVQCg0xBQAAVQAAVQQIECAF2BE6kCCqAACqAACgB02gAKoAAKoAAKKFAAoCtwIlVAARRAARRAAYBOG0ABFEABFEABBQoAdAVOpAoogAIogAIoANBpAyiAAiiAAiigQAGArsCJVAEFUAAFUAAFADptAAVQAAVQAAUUKADQFTiRKqAACqAACqAAQKcNoAAKoAAKoIACBQC6AidSBRRAARRAARQA6LQBFEABFEABFFCgAEBX4ESqgAIogAIogAIAnTaAAiiAAiiAAgoUAOgKnEgVUAAFUAAFUACg0wZQAAVQAAVQQIECAF2BE6kCCqAACqAACgB02gAKoAAKoAAKKFAAoCtwIlVAARRAARRAAYBOG0ABFEABFEABBQoAdAVOpAoogAIogAIoANBpAyiAAiiAAiigQAGArsCJVAEFUAAFUAAFADptAAVQAAVQAAUUKADQFTiRKqAACqAACqAAQKcNoAAKoAAKoIACBQC6AidSBRRAARRAARQA6LQBFEABFEABFFCgAEBX4ESqgAIogAIogAIAnTaAAiiAAiiAAgoUAOgKnEgVUAAFUAAFUACg0wZQAAVQAAVQQIECAF2BE6kCCqAACqAACgB02gAKoAAKoAAKKFAAoCtwIlVAARRAARRAAYBOG0ABFEABFEABBQoAdAVOpAoogAIogAIoANBpAyiAAiiAAiigQAGArsCJVAEFUAAFUAAFADptAAVQAAVQAAUUKADQFTiRKqAACqAACqAAQKcNoAAKoAAKoIACBQC6AidSBRRAARRAARQA6LQBFEABFEABFFCgAEBX4ESqgAIogAIogAIAnTaAAiiAAiiAAgoUAOgKnEgVUAAFUAAFUEAN0F9/5yXeVKzA5956+3S1m2qTv/BdP9980zd9U1WdPvnJTzb/8cP/qepaLkIBFBgrcMbnS8qPAJ3WLV6B+89/pXnvvffE2xkbCNBP5zIMvqgCAF2Y44nQhTlkQ3P+wr/7883P/dzPbZjjMVkB9GN0phQUWKsAQF+r4MbXA/SNBRWUHUAfOoMhd0GNE1NUKADQhbkRoAtzyIbmAHSAvmFzIisUGCkA0IU1CoAuzCEbmgPQAfqGzYmsUACgS28DAF26h+rtA+gAvb71cCUKzCtAhD6v0aEpAPqhch9aGEAH6Ic2OAq7nAIAXZjLAbowh2xoDkAH6Bs2J7JCAYbcpbcBgC7dQ/X2rQW6WRX+iM/U4S8cLPMIj1AmCqQVIEIX1jJKga7FccLkH5lT6o+SeqwF+pa2lNhbkgagl6hEGhQ4RgEtXLjcSXFaHHdMM68vZUuIAnSG3OtbIleiwLwCWrgA0Od93ab48pe/3HzjN35jYWqb7Cd/8iebn/qpn5q85iMf+UjzW7/1W81HP/rR5td+7dcW5e+uLb2opozSvON0AH1aOSL02pbFdSiwvQIAfXtNV+VYCpBaxwH0Ze4p9UdJrkToROgl7YQ0KFCrQC0Xasvb6zoi9EJlQ6B/+MMfbkx0/Cd/8ifNL/3SL7U5pL4ridB/4id+ovmd3/md5lu/9VubH/mRHym0xiZzEfo3f/M3N9/3fd/Xfvfuu+82f/AHf9CkviNCXyTvromJ0HeVl8xRYJECAH2RXPsnLo0Iax0XAt2BuvS7vWqfGq4v/W4vm1y+pf4osYMInQi9pJ2QBgVqFajlQm15e11HhF6obCm843QGsC9fpt/Vfr/f+yjbRM/vvPNOdp7+V3/1V5vv/u7vHlhbCu843e12S9balGFs+Nmf/dnR77F9plMT1i22D6BPNywi9MIbj2QocIACAP0AkZcUUQqQWsdpAbp5r/gHP/jBpLS/8iu/0nzuc58D6EsaXmXaNUCvLLLostL7qCgzAYnQWYATTmBCLRekVY0IvdAjtUB//fXXm5/5mZ/pS/nN3/zN5rd/+7fbv6ci9G/7tm9rPvShDzVf/OIX27RbRejhKECqjDBC/8Ef/MHml3/5l9s5eSL0woZSmAzQFAq1Mhk6rxTwIpcDdGGOLo0sah1XC/Qf/dEfbf7oj/6oV+vHf/zHm0996lOzQJ8b0jYZ1Ay5h0D/4R/+4cZA/ROf+ETfaQiBPtXhmLOv1B8lzYg59BKVtkmzpd+2sWhdLgB9nX5XubqWC9L0IUIv9Egt0Gvn0OeAuQXQ46rHc+gAvbBxVCQDNBWiVVyCzhWiXfASgC7M6aWRRa3jtgD6T//0Tzdf+tKXms985jMiIvQf+qEf6qNzY9B3fMd3NObcc7coDqDv18gBzX7ahjmj8zE6n72UWi5IqzcReqFHtgD6937v9za/+7u/WzSHfkSEbobbv/3bv71X4Md+7Mfa6YASoM/N8Zd2sErkZ8i9RKVt0mzpN2fR5z70hVnjXv/9759NU5MAoNeodr1rALown5c+iGodtwXQY8nWRMCPHnIP6/J1X/d1zRe+8IXmu77ru/qvS/1R0owAeolK26TZwm8jgP/1P5437te/epBmK8AD9HnpSdE0tVyQph0ReqFHAPqyffJbgMG5Zi3Q93p96gc+8IHG/F/u8+//8n/I/qYRND3ISwA+d991gF8Ldo06z0nH78sVAOjLNdv1ilKAaHHcrmJukHmpP0qKWgv0kjL2SDOlgSbQbAry2BErwa5J5z3aKHlaBbRwgQidFr2LAgC9aa4A9BbmW0Tkc63w17+6qYnWAfqcsPwO0AW2gVKAaOmJCXTBwKRSf5TUgwi9RKVt0izx22Ewd1WrgDpA36ZdaM9FCxeI0LW31AfVbwkY5kwE6HMKbfd7id92HWKfq8rCIXiAPicovxOhC2wDJQ8iTY4T6AIi9MgpGofcN4nK//Zfapp/89/WNeHCaB2gr5P5KlcToQvzNECX5ZBSf5RYTYReotI2aeb8BtCP0XmbUsilVAGAXqrUQenmHkTODC2OO0jW6mJK/VFSAEAvUWmbNFN+2wTmxswtInSTT0GUToS+TbvQnosWLjCHrr2lPqh+AH2/Ve7mzP33338/6dnv/M7vbF577bVqr+f8tgrmBuBLP6VD8jNQXwP0R+i8VCbSb6MAQN9Gx81yKQWIFsdtJtxOGZX6o6R4IvShSuagnP/44f+UlG6tVim/VcPcgTyG81YRulNgAuprgH60ziX3Amn2UUALF4jQ92kfl88VoO8XoR8NmiqgT0EboE+eUXD5h8cDBADoDxB9qshSgGhxnDD5R+aU+qOkHmujzpIy9kiz1yr3I4G+OcyN0FsD3eSZidKJ0Pdo2fry1MIFInR9bVNEjQC6jgh9MdBLYF2SZmkrBuhLFSN9oABAF9YcSgGixXHC5CdCTzjk7BH6YpiXRt97AD0TpROhS39SyLBPCxeI0GW0J3VWlHawSirOkPtQpaOG3KuAXgJ1gM4cesmNf2AagH6g2CVFlQJEi+NKNHlkmlJ/lNgI0E8G9DmoA3SAXnLjH5hGCxcuF6Ef2EYo6uQKrO1IXHLIPfT5UdvWXJmJeXSG3E9+Ex5kPkA/SOjSYraMCEvLJJ1uBa4M9Orh9lSTKDlYpvQgmbkmF0EdoM8Jxu9GAYAurB0AdGEOUWAOQP/jfbyYGnLPRfNLLQDoSxUjPUCX1wYAujyfnN0igH4g0F1jWTu/DtDPfts9xH4i9IfIni8UoAtziAJzAPoDgG7azRqoA3QFd97xVQDox2s+WSJAF+YQBeYA9AcBfQ3UAbqCO+/4KgD04zUH6MI0124OQAfoR74ER/v9JLl+AF2Yd4jQhTlEgTkA/UFAZ8hdwd1zrioAdGH+AujCHKLAHID+AKCvgblpcwy5K7jzjq8CQD9ec4bchWmu3RyAfiDQ2bam/XYSXT+ALsw9ROjCHKLAnCsD3biv6nCZkkNkUm1ji4NlOClOwV33mCoA9Mfoni0VoAtziAJzAPoXmuav7xClrx1Wz7UtgK7grntMFQD6Y3QH6MJ012wOQAforHLXfIf7ugF0YX4mQhfmEAXmAHSADtAV3MgFVQDoBSIdmQSgH6n2sWX9wU//r+bly5fHFto0zUc+8pHme77ne6rLPfvb1qrn0ecU22PIPTHcbsxY83KW3/iN32i+9KUvJWuzZ9uYk4/ft1cAoG+v6aocAfoq+URfvDZSflTlAHpG+ZMAfc92w/NqT3WX5w3Ql2u26xXcILvK+9DMAfpQ/k9+8pPNkUPBVavdp1rM1kDPROdrI/Q9Gz3Pqz3VXZ43QF+u2a5XcIPsKu9DMwfoAH2yAQL0h96fGgoH6MK8CNCFOWRDcwD6Y4FuSt88St+qfUzAnAh9K5H15wPQhfkYoAtzyIbmAPTHA10k1GdgDtA3vAmVZwXQhTkYoAtzyIbmAHQZQBcF9QKYA/QNb0LlWQF0YQ4G6MIcsqE5AB2gj5oTQN/wDiMrgC6sDQB0YQ7Z0ByALgfofZRu/rHHsbBz7ebXv7pN8frvf/9cyvb3NfvQiwqoTMTzqlK4nS4D6DsJW5stN0itcvKvA+iygO6sOXyhXGFUHqoF0OXf3xIsBOgSvBDYANCFOWRDcwC6TKD30foRkXoFzInQN7wJlWcF0IU5GKALc8iG5gB0uUDvoW7+sQfYFw6xx82OCH3DG1FxVgBdmHMBujCHbGgOQJcN9MEQ/FZgXwlyZxNA3/BGVJwVQBfmXIAuzCEbmgPQzwH0EdjdFyWRewdwd0npore5ZgbQ5xTi93aU6a23VQhxu9/vdw01AegavJiuA0A/F9BjL7aL52Y+WwGcIfc5pfk9pQBAF9YuALowh2xoDkA/N9A3bAqLsyJCXyzZJS8A6MLcDtCFOQRzUECAAmuAPvVWOwFVw4QNFQDoG4q5RVYAfQsVyQMFdCkA0HX5c6/aAPS9lK3MF6BXCsdlKKBYAYCu2LkbVg2gbyjmFlkB9C1UJA8U0KUAQNflz71qA9D3UrYyX4BeKRyXoYBiBQC6YuduWDWAvqGYW2QF0LdQkTxQQJcCAF2XP/eqDUDfS9nKfAF6pXBchgKKFQDoip27YdUA+oZibpEVQN9CRfJAAV0KAHRd/tyrNgB9L2Ur8wXolcJxGQooVgCgK3buhlUD6BuKuUVWAH0LFckDBXQpANB1+XOv2gD0vZStzBegVwrHZSigWAGArti5G1YNoG8o5hZZAfQtVCQPFNClAEDX5c+9agPQ91K2Ml+AXikcl6GAYgUAumLnblg1gL6hmFtkBdC3UJE8UECXAgBdlz/3qg1A30vZynwBeqVwXIYCihUA6Iqdu2HVAPqGYm6RFUDfQkXyQAFdCgB0Xf7cqzYAfS9lK/MF6JXCcRkKKFYAoCt27oZVA+gbirlFVgB9CxXJAwV0KQDQdflzr9oA9L2UrcwXoFcKx2UooFgBgK7YuRtWDaBvKOYWWQH0LVQkDxTQpQBA1+XPvWoD0PdStjJfLUDX0rAq3SjuMi3tSpywBxkE0A8S+uTFaHnu3u73+/3kvmjN1/Lg1dKwNLQpTe1Kiz+W1gOgL1Xsmum1PHcB+kz7/Vcv3xml+Dtvv7Vbq9fSsHYT6OCMtXQUD5ZNTHEAXYwrRBui5bkL0AG66Bvt0cYB9Ed7gPJRYH8FAPr+Gi8qYasHbyoinzNky4hdS8Oa0+wsv2/Vrs5SX+xEgSsqoOW5S4TeNE0NxHONfi3ctTQsLQ8FgK7Fk9QDBfIKaHnuAnSAzn0+oQBAp3mggH4FALowH9c+eLeMzp0ka6J0LQ1LWPOoNqe2XVUXyIUogAKHK6DluXvZCH0PkMetsAbsWhrW4XfkTgUC9J2EJVsUEKSAlucuQN+xUQH0HcU9KOuzAP3W6WEOlbg39v+bz60xv9hfzfcunfn56Wb/eu5+denMn09BfqHU9z61zbn/mOKCL9zhFjaVLcHnWu48b73NJaxneS6kRIFpBQC6sBay9MFLhC7MgULNWdquhFbjtGbFQHcVMd+rOBHrtJ7RZThAF+bPpQ9egC7MgULNWdquhFbjdGYNIvHuMMv7zQ4D2N9u7SiCH4s4XRUxWJACAF2QM4wpSx68R8DcybN02F1LwxLWPKrNWdKuqgvhwmkFAqA/dZMB7YnV3ZTBVLSe+i3+zv1N1H/dhqjluXvJOfQU0L8+c6T9H3YPjbCpL0kL0M/9kADo0vznJuvvjYnYn+5utt8Pwefm2edADtCl+fo4ewD6cVoXlbTkwQvQiyQl0cKRHwQ7WoHndsjdRO1u6V24UM8s4DPLAG9tqm6g/t4098FqPhvou/58mJo5+qP9+bjyAPrjtE+WDNCFOUSJOUvalZIqn6saLavvLdKf+9X0eXiPK2fX/luQ23+ZD9H6uZrBWmsB+loFN75+yYM3FaF/Q2BP+EbZ1JD7krQMuW/s6IOzW9KuDjaN4joF7Hx6tyXPhN8tk6N9dLNqBQhvp+fdVr/8hVGgz6r7WY3lJgDownyz5MEL0IU5T7A5S9qV4GooN83E1i66Hg66DzbHJ1XwO+ZdJ8DutTer6O1gffxxJbguAEPz529eAF2YD0sevFOr25dE3UvSOplKI3UtDUtY86g2p6RdVWfOhRsqEEbkIWLjODouMpXWdRD8gjt3Vbsv/sle82y4387J27LtTL79APkNXXtAVlqeu5da5X7kdrW4DQL0A+7KHYoA6DuIumuWaUCnI/Uc+C3QzWK7MIXD+/PtuV1ZZ0/k8ygH4rs6dtfMAfqu8i7PvOTBC9CX63r1K0raVY1G7718Z3TZa2+/1X8X/h5+P1WWuyaVvvY3V96UPbnfzPc52+P6l9ZxWusYqSVL21Iz4WYe/lVzu7/oo213Yt3zvcN4O2c/PCDXIP45iNQBfM2d8ZhrAPpjdM+WWvLgBejCnHYCc0raVU015mAXw30KeDGsw79rfwvrVGtr7XU1etprctG535bWRtSjsyXi6yy03RB6f3694Xy/5y3c7Z47vqa+Jlx5rAIA/Vi9Z0srefBqBXr8gDKrfscPre6R1224Tf0eru7vV/kGB+6Y78I0oVOmyovzSuWdftAObY7TOFtS+Ye/xfXK1SHVyEra1WzjTCTYEna10J667oxA9+1+PJ9uD5a7N1/19KL5yrMDeLwgznUL/PY1p4NfABdH5rlRgLm5+5pWwzV7KQDQ91K2Mt+SB682oOfAGEuYAnEOzlPwnQJ6/+Bz232ijkAbP0WdibnOQfx7XL77O+xMuId6DPRSrWLtStpVTZOdGnKOYT8Ff1P2kUBPlZUbTdiy01KisfV92+3rkvtT5Yp3lptDZrpV7matm/m/+yCij1fRh8gvsZI0EhUA6MK8UvLgBejeaQC9rAGXtKuynKZThfCbA3rqd5O7A+uSIffUvH08vD9lz5LfpqL+uU5LicZpoMcxdklOrlMwtVo+7izwatdSZSWmA+jCvFLy4NUG9KmoOHRPLkIP08TRc+zeOPLNuT83VJ9KXxOh5+xyIwClw/ulzbekXZXmFQMt/DsFUff7HGBdupqFb6WL72oXxcXaxJ2OXB1rNB0eJhMCtyS3qQV0qah8mN7+xTB7idIS0wB0YV4pefBqBfoc2K8y5J6aN4+H5ZfMnxtdS9pVza0wt8p7DqBbrWTXDfTYMw7C4ZC8i8Zd2vzCOpsiAvnt3ty7hXJ+G5tbE1/TMrjmEQoA9EeoPlFmyYNXG9BTi+FSEpVE6CUR7txCuvZx98A59Cmgh52eJVAvaVfCbgXM6Y99LdnGljpZLo7ux9F7uwq+e6kLID9/kwPownxY8uAF6N5pOTjPLYqL3Z5auOY6B7mRg7kFalOr4FMr1s+4yl3Y7aPMnKkd4CGcczDvW276zLf7rXtDm32Fq43bicrP3IgAujDvXRHowlyg0pySdqWy4qet1NzceQro8xG5X/LmXskaCsTc+WmbS2c4QBfmwZIHr7YIXZgLVJpT0q5UVvy0lcpF5zHIS7e0WVi3Z8C1L06fmn8/rWiXNxygC2sCJQ9egC7MaScwp6RdnaAaFzFxLjpPDaVPHQxjNqK7UXc7qG5PeU+92e0iEiutJkAX5tiSBy9AF+a0E5hT0q5OUI2LmGijc3em0eiE12iF+rwoQ4ib9MyVz6t2xhQAXZjXSh68AF2Y005gTkm7OkE1LmKii54N1W/tgjX7ApW516QEw+qDxW1TB8tcRNKLVBOgC3N0yYMXoAtz2gnMKWlXJ6jGhUy0UA8/NqpODbfb7+635+Z2d29Oi4fgWfB2hcYD0IV5ueTBC9CFOe0E5pS0qxNU4yImGpC7eXT7MlM/TJ4Guhmef3ryw/TMj1+kqUTVBOjC/F7y4AXowpx2AnNK2tUJqnERE4dA7+LvYBGbxbsfgu/eHni7dwPz7CW/SEMZVROgC/N8yYMXoAtz2gnMKWlXJ6jGJUwcntjWVfl+b+5P9+b2/GRfnTb4WKC7N7RNneZ+CQEvXEmALsz5JQ/eEOh/5+23mvhvUyX3nfl96m/zWyqPXBkuvznZtDSsuXqe5feSdnWWumi3086Fm1q6+fAuRu+h7RSIonSzx5yT3rQ3j8n6aXnu3u5LDrYW7PKSBy9AF+xAoaaVtCuhpl/MLLsY7ql9f7kH+jBqT6129yvcn5qn2fXwFxP1MtUF6MJcXfLgnYu+XUQeRtPmmrV/u2i+RDItDaukrmdIU9KuzlAP3TZ2+8+be/OieeqWwrkam4VxqZXqqZPjzDVu77luxajdUAEtz91LRehnaMRaGtYZtC6xEaCXqPSINC7a9q9EtfPhQyC3R7b2w+lTx8J2x7t2aZlPf4RPH1emlucuQH9cG0qWrKVhCZO12hyAXi3dThemXqRiijLvJTev7zUz6LcgSh+DP0zrjcwdLrNTNchWlAJanrsAXVSzahotDUuYrNXmAPRq6Xa60J6j/txG3u4UOB9Pt/Po/Vz4vV0kZ9+nMoy5Lfzbt5oPtrHZroHNmyh9JxcKzFbLcxegC2tcWhqWMFmrzQHo1dLtdKGL0MO5cf8GNHswjINxCugO0y6f0Mzxm9SA+k5uFJatlucuQKdhCVNAljkAXZY/rDV27brFr59DdwfG2LjbnBQXn8Xuh+vtinh73vtw0RxD7xI9vrdNAH1vhRfmr+XBq6VhLXSf2ORa2pVYgasMS70mdRylN+1Muvt+CH4XydtOQBrq7nui9ConneoiLc9dInRhzU5LwxIma7U5AL1auh0vtAC2UI6j9HC1egjqFNDj7WyJrWxtFmxl29GZIrLW8twF6CKakzdCS8MSJmu1OQC9WrqdL4wj6zlgx0PzwwVx4xn14Xy6QfrcS1h3rjDZ76iAlucuQN+xkdRkraVh1dRd4jUAXaJXjE1hlB5H1vZ3/+a0OIq3nQGzeM7PxjP0LtXTR9il5bkL0I9oLZSBAiiwgwKpKN0X4w+VcUB3HQGL+3B729C49ClyROk7uFBIlgBdiCOcGURSwhyCOSiwuwLxFrbUEa/hArrcfnUP+twCOfam7+7MhxYA0B8q/7hwgC7MIZiDAoco4M5xd6ewx4Wmt64ZQA9fxpJaOe/y8h0BovRDnHp4IQD9cMmnCwTowhyCOShwmALh0LuP0v0ceuLc9/Y0OPMJX7UaL3sbDr27KP2walHQYQoA9MOkLisIoJfpRCoU0KdA6nx3t9zNrE6P33fuwT2MuInS9bWNshoB9DKdDksF0A+TmoJQQKgCHajN4e23cD+6MTd1oly4MM5VaTpKt6vj2cImtAFUmwXQq6Xb50KAvo+u5IoC51XAwTl9wMx4Ht3UdDpKZ9j9vK1hynKALsyvAF2YQzAHBUQo4CLq/Dy63Y8efqaidJcutaJeRIUxokIBgF4h2p6XAPQ91SVvFDirAjbiNi9ctQfFDofe7eB5Cui5E9zt9wy7n7U9pO0G6ML8CdCFOQRzUECEAt0Q+u25ezF67iUtcYQeA334N0AX4dzNjADom0m5TUYAfRsdyeVcCvAmsBJ/PbevSb3d3Ra14ZvZhvvRXX75YXf/praSsklzBgUAujAvAXRhDsGcXRVIH066a5GnzdxuYLNntw9PgnPnvYd70UOgp7tLbn87Ufppm8TIcIAuzJcAXZhDMGc3BcLlWP7lIn52eLeCT5ix0er59tzczFa29hO+Re3W3NvfSoE+dXTsCcXB5F4BgC6sMQB0YQ7BnJ0VMHuo7VIvt+zL4YrXfHrpW6CPDpYxv1uV7FvV64BuI34+GhQA6MK8CNCFOQRz9lPgfveBZrdq20GdOfUhzM2+cbsmPRGd3+/Nrf0htwVtbh7d5MshM/s19ONyBujHaV1UEkAvkolEKhSIzy53LyhJnXymosKLK9Ei+m4Ww7kujoumUwfHTAF9evtavjOw2GQueKACAP2B4qeKBujCHII5+ynQMik8KKWlV/s/G4ty6IkNyF81TWJl+9gx00Afv1c9foPbfq4m52MUAOjH6FxcCkAvloqEp1Yg/TpQNyfMlirrXKuDPTRmeJxreDBM3ClKNYy73b8+6EAB9FPfQgnjAbowjwJ0YQ7BnJ0UiI8wbUPRHlq5s8bDtd07GSYs2+emnSBvV7cPh9kNm/tF77OjGWb/epg+qGabLaMhwhxfZQ5Ar5Jtv4sA+n7akrMkBVJr2GNoecjEuLnOCvjnrqMzPhnOHiRTEp3bWD89RF96vaS2gy05BQC6sLahBehaGpaw5lFtjrh2NZg/d9WK42+3qrsLIO9m65bf3naFaD37DvTbvbl34blXaap5TAOdVe7Vt5aoC7U8d2/3u9kDc/6PuAdvpaRaGlZl9cVdJqtdpYZ/x0PK9mUk9tPOJZuRZzNs3L2mRD/Q89MS7Y6/dj7c7EkW9NEAACAASURBVOJPx9/DRkiELu6m3MEgLc9dgL5D41iTpZaGtUYDSddKA3pemxDs3az6/W7PMG9/covDbA4O+Sp68yNRJl6Z2u3htzgH6JLutUfaouW5C9ALWtF7L9+ZTfXa22/NpilJoKVhldT1DGmkAT27QGvwWlCzKLt7XWg/iR4vnNN1KEoYbZtRiRftKXruM4zY3a59c9LbfJQ+NSpiu0Z8zq+AlucuQM+0xRKI55rxGrhraVjnv8VtDaQBfTpCdzGn2bDVnV+eALrfs36+o0uH4xBWjdSOfDu3HR8iE05E2CjeHp879ckDnS2CWu7yptHy3AXoUZtMgTxcZvDpb/iG/opPfPnL/b9vie0rNWDX0rC03OqigG4ngBMDxeNV7sPDUFwkGQLO7dE+h6fCef9BTNyeBmffpTZYNxAcyWq/H681sKe4mxXv00Af/xr6gAj9HC1o2kotz12A3vk5BnlurWAO6GFzieG+BOxaGpaGm1xchO54nEVQMKzen1PuPBFv3wqXzsn1lrE6xKedSOiE6PWII3U7QpGuYTz1MDfszip3ua1jO8u0PHcvD/RSkLumUwL0/hEaRe0lYNfSsLa71R6bk5wIPbVyO4w83b8d3Az4Bt3MwQErbsX7Y9XNlx7abmwdjzhMWR7E6mZQ4yk8GcYD3Qy2P3fD7vkonVXuUtvIlnZpee5eGugG5h/72Mea999/vyndvbcE6CHYXTlzUNfSsLa82R6Zlzygx2rc2rZrR4XCkDUGURyhlww3H698DPLhGfXhaEPKtnBKwdbXnW8/Pue+Wxp3uzcv7k8T8+gA/fhWcHyJWp67lwV6DcxNM6sBehs73W5952EK6loa1vG35D4lygG6rd+tOzbi2WxH688pjyJxd7SEOUQlmFvucugSz21YM3PSZoB734VzY4D709fdmMP0orXY72G9gpn3lu/jTo8/7328hc11CcYty3cWzBy8+cTL7/ZpjeS6lwJanruXBHotzNcAvRTqWhrWXjfe0flKAnqLkX7kPYcQN39s557NISr3/o1jDpFzUWen8tO9uT0PF42lluTV+iR8oanJo+0+tHvng5XrbYFLOhVxlN4drhPNtzubjZ5PYXmdNP6s9+G4h+8U2YThfv4ttanVlOvqFNDy3L0c0NfAfC3QS6CupWHV3VbyrgLo0oA+N7Lg2tBwXbwdqxh/3Jnu7a781EtYBm9aC/N2R+910x3P3UvZ/JK9mdXz8tr6lS3S8ty9JNDbIbLKE29rh9zDm8Wtgk8NvWtpWFoeDpKA7uaEbWQYx7de8T6Iv5klX2aOPUTZePvWMOq0aS3o7Dx7H80WOrUkUjW52qH0NKD7qYUnM80Q2z+8rs0hyMauRQ2Bbn9vT83b5CCYbsjdRPdP7mz44XB+iQaFcpLsAAW0PHcvBXS3or0W5ltE6H0fv1sBH0NdS8M64B48pAhpQO/f+9nyKr3wzcGwHbq+37o3fKbjUy/iGEFu23v/KnA3Dd1BcRgD25ym5pLj+fL8yvXQlgjM0Vr9F03TvGpfV56DfjxvvrTZFE5PtNkO7QbqS7V+XHotz12AvrANbRGht7c+QF+o/GOSA3QDS6+95aaF53FA7wzoCGkmAczYQ/oTdxv6LnRlAyoFui/XdVR4E1ul5A+4DKA/QPSpIucevFtE51tG6Dmoa2lYwppHtTlz7ao646oL45VdQ9i4oN0Hq92kcOIUQx9P5w3xW75sGnM+utsTbr+xw/Ht/zd7vU30/BweuWpTOdSZ/5qFb226dk47t9gtPQzfH5SXNTmGeRwj5zoBU84oAfq43LuZ7li0mK+qQXDRRgpoee5eJkIH6Bu1/ItlIwroA6IltmfdnpvmOYDLYC45dFzpwjJ3TRyLuxXedvz79sIM7Xeby+4v+uNa3NXtKvL2nex2Tt9C3nYOxp9+BUDw01J7XTei5rqSBp6aErDdlHjYvSQ30jxeAYD+eB8MLJh68G4Fc1PgVkPu/aMyGnrX0rCENY9qc/YGeurdAW5dRfjba2+/2a3CHs8puxjYpf9Hv/qP2/r+1y//t/a/w3zMWwEt6N57+dlel9zZCLlrw+uNbW61+OdfvtuYvKyVfmvXu9EbC315Oejmhs5DV+ZmqfOz1/0WfYffmqA9miu3FgUwb/9ZlXF1O+XCdQpoee5eIkIH6Osa+5WvBuj+1cEWwh7ArkOwP9BzLTC9KDBMHcbMhrHZ99ssauRxhD6MzJk7XySmiMQAXYQbvBFE6MIcosScI4Ceio5NJzT83v79ZkLVWx+Bv/YZ+/t7H7eRt0lvoBteZ/9+q71mnL8Btv/EaeLI3uZry/+Bt99oL3QRus3FbUy7NZ9/+dnm9bff6rsDU/XxEa+zpTTaDYfrLWTbkYP2n/bvYU6pfOMh/9I5dFuOOcjHvxam1G4lN8uJqwHQhTkv9+DdKzp31Q9foVorSbjiXUvDqtVC2nVHAD2sczjcHgP39c+82e9a89cMgW4WxBl4bgn0eNjd/e0i9rDT4DoM4flqZpvo5z+e6ljYDoH/JNYFDM6nL20d8Vx2uJhwCrKl8/eJIf1u65xb8mctBeilHnt0Oi3PXfVD7lsB3cydp+Cd+35JAwXoS9Q6Ni1At9G8+7jo3nYY7BD8/kBP+XwOluNoPThQNsowBXKTZGp+P7zGvrMNmB97b25ZGkDfUs0N8tozQp+D9tzvc9VbA/T43euurPDwHJcm9V372OpWCk3lFf8WXxMf1hOXaf6eOtCnph5xnu7v3Pdzfkj9vjfQ4zLdMPfckLuHqI/Qf+Azb7bnG/gI2g25+6H0Yf5+CD8emjd2+WjbWhle60YA3JD7cGRhODVgmpeP0C2Iff3iqLx2ZXoO8INZ9AlIL2kdwwjdHhmbW/m+JF/SPkoBgP4o5TPlzgH9X37916+yeGpoPVz5XlPI3/3DP2wvMw/FNQ0rBbIY2O5VmzHc5zoAJp8pUIYwLSlzSqcl9Qg7MDHQUx2Zpf7ZG+jxKvd4mN3ZG8+fh0BPz227Kz3gXRtLr3JPzc/HK+FdGg/lqTxD28MV9b4z0LaqBZAdp+1XrQeL/9vFb6OhfHftkvJyrSXOA5gvva+kpV/z3JVUl8sMuV8Z6GGDSwE9bpA5EJZEvqlIu6TM0IYc0KfqEdq8BchdWXsDvfxhUDL8O/JkAMuSbWD+bW5lu65yW+hKouzlW8587ey1BuZf84Gvar7mA1/bfPmP/8h2DdqFaYPW1P0Rz6uXKz9O6cs3hfri5qYB1pTJtXsqAND3VLcibyL0dAQdS1kCV4DuVQPoUzfjUUBfG13Hdm6Rn915b4/HMfkB84rHtphLALoYV1hD5oC+9oUsc0Pua1a7r5lDL4lsw/lubUPuqamD3PB/TZOVD/Q4Gg1XdMf/NmnzQ87xCwj9W8viBWZLIT+nfKpjMDXaMJdf7vewnDVQDzUE6LXekHQdQJfkjZ2Bbqp6hlXupXPP7WM9GJecW9AWpzd/5xbF5RbD5YbiU82otB4aF8Xlb6t7+n3dgwtSc7utt4JU+aFudwr72IZS4LqySuepS4bdS/MqfSDNQT2OtPP70NsOUDvkTnReqr7UdABdmGf2jNBdVVOL39ZE5i7frSJ0YS5RYY6kCN2upp6LjnN7ufvWllmEZs83S89+SwZ6CUzjWi2B+tzBMiXlq7gVVFcCoAtzLyfFCXOIEnPOAfTUHHEcmc8BPTlWsmAFenh9OJwdRu1hmlTHw/1esqiuHTda0MqWQr1bk/JsVtnFxYSjBktsWGAuSQ9VAKAfKvd8YQB9XiNSLFfgsUA3m6/iuDk1rJ6CVXqYPZxVb1HbfnFvXjRPzfM9iNBvtmz7mRr2noreSyL7qY7GmnnuUl/HcO6OhwnkG6/499dwbnupzrLTAXRh/gHowhyixJxHA30s4xTQhwAMl2u588XtSePBvK8bw78ZmFuK2feUz827l8A61RGo6ZAc1Zi8fu0aka5TM57qIEI/yiNHlQPQj1K6sJy5B++WR8A6k9bOn4dz5y5PLQ2r0G3ik821qyMq0EP4fmvn0IeDvBEg2z8zUWfwOlP75jGbzvzPRuNhzrYLMPykhtJtCvsSlFuwBzwF/LRdvoypkYCc0rGNpcP1nd2DfetDm02dnroOz3DtQqib6fzwObsCWp676g+WcQ0NoJ/9lnuM/RKA7mreAqa5N88dXfqh4GC1tdsd7UDto/FcNyDckmZLsrgKgR6CLh+Z+0NdSqL30J+lII/h7ZVJdzxSbSa0zQ2vm47IcHTDXZleiAjQH3M37lcqQN9P26qcSx68W0A9XOm+JkJPReem4loaVpUTBV5U0q6ONtvjzw4LN8/2eBNLYxNt10eNDnfPzXNz64cD8pG5m0P2Q/mu7Ll5fd91mNcvBrmdNnDDFePT4eLOQvi3s6uDeT8O4coIht0HGuQ6ILlOxnytSCFHAS3P3ctE6KbpAHQ5N9BZLJEI9L2087FrN5c+eOnIOOr2C8LiKH9u2HsYkbdAdnyeurSf2o+GxvsRiriTENncLQB0EwzpNQPDSN1PRwD0vdqdhHwBugQvBDaUPnjXQn2LCD0XnROhC2tUEwcWybN0nUUxzLtwv1scVwLzVMQdD6XHc+htmN2dvx6vv0/Up0viQWzyC4e/OztNR8T8kukA+JxDeLsphtTQe0k91unP1Y9VAKA/Vv9R6UuA/rGPfax5//33J1/nmaveWqAbmLvywzdrufK0NCxhzaPanNJ2VV2AgAvHME+D15lqB/jNbL4Pp90Wt+HRNLm58USEXvY2mEm1+nq4kL9LPe4AhNm4jkQ4yuC/sxH6eNW/mWIYLyQU4ExMqFJAy3P3UkPuztMmSq+F+hqgz8GcCL3qXtz1Iu1AH8yZ9yvdw2h5PAY+HIaegraNwN0nH4PbDsLjPvGK/mBEol0VN7WNjzn0x/ltu5IB+nZabpLT0gdvLdRrgV4Cc4C+SVPYNJOl7WrTwnfOzKM4FZ0OYRweLjNeze6RPT/nbNLmhrp3rnA2++d2lbvbxue36xld4m6IBTgR+qN8tU+5AH0fXatzrXnw1kC9BuilMAfo1e7f7cKadrWbMRtn7GafbbYhZFMw999ZxOUi09TpdinDu/J6Xj4w0r296qLw8fz5uIMSztk/clRh48Zw8ewAurAGUPvgdYvk+hgjfodkVM8lQI/fMJaaM49l1NKwhDWPanNq21V1gQdd2GKph+nUKvV4QZy7aAnQc8PyYWUfA3RT6nM7T+4+w/qOpwlS6wsOchrF7KaAlufuJefQU62iFOwlQK8BubNJS8Pa7c47OGONQB9G5mmQ+ZHmuTny2CHxIrMw+nf/7noT/WEuwfcH+9cCPbXAza6+t69HjTsecQfoYKMpbnMFtDx3AXrUNGKwm5/D94XngJ5633dJRE6Evvm9uWmG2oA+XtEewrQD1WAYPJ5fjzsAKbn9cbJ+eD69itxe/ejoPB51sH+b+96fIOfrzfz5preYiMwAugg3eCO2fvCmwF5a5RqQE6GXqntsuq3b1bHWj0sbR+fxkLo5bO7e3Ge3kc1D2B0f26YMxq7zB9Icp4616TmoZ6hDp1J7zns8QmE27bFl7ThPHVMSQD9G5+JS9nzwlsB9DcTDSmppWMWOE55wz3Z1dNU9zPML4O7m6Njhm0gSr0+dh3lYN78PPLVxbVleW2jmNqH5PfOhDW6Uwoy1p6cPAPoWXpCVh5bnLkPustoVZ7kL84cmoI9fiepCZyt6bkW3+W34kpI6CPtY93Fz0PMwt++ftyxny5qw23E3cwD6btLWZazlwaulYdV5Ud5VWtpVOjoPD31xK70drH0UbxHnPnUwf7RnwwF1H2GHB9qkovHYala4P9qPe5Wv5blLhL5XC6nMV0vDqqy+uMs0AN3OF5t58RjKHtPhwTHeCakV7ucDergQcHiU63je3I5EdFF68lCZx40uiLs5FBmk5bkL0IU1Si0NS5is1eZoALodT4/3YIUwbwfVg2NaUyB3aaqlfMiFZojdvmw1BeLUCn43lsEZ7g9x2IMK1fLcBegPakAUiwKHKeAOS+pXrgcD6Dez3zo8q3wIc7si/XxRaXqIPV78Fs7qhwDvzm/vl+eHIxvn0+KwdnbiggC6MOepiaSE6Yo5GhSY2kueHna3qDvnnHG48M0PsYd+HM+Xj7bSmfmJBNBZ4a7hfhjXAaAL8ytAF+YQzBGjgIWQO3c8HmpPmylhr3iNgK4LkgZvumMzhnnqrJsw5j/n9EONnle5BqAL8zRAF+YQzBGkQHo/9XAbW3j4y/ngtXSI3SPZvt19MMcevU/dOvKcoxWCGqFoUwC6MPcAdGEOwRwhCoR7qadXtY+j8nNEoulV7BND7P36wATM+1ej8g50IQ34EDMA+iEylxcC0Mu1IuWVFOheZzqIOuMI3L4OdRSpnkCmEOY+knaGFw6x9xG4jcPty1pSQDfr5U2K823dO4ErH2oiQH+o/OPCAbowh2COEAVSc+Zua1bTYet8Q+zDQfDclrTx9rvZjkvf8YmvZchdSIPexQyAvous9ZkC9HrtuPIKCvg3oIXTwW5bmj/2VX706WJkEy/nF78FAG5fsuJW7Rtfp7eeuW7OcKteOO3AljWtdwpAF+ZZgC7MIZgjUoF4vtni6jzDyEu3pNn6xtMJIaRDN01t72O4XWSD3sgogL6RkFtlA9C3UpJ8UECeAq7LYZBrxxrcQTDO1tL58vQIhJ+EGA7m29xNeG/C/FxHQJ5eWLRMAYC+TK/dUwP03SWmABQ4XIEQvxbmqXeRR9vy+oX9S9YGxMPpuf368qckDneSggIBujAnAnRhDsEcFFipgB9eNxmlonL7ffximaXb72aj8+Bdc6xwX+lUoZcDdGGOAejCHII5KFCpQBrk8Tz/uiH2oWlE55WuUnMZQBfmSoAuzCGYgwIVCowXvZWB3BQ1uyUtYQ/ReYWTFF4C0IU5FaALcwjmoMACBcq2ollsD46srZovDw0jOl/gJrVJAbow1wJ0YQ7BHBQoUCAE+ZJ5cpf10vny0KR+37l5W2r7atmpo3FZDFfgztMmAejCXAfQhTkEc1BgQgGHR4dRc9yqjb/dW+HMX/l94XZn+brjam/3e2PekuoXurGy/aqNFqAL8zxAF+YQzEGBjALhrPirAOTDGDnzhribCafr5stH0XnRMa8h8HGpVgUAujDPAnRhDsEcFJgA+nNzb57aXeWFUXlP+3hveS1wmTungXoFALqw1gDQhTkEc1AgoUDL5TbKdhF46vCX4dC3ZflWIHfL6iaG8/uh+NrOAq4/mwIAXZjHtABdS8MS1jyqzdHSrqoF2PhCuy3Nxuf+UBhTyHj+etk57EsMzUfnw2F/FsItUfXMabU8d2/3e9tVPv1n6sEbr1+VXFktDUuyxktsA+hL1MqndfPmr1pw23e0Dxejje/Smn3lc9ay73xOoWv+ruW5qxro7hEB0K95k25Raw1AD9v/I+4FX6aJzM072M3sefiJh9hL345W42HmzmtU034NQBfm4fjB6/r/5vY1jxEbEQyPpQgH1KQMU2hpWMKaR7U5ZwZ62KF1g9pHA93dYxblqcjc3pVmoPDpyUytp+bUq903uLCPztvp+1iJW+O3sbknxTblkot8BbQ8d9VG6GaGzgzvPbV7VfvjpPqBPgfweGlM6QNvr86AloYl/xYus/CsQG87tLemeW4buLsP7NEtbqi7tK2XKZVOZRfBvWqa+4vEnHlggdmKdrP3qn01ajgkv8aC1EhAaoFdmI65860UP0s+Wp67qoBubsNPffqds7ShgZ1//xNvtX9raVindELC6LMCXYL+rlPt8RiCdNidsB1r29lwo2lxHeYwPFXnrefOj+gMSfDhVWzQ8txVA/Q33nnZtj0D9G/5pz85aIe/9w8/WdUu43ziTLbK1+QD0KtctPtFAL1eYgM9ewJcDGk/weXgbXekm094Upz9Zg7k8dSCs3g4+tae7xq8/TSYZLvdm9vzrfs5HZ3nNrml1AntkTKVV+/Fa1wJ0IX5uQX603PzqV94dwR0YaaOzAHocj0E0Ot9c789N2bL+RDSHnF+aL09+q2Favzpp9QDSj+189+W9Kk9OuPZcTP1YMsYvdyle9mLPYAuWFdv7Lndgzn9rsiuX+AMcN2PcAoPoNe3mUddCdAfpXym3DfeeaOdKfznn/4sQBfmmzObA9DXeM9F3emot4WogXK35j031P50uzfPd3PKu108ZxYHODYbUFu+27n3p/utHxOwlnfntbc9g3Gs79cU3JunlsTDVfDtgTbO0H4tnS3fjD64s+ddzu2IhKF8W3U/MmHT+w9D9mva1fbXAvTtNV2V4+uffaNp7gB9lYhcPFIAoNc3CjOMnoN0fa5ukavDsxtK70Dc/sd3INpT6ZJ7XFw6u/7e4XfeLh9/327PbUfDdxNsp8Cs1Df/s8fn2F8d0Ine5xV+RAqA/gjVJ8p8/R2zqOzW/POCRXFbzbGnzAnzXjLHzhy6sAbVmXN2oLuFaRYqIdy6b25d3Hi3g8dz89XHeyk9C+1Ojk3H/ikrd6qZAfhz09ye7jaQDzoTVs/n5tmMKARzBz7g97Bnrv34lhWWCNAfq38ikgLowlyiwpwzAj1EVwuUzhMvmqc2anSjynaoOowgxxHl0U60EewSvM0hPbec7bhBb1efMHo/evvg0X48W3kAXZjH3IOXCF2YY05uztmAHuLNjj6bPeDu23Ae2fwYgtPMFZu0NlI3Q8XhnO/ebgwt9IPgYalz4M5Z6Pfdp86L37te+fzDDoUdtl/SjXmc3TpLBujC/ArQhTlEiTlnArpbcR2+Yzxc0DV8GUoMdBut9wvU+tPUrCP3Ak7bxQgy9y9u2aIBlUbntZ2FJTYOF9u5K906A7++fkmepN1KAYC+lZIb5QPQNxKSbAYKnAHo4dYpM8DutnXZY1TjuG8YGca/x8Pxo4h/g/bRQ9xTrXk2owNtH8P8Ot6Lni62NKaNdYg12KBS2SyiUZA+nQe8hXppnfe09bp5A3RhvmdRnDCHKDFHOtCHwLXx9YuODc/tvu4poKec5NLb9dl+PnvdsHBrUjC570pph9fb0QD7TfrI1xy4Sxa6TcE8NGrrBjsP8tGUx9YmkF+xAgC9WKpjElqgN+0+9LkPq9znFOJ3p4B0oHdhbfsfF5m3h6h0eBxDYy6qzcfkS4eF+5gzYqoZRehfwmJOaRuc4JbrhJgKzdkeA9rNn8dwLclnq3sg7nRM1W+rMslnqQIAfaliO6cH6DsLfNHs9wD6ey/H7xt47W3bITWf8Pfw+7QL7s17L30n9rW33wyS3TJ5WaANr3PlD+E3ZUvuN/P9G599s2m6k99Mjgbin3/57qgK+fotgW5qDnxNp2aLxl4G8r3WJmxRgyvlAdCFeTsE+twZ7MJMbzj6VZpHvD17AT0HMgPDGO5TUDfpX3/n9dbg+/2phbSDuv+3hYvN+80e5CH847QmGg7zsop4cKbsfKMbJXv3rXea199+y2+YayPwew/0+fqNYd4dEDforEy3mlSHIDVEXzJsv6R9loHcT2bs9Wa5JTaTFqALawMAXZhDlJgD0IcRfziKEIN5a6APF+i5kqdWpM+tF5iLh+d+zzXqXEchZ4+B+FM3gZBe/a7k9jlNNQC6MFcB9KFDzLul3ac9/7rdHmS/C/82/47Txun6R2kizzDfsLwwz7DMVF6h5XO2htc72+P6bNk09wJ6aONUxBpHwnHdXNTtQbs2QvclmAjdRNrm87lumsDZ2g6ru2mCW9O4qNyl/YHPvNmdsd62kDYPN0Sfru/cEHsO5KnFZ3sPt+ei+lyHwH/fnjffwjy3aG7L1ktepQoA9FKlDkqXA/qS41dTpuaG79fkm1qUt/XRrwaoOXCnABinz0F47toYuLm/p75fYrtLG16zZZPbA+hpKFtwzg25u98dUgxoDTxtx8rOiw+H3Idz8wamHqw++rbXmbQehiG0zSEz4d/vhkBvmsb87ez4/MddXsOaDoHu5/GtvbnFeFPz476zkF4wN7fCfa6lzA3Hz0X0qd/dQr3Q9jk7+H1vBQD63govzB+gDwVbEqFbGAw7AEuB7kqPI+z4+9itKRCnovsQ3GGeudGEhc0nm3wPoMeL4uI58rmFaCF4zeCLAaj7TC+KC4fP40Vxw9/sS8ZufWRu8m8XurnP862FuPv8wNtvtJGn+bhoPhQ17EjE34cw9nPlMcinIvEwx6kouW3VE01jaeSdymq6E2BfGcue863uz63yAehbKblRPgB9DHQJEfoccHNAT9nuvgs7IBoi9LpbwILpz/6ZP9v8z//7PzNZzEWQ0yW3JXT8sy8fsa93ab9uA00fzaffqpaaQ3ZlDn/72j/9mvaHP/fN39L81//9PwLD4jnmuaH5XJ2mtovNzb1P6TTV8ZgCfp3XuWofBQD6PrpW52qAbm6tT0XvQ18zNG6MYcjduyQ1FB7CNXReHFGHME6ly32XA3b8/ZmH3KsbvWHq071921f6Uw/0FthP9v3iDuAt9jqIP7VvLc1FmqmFXh6a7k1pJjtzfI2pw7OrRHuWfGj3VNQc9DhmRZzSokSnucV44VD62Jh2zrwb9RhOL8waToIDFADoB4i8pAizeMc81D/16XcGEL4y0J1+MUxTi+PCtPGQt32gjxfPuWumhsjDNCmobxWh5zoWS9pQKu0eQ+5rbbL4cRQ3AMxFrSWgWmLNHNR6b3f/CCDeTuvYv+2iMGv/s/kqeLWo9WP7/0d5eEtr6rX0mtK6OlsnRg5a1qc6KUu0J+2eCgD0PdWtyPuNd162jzXztrXad5Knij1rhF4hIZckFJAGdIfvewfH9l3cWfZMQWnO3akoO74mt1Lbft+/6MXMGj81zauW3mOAt5HrIOul8E3VZemCuJRWM8P700F5b5Q/Ya9E0zm/8PseCgD0PVRdkWf4chaAvkJILh0oIAHoIWp6Sh4otQAAH8dJREFUVLq57Vs3nx1sS6x34dSct8t1GuIu1YvbU3N/6qJw81bW6LLhnHuc59J58umFaNamkgi5fC59fNBNrLotD5jXt8YjrwToR6pdUBZALxCJJIsVkAD04WtPp2FnQWNhMlwxHoaTKSjnQL1Ysu6CufziaHUJxKfm1UumH7aalw+1cR0G81/z9jSzgDAuZ82ISa0fuK5EAYBeotKBaXJAP9CE6qI4+rVaut0vfDzQp0BsoT3AymD+ORVVxxAyf9cMBZcCOIZYCcjjDknOzWuH5ksi+/Ky7ZY0k6d7M535hqNdd79JNygAoG8g4pZZAPQt1SQvp8DjgD4FPhP9da8dXTTUXgrhrfyfAmbtYrdSeFfMhW9V3eRCPqLyzeTdMSOAvqO4NVkD9BrVuGZOgccAPYR5AGEzX25A3r3FbLgaPFeT/PD6/DzwnDqp31PgnYN4OEqwtMw9AF4aufuyuzGFwHhAvtSTj0wP0B+pfqLsEOilpm25eM6UuSa/rY9+LdWAdNMKPAbozqZuyNaQ23C5C8rtn7kTx9wQfcnQdg7IKcCWQi7Ocyngp/yRg2TtyENtZyC8zi18q5m24O6TogBAl+KJzg6ALswhSsx5LNCtiC76c6ezDY8Oza1M79bDd3xvEdS+xtSuPL/fb9F2t9yQ9vxQdxvpO0Mn/Z7rFExBP5VhV+d50yYi5qlOwJQ9Rj/bwbIdq7hOROZnvPUBujCvAXRhDlFijgSgj6WcX6Vu3+rlKfvsQvwuM7dtzC/ayjssOzSfDUoXkTYqOAaiB6jlqEeo270+rIMHbL+cMBy0aE2z72cfbmUbmjF4dWu4LjEEePc9C9/Of7MDdGE+BOjCHKLEHHlAz61670ndnbzWRejBKXLjl4IEq7A7DroTWF3EnYN5+vtcZNsdMeMCa7d33nE1bCsui57rQQQcANR2Vexvo05JL1HqWj/qMWiiycGDuY4J0biS27wB6MI8CdCFOUSJOVKA3qJlQNHhYrluHL2LOmMQTYPHp/aA9+/rNthM5Tc3b92VeTMvdXFHrHRH1nZR8iDwde0l+aX7MV+P3sJ+/D8GfUhsm9p1BMxf7gAYV+901K+kUVONkQIAXVijAOjCHKLEHBlAd/BxL0PxMG3ncduz0H1EbiPxSTIWemdmNCCZSxd2m7P/k4er+GmAQiN2StZp2B3EYxVmz/hOYovPFqALcxFAF+YQJebIALoPX/u5XfcO+37QeQ8YpeaZc8PQDuQhsMNOho2DpX3CrlBmMF6aydizgwIAfQdR12QJ0Neox7U5BWQB3SPHgMgsdEuvfN/Cn66D4F+y4kuPVsPZMfXBueVEu1v4gDyOUgCgH6V0YTkcLFMoFMkWKSAR6IsqUJ04NUcerRzrg/LUijJ50Xi1FFyoXgGALszFAF2YQ5SYc12gOweGc8vdzHx2D3Y43K6kAVCNSygA0IW5eQro5uUnNZ/cu9BdXlvly8tZarxzzDUA3Q+0m3/Fq8AZWj+mHVLKvgoA9H31XZx7zRz64kJ2vICjX3cUd0XWAH2FeFyKAidRAKALc5SWB6+WhiWseVSbo6VdVQvAhShwAQW0PHdv9/a0ivN/tDx4tTSs87coWwMt7UqLP6gHCuyhgJbnLkDfo3WsyFNLw1ohgahLAbood2AMCuyigJbnLkDfpXnUZ6qlYdUrIOtKgC7LH1iDAnsooOW5C9D3aB0r8tTSsFZIIOpSgC7KHRiDArsooOW5C9B3aR71mWppWPUKyLoSoMvyB9agwB4KaHnuAvQ9Wgd5ogAKoAAKnEYBgC7MVURSwhyCOSiAAihwEgUAujBHAXRhDsEcFEABFDiJAgBdmKMAujCHYA4KoAAKnEQBgC7MUQBdmEMwBwVQAAVOogBAF+YogC7MIZiDAjsoYF7KquJoyx20Ict6BQB6vXa7XAnQd5GVTFFAhAIe5AbnvGtdhFMUGQHQhTkToAtzCOagwEYKWJg7kHugE61vJDDZNABdWCMA6MIcgjkosJkCdzvOfgsj83tza24Mv2+m8bUzAujC/A/QhTkEc1BgpQIO3yY6N/+7NU9BjnYmHaivFJnLWwUAurCGANCFOQRzUGCFAtMwtxnfm+fmqXkiSl+hM5daBQC6sJYA0IU5BHNQYIUCBujPzXMbgecXwbFAboXEXBooANCFNQeALswhmIMClQp4mJsh9iloA/RKibksUgCgC2sSAF2YQzAHBSoUaIfa7/fmPtiZFq9nt8hvBnPqFYVxCQp0CgB0YU0BoAtzCOagwEIFhnvNzcXumxTQ3fEy7ElfKDPJEwoAdGHNAqALcwjmoMBCBfx+8/BCvzyuRfzNBPAh4AH6QplJDtDltwGALt9HWChPgSEuH2dfi+j7vdtrHs6NDw969dvUUnvTH2c/JZ9bASJ0Yf4D6MIcgjmiFXBrx81MdDi4/Qijx5H5uJthEd9F5j3vic4f4S+NZQJ0YV4F6MIcgjliFXDHsxguPrcHtlikPyJaH8+bj7sXNxOMt6fEtf/oD4zzx8H62XaxomOYaAUAujD3AHRhDsEckQqEB7YYgt/u7vjUx5yR7ofanVxxt8KeEGfhbda139pOiN/ORpQusqGdzCiALsxhWoCupWEJax7V5mhpVwMB2rlq842DuJ2Z9sPax7yi1A+1h6vZnV3OYmOV3Y/eRupPNkq3nxD+/YB8ta+58LoKaHnu3u7tSpTzf7Q8eLU0rPO3KFsDLe2q90dL7fDNZQ6MdpHZ/WYj4nij2Fb+HG5ECx894wH/W7sf3R0uEwLe2eyuMSsB3KqArSxdls+wa3FMh2iZhaSeUkDLcxegC2vnWhqWMFmrzVEH9P7k83jrl4W8OR/dzqdvM5Qd5hLvHLdHu4anwcWxhbfRDrXbGX8352+d6sYV7OtbjvzEdRt2Vo60hLLWKqDluQvQ17aEja/X0rA2luVh2WkC+nCI28EwiNC7uNwOv4dvNsvLH3cLPGLHh7aazkIb/5vIu53Ad/mmluPFMA/fuJba1uaG5suaiutG1AxPhiC3awCem+fu1a5+MsCuTdhrpKOslqQqVUDLcxegl3r8oHRaGtZBcu1ezJmBPo6O3d7tEOJjoIdz6TmBYwQPYuO73Qxnp7ozUfNo65lHa7iq3c4O2IVwPqfUAHdZdN52U9rDaZYPi6f0tF2Tm53f7+v7mAWGu98MigvQ8twF6MIaqZaGJUzWanPOBPQYc+MY3MLHojOev/bxpI11XbqxdONtb11ehmgB9QzknodfRZmlIvPhIjjfMQij8lxEn3ZzHzW/6Ox8dWteBVMLJdF6mMZOS3Qb/UKzum97bdtzcmzpNSMB1Y2WCxcroOW5C9AXu37fC7Q0rH1VOi73swA9hKyFRzffHI9Ou/3cIwk9JG3UmR5yN9928fcCJ6Si59SAtAmdm+ZmVrI/Owiafedu01oY8Xdz/vd7vy89OHqmXyI3GAi43ZtXz8O97K7jkgNu2HUYgHw08pBa4OfXIwD0Bc3lAUm1PHcB+gMaz1SRWhqWMFmrzTkD0H306OeZ21nw/tzzbhV4N3ydj85tLOmAnov4DdjGi+ZiaKcA59wQLo8bpnOz9+3eG5PlzZA97FykonO3cN+OKrhhdfNHu2fdrNw3/+62u9mRcTu77YbMn2630TD8sIvjluPlhvbDDkpZ56i6UXLh5gpoee4C9M2bxroMtTSsdSrIuVo60B067IpxvyTLQztclpUbqo5gdHvV3O4vBuvhPerz0bv3Wgrm8VBB7ONuaLrbRfv0ZOvjIGxTD7sYZsetm7ZuN9o93Zt2+r6bz3awdn8PURxC1+U+nKV3UxPW8qk5+riD4jQ3/+U1r3Lu5rwlWp67AD3w8X/5vV+sant/5Vt+qOq61EVaGtZmgjw4I+lAt/Kko0MPwVxE7CAZDX/3J8jZ6+y8u9syVgK2EL5xVO7+Djsa3o6W508moo73n4flOtTaqg9mEbpkLcyjOX3flOKxBxupPwXTDG0Jt1f2qNn2pLrwMzeAHtatHzN4cEum+CkFtDx3Lwv0WniX3ha1kNfSsEp1kp5OPND7U9+mhrzj6DZW3QPOxZS3dqj7RddZcFDKRZsx4MbwbbsdDr5dNyE99J9qEfHIwrAz4K2bGhYPOzXjTkY7ut/udTef53aY/qkbpfBnxqdBPq5X18nqpzikt3Ls0/LcvRzQ50C+FMRb56elYWl5RIgH+mhgvDQijqNzC6EWTk9m6np4SptfKDcVnc51KsJWkY7Qx1F0B8dkJ2AK/qmIOu5oBDa4aP72qrnfn5oX/ZnxS1rysOPhD9OdGtVYkj9p91JAy3P3MkBPgXcpvEsb05qytDSsUq2kp5MPdKkK5qL2VIcgN7fvYB7Df6rOU52K8VC7z8l0YMxo/1O05z03mhF3NMYdiH4O/+AT7KS2CMl2aXnuqgf6GriubYA1ZWtpWGu1k3I9QK/1RG5hXC6qnlpIl+ocxOmjdQCjYvIw97MWUx2H9DTC8OCc4VoFgF7bdo6/TstzVzXQY6DuFZHPNb8ldmhpWHOanOX3UwG9ZVouAl4SMRvvJACW5N1cdG097eeZUxF0mPEUOEvqNmdPNP/e9QN8qampANdaU52OOFL3C/Zsngy3n+Fe1/LcvQTQHwXyuCE7sE/Zo6VhneEmLrHxHECfi4bNHuvwABYHmtx8eC5SXQK7YCV6dtuXi6pTw9dzEXfovZLIephfuJbQR9Kp4fWSKYK4/DBSB+gl99mj02h57qoFehgVSwO6abw5m7Q0rEffoFuVD9CnwDkfTXu0pcCWgraD45J585y3E8Ps96Z5+kDTvHrlz2PtTmedaTKpupbYCNC3uhf3zEfLc1c90KXA3DXGuShdS8Pa8+Y7Mu8zAH18JOl4eN2+QS0zlD4raG7+Of4+jLTd9vi5RWpz8+OzxmUSpG2+//6fNk8fetEeWONBXhrhu6JS6bvv2tP5nNLmO4Be68Ejr9Py3AXoQat57/f+alUbeu1b/kPxdQC9WCoRCc8A9HaCumV1DjT2kNPhnO7U9rMQXO7fU/PH+XK9E4cLxtLOrYvOh/vAw07LeASgl2rw+taaYfUx3I3897t7p7xBedkraEU09IsbAdCFNYD4wTs35F4L79JqpyA/Z5PJW0vDKtVJero9gP7ey3ea195+a1B18535lH4/wElHqfc+/tkujzeDvG+Nz9t87yPX916m0nsguuusXS5Pn1/4/TCvcRnOIJ9PzvMlw9hBN2FwWM0UzG2+0yvaw6h+DOyhxaGd5t/2vHxWtku/o9P2aXnuqo3QjdtS0fAcyJdE26aMJfnNRedbAt29ttHkaRdE2YVR7SPPvdIx+t795tK7a/tHWyKPME1cRphPeBvF3zu7wjSh/fG1cR1Cu2NbUzYteeQAdKvW/kCfG/YeR9HZE9psKx+9tNS98GU8CB6DPGfLBPDD9X19+UtaGmkfqQBAf6T6ibJTD954u9h/b/7Z4Mql8C6tcgz5v9j8g8GlR6xyB+huZfewI5PqPEz5dUugDyNeG6HHkbmL3nPfj2y93xsfmZs8742Jlh2A7b99WcPvbdQdpnH5D0cR/HB5/L0rKx3tx6MDrqxwBKH0rhqmG2+Dmx7SH8xK9FltAPLunW12eD20oa5eXPUYBQD6Y3TPlpp78NYc7rJV1WrKXtuwwug7rMdUhG7SxVF5qkMQpgvB6PJORf5L7Ym1D+12v82NBIQjEqFtS2FuytsS6ClYrgW6WRD3+ZfvtlnnIf5W69/PfzwGfQroFkpxp8Dm/1bbAYmjdfN3bvg+/j7M1+qxbIjdtkE/ypR+q1zuDp6KvJfu0+/KaCPzePh9qycI+RylwNrn7lF2zpWjesg9jJTjKNkIs9cK+BTIw9GBqZGBrRpWDNK5IfcYfDk4x8P37SO5G4rPgT1shPGwv+skZB/BwTB/CPQ4z5IOy9zNkPr9OkD3c+QW6CG4fRS/LEK31407G8Oyhi9p8V6YXuzWt4bR0HqNn/01C+bR+7e5EZmv0/zxV2/13H10TS4B9BCgW79MZUl+roMB0MMIy2/zSd0MuQj96kDvwToYch9H3C7d13/1B5t//Td/MYriw/SygJ4+WW5upfzyaH8I8pKV//aKr3r6QPPq+VX3djaA/miQrS0foK9VcOPr40gqjM7nVpxvbEqbXSr6n7PJXLd1w0oNOa9dFBdDNldGbnFdKqpfGqGHkX7pqMGjh9zDOXQbtfqV7ktXufcRdD/kG87H+9Xsbqg9jJKd1umV6eZXP+zu0lpb7cqv1Op3l1fox3Ao3ucTrrT3ZYVgte8mNyvG3WdrkK/fG2+A/pXn4HQa9pvv8Rg9LM+tn7uHGR4VpDZCL4mGY9Hnou2ck5YM3c/ZtXXDAujjxXFLbrY9htyXlD+d1u4tdzvMx+8Xd8BMDSO33aoe0r6c3H7zFHxD60JIdtvDBtycK2uqplP73Eui6tzhLiXXxnYF29/6bHP5b+dpctpXga2fu/tam88doAfa/J9P/70qP3zwE/+i+Lq9gR5v94oXr7WPZbfkN5j/dhVIzYPHv7UoCM7LjKNl9/vctrXUNrrR4zMzh740Qg9tKnbWTovilpR/HaCXDpfnOic5pZamL/OOuYWeTNssS04q4QoAdGEOWjrkXgvv0mqnIP+IIfdSe0mXVkB6hJ5eJR7DcW6Pdyr6Tg1LF0TZZgX6k12Jbj8hUOdGCqZaYRnw09vTtm/dt6dbczdnyfBRoQBAF+bG1IM3FQ3Pgfzf/+2PLqrZX/s3X5xMH4J9Ljo3GWlpWItEFJxYL9DnItfhEHrN9rLhNVMwDzsUqeHrfBwcDDZ13Ydb5hWyqUZW0vFJdyRe3J6aV3Hhgtsxpk0roOW5q3bI3bgvPuDlo//22wZeXQrv0psihvwX/8Z/Hlx6xCr3UltJN62AXKDbl63ckvu4p0AVgzoHuhiyJQvTUteUlpdaKT41oG1WDtgh7zbV6AhYXy+7Yt6kKjn8JV5DMGUDc+danh8AXZgncw/e1NGsL77pHx5i/av//U9H5cydTqelYR0i8AGFAHQjcthByA19xxF4wfD8Cv9ZzHrY+pPaEiMBtucTEL72JWjdgrh2GaL5N0Bf4UJRl2p57qqO0MPh9ThKNq1pL7CnQB6ODkwtotPSsETdrSuMkQx0G6GWnHA2D1c/91wy5x0KOjV0Pxedl82LW3Tb9xG0ObZvNLP/82ANqR1ueOvGMdoA/d7cn8PXppY0jJL1ByX5kEayAlqeu5cAeji0noJt2NCWQn5Jfm4oHqBLvrWHtskFemhnDLMUwF36eGg76BCY13+6hV5mKnqwoG2Jz8pBPc417AQ822NVWxPd5IIFuf8m7oCMt921OyJupkNgX2/qS8iNKjircvVwehOhL2kVktMCdGHeiR+8YXSemiufA/Ha6qU6BuHceg7qWhrWWv2kXH8uoKfmoYdD08NV5zbyNYe4uO+fDSz7+eilYF6a3mJ5+BlC3P6WG4UIOyke+D6/UntS6aZGHkLgS2mp2LFGAS3PXbURugP6koVvtZBfEtXPRelaGtaam0vStWcBejv87l/23UmYjsaHU8rDyNfHr3NAK4Vl7M1UVFsD8TjfkqmH0pY1V/dUR6Q0b9JJVEDLcxegH9y6APrBgq8s7ixAT8WqcTTeLYsPomIzvh4Cth2T7t4eloqMV4oZD+H3fYIcQJcOadvRhf6dKb25JYCOo+706vbhvP1aPbheigIAXYonOjuWDrk/wnyG3B+h+royzwF0W0fzKlUbO5rtWSHi7b9v93sLO5MiDaa5rWmlWsYAjUcKhr+n58RLywrTubGHFIynOgemE2MX26Xf/DY1JF9jJ9dIUwCgC/NI6sFbM+y+Z7XmonNTtpaGtaeOR+Z9JqCH7wY3cB8s/2phHkXjIyE9ED3wS9Qez4O368/7sf2pCHlpFD5nT7Borl0FF8I4Liu3x3xuOmFrm+fqxO97K6Dluat2yN00gPhUuCXz6Vs2oPigGVa5b6nuvnmdC+hhXP7c3MyK7lvTv+JzPD+e085vCRtGz2F6A7VuyD7e5z25Ov4oGJouhXlJjD18ZtCX6auRi7xzEX5u0eG+bZDc91cAoO+v8aIScg/e1FGvR4E9dSzs3ItctDSsRc4TnPisQDeS+rg43rNdJrjFXXetHauPpuGDyDuA+jC6Pwrg+Tq5eszXOo7mw4Nrwj3/j6/TfF1IsUQBLc9d1RF66NAjwV4Dcmerloa15GaSnPbMQN9S1x51/Ur67rCXvtsgHXKp+fU8wNt1Bl1dh8frSq/nll6/Tl5anruXAbprmo98OUvJ7aGlYZXU9QxpAPoZvDRnY/oUufiFM/b8mu70uWjUndXtcxqf+3ctz93LAb0U7Gub59zQei5/LQ1rrX5SrgfoUjyxxo5cdO4mEWze7mUv4Up3QL5G9/Ncq+W5e1mgp5raXPSea5618E7lp6VhnedWnrYUoGvwpHszW3wsbPd3u/XeLp4LdwpoqDl1KFNAy3MXoJf5+7BUWhrWYYLtXBBA31ngQ7LvIvRg/t8PtxvYPwHzQ/wgtxAtz12ALqyNaWlYwmStNgegV0sn7MIgOh8dIjO1V11YNTBnFwW0PHcB+i7Noz5TLQ2rXgFZVwJ0Wf5Yb43dWW/O0hvvNmcF+3p9z5mDlucuQBfW/rQ0LGGyVpsD0KulE32hj8k5LEa0ow4yTstzF6Af1GBKi9HSsErrKz0dQJfuIexDgfUKaHnuAvT1bYEcUAAFUAAFTqwAQBfmPCIpYQ7BHBRAARQ4iQIAXZijALowh2AOCqAACpxEAYAuzFEAXZhDMAcFUAAFTqIAQBfmKIAuzCGYgwIogAInUQCgC3MUQBfmEMxBARRAgZMoANCFOQqgC3MI5qAACqDASRQA6MIcBdCFOQRzUAAFUOAkCgB0YY4C6MIcgjkogAIocBIFALowRwF0YQ7BHBRAARQ4iQIAXZijALowh2AOCqAACpxEAYAuzFEAXZhDMAcFUAAFTqIAQBfmKIAuzCGYgwIogAInUQCgC3MUQBfmEMxBARRAgZMoANCFOQqgC3MI5qAACqDASRQA6MIcBdCFOQRzUAAFUOAkCgB0YY4C6MIcgjkogAIocBIFALowRwF0YQ7BHBRAARQ4iQIAXZijALowh2AOCqAACpxEAYAuzFEAXZhDMAcFUAAFTqIAQBfmKIAuzCGYgwIogAInUQCgC3MUQBfmEMxBARRAgZMoANCFOQqgC3MI5qAACqDASRQA6MIcBdCFOQRzUAAFUOAkCgB0YY4C6MIcgjkogAIocBIFALowRwF0YQ7BHBRAARQ4iQIAXZijALowh2AOCqAACpxEAYAuzFEAXZhDMAcFUAAFTqIAQBfmKIAuzCGYgwIogAInUQCgC3MUQBfmEMxBARRAgZMoANCFOQqgC3MI5qAACqDASRQA6MIcBdCFOQRzUAAFUOAkCgB0YY4C6MIcgjkogAIocBIFALowRwF0YQ7BHBRAARQ4iQIAXZijALowh2AOCqAACpxEAYAuzFEAXZhDMAcFUAAFTqIAQBfmKIAuzCGYgwIogAInUQCgC3MUQBfmEMxBARRAgZMoANCFOQqgC3MI5qAACqDASRQA6MIcBdCFOQRzUAAFUOAkClwC6F/5yldO4o6m+fjn/9ZpbMVQFEABFEABOQoAdDm+aC0B6MIcgjkogAIocBIFALowRwF0YQ7BHBRAARQ4iQIAXZijALowh2AOCqAACpxEAYAuzFEAXZhDMAcFUAAFTqIAQBfmKIAuzCGYgwIogAInUQCgC3MUQBfmEMxBARRAgZMocAmgS/VFajsdQJfqLexCARRAAdkKAPQH+gegP1B8ikYBFEABZQoA9Ac6FKA/UHyKRgEUQAFlCgD0Bzo0BfQP/pMPPtAiikYBFEABFDirAv/vZ/7fWU0f2H273+93FTWhEiiAAiiAAihwYQUA+oWdT9VRAAVQAAX0KADQ9fiSmqAACqAAClxYAYB+YedTdRRAARRAAT0KAHQ9vqQmKIACKIACF1YAoF/Y+VQdBVAABVBAjwIAXY8vqQkKoAAKoMCFFQDoF3Y+VUcBFEABFNCjAEDX40tqggIogAIocGEFAPqFnU/VUQAFUAAF9CgA0PX4kpqgAAqgAApcWAGAfmHnU3UUQAEUQAE9CgB0Pb6kJiiAAiiAAhdWAKBf2PlUHQVQAAVQQI8CAF2PL6kJCqAACqDAhRUA6Bd2PlVHARRAARTQowBA1+NLaoICKIACKHBhBQD6hZ1P1VEABVAABfQoAND1+JKaoAAKoAAKXFgBgH5h51N1FEABFEABPQoAdD2+pCYogAIogAIXVgCgX9j5VB0FUAAFUECPAgBdjy+pCQqgAAqgwIUVAOgXdj5VRwEUQAEU0KMAQNfjS2qCAiiAAihwYQUA+oWdT9VRAAVQAAX0KADQ9fiSmqAACqAAClxYAYB+YedTdRRAARRAAT0KAHQ9vqQmKIACKIACF1YAoF/Y+VQdBVAABVBAjwIAXY8vqQkKoAAKoMCFFQDoF3Y+VUcBFEABFNCjAEDX40tqggIogAIocGEFAPqFnU/VUQAFUAAF9CgA0PX4kpqgAAqgAApcWAGAfmHnU3UUQAEUQAE9CgB0Pb6kJiiAAiiAAhdWAKBf2PlUHQVQAAVQQI8CAF2PL6kJCqAACqDAhRUA6Bd2PlVHARRAARTQowBA1+NLaoICKIACKHBhBQD6hZ1P1VEABVAABfQoAND1+JKaoAAKoAAKXFgBgH5h51N1FEABFEABPQoAdD2+pCYogAIogAIXVgCgX9j5VB0FUAAFUECPAgBdjy+pCQqgAAqgwIUVAOgXdj5VRwEUQAEU0KMAQNfjS2qCAiiAAihwYQX+P0xNRl4+3KtDAAAAAElFTkSuQmCC'
    shopItemsMenu.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAgAElEQVR4Xu2df8xV1ZmoF15/1ME6hUlkYrBYczNSW34oNI3OdMAWW4aORojBEurYpP6BM39AonNj0Ag0dZw7tVrGP2aazE0uGQ3GsUB1LtVKBZorkImOoBSJiWTskI5wE6ANpA6QfjdrM/t0f/vb5+y1117r7He95/kSE+Hstfa7nvc96zlrrX0+Jo2NjY0ZfiAAAQhAAAIQSJrAJISedP4IHgIQgAAEIJARQOgUAgQgAAEIQEABAYSuIIkMAQIQgAAEIIDQqQEIQAACEICAAgIIXUESGQIEIAABCEAAoVMDEIAABCAAAQUEELqCJDIECEAAAhCAAEKnBiAAAQhAAAIKCCB0BUlkCBCAAAQgAAGETg1AAAIQgAAEFBBA6AqSyBAgAAEIQAACCJ0agAAEIAABCCgggNAVJJEhQAACEIAABBA6NQABCEAAAhBQQAChK0giQ4AABCAAAQggdGoAAhCAAAQgoIAAQleQRIYAAQhAAAIQQOjUAAQgAAEIQEABAYSuIIkMAQIQgAAEIIDQqQEIQAACEICAAgIIXUESGQIEIAABCEAAoVMDEIAABCAAAQUEELqCJDIECEAAAhCAAEKnBiAAAQhAAAIKCCB0BUlkCBCAAAQgAAGETg1AAAIQgAAEFBBA6AqSyBAgAAEIQAACCJ0agAAEIAABCCgggNAVJJEhQAACEIAABBA6NQABCEAAAhBQQAChK0giQ4AABCAAAQggdGoAAhCAAAQgoIAAQleQRIYAAQhAAAIQQOjUAAQgAAEIQEABAYSuIIkMAQIQgAAEIIDQqQEIQAACEICAAgIIXUESGQIEIAABCEAAoVMDEIAABCAAAQUEELqCJDIECEAAAhCAAEKnBiAAAQhAAAIKCCB0BUlkCBCAAAQgAAGETg1AAAIQgAAEFBBA6AqSyBAgAAEIQAACCJ0agAAEIAABCCgggNAVJJEhQAACEIAABBA6NQABCEAAAhBQQAChK0giQ4AABCAAAQggdGoAAhCAAAQgoIAAQleQRIYAAQhAAAIQUCP027ctJZuKCbx059bkRjeoJv/3F/7B/N7v/Z7XmDZs2GDemLPfqy2NIACBiQRSnF+q8ojQqW7xBMb+4Zz553/+Z/FxlgNE6MmljIBHlABCF5Z4VujCEhIwnGt/Mt08/fTTAXscTlcIfTicuQsE2hJA6G0JBm6P0AMDFdQdQh+fDLbcBRUnoagggNCFpRGhC0tIwHAQOkIPWE50BYEJBBC6sKJA6MISEjAchI7QA5YTXUEAoUuvAYQuPUP+8SF0hO5fPbSEQD0BVuj1jIZ6BUIfKu6h3gyhI/ShFhw3GzkCCF1YyhG6sIQEDAehI/SA5URXEGDLXXoNIHTpGfKPr63Q7VPhXfwM+uUv/GKZLjLCPSFQTYAVurDKcBW6lsQJwz8hHNd8uIyjrdBDxuISr8s1CN2FEtdAYDgEtHhh5H5TnJbEDafM/e8SUqIInS13/0qkJQTqCWjxAkKvz3V2xalTp8yUKVMcr75w2bp168z69esHtpk7d645cOCAWbBggdm1a1ej/vO2ro187uHad/k6hD6YHCt038qiHQTCE0Do4Zm26tFVIL6JQ+jN0uOaD5deWaGzQnepE66BgC8BXy/43i9WO1bojmSLQp8zZ46xq+OzZ8+azZs3Zz1U/Z3LCv3RRx81P//5z83MmTPNQw895BjNhcvyFfq0adPM4sWLs7978cUXzcmTJ03V37FCb4Q36sWs0KPipXMINCKA0Bvhin+x64rQN3FFoeeidv27WKOv2q53/btYMeX9uubDJQ5W6KzQXeqEayDgS8DXC773i9WOFbojWVd5l6+zgl26tPrfah8bG+utsu3qedu2bX3P6Xfu3GkWLlw4LlpXeZevmzRpUuWo7T1sDBs3bpzwejk++6GmOLZyfAh9cGGxQnd843EZBIZAAKEPAXKTW7gKxDdxWoRu/13xj3/845Vod+zYYV566SWE3qTwPK9tI3TPWzo1c30fOXUm4CI4C0hCAiH4ekHa0FihO2bEV+i33367eeqpp3p3efPNN83hw4ezPw9aoc+aNctMnTrV7N69O7s21Aq9uAtQdY/iCn3lypVm+/bt2Zk8K3THQnG8DNE4gmp5GZxbAhyR5ghdWKJdVxa+ifMV+tq1a82vfvWrHq1HHnnEfP/7368Vet2Wtu3AZ8u9KPS//Mu/NFbqf/Znf9b70FAU+qAPHHXxuebDpYw4Q3ehFOaakHkLE1G7XhB6O36j0trXC9L4sEJ3zIiv0H3P0OuEGULo5aGXz9ARumNxeFyGaDygeTSBswe0EWyC0IUl3XVl4Zu4EEJ/4oknzJ49e8yWLVtErNDvueee3urcBjRv3jxjf+95/lAcQo9X5IgmHttiz3AeDufU7+LrBWnjZoXumJEQQl+0aJE5evSo0xn6MFbodrt99uzZPQIPP/xwdhzgIvS6M37XD1gu+Nlyd6EU5pqQecsjemnqj2qDu/3En9Re43MBQvehNnptELqwnLtORL6JCyH0MrI2K+Cut9yLY5k8ebL50Y9+ZL7whS/0/to1Hy5lhNBdKIW5JkTeJgj8j/+zPrifXjbumlCCR+j16LnCGF8vSGPHCt0xIwi92ffkQ4ghT01bocf651MvvvhiY//r9/N/r9/X9zWNoumJ3EXgde+7/xJ8W7Fr5FyHjtebE0DozZlFbeEqEC2JiwozQOeu+XC5VVuhu9wjxjWDGGgSTVCRlxPRUuyaOMeoUfq8QECLF1ihU9FRCCB0Y0ZB6JnMQ6zI66rwp5cZn9U6Qq8Dy+sIXWANuApEyycxgSkYF5JrPlzGwQrdhVKYa5rkbWgyz4fmIXWEHqYutPeixQus0LVXakfjayKGuhAReh2hcK+75C3qFnvdUBpuwSP0OqC8zgpdYA24TESaEicwBazQS0nRuOUeZFX+teuMee5IuxJ2XK0j9HaYR6U1K3RhmUboshLimg+XqFmhu1AKc01d3hD6cDiHuQu9uBJA6K6khnRd3USUh6ElcUPC6n0b13y43AChu1AKc82gvAWRuQ0zxArd9uOwSmeFHqYutPeixQucoWuv1I7Gh9DjPeVuf+f+a6+9VpnZz3/+8+ZP//RPvbPeL2+tZG4F3vTHdUu+RupthN4F56aYuD4MAYQehmOwXlwFoiVxwcBF6sg1Hy63Z4U+npL9RTlvzNlfia4tq6q8ecs8F3lZzqFW6DmBAVJvI/Rhc3Z5L3BNHAJavMAKPU59jHyvCD3eCn3YovES+iBpI/SBv6Ng5CePDgAg9A6gD7qlq0C0JE4Y/gnhuObDZRxtV50u94hxTayn3Icp9OAyt6BDC9322WeVzgo9RmXr61OLF1ih66tNESNC6DpW6I2F7iJrl2uaVjFCb0qM6wsEELqwcnAViJbECcPPCr0iIamv0BvL3HX1HUPofVbprNClzxQy4tPiBVboMupJXRSuH7BcBs6W+3hKw9py9xK6i9QROmfoLm/8IV6D0IcI2+VWrgLRkjgXJl1e45oPlxgRemJCr5M6QkfoLm/8IV6jxQsjt0IfYo1wq8QJtP0gMZJb7sWcD+tra/k9K87R2XJP/E04pPAR+pBAu94m5IrQ9Z5cp5vAKAvde7u9qiRcfrGM6y+SqSu5ktQReh0wXrcEELqwOkDowhKiIByE/p9xsli15d5vNd80AoTelBjXI3R5NYDQ5eUk9YgQ+hCFnhdL2/N1hJ76266T+Fmhd4K9/00RurCEKAgHoXcgdFs3baSO0BW884Y/BIQ+fOYD74jQhSVEQTgIvSOht5E6Qlfwzhv+EBD68JkjdGHMtYeD0BH6MP8RHO3vJ8njQ+jCssMKXVhCFISD0DsSOlvuCt49aQ0BoQvLF0IXlhAF4SD0DoTeRua25thyV/DOG/4QEPrwmbPlLoy59nAQ+hCFztfWtL+dRI8PoQtLDyt0YQlREM4oC92mz+uXy7j8Epmq2gjxi2X4TXEK3nXdDAGhd8O9710RurCEKAgHof/ImD+OsEpvu63er7YQuoJ3XTdDQOjdcEfowrhrDgehI3Sectf8Dv/t2BC6sDyzQheWEAXhIHSEjtAVvJEdhoDQHSAN8xKEPkzaw73XySf+n1m6dOlwb2qMmTt3rvnSl77kfd/U/7U173P0OmIxttwrttttGG3+cZa9e/eaPXv2VI4mZm3U4eP18AQQenimrXpE6K3wiW7cdqXc1eAQeh/yiQg9Zt0wX8Wk27xvhN6cWdQWvEGi4u20c4Q+Hv+GDRvMMLeCvZ52H1QxoYXeZ3XedoUes+iZr2LSbd43Qm/OLGoL3iBR8XbaOUJH6AMLEKF3+v7UcHOELiyLCF1YQgKGg9C7Fbq9e/BVeqj6GCBzVuihIOvvB6ELyzFCF5aQgOEg9O6FLlLqNTJH6AHfhMq7QujCEozQhSUkYDgIXYbQRUndQeYIPeCbUHlXCF1YghG6sIQEDAehI/QJ5YTQA77D6AqhC6sBhC4sIQHDQehyhN5bpdv/ifFrYevq5qeXZVfcfuJP6q7MXm/zPXSnG3hexHzlCS5SM4QeCaxvt7xBfMnJb4fQZQk9j2boD8o5rsqLtBC6/Pe3hAgRuoQsFGJA6MISEjAchC5T6L3V+jBW6h4yZ4Ue8E2ovCuELizBCF1YQgKGg9DlCr0ndfs/McTecIu9XHas0AO+ERV3hdCFJRehC0tIwHAQumyhj9uCDyX2liLPY0LoAd+IirtC6MKSi9CFJSRgOAg9DaFPEHv+Fy4r9/8SeN7E9aG3ujJD6HWEeD3bZbpzqwoQk8bGxsY0jASha8hi9RgQelpCL2cxe3iu5ieUwNlyryPN61UEELqwukDowhISMByEnrbQA5ZC465YoTdGNpINELqwtCN0YQkhHAgIINBG6IP+VTsBQyOEgAQQekCYIbpC6CEo0gcEdBFA6LryGWs0CD0WWc9+EbonOJpBQDEBhK44uQGHhtADwgzRFUIPQZE+IKCLAELXlc9Yo0Hosch69ovQPcHRDAKKCSB0xckNODSEHhBmiK4QegiK9AEBXQQQuq58xhoNQo9F1rNfhO4JjmYQUEwAoStObsChIfSAMEN0hdBDUKQPCOgigNB15TPWaBB6LLKe/SJ0T3A0g4BiAghdcXIDDg2hB4QZoiuEHoIifUBAFwGEriufsUaD0GOR9ewXoXuCoxkEFBNA6IqTG3BoCD0gzBBdIfQQFOkDAroIIHRd+Yw1GoQei6xnvwjdExzNIKCYAEJXnNyAQ0PoAWGG6Aqhh6BIHxDQRQCh68pnrNEg9FhkPftF6J7gaAYBxQQQuuLkBhwaQg8IM0RXCD0ERfqAgC4CCF1XPmONBqHHIuvZL0L3BEczCCgmgNAVJzfg0BB6QJghukLoISjSBwR0EUDouvIZazQIPRZZz34Ruic4mkFAMQGErji5AYeG0APCDNEVQg9BkT4goIsAQteVz1ijQeixyHr2q0XoWgrLM43immmpK3FghxQQQh8S6MRvo2XenTQ2NjaWeC6y8LVMvFoKS0NNaaorLfloOg6E3pTYaF6vZd5F6MLqV0thCcPqHY6WD4reABJviNATT+CQwtcy7yL0IRWM6220FJbreKVfh9ClZ4j4INCegJZ5F6G3r4WgPWgprKBQOuwMoXcIn1tDYEgEtMy7CH1IBeN6Gy2F5Tpe6dchdOkZIj4ItCegZd5F6O1rIWgPWgorKJQOO0PoHcLn1hAYEgEt8y5CH1LBuN5GS2G5jlf6dQhdeoaIDwLtCWiZdxF6+1oI2oOWwgoKpcPOEHqH8Lk1BIZEQMu8i9CHVDCut9FSWK7jlX4dQpeeIeKDQHsCWuZdhN6+FoL2oKWwgkLpsDOE3iF8bg2BIRHQMu8i9CEVjOtttBSW63ilX4fQpWeI+CDQnoCWeReht6+FoD1oKaygUDrsDKF3CJ9bQ2BIBLTMuwh9SAXjehstheU6XunXIXTpGSI+CLQnoGXeRejtayFoD1oKKyiUDjtD6B3C59YQGBIBLfMuQh9SwbjeRkthuY5X+nUIXXqGiA8C7QlomXcRevtaCNqDlsIKCqXDzhB6h/C5NQSGREDLvIvQh1QwrrcJWVinTp0yU6ZMqbz1zp07zcKFC13DcrpuzZo1ZuPGjROuXbBggdm1a5dTH9Iuki70QfWyYcMG88ac/UNHuuiDhWb16tUT7rt7927zxMnvmX4x33rrreaK1Z8Y1+7BKWuMrZ+qn7y/QQMsxlKVyzyWrlgNPTncsJJAyHm3S8QIvUv6FfcOWVijJPTDhw+b5557LiP6jW98w1x77bVBMpuy0J9++mnz42teC8KhSSchhb7+mkfMvHnzEHqTBHBtYwIh593GNw/YAKEHhBmiq5CFVRT6nDlzzNSpU41dmduf2Cv0e++917z44ovm5MmT2Qor9gp927ZtZunSpcHHlorQDxw4YPbv328uvfRSs2LFioxD21XnxZuM2bp1a9ZXEw5FoW/atKn3trAfug5+/nBvhW5jPnHihLErc/tTtUL/9qfWG1u7x44dMy+//PK4t1jeHyv0EDPPaPcRct7tkiRC75L+EFfo69atM3Pnzo0ivXwYxS33sbGx7H520kbo8Yqsass41DZyCKHXbXPbDyH5h4ZBQnfZXq+izJZ7vNrT1DNCF5bNJisIYaGPCydkYRVX6P2EXl7Z2j/n5+BlKdvX8jN529/69evHxe4idHtubyfn8o/tz76Wr9bKr9tYBrUtfljJ24b4ICG9rlyE3q+m7Nj6nVHb14pCz5m6iLWJREMIfdCZ/J133tk7zx803ra7GZLnFGKrJxBy3q2/W7wrWKHHY+vVc8jCKgp91qxZ2ZZ7LtN8yz2W0FeuXGm2b98+Ycv9pptuMm+99dYENg8//LC55ZZbzFe/+tVKblbo8+fPN2+++WblhwGE3v8BuEFC73dGXSf0r525K8vDli1bzNl7fjMuJ0WhP/vss73X3n33XXNg/s96W+5Wok2Fnt/Xdlrur1wYX/rSl8wdd9wxTuj9xovQvaYrNY1CzrtdQkHoXdKvuHfIwhr0UNzatWvN3XffbY4cOTJuGz7UCr04tJkzZxor7K9//eu9bfgrr7zS3HDDDWbfvn3ZpeUdhM9+9rPZ+eovfvGL7PXibsFVV11lbrvttt4tli1bZi666KLeOBYtWmSmTZtmPv3pT2f3bfOT4gq9PN68pg4ePJh9qLv66quzS+zY8jPq48ePm1dffbXX9LnJL4xboe/YsSM7x66ScvlJeteH4nyEXnx/lJ+a//d//3dz+vTpLO/2x+72lFfoxfHa5zqWL1+eXYvQ27xL0m8bct7tkgZC75J+h0KvkqhdtccQej7Mui388hl/OZZ8dW9X8j/84Q+zbu1DYB/72Mey3YBRfiiuKKRP/OCKjM3bb79tPrnhut6K2Aq8vB3+rRmPmhtvvNHs2bPHPH78O+MqctAZ+qBz+q6EXvUBoZ/Qyx8GELqwiXDI4SD0IQOvu530lVRd/PnrIQurfIZuz7zrztVDCd3K2/4MOlcvn8nXCd31DD3kE/zS66rJGXqV0F3P0NtwyFfFVRJtuuVua6pJfwjddeYZ7etCzrtdkmSF3iX9Ia7Q84fYUha6PT+3X4OzP2fOnMm2U/vtNIT6pTltRDaM0mor9HKMeX/lM/Qyh7t+eYG9/VB2/t7BI20i4PIvlim3RejDqKrRuwdCF5Zz6ROvK66QhVV+KG727Nnm3Llz5vnnn68UoT17Pnr0qLHf77U/5XNrK8li20FPuduH4uyPlbBrf8UVelUs9jz8gw8+yPqtG8eonaG/88472Rb7JZdcMuFcOK8pew4+ffp0Y59psD/2PXPzoc+ZGTNmZH8uti0LvckZut3yX7x4ca/klyxZkn07orxCtzHb5yTy3wQ36GtrxTP+Jv1xhu4684z2dSHn3S5JskLvkv4QV+hVwyw/iJZfYx8wu/zyy7MHjPLvkpfb131trep+9gPC9ddfb957770JL9v+7JP4d9114enp8k/xw0XxNSvuhx56KPvNcPkZev76KH1trYrZY489ZvZ95o2+v2q1+FBcsb19+O2+vasGfm0txBl6OeYqoX/3hv9p/uAP/qCyJlx+lWw/oZc75Axd2EQ45HAQ+pCB192OFfpEQoOecrdX9xN63lM/ieZtB63Qm0g5RCyj/rW1Kt65pAZ9bS3f0i63r/va2qD3o+tDcS5C7xefbYvQ62ZFXnclgNBdSQ3pOoQeH3TVQ3bx79rtHbTUVbcUuTsEZBNA6MLyo2XilVZYTz75pDl06JA5f/68OXv2rNm8eXOW+aotd2ElESQcLXUVBAadQEApAWnzri9mztB9yUVqJ62wBn1VrLzlHglJp90i9E7xc3MIDIWAtHnXd9AI3ZdcpHbSCguhX/gX3PiBAAT0EpA27/qSRui+5CK101JYkfAMvVtW6ENHzg0hMHQCWuZdhD700hl8Qy2FJQyrdzgI3RsdDSGQDAEt8y5CF1ZyWgpLGFbvcBC6NzoaQiAZAlrmXYQurOS0FJYwrN7hIHRvdDSEQDIEtMy7CF1YyWkpLGFYvcNB6N7oaAiBZAhomXcRurCS01JYwrB6h4PQvdHREALJENAy7yJ0YSWnpbCEYfUOB6F7o6MhBJIhoGXeRejCSk5LYQnD6h0OQvdGR0MIJENAy7yL0IWVnJbCEobVOxyE7o2OhhBIhoCWeRehCys5LYUlDKt3OAjdGx0NIZAMAS3zLkIXVnJaCksYVu9wELo3OhpCIBkCWuZdhC6s5LQUljCs3uEgdG90NIRAMgS0zLsIXVjJaSksYVi9w0Ho3uhoCIFkCGiZdxG6sJLTUljCsHqHg9C90dEQAskQ0DLvInRhJaelsIRh9Q4HoXujoyEEkiGgZd5F6MJKTkthCcPqHQ5C90ZHQwgkQ0DLvIvQkyk5AoUABCAAgRgEEHoMqi36ZCXVAh5NIQABCIwwAYQuLPkIXVhCCAcCEIBAIgQQurBEIXRhCSEcCEAAAokQQOjCEoXQhSWEcCAAAQgkQgChC0sUQheWEMKBAAQgkAgBhC4sUQhdWEIIBwIQgEAiBBC6sEQhdGEJIRwIQAACiRBA6MIShdCFJYRwIAABCCRCAKELSxRCF5YQwoEABCCQCAGELixRCF1YQggHAhCAQCIEELqwRCF0YQkhHAhAAAKJEEDowhKF0IUlhHAgAAEIJEIAoQtLFEIXlhDCgQAEIJAIAYQuLFEIXVhCCAcCEIBAIgQQurBEIXRhCSEcCEAAAokQQOjCEoXQhSWEcCAAAQgkQgChC0sUQheWEMKBAAQgkAgBhC4sUQhdWEIIBwIQgEAiBBC6sEQhdGEJIRwIQAACiRBA6MISpUXoWgpLWHl4h6OlrrwB0BACI0BAy7w7aWxsbExDvrRMvFoKS0NN2TFoqSst+WAcEIhBQMu8i9BjVEeLPrUUVgsEopoidFHpIBgIRCGgZd5F6FHKw79TLYXlT0BWS4QuKx9EA4EYBLTMuwg9RnW06FNLYbVAIKopQheVDoKBQBQCWuZdhB6lPPw71VJY/gRktUTosvJBNBCIQUDLvIvQY1RHiz61FFYLBKKaInRR6SAYCEQhoGXeRehRysO/Uy2F5U9AVkuELisfRAOBGAS0zLsIPUZ1tOhTS2G1QCCqKUIXlQ6CgUAUAlrmXYQepTz8O9VSWP4EZLVE6LLyQTQQiEFAy7yL0GNUR4s+tRRWCwSimiJ0UekgGAhEIaBl3kXoUcrDv1MtheVPQFZLhC4rH0QDgRgEtMy7CD1GdbToU0thtUAgqilCF5UOgoFAFAJa5l2EHqU8/DvVUlj+BGS1ROiy8kE0EIhBQMu8i9BjVEeLPrUUVgsEopoidFHpIBgIRCGgZd5F6FHKw79TLYXlT0BWS4QuKx9EA4EYBLTMuwg9RnW06FNLYbVAIKopQheVDoKBQBQCWuZdhB6lPPw79S2sU6dOmSlTplTeeOfOnWbhwoX+QbVsuW3bNrN06dLKXsbGxrK/X7Nmjdm4cWP2//nf+d527ty55sCBA2bBggVm165dvt1k7aQLfVC9bNiwwbwxZ3+r8fs0XvTBQrN69eoJTXfv3m2eOPk90y9my7qubbHT0xtPGVvb/X5ccpfHUsWqGItLXz6saCODgO+8KyP630aB0IVlxLewEPpvE4nQL7BA6NUfIotveYQubALsKBzfebejcPveFqELy4hvYRWFPmfOHDN16tTe6kXSCv0rX/mK+fnPf27efffdcatxVuh+hZjXi92R2L9/v7n00kvNihUrggj94k3GbN26tfFOxZX/9Dvmy1/+cm9Ad9xxR7Z7VF6h25hPnDhhbr311t49iqviTZs2mXLbfiv0V155xXz44YfjIL7wu9tqoSL0WkQjcYHvvCsNDkIXlhHfwioKfd26dcauUvNtbklCt7HYLfjy9jpC9yvEKiENklSTu/gKvXyPb39qvbEfMstCz3cQ8njLW+72z+W2/YTuuyWO0JtUhN5rfeddaUQQurCM+BaWi9CLZ9lVYi1uVdtr8zN5+wFh/fr140gVBVx8oercuu6+tn1Z6PbM3wqg/GNjsa/lq7ry6/b8vck46tLvK4q6fkO97iL0QWfWD05Zkz1rUP6x4y4KPX89l3KT+LsU+vwDc42tmfJP3YeLKh5Nxsy1aRHwnXeljRKhC8uIb2EVhT5r1qxsyz0XYr5CrxNrExH6Cn3RokXm6NGj5vDhwxn5fg/F3XTTTeatt96akJ2HH37Y3HLLLearX/3quNcuu+wy8/u///vm3/7t30Ze6GVog4S+/ppHzLx58xoL/Wtn7srabNmyxZy95zfj2n/iB1eYxYsX9/5uyZIllVvuoVfo5fs+N/kFs/D9PzIPPPDAhPHt3bvX/NWxv+k9oFeOBaELmxgjh+M770YOq3H3CL0xsrgNfAtr0ENxa9euNXfffaKmxAUAACAASURBVLc5cuTIuG348tZ3E6E/88wz5uWXX+7B2L59uzl58mTlk+WDnnL/x3/8R3PbbbeZxx9/fNw2fB7LlVdeaW644Qazb9++7F7l44TPfvaz2TnsL37xi94HhLzttGnTzBe/+EWzefNmYz9I3H///WbZsmWNEpjiCr2f0A8ePJh90Lv66quzS4pb2sePHzevvvpqr6mVYXGFvmPHDnPs2LHs2YcD8382QYTFe9Y9qV7eVQi15V6+b3kL//XXX88+7E2ePLl2+9+Od/r06WbmzJk9Vo0Kh4uTIuA770obJEIXlhHfwhok9CoRtt1yP3PmjPn1r3/do2e3wK0w6rbcq3A3jaX8fEC/DybFe9kJ2sZ40UUXNcp4ikK3K1X78/bbb5tPbriuJ9+qM+pvzXjU3HjjjWbPnj3m8ePfGcdm0Bm669lzscO6be62Z+h1Qq86k2/y4aJR4XBxUgR8511pg0TowjLiW1jlM3R75l13rt5mhe675R5i+7/ugb98hV5ObdWzAHXpT1HoTSTleobehEPd97ebxNf0objyh5ByLG3O8+tqhdfTJeA770obMUIXlhHfwupS6HardtWqVeb999+vXaHHEHqbDyZ16W8isrq+Yrze5KG4qhVwOabi9vegFfpdv7wza2rZn793fC8IPUam6TMmAd95N2ZMPn0jdB9qEdv4Flb5objZs2ebc+fOmeeffz6Ltnz2XPVwWr6yveqqq7InyYttBz3lvnLlSuN6hm7va8+27YNxxYf2+km5KpbiCn3QOOz2f93T+nWpTEXo77zzTrbFfskll5jly5dnwyo/6FV1Lnzzoc+ZGTNmZNcX25afcvc9Q3/22Wd7iO25vP3Ja9zGbJ9/yJ+yL3/gsG3LD9QV81X8TXF5fPbcu0l//XYLOEOve2foet133pVGAaELy4hvYTU9Q8+Hbc+UL7/8cnP69One0+FlJCG/tlaFu+oM/frrrzfvvffehMttLPYp/rvuuvCUdf4z6k+5V3F1eXI734IutrcPv923d9XAr62FOEMvxxzjV78u+Y8vZw9Dln/qzvOrYhM2VRBOQAK+827AEIJ0hdCDYAzXiW9h+Qo9j7z4/W0JQh90Dl5coZdjHdXvoYcUuu2rzffQXZ9yH4bQXWNx+fAT7l1OT9II+M670saB0IVlREphVZ3JC0M1lHCkb7kPBQI3gYByAlLm3baYEXpbgoHbd1lYTz75pDl06JA5f/68OXv2bPb9bfvj83R4YCyddYfQO0PPjSEwNAJdzrshB4nQQ9IM0FeXhTXo162WH4oLMNQkukDoSaSJICHQikCX826rwEuNEXpImgH66rKwEPrEBCL0AEVNFxAQTqDLeTckGoQekmaAvrQUVgAUIrpA6CLSQBAQiEpAy7yL0KOWSfPOtRRW85HLbIHQZeaFqCAQkoCWeRehh6yKAH1pKawAKER0gdBFpIEgIBCVgJZ5F6FHLZPmnWsprOYjl9kCocvMC1FBICQBLfMuQg9ZFQH60lJYAVCI6AKhi0gDQUAgKgEt8y5Cj1omzTvXUljNRy6zBUKXmReigkBIAlrmXYQesioC9KWlsAKgENEFQheRBoKAQFQCWuZdhB61TJp3rqWwmo9cZguELjMvRAWBkAS0zLsIPWRVBOhLS2EFQCGiC4QuIg0EAYGoBLTMuwg9apk071xLYTUfucwWCF1mXogKAiEJaJl3EXrIqgjQl5bCCoBCRBcIXUQaCAICUQlomXcRetQyad65lsJqPnKZLRC6zLwQFQRCEtAy7yL0kFURoC8thRUAhYguELqINBAEBKIS0DLvIvSoZdK8cy2F1XzkMlsgdJl5ISoIhCSgZd5F6CGrIkBfWgorAAoRXSB0EWkgCAhEJaBl3kXoUcukeedaCqv5yGW2QOgy80JUEAhJQMu8i9BDVgV9QQACEIBAcgQQurCUsZISlhDCgQAEIJAIAYQuLFEIXVhCCAcCEIBAIgQQurBEIXRhCSEcCEAAAokQQOjCEoXQhSWEcCAAAQgkQgChC0sUQheWEMKBAAQgkAgBhC4sUQhdWEIIBwIQgEAiBBC6sEQhdGEJIRwIQAACiRBA6MIShdCFJYRwIAABCCRCAKELSxRCF5YQwoEABCCQCAGELixRCF1YQggHAhCAQCIEELqwRCF0YQkhHAhAAAKJEEDowhKF0IUlhHAgAAEIJEIAoQtLFEIXlhDCgQAEIJAIAYQuLFEIXVhCCAcCEIBAIgQQurBEIXRhCSEcCEAAAokQQOjCEoXQhSWEcCAAAQgkQgChC0sUQheWEMKBAAQgkAgBhC4sUQhdWEIIBwIQgEAiBBC6sEQhdGEJIRwIQAACiRBA6MISpUXoWgpLWHl4h6OlrrwB0BACI0BAy7w7aWxsbExDvrRMvFoKS0NN2TFoqSst+WAcEIhBQMu8i9BjVEeLPrUUVgsEopoidFHpIBgIRCGgZd5F6FHKw79TLYXlT0BWS4QuKx9EA4EYBLTMuwg9RnW06FNLYbVAIKopQheVDoKBQBQCWuZdhB6lPPw71VJY/gRktUTosvJBNBCIQUDLvIvQY1RHiz61FFYLBKKaInRR6SAYCEQhoGXeRehRysO/Uy2F5U9AVkuELisfRAOBGAS0zLsIPUZ1tOhTS2G1QCCqKUIXlQ6CgUAUAlrmXYQepTz8O9VSWP4EZLVE6LLyQTQQiEFAy7yL0GNUR4s+tRRWCwSimiJ0UekgGAhEIaBl3kXoUcrDv1MtheVPQFZLhC4rH0QDgRgEtMy7CD1GdbToU0thtUAgqilCF5UOgoFAFAJa5l2EHqU8/DvVUlj+BGS1ROiy8kE0EIhBQMu8i9BjVEeLPrUUVgsEopoidFHpIBgIRCGgZd5F6FHKw79TLYXlT0BWS4QuKx9EA4EYBLTMuwg9RnW06FNLYbVAIKopQheVDoKBQBQCWuZdhB6lPPw7dSmsU6dOmSlTplTeZOfOnWbhwoX+AbRsuW3bNrN06dK+vbSNb82aNWbjxo1Z/2NjY2bu3LnmwIEDZsGCBWbXrl3j7luMxfe+0oU+qF42bNhg3pizv2VGmzdf9MFCs3r16gkNd+/ebZ44+T3TL2bLuq5tsdPTG08Zm9d+Py65y2OpYlWMxaWv5qRoIYWAy7wrJdZBcSB0YVlyKSyE7if0jz76yOzbty/L+Pr1650yL30iH1QvTz/9tPnxNa85jTPkRXVSDiV087/Om5deegmhh0zeiPblMu+mgAahC8uSS2EVhT5nzhwzderU3krFdyUaCsO//uu/mr/927/NunvxxRfNyZMnzbRp08zixYuzv3vooYfMzJkzvW/XZIVejMXe9+///u/Hre5dgkhF6HaXYv/+/ebSSy81K1asyIbWdoV+8SZjtm7dmvXVhENR6Js2bephPnz4sDn4+cO9FbqN+cSJE+bWW2/t3aPc9o477sh2o/LVfTFnxfheeeUV8+GHH45L6Qu/u602xazQaxGNxAUu824KIBC6sCy5FFZR6OvWrcu2nfNt7q6FXsQ5aDvcF3sToZfvUW7rEkMTkbn0F/qaKiENklST+4cQehW/cnz5n8tb7vbP3/7UemM/tNYJ3TdPCL1JRei91mXeTWH0CF1YllwKy0Xo5fNj++d+Z8/2tfxM3n5AKG9HF0VYxFV1bu0idNu/XT2Wf/L+Jk2aVJkV+2GlyTjKH3SKbfMbVI23eHNfUQyrrFyEPmiL+8Epa7LnD8o/dtxFoeevV4m13Lbu7HmYQp9/YK6xOS7/lM/z892MQayGlVPuM3wCLvPu8KNqfkeE3pxZ1BYuhVUU+qxZs7ItdztB2Z98hS5Z6A8++KD57ne/O4HjzTffbF577TVz+eWXVzL+yU9+km3ju34wGVWhl+ENktT6ax4x8+bNayz0r525K2uzZcsWc/ae34xrXxT6s88+23vt3XffNQfm/6y35V6WaNsV+id+cEXvaMfe9LnJL5iF7/+ReeCBByaMb+/eveavjv1N31iqPuBEfePTeacEXObdTgN0vDlCdwQ1rMtcCmvQQ3Fr1641d999tzly5Mi4bfgmK9vyCv2ZZ54xL7/8cg/B9u3bs7Nx3xV6ccX/h3/4h9nZ75kzZ7L+irsF11xzjbniiiuMFUH+YaXfOOw5/Re/+EWzefNms2jRInP//febiy66qC+DlStXZn0uW7Ys+6/fT4or9H5CP3jwYPbh7+qrr84uKW5pHz9+3Lz66qu9plaGxRX6jh07zLFjx7JcVEm5eE/Xh+JCC7183/IHhNdffz07npo8eXJvC7/fboEd7/Tp03vPe0ivg2HNT1rv4zLvpjB2hC4sSy6FNUjodjhV5+pthG5l++tf/7pHyj7EZOUQQujlr56Vt/8HrbKLbYtptJOxjdGu5ovPFpQZuKRe+kReteVuV6r25+233zaf3HBdbxVatQL+1oxHzY033mj27NljHj/+nXFIBp2hu549Fzus2+Zuu0KvE3rVmXyT7X+XeuGaNAm4zLspjAyhC8uSS2GVz9DtirruXL2N0EOfoQ96sC2E0F0/1LikPkWhN5GU6xl6Gw7lB9uaxNf0objyh5DyeX6bWFzqhWvSJOAy76YwMoQuLEsuhdWl0O227KpVq8z7778vaoVe3q532aVwSX0bkbn03/aaJg/FVa2A+23Plx+KK3O465d3Zk3tB7Dz9w4eRRuJIvS2FUJ7FwIu865LP11fg9C7zkDp/i6FVX4obvbs2ebcuXPm+eefz3ory8yeKR89etTY7wHbn+JW9VVXXZX9Zrli20FPuduz55Bn6OX+iiv0qgf+muw0DNqu13aG/s4772Rb7JdccolZvnx5lufyGXXVufDNhz5nZsyYkV1fbFsWepMz9PLDaUuWLBn3XfK8xm3M9nvo+VP25Q8c9oG6ctvi26XqjN+eezfpjzN0YRNgR+G4zLsdhdbotgi9Ea74F7sUVtMz9Dxq+5CYfYL89OnTvV+ZWh7RML629ud//ufm7/7u7ybALK+yyxfwtbWJ9TeoXh577DGz7zNvDPxVq/kKuNizffjtvr2rBn5tLcQZenk0TX/162XP/Dfzwgsv9H1T2v6W/MeXswckyz915/lVscV/93OHrgi4zLtdxdbkvgi9Ca0hXOtSWL5Cz8Pv9zCZfX0YQh90Jl9coSP0+oIbVC8u362uErq9a6jvoRdHEPp3uVd9T754P5cPCP1W6Ai9vvY0XeEy76YwXoQuLEtdFFbVmbwwLJ2FI/0MvTMw3BgCigh0Me/GwIfQY1Bt0eewCuvJJ580hw4dMufPnzdnz57Nvr/db4XeYjjJN0XoyaeQAUCglsCw5t3aQFpegNBbAgzdfFiFZR+Ey3+7XHEMdb8KNfR4pfeH0KVniPgg0J7AsObd9pEO7gGhxybcsP9hFRZCd0sMQnfjxFUQSJnAsObd2IwQemzCDfvXUlgNhy32coQuNjUEBoFgBLTMuwg9WEmE6UhLYYWh0X0vCL37HBABBGIT0DLvIvTYldKwfy2F1XDYYi9H6GJTQ2AQCEZAy7yL0IOVRJiOtBRWGBrd94LQu88BEUAgNgEt8y5Cj10pDfvXUlgNhy32coQuNjUEBoFgBLTMuwg9WEmE6UhLYYWh0X0vCL37HBABBGIT0DLvIvTYldKwfy2F1XDYYi9H6GJTQ2AQCEZAy7yL0IOVRJiOtBRWGBrd94LQu88BEUAgNgEt8y5Cj10pDfvXUlgNhy32coQuNjUEBoFgBLTMuwg9WEmE6UhLYYWh0X0vCL37HBABBGIT0DLvIvTYldKwfy2F1XDYYi9H6GJTQ2AQCEZAy7yL0IOVRJiOtBRWGBrd94LQu88BEUAgNgEt8y5Cj10pDfvXUlgNhy32coQuNjUEBoFgBLTMuwg9WEmE6UhLYYWh0X0vCL37HBABBGIT0DLvIvTYldKwfy2F1XDYYi9H6GJTQ2AQCEZAy7yL0IOVRJiOtBRWGBrd94LQu88BEUAgNgEt8y5Cj10p9A8BCEAAAqIJIHRh6WElJSwhhAMBCEAgEQIIXViiELqwhBAOBCAAgUQIIHRhiULowhJCOBCAAAQSIYDQhSUKoQtLCOFAAAIQSIQAQheWKIQuLCGEAwEIQCARAghdWKIQurCEEA4EIACBRAggdGGJQujCEkI4EIAABBIhgNCFJQqhC0sI4UAAAhBIhABCF5YohC4sIYQDAQhAIBECCF1YohC6sIQQDgQgAIFECCB0YYlC6MISQjgQgAAEEiGA0IUlCqELSwjhQAACEEiEAEIXliiELiwhhAMBCEAgEQIIXViiELqwhBAOBCAAgUQIIHRhiULowhJCOBCAAAQSIYDQhSUKoQtLCOFAAAIQSIQAQheWKIQuLCGEAwEIQCARAghdWKIQurCEEA4EIACBRAggdGGJQujCEkI4EIAABBIhgNCFJUqL0LUUlrDy8A5HS115A6AhBEaAgJZ5d9LY2NiYhnxpmXi1FJaGmrJj0FJXWvLBOCAQg4CWeRehx6iOFn1qKawWCEQ1Reii0kEwEIhCQMu8i9CjlId/p1oKy5+ArJYIXVY+iAYCMQhomXcReozqaNGnlsJqgUBUU4QuKh0EA4EoBLTMuwg9Snn4d6qlsPwJyGqJ0GXlg2ggEIOAlnkXoceojhZ9aimsFghENUXootJBMBCIQkDLvIvQo5SHf6daCsufgKyWCF1WPogGAjEIaJl3EXqM6mjRp5bCaoFAVFOELiodBAOBKAS0zLsIPUp5+HeqpbD8CchqidBl5YNoIBCDgJZ5F6HHqI4WfWoprBYIRDVF6KLSQTAQiEJAy7yL0KOUh3+nWgrLn4CslghdVj6IBgIxCGiZdxF6jOpo0aeWwmqBQFRThC4qHQQDgSgEtMy7CD1Kefh3qqWw/AnIaonQZeWDaCAQg4CWeRehx6iOFn1qKawWCEQ1Reii0kEwEIhCQMu8i9CjlId/p1oKy5+ArJYIXVY+iAYCMQhomXcReozqaNGnlsJqgUBUU4QuKh0EA4EoBLTMuwg9Snn4d1ourFOnTpkpU6ZUdrhz506zcOFC/5tVtFyzZo3ZuHHjhFcWLFhgtm3b1otl3bp1Zv369d73tn0tXbo0a181jrlz55oDBw4Ye99du3Z536dtQ+lCHzQRbdiwwbwxZ39bBI3bL/pgoVm9enWvnWVY/rv8xd27d5snTn7P9BuHS9tygIOY3HrrreaK1Z8YOKa8fRW/4jik10bjxI1wA4QuLPla3lwI/UJhIXS3N9igiejpp582P77mNbeOAl6F0APCpKuhEEDoQ8HsfpNREPqcOXPM1KlTsxVtv5WtO7HqK4sr9Hvvvbd30cyZM82qVatYobcFHLh9PhHZ3Yz9+/ebSy+91KxYsSK7S9sV+sWbjNm6dWvWV5P315X/9DvmvvvuM3Y1nLctSn7Tpk3mjjvuyGqpvEK34zhx4kSjtv1W6K+88or58MMPx73813/91+a/Pz6TFXrgOky9O4QuLINNJhxhoY8LZ9AK3W5z25XroK3qtmMrCn1sbGxcd8Xtf7bc25IO075qe3jQlnGTu/oK3d6j3La8Vf3tT6039gNqWej5h5B8DOUtd/vnctt+QvedE9hyb1IlOq5F6MLy6PvmFTaMCWeJZYlWCb18Hm3/nJ+DWykXt6/rzsGbCL0YS5mjva8937cTdvmn/MGkqm0ec+i2NqZ85Vh13/LfSa8rF6EPOp9+cMqa7DmFqnEXpZy/ngu47n0jWejzD8w1tgbLP3UfLlKrjboc8fpvCSB0YdUgfeJ1xTVohT5r1qxsyz2XZP4wWSyhr1y5shf2pz/9afMXf/EX47bc64Q+f/588+abb4oSuo3p9ttvr0xHeUfCXiS9rlxW44OEvv6aR8y8efMaC/1rZ+7K2mzZssWcvec3E9pLEfonfnCFWbx4cS++xx57zHzzm980DzzwwISY9+7da/7q2N/0PlSXdwsQuusslt51CF1YzqRPvK64mjwUt3btWnP33XebI0eOjNuGD7VCL8Zc9ZS7FeNTTz3Vu8zK+/Dhw9mfizsDV111lbntttt61y1btsxcdNFFvZgXLVpkjh492retXVU///zzWXu7smpy33Lb4oeQqvumNmk3EfrBgwezD4RXX311Nszi9vXx48fNq6++2hv+c5NfGLdtvmPHDnPs2DHz7rvvmgPzfzZBemVuUoRefkDP7s7ceeedvafwX3/99WwHa/LkybXb/5bB9OnTjX2eJIUPe65zDteZvt+ySI0NX1sTlrEmQs8FV96GH5bQ7QeKX/3qVz2CjzzyiPn+97/fE/pNN91k3nrrLXPLLbeYH/7wh9nf24e2Pvaxj5nt27d7fwipu++gI4Y6VhqEblel9uftt982n9xwXW+yqjqP/taMR82NN95o9uzZYx4//p1xwx90hl73QSIVoVedyZfHNug8X9j0QTieBFihe4KL1WwUVuj5g2h15+qhhF73UFzdlrvrGbo9OmgSc919R13oTYTkeobe9P0lReh2vinGUl6hI/RYM3Ja/SJ0YflqOuEIC78XTt1T7vaXuUgU+hNPPJGt8uyZqv2xHwbsFvzJkyezP585cybb6gyxq1AUetV9EfqFr5q5PDFefh8UV6ODVuh3/fJCLu0HsfO//XZjrzuELnWGIa4qAghdWF2MgtDtQ3GzZ882586dG3emXHcunAvOnmWXz5TLv+3N9yn3qvPohx9+2HzwwQdZpQyKedAKvSpm1/FWnfvXtS2XtfS6yieid955x6x9/1Fzz9kVZvny5ZVCrzoDvvnQ58yMGTOy6y+55JJeWzvuopRDnqE/++yzZsmSJZXfQ7fjsN9Dz5+8Lx8TVLXt96Ekj9mee+f9lVfog2IpfyDiDF3YpB8wHIQeEGaIrqRPvK5jbHuGnt/HPnR2+eWXm9OnT/e+tlaOoeq75L5CL/ddfCiu+Nq0adPMQw89ZK699lqnM/SqmENtuVfFnKrQq+rLPtG97zNvDPy1qvl3uovt7cNv9+1dNU7o+ev9vtpVvv+gFXrx2i5+9av9wHP//fdPQMbX1lxnKX3XIXRhOUXoF34vuqtY7XVdCD2/b93DaSG+h163QtcudJevXVUJ3XIpr9DLQq97+0sWevEp90EfLlz41XHg9TQIIHRhedIq9NCYQ/62t9CxddHfoB2JXGxdxMU9IQCB4RFA6MNj7XQnhN4f05NPPmkOHTpkzp8/b86ePWs2b96cXdz217c6JUbYRe+9956xPD766KMssn/5l3/Jvlttf1L8xTLC8BIOBJIkgNCFpQ2h90/IoK+PtfknUIWVgFM49p9i1fSrX50GzUUQgMBAAghdWIEgdITuUpII3YUS10BgtAggdGH5RujCEqIkHC11pSQdDAMCUQgg9ChY/TvVMvFqKSz/TMpqqaWuZFElGgjIIqBl3uV3ucuqKzX/SIAwrN7hIHRvdDSEQDIEELqwVGmZeLUUlrDy8A5HS115A6AhBEaAgJZ5lxW6sGLVUljCsHqHg9C90dEQAskQ0DLvInRhJaelsIRh9Q4HoXujoyEEkiGgZd5F6MJKTkthCcPqHQ5C90ZHQwgkQ0DLvIvQhZWclsIShtU7HITujY6GEEiGgJZ5F6ELKzkthSUMq3c4CN0bHQ0hkAwBLfMuQhdWcloKSxhW73AQujc6GkIgGQJa5l2ELqzktBSWMKze4SB0b3Q0hEAyBLTMuwhdWMlpKSxhWL3DQeje6GgIgWQIaJl3EbqwktNSWMKweoeD0L3R0RACyRDQMu8idGElp6WwhGH1Dgehe6OjIQSSIaBl3kXowkpOS2EJw+odDkL3RkdDCCRDQMu8i9CFlZyWwhKG1TschO6NjoYQSIaAlnkXoSdTcgQKAQhAAAIxCCD0GFRb9MlKqgU8mkIAAhAYYQIIXVjyEbqwhBAOBCAAgUQIIHRhiULowhJCOBCAAAQSIYDQhSUKoQtLCOFAAAIQSIQAQheWKIQuLCGEAwEIQCARAghdWKIQurCEEA4EIACBRAggdGGJQujCEkI4EIAABBIhgNCFJQqhC0sI4UAAAhBIhABCF5YohC4sIYQDAQhAIBECCF1YohC6sIQQDgQgAIFECCB0YYlC6MISQjgQgAAEEiGA0IUlCqELSwjhQAACEEiEAEIXliiELiwhhAMBCEAgEQIIXViiELqwhBAOBCAAgUQIIHRhiULowhJCOBCAAAQSIYDQhSUKoQtLCOFAAAIQSIQAQheWKIQuLCGEAwEIQCARAghdWKIQurCEEA4EIACBRAggdGGJQujCEkI4EIAABBIhgNCFJQqhC0sI4UAAAhBIhABCF5YohC4sIYQDAQhAIBECCF1YohC6sIQQDgQgAIFECCB0YYlC6MISQjgQgAAEEiGA0IUlCqELSwjhQAACEEiEAEIXliiELiwhhAMBCEAgEQIIXViiELqwhBAOBCAAgUQIIHRhiULowhJCOBCAAAQSIYDQhSUKoQtLCOFAAAIQSIQAQheWKIQuLCGEAwEIQCARAghdWKIQurCEEA4EIACBRAggdGGJQujCEkI4EIAABBIhgNCFJQqhC0sI4UAAAhBIhABCF5YohC4sIYQDAQhAIBECCF1YohC6sIQQDgQgAIFECCB0YYlC6MISQjgQgAAEEiGA0IUlCqELSwjhQAACEEiEAEIXliiELiwhhAMBCEAgEQIIXViiELqwhBAOBCAAgUQIIHRhiULowhJCOBCAAAQSITASQj937lwi6TBm2f9ZnkysBAoBCEAAAnIIIHQ5ucgiQejCEkI4EIAABBIhgNCFJQqhC0sI4UAAAhBIhABCF5YohC4sIYQDAQhAIBECCF1YohC6sIQQDgQgAIFECCB0YYlC6MISQjgQgAAEEiGA0IUlCqELSwjhQAACEEiEwEgIXWouqr5Oh9ClZou4IAABCMgmgNA7zA9C7xA+t4YABCCgjABC7zChCL1D+NwakNa3tgAAAvhJREFUAhCAgDICCL3DhFYJ/eP/4+MdRsStIQABCEAgVQIfPfVRqqGPi3vS2NjYmIqRMAgIQAACEIDACBNA6COcfIYOAQhAAAJ6CCB0PblkJBCAAAQgMMIEEPoIJ5+hQwACEICAHgIIXU8uGQkEIAABCIwwAYQ+wsln6BCAAAQgoIcAQteTS0YCAQhAAAIjTAChj3DyGToEIAABCOghgND15JKRQAACEIDACBNA6COcfIYOAQhAAAJ6CCB0PblkJBCAAAQgMMIEEPoIJ5+hQwACEICAHgIIXU8uGQkEIAABCIwwAYQ+wsln6BCAAAQgoIcAQteTS0YCAQhAAAIjTAChj3DyGToEIAABCOghgND15JKRQAACEIDACBNA6COcfIYOAQhAAAJ6CCB0PblkJBCAAAQgMMIEEPoIJ5+hQwACEICAHgIIXU8uGQkEIAABCIwwAYQ+wsln6BCAAAQgoIcAQteTS7Ejue6667LYjhw5IjZGAoMABCCQOgGEnnoGE4gfoSeQJEKEAASSJ4DQk08hA4AABCAAAQgYg9CpAghAAAIQgIACAghdQRIZAgQgAAEIQCBpoXM2SwFDAAIQgAAELhBA6FQCBCAAAQhAQAGBpIWugD9DgAAEIAABCAQhgNCDYKQTCEAAAhCAQLcEEHq3/Lk7BCAAAQhAIAgBhB4EI51AAAIQgAAEuiWA0Lvlz90hAAEIQAACQQgg9CAY6QQCEIAABCDQLQGE3i1/7g4BCEAAAhAIQgChB8FIJxCAAAQgAIFuCSD0bvlzdwhAAAIQgEAQAgg9CEY6gQAEIAABCHRLAKF3y5+7QwACEIAABIIQQOhBMNIJBCAAAQhAoFsCCL1b/twdAhCAAAQgEIQAQg+CkU4gAAEIQAAC3RJA6N3y5+4QgAAEIACBIAQQehCMdAIBCEAAAhDolgBC75Y/d4cABCAAAQgEIYDQg2CkEwhAAAIQgEC3BBB6t/y5OwQgAAEIQCAIAYQeBCOdQAACEIAABLol8P8BbOiJEyRmrXcAAAAASUVORK5CYII=';
    shopKitsMenu.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAgAElEQVR4Xu2dT+gtTVrf+/zekUmUN+C4MJAEoptIgs6AC3GhDuhCuX9/E4gB0U0WrgLZmIhO1ElcJEQw4mrARXbGzb33vX+cRYyOQxizUFAhwZWCMSAERhlBHMf3d0J1dZ2urq7qrq5T3efppz9nmPfee079/T5P96eequrq0/l8Pjd8UAAFUAAFUAAFdq3ACaDv2n40HgVQAAVQAAVaBQA6joACKIACKIACChQA6AqMSBdQAAVQAAVQAKDjAyiAAiiAAiigQAGArsCIdAEFUAAFUAAFADo+gAIogAIogAIKFADoCoxIF1AABVAABVAAoOMDKIACKIACKKBAAYCuwIh0AQVQAAVQAAUAOj6AAiiAAiiAAgoUAOgKjEgXUAAFUAAFUACg4wMogAIogAIooEABgK7AiHQBBVAABVAABQA6PoACKIACKIACChQA6AqMSBdQAAVQAAVQAKDjAyiAAiiAAiigQAGArsCIdAEFUAAFUAAFADo+gAIogAIogAIKFADoCoxIF1AABVAABVAAoOMDKIACKIACKKBAAYCuwIh0AQVQAAVQAAUAOj6AAiiAAiiAAgoUAOgKjEgXUAAFUAAFUACg4wMogAIogAIooEABgK7AiHQBBVAABVAABQA6PoACKIACKIACChQA6AqMSBdQAAVQAAVQAKDjAyiAAiiAAiigQAGArsCIdAEFUAAFUAAFADo+gAIogAIogAIKFADoCoxIF1AABVAABVAAoOMDKIACKIACKKBAAYCuwIh0AQVQAAVQAAUAOj6AAiiAAiiAAgoUAOgKjEgXUAAFUAAFUACg4wMogAIogAIooEABgK7AiHQBBVAABVAABQA6PoACKIACKIACChQA6AqMSBdQAAVQAAVQAKDjAyiAAiiAAiigQAGArsCIdAEFUAAFUAAFADo+gAIogAIogAIKFADoCoxIF1AABVAABVAAoOMDKIACKIACKKBAAYCuwIh0AQVQAAVQAAUAOj6AAiiAAiiAAgoUAOgKjEgXUAAFUAAFUACg4wMogAIogAIooEABgK7AiHQBBVAABVAABdQA/cmre6ypWIE3z1/urndTPvlfvuuXmm/4hm8o6tNnPvOZ5rc//rtFecmEAigwVmCP95eYHQE63i1egfMvfbV5+/at+HaGDQTouzMZDT6oAgBdmOGJ0IUZpGJz/uF///vNL/7iL1YscZuiAPo2OlMLClyrAEC/VsHK+QF6ZUEFFQfQh8Zgyl2Qc9IUFQoAdGFmBOjCDFKxOQAdoFd0J4pCgZECAF2YUwB0YQap2ByADtAruhNFoQBAl+4DAF26hcrbB9ABern3kBMF5hUgQp/XaNMUAH1TuTetDKAD9E0djsoOpwBAF2ZygC7MIBWbA9ABekV3oigUYMpdug8AdOkWKm/ftUA3u8Jv8Zk6/IWDZW5hEepEgbgCROjCPCMX6FoMJ0z+UXNy7ZHTj2uBXrMtOe3NSQPQc1QiDQpso4AWLhzupDgthtvGzctrqQlRgM6Ue7knkhMF5hXQwgWAPm/rNsWf//mfN1//9V+fmdom++mf/unmZ37mZybzfOITn2h+7/d+r/me7/me5vOf//yi8l3e3EwldeSWHaYD6NPKEaGXehb5UKC+AgC9vqZXlZgLkFLDAfRl5sm1R06pROhE6Dl+QhoUKFWglAul9a2Vjwg9U1kf6B//+McbEx3/9V//dfPLv/zLbQmx73Ii9J/6qZ9q/viP/7j5lm/5lubHf/zHM1tjk7kI/Ru/8Rub7//+72+/e/36dfNnf/ZnTew7IvRF8q6amAh9VXkpHAUWKQDQF8m1fuLciLDUcD7QHahzv1ur97Hp+tzv1mqTKzfXHjntIEInQs/xE9KgQKkCpVworW+tfETomcrmwjtMZwB7fx9/V/v5fL5E2SZ6fvXqVXKd/jd+4zeaT37yk4PW5sI7THc6naK9NnWYNvzCL/zC6PewfWZQ4/ctbB9An3YsIvTMC49kKLCBAgB9A5GXVJELkFLDaQG6ea/4+++/H5X2137t15o3b94A9CWOV5j2GqAXVpmVLfc6yipMQCJ0FmCEHTShlAvSukaEnmmRUqA/efKk+fmf//lLLb/zO7/T/MEf/EH776kI/Vu/9Vubj33sY81v/uZvtmlrRej+LECsDj9C/6Ef+qHmV3/1V9s1eSL0TEfJTAZoMoW6Mhk6XyngQbIDdGGGzo0sSg1XCvSf+ImfaL785S9f1Pr0pz/dfPazn50F+tyUtimgZMrdB/qP/diPNQbqP/IjP3IZNPhAnxpwzLUv1x45bsQaeo5KddLUtFudFl1XCkC/Tr+j5C7lgjR9iNAzLVIK9NI19Dlg1gB62PVwDR2gZzpHQTJAUyBaQRZ0LhDtgFkAujCj50YWpYarAfSf+7mfa774xS82L168EBGh//AP//AlOjcN+vZv//bGnHvuNsUB9PWcHNCsp61fMjpvo/PeaynlgrR+E6FnWqQG0L/v+76v+ZM/+ZOsNfQtInQz3f5t3/ZtFwV+8id/sl0OyAH63Bp/7gArR36m3HNUqpOmpt1ci9587HOzjXvypR+YTVOSAKCXqHa8PABdmM1zb0SlhqsB9FCyayLgW0+5+335uq/7uuZzn/tc813f9V2Xr3PtkeNGAD1HpTppathtBPDv/sp8477w0UGaWoAH6PPSk6JpSrkgTTsi9EyLAPRlz8nXAIMzzbVAX+v1qR/5yEca8//U53/8o/+Z/E0jaC4gzwH43HXXAf5asGvUeU46fl+uAEBfrtmqOXIBosVwq4pZofBce+RUdS3Qc+pYI82UBppAUxXkoSGuBLsmndfwUcq0CmjhAhE6Hr2KAgC9aY4A9BbmNSLyOS/8wkebkmgdoM8Jy+8AXaAP5AJEy0hMoAkGTcq1R04/iNBzVKqTZondNoO561oB1AF6Hb/QXooWLhCha/fUG/VvCRjmmgjQ5xSq93uO3VadYp/rysIpeIA+Jyi/E6EL9IGcG5Emwwk0ARF6YBSNU+5VovJ//s1N81//8DoXzozWAfp1Mh8lNxG6MEsDdFkGybVHTquJ0HNUqpNmzm4AfRud69RCKbkKAPRcpTZKN3cjcs3QYriNZC2uJtceORUA9ByV6qSZslsVmJtm1ojQTTkZUToReh2/0F6KFi6whq7dU2/UP4C+3i53c+b+r//6r0ct+x3f8R3N48ePi62esttVMDcAX/rJnZKfgfo1QL+FzktlIn0dBQB6HR2rlZILEC2GqybcSgXl2iOneiL0oUrmoJzf/vjvRqW7VquY3Yph7kAewrlWhO4UmID6NUDfWueca4E06yighQtE6Ov4x+FLBejrRehbg6YI6FPQBuiTZxQc/uZxAwEA+g1En6oyFyBaDCdM/lFzcu2R049ro86cOtZIs9Yu9y2BXh3mRujaQDdlJqJ0IvQ1PFtfmVq4QISuzzdF9Aig64jQFwM9B9Y5aZZ6MUBfqhjpPQUAujB3yAWIFsMJk58IPWKQvUfoi2GeG32vAfRElE6ELv1OIaN9WrhAhC7Dn9S1IneAldNxptyHKm015V4E9ByoA3TW0HMu/A3TAPQNxc6pKhcgWgyXo8kt0+TaI6eNAH1nQJ+DOkAH6DkX/oZptHDhcBH6hj5CVTtX4NqBxCGn3H2bb/XYmqszso7OlPvOL8KNmg/QNxI6t5qaEWFunaTTrcCRgV483R5ziZyDZXIPkplzuQDqAH1OMH43CgB0YX4A0IUZREFzAPpX1rFibMo9Fc0vbQFAX6oY6QG6PB8A6PJssvcWAfQNge6c5dr1dYC+98vuJu0nQr+J7OlKAbowgyhoDkC/AdCN31wDdYCu4MrbvgsAfXvNJ2sE6MIMoqA5AP1GQL8G6gBdwZW3fRcA+vaaA3RhmmtvDkAH6Fu+BEf79SS5fwBdmHWI0IUZREFzAPqNgM6Uu4KrZ19dAOjC7AXQhRlEQXMA+g2Afg3Mjc8x5a7gytu+CwB9e82ZchemufbmAPQNgc5ja9ovJ9H9A+jCzEOELswgCppzZKAb8xUdLpNziEzMN2ocLMNJcQquutt0AaDfRvdkrQBdmEEUNAegf65pvnuFKP3aafWUbwF0BVfdbboA0G+jO0AXprvm5gB0gM4ud81XeN83gC7MzkTowgyioDkAHaADdAUXckYXAHqGSFsmAehbqr1tXX/2c/+vub+/37bSpmk+8YlPNN/7vd9bXO/e37ZWvI4+p9gaU+6R6XbTjGtezvJbv/VbzRe/+MVob9b0jTn5+L2+AgC9vqZXlQjQr5JPdOZrI+VbdQ6gJ5TfCdDX9BvuV2uqu7xsgL5cs1VzcIGsKu9NCwfoQ/k/85nPNFtOBRftdp/ymNpAT0Tn10boazo996s11V1eNkBfrtmqObhAVpX3poUDdIA+6YAA/abXp4bKAbowKwJ0YQap2ByAflugm9qrR+m1/GMC5kTotUTWXw5AF2ZjgC7MIBWbA9BvD3SRUJ+BOUCveBEqLwqgCzMwQBdmkIrNAegygC4K6hkwB+gVL0LlRQF0YQYG6MIMUrE5AB2gj9wJoFe8wigKoAvzAYAuzCAVmwPQ5QD9EqWbv6xxLOyc33zho22KJ1/6gbmU7e/XPIeeVUFhIu5XhcKtlA2gryRsabFcIKXKyc8H0GUB3bVm841ymVG5rxZAl399S2ghQJdgBa8NAF2YQSo2B6DLBPolWt8iUi+AORF6xYtQeVEAXZiBAbowg1RsDkCXC/QL1M1f1gD7win20O2I0CteiIqLAujCjAvQhRmkYnMAumygD6bga4H9SpC7NgH0ihei4qIAujDjAnRhBqnYHIC+D6CPwO6+yIncO4C7LLmb3ubcDKDPKcTv7SzT85cqhDidz+ezhp4AdA1WjPcBoO8L6KEV281zM59aAGfKfU5pfo8pANCF+QVAF2aQis0B6PsGekVXWFwUEfpiyQ6ZAaALMztAF2YQmoMCAhS4BuhTb7UT0DWaUFEBgF5RzBpFAfQaKlIGCuhSAKDrsudavQHoaylbWC5ALxSObCigWAGArti4FbsG0CuKWaMogF5DRcpAAV0KAHRd9lyrNwB9LWULywXohcKRDQUUKwDQFRu3YtcAekUxaxQF0GuoSBkooEsBgK7Lnmv1BqCvpWxhuQC9UDiyoYBiBQC6YuNW7BpAryhmjaIAeg0VKQMFdCkA0HXZc63eAPS1lC0sF6AXCkc2FFCsAEBXbNyKXQPoFcWsURRAr6EiZaCALgUAui57rtUbgL6WsoXlAvRC4ciGAooVAOiKjVuxawC9opg1igLoNVSkDBTQpQBA12XPtXoD0NdStrBcgF4oHNlQQLECAF2xcSt2DaBXFLNGUQC9hoqUgQK6FADouuy5Vm8A+lrKFpYL0AuFIxsKKFYAoCs2bsWuAfSKYtYoCqDXUJEyUECXAgBdlz3X6g1AX0vZwnIBeqFwZEMBxQoAdMXGrdg1gF5RzBpFAfQaKlIGCuhSAKDrsudavQHoaylbWC5ALxSObCigWAGArti4FbsG0CuKWaMogF5DRcpAAV0KAHRd9lyrNwB9LWULy9UCdC2OVWhGcdm0+JU4YTdqEEDfSOidV6Plvns6n8/nnduibb6WG68Wx9LgU5r8Sos9lvYDoC9V7Jjptdx3AfoC/337/s82j//i0wtyLE+qxbGW91xmDi0DRZnqrt8qgL6+xhpq0HLfBegLvBGgLxBLSVKArsSQdAMFJhQA6MLcY+0br4G5+6wZpWtxLGHuUdyctf2quGFkRAEUqKaAlvsuEXqmSwD0TKGUJQPoygxKd1AgogBAF+YWa994Abowg2/UnLX9aqNuUA0KoABT7vvxgTVvvD7M15521zJS3I/nTLd0Tb/SolHtfpyaplHx6E1tYShvNQW03HeZcs9wEYCeIZLSJAD9NoY1UDefKbCnwB/L63/HgOE2NpVcK0AXZp01b7wAXZixN2zOmn61YTd2VZWDr2u0g7r/fQz0MVC77wD6rlxg88YC9M0lv93UKEAXZuwNmwPQNxT7EpU/NHfNXfsvA+KH7k+/JefmoTk3p+Z0ieEtsk/npjmHIwLzffedO0YrzMkU//Z2llQjQJdkjZVPigPowoy9YXNqAp2p3kzDGboaMjdNc9ecmocW6Q7v3d8S4I7XcG7hb0swQwA7FHAf7JJpF8XJALow49a88YZdA+jCjL1hc2r71RQ8pqaMQ/hoh9DlROpTh2ITdl8Y3BK/0As85bqw/NSF7zlReqzWnHyFjSXbRgoA9I2Ezq2m9o3Xrxeg51pBX7qafhVbA05t/gqB7a8FO5zpBontXR9Nx1bWc6HuK9UraabtXeRvZgJSevq1uNy6tdd3Hc/1CKDPKbTx7zVvvEToGxtPcHVr+tVct0/n82g92AST4Tqw7mjdj8ZDjJYA3U68248/aLBr9vMfM9dv1un7rXpmMOB/gP28itJSAHRhFlnzxkuELszYGzZnTb9a1g2DCRevDteBdQPdqZSCeWwP/BRex/MkRlWzCQ8QL/NITakBujBrrnnjBejCjL1hc9b0q7JuBBPuZ7OD20Jq7pntuTRl7dkiV+5Daqm2pFa+u+9PHzan83uzGm7RU+q4jQIA/Ta6J2td88YL0IUZe8PmLPWrt/evmscvn49aaL43H/dbmM7926VzBZj0/nd+2anvTd65+lyaWFtd3WEZsXKX1OX3KVXHlC6PXz5rs729/6DT8dz+vS/X/u5/hr9bu9g6TFoLdPPvJy+fjwZEsbakbJiy+4auSlVXKADQrxBvjaxLb7xL2iAN6CYiu+wCDjpyidbcQmv7DO4wvf9vl94VY8qNfRfTK1WXX5b7u1+ma3uqH7G0YTlTZbhyp/qda/+lfhW7sfvf+eAO4ezgHUI2J/8QYv2gIlVfD7bx4CP8LVZG7nc+uMPBTE6/woGOAbEDeg/mfl+Bqc/3n74OC38/v5vTMN+HQJ8aEKUGHbk+RTp5CgB0YTZZeuNd0nyAHp/MBehjL1oK9BCMsah7LkKPDQD8luUMFMKeLO2HPwiokTemiz9b4Q8O+qj5ofmau/earz70/jrWzv4Wj+yHU/OxAUdqsLPkfkJaeQoAdGE2AejDSNyH7VyEHkb7Iahj+Y35wyg5VU/u934UHis7p04XofkzDXMzAlOuvNSvlsDMgciPBqfyp6Z1pyLGPUboc7qkNYpPuQ+n6rs0p6Z5+/yD5tGrJ/a4mZbzw53uAF3YTX7F5gD0FcUtKXrpjfcyFfj+zzaP/+LTk1WWROgmz1y5sUpzHGtqqtqHlwMgQLdR2dRSRcoBlvqVHxH2keP8mnY4tezaE0bXsXS5U/SxqD38zv93OO3solO/X7HvYhrkzjKkBiep6Xlfp9i2wKk1djcFb9fSh4fVTM2UxPZBpAZbJfcy8myvQM59d/tWLa/x8G9bywGvFqDH1pZTa9u+K81F8C5tbiSeWuOOleOndQMUH86x38Pyp9bk5y6ZpUCfK4/f11Qg58GzubP61mwfZUtVAKALs0zpjdfBeiqaXgr0nDJT8uU4VkmE7kfrMTD67ZmarncwnYJrCOZc0JcCPWyL31d/MLJFhC7ssjhgc8JjYVPPqZceUnNASQ/Q5Zz77h5kIEJ//2cvdkpBfQnQ/bSSptwB+jZT7nu46HW3MfXM+mDOqXtkzYd9CP5wYKBbtaP3DqAL84BrI3S/OyGIc4CekyZHshzHCh8rS0WosYjXRdjhWrtLu2QzWY1d7oPbbPeo3Z4fW8uxMWnWUiB2yn3qDPhYhJ6Ysu/ex2qP3TXH8fbp7Fvbco+gXavflHutAjn33Wvr2CI/EboXoaegPgfr2O+mrDUj9NA5YpvhAHr6+fvci6t0oJhbPulqKjAF9LloPGyHg7R973o7EB4kAeI1LXfrsgD6rS0Q1H/NjXcOyFNAn8u7VCYtjrW031LTL/WrcHf01KNP7aCvO1Vu6eNuYV6nX2pXvPk9tmvd/z7Vnrkd+IOBsNcfv01+mrAdeTvYhx4SKyOsL26L4aNt4Uly4Q59p0msrKX9lurjtKtptNx3Dx+htze0RJRufvvRn/o/zWf/3T8Y+Xzq+8tNZeZRuNRFpMWxtNwklgLdATJ28EnsmfDc58Sn8i6tM7RNCqhTJ7u5MuYGIrl1+eCcGpTE+tqnjx8Na/vRnxQ3bpM9Qe7y9Fp38Iw9Pc7+z3ze3b/OHoBp8f+j9EPLffewQP97/+lHB74ag3apMxvYu8///bHPLipGi2Mt6rTgxDWAHp5wlgvBXNBOQS4GSvedD+XLQDQyYzD17Hvu8+WxuqbalprZcH31XWZ83n0fhffPmrsz4FOHzzzveN6fJPfo5VNz5MzgzPdwoJbScuqMfMHuftimabnvHgroIcRD760BdR/mYfk5cNfiWFruDLWA7kN3DtSx6HdphJ5zbvrSenIHIr7tUwfFLNEjvww/Qg//fupeymKi7ocIqMdT8TkH9qT8nINm9nUH0HLfPQTQUyCPAXZq+n3ORcNNcEvqdWVrcaw5rfby+1KgX7OGHkadfjQ6FRmW1jkVoafqnoLcXHlTMwmxAUg4azA3AOnT9zB335kjXt89fzNwO/eillg9sUGEny5ca596K95efP3I7dRy31UP9BCqOVFyCdTndrTntkOLY2m5OSwFupZ+77MfsV3u9k1sd90x7e4lhGZd3E6nmw871vdp73qt1nLfVQ10H6I5IB9MFU5slAvdaA7mfvq5NmlxrHqX2m1LWg/odrNVi5LzqX22+dS+HGTu+FILn3DaeJjvqICKP7bWPtLZPTtuFebZ8dteVfJq13LfPQTQl8L8MrWXAfUlMHflOqjH2qXFseRdsmUtWg/orj39Lmr3TR85uuhxDPnz6aE5nd3bwcLzyY8M9NDO9iwCcyiMi8anTnMv8xJy7V0BLfddgD7jiVPT7yUwN9UB9P1c/usC3YDaRZWnNuq2CPeBPMaPm0IeTx/rBLkdvBhlhq83bWcqLqcL+j7ldOgPkxlqq1On/VxV8loK0IXZJHbjnZvezu3C3ElxueX4MDd/J0Jfotxt0pYCPbUj3PSi31g2BLr5rT8wxe26truz3cfkdVPI5rnoYXm9RnOHyKQ22aUeF4ttAtvGInYG4+5sViaGQHfPh/eDoNRyRQ9wA/a75m52YWObvlGLFAUAuhRLdO1I3XhzN6PNdefal67ktkOLY83puZffAbo5lKUfaPgDi3Vt2MPZgPu95q6bv/BrNTMaU9G2P7sxjtrDb9btD6VLVkDLfVf1lLtzoJLHx2LOl/PudD9fSb1aHEvyxbukbSVAj0XHscjXRZhuir1N8+JZc747N++em1PJnjX+wSgWrB80j148a959ypxuZg9DeWPydQfCOPjGjpSdir5zT7Zbot11ab3p8nYNvF+IcL/0GwNtjD69oXAYpfsHxszlvK4f5N6DAlruu4cA+hzYfYcr3UA3d2iNqSOnbC2OtYeLOKeNS4GeOso1CvRgbdilMVPqFtgO6DZK7o8vfd4dkmKm5fsDU1x/Uoea5AI9PNkujNC3PQXt3D52Zja13XXRuN1pYPXoPw7LPZ5dvqGdw6cE3GAgxxtIo1UBLffdQwHdd8YcANdw3hyI+/Vocawa2kkoowTofrvTkfKzdm04PB/8kTlTvD033Ad6eJSpBbo5mtSsB9eO0KdeGOPgvi7U/XeRW2i36+jdpjj3jdko173ZNBqhW6Cb1OHjgD3ULc7tUGHugUEJ/kgb1lFAy333sECPucW1kF8K71gbtDjWOpfd9qUuBbpr4fSmOAvoEOgOlubP9mUh3Wf4RrDxZjk39e4DqeamuO03xPlAd0q55/Td5Pq53fk+BrqP5rAcp2hs9dxOwgP17a8xCTVque8CdAne5LVBi2MJk7W4OaVAn67QgsY+Hz0EyfDAmCm8jA+Y0QWk8fvL3TcmnvZ3uFutI1Pu7e54e2jPePNcek292FnIuFsFtNx3AbowF9TiWMJkLW4OQC+W7sqMAP1KAcm+QAEt912AvsDoWyTV4lhbaLVFHesAvZtG9jZ7jTd65WzyGkfpW2iyfh3hZrd+2t3G4mZNPHxkLR6h98fp+gOEcOrdlu8f6cPU+/pWllSDlvsuQJfkVeYRpOcvhbXo2M2pD/TheeNus9d4o1f8hLjxJi8f6lo2d4Wb4sJNbD68fTiPtR2euhemDf7dZrd16Vq+OPY1nNN7LfddgJ5j7Q3TaHGsDSVbtaq6QA+jRPeCljDiHEab9iUubqd2uB7sb/DSsrFr5nG0wZvS0kC3Ufdwh3t8m9x4k5xRkih91UtLVOFa7rsAXZRbEaELM0dTH+jD6LLfBOejJgb0fhOX1ciPIbu/dxHm3qPL4atNY1PlbiDkazKecrfvVTPHvIYR/9Qmub6+veso7VqS3B6ALsw6dW+8t+ucFse6nYJ1a67rV+FGL4uMIcB8WDukDCPNMY6GEeb+o8vY+vjQrsNT4pxm5s8+rg6XM8aeEUbmQ4TvX8e614Lm0rTcd4nQhXmpFscSJmtxc2oCvQd3aurdASaMNsNI0+TXPvXutgkaLcLZCWfO2Fp7Cuh+Hgf+2KyHTWcHDFr2JBS7/2EyarnvAvTDuCwdvb0CfuQ5hMn4XPI+2pyONCNT791+7f2uAftQDl8n61sx7OF4OWP8ZrXhxrm5qJ0o/fZXzRYtAOhbqLygjpqR1IJqSYoCCxVwUI9FnT5selAPge5PK8ejdB3RpYO162M4ADKzFuEqd2x/gtErfI96bKgzLsvpuNDAJN+hAgBdmNEAujCD0JyIAhYkFlEpoLtsPZzG7/CeijJ7MO07ugz3Gxhd+jVvp6HRxv7iT80PgT3WYS5Kd3XZcpyt9jvjwcU4pwBAn1No498B+saCU12hAv76dw/1+Pq6BY+NFMNIMzXdbBGkK7rs+uoObj+5Ne65Xe79S13GMM6L0u1OeVsPQC90+R1kA+jCjDcNvFYAACAASURBVATQhRmE5iQUCKNDt9PdRoLDs9yHGBlGmkeI0qecyJ+S96P34Z6C8eyGKzPcUBerS9vAiIsypQBAF+YbAF2YQWjOjAJe1DmKOMfPVI/X0WMx43iD3L6n3eecyI+e41P0bnbDHswTRtmpKD0Vj6d228+1k9+lKwDQhVkIoAszCM1ZqEC4CcxFnf20+7Id20eILvso+9Q8tJPjsQN37JlvU0APN8T5ptOyJ2GhOx4sOUAXZnCALswgNKdAARtx9q8GHW6Ms7gyJ5/5n6m1dJdOa2TpTZufHrqXo49nKfqjc0OTxKJ6X7O5zXUFJiaLSAUAujCzAHRhBqE5BQq4vdthtGkh5SLNMdBTEabLp/lccrvL3bzz/HR2j6f5Axh7Ep+Z3UhNpMe3uw0fG7TzJeHjbwUmJotIBQC6MLMAdGEGoTkFCnQR5yjadBvn3Etawgg99Ty2Tad7Hb1AZrKgQKAAQBfmEgBdmEFoTqECD4los4805x/BGkaXRJaFpiDbYRQA6MJMDdCFGYTmFClg18/916X6h6nEInQ3kRyfdnfPtxOlF5mDTAdRAKALMzRAF2YQmrNYAYPkh9NDc3IHqHgno7WHxbS/xdZxY8+kD6P08Q75xc0jAwqoVQCgCzMtQBdmEJqzWIEW6O2pcP4xpi4Ct+e/me1d48880Jl2X2wOMhxIAYAuzNgAXZhBaM4iBWw8bXe320n2fqq9Rfn53Jzar1KPoKUfX2PafZEpSHxABQC6MKMDdGEGoTnZCrSIPpvNcG6a3EXhqcg7VnTOUbBan0fPlpqEKBBVAKALcwyALswgNGeZAqcPm2b0HPXU0aRh8f2Jcv2b3GKPsy1rFqlR4AgKAHRhVgbowgxCc7IVsFPi7nhSP9L2D4bxTzVLFX22p6WdUiegEaFnG4WEh1IAoAszN0AXZhCas0CBh6ZdIG93tw+nzg2bL5vek+vnripzWpqf3mtCWyxAX2AUkh5IAYAuzNgAXZhBaM4CBczxpW5new/0/g1rOdG5qS41RZ+bf0GTSYoCihQA6MKMqQXoWhxLmHsUN2cLv0q+A/10bs5deN6vi091ZRro+z9cxu9f/Ljb89lMRLh0d+0wKaZKXMVUSv+JA5OTmY7iC0poRi333dPZPA+j4LPFjXcLmbQ41hZabVHH+n4VW+/uzm6/wCkXTNojdNu/8Rvp3OyEfbzP8daqBtC3uE72XoeW+y5AF+aJWhxLmKzFzdkC6MlXpnZwygeTPqD7EbZ7rex7jXn7ufuMo/b+G3tUbn6UbvcgtIOAQRBOhF58Ae0ko5b7LkAX5nBaHEuYrMXNWQPoIaTigOqi9C7GzANTbFOcX9s+popTD9s5oPcDoHRKG5nbY3oM0vsBwPSSxRzQ0+9WL3YxMgpQQMt9F6ALcCa/CVocS5isxc2pCfQYWh10LKxSKXLBlAb6nkB0ag/Z6SLly8l5FtD9lLv9vR+ijJ8QcFDPP8eeNfTiC2XnGbXcdwG6MEfU4ljCZC1uTlWgd6Dqj3fto8jhPvQxnPod79MR5vjX/UXo4ap3r41bE7dPBcTnG4b9tRsO3aLF3Hp6HtD3v7mw+HJQm1HLfRegC3NRLY4lTNbi5tQEuo8TH1IGOnFA9WD3wZTexapll/scWLvY2+x/uwsXvYdAN5PtPtKndwDP1cvjf8UXkvCMWu67AF2Yo2lxLGGyFjdnLaD3Deo3XLV74AygBq9PtVPxPphKgb7fx61iyxFmzqKf6+j/7sftVqnz6dy81x2rO72WDtCLL5SdZ9Ry3wXowhxRi2MJk7W4OXWB7jfDh5TbAGcBNYbTEExpKOnb5d4rFvYt2HneSugvVbic/rG69ru0SvNAd0sffenDnQ/FjkbGmyqg5b4L0G/qRuPKtTiWMFmLm7Me0EO0eICKwmkIphje3KaxYWf7wYLZHOZqzX+Uq1i6yhndAMjvgbdRzv85qNnMfNxdNtnZHy9nzwSPqMUn1YeDB3/1Ppw7qNxpittIAS33XYC+kcPkVqPFsXL7Kz3d+kAfRpVtdJ6AkwPTBTrh8rF3Enyva49uByL5EJpe6Q7fFW8RbwY88Y8/0DEapM+7919s45flAb0Vz7ybvvvuoXsfTvd6nXawsOgwG+lXwDHap+W+C9CF+asWxxIma3Fz6gJ9fkrX4CAFJ/+xrSiUBm9ac13u8OKDKICQPAB5CPYk698tE0y3d0G7WSvPOyJ3qTsM9znc3fVH8vb4Hj9At7QW0t9OAS33XYB+Ox+K1qzFsYTJWtyc2kBv+TSCVACodl9cLTh1U+5m2vkCouGsgDSg3zUPzYedJKfBkW3hhrfhjvZiI18y5gy4YrWECtphxdw8w/XtpYRaCmi57wL0Wh5RqRwtjlVJjpsXUxPoDlRxSIWb5Eq6PrcpLpxG7leMZQEo1Q+//alNciW6uTwlQO8HGfbxQ/tMAs+qX2OH7fNque8C9Azf+bvf9luDVH/6+9+ZkassiRbHKuu9vFw1gT6O2cKI0/U/tRo8p08u0IcQkgegOVindgGU6lYC9LCufuPD+fTQnLrH5OYsxu8yFNBy3wXoGf4E0DNEUppkPaBPTR/bOG/5Jwfo43rlAWhusnrr+YRwzT4Fc7/dJfZbbnFy1FEAoNfRsVopdW+8w2YB9Gpm2l1BS/zq7f2rtn+PXz5v/zT/Hv79WfP2/oORBjbN+fLb45fPuvwfNO7vtrz+364cP21YsPnNr89vS99OW6/7zZXhtz3sS7xvfZ9dGaZMp4n/nd/OUDP7W6+Fn3bcfquT08b8+eiF/e7dpz5oHr94Zk/I79ga6jfWZ8o+fbt8rZxO7Z9d3W9N3Ym+j33C6sbntgoA9NvqP6p9yY13adMB+lLF9KTP9avYjToGdB9AFsY22pyCjQGFgYT5OAjlQN/WdeoGFrYu2ybv713ZNYAeDgIsaPtBTegVabj1kW4/2LDfpXR69PJZC+63zz9oHr96Zv/sBkah5n4ZYZlj+1gN7ccf/AzbY8p80g3k3nR9Tulh7WgHO6Hueq6cffUEoAuzV+6NN6fZIcDn8tRcU9fiWHOa7eX3XL+KRaJ5QLdKGCC0UHLQclDy4ORAFEb5qSg+DXQ7I2BRdWocgGLAdd9NAT/spwOWA/pUGX6dMbj1AxDb2nBAYiJyE40/evm0/f1dN4vRQ7uf+h7OBqQHDWPIh0C3kfwFyt6Ay++r7xPhrE18ZmIvV4W+dmq577KGHvFNzUC/HIhhbunmpJJ2SvJ0+Xv4bz+9L5XL69K73/zv3Xep8ufKjrU11t41by9LgB67acem3x3owgjdAN1w1gLq2QVOQzD7kV04XT6M9MdA9yLcc9NG/QaE7+5fJyPFVBQ5N3U8mIruItfYgCGlj0s7rH8YIVuoPm3etu33lyni0e+wTW52xJ9FCCHvpvSHQLcDln563kTmdlCU34beB5hyX/P6zS0boOcqtVG63BtvTnMAeg/4OegCdOtRKcClIrE+fb9jO7YunlyvDeBhWzEG+nANvQeOhdJw7d5dG7E6Y9dNbM9AWMbUGvrcoCAFvVikPdTORvL+Z7hU4SL94Rp/eqYjNuVuH1Gzgwk3gBgDfdiG4UCDKfecu/E2aQD6Njpn11IT6GGlWtbQpyLlMOIOI20H9tj3OXljaVJlukFCqtywH9lOUpBwTb/qm9M/D26nwqcOTPHT+h2aeobaz2NfJmq/SZVVIFTVLLm7xftHxca61WxQuMu9Pwv+3O64k6pjTQ10lwXQhdm31o33L3/xy83X/su/M+idJqCHZjPQnJpyd+mvBXqq3gGSuiUAB/RYnhjs13TFWn5l2zh34/dhXuvRrGE59shY2RAaDuQ665pXy3ZcHZ7aPjUYquUZY6APz5NvvbJWZZRzAwUA+g1En6qyxo3XwNx8lgLd5QvbF5aTI9majhWbPgfo01ZZ4ldTU9CuFrOJ63ImeQepN4PHxvzH1/rNayb/3GNXJk34GJs/re6v2bvywt6Hu69vMS380fc+0vytj/zttml//pUv2/kKD+h9m+ee5c+54nLS9EBvx5zmHS6DbMA8R0XJada8727ZbzbFdWqnYG5+novQ9wT01NR3zrR5G2N6UXQbl0xsqHOOPLcpbslGulida14wuUCfWg92vzlQ+ZAyG+B6EKceMbM9TAE99XiWn8ffRW9LG+7+Tq2b3wLoy05BD6Hu9y11otxSj/GA3irnT/UD86VqSkwP0IVZJffGGwP3FMxzuukDvSQq9+tY07GkrqGHGs/NGqRmGnJstTRNrl/lbf4arnP7j6GFO6THzzynN3q5PqUOmek3hNlHu8L4MoT2VF+W6leWfu6kuFSpsRPdakA9LBegl9lVbq4177tb9vpwEXoI72thbowVRujXQH1Nx8p9PC0VMccidBcxO6ddEm1P7aDPbetUvTUupCVAn35EbTh97qJnv43xndjzh8/kRujmEa9coM89TlZD23QZAH1dfSk9VGDN++6Wah8a6DVg7gPdgPzaMrU41pZOvGZdawLdtTt2cpn5rZ8GtxGh/6ibf7BJelDQT9OPD6Ppn3+ORej7BHo7vPTkCDcZtkPS7vfYVPnUkwI2b7+GzlT7mtfd1mVrue8eFujOYa6Jpl0ZNaN+LY619QW5Vn25QK9XfwgVf7rX7U5P7VKPAUn2jvbrdUv1+TJ3E3kMsOTRv7nBwPU9oYTbKaDlvnt4oPsuVAr3muvyWhzrdpdm3Zq3B7oPDteXOah3mxMfzFbwsP/age6DO9Z3p2dMwz59u8QT1c+P+uceO6zre5S2nQJa7rsA3fOZUqCn3K5k+l2LY213Ka5bU32gl64PO7CEz13bR7ou6J8Aujm3/Zra11XaL712K8Nn+of/ntZvDPT96LidxfZek5b77uGAvrXjLYW6FsfaWue16qsP9LVaqqnc2kBPzVrY0U+7kfNkjqs5mT/sd4OBUWz3vCa96YuW+y5A38CXl0Bdi2NtIOsmVQD0TWTOqKQc8uNDacYnv52bc3PnUTwFdJPOHJ3LR5cCWu67AH0jv8yFuhbH2kjW1au5LdC7XdXtAT5TXZ3bsW1h6D893S+2D9fYz81DczI0i9a3ZaQ6tdltCu7hLveu7x2h7aOS8WfT7bG4MZ39g2UA+uoX3Q0q0HLfBeg3cJ6pKrU4ljBZi5uzDdBN3DeElJn+7T/hGrCbFjbwddFibJ24uNtkRIFDKaDlvnsooPtHuP7p73+nSIfV4lgixS1o1PpANyB3EaPZtGZfyzkHdDONfHfXb4izU8EG6jwfXWBmshxcAS33XYC+gSMvGUhocawNZN2kimuAHjsHffx+9CHQTaf6NPb92u5QGddhc+iL28j17v51+7V/FrtLN/Uudj+Pf9Tr1MtZbn8k7CYmp5IDKqDlvgvQN3BegL6ByCtVAdBfXQYLAH0lJ6PYmysA0G9ugmEDcm68S8Bas3tL6tXiWDX1u2VZOX4Va18sOo69kc2tnbsp9jbNi2fN+e7cvHv+un0dauxoWPMaVvOmNhNRm0n2Nybfy/hxrqk3wU197yJ4l+Y2b127peWp+0gKaLnvqo7Qw9eezjnoknV1s2s99yAagD6nvNzfS4CegmAU6Cezsc30325uc2nMlLoFtgO6g7V9X3o/NW5f3jJ1Hruv7hK4T73fPTbFL9eKtAwFphUA6MI8JHbjXRvoRoIY1K+pV4tjCXOP4uaUAt2vMPaiEwvWZ+3udrMO7qd59NKunb9rwR2P0B3QH7182tw1d9Uj9LmXsxCxF7sUGQUqoOW+S4TuOdeSCN1kSz1bDtAFXrGFTSoBuqtqelOchXYIdBelmz/916L6b0vrv+/fke6m3v2H32puimP9vNCByLYLBQC6MDPl3HiXTH3ndi8GdYCeq578dDl+VdYL+6iamVo3h534T5G3h7tcHkGbP0TFTx9/Yr2sheRCgaMoANCFWTrnxrsE6A7US7qZWlNfUq8Wx1qim+S0OX5V1n6AXqYbuVCgvgJa7ruqp9xDsy8BK0Cvf9HsscT1gN5NuJv3gpzMlrhTd6SM/d5+xseUps4lH0b1e1SaNqPA7RQA6LfTPlpzzo13CdBzu5dzRvuSerU4Vq5+0tPl+JXEPpg9825Y0B8rG54i180SdCnb3fb2P0GXureSDU6x40Q6iXanTWUKaLnvHipCLzN1OlcOzE1ugF5b+e3K2xvQ/denDI+RNaD23xIWf0FJXFnOid/O46jpFgoA9FuoPlHn1jfeXJgvlUmLYy3tt9T0W/uVm2g3f9pDYd0GOR/IOTAevkVNqr60CwUkKKDlvkuEXuhNSw6WWVKFFsda0mfJabcG+vBFne5Vnf40eA9z+7pP80IXky6cKj8G0IcvS7VvrevfVx7q5oZL53ZlwWhnB04x/fq0vX+yzCD5Wr2mbVruuwD9Gi9YIa8Wx1pBmpsUuSXQzYS4fdeavynOdTvcKNcDp4eY/4ibfXOb5jew9XqlNIt9P9Yxrp/Vt9fQnzu5iStS6YoKaLnvAvQVnYSiUSBHgXDdewzh4RT7EEAm1Lzr9rENgR4fGOS0SHaa9D6B+Ka/XgcXjbstg51eUf2I0GV7Qd3WAfS6el5d2paR1NWNpQAU6BTo0RJOF/uR+XBTmgG+RZED0ikKdG0RutvSZ/cWpPQKo/LhwTwj7drkMf2I0I90kQJ0YdYG6MIMQnNmFXCYjoM3NsVuuB3CPPKU2eWxs9jU/WyzxCYA6GJNs/uGAXRhJgTowgxCc5IKLJ1id5O/dk03iEDbk2bCqvRtiEsfjev3PRzA9NG5ez3tcGajyxvd6+ZbydXBpjitlzVAF2ZZgC7MIDQnqkB8F3sIJQ9f3fL5eKq4P+zF7urWuX5+zXr5eCAUGQz1iQJ76RsUcUmmFQDowrwDoAszCM0ZKeDD3P44fOiq/24YWQ6jch9TZle82ckeA7rZL2/K32dUOf04Wioqd3sK+sg7umbeadIfvBPq5zTWtWTBJQnQd+MDAH03pjp4Q1OPpIWnsUXWy0M4X6bbYye57RdGIczHg58g0m4TzEyvX7zOlm7+awZD9u8poO97UHTwC21R94nQF8m1fmKAvr7G1HCdAunNb8MpdnPeiTnqxH7iYLY5TCp7sEz/mc53XQ+2zB3rk6t/+Bif36o+Ik9rZ2X19x6MB1P9i3Ec/rfsO3VtrQBA31rxmfoAujCD0JwMBYZgcrFjaop9WGB8F7yLPfc83Z4+zCXS5+6r+CAoDmN/MGSxnxo87HeWI8P5SOIpANCFuQNAF2YQmjOhQOYjaYn1bwckW0Fkun0wFa/FEG7w00fe/eS5m8cYPnOe3j+Q3g3f52G6XYvn5PQDoOeotGEagL6h2FR1hQLBdPGFU0umyqeA5Ddtnxvi8mYi/D0GPeTnNgGOB0N+Xi+iVzkousJtlWcF6MIMvAXQ/+P9q0Gv/83L58JUoDmyFRiv/U7two71ZTY6T04fy1Zm3Lr09LqduCh9Tjy1KTGlj4ZB0d5sv317Afr2mk/WuDbQfZgDcmHG31Vzph5Jy4HHEaLz8fS6Rbh79j6Mqn3Ax50h/shgbIo+d9p+V05HY2cUAOjCXGQtoANyYYbedXOug/lxovMxsLN3ryf9Y+pJAKLzXV9WFRoP0CuIWLOI2kBner2mdSirVWC0Lrt0F/URovMpmE89ypb2seFAyEXzROdclb0CAF2YN9QEuoH5P/7Bb257+L9/5Q8bptiFGXunzen3oy8H0+VRK/O2VPOMVvKo1/npZ/HyjZ4RDyGfszTh95LoXLzNb9xAgH5jA4TVXwt0PyL3YW7qAejCjL3r5iyNym1nT+dz+5bP+GYwX5ClsJMn5nC4snzwE6rRHsDTFTMeDE1F7PK0oUXrKADQ19G1uNRrgO5g7kDuGkF0XmwOMkYVKIR5O1vvTjZTHp1fdLsO5K6Y8UAojPZDQ+1/QMTFt1wBgL5cs1VzlAIdmK9qFgofKODv3l4izVHWzp0mlWA+GgiFMC999G2J7Ui7BwUAujArlQA9BXPTNaJzYQY+aHPmdrb3EaibOtYgVOnAJ+w7z5xr8IYt+gDQt1B5QR2lQA+n2W893a7FsRaYTnTSEr+q26F0dD6cfGeq+DLN3v2lP6M9/vIVMxhqY/aLdGhY13f3U5qW++7p3C7O7f8zdeMNL+epyByg798XavbglkCfi87HO91r9nzPZc3vah8jHpjv2eLXth2gX6tg5fyxG6+7aGNAT0XmNYDuBgwlu+O1OFZl896suFsCffzq1NTYGxj50fn47Wk8c36zC2gnFWu576qN0M0tzoH8oXtvtPEt891/8J4zj/mbWT83Hx/IS06MW5I2rF+LY+3kOp5t5q2AfonO2+Xk8c52nWvns+bISDAfnY8LYUCUIazqJFruu2qBftc0zYfNublrTt37ju1Fmwv0MLp2h8042Juyfrx7OUssbiqFuhbH0nL13wroROfLPWgwCGov9vTaeb9u7u4Ky+sjhx4FtNx3VQE9dvmGLle6fu6fHmfKnNsF7w8AYlPv5vfY91ocS8ulfgug1147z7kudNiLXe067Lh9L7Tcd9UA/emr+9YL5nb4hWBOTbmnInQ/fWxq3vzuDxr8NDnnw2txrO0vyXVqrA10f1+H32IfRe0asAkhLzPBnlefzs3p4dT9HJ8qHpY1rUtqn8k6aq5X6ngQFN4Nrn/m/DgDo/XsJLVkLfddXUC/e2jOD2ayPf0pidDn8vjT8K5mf9Od//vcGfFaHEvqhbu0XblAf3v/qi36cbcMY/4d/t1896T73aR9c/+qefryefO6y+u37fHLZ83b+w8a86f52L8/b1w95rsnL541bz5l05xO5+bN89eD9LY9Nr8ZHZgrw6/LlWf+NLAy7XFttnXaPg3bZdsQ61uYzv+3X5afN6WZ356wHUZDN2gZlPvieTsI6ts3HAi9ff66s9Gzy6tYTZ9bLTu7mH+78p29/CAhtHOok8ub0jOn776+S/2V9GUKaLnvKgL603bF3AtrkpZdEqXPwdxV4qAd2z0f+y01Za/FscouK3m5coA+BTgfmi0gXhnAmhj83Lx9/qZ5/PKpB5cPmsevnnYPRp86MPVAf/Tyaevf7+4/aJ606c7Nm/s3HZCeNm/uX7f5Dbgs5O1D1uaPh+ahQ3o/kDDfvb3vIWcGDQZINn3/CQGT6q8/GAhhHv42pZlpgw9aB9lYjO0GRQ/nc/O2Hdy4AYcbyNjBjK3vWXumuxkEucGQ+dNMhrjBU/inSe/W292gyg2M3GDMh7ipzR2LExsg+f7gBgOp7+RdDXpbpOW+qwboTz4wN7i6QHfuO/eI2xTUU6BPTddrcSwtl34u0FNR7DCi6wHaRqYvLKDdxwDpkYFO+4WF0LBcM2htmncG3IPI3UbzbZleZN9/97w5nR6ahw7u5nszOLg7NW1UHwLUAd1NMceAHuuv+y4G9tgMxpRmrk1+20x6A1E/Ynbgd3334d2XPw90M8NxPpt0vY6tFQKg+212bQmjfAd082dqZiI2wxGb3dFyHUnvh5b7rh6gv3qeFZ0bx8qN0HOcMPZCl/BtbUtOo9PiWDna7SFNLtDzbtAWRf5Uuq9B/71FuosqwzzjqXh/Wj42Vd9Fq6emOT80zTsTob40gwMLMH9w8OjV0+bkbQF3Ea3rXziV7GCfO00cSz/+7tzOLrx5bmcizKDj6cunl1kDO+dgPmaGws5KmDDaX5bolxrsfIgZBJnBkvmX+fuTl0/a2Q3zpxkEtOW4wVBklsPUZiNuM6Ny1/7dzWaY39xsx2MTWLTTG6aVtp2XWYSu1a6/4bICQL/dHUHLffewQDeuMwXaf5LpW/+rSze3Zh4rLjbtrsWxMuUTn6wu0G13U/DrAW5jYx/w7jcXQZpy4uvsPdxdmnDQ4P4dy2+n9Zs2ejcRq/mfP3U8BXS/ntRa/NweA/v7gwV0O4X+9LIscGn3C9vH88nCue+PyWt1G7aln9kINQkj8ti+BbfVtl2SePW0nekwyx5OP19Ph3Jz9oV5ZDYEuj/rYdti2tz7Rfid+AtESQO13HcVAd3ucs/9pKJ0A1kH82/6pm+aLO6P/uiPLr/7YJ9aT/cLBOi51rpduhyg12ud/1KSuec16tU6X5K/v9vGneu2rqT02Ar7fM+KUnQzHS287+y71s1sR/g5mbi93chgpu773/1pfLfksr6mRT09TCaALszUS2+8MaAvgbnf/RjYze9za+8AXZgTRZqT41epTWPh+mksMh/vyO5v7WGk6UdvsTXp0p3n8SjRRMHe+r63nh/bZZ6aLg43v4URaM7aujPLeD06Fo33+wlsXf7mOFtSbJbDN72/BBGWMR/RD+t3Mx6mHH/WY/jimP5US/lXhM4WAnRhds258fpNjh0UE0bmn/zkJ5vPf/7z2T1NReypAgB6trQ3S5jjVzm7wC1I7E5r/zNeTw9jNX+3u7+2bqdqw/Xn2O7xmHjpnerDx+RsXjf9bzakPXSb8qYfX/MfscvZ2e1DO2e3vL8E0acf71EI9Y3tX5j7Ltz57gYF/pR7uBkx3LQ4tkE4o+DvDLiZux+2YoAuzPRPFmyKM00PD3/JnWbP6XYu2AF6jpq3TbME6FMbvvKAPoZ5uJbuyhlGlP3z6XNr16Ga4413/kChf4Y9lm68C7+HfGygEWtzDPwuXexRt/634V4B830YWfvfxWYbwmfCbfrhQGk4+Io/f58zeBkf5duXbAZJdreChXzJgsNtr5L91w7QhdnQAt18xmtZqaY6qNeEuV9XCPbYxrnwRDotjiXMPYqbkwt0B4P5qfDY89F2M9d44jXcHGe7EdvlPhVFpjofj177jWUWbmHU62YM0hF6OKjIidCHA4DxNHnYb9cnt7Pdx+B4t3v8njC1Qz89gxGfGekHbL4u/sbG8N40fImMhfr0oVjFTkzGWQW03HcVbYpbDnRjZQN1A/S5DXCzHjGRwIHdbJzjrWTgWQAAIABJREFUpLhrlNw+by7QUxBzLfajuOnvhsAfRujzQPfBN97t3Zc9/m24Qzy2NOC32wdYHGaurTaqTS0FzK+hD9s8bFfsWf3hwTI98Pv2xPrhe1YYuU/tRE+BP6yjLz88bz72drjt/fzoNQJ0YR5QEqHHgL503TxXBgN1gJ6rlpx0OUC/vrWxSdbYru3aO7nH67jjCV87S2BbaPZym3/nz4Jdr81cCTnvOp8ro9bvknSp1adjlAPQhdm5FtBrdssfHAD0mspuV9b6QE+9SmXuQaa531MapQYK/iNz2+m7Tk23WIUG5uvYcptSAfo2OmfXIhHofuP9aXfzfezVqeb7ax3r1D336td9PpuTt06N+dN83N/DtC6dyxumd9+HZbWxW1D2lOFcvX6ea+sM82c7zkzC9YFe0tIlwEpF9bEBAVAqsQZ59q/AtfddKQqwhr7BGroztovSAXq3m9cbBAB0KbcE2oECx1MAoAuzufQI3ci1BdBjUbQflYcRuouSU9G3H7XH/h6L+sMyfVdJzRSkZg+W1umXX8NFZUboNXpGGSiAAk4BgC7MF64FuunOmjvdHdDNn2Zz3FpT7gC9X1qo4aIAvYaKlIECshUA6MLsY4BuVwWXrwO6R9e2gvqtgO6bbGq9PDX97a+9h/lNnnCtPuYiS9ftY3VO7QWYmh0ocdn1gW7fjT6YxRj4cHzz2/n00Jza1wW3cy7BcSTLr4ESbciDAloUAOjCLNm+ytBs/Cps11ZQd9Pu0iP02NR4GP3PTZ+ngJ6apr+2ztrT7ab96wLdeKvbXW58t33v5uXEsDiszcCpae7u7J/tQErk42SFFyLZUOAGCgD0G4g+VeXTV/fFMHflbgF1gB7fbZ/aib9kECEJ6KkXsZj+DM8e74FufusPWhkeu+p0MHnbAZH36tDY2efhgS0u/9RBLjkHwPjtEHYLoDkoUKwAQC+Wbp2MtSKpGNRrHTbjP7omJUL3rTH1eJs/lR1Og7dRZfdYXOqxuRDMYZ65zXW5j8rVhnqpXwH0da5zSkWBNRQA6GuoekWZpTfeWJVrROo5MDdtqeVYIdiWPIeeC9ecMtspYTc37IFfM9Bj0XHsiFC3du5eytGmefGsOd+dm3fPX7cvG4mdt/7oxbPm3ac+aCN9s1r+pn2Lmzv6uH8Dm4v4Yy8cyTmKdeqs8ysuVbKigDgFat13b90xRc+h31fV0oe6X3DJTvhcmNcEelUxDlzY0oFiCoJRoLcb24y4dnObS2MGQBbYDugO1v1rTPu3n/UvS3Fmis0OLIX73NT8gV2CritUAKALM+rSG29O893b2Fxa91a2OcCHR76a9FM72/3ytDhWjr57SLPUr2KvFI3D9Fm7me3dvYnE+zd4Perel/7u3gd6+IIS+wKSRy+fNnfNXfUIfeoVomFf9mBD2ogCcwpoue8Soc9Z2vt9KeCXROauGi2OtUBW0UmXAn0qSg43vIVAd7A0f/pvFfPfjNZ/379pzE29+0941NwUlxqkrGe40mdVTIt4ZG89u+gtWct9F6Bf4aM5gM+NzAH6FYZYMWsp0KebZHe2u02I/lPk9r3Y7o1mU2Drjs/10pe+rmVF+QqLnnr7XFjkNfB3ZTEIKDSUmmwAXZgp17nxLutkCHiTO7WbPVWyFsdappzc1Ov4FUAvs3gNePs1+yDPLXsIf//lsrZkBgdltr1tLi33XSL02/rRqHYtjiVM1uLmrAN005xzezCMeTneXXPqjpSx3/dgcM+od28j79L3nRlH6cUdFZcxF7BzDU8Bdkn5YRnuvfD9m+IB+ZwdZP+u5b4L0IX5mRbHEiZrcXPWA3pxkw6ScQlwpyQB6AdxmKu6qeW+C9CvcoP6mbU4Vn1lblMiQL+F7iUwj4F7rpzUu+L9qXNzHG+XrpshGb4vwp9RuYVW1FlDAS33XYBewxsqlqHFsSpKctOiAPot5O9fWOMO3RlOadsjeVrMRlcoOgC3D/nPbxV05x6Z5Y9BeW3Xg3X2rsjhoUCsm9/CS2rWqeW+C9BrekWFsrQ4VgUpRBQB0G9hhhjQQ9gG0fUFtH17o5hNBuXz4LclA+9beMTadWq57wL0tT1lYflaHGtht8UmB+i3Nc0Fs+0Owr4t/Rvm3Hc+qfu/u3TuGxPx23fc9dPxw7IA9m0tfpvatdx3Afpt/CdZqxbHEiZrcXMAerF0AjJe3i/bvjzIztDb//XP+gtoJk24uQJa7rsA/eauNGyAFscSJmtxcwB6sXRiMg4n09nEJsYwghqi5b4L0AU5lWmKFscSJmtxcwB6sXRkRIHdKKDlvgvQhbmcFscSJmtxcwB6sXRkRIHdKKDlvgvQhbmcFscSJmtxcwB6sXRkRIHdKKDlvgvQhbmcFscSJmtxcwB6sXRkRIHdKKDlvgvQhbmcFscSJmtxcwB6sXRkRIHdKKDlvgvQhbmcFscSJmtxcwB6sXRkRIHdKKDlvgvQhbmcFscSJmtxcwB6sXRkRIHdKKDlvgvQhbmcFscSJmtxcwB6sXRkRIHdKKDlvgvQd+NyNBQFUAAFUGANBQD6GqpeUSaR1BXikRUFUAAFDqwAQBdmfIAuzCA0BwVQAAV2ogBAF2YogC7MIDQHBVAABXaiAEAXZiiALswgNAcFUAAFdqIAQBdmKIAuzCA0BwVQAAV2ogBAF2YogC7MIDQHBVAABXaiAEAXZiiALswgNAcFUAAFdqIAQBdmKIAuzCA0BwVQAAV2ogBAF2YogC7MIDRnpMCpaZozuqAACohTAKALMwlAF2YQmgPQ8QEU2IkCAF2YoQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAgBdmKEAujCD0BwUKFSAvQaFwpGtWAGAXizdOhkB+jq6UioKbK0AQN9aceoD6MJ8AKALMwjNQQEUQIGdKADQhRkKoAszCM1BARRAgZ0oANCFGQqgCzMIzUEBFECBnSgA0IUZCqALMwjNSSrAGjHOgQKyFADosuzRAHRhBqE5KIACKLATBQC6MENpAboWxxLmHsXNqe9X5vDXU0OUXmwSMqJAdQW03HdP5/NZxfHS9W+81X0mq0AtjpXV2R0kqu9XDy3Q7f9v83E126EF58vfxgrUKkkBLfddgC7Jq5qm0eJYwmQtbk5doLvo/NycNwa6P3zwQS4d6NLbV+xYZBSlgJb7LkAX5VYAXZg5Ku/NcJNh5s+77K6alCZHyVSaD/IWjueH5uFkv3W/mQUAovVsc5BQoQIAXZhR60ZSt+ucFse6nYJ1a67vVw6d8+1skX8yEF4O8zAiNyXYeYFTczLltQlc/NsDvmTQMN+TkhSuJew3KFGPPMsU0HLfJUJfZvfVU2txrNWF2qiCukDvIZVq/iVqfq9L++Gp+bCx6+7mfznRup/m3ObttuGNxhLehPa5aU5d5C4D6rbdt95vMOVmqWWMjVyTaioqoOW+C9ArOkWNorQ4Vg0tJJRRE+hm/2nHzMFOd7dF7sLb9otz8+GDDaVdnrtuajwFXH+z2wDko/V6vwSXqx803B7o/cjDLAZsvd9gCcTDeY7bayfhqtlfG7TcdwG6MN/T4ljCZC1uTk2gu4lzM+VtKH1ZSe8C5QfzzencTok/2Dlxi+KW6P2U+Z3JG0zD9zDv06V30ofb4kwlLl/+2n6xqLMZfSwu22/gF50zmzHblMjzCOFehIudLsM0a2k29OWoKyONlvsuQJfhT5dWaHEsYbIWN6cq0A2FT2Y7nPnPuTmboLhbz3br2+7fw4fafFzbrtiVZfd3N1QwpQx/G3fcn/YPwW4aJAHodoDR9y7ffG7fQVtCwd4Dv6bxPgR/4GP3IrTfRPYjOMgTsefb7pYptdx3AfotvShStxbHEiZrcXNqAv0y5R6wqoW5ocKQ4gHQevia9HceeNviTh/a6fnmLtgNP4cUP44sj4aLBY5mjC0JxGsY7OL39h2Y1OHeAzdMmFPEDWlMOvt/t56f2osQwbe4PQl1LaStNC33XYAuzDO1OJYwWYubUw50Gy0bOJipdBtRx4gdi0R75Nhcpw4pD+2U/N35vQ400xO77YTApUpvhb2b8i8WpWLG4b4CC0Z/iDHaX+DY6e0xaL/quuf2GTh4p5qamg4fDm+8DYWTtnO1+PaVtCehosGUFqXlvgvQCxz0V37nVTLXD37784IS+yxaHOsqEQRlvgbow24sgbmHFRe5nz5szue75r0W7nMxZiigv13Orpa7XfO3l9obvIR7C7zA1+0vaL8K9xhciD7cZ9BCPTLtPlQjVMCW0Q8totMmXabY8sXtFaUFyxXQct8F6Ats70D+tf8+nekv/639rRTsWhxrgayik64H9Ni0ciziPrdrtHfnuy7Sn4o53W+p9Wf7/WW9fuPT6qIt7/YVmN/CvQVtW81z+G6Gw1uzHmN2vM+gh7KtORzWxGZMem1EuyWNq6yAlvsuQM90DAPzKZCHxRiwl0Bdi2Nlyio+2XVAd5H21H5nHzPDdBfWdbvc42LFt24NYTWMJCUBfTDlHhmHTO8v8DFthypub7mLskd7Ddosdr9BbANg3FLhjMhU1C7epWlgRAEt912AnuHeS2HuiiyBuhbHypB1F0mWAP3tvV2KefzSLbucm7f3Hwy+c2lcOvPvxy+ftSDq/26nik1I+W6QfxzV++UNyzRtsPXb9ti89t+2PvtvU79tb9j+sK1+Gme8vq/2m7EGc+U+a9v06OWzdr/BG699sfptLdO6mhRPXj5ry/I/j189af9p9iD029x24YY0cmUFtNx3AfqMo5TCvBTqWhxr5etvs+Jzge6D0YeOhacPUvf3EKId2F486549j+WxZflPOPeAdt87SI8HCRaEr9sSDIh9+IZgd7/7sA+/C42QW4YDvxto9IMeq0G6Hn9Q0us3TN/Py7/9VDdY8fYfmLrtHgSi7M0uoh1UpOW+C9AnnO1amJdAXYtj7eAazmriUqD3kLRwGkSIA7j7EXSf1kSqbs/bu0+5aHoId79Mvw43eHDRvgW3GwTYXG4AEIIzBWNXVxjFu0HBsC19tO++j5UbA7ppZ2o2w59h6PvgA32ote1zP3gxeYyuQ4QD9KwL4CCJtNx3ATpAP8glW9bNpUDvYeWizTBC96fW+7+fv/Q3zbt/8a55ZCL0jjV+9D2MxPto3H9tyxDWQb3tS17Ozbv719Ep96noOgXlmhG6H62nBiH9IKFfRhgOTGyf3d6Dt+2AqF9uGLYXoJddETpzAXRhds298eY2Ozs6f2vCoPlSc9fTr3Us94IN1yJzE3efy8s3vO9aNPQPK7c3fT+9/2+X1n0X1mVvpt0jP+3xpOmyYm2ZK89XOWzXvAXKUizxq+H6sVu/9sHaT3Ob1rio1PxppGoj8hfPvdlgf624n27319pjEbq/bm7+/uSVKd8cNfvQAb1/tHJuDT025e4rWWsNPQR6fObh4pnR9X43EADoZb5+5FzX3nelaEeEnrBEFtANzAd3t7RZAfoY8P4AIBxYhAOSqYHKmhfTEqCHAw5vnBQ8NBUenXK+RJXu3PZhn2JPTuccjWLS2ANOJO1sH9trahd57PG+1nOip6W7zYTj6XV2qq95ney9bIAuzIKlN95UN2aB7iJzQRG6gV4scg2jYf/fYR7/37Hy5n73o/tYxJ6aJQij/9AuU+1c0xXL/Sp8Bis8hKRvdRxCrSJdonRem2AC+KP9X9Kmmv0GhtD1LRs+npd6xM9/zj7lGdI0WNODKTtHAYCeo9KGaZbeeN987HOj1j350g9cvpsFuknpR+gz0+5bROgAvb7DLfWrdPSZeMK5Y1g6mnfAjsFuOnK3Ubl755gP/vo6lZc4BfHL8DA+sDk3zd1H7E8ffti/z2WoJVAvt81xcgJ0YbbOvfFeQP7dXxn34Asfbb8zYM8CuoO6gflMpL4F0NtbdmQ9fC4qDoVIrX3HomQ/byxf2J5wdsDltweMDCOn2Pq/n34LF8z1q9YVBs+h9+vfrp3+Zq/Yhrdw3Xi8Ec5/5K0HYWw3/XgNutd2+tn1/tn0+DPz4/X3WDrfNtM75OeAHh68Y0ruTrz70t80dx97r3/V7KWLUwf5hIOEmBfFpudnBlTdpsO+dNMGZgK2uEZr1AHQa6hYsYycG28L8xjIw3Z84aPzUF9h/dw041rHAugVncoM7l7dZxWY3iU+fGzMFRbbkW4HBalH1fyDYIbPo4/Afz419hns4Ya8vu7442Xm99Tz5+GAJXYITbiBzhdu/Jz+ZWhmD9HxXrDS/cvLPrFe7r8u9sLP1CAhNm0/Gs4mljD8dMMBgxmHuk2HPdClvIY2y30Pn+ja+64UAQ+zKS4b5s4yX/ho85f/7Sv2uNd/NWGu75vf5Z4bne8V6Kl1+6mIPdwp7yL01O51NzswNduwxkVVDvTYs9F9C/3DVIbgC4E+fOwqFbW7Mu4/+KfNhw8fNq8vJ9AND7LxwewA7r4LD5vxvw/T5IDfLz8cEFxw3s7MhJabXi+/HO9qXy/vvVIud1mizZRwl7nBgL+fwZ03J33T4RpXhq4yAbowe87deEuA3rzopuX/80RnHewTaZbAvAbQB3FEt0mudFOcvVf2G+2WbExLpV3Slpy+rO2Gc37l6h9H6MPn0MN2xiN091z68NAUm9dGheOT4Ybffc3dR5qvPnzoHSM7LDOEdLr9w5PYYvnmD42JW2cYrcd2sYffjSGb3kgYwnpqQ2EM6nPt6W1xeendoBim2de+JtcoH6CvoeoVZU7deCdhnlr7NqCeAnnY1kj6pTAH6PFd+v7AQmqE7oBn/pw6JKUHqDv0JLVmHT9pbm693NYfzg4MX+l7izX09Lns/oU0t5vfpo1P0fsgnx8Q9IOkqZtOGu79hsO2RV0hwPyKW/hNswL0m8o/rrwI6Kld6kth7prj5SuB+bVAn3tWO3WYS85hMvZGOoyU5g6CqX2wzFw713DJ3Ah9XPeSzV71Wm5MdGdmVeoVuXJJufANmzG18W3qGX1Xzuh5vu6HDOXarGEdwHxlR1m1eIC+qrzLC18M9NRz5KUw96D+l++XvToVoI9virknzy33mLwctYCejirz2pGb6nR3as67e5VYBkRzBYimiz3e50f0YaaJ9pjXrrbFxaL3qxpJ5hsqANBvKH6s6sVAN4WEEfq1MPeg/uSTw2nOXLm0OFZuf6WnKwd6JGYPZzgMGU65MItFhHmPV713ums+DOqWrXtev5b3IQX2+ZLM3gTzMRsO7XgJoM+rtp8UWu67h9jlnrWGbuD+awvXzVP++q+aBqDv52KeamlNoNszzCwLUoeftOeQt5DPORBmyXT1nqaE/enw3AGPb8W5zW4uOk/tih9/74BuNhyyZq7j2vZ7AdCF2XTuxjuCevgceS7MM496NY+6lUBdi2MJc4/i5sz51ZKCzctRLEocrCMRY7i0m1rqna3YRvSyz3BPdWIJxOcGKkvKirWn07Erph+IzdU7ayASCFJAy333EBG68ZtklO4AnTPdvuAwGYAu6Gq9oik1gT58ocgY5sOd01c0evdZcyFcPoW+VCK3YrGvDYdLe3nc9ABdmO1zbryTU+9zQF/4MhaALsxBCpuT41eFRZMtqcAY6P4WgLyz2uvKazYbms/+NhzW1UFraQBdmGVzb7zJs9zngG76u+BlLABdmIMUNifXrwqLJ1tEgejJg4s2EPqFxh5hm3qsLT47YDYWms++NhfiXrkKAPRcpTZKt/TGO3rbmjkVLucgmdxInTX0jSy/bjVL/Sq/NXZ9e4gef102Dp3z6aE5nd054VqfhfaUmdhA2EbMl02E5l85Gwmd4qU76Vk7z/fx/aQE6MJsde2N983nX00Dfcn6udEGoAvzkLLmXOtX8VoNTNxuN3MIjNssNw309tCYu/6ktH1ueJu3g9PDpBxvIGwx3hcS2zRYvJEwbFu4sdC2iI8+BQC6MJtee+OdBbrrL7vchVl+3eYs9avYW8XCN5NZIPVANz3o0/RvUguPS22nok/n5t3967bT7u1mvgLjuuyvsbejuXy5L1pJ1VnfAuFja/HNbzaOP/WP8t+dm/PDKflI4Hw7p06fc7kB+ryO+0sB0IXZbOmNN9b8bKjP9b0wOjfFanGsOYn28vtSvwLotS3r4B6bIu+OI+4O53GvMR0iNzYYWLKu7mYEXDkAvbaFJZSn5b57mMfWcpwGoOeodKw0S4Aei45jbyRzK8Snbvq2TfPiWXO+Ozfvnr++vFzFvc/cRtgfNI9ePGvefcq+1MVg5U37itT+RMKct59NpXERuEuTfof5Fj7gAG5VCvcbuLfP5bdkaiOcKyXncTmAnq/5flICdGG2WnLjnWr61VC/IjonQhfmVE3T5PpVCoJRoLcb20xf7eY2l8ZMqVtgu1evOli7V6Q+X/xaVL/8nL/PTc1vb6HUaW4ucr6mRT6c52AOyK9RWnpegC7MQrk33pxmF0P9SpgD9BzrbJsm169irySNA/RZG22adXAXXZu8j17atfN39z7Q3Xq6jdDdWvejl0+bu+aueoTutye1Ph/7fn2LnNstB+a9KOYTn1JfCvi8Q2m0bjxc32b7qgGgC7NX7o03t9mLoV4B5gA91zrbpVvqV9Nr6BbQIdAd+M2f4TS762n//WmwwS2cSK65KS41SNlOfVdTah3d/Z4TPZsNhV36dmSQishzpua3V4Aa11UAoK+r7+LSl954cypooW4+U8+nmwNpzNRs4dvVwnZocawcffeQpq5fWTCZqXXzLnkfHeZRLfvN3E5rSyU//VyOPeg838bhc/tGPxuy+2COgX1qKj1XuZwBw3wPSCFXAS33XTbFZfjYBeyRtLVA7orW4lgZsu4iCUCXYiaALsUSGtuh5b4L0IV5pxbHEiZrcXPqAt3G1vZ0M7Ml7tQdKWO/tx8XNfbRo0vfd2IcpRd3cHcZrU7mOJ7z2fw3/SraoZ5hR+em3UOb7E4oGrxAAS33XYC+wOhbJNXiWFtotUUd9YG+RauPU0f8kba5/vswH2+Oc48T9o/KMeU+p+jef9dy3wXowjxRi2MJk7W4OQC9WLoNM4azG37V0/B2O+XsKXztuXMZ6/Ibdo2qNlFAy30XoG/iLvmVaHGs/B7LTgnQZdunP0LXtXP+pSttitPZwjuyZ2548A/RuXQPqNE+LfddgF7DGyqWocWxKkpy06IA+k3lz6g8PPfdZUkfGmNAbjk+pDnPnGfIrTSJlvsuQFfqoHQLBY6hQBdpj46HDdbG242IbjOhD3Mi8GP4yXQvAbowLyCSEmYQmoMCmyjgRejdOrit1ge6gf6dF4/7a+6bNJJKhCsA0IUZCKALMwjNQYHNFAgWwtt9beHudTa7bWaOHVYE0IUZDaALMwjNQYGbKGC3tNnX3qQOeGWa/SamEVwpQBdmHIAuzCA0BwVuqMAwPmeK/Yam2EXVAF2YmQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAgBdmKEAujCD0BwUQAEU2IkCAF2YoQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAgBdmKEAujCD0BwUQAEU2IkCAF2YoQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAgBdmKEAujCD0BwUQAEU2IkCAF2YoQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAgBdmKEAujCD0BwUQAEU2IkCAF2YoQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAgBdmKEAujCD0BwUQAEU2IkCAF2YoQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAgBdmKEAujCD0BwUQAEU2IkCAF2YoQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAgBdmKEAujCD0BwUQAEU2IkCAF2YoQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAgBdmKEAujCD0BwUQAEU2IkCAF2YoQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAgBdmKEAujCD0BwUQAEU2IkCAF2YoQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAgBdmKEAujCD0BwUQAEU2IkCAF2YoQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAgBdmKEAujCD0BwUQAEU2IkCAF2YoQC6MIPQHBRAARTYiQIAXZihALowg9AcFEABFNiJAocA+le/+tWdmKNpPvXun+2mrTQUBVAABVBAjgIAXY4t2pYAdGEGoTkogAIosBMFALowQwF0YQahOSiAAiiwEwUAujBDAXRhBqE5KIACKLATBQC6MEMBdGEGoTkogAIosBMFALowQwF0YQahOSiAAiiwEwUAujBDAXRhBqE5KIACKLATBQ4BdKm2iD1OB9ClWot2oQAKoIBsBQD6De0D0G8oPlWjAAqggDIFAPoNDQrQbyg+VaMACqCAMgUA+g0NGgP6+//6/Ru2iKpRAAVQAAX2qsBf/fxf7bXpg3afzufzWUVP6AQKoAAKoAAKHFgBgH5g49N1FEABFEABPQoAdD22pCcogAIogAIHVgCgH9j4dB0FUAAFUECPAgBdjy3pCQqgAAqgwIEVAOgHNj5dRwEUQAEU0KMAQNdjS3qCAiiAAihwYAUA+oGNT9dRAAVQAAX0KADQ9diSnqAACqAAChxYAYB+YOPTdRRAARRAAT0KAHQ9tqQnKIACKIACB1YAoB/Y+HQdBVAABVBAjwIAXY8t6QkKoAAKoMCBFQDoBzY+XUcBFEABFNCjAEDXY0t6ggIogAIocGAFAPqBjU/XUQAFUAAF9CgA0PXYkp6gAAqgAAocWAGAfmDj03UUQAEUQAE9CgB0PbakJyiAAiiAAgdWAKAf2Ph0HQVQAAVQQI8CAF2PLekJCqAACqDAgRUA6Ac2Pl1HARRAARTQowBA12NLeoICKIACKHBgBQD6gY1P11GaYZYXAAABdUlEQVQABVAABfQoAND12JKeoAAKoAAKHFgBgH5g49N1FEABFEABPQoAdD22pCcogAIogAIHVgCgH9j4dB0FUAAFUECPAgBdjy3pCQqgAAqgwIEVAOgHNj5dRwEUQAEU0KMAQNdjS3qCAiiAAihwYAUA+oGNT9dRAAVQAAX0KADQ9diSnqAACqAAChxYAYB+YOPTdRRAARRAAT0KAHQ9tqQnKIACKIACB1YAoB/Y+HQdBVAABVBAjwIAXY8t6QkKoAAKoMCBFQDoBzY+XUcBFEABFNCjAEDXY0t6ggIogAIocGAFAPqBjU/XUQAFUAAF9CgA0PXYkp6gAAqgAAocWAGAfmDj03UUQAEUQAE9CgB0PbakJyiAAiiAAgdWAKAf2Ph0HQVQAAVQQI8CAF2PLekJCqAACqDAgRUA6Ac2Pl1HARRAARTQowBA12NLeoICKIACKHBgBQD6gY1P11EABVAABfQoAND12JKeoAAKoAAKHFiB/w+QG2P0fGgfeAAAAABJRU5ErkJggg==';
    shopTanksMenu.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAgAElEQVR4Xu2dz881yVXf+953RiaxvLCRQhZZRNmEDdgSkVhEBkuwAHkmnteKokgIssg/kA0RAgR4b8lBrJCIlKzIyjP2zOANAYMSkwVIsGOVBWIRJRJ2HEXCnjy3o6rq6qquruqu7lvdfbr6cy2Y97m3uvrU99S9nzqnfvStbdu24YUCKIACKIACKHBqBW4A/dT+w3gUQAEUQAEU0AoAdDoCCqAACqAAClSgAECvwIk0AQVQAAVQAAUAOn0ABVAABVAABSpQAKBX4ESagAIogAIogAIAnT6AAiiAAiiAAhUoANArcCJNQAEUQAEUQAGATh9AARRAARRAgQoUAOgVOJEmoAAKoAAKoABApw+gAAqgAAqgQAUKAPQKnEgTUAAFUAAFUACg0wdQAAVQAAVQoAIFAHoFTqQJKIACKIACKADQ6QMogAIogAIoUIECAL0CJ9IEFEABFEABFADo9AEUQAEUQAEUqEABgF6BE2kCCqAACqAACgB0+gAKoAAKoAAKVKAAQK/AiTQBBVAABVAABQA6fQAFUAAFUAAFKlAAoFfgRJqAAiiAAiiAAgCdPoACKIACKIACFSgA0CtwIk1AARRAARRAAYBOH0ABFEABFECBChQA6BU4kSagAAqgAAqgAECnD6AACqAACqBABQoA9AqcSBNQAAVQAAVQAKDTB1AABVAABVCgAgUAegVOpAkogAIogAIoANDpAyiAAiiAAihQgQIAvQIn0gQUQAEUQAEUAOj0ARRAARRAARSoQAGAXoETaQIKoAAKoAAKAHT6AAqgAAqgAApUoABAr8CJNAEFUAAFUAAFADp9AAVQAAVQAAUqUACgV+BEmoACKIACKIACAJ0+gAIogAIogAIVKADQK3AiTUABFEABFEABgE4fQAEUQAEUQIEKFADoFTiRJqAACqAACqAAQKcPoAAKoAAKoEAFCgD0CpxIE1AABVAABVAAoNMHUAAFUAAFUKACBQB6BU6kCSiAAiiAAigA0OkDKIACKIACKFCBAgC9AifSBBRAARRAARQA6PQBFEABFEABFKhAAYBegRNpAgqgAAqgAApUA/S333uNNytW4P133j1d66b65H/47O80P/iDP7iqTV/60peaP/30n6+6lotQAAXGCpzx9yXmR4BO7xavQPs7HzUffPCBeDtDAwH66VyGwRdVAKALczwRujCHFDTnH/7nf9D81m/9VsEa96kKoO+jM3dBgWcVAOjPKlj4eoBeWFBB1QH0oTNIuQvqnJhShQIAXZgbAbowhxQ0B6AD9ILdiapQYKQAQBfWKQC6MIcUNAegA/SC3YmqUACgS+8DAF26h9bbB9AB+vrew5UoMK8AEfq8RruWAOi7yr3rzQA6QN+1w3GzyykA0IW5HKALc0hBcwA6QC/YnagKBUi5S+8DAF26h9bb9yzQ1arwI15Th79wsMwRHuGeKBBXgAhdWM/IBXotjhMm/8icXH/ktONZoJe0JcfenDIAPUclyqDAPgrUwoXLnRRXi+P26ebr71ISogCdlPv6nsiVKDCvQC1cAOjzvtYlvvOd7zSf/OQnM0ubYr/+67/e/MZv/MbkNZ/5zGeav/iLv2h+8id/svnmN7+5qH57be5Fa+6RW3dYDqBPK0eEvrZncR0KlFcAoJfX9KkacwGy1nEAfZl7cv2RUysROhF6Tj+hDAqsVWAtF9beb6vriNAzlfWB/ulPf7pR0fH3v//95nd/93d1DbH3ciL0X/u1X2v+6q/+qvnhH/7h5pd+6ZcyrTHFbIT+Qz/0Q83P/MzP6Pe+/vWvN9/+9reb2HtE6Ivk3bQwEfqm8lI5CixSAKAvkmv7wrkR4VrH+UC3oM59b6vWx9L1ue9tZZOtN9cfOXYQoROh5/QTyqDAWgXWcmHt/ba6jgg9U9lceIflFGBfv44/q71t2z7KVtHze++9l5yn/8M//MPmc5/73MDaXHiH5W63W7TV6h7Kht/8zd8cfR7apwY1fttC+wD6dMciQs/84lEMBXZQAKDvIPKSW+QCZK3jagG6eq74Jz7xiai0v//7v9+8//77AH1Jx1tZ9hmgr7xl1mW536OsygQUQmcBTjiBCWu5IK1pROiZHlkL9Lfffrv5yle+0t/lz/7sz5q//Mu/1H9PReg/8iM/0nzqU59q/uiP/kiXLRWh+1mA2D38CP3nfu7nmt/7vd/Tc/JE6JkdJbMYoMkU6sli6PykgBe5HKALc3RuZLHWcWuB/su//MvNd7/73V6tX/3VX21++7d/exbocyltVcGalLsP9F/8xV9sFNR/4Rd+oR80+ECfGnDM2Zfrj5xuxBx6jkplypT0WxmLnqsFoD+n31WuXssFafoQoWd6ZC3Q186hzwGzBNDDpodz6AA9s3OsKAZoVoi24hJ0XiHaBS8B6MKcnhtZrHVcCaB/+ctfbr71rW81X/3qV0VE6D//8z/fR+fKoB/7sR9r1LnndlEcQN+ukwOa7bT1a0bnfXQ++13WckFau4nQMz1SAug//dM/3fz1X/911hz6HhG6Srf/6I/+aK/Ar/zKr+jpgBygz83x5w6wcuQn5Z6jUpkyJf1mLXr/U9+YNe7tv/nZ2TJrCgD0Napd7xqALsznuT9Eax1XAuihZM9EwEen3P22fPzjH2++8Y1vNJ/97Gf7t3P9kdONAHqOSmXKlPDbCOA/8b154/74Y4MypQAP0Oelp0TTrOWCNO2I0DM9AtCX7ZMvAQbrmmeBvtXjU994441G/V/q9V/+8X9LflYjaHqQ5wB87nvXAf5ZsNeo85x0fL5cAYC+XLNNr8gFSC2O21TMApXn+iPnVs8CPeceW5SZ0qAm0BQFeeiIJ8Fek85b9FHqNArUwgUidHr0JgoA9Ka5AtA1zEtE5HO98I8/1qyJ1gH6nLB8DtAF9oFcgNQyEhPogoFJuf7IaQcReo5KZcos8dtuMLdNWwF1gF6mX9ReSy1cIEKvvace1L4lYJgzEaDPKVTu8xy/bZpin2vKwhQ8QJ8TlM+J0AX2gZwfopocJ9AFROiBU2pMuReJyv/lP2qa//Tfn+vCmdE6QH9O5qtcTYQuzNMAXZZDcv2RYzUReo5KZcrM+Q2g76NzmbtQS64CAD1XqZ3Kzf0QWTNqcdxOsq6+Ta4/cm4A0HNUKlNmym9FYK7MLBGhq3oyonQi9DL9ovZaauECc+i199SD2gfQt1vlrs7c/4M/+IOoZ3/8x3+8eeutt1Z7PeW3p2CuAL70lZuSn4H6M0A/QuelMlG+jAIAvYyOxWrJBUgtjism3EYV5foj5/ZE6EOV1EE5f/rpP49K96xWMb+thrkFeQjnUhG6VWAC6s8AfW+dc74LlNlGgVq4QIS+Tf+4fK0AfbsIfW/QrAL6FLQB+uQZBZf/8ThAAIB+gOhTt8wFSC2OEyb/yJxcf+S049moM+ceW5TZapX7nkAvDnMldGmgqzoTUToR+hY9u746a+ECEXp9fVNEiwB6HRH6YqDnwDqnzNJeDNCXKkZ5TwGALqw75AKkFscJk58IPeKQs0foi2GeG31vAfRElE6ELv2XQoZ9tXCBCF1Gf6rOitwBVk7DSbkPVdor5b4K6DlQB+jMoed88XcsA9B3FDvnVrkAqcVxOZocWSbXHzk2AvSTAX0O6gAdoOd88XcsUwsXLheh79hHuNXJFXh2IHHJlLvv8722rdl7RubRSbmf/Eu4k/kAfSehc29TMiLMvSfl6lbgykBfnW6PdYmcg2VyD5KZ63IB1AH6nGB8rhQA6ML6AUAX5pAKzAHo39vGi7GUeyqaX2oBQF+qGOUBurw+ANDl+eTsFgH0HYFuO8uz8+sA/exfu0PsJ0I/RPb0TQG6MIdUYA5APwDoqt88A3WAXsE3b/8mAPT9NZ+8I0AX5pAKzAHoBwH9GagD9Aq+efs3AaDvrzlAF6Z57eYAdIC+50Nwav8+SW4fQBfmHSJ0YQ6pwByAfhDQSblX8O05VxMAujB/AXRhDqnAHIB+ANCfgbnqc6TcK/jm7d8EgL6/5qTchWleuzkAfUegs22t9q+T6PYBdGHuIUIX5pAKzLky0JX7Vh0uk3OITKxvlDhYhpPiKvjWHdMEgH6M7sm7AnRhDqnAHID+jab5iQ2i9GfT6qm+BdAr+NYd0wSAfozuAF2Y7jWbA9ABOqvca/6Gu7YBdGF+JkIX5pAKzAHoAB2gV/BFzmgCQM8Qac8iAH1Ptfe917e//L+a169f73vTpmk+85nPND/1Uz+1+r5nf9ra6nn0OcW2SLlH0u3KjGcezvInf/Inzbe+9a1oa7bsG3Py8Xl5BQB6eU2fqhGgPyWf6IufjZSPahxATyh/EqBv2W/4vdpS3eV1A/Tlmm16BV+QTeU9tHKAPpT/S1/6UrNnKnjVavepHlMa6Ino/NkIfctOz+/VluourxugL9ds0yv4gmwq76GVA3SAPtkBAfqh388abg7QhXkRoAtzSEFzAPqxQFd3Lx6ll+ofEzAnQi8lcv31AHRhPgbowhxS0ByAfjzQRUJ9BuYAveCXsPKqALowBwN0YQ4paA5AlwF0UVDPgDlAL/glrLwqgC7MwQBdmEMKmgPQAfqoOwH0gt8wqgLowvoAQBfmkILmAHQ5QO+jdPWPLY6Fnes3f/wxXeLtv/nZuZL682f2oWfdYGUhfq9WCrfRZQB9I2HXVssXZK1y8q8D6LKAbq3ZfaFcZlTuqwXQ5X+/JVgI0CV4wbMBoAtzSEFzALpMoPfR+h6R+gqYE6EX/BJWXhVAF+ZggC7MIQXNAehygd5DXf1jC7AvTLGH3Y4IveAXseKqALow5wJ0YQ4paA5Alw30QQq+FNifBLm1CaAX/CJWXBVAF+ZcgC7MIQXNAejnAPoI7PaNnMi9A7i9JHfR21w3A+hzCvG5zjK9824VQtzatm1raAlAr8GL8TYA9HMBPfSiXjw38yoFcFLuc0rzeUwBgC6sXwB0YQ4paA5APzfQC3aFxVURoS+W7JIXAHRhbgfowhyCOSggQIFngD71VDsBTcOEggoA9IJilqgKoJdQkTpQoC4FAHpd/tyqNQB9K2VX1gvQVwrHZShQsQIAvWLnFmwaQC8oZomqAHoJFakDBepSAKDX5c+tWgPQt1J2Zb0AfaVwXIYCFSsA0Ct2bsGmAfSCYpaoCqCXUJE6UKAuBQB6Xf7cqjUAfStlV9YL0FcKx2UoULECAL1i5xZsGkAvKGaJqgB6CRWpAwXqUgCg1+XPrVoD0LdSdmW9AH2lcFyGAhUrANArdm7BpgH0gmKWqAqgl1CROlCgLgUAel3+3Ko1AH0rZVfWC9BXCsdlKFCxAgC9YucWbBpALyhmiaoAegkVqQMF6lIAoNflz61aA9C3UnZlvQB9pXBchgIVKwDQK3ZuwaYB9IJilqgKoJdQkTpQoC4FAHpd/tyqNQB9K2VX1gvQVwrHZShQsQIAvWLnFmwaQC8oZomqAHoJFakDBepSAKDX5c+tWgPQt1J2Zb0AfaVwXIYCFSsA0Ct2bsGmAfSCYpaoCqCXUJE6UKAuBQB6Xf7cqjUAfStlV9YL0FcKx2UoULECAL1i5xZsGkAvKGaJqgB6CRWpAwXqUgCg1+XPrVoD0LdSdmW9tQD9Xzf/aqUCXLaFAv+++Y9bVEudOykA0HcS+uS3AejCHAjQhTmkEnMA+rkdCdDP7b+9rAfoeymdeR+AnikUxRYpANAXySWuMEAX5xKRBgF0YW4B6MIcUok5AL0SR9IMFJhQAKAL6x4AXZhDKjEHoFfiSJqBAgD9PH0AoJ/HV2eyFKCfyVvYigLrFCBCX6fbZlcB9M2kvXTFAP3S7qfxF1EAoAtzNEAX5pBKzAHolTiSZqAAKffz9AGAfh5fnclSgH4mb2ErCqxTgAh9nW6bXQXQN5P20hWfDei3zltt0zT239aB6r2lL1WHX9eaOuw9/brCeuxnc/b57Zsry+cokKsAQM9VaqdyAH0noS92m7MAPQfkc9CMDQCeBWho1zMgV3U9uv4315aLdVOa+6QCAP1JAUtfDtBLK0p9SoEzAF3B7XZrmrY1/6dj6pt6r23a1mH65sXsFqzJgUBr0Pm4+Vepd9zfU9F6ODho2kdXVyx34GJ43RYP3OqTu5d1eDRt14qbfl/V9kzWgF6OAkoBgC6sHwB0YQ6pxBzpQLeRatuhzeK2x2bbNPfbrYtsHUwtDK2b2sagsgexx13zT4tZi9e0g30oq3r7YUCE5bZu0w5V1tTvp+fNndumvbXNvTWft2rkokYxXtmYRbFIPnxvaiqgkm5MM2YUAOjCughAF+aQSsyRDHQL34cPzR7Jkbi1i+Jf6XDepq/VUGCA8oTnOux1UL43t2h03MOyNQAO4/tx5Q6nN3VNN9JQ193uyshb89K3zw5DzDU+4Lu8RA94/28X/w+j+RDkpPEr+dKuaAZAXyHalpcA9C3VvW7dkoFuolMVu/qgsyhL48kCfAzyUaLcS2g7/JlIWqW8DdRtlKz/e2ualy5db5Licy8btpuaFNTVmOTR3JpX96Z5ebTNTU0b6GjcL2vjdhXTK0vMAMVvgcs6uNTATU1LBM200xWmHaZNwH3Ob3V9DtCF+ROgC3NIJeZIBroBsvq/u46yh4nq9MyyA3kM4NZxqeVrBnUGe1362wNpnwr3UuLzXcEfFph2tLeHjs4VbC1kTT1BWQ17NaRRQxt/yZx+ewTvsS1WDdUil1EA6PNeq6kEQBfmTYAuzCGVmCMZ6M9JbOFna7Fwt/+dW2oWDgYWLZFLmD6sQy/w02l7P9KPwF8PHrrFchrwfpYiVyUP4Xp63rRvSavmFMu1hHL7KwDQ99d88o4AXZhDKjEHoKcc+QzQw0T9XFZAfR4baLj34lmHqRX14T2npxR8FfyhD6vs6/iiA3RhfgTowhxSiTm1At2k6HM3ocVwFoPwVArf1aGD3y6V/vLy0NHwx9/8WPN/P/pedM7eXGnsHWbyQ8hPQT/VIf24OhwgjNcAaNPVYj01Z6+SHDqtbwYOZibft7aSL8EFmgHQhTkZoDuHvH79euSdd999t4m97xdUZdQrVW7J57bsXDfJtSm0y6/f1pF6b+7zKRsBeqjOVGSeB3Q1z64gbv5rIP3mq1fNRy8v3U40fwHcEOZ6ENKlw0fz6aOlbD7gY7bFYG7up/6nFtv5JSzeH2p+vzVz7mZQZFBOyn3u2y73c4AuzDcAHaAD9Pwv5ZERul441zTNm49Xzf97ZcLc7i0DSM3ecbRt4mAf9nMIzcFsbICi0gcvza19pe2xiXtV8qG21mnz/Ll9C3a1rt5F6nPW5XuLklsrANC3Vnhh/QDdRdY50XEsarWSq8+m6nj285RrUzbNRdhhlO9nI8J/20g/RyNV9qgI/YPX7/UyvfXuO1HJ/DKqQKrc+GKT5lav99/52uBjV8dwnnt8ry+Mqv3gdaouW9TVaaNzf2X+8D2LUnttGG1bzKawORwQDAcMc1kGA22bQreH45gF9f4Muo/70I68bMXCnzmKb6QAQN9I2LXVAnSAbvsOQJ/7Fh0P9HG63Af3HBx9mLso3Myvp9P16ijcN++vmo8etv5wxbyxIXbQjq3VfOYfRpvKAgD0uV4o6XOALskbTdMAdIBeA9BtJOxH2+F7sTKq7an3YxH6B6+/rt9+691hpG2jbHv/sE6bLv/wiyYat9er68IMgbs2jOZzUuGq9ikoDqFu7Rqn65ek6M0t7cE5aq2b2cvu2+H/OzUHL+zHEXNmFQDosxLtWwCgO739FHQqtTyXcg+9F5uf9suEn+emtP065lLusR6VSqnH4O6/l9s79065XwPoMfXDOfPxHHr8gbBhhO4PBELgLpnVDgcdibl27+GyMYtz+xnljlUAoB+r/+juAB2gx1bhx1b3Lxls7A30sGMrwKcj3+Hc+nMRusFRGFW7iN1G2WE5Y4OxMxXtD68dpttti1PwncJkLNKPzbXn/FhNZQ1iUfmwvPmLNHuO0hLLAHRhXgHoAB2gm4V084vjHs045b430GPwi+8Jn/6pSUXSsWg8Pr8engDv7pfKEri6/UfUDncOAHdhiJg0B6AL8xZAjztkLo0di1afXcU+d32q6yyx1S+buwreB35ulE6EPpwrt/CLzevnR+ixqDznB2Vu7n0uOp+K6NX944vtYo9q0avg7TPoB4f0APIcT0orA9CFeQSgA/TcfehLBhxHAX0qfb50Udy4fNssSaUPo34TnYbXm0Vx0wvsXA8dbokrdyRLziy2D/UwlR6L9iORfmseGqMW4plT4uz+eGAuDAvZ5gD0bKn2KQjQATpAj6fc5QE9HqGPH8biYDp+eNtUqn1uPjw2oAhBn9o6px5XGz7lHZDv8yu/3V0A+nbarqoZoMePbC21yl05ZcnRrzEnzqW590q5K9umVvn7tu8doYcHuPi2pBbH2TJzB9CEEbQfaefWMS7nwDd/sMxEqr171KlDpQ9Uc8xLLJJ3oJ+LzmPA9ufUpw+o0WfA6bA8vA8wX/WDLewigC7MIQAdoOdG6ADdfXlzIZw+le4ZoBug6pnr7iEn5tCWqe1mHYT7h52HUJ6bZzcJ8vTBNlYbex9b1B46a855N38Bc2EYWG0OQF8t3TYXAvRtdL16rXtH6NvqvWQf9hpLUoBL31dH2IqZkbPR5ywYg9Xcxx7zOjgPRn+SA3v/rkOImxoA+Zxfzvg5QBfmNYAuzCGVmAPQlzgyF+guMvej8fno3Niizlh/uT2aW2ujeZcGN5DXIwRvkDA3kDHXm0er+MDmJLgl3j9zWYAuzHsAXZhDKjEHoC9xZC7QTRpbP0K1B6l5DKndy23uOk6NK5g/mkf3kQ90t+VsWIf/zPd4lN72g4PY56TVl/SAs5YF6MI8B9CFOaQSc84M9PEysW6LVbcAbZ2LUovP5laWe3ezz0Pv4mKbOo8fzGK3hRnY9gMAfbsQ6CbONrRXZdWjWaeBrtLz97tL0zM/vq5XnP0qgC7MgwBdmEMqMQegh44sA3Tz0PPx9rHBk866gYd5tpmBuVlE5y9K8w+DGQLdWO6XH0f9+qEut7Zbisf8eCVf28XNAOiLJdv2AoC+rb5Xrf1YoHcw6rZKmf/4B5lYQK3zjj/zbNHnItow1eyBsd9iZlPn/jnm6Xnn4Ux3eqV4X86umOvXwqeibX9OPiijDn+5t83tcTdL6Qevmz4cxj6hbemSuXWqc5VEBQC6MK8AdGEOqcScY4FuRMwF4R6Sh4MAfyCQHgyUtCy2pa0Duloprz+2zyvvYvQe2taOYXZgvBiupL3UdQYFALowLwF0YQ6pxBwJQK9EykLNiGUAXDL+rrMHDujDefnYane3wv2uF+XxuqICAF2Y1wG6MIdUYg5Al+ZIf959OH+u4P2quXdL4azdamFcbKX6eAW9zYeQepfm8+3tAejba7zoDgB9kVwUzlQAoGcKtVsxM/futqZZ/Nr5cHsYTJduH+wtzzjetVshD9R3c6iIGwF0EW5wRgB0YQ6pxByALs2R4ap2b1Zf7203B8+YDWvqFY/o4w97iR0uI6392LOFAgB9C1WfqBOgPyEelyYVAOjSOoeJ0NXhMu5kORdPq8jdzYWbRXJmd8Aw5jZAV7D3z4538+mqbqJ0ab7fzh6Avp22q2oG6Ktk46IZBQC6tC5iI3R/bny4D8DBOAZ0i2lbj9++cI/9MH0vTQnsKacAQC+nZZGaAHoRGakkUACgS+wS3RPatGluDt2m103c7c+zj8vpSL477324aM6P0jloRqL3t7AJoG+h6hN1AvQnxONSUu6n6gPhPLoFtvqvPQBHpdLDKH543rtL2YfRuh+pE6WfqmusNBagrxRuq8sA+lbKXrteInSJ/jcA7p+sNojS/YVtPqiHkfzgiNm+iZGtbLoKoC6xF5S0CaCXVLNAXQC9gIhUMVIAoEvtFOYxLS5dPgfsMDU/XBA3nlEfzqebrXK8alUAoAvzLEAX5pBKzAHoUh3pR+lhZO2n3i2Yx0B3j2tNLZQj9S7V+6XtAuilFX2yvlqA/qQMXI4CF1IgFqW75rsz2tV7w4g73N42FC1+ihxRer1dC6AL8y1AF+YQzEGBzRUIt7DFjnj1F9Cl9qubiN684gvkzOCAvembu/SgGwD0g4RP3RagC3MI5qDALgrYB7OEj1a1Nw8f5mJT9Q/vABof5KkHuJj3idJ3ceruNwHou0s+fUOALswhmIMCuyngp95dlO5Wso+PfzURt3r5j1oNYT5MvdsofbdmcaPdFADou0mddyOAnqcTpVCgPgVi57vbw2fU6nQFb/+QGAfuYcQd299u1XJwJ0qvrwcBdGE+BejCHII5KLC7Ah2o1eHtN38/ukmWu/lxm6b3z32Ppej969y/Afrujt38hgB9c4mX3QCgL9OL0ihQvwKxhW4O7Cpydw9y8YGeeiyLjfb9NH39Kl6hhQBdmJcBujCHYA4KiFBg4vnp3Ty62Y/uv6bm0v0UvIgGYkQBBQB6ARFLVgHQS6pJXShQiwJmXlw9cFWhPUy9m/PfYkBPR+kqdU/avZb+YdoB0IX5E6ALcwjmoIAIBbqFbrdH92D02BGxYQo9tjhuCHiALsK5xYwA6MWkLFMRQC+jI7WcS4FUHHmuVmxt7aNpb+rZ6Bbc7tS44YlxeWl3sx2OefStvbZn/QB9T7Uz7gXQM0SiSDUKxA8nraZ5RRtiNrBZCPsnwfmPWg1vmd7CZve3E6UXddOhlQH0Q+Uf3xygC3MI5mymgH/Aqd1trW5GtD6WXGnyuD2am9rKpl/DB660+rNYtD2ddk9H9pu5nYo3VACgbyjumqoB+hrVuOa8Cqg91Gapl132BdQTQB8dLKPK2aNcDZrHr3mgk3Y/77cntBygC/MlQBfmEMzZToG2dYFmF5dbqBOlO9mNFmZ1u4nLg+i8bZubfiv2UBcH/bBG84mplbT7dt18z5oB+p5qZ9wLoGeIRJFKFAjPLh+efBFfkzwAACAASURBVBZDUSUNz26GRnSrFsPZIY6NwlORd6zqnKNgU4OBbFMpKEABgC7ACb4JAF2YQzBnOwU0Z/wHjtiYkahxIPrtpWkiK9vHjpmK0P2z4G007x88A9C36+j71QzQ99M6604APUsmCp1egRAmwyiSxVrGwSYlbg6NGZ7hbiJ2kyoPB0WJKF2fDe+XBein/xoFDQDowjwK0IU5BHM2UiAGluH55LHFWv7s8UaGCav20egJcr26fTjoUWzuF70n589tc9T+db+810xdLRG6MMevMgegr5Jtu4sA+nbaUrMkBcJzxv00sIWXjUzHy71iV0tqXTlbHt1it/HJcOaBLDnRuYn1x6/wFIByVlPTMQoA9GN0T961FqD/6TvvClP22ub8k/deyxJgMH9uTRvH3/267o49/va2K0TryWeg39qm7cJzq9G0g6eBzip3WV+PtdYA9LXKbXQdQN9I2ItXKwvosfTvOKVsYnaDbT2XrDLPKm3cvV8/0NPTEnrHn54PV7v44/H3sMsToV/hJwCgC/MyQBfmkErMkQb0tKzDeXSdVm5bc4a5/sguDjM1OOBX4qhBMyYemdrt4Tc4B+g1en9NmwD6GtU2vAagbyjuhauWBvTkAq0+3uxWcSuYq/f6NVvhwrm6DkXxo23V8lf6FD37Gkbsdte+Wjw4H6VPZUXM0IjX+RUA6MJ8CNCFOaQSc6QBfTpCdzGnxrnebmWvGC8OO+PRpX4rYrg2CvgRemwBm30oi0G6A39M3TTQeepaJV9ynocuz5EAXZ5ParBIFNDNBHAkUTyeR7dQcz4Iy7iV8Gfwkz/vP4iJ9Wlwdme5aYkDuvnblI9rZFa8T72YQz9D/3jWRiL0ZxUsfD1ALywo1WkFZAHd0imFIC+t3p9TnorQ3Ty6ZFerFvlDGHMye7ev3G4v7xrgEuvmdLd4C8Oph7m0O6vcJfePUrYB9FJKFqoHoBcSkmoGCsgB+tRJZeGeav+hqucEuh+Fq9a5bWg5HdTF8zqpcfdPhnFAV8n2R2PS7ukonQg9R/GzlwHowjwI0IU5pBJz5AE9FPamV7Pf+lR8apnXeA59Pt28vxNDkJsEuntemrEotRDNPxHOtNefTzcr+/3kvToBrm1etfeJeXSAvn8v2P+OAH1/zSfvCNCFOaQSc8QAffDI1KG4/dT64O3UXLsqZEEXew54eceNZ6/j97Ab6+wqdAfyJSvJfQB78NZGhEvq/PPex1vY7JBgbK0bLKhBkVF0ODVQXkVq3FIBgL6luivqBugrROOSWQUkAN3iw6zIDoGlN6dFUsbTkbren94/iWxWhuwC/l1DDE/N/FsoDiPyJSC3JoZRerdILphv70u3TXPXi+q62L/7hzvr3cHaieBa6e/nTymeLR4FD1MAoB8mffzGAF2YQyoxRwTQ27Z5KAD3Tw6z4nZHwKnjTPujYtRnw7RyLMI0Q4NyEXp4R/8ZZy9mR7x+2aNSx7Gyn1qfAvn0mvQhdF3s3O3KH0lhz3TXu/JjD2EZPGnN010NAfQgoZvueJgEQNi+Sr4G1TcDoAtzMUAX5pBKzDka6Bp8HTjGSV37rG5znGk7GXEH8eO9bW4Pt2XrmejSDguUmXqYoE+o63jXp7rTMDdL0/z5bb/zhAAfD1Z0Ca+YeQBaUE7DOpyLX9tJu5yJiu7v9mz44RDlGT3XWsV16xUA6Ou12+RKgL6JrJev9Gigj+e7h4DTrL8ZINqHjgyj1HB1vH0OuIrP3fzvlKOn4KRqMAezjCPnm8os3FUQ68PahLJqEZ96104hjB9yMl65b210SXV1IlzTvOjHlftRvQ/zMBewtEvPLYrz6xtOOAD1pVofVx6gH6d99M4AXZhDKjHneKCnhMxJPQepax3qe6/+Wd4r6lrtX3PyWqMiW0P7Pk1tqkyD3C4WUHkFNYSJv/z3YwvklhqeC3R3X7vFjiexLdX6uPIA/TjtAbow7Ws2B6Bb765ZoBb2DG/BWr9ITdN9GuRdNfHV/GGEPI7jXYk1bcgBejiIuDWtyppssOiw5u/akW0D6EeqH7k3Ebowh1RizpFA/+D1e81b774TVVJ95r/eevcLkXLjeWQdA3dvf/j6a7N1fDAqM29PymYbfZtpgnAF2nhu/LkutGXCe5had3b6K+zVu2sGEM+1mqvXKQDQ1+m22VUAfTNpL10xQA+h/wzQzYqAN+9vNB9/4+82//v7381YkT/V/VLQTsPczjr0M+urmBsDeph9WFXxpb9rRzYeoB+pPhF6VP3/0c+Jmo//fvfrlXpflVGf2XJ+pf77ufX618/dO3XfsGHhve3nvs1+mfD90A71d+zeKXuOALoffYfRrv0sjMhtJD18fxj1Wph9+EUD6XQd7zTuPkOAh/eP3TduS6zLzkXlc1CMrYCPLBq0B7/YXX5PB+8h0IeROXPnwuCQYQ5AzxBpzyJXj9CnIGWB58Mt9u8Y0JeANxwYzNkUG0ik+kzYBjsY8Qcu4QDFvyb8dwz0sXsD9K2AnoL5HMSnflUSK/q9bX/D2mP3mliU1986HBF0f+uH4fmPhXmmLXv+enIvgC6sDwD0caRtgQrQTVRu4R/LPqQGF3sCPYyKY3PoT0Xo3XfWzp3HInSbEZi/jwF9rNx8hB4LkUvCL5zL7lfgzcxrhzBXLcxZFGeqVVvnTOlYPcJ+MDFnoABAF9YhADpAP3uEfh2g+z8eKZDPbaWbGwCMo/U0aFMATtkQptzNM9uAuTAoLDAHoC8Qa4+iAD0+F+6npq0fYqnx0Ec5c9SxFH34Xsz3qXnsqX4ylXJP2R5mKPwIPaVFWNc1InQDR5MRMKvlXZRtU+4GV2H0vX4OPQRybD48FSHHesrUwKBfApeIuJf+Qg0zDGbBfmrl+9K6KX+EAgD9CNUn7gnQAXoI6bMCPdbN81PhbvtaKvU9fn9PoKdAPrdATqkyTtX3q9a9yxVb3enxdlBQEuqJOXTtuLnMgbAfTszRCgB0YR0BoOel3KcWueUsgMtZFW+7hoRFcXOr3GORv9+1j4jQ6wW6D7ucxWepHxkDVAXzH3jjzeYH3vg7zXe+912De70wzb/OHyjE5u7X/JC5++u5874KYL5GTQnXAHQJXvBsAOgAXXWHcMHbmYAefqWePVgmFYnbueTpQ2OGKXZrW/wAG5eKV+U+/9XhITcOsLG09NxcuR+ZrwVyGP0/G613QNfxuF1wB8yFIWGROQB9kVzbFwboAB2gD79n8oDup6SXgDyebl/+qxKL1tcMEvxrAPpyP8i7AqAL88nVgW5h5rsltdd6ado8Vmfo/tzDWmLz2rauqX3pa/ehz0Xo9t6SDpbJ+2oZIOoHnXjHuU5fm5qnjqXC86xwpdxxKmlU58IzjORz4J9r7xzUU3P8fv1hyp3oPFd9qeUAujDPAHSTbgboLlOR2m+eO/hQWu45h77sK3VGoM+1cA3Ic2A6tYI+ln6fG+DEpg7m2sbnkhUA6MK8A9CFOaQSc+oA+lxkXjICjjk+dX9bdg3IdX5iQS9bCnW1uO7WtA+1yi68DUBfIPwpigJ0YW4C6MIcUok5AL2EI+eAbuEcg2546lsJe8bpc/OOA7X/6Pgg8TUox7ntpf1xTH0A/Rjdk3cF6MIcUok5xwI959jRdHTrL9d6aAypNdnqX+bf+mWfpXpTz0Hr0viD55NPwVZXMOHp2Jx5+F5Omb06k0u/t3r/m9rNftPrFKxMIfiXZQn2agf3WaoAQF+q2MblAfrGAl+0+mOB3jHXQlidFT7Y9xyB7SgY9qLOHtg3vVe71ZQyIDeA93PLZggwfPnzzUOY68FAa+p10AuBP5daj8F9ruOFNi6bPhjuWx+Kp9p070geA7rR7T5nIJ+fQAGALsxJAF2YQyoxRwLQrZQaMCrG7ujSw7MLxU0E7oDmIu4Q1oPkcgBg+5kPdB906fS5g2NOit3vILkgT82Zp1L1sU7o29advq4HIv5gxdU3js7dIAqgV/Il56Q4eY4E6PJ8UoNFkoDux779g0AUcR72KV9mH9s42s73hMXdo3nop4eZID0dmds5ZJfKtxFrDmTXgtxMG9h0xfh0uHCw4P9t7XKPUjFtti330u4DDVIDkNQgI19zSh6vABH68T4YWADQhTmkEnMkAn0raV3s6rbEuYeOjKPu8c7z3Mh8CHINZMvnqYy5Givoz4PUeJ+h8Acf43LmUDc3wRBfMzCM1F1pgL5Vv5NQL0CX4AXPBoAuzCGVmHMVoIcwN+6bj8yHi+LCiHvqb++O+vx1u4RvouN0RcZTCf46AAN9XTQxAHB38NtnpxhiqfecdlTS4S/aDIAuzPEAXZhDKjHnCkAfw9wHWCwyV7hUs/neXHMXNfvvxZ6ONhwomL9Myvz51HVvqQ35uz44tZagQ7+3Wj8Wofcjg65Gs1vgmamNSr4e1TQDoAtzJUAX5pBKzKkd6IM5836lux8tj3PgwzR0ah58PBBIx+BmgHDcK1zR79muV8WNge4yE88PRI5rN3e2CgB0YX0BoAtzSCXm1Ax0h2IL7XSKPTx0xQTUIcyC1HffB1Ipa/W+n+o+qtM8zKlw3Ta+4Rx9OAwxbSZCP8pX29wXoG+j6+paAfpq6bhwQoH6gZ4Lc4Mx+//7g2lG2plV9sPUe0zgDvI9Lw+MdG8vXRQ+nj8fL4ozZdiyVtfPBkAX5k+ALswhlZhTK9A1lnqY+lB34DYuDFPn9qIUgGNAT6Xl/U5yDNBNjkDZbF/D9o6nCWLrCyrp7BduBkAX5nyALswhlZhTI9BtjDl0UQDuQeQc20uWBvp4MVwEgv3m8XAwsW/HMUCPLXBr+wHPsKUAfV8P7XM3gL6Pztl3AejZUlFwgQK1Ad1h24d0Dsxj8+ApId1cuhkXhBC0owUJMFfReZh1MH+r89zdCXIugmf+fMEX6CRFAbowRwF0YQ6pxJwagT7eWjZMs9/atjEHyky95j43C8f6x8B4uev0gTT7dRptffvw2ukParocRr+dbjj4Aej7+WmvOwH0vZTOvA9AzxSKYosUqAnoLtXuL/4awrxVR8cOn0QyeFyoEW8e5r7Ibh94bOPasroWOS9R2G5CcwMb34YO3vrY2zCTYNoO0Et4QVYdAF2WPxqALswhlZhTE9DHp7oFMO+fumad51Llw4eUrIOwq+24VPs8zM0KfcNytqxV8jWebQZAn5Vo3wIAfV+9r3K3WoAej85dKtlsw/KjbxfFDzehrYP50f3FT6i7CNs/0CYWjYdWsyDuaD9udX+AvpWyK+sF6CuF47JJBWoAupkvVvPifuQ9jM7jx7SGC+GWp9sldC9/IaDbP+5H3w7mJhPRRen9YrlxxmLptIMEHbAhrQBAF9Y7ALowh1RiTg1A166wm857qIdb0WIr0WMR6rkcq1Ls5mGrsTR/eKiOHbD4B8ewIO5cHl9nLUBfp9tmV9UC9M0EouLrKqBhruaEg+1p+i2139o/q3wYlQ9PfTtPuj2eYg8Xv/mz+mrKwc2w6/PbTWrD6zek3Gv9EgF0YZ4F6MIcgjmCFIhFoi4adYZ6i+A0ys4JMH/hW/yI1vF8+WgrnV7lPgY6K9wFdeuCpgD0gmKWqAqgl1CROmpUwEDIPs0sdurbuNUS9oqv8YUdgsTBGx/YjGEe25kXZjfOk61Yo+PVrgHowjwO0IU5BHMEKRDfTz1MJ3fZZc2p88FraYrdtdKcEzeYYw+ep97NVyTm4QW5GVNWKwDQV0u3zYUAfRtdqfXsCoSruW17xivYx1G5D3e5OsRXsfv2BgOa7s9UFsJlNJg/l+v1spYB9LJ6Pl0bQH9aQiqoUoHu6WeDqDOyOK470Ty+GlyuMD7MXSRt7c1Msevibhe+eViLv1DQfi7h2e1yfXFmywC6MO8BdGEOwRwhCsTmzE10bo9VcQ8PPe4EtzVi+WvU4zDPyUIEc+H9wCe89pwLBNfoesVrALowrwN0YQ7BHGEKuCeg+YvX7bY0sxpcQUv+Yi9rpYqX04vfPADrh6zYVfvKLfGBix3mDLfq+dMO5xrwCOuAos0B6MLcA9CFOQRzRCoQzjcbXJ0D5MrWpVvSbCJ9uPDNh7TvpqntfaTbRXboQkYB9EJClqoGoJdSknpQQJ4CdsihkGtyDfYgGGtr7nx5PAPhJiEs7IMFcYNUvDx9sOg5BQD6c/oVvxqgF5eUClHgcAV8/BqYqwVrYUYhvop92fa7MJ2e2q8vf0ricKed0ACALsxpAF2YQzAHBZ5UwDuItY/L3QE5fmQ+XMC2dPvdbHQ+Ov71yYZxuTgFALowlwB0YQ7BHBRYqUAc5LGofJweX3fCHdH5SldVcxlAF+ZKgC7MIZiDAisUGC96ywO5QXvk1LcZG4jOVzipwksAujCnAnRhDsEcFFigQN5WtHFE7h5ZHh6WkzvXTXS+wE3VFgXowlwL0IU5BHNQIEMBH+R2/XrOPLmteul8uW9Sv++8bZpWP1o2ddSrHUhkNIgip1QAoAtzG0AX5hDMQYEJBWz8bDGqjls12LRPhVN/pfeFm+T68hT7AOht26inpI5Xw4eG50b7uPysCgB0YZ4D6MIcgjkokFDAnxV/8UA+jJETT4i7qXB63Xz5KDrPOuaV6PwKHRmgC/MyQBfmEMxBgQmgP5q2uetd5ZlReU/7cK58LXCZO6eDOgUAurDeANCFOQRzUCCigOayjrJtBB5bzDY81MWwvBTITYLdPJrGDgaGc+e3PhW/drCA68+mAEAX5rFagP5//t23hSl7bXM+8W8+eW0BCrfebEsz8bmZI4+B2qF2PE9eArLp6Dw8oqZw86lOqAIAXZhjALowh1RiDkAv40g7b/6iIR4+2S02X/78PHnM8rnofLzSvUz7qUW2AgBdmH8AujCHVGJODUD3o85wc9YebnL3VJG5emKamj33X2GKPVy9XiIqt/dj7nwPn5/tHgBdmMcAujCHVGLOmYFuQeontfcGur23QXksMjewbtu2ud/V1PraA2LmO1wfneuEwHjfOXPn8xrWWgKgC/MsQBfmkErMOSvQdYr71jQPHZCqGWsT9ZrFYAaae8DdLIJ7aZr2VWTO3LNAbUW7KRvto1H9zW2lOhPReSkla6sHoAvzKEAX5pBKzDkj0P35ansAi0V4e3s0TWsOb9ka6Oouas7cxtzDBXDjXed2kZx5POr4lV4+N9/ZSs+db63dfIsoUVIBgF5SzQJ1AfQCIlLFSIGzAd3CXK0kV3u8XzV3vaZcn6GiiWhwadaZ23+Xd7xLtZud5kNIuzlz+77Zka5e/klx5p05kIdTC7Y1w41p+nxXV5k/i39rm9vj1n0cH0ykNrmlBh7xZX7ldabGMgoA9DI6FqsFoBeTkoo8Bc4EdAe+R9Povd4W75F5aZ0GV8vTbj1KSzm+zxDcHtqMIaQdzFVy3QBdH/2moRq++il1j9J3MzLR1+jt7MFrPDuuph7MPcZ5CVPaHEDnnQyv7LmpgdDQpl7WzgA7/LBm+CAnii/Vo7avB6Bvr/GiOwD0RXJROFOBswDdgsUcpXrr1nyFsevwABUN1H6B2PMpeA3y7pZmr7l9JVLo3ZjDHvKSSrXfb23zaNUp72bxnLqJZbMCteG7mXu/t2aA4urqzmvvBzemrBsfOATfte3DeXY9YeBR3LTPZjYe/dnzg4yEcoZOOLjMhMmKuBewz/wC7lQMoO8kdO5tAHquUpRbosAZgG6PaFFJdhO9Ppq2myfvQ9l+MZwHsyANP0Rdvko9yLsKHjr6H87e59c2V9IsnLOxtoazTqV37dL/cQMIfSpdj/do7O6dGRcfeAwtcoOk2+2hBxou/9E2alCgonr1PzutYS0yAw13nE4kuTDXeD7fSAGAvpGwa6sF6GuV47opBaQD3YeJAsSre9M8/FCwR18MH+o9Ffe61e9LIkedFegjcjMvb8YNNpXuK5sDy5gn4tizJ8fm1+qUKtrjFcDVDMfdpe2HrX40D5VR8FL3LuA3Ni3RvKjtVNYrANCFdQaALswhlZgjHehGZhUZGjjoueLBawoXY8i5WeS0A32Q63vqjICJkjW4bD58VR9YGrfOIT21nG0/jNqMgh+97719cJUrLnQRQBfmbIAuzCGVmCMd6BZnIdZ6sHcfuDT1eBW5HRRMuyy8w1LwhrWnIubUfebAnbLe7bsfHut6dAf1BxT90sCjjbrs/QG6MNcDdGEOqcQcgG4deUag50bnawcLSzr5cLGdvVItHjQz7va42yV1UraUAgC9lJKF6gHohYSkmoECAH0voM/Nccegm5sliC2GiwO2fPcf7ixw9bv7G6inMiflLaLGsQIAXVivAOjCHFKJOVKBPo6XDSDcc76NA9wa7LkIe+7zPIfG9oX7V6rjXc0rBdRcSJvWDeuK2TgFc7+OvPbll5oHOU92y1dz65IAfWuFF9YP0BcKRvEsBQB6lkx9oe2AnruIzR+YuD3mrhW59Sxrd7p0bAATW7hY6n7Us0YBgL5GtQ2vAegbinvhqo8C+gev35tU/a133+niXLNH7cPXXx+Ut5+7N+cgYj7/4PXXgnq+ENhhABXa9/l3u3LBbT78Yqq+MhkBY9xUOn4uQt+qc+eBfO/hxVatPXu9AF2YB68O9G9+85sjj3zuc58bveeX8z+fu95+Hl4zd49YN0ndN6xL3TNWVr2XskfdL2bT2u4K0GUA3Z1Fbz05t5AtlrqPpehz0vZLek8eyN3kSPhI2SX3omwpBQB6KSUL1QPQAXoM8s92L4CeD/QPX3+t+fxXTXkbmb9lI/cuiraRvXt/qYemQD63B38uHp77PGVraqCQskdB/N6tIthrcd5Sna9VHqAL8zdAN0BfGtHa8nMRuP/5EnBOlZ2KwFVbwmzClA1hXaW659FAH6fObcuGKfKw3Bic0ylum2oPQRu+b/+24LbWWIBvB/QUyGOLz3K3q63tJamoPjUgcO/r8+Y1zFOL5tbaxHXPKADQn1Fvg2sBOkAvmWq3XRSgmzlwC3oZQJ+CuP/j8uz8+Vw6fi6ij33uH3QzN22wwQ8lVUYVAOjCOgZAB+g1Aj38mqUjcbNIzm6FcpH18P3U13YuQk9F3nZVu0uxm/u5DIG9v7lzOuW+ZJHcFGinomSnT1yHpZF3rJbpQYB5ZCx7zoXhowHowjwC0POAHrotTLn7n+cuXpvqCnMp99i1vk0x+/xFcfb6LWCu6j46QgfoVoG5aHnuBykG+pIAt/ef20dPVD7nqSM+B+hHqD5xT4AO0LfoklcDempuPEy5z83Zz0X84WI557vU9rrc91O9YCo9Ppc6V3XOLcbzU+ljG/ScuX5+jaoHqG/xXX2mToD+jHobXAvQzwn01CI+1UWWbFurbVFc6isSprJTi99Sqe1UKn5ublwW0HMAHCq49Jo5gPv1z9StWe+XAegbIOCpKgH6U/KVvxig5wE9tY98bhX73Ocpj86l3AH6su8CQM/Va+mCuBhkZ9Ln00F5b6h78Apb1HK9t3c5gL634jP3A+gAfYt5dLkp9yFs5k94M1+gPhLPPtltuKjNfg3Dk+JS2+vG5cJ97SV+SHLm13Mi5Ll97N7EgMqgTwba5n7AvIR/t68DoG+v8aI7APTpg2WW7DO3wvtp7Lnrj47QbYpe/bck2AG6RKCvWcwWA/qz8/KxtLu6j3kkqsK5eRGZL/oxP6AwQD9A9KlbAnSAvuTAm9zue0agDw6H6ZjSH2TyGIaVLsp8dl43lZ4O6x2Ws9vepqPdlLeWzouH9eRE9vn3NlvSzKI3Y5n5n30vt89Rbn8FAPr+mk/eEaAD9JqAnu7sC6PKaoCeC+8Vc+HFfstiA4RnB0rFjKOiCQUAurDucXWgC3NHNeYcFaED9NwutAXAcyN3d28bkTurAXmuByWUA+gSvODZANCFOaQSc+QBPRTWROw2pX6zuWsbyHf/7Wdxg4g9/9SypQemzJVf00FSkFx7r7WDAf86u/CNefI1HpVyDUCX4onODoAuzCGVmAPQrSPnoDk9V/58dxhCVNeXm4XvC/tWTLUnVrG9v3r+/E2vc1NjJxa+Pe9ZCTUAdAleIEIX5oX6zJEP9KHmLlncRe594Gj+kY7InwO2XdxmF3b3+NUno6kbKxC6V9u9n14MF6S9uz3fZpGZfZl/uYVnlvL23e5MNrtfvP+vPrJtckQwWKw32G/u2TWwySyG43VOBQC6ML8RoQtzSCXmAPQhPD0kB4Du/rT8tp+WArphdBcc28FJDOiunANs613bjS/C/hmdNp9LAQDwSr7mPJxFmiMBujSP1GEPQBcE9IkIuEevCq29dQJmy9gwcre5ehvZm9ha7Rt3axHiUX8dfZpWjBUgQhfWKwC6MIdUYo4MoNv9zPaxmy49rudxdRQcLI7r9kM/5wY/1zyXkvfAr9LZrV0sFgt9JUS2brWgWkjInvHnesrZrwbowjwI0IU5pBJzZAC9z18rTvbxpgGRyUVvc4BJbJ45lYbu5qW1QUOIb2NbmQ7mD4USyfgyN6IW0QoAdGHuAejCHFKJObKA7sFcHzDaGsDfVDRso/dSwtsTzlwa2t092KKlI3KXtt5ugFGqbdSDAkMFALqwHgHQhTmkEnMkAn0faWMp9iB93gflUtPq+yjFXc6vAEAX5kOALswhlZhzXaB7af7+PPJusXhyD7afbq+kA9CMSygA0IW5GaALc0gl5gD04cyyPZHOzotLnh+vpAvSjB0UAOg7iLzkFgB9iVqUzVUAoOcqRTkUOK8CAF2Y7wC6MIdUYg5Ar8SRNAMFJhQA6MK6B0AX5pBKzAHolTiSZqAAQD9PHwDo5/HVmSwF6GfyFraiwDoFiNDX6bbZVQB9M2kvXTFAv7T7afxFFADowhwN0IU5pBJzAHoljqQZKEDK/Tx9AKCfx1dnshSgn8lb2IoC6xQgQl+n22ZX1QL0zQSiYhRAARRAgagCAF1YxwDowhyCOSiAAihwEgUAujBHAXRhDsEcFEABFDiJYaxFKQAAFoVJREFUAgBdmKMAujCHYA4KoAAKnEQBgC7MUQBdmEMwBwVQAAVOogBAF+YogC7MIZiDAhsooB7UGnuw6wa3osoLKQDQhTkboAtzCOagQEEFHMgVzu3z1wvegKourQBAF+Z+gC7MIZiDAoUUMDC3IHdAJ1ovJDDVNABdWCcA6MIcgjkoUEyB1uTZb35k3ja35kb6vZjG164IoAvzP0AX5hDMQYEnFbD4VtG5+t+tuXs1mpl0oP6kyFyuFQDowjoCQBfmEMxBgScUmIa5qbhtHs29uROlP6EzlxoFALqwngDQhTkEc1DgCQUU0B/NQ0fg6UVwLJB7QmIu9RQA6MK6A0AX5hDMQYGVCjiYqxT7FLQB+kqJuSxQAKAL6xIAXZhDMAcFViigU+1t27SDnWnhenaD/GYwp77iZlyCAp0CAF1YVwDowhyCOSiwUIHhXnN1sX0nBnR7vAx70hfKTPGIAgBdWLcA6MIcgjkosFABt9/cv9Atj9OIv6kA3gc8QF8oM8UBuvw+ANDl+wgL5SkwxOVx9mlEt22319yfGx8e9Oq2qcX2ph9nP3c+twJE6ML8B9CFOQRzRCtg146rmWg/uX2E0ePIfDzMMIjvIvOe90TnR/irxnsCdGFeBejCHII5YhWwx7MoLj70gS0G6UdE6+N58/Hw4qaCcX1KnP5Hf2CcOw7WzbaLFR3DRCsA0IW5B6ALcwjmiFTAP7BFEfzW2uNTjzkj3aXarVzhsMKcEGfgrda13/QgxG1nI0oX2dFOZhRAF+awWoD+/j99X5iy1zbn7f/6dn0C6Llq1SwLcTMz7dLa+zyi1KXa/dXs1i4ru7LK7EfXkfrdROnm5cO/T8jX5y9atLkCAH1ziZfdAKAv04vSeQpUB3RNbf/JZV6KW7PSRMThRrE8teZLDTei+Qvexgn/m96Pbg+X8QFvbbbXqJUAdlXAvA1blBgOLfYZEG3RjqvWCdCFeR6gC3NIJeZUB/T+5PNw65eBvDof3cynl0ll+7WEO8fN0a7+aXDDFe3+zLhJtZsZfzvnb7qYzSuYx7fs+QrbNhys7GkJ93pWAYD+rIKFrwfohQWlOq1ATUAfprgtDL0IvYvLTfrdf7JZujOEwwKH2PGhrWqwoON/FXnrCXxbb2w5nqvZzpu7J67FtrXZ1Hxex7XDiHAIkXO1D3KzBuDRPLpHu7rJALM2YatMR46dlMlXAKDna7VLSYC+i8yXu8kZgR7OSDt0273bPsTHQPfn0lMODxE8iI1bsxnOTHUnoubR1jOHVn9Vu5kdMAvhXE2xBHdedK6HKfpwmuVp8XG2wT7W9Wbm9/v2HrPA8HJfzoINBugFxSxRFUAvoSJ1hAqcCegxpPlpbhPh+qva/QjZxZMm1rXlxn1ivO2tu4simmeEgtxj+FZQWSwyHy6CcwMDPypPRfTx/ttHza86O19uzYs3tZATrftlzLREt9HPN6t7tx8q6HNyzN3XZAL4Nu6nAEDfT+usOwH0LJkotFCBswA9BlmN7zA7bfdzj3RwkHSp7TjMu/h7gZKpoUaYkFahc9Pc1Er2h4Wg2nduN635EX8359+2/b507+iZfoncIBFwa5uXx3Avux24pIDrDx0GIB9lHmIL/Nx6BIC+oLscUBSgHyD61C0BujCHVGLOGYDuokeXAtZxbn/uebcKvEtfD+PFMEFv6zAr3W10GYJtvGguhHYMcLZTDPMGrqu42XuVEjdUVmT35/Nj0blduG+yCjatrv7Qe9bVyn317267m8mMm3bbrMX9dhul4YdDHLscL5Xa92fM8wZHlXxFqmgGQBfmRoAuzCGVmCMd6BYdZsV4DDZ+FJxKVQcwur00t/bVYD28AbuD/bR7YzAPUwVhDV1qWpO8ae530x4LYVN6OMRQZ7/baWs9/Li3jZ6+7+azLazt30N1fOja2l0Jp9pwf3683eEAxR8k8ZjXM/wUAHRhXgLowhxSiTnSgT6MoWPQDIEeJn99+HSf9SfImb/NvLvdMpaKUG0sbx0/FbFbOIfgb3WUrMB801F5Kso37+v/7z/PxWO+hnkwp+9nAkLdzLp+lwnQdd9ezFGz+qQ6/zWXQPc173MGlXwj6mwGQBfmV4AuzCGVmCMe6P2pbzkADaEbwtdEvDpBr1PdrzpsWiilos3YIMHW7T7rH6bWR9tjoMe7TZhZ8IFpwK5Q7HapxwYdfhQ9tk1n9/Ved/V66DT9vctSuDPj4yAft6vTuZ/iqOTLUHEzALow5wJ0YQ6pxBzxQB8lxsewMu8MITh8z0s/60Vpaup6eErbcA94yrlzgwr/utCe2N9hW2JtCG1ZAnOvPhvN316atr03r/oz45d05OHAwyXrp7IaS+qn7FYKAPStlF1ZL0BfKRyXTSogHug6CsxJAc9H53Hux6JjV5eLTn1oxeyJzd9b6WOfDefL8zd+TQ0qpupUAxiV7b8He96nBgyxqN/Tpj/XDqBL/5kB6MI8BNCFOaQSc2QDPQRKbnrbB2lqTt1G9eko2czYxwCairZTA4+p8lOZhbAdUxF8GuZu1iLn+nBgFBvImHr6RXk7H0lbyVdv12YA9F3lnr8ZQJ/XiBLLFZAMdLVQzQB1DCL3zlRk7OsRA95ERDuaHw5XeucOGuzAYXqOO32Ias7gYRrmg1NoR1qmMg9pkFvkmxJE58u/dftfAdD313w6Nfrea2EWrTOHx6eu022rqyQDvVybvVXjg6h7aeo8ZtFcKj4FdBP/56XacyLrYX3+WkIXSYf2p+oNB0lhOX9wAtDL9dPtagLo22m7qmYi9FWycdGMAgA9Bjkbg+Z0nxygh5mCJTCfsiESmbdNc3+jaV5e3FihO511pjHTEXn6YoCe00uOLgPQj/ZAcH+ALswhlZizJ9A/eP1eVLW33n1n8H6qnCoUlp1yQ1jP2919/INW1fUfvP5aX42rfxi9+2Xidswt3LO3yI3K5zpYPM3e/s3/a+6feqUPrHEgz43wp2zs6tCn85m2uumQOVv5/GgFAPrRHgDowjxQpzkAfU+gW7Aug/pwH7hBqXmNI/2OtQa2ffCcM7UwlX7vAN4dtevOfM97BG2d35xztQqgC/PXVSP02997adr/qQ4AGb/UZ+HLL+t/Hr4fK6fei9Xp3yO0Zap8ri36p9lro9/msP32filNlnZbgL4n0C2Mc6N58xjUYco8BXMD5OkV7bEFhPMgNyMDc14+K9uXfsNklAfoMvzQWwHQAbrGQTeIOSPQU18pmxq36e7w7zVfRVVHmD63afO33v2CrjL8WwHL3Ttdxlxrpg9sXfM2zqW9x5BPntCmbzauzz7wZTyrPbfIzVo/AXybDOgbytz5vM/llADocnyhLQHoaYcsjWJzyy+FZyqbMHW/WBYhFaFPZSvWdtc9I/RrA32Zh8YH2qS2zZl6+1T7gLMFQN7tOFfnvbsV+cB8mTePLw3Qj/fBwAKADtCvBnTf40sWw9kIOhWhf/7df6ar/vD114Mo20Xon/+qidA//KJZMOcicQOzvAg9FvHO/7AYmOfC2K8vJ30+FY1HbOv346dOjZtvDyWOVwCgH+8DgN6lmOfSy7kRd/9TFszLpyJxIvR9vgSplHvtQJ+eH/fBmz/nPu+xBfPo/dPcgPm8rrJLAHRh/iFCXxehp1LasdpSC97mBhSpgYL/fup+/qBhztZcO5Z03SNT7kvmylNlp95/bg7dbKWbv6+J5M0rlYqemh9fsv972er4YT9Ydu2b9zeal8dL93Q2gL7kOyWxLEAX5hWADtAB+ngf+vmAblaK3/WKcfuanh/PP1EuNbDwBxQxsI/fU0D/6OGdTsMRr8KIsMwcgL5Mr81LA/R1QI9Fz7kp+j1T7jnb1mqbQ79yhD4fPS+Zf8/PDMz/UHnb3/pqWQQ3r5vsEgBdmH8AOkC/CtBjJ8WlFsVNDQrCesIUvD0xzi6OUz3s9df+ue5o33/5yCTRu8Vp47r8VHsYGacOcsmZC18Ccj/Nn1N3/o+amt+/3/wsQv61lJSnAEAX5pMrAz3mitQhMLmHuaQOlvHvJS1C1z/fFe1Dn5uf9n1xbqDnzF/nlEltTyv/Y3W735pWnSXDqwoFALowNwL0oUMAevz0vKXd9shFcUtt3aq8n1B+9Bu6/fPKn72zD+tY+jodXVtzXA5AHemaG42Hg4S8+XN1r1e3e/MS3vxZGbj+MAUA+mHSx28M0AF6bRG6lK/YcUCfAnOrz2NXJXSp0RGwTj2z/U2Vyjn8ZW6B3CBHJcVF2PGkAgD9SQFLX35VoJfWkfqGChChBwPFbqOWORmtI2l2pym3OM1g18HXndQWmWMPj2UdHdOa24BuQVxjBhPpbXi59VFOigIAXYonOjsAujCHVGIOQJcBdP3Qk5vBaNOarW3DB6H4lPY3vJn3TYDeNu3Df2xqTiedmrtndXuOgmcoA9CFeQmgC3NIJeYA9L2BPkjwm0epaT4bmFuQu3f8iDw2J949n/ymBgTq392gQDcrjOZz5tRtZoAIvZKvuG4GQBfmTYAuzCGVmAPQQ0eGp6ItWYCW0ykeA4ibK8J7hJFxF4V3wHd3yVsZHz+YJmd7HBF6jkfPUAagC/MSQBfmkErMAeh7AH0NxFN22fdzgJzqpDnXAvNKvuJE6BIdCdAleuX8Np0D6MNDUm1yepRS7qeZLYzURmofTDonbSLkaGT8rD+DevsAOnW/pdBsdTDfPzOlN3dJe6bLDuftn9WD66UoQIQuxROdHQBdmEMqMeccQLf4tSedKKSPwXRrWw07BfE4mObOTM91anjvME0//NyeSPf8ynE7YkmdRJeyXw1izGK7cXrf6DWf9s/VhnISFQDowrwC0IU5pBJzzgT0Hjo6SrVbq4wjDMyDaHzkIwdEB/wcR4aR9MNs7BplBJaCNufeYRmzbE63Wa+C82Ecm3uP3WNu7n1p5mBNO7hmTwUA+p5qZ9wLoGeIRJHFCpwL6LZ53ZYutaL71vSP+HRR+xyQ3JawYfTsy6fq6FL2o33dU2nruXsvdlHiAjWkaPWqdm1e9LapyDs18AgzDaVspZ6jFQDoR3sguD9AF+aQSsw5J9C7qLzzwXjPdp5zDO7c4EBdNQy6PXB7UB9G93sBPN0m2475VofRvH9wjZmmGCo7XyMlzqEAQBfmJ4AuzCGVmHNmoJd0QY86vWjO1CwN3NPtjc2vpwGu1xl0bXWLDNUdjh+glPQrdRkFALqwngDQhTmkEnMAeg2OjJ8iFy52M+fXdGsPgqw7q9tr6AfpNgB0Yf4F6MIcUok5AL0GR6aic5NnsC/7sBf/PUBeg//n2wDQ5zXatQRA31Xuy9wMoNfgavtkNj/s9tLteuu9WTznYE5qvQbP57YBoOcqtVM5gL6T0Be7DUCvweFdhO7N/7t0u4L9HZjX4OYn2gDQnxBvi0sB+haqUidAr6UPeNH56BCZqb3qtbSfdkwpANCF9Q+ALswhlZgD0CtxZN8Ms/lMPc19vNucNHtt3s5tD0DPVWqncgB9J6EvdhuAXqfDXUzO3vI6PbysVQB9mV6blwbom0t8yRsA9Eu6nUZfTAGALszhtQBdmKyYgwIogALVKwDQhbkYoAtzCOagAAqgwEkUAOjCHAXQhTkEc1AABVDgJAoAdGGOAujCHII5KIACKHASBQC6MEcBdGEOwRwUQAEUOIkCAF2YowC6MIdgDgqgAAqcRAGALsxRAF2YQzAHBVAABU6iAEAX5iiALswhmIMCKIACJ1EAoAtzFEAX5hDMQQEUQIGTKADQhTkKoAtzCOagAAqgwEkUAOjCHAXQhTkEc1AABVDgJAoAdGGOAujCHII5KIACKHASBQC6MEcBdGEOwRwUQAEUOIkCAF2YowC6MIdgDgqgAAqcRAGALsxRAF2YQzAHBVAABU6iAEAX5iiALswhmIMCKIACJ1EAoAtzFEAX5hDMQQEUQIGTKADQhTkKoAtzCOagAAqgwEkUAOjCHAXQhTkEc1AABVDgJAoAdGGOAujCHII5KIACKHASBQC6MEcBdGEOwRwUQAEUOIkCAF2YowC6MIdgDgqgAAqcRAGALsxRAF2YQzAHBVAABU6iAEAX5iiALswhmIMCKIACJ1EAoAtzFEAX5hDMQQEUQIGTKADQhTkKoAtzCOagAAqgwEkUAOjCHAXQhTkEc1AABVDgJAoAdGGOAujCHII5KIACKHASBQC6MEcBdGEOwRwUQAEUOIkCAF2YowC6MIdgDgqgAAqcRAGALsxRAF2YQzAHBVAABU6iAEAX5iiALswhmIMCKIACJ1EAoAtzFEAX5hDMQQEUQIGTKADQhTkKoAtzCOagAAqgwEkUAOjCHAXQhTkEc1AABVDgJAoAdGGOAujCHII5KIACKHASBQC6MEcBdGEOwRwUQAEUOIkCAF2YowC6MIdgDgqgAAqcRAGALsxRAF2YQzAHBVAABU6iAEAX5iiALswhmIMCKIACJ1HgEkD/6KOPTuKOpvnih//iNLZiKAqgAAqggBwFALocX2hLALowh2AOCqAACpxEAYAuzFEAXZhDMAcFUAAFTqIAQBfmKIAuzCGYgwIogAInUQCgC3MUQBfmEMxBARRAgZMoANCFOQqgC3MI5qAACqDASRQA6MIcBdCFOQRzUAAFUOAkClwC6FJ9EdtOB9Clegu7UAAFUEC2AgD9QP8A9APF59YogAIoUJkCAP1AhwL0A8Xn1iiAAihQmQIA/UCHxoD+iX/7iQMt4tYogAIogAJnVeBvv/K3ZzV9YPetbdu2ipbQCBRAARRAARS4sAIA/cLOp+kogAIogAL1KADQ6/ElLUEBFEABFLiwAgD9ws6n6SiAAiiAAvUoANDr8SUtQQEUQAEUuLACAP3CzqfpKIACKIAC9SgA0OvxJS1BARRAARS4sAIA/cLOp+kogAIogAL1KADQ6/ElLUEBFEABFLiwAgD9ws6n6SiAAiiAAvUoANDr8SUtQQEUQAEUuLACAP3CzqfpKIACKIAC9SgA0OvxJS1BARRAARS4sAIA/cLOp+kogAIogAL1KADQ6/ElLUEBFEABFLiwAgD9ws6n6SiAAiiAAvUoANDr8SUtQQEUQAEUuLACAP3CzqfpKIACKIAC9SgA0OvxJS1BARRAARS4sAIA/cLOp+kogAIogAL1KADQ6/ElLUEBFEABFLiwAgD9ws6n6SiAAiiAAvUoANDr8SUtQQEUQAEUuLACAP3CzqfpKIACKIAC9SgA0OvxJS1BARRAARS4sAIA/cLOp+kogAIogAL1KADQ6/ElLUEBFEABFLiwAgD9ws6n6SiAAiiAAvUoANDr8SUtQQEUQAEUuLACAP3CzqfpKIACKIAC9SgA0OvxJS1BARRAARS4sAIA/cLOp+kogAIogAL1KADQ6/ElLUEBFEABFLiwAgD9ws6n6SiAAiiAAvUoANDr8SUtQQEUQAEUuLACAP3CzqfpKIACKIAC9SgA0OvxJS1BARRAARS4sAIA/cLOp+kogAIogAL1KADQ6/ElLUEBFEABFLiwAgD9ws6n6SiAAiiAAvUoANDr8SUtQQEUQAEUuLACAP3CzqfpKIACKIAC9SgA0OvxJS1BARRAARS4sAIA/cLOp+kogAIogAL1KADQ6/ElLUEBFEABFLiwAgD9ws6n6SiAAiiAAvUoANDr8SUtQQEUQAEUuLACAP3CzqfpKIACKIAC9SgA0OvxJS1BARRAARS4sAL/H5b4kTFrTJ9vAAAAAElFTkSuQmCC'
    cosmetics = [{
      name: 'Venom',
      image: new ImageLoader(),
      id: 'venom',
    }, {
      name: 'Blue Tint',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'blue_tint',
    }, {
      name: 'Purple Flower',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'purple_flower',
    }, {
      name: 'Leaf',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'leaf',
    }, {
      name: 'Basketball',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'basketball',
    }, {
      name: 'Purple Top Hat',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'purple_top_hat',
    }, {
      name: 'Terminator',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'terminator',
    }, {
      name: 'Dizzy',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'dizzy',
    }, {
      name: 'Knife',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'knife',
    }, {
      name: 'Scared',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'scared',
    }, {
      name: 'Laff',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'laff',
    }, {
      name: 'Hacker Hoodie',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'hacker_hoodie',
    }, {
      name: 'Error',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'error',
    }, {
      name: 'Purple Grad Hat',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'purple_grad_hat',
    }, {
      name: 'Bat Wings',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'bat_wings',
    }, {
      name: 'Back Button',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'back',
    }, {
      name: 'Fisher Hat',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'fisher_hat',
    }, {
      name: 'Ban',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'ban',
    }, {
      name: 'Blue Ghost',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'blue_ghost',
    }, {
      name: 'Pumpkin Face',
      rarity: 0,
      price: 0,
      image: new ImageLoader,
      id: 'pumpkin_face',
    }, {
      name: 'Pumpkin Hat',
      rarity: 0,
      price: 0,
      image: new ImageLoader,
      id: 'pumpkin_hat',
    }, {
      name: 'Red Ghost',
      rarity: 0,
      price: 0,
      image: new ImageLoader,
      id: 'red_ghost',
    }, {
      name: 'Candy Corn',
      rarity: 0,
      price: 0,
      image: new ImageLoader,
      id: 'candy_corn',
    }, {
      name: 'Yellow Pizza',
      rarity: 0,
      price: 0,
      image: new ImageLoader,
      id: 'yellow_pizza',
    }, {
      name: 'Orange Ghost',
      rarity: 0,
      price: 0,
      image: new ImageLoader,
      id: 'orange_ghost',
    }, {
      name: 'Pink Ghost',
      rarity: 0,
      price: 0,
      image: new ImageLoader,
      id: 'pink_ghost',
    }, {
      name: 'Paleontologist',
      rarity: 0,
      price: 0,
      image: new ImageLoader,
      id: 'paleontologist',
    }, {
      name: 'Yellow Hoodie',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'yellow_hoodie',
    }, {
      name: 'X',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'x',
    }, {
      name: 'Sweat',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'sweat',
    }, {
      name: 'Spirals',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'spirals',
    }, {
      name: 'Spikes',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'spikes',
    }, {
      name: 'Rudolph',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'rudolph',
    }, {
      name: 'Reindeer Hat',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'reindeer_hat',
    }, {
      name: 'Red Hoodie',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'red_hoodie',
    }, {
      name: 'Question Mark',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'question_mark',
    }, {
      name: 'Pink/Purple Hoodie',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'purplepink_hoodie',
    }, {
      name: 'Purple Hoodie',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'purple_hoodie',
    }, {
      name: 'Pumpkin',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'pumpkin',
    }, {
      name: 'Pickle',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'pickle',
    }, {
      name: 'Orange Hoodie',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'orange_hoodie',
    }, {
      name: 'Helment',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'helment',
    }, {
      name: 'Green Hoodie',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'green_hoodie',
    }, {
      name: 'Exclaimation Point',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'exclaimation_point',
    }, {
      name: 'Eggplant',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'eggplant',
    }, {
      name: 'Devils Wings',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'devils_wings',
    }, {
      name: 'Christmas Tree',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'christmas_tree',
    }, {
      name: 'Christmas Lights',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'christmas_lights',
    }, {
      name: 'Checkmark',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'checkmark',
    }, {
      name: 'Cat Hat',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'cat_hat',
    }, {
      name: 'Blueberry',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'blueberry',
    }, {
      name: 'Blue Hoodie',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'blue_hoodie',
    }, {
      name: 'Blue Helment',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'blue_helment',
    }, {
      name: 'Banana',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'bannana',
    }, {
      name: 'Aqua Helment',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'aqua_helment',
    }, {
      name: 'Apple',
      rarity: 0,
      price: 0,
      image: new ImageLoader(),
      id: 'apple',
    }, {
      name: 'Hoodie',
      rarity: 20,
      price: 1000000,
      image: new ImageLoader(),
      id: 'hoodie',
    }, {
      name: 'Purple Helment',
      rarity: 20,
      price: 1000000,
      image: new ImageLoader(),
      id: 'purple_helment',
    }, {
      name: 'Angel Wings',
      rarity: 4,
      price: 20000,
      image: new ImageLoader(),
      id: 'angel_wings',
    }, {
      name: 'Boost',
      rarity: 4,
      price: 20000,
      image: new ImageLoader(),
      id: 'boost',
    }, {
      name: 'Bunny Ears',
      rarity: 2,
      price: 1000,
      image: new ImageLoader(),
      id: 'bunny_ears'
    }, {
      name: 'Cake',
      rarity: 4,
      price: 4000,
      image: new ImageLoader(),
      id: 'cake',
    }, {
      name: 'Cancelled',
      rarity: 1,
      price: 1000,
      image: new ImageLoader(),
      id: 'cancelled',
    }, {
      name: 'Candy Cane',
      rarity: 5,
      price: 1000,
      image: new ImageLoader(),
      id: 'candy_cane',
    }, {
      name: 'Cat Ears',
      rarity: 4,
      price: 20000,
      image: new ImageLoader(),
      id: 'cat_ears',
    }, {
      name: 'Christmas Hat',
      rarity: 8,
      price: 30000,
      image: new ImageLoader(),
      id: 'christmas_hat',
    }, {
      name: 'Controller',
      rarity: 10,
      price: 1000000,
      image: new ImageLoader(),
      id: 'controller',
    }, {
      name: 'Deep Scratch',
      rarity: 4,
      price: 20000,
      image: new ImageLoader(),
      id: 'deep_scratch',
    }, {
      name: 'Devils Horns',
      rarity: 4,
      price: 20000,
      image: new ImageLoader(),
      id: 'devil_horn',
    }, {
      name: 'Headphones',
      rarity: 4,
      price: 20000,
      image: new ImageLoader(),
      id: 'earmuffs',
    }, {
      name: 'Eyebrows',
      rarity: 4,
      price: 20000,
      image: new ImageLoader(),
      id: 'eyebrows',
    }, {
      name: 'First Aid',
      rarity: 4,
      price: 10000,
      image: new ImageLoader(),
      id: 'first_aid',
    }, {
      name: 'Flag',
      rarity: 4,
      price: 20000,
      image: new ImageLoader(),
      id: 'flag',
    }, {
      name: 'Halo',
      rarity: 1,
      price: 5000,
      image: new ImageLoader(),
      id: 'halo',
    }, {
      name: 'Hacks',
      rarity: 10,
      price: 1000000,
      image: new ImageLoader(),
      id: 'hax',
    }, {
      name: 'Low Battery',
      rarity: 5,
      price: 50000,
      image: new ImageLoader(),
      id: 'low_battery',
    }, {
      name: 'Mini Tank',
      rarity: 8,
      price: 20000,
      image: new ImageLoader(),
      id: 'mini_tank',
    }, {
      name: 'MLG Glasses',
      rarity: 10,
      price: 100000,
      image: new ImageLoader(),
      id: 'mlg_glasses',
    }, {
      name: 'Money Eyes',
      rarity: 2,
      price: 10000,
      image: new ImageLoader(),
      id: 'money_eyes',
    }, {
      name: 'No Mercy',
      rarity: 4,
      price: 2000,
      image: new ImageLoader(),
      id: 'no_mercy',
    }, {
      name: 'Peace',
      rarity: 4,
      price: 2000,
      image: new ImageLoader(),
      id: 'peace',
    }, {
      name: 'Police',
      rarity: 4,
      price: 2000,
      image: new ImageLoader(),
      id: 'police',
    }, {
      name: 'Question Mark',
      rarity: 4,
      price: 2000,
      image: new ImageLoader(),
      id: 'question_mark',
    }, {
      name: 'Rage',
      rarity: 4,
      price: 20000,
      image: new ImageLoader(),
      id: 'rage',
    }, {
      name: 'Small Scratch',
      rarity: 4,
      price: 20000,
      image: new ImageLoader(),
      id: 'small_scratch',
    }, {
      name: 'Speaker',
      rarity: 2,
      price: 2000,
      image: new ImageLoader(),
      id: 'speaker',
    }, {
      name: 'Swords',
      rarity: 4,
      price: 20000,
      image: new ImageLoader(),
      id: 'swords',
    }, {
      name: 'Tools',
      rarity: 4,
      price: 2000,
      image: new ImageLoader(),
      id: 'tools',
    }, {
      name: 'Top Hat',
      image: new ImageLoader(),
      id: 'top_hat',
    }, {
      name: 'Uno Reverse',
      image: new ImageLoader(),
      id: 'uno_reverse',
    }, {
      name: 'Victim',
      image: new ImageLoader(),
      id: 'victim',
    }];
    crates = {
      red: [{
        name: 'Apple',
        rarity: 'uncommon',
      }, {
        name: 'Rage',
        rarity: 'epic',
      }, {
        name: 'X',
        rarity: 'common',
      }, {
        name: 'Hax',
        rarity: 'rare',
      }, {
        name: 'Red Hoodie',
        rarity: 'common',
      }, {
        name: 'Devils Wings',
        rarity: 'common',
      }, {
        name: 'Devils Horns',
        rarity: 'common',
      }, {
        name: 'Exclaimation Point',
        rarity: 'common',
      }, ],
      orange: [{
        name: 'Pumpkin',
        rarity: 'uncommon',
      }, {
        name: 'Orange Hoodie',
        rarity: 'common',
      }, {
        name: 'Tools',
        rarity: 'rare',
      }, {
        name: 'Basketball',
        rarity: 'uncommon',
      }, ],
      yellow: [{
        name: 'Yellow Hoodie',
        rarity: 'common',
      }, {
        name: 'Banana',
        rarity: 'uncommon',
      }, {
        name: 'Halo',
        rarity: 'epic',
      }, {
        name: 'Uno Reverse',
        rarity: 'legendary',
      }, {
        name: 'Money Eyes',
        rarity: 'rare',
      }, {
        name: 'Dizzy',
        rarity: 'dizzy',
      }, ],
      green: [{
        name: 'Pickle',
        rarity: 'uncommon',
      }, {
        name: 'Checkmark',
        rarity: 'rare',
      }, {
        name: 'Green Hoodie',
        rarity: 'common',
      }, {
        name: 'Leaf',
        rarity: 'common',
      }, ],
      blue: [{
        name: 'Blue Hoodie',
        rarity: 'common',
      }, {
        name: 'Police',
        rarity: 'epic',
      }, {
        name: 'Blueberry',
        rarity: 'uncommon',
      }, {
        name: 'Sweat',
        rarity: 'rare',
      }, {
        name: 'Scared',
        rarity: 'rare',
      }, {
        name: 'Blue Tint',
        rarity: 'rare',
      }, ],
      purple: [{
        name: 'Purple Hoodie',
        rarity: 'common',
      }, {
        name: 'Eggplant',
        rarity: 'uncommon',
      }, {
        name: 'Purple Flower',
        rarity: 'common',
      }, {
        name: 'Purple Top Hat',
        rarity: 'rare',
      }, {
        name: 'Purple Grad Hat',
        rarity: 'rare',
      }, ],
      mark: [{
        name: 'Boost',
        rarity: 'common',
      }, {
        name: 'Peace',
        rarity: 'uncommon',
      }, {
        name: 'Question Mark',
        rarity: 'uncommon',
      }, {
        name: 'Cancelled',
        rarity: 'common',
      }, {
        name: 'Eyebrows',
        rarity: 'rare',
      }, {
        name: 'Small Scratch',
        rarity: 'uncommon',
      }, {
        name: 'Deep Scratch',
        rarity: 'epic',
      }, {
        name: 'Spirals',
        rarity: 'common',
      }, {
        name: 'Laff',
        rarity: 'common',
      }, {
        name: 'Ban',
        rarity: 'uncommon',
      }, {
        name: 'Back',
        rarity: 'epic',
      }, ],
      grey: [{
        name: 'Headphones',
        rarity: 'uncommon',
      }, {
        name: 'Helment',
        rarity: 'rare',
      }, {
        name: 'Speaker',
        rarity: 'common',
      }, {
        name: 'Spikes',
        rarity: 'common',
      }, {
        name: 'Controller',
        rarity: 'epic',
      }, {
        name: 'Bat Wings',
        rarity: 'common',
      }, {
        name: 'Terminator',
        rarity: 'legendary',
      }, {
        name: 'Knife',
        rarity: 'uncommon',
      }, ],
      christmas: [{
        name: 'Rudolph',
        rarity: 'rare',
      }, {
        name: 'Christmas Hat',
        rarity: 'legendary',
      }, {
        name: 'Christmas Tree',
        rarity: 'common',
      }, {
        name: 'Christmas Lights',
        rarity: 'epic',
      }, {
        name: 'Candy Cane',
        rarity: 'common',
      }, {
        name: 'Reindeer Hat',
        rarity: 'uncommon',
      }, ],
      halloween: [{
        name: 'Pumpkin Hat',
        rarity: 'uncommon',
      }, {
        name: 'Pumpkin Face',
        rarity: 'common',
      }, {
        name: 'Candy Corn',
        rarity: 'rare',
      }, {
        name: 'Cat Ears',
        rarity: 'uncommon',
      }, ],
      misc: [{
        name: 'MLG Glasses',
        rarity: 'mythic',
      }, {
        name: 'Cake',
        rarity: 'uncommon',
      }, {
        name: 'Cat Hat',
        rarity: 'uncommon',
      }, {
        name: 'Flag',
        rarity: 'rare',
      }, {
        name: 'Mini Tank',
        rarity: 'legendary',
      }, {
        name: 'No Mercy',
        rarity: 'epic',
      }, {
        name: 'Swords',
        rarity: 'rare',
      }, {
        name: 'Top Hat',
        rarity: 'common',
      }, {
        name: 'Victim',
        rarity: 'common',
      }, {
        name: 'First Aid',
        rarity: 'uncommon',
      }, {
        name: 'Pink/Purple Hoodie',
        rarity: 'common',
      }, {
        name: 'Paleontologist',
        rarity: 'legendary',
      }, {
        name: 'Bunny Ears',
        rarity: 'common',
      }, {
        name: 'Fisher Hat',
        rarity: 'uncommon',
      }, {
        name: 'Error',
        rarity: 'epic',
      }, ],
      pizza: [{
        name: 'Red Ghost',
        rarity: 'common',
      }, {
        name: 'Blue Ghost',
        rarity: 'common',
      }, {
        name: 'Pink Ghost',
        rarity: 'common',
      }, {
        name: 'Orange Ghost',
        rarity: 'common',
      }, {
        name: 'Yellow Pizza',
        rarity: 'legendary',
      }, ],
    }
    // add onloads for image loading percents
    var l = 0;
    while (l < cosmetics.length) {
      cosmetics[l].image.src = '/assets/images/tanks/cosmetics/' + cosmetics[l].id + '.png';
      l++;
    }
    wall_image.src = '/assets/images/tanks/blocks/block_blue.png';
    tank_ui = {
      values: ['tank_main_singleplayer', 'multiplayer', 'pvp', 'ffa', 'duels', 'main_menu', 'victory', 'defeat', 'cosmetics_left', 'cosmetics_right', 'cosmetics_middle', 'cosmetics_main', 'co_op'],
      co_op: {
        image: new ImageLoader(),
        path: 'co_op',
      },
      cosmetics_main: {
        image: new ImageLoader(),
        path: 'cosmetic_main',
      },
      cosmetics_middle: {
        image: new ImageLoader(),
        path: 'cosmetics_middle',
      },
      cosmetics_right: {
        image: new ImageLoader(),
        path: 'cosmetics_right',
      },
      cosmetics_left: {
        image: new ImageLoader(),
        path: 'cosmetics_left',
      },
      victory: {
        image: new ImageLoader(),
        path: 'victory',
      },
      defeat: {
        image: new ImageLoader(),
        path: 'defeat',
      },
      tank_main_singleplayer: {
        image: new ImageLoader(),
        path: 'tank_main_singleplayer',
      },
      multiplayer: {
        image: new ImageLoader(),
        path: 'multiplayer',
      },
      pvp: {
        image: new ImageLoader(),
        path: 'pvp',
      },
      ffa: {
        image: new ImageLoader(),
        path: 'ffa',
      },
      duels: {
        image: new ImageLoader(),
        path: 'duels',
      },
      main_menu: {
        image: new ImageLoader(),
        path: 'main_menu',
      },
    }
    var l = 0;
    while (l < tank_ui.values.length) {
      tank_ui[tank_ui.values[l]].image.src = '/assets/images/tanks/ui/' + tank_ui[tank_ui.values[l]].path + '.png';
      l++;
    }
    condensed_bullet.src = '/assets/images/tanks/condensed_bullet.png';
    shotgun_bullet.src = '/assets/images/tanks/shotgun_bullet.png';
    pause_menu.src = '/assets/images/tanks/pause_menu.png';
    stupid_tank_base.src = '/assets/images/tanks/stupid_tank_base.png';
    stupid_tank_base2.src = '/assets/images/tanks/stupid_tank_base2.png';
    stupid_tank_top.src = '/assets/images/tanks/stupid_tank_top.png';
    area_tank_base.src = '/assets/images/tanks/area_tank_base.png';
    area_tank_base2.src = '/assets/images/tanks/area_tank_base2.png';
    area_tank_top.src = '/assets/images/tanks/area_tank_top.png';
    condensed_tank_base.src = '/assets/images/tanks/condensed_tank_base.png';
    condensed_tank_base2.src = '/assets/images/tanks/condensed_tank_base2.png';
    condensed_tank_top.src = '/assets/images/tanks/condensed_tank_top.png';
    ai_turret_base.src = '/assets/images/tanks/ai_turret_base.png';
    megamissle.src = '/assets/images/tanks/megamissle.png';
    powermissle.src = '/assets/images/tanks/powermissle.png';
    dark_tank_base.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAA1klEQVRYR+2YUQrEIAxE16sJOYgn8yCBXK0LBcEKtVNGYVmmXy3EZPp8X0mf7sk5H/13RKT+e9f7bO4ZwMwuwcYg7r4lKDI3lVKm4VrYWuvSkOjcNwGX3nApBeqXxvu/OxURUEO0KOcMlaYnD1oXd4caokVmBpX+/hWjstZaoT9Gi+QgSuquTg6yBOUgS1AOsgTlIEtQDrIE5SBLUA6yBOUgS1AOsgTlIEtQDrIE5SBLUA6yBP/HwRcLzKUrYHTuOfRpBbd6P930QOZeqIwHdgUb/Z3N/QI4D9Svu5ywMgAAAABJRU5ErkJggg==';
    dark_tank_base2.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAA2ElEQVRYR+2YUQrEIAxE16sJOUhOloMIudouFAoqaGeJgpTpVwvRTJ/Pn6RP9ajqt/42s1R/73qf9b0C9AV9kF1Bkb4p59xQG1Fy96U00b7/BFx6wjlnaL/0hPnexcygDdEiVYVKzw8oIpCDpRToj9EiEYFK6eAIEx2EBJoU0cEoQToYJUgHowTpYJQgHYwSpINRgnQwSpAORgnSwSjB9ziIzkjcPQqtWQ+PPo4PyNnMQAz4khx/xMcHfJODS0fAKJir6dMxr55P3/cG6dtQ6RfsCtZf7FnfH3mg1J0r/kpqAAAAAElFTkSuQmCC';
    dark_tank_top.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAtCAYAAADcMyneAAABAElEQVRYR+3YwQ7CIBAE0O25B1PS///ChsYD55ptxKQqdChLgmY8F30ddkEYpPPP0LlPCKydob9LcKtN5DkeDgZ+UES22c0mvsUv+j3Qb0MPRdw4jibAEIKgSAS4J6e46TaZANf7KijyDHjAOedMgN57QZE5YBNcfEMUmQI2xZUgCcwVLTLNTJAJ6uJstf69p8karN1WmCATzPxp5F6s5cEmYZP8cpPo7DXtZKRBkLNpEySKQ4AfSdY2ho5HT3Qo8IC0AKJn4hLgC2kBRG8VSoE70gKI3stcAUYf1DwlzZB68bOrj9Q4AmMyTLBkzftWT0yQCSbWmf6XmYs7SnHNPwBtJsA9viK98gAAAABJRU5ErkJggg==';
    light_tank_base.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAA7UlEQVRYR+2Y0QkCMRBETWVCJNYgCEJswhpswoAgWIPBgJUpHATuAskNTAJ+zH0lsNkdZt/tx5rN7LPWfuf3lJKZ30edW3UnAc65hbBSSIxxiFCkrvHeN8VlsSGEriLRurDA2/XetcPnywnKZ8r+1169nx8oIRq0O2yhULPGQc7yeiQoIRq0P1oo9P9bjMIqBisNF4O1PwEeM2KwYqHmIDRlG0FikHVQDLIOikHWQTHIOigGWQfFIOugGGQdFIOsg2KQdVAMsg6KQdZBmEF0gdl7oY7WnfbOa+uP3vvp7D5Sd7EYLx+MElbi0ar7A2OA1K9QZe48AAAAAElFTkSuQmCC';
    light_tank_base2.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAA6UlEQVRYR+2YUQrDIBBE68kKBnuGQiBgL9Ez9BIVCoWcoVAhJ0shEDCCZmCVSph8Rdi4k+fzZ9UpeKy1c7h2zqlwXes913cJEBfEQWoFRfoqrfWGWoqS974oTbQvHPA7TkVPuLueof3UHuZ1l+fjBW2IFt3uA1TafkBjDOTg5+2hP0aLLr2GSulgChMdhATKFNFBKUE6KCVIB6UE6aCUIB2UEqSDUoJ0UEqQDkoJ0kEpweM4iM5I/jb6aD4gZzOJ2wRfkuaPuPmAh3Gw9JwaBbPMnfeOufR8er03SN/NYDz+oFaw+GLn+v4ApRHUnY0j2+8AAAAASUVORK5CYII=';
    light_tank_top.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAtCAYAAADcMyneAAABuUlEQVRYR+3YP07DMBgF8BcViblIzBk6ZWDIFRiYuEQHJDhAOAQ5AEgMvQQTA1fIwJCpQ+ZKdEYCBSU0LY1j+9lOolK5Y/PHv7w4/vIlwIH/ggP3wQNd79DRJVi6JrI5ng6G3hFAWT6JvPtlojQ/zFJhe3Bb/0WNTe20xUWnwmCL5aMSOJ/didvzT7BIBvibXIWLvoTB3t5flcDLi6sO4AlAInXAfdz5tzDYev2hBE6nZ+L21QTIOaQKqMVVI1sBqwNJpAzYC66JrjNFEnm8QN2tbU8827lolaApTnqriXloBLSFSdN0ASZJgiiKEIYh4jjuqcLtnybLMhRFgTzPkaZ1xRECkybogQB8gq4Tc/QEm7WOfdpHBbYXYgY5GlBWa3XIUYDSF4HNBFUhBwfqcM1DJEMOCmRxKuRgQFOcDDkI0BbXhXQBVucr2/XYFfcXyeCY3nSLnF/fAB1Nk1U1WU2weHlWvsU059V1dXWSqrbTCkh2dEyCzfg7pJWodRDZE5sAd0n2AGS/KpgCa2QPPqNxmTnYZaL6ZrY5V120B7LfYGQp+gR9gpK54Z9ik1r8P9dBy4pivGr8AM7yrD1RUkDvAAAAAElFTkSuQmCC';
    ai_base.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAuElEQVRYR+2YQQ7DMAgE68eRR5fHtWpaS7UVwkYmt8nNkpVdxmCQ22P8XtO6Teu7lqHubsDM5g2DEXe/xaii+xE+NffntNqkpNuyKLrBp3vp8W5m0v9kglK4kuR3k3ocEIygQvBCuh1uhSAEfwS4B6NUoJOsFgkEIdivGebBIBfoxatFAkEIMs0kOUAvXi0SCEKQaSbJAXrxapGUE6x+p1Yn+R5I9oCqBnwVbKo7CM9RVVOL3J/pvgGoAo+TFbDWigAAAABJRU5ErkJggg==';
    tank_top_png.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAtCAYAAADcMyneAAAA90lEQVRYR+3YUQrCMBAE0OkhhOKJPLInEsFDVLY2QqtJZ5sNRBm/G32d7CYmAzr/DJ37IGDtDP1dglNtIst4Ohj6QQDTNUh3eX0P9dvUQwl3CgI+ALBIBjgnZ7gxCHgHwCL3gCvcOQh4A8AiS8AmuPSOLDIHbIrzIAUslS0zzUpQCdriHLX+bdNUDdZuLEpQCRb+NGovtvJQk6hJfrlJbPaadjLTIMzZtAmSxTHAjyRrG8PGsyc6FrhCRgDZM7EH+EZGANlbBS9wRkYA2XuZI8Dko5rH0wy5F9+7+siNEzAlowQ9a963elKCSjCzzvS/zBzcUdw1/wSJGLAu8rB/lgAAAABJRU5ErkJggg==';
    tank_base_png.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAABGElEQVRYR+1Yuw6DMAx0NiT+gMXdGNI5/f+xmZu9Gdo/QGKjCi0SpUpiRBAdjgWQLvhy8QsrImqI6ElExMxDuE+X917N3/d6TtkNBBpmfqSM70V0SWzJIdhVWusv1WJEnXNF1ZTaVcYYEcGrtUVP+GKM6Hv/TzDnB9M2796LdiwFnZhFUNW27dD3fRZ8BMGqqmg84q7rsgRvzmUxawBnrbPwuq7fBLNIIkKQRFRSCJKIMpIoRpCkgg+VRJKaUhiJD4b1qCQxFVFJtvqgOIpRSVBJPgqg3UK7tTLvSEod2i20W3MFjvgvRruV8kG0WyvT3g8c7dZWBYtXkiNGwON0KyiRm3BZa4vOpyf1JXa/DC8X7EVs6R4puy8xsfCO7UNcsAAAAABJRU5ErkJggg==';
    ai_top.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAtCAYAAADcMyneAAAA90lEQVRYR+3YUQrCMBAE0OkhhOKJPLInEsFDVLY2QqtJZ5sNRBm/G32d7CYmAzr/DJ37IGDtDP1dglNtIst4Ohj6QQDTNUh3eX0P9dvUQwl3CgI+ALBIBjgnZ7gxCHgHwCL3gCvcOQh4A8AiS8AmuPSOLDIHbIrzIAUslS0zzUpQCdriHLX+bdNUDdZuLEpQCRb+NGovtvJQk6hJfrlJbPaadjLTIMzZtAmSxTHAjyRrG8PGsyc6FrhCRgDZM7EH+EZGANlbBS9wRkYA2XuZI8Dko5rH0wy5F9+7+siNEzAlowQ9a963elKCSjCzzvS/zBzcUdw1/wSJGLAu8rB/lgAAAABJRU5ErkJggg==';
    start_screen.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAgAElEQVR4Xu3cP6ht1Z0H8COK70oyEhhIMxAGbAQLu4c2TpPGysYUNq+PAyFC0CCmEESwshrLFK+xGBsrIZ02ilWmeEQDgghTjDAgM2N8U4jDvnfO5fjePfes3z6/vdc6a31uk8zkd9b6rc9vn/09//SBG7+98cPGHwECBAgQIHDSAg8I9JOen+YJECBAoEOBFx58avPu95+ETibQQ1yKCRAgQIDAcgLbIE8P9DkLLndMKxMgQIAAgX4Fpsx95Majmz/+7U+bNx57ZfPaF2+dH7Y0i698h1764H5ZnYwAAQIECKwrMGXvk4/9cvP7v76RE+jTgtPf9Nn9nFcI6x7fbgQIECBAoA+BKXOnv+md+e2nb29ufXzrPIc//ez9zU/OfnbwO/Ur36HvBnkfTE5BgAABAgTaFtiG9/PPvHrZ6Fdf3zn/73e+/DAe6LthvvsKYXrF4KP4ti8G3REgQIDAaQtMuXvv37fffbP59Z9/c/Bge3/lvrvoex+9ubn5+HOXX9AfXFUBAQIECBAgEBKY3jQ/e/PF+x4zvUvf/kDuugWvDPSrXiGULhjqXjEBAgQIECBw/l359LH6t3e/2ex+5P7Bp+8c/Kh9y3dfoG8/Vt8N9emLeX8ECBAgQIDAMgLbr7u3P4z7xc+fOP9RXOSr7h8F+r0/htt+hx5ZcJmjWpUAAQIECPQvsA30s4d/uvndX/5w/s59+pv9kft2gYcfOtu8/Pnr/Qs6IQECBAgQaEhg+0b66EDfnmnOgg15aIUAAQIECAwj4N/lPsyoHZQAAQIEehYQ6D1P19kIECBAYBgBgT7MqB2UAAECBHoWEOg9T9fZCBAgQGAYAYE+zKgdlAABAgR6FhDoPU/X2QgQIEBgGAGBPsyoHZQAAQIEehYQ6D1P19kIECBAYBgBgT7MqB2UAAECBHoWEOg9T9fZCBAgQGAYAYE+zKgdlAABAgR6FhDoPU/X2QgQIEBgGAGBPsyoHZQAAQIEehYQ6D1P19kIECBAYBgBgT7MqB2UAAECBHoWEOg9T9fZCBAgQGAYAYG+8qjvvn03Zcezl86uXGff+vvq9zWz9Dr79s3qM4pca9+oQ9b1U2vfWs7R50ut6yfqE+2z1vUT7VP9PAGBPs9t9qOynlDRG1T0RiHQrx9x1hxrBWutfbOuw+gTMPp8yVp/6RfM0T5rXbfRPtXPExDo89xmPyrrCRW9QWXdSLPWiQZK9MYYHdDS58rqJ+v6ifpn7VvLOfp8yZpX9LqN+kT7zJpj9PqJ9ql+noBAn+c2+1FZT6joDSp6o/AO3Tv0XYGlr9to8EWfgNHnS9b60XNFn6fRPrPmKNCj8uvUC/R1nC93yXpCRW9Q0RuFQBfoAr385lDr+VXe4UVl1v1HoEfl16kX6Os4C/RC56wbY+F2l2W19o3eGGvdkLP2reUcfQFc6/qJ+kT7zJpj9LqN9ql+noBAn+eW/qjW3hFH+4nWZ30Ueer7Ri+krPPW2jer/1NfJ6v/WnOM7qt+HQGBvo7zwV2ynuDRV+DRdy5Z9QL94CVxZUHWdRLdPWtf61zIZznUmmN0X/XrCAj0dZwP7pL1BBfoF9TRjy6j/tH6gxdAYcGp75vV/6mvk9V/4WVzWVZr32if6ucJCPR5bumPynqiCXSBnn5xJr6jXPo6X/qFXGufLEVnneUf3Vf9OgICfR3ng7tkPdEEukA/eLHNKFj6+jyVIBboMy4eD1lNQKCvRn39RkvfMPftnvWd+NL9Z/Xphjzvgq8136x5tbZOlmd0mrX2jfapfp6AQJ/nlv6orCdadJ1a7+izAKP9R1/YRIMgeq6sd6a19s3qv7V1TsUz2met50u0T/XzBAT6PLf0R0WDOBo00Xe40eDL6j8KW+sGZd+LSbUWxFn9RK/DrH2j60T7rHXdRvtUP09AoM9zS39UViBG14k+waMvDHq9QUXdoi+Qoi/YohdkdC7R6yraf2v9nIpntM9a1220T/XzBAT6PLf0R9W6YUaf4AL9YvRRN4F+vZtAn/eJR/RGVOu6jfapfp6AQJ/nlv4ogT6PtNYNyr7zAqjWdR79xCB6NdZ6QRLts9Z1G+1T/TwBgT7PLf1RtW500Se4d+jeoe9e/LWCbOnnS/QJXssh2mf0+Z71yVK0T/XzBAT6PLf0Ry19g1r6hlOr/+i+0froO7uoc/RCyuq/1r5Z/Z/6Oln915pjdF/16wgI9HWcD+6S9QSvtc6p7Furz4MXQGFBVv+F212WZe1rnes/4en1BWH0elM/T0Cgz3NLf9Sp3+hq9R/dN1rvHXpuALXmX6ufrH2jN6Ja+0b7VD9PQKDPc0t/VNYTrdY6p7JvrT6zLpis/qP9ZO1rndwXSLXmGN1X/ToCAn0d54O7nPqNrlb/0X2j9d6h5wZQa/61+sna9+CN5Z6CWvtG+1Q/T0Cgz3NLf1TWE63WOqeyb60+sy6YrP6j/WTta53cF0i15hjdV/06AgJ9HefLXfbd0KJtLP2Pjy1949133uiPgpbuM+ocnWPUIev6qbVv1nyjzkvPsda5as0xum90XurnCQj0eW6zH5V1Q47eoLJuOFnrZN0QBPrsS/HKB0avq+juS18/0etq6efjvn6y9q113ui+0etE/TwBgT7Pbfajsp7I0Rtv1o00a52sG4JAn30pCvQO/hW+0edR1v0num/uVWq1fQICfeVrI+sJJdAvBifQcy/g6HUV3X3pF4TRoFn6+egdevQKUX+MgEA/Rm/GY5e+gdQKuKwbV9YNf+l1suZYK4Bq7Zs1l+hTr9cXKrXmGN03Oi/18wQE+jy32Y/KCoLoDSrrRpq1TtYNodYLmKw5Rh1Ofd+lr59anrXOVeu80X1n3zA9MCQg0ENcigkQIECAQJsCAr3NueiKAAECBAiEBAR6iEsxAQIECBBoU0CgtzkXXREgQIAAgZCAQA9xKSZAgAABAm0KCPQ256IrAgQIECAQEhDoIS7FBAgQIECgTQGB3uZcdEWAAAECBEICAj3EpZgAAQIECLQpINDbnIuuCBAgQIBASECgh7gUEyBAgACBNgUEeptz0RUBAgQIEAgJCPQQl2ICBAgQINCmgEBvcy66IkCAAAECIQGBHuJSTIAAAQIE2hQQ6G3ORVcECBAgQCAkINBDXIoJECBAgECbAgK9zbnoigABAgQIhAQEeohLMQECBAgQaFNAoLc5F10RIECAAIGQgEAPcSkmQIAAAQJtCgj0NueiKwIECBAgEBIQ6CEuxQQIECBAoE0Bgd7mXHRFgAABAgRCAgI9xKWYAAECBAi0KSDQ25yLrggQIECAQEhAoIe4FBMgQIAAgTYFBHqbc9EVAQIECBAICQj0EJdiAgQIECDQpoBAb3MuuiJAgAABAiEBgR7iUkyAAAECBNoUEOhtzkVXhQJ3375bWJlbdvbSWWjBaJ9Z6y+9zr5zLb3vPvyo8751ov2HLgbFBBYSEOgLwVp2HYGsG3i02+gNP9pn1vpLryPQo1eOegLLCQj05WytvIJANCizWsoKyqx3iLWCtda+3qFnXcnW6UlAoPc0zQHPItAvhl4rWGvtK9AHfLI78kEBgX6QSEHLAgJdoO9en1nXQ/QTmJafI3obR0CgjzPrLk+adQOP4kRv+NE+s9Zfeh3v0KNXjnoCywkI9OVsrdygQFYART/yrRWs+/rc10/UZ+n6pZ0bvES1RGC2gECfTeeBpygQDaDoGbPWj66T9QlA1r5ZLxgEevQKVD+ygEAfefoDnj0aWFGirPWj6wj0i0lFPwmJzlc9gZYFBHrL09FbukA0KKMNZK0fXUegC/Totaq+PwGB3t9MnegagWhQRjGz1o+uI9AFevRaVd+fgEDvb6ZOJNAPXgNZ33FnvfCIflQe3fcgiAICHQgI9A6G6AjlAksHQfSdcnnn178DjZ4r2mf0BcC+c0WDO+qTda7ovuoJtCAg0FuYgh5WE4gGX7SxaKBE148Ga7Q+GsTR8wr06MTVEygXEOjlVio7EBDoF0PMCuKsdbIurdb6yTqXdQiUCAj0EiU13QgIdIG+ezEv/YlBN08cBzkJAYF+EmPSZJaAQBfoAj3r2WSd1gQEemsT0c+iAgJdoAv0RZ9iFq8oINAr4tt6fYFagR79aDfaZ7R+n3x0naXrl75Cov0v3Y/1CRwjINCP0fPYkxNY+gaetX50nWi9QL/+k4roC7CTeyJouEsBgd7lWB0qK7CikqcSrFk+0fNG66P+0frW+on2r57AroBAdz0MJbD0DTxr/eg60XqB7h36UE/8QQ4r0AcZtGOucwM/lWAV6OtcD553BNYUEOhraturukBW4GYFYtY6WeeKrrN0/dIXTLT/pfuxPoFjBAT6MXoee3ICS9/As9aPrhOt7/WFRPSC9OO3qJj6lgUEesvT0Vu6QFbwZQVi1jpZ54quE61f+rzRC0agR8XUtywg0Fuejt7SBbICKCuYstbJOld0nWj90ueNXjACPSqmvmUBgd7ydPSWLpAVQFnBlLVO1rmi60Trlz5v9IIR6FEx9S0LCPSWp6O3dIGsAMoKpqx1ss4VXSdav/R5oxeMQI+KqW9ZQKC3PB29pQtkBVBWMGWtk3Wu6DrR+qXPG71gBHpUTH3LAgK95enojQABAgQIFAoI9EIoZQQIECBAoGUBgd7ydPRGgAABAgQKBQR6IZQyAgQIECDQsoBAb3k6eiNAgAABAoUCAr0QShkBAgQIEGhZQKC3PB29ESBAgACBQgGBXgiljAABAgQItCwg0Fuejt4IECBAgEChgEAvhFJGgAABAgRaFhDoLU9HbwQIECBAoFBAoBdCKSNAgAABAi0LFAf6Cw8+tXn3+09aPoveCBAgQIBAVwKR7D0Y6NvFIot2pekwBAgQIEBgZYE5mXttoE8LPnLj0c0f//anzRuPvbJ57Yu3zo80Z6OVLWxHgAABAgROUmDK2Olv+lQ8kr0HA/3Jx365+f1f3wgtepKCmiZAgAABAo0I7AZ5aUvXBvq04PQ3vTO//fTtza2Pb50H+6efvb/5ydnPfKdeqqyOAAECBAgUCuyG+W72Tll83SfkBwN9Cu/nn3n1so2vvr5z/t/vfPmhQC8cjjICBAgQIBAVmMJ8+/feR29ubj7+3OVX31etdfBHcbsLbhf49rtvNr/+82+ivaknQIAAAQIECgSuyt7pDfX2t2zhQJ/e2j9788X7Hndo0YJelRAgQIAAAQJXCGw/Vt8N9ekr70N/e9+hT5/hTx+rf3v3mx995P7Bp+/4qP2Qqv+dAAECBAjMELj3x3Db79BL/umyawN9emu//WHcL37+xPmP4koWnXEGDyFAgAABAgT+X2CbvQ8/dLZ5+fPXi1wOfoe+XfTs4Z9ufveXP1wG/HWf4xftrIgAAQIECBC4VmD7Jnr3nzrb94CDgb59YGRR8yFAgAABAgTWFSgO9HXbshsBAgQIECAQERDoES21BAgQIECgUQGB3uhgtEWAAAECBCICAj2ipZYAAQIECDQqINAbHYy2CBAgQIBAROCBSLFaAgQIECBAoE0Bgd7mXHRFgAABAgRCAgI9xKWYAAECBAi0KSDQ25yLrggQIECAQEhAoIe4FBMgQIAAgTYFBHqbc9EVAQIECBAICQj0EJdiAgQIECDQpoBAb3MuuiJAgAABAiEBgR7iUkyAAAECBNoUEOhtzkVXBAgQIEAgJCDQQ1yKCRAgQIBAmwICvc256IoAAQIECIQEBHqISzEBAgQIEGhTQKC3ORddESBAgACBkIBAD3EpJkCAAAECbQoI9DbnoisCBAgQIBASEOghLsUECBAgQKBNAYHe5lx0RYAAAQIEQgICPcSlmAABAgQItCkg0Nuci64IECBAgEBIQKCHuBQTIECAAIE2BQR6m3PRFQECBAgQCAkI9BCXYgIECBAg0KaAQG9zLroiQIAAAQIhAYEe4lJMgAABAgTaFBDobc5FVwQIECBAICQg0ENcigkQIECAQJsCAr3NueiKAAECBAiEBAR6iEsxAQIECBBoU0CgtzkXXREgQIAAgZCAQA9xKSZAgAABAm0KCPQ256IrAgQIECAQEhDoIS7FBAgQIECgTQGB3uZcdEWAAAECBEICAj3EpZgAAQIECLQpINDbnIuuCBAgQIBASECgh7gUEyBAgACBNgUEeptz0RUBAgQIEAgJCPQQl2ICBAgQINCmgEBvcy66IkCAAAECIQGBHuJSTIAAAQIE2hQQ6G3ORVcECBAgQCAkINBDXIoJECBAgECbAgK9zbnoigABAgQIhAQEeohLMQECBAgQaFNAoLc5F10RIECAAIGQgEAPcSkmQIAAAQJtCgj0NueiKwIECBAgEBIQ6CEuxQQIECBAoE0Bgd7mXHRFgAABAgRCAgI9xKWYAAECBAi0KSDQ25yLrggQIECAQEhAoIe4FBMgQIAAgTYFBHqbc9EVAQIECBAICQj0EJdiAgQIECDQpoBAb3MuuiJAgAABAiEBgR7iUkyAAAECBNoUEOhtzkVXBAgQIEAgJCDQQ1yKCRAgQIBAmwICvc256IoAAQIECIQEBHqISzEBAgQIEGhTQKC3ORddESBAgACBkIBAD3EpJkCAAAECbQoI9DbnoisCBAgQIBASEOghLsUECBAgQKBNAYHe5lx0RYAAAQIEQgICPcSlmAABAgQItCkg0Nuci64IECBAgEBIQKCHuBQTIECAAIE2BQR6m3PRFQECBAgQCAkI9BCXYgIECBAg0KaAQG9zLroiQIAAAQIhAYEe4lJMgAABAgTaFBDobc5FVwQIECBAICQg0ENcigkQIECAQJsCAr3NueiKAAECBAiEBAR6iEsxAQIECBBoU0CgtzkXXREgQIAAgZCAQA9xKSZAgAABAm0KCPQ256IrAgQIECAQEhDoIS7FBAgQIECgTQGB3uZcdEWAAAECBEICAj3EpZgAAQIECLQpINDbnIuuCBAgQIBASECgh7gUEyBAgACBNgUEeptz0RUBAgQIEAgJCPQQl2ICBAgQINCmgEBvcy66IkCAAAECIQGBHuJSTIAAAQIE2hQQ6G3ORVcECBAgQCAkINBDXIoJECBAgECbAgK9zbnoigABAgQIhAQEeohLMQECBAgQaFNAoLc5F10RIECAAIGQgEAPcSkmQIAAAQJtCgj0NueiKwIECBAgEBIQ6CEuxQQIECBAoE0Bgd7mXHRFgAABAgRCAgI9xKWYAAECBAi0KSDQ25yLrggQIECAQEhAoIe4FBMgQIAAgTYFBHqbc9EVAQIECBAICQj0EJdiAgQIECDQpoBAb3MuuiJAgAABAiEBgR7iUkyAAAECBNoUEOhtzkVXBAgQIEAgJCDQQ1yKCRAgQIBAmwICvc256IoAAQIECIQEBHqISzEBAgQIEGhTQKC3ORddESBAgACBkIBAD3EpJkCAAAECbQoI9DbnoisCBAgQIBASEOghLsUECBAgQKBNAYHe5lx0ReCkBX71z0/+8K//8m/uLyc9Rc2fmoAn3KlNTL8ETkBAoJ/AkLTYnYBA726kDkSgDQGh3sYcdDGOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOgEAfZ9ZOSoAAAQIdCwj0jofraAQIECAwjoBAH2fWTkqAAAECHQsI9I6H62gECBAgMI6AQB9n1k5KgAABAh0LCPSOh+toBAgQIDCOwAM3fnvjh3GO66QECBAgQKBPAYHe51ydigABAgQGExDogw3ccQkQIECgTwGB3udcnYoAAQIEOhB44cGnNu9+/0nRSa4N9Gmh6a90saIdFREgQIAAAQLXCkSCfLvQlYEuyF1pBAgQIECgjsBuBr/x2Cub175467yRQyF/X6BPD3jkxqOb7/73vzZP/OM/nS8yLSbk6wzWrgQIECAwnsBukJee/sp36NNC2yAvXUgdAQIECBAgcLzAbpjffvr25tbHtzbb/99179LvC/RtmH/62fub55959byzr76+c/6fd7788Pw/fad+/MCsQIAAAQIErhOYwnz7995Hb25uPv7c5cfvVz1u74/idhfaDfXtZ/nGQIAAAQIECCwjcG8Gb3P4ugy+MtCvWqhksWWOZVUCBAgQIDCOwPZj9d0snj52P/R35Y/ipgc9e/PFHz22ZLFDm/nfCRAgQIAAgf0C9/4Ybvsd+qFfuE8r/ijQd78/n/7H7XfoH3z6zvnuvjt3GRIgQIAAgeUFtnn88ENnm5c/f71ow72/cv/7v/uHzYMPPrT55n/+o3ixoh0VESBAgAABAkUC23fmJf/0WdG/KW7659GngP/P//73a39hV9SdIgIECBAgQCBdwL/LPZ3UggQIECBAYH0Bgb6+uR0JECBAgEC6gEBPJ7UgAQIECBBYX0Cgr29uRwIECBAgkC4g0NNJLUiAAAECBNYXEOjrm9uRAAECBAikCwj0dFILEiBAgACB9QUE+vrmdiRAgAABAukCAj2d1IIECBAgQGB9AYG+vrkdCRAgQIBAuoBATye1IAECBAgQWF9AoK9vbkcCBAgQIJAuINDTSS1IgAABAgTWFxDo65vbkQABAgQIpAsI9HRSCxIgQIAAgfUFBPr65nYkQIAAAQLpAgI9ndSCBAgQIEBgfQGBvr65HQkQIECAQLqAQE8ntSABAgQIEFhfQKCvb25HAgQIECCQLiDQ00ktSIAAAQIE1hcQ6Oub25EAAQIECKQLCPR0UgsSIECAAIH1BQT6+uZ2JECAAAEC6QICPZ3UggQIECBAYH0Bgb6+uR0JECBAgEC6gEBPJ7UgAQIECBBYX0Cgr29uRwIECBAgkC4g0NNJLUiAAAECBNYXEOjrm9uRAAECBAikCwj0dFILEiBAgACB9QUE+vrmdiRAgAABAukCAj2d1IIECBAgQGB9AYG+vrkdCRAgQIBAuoBATye1IAECBAgQWF9AoK9vbkcCBAgQIJAuINDTSS1IgAABAgTWFxDo65vbkQABAgQIpAsI9HRSCxIgQIAAgfUFBPr65nYkQIAAAQLpAgI9ndSCBAgQIEBgfQGBvr65HQkQIECAQLqAQE8ntSABAgQIEFhfQKCvb25HAgQIECCQLiDQ00ktSIAAAQIE1hcQ6Oub25EAAQIECKQLCPR0UgsSIECAAIH1BQT6+uZ2JECAAAEC6QICPZ3UggQIECBAYH0Bgb6+uR0JECBAgEC6gEBPJ7UgAQIECBBYX0Cgr29uRwIECBAgkC4g0NNJLUiAAAECBNYXEOjrm9uRAAECBAikCwj0dFILEiBAgACB9QUE+vrmdiRAgAABAukCAj2d1IIECBAgQGB9AYG+vrkdCRAgQIBAuoBATye1IAECBAgQWF9AoK9vbkcCBAgQIJAuINDTSftb8O7bdy8PdfbS2cEDltTv1uwuuG/9ffVZjy1ZZ7cmq58sz31DifZZUr9vr+jsovUlMyq59lqzipqXXDMHn6QKuhQQ6F2ONfdQ0ZtkSb1Av5hRyc25xLO1kCp58SPQL5QEeu79auTVBPrI0y88ezRQSuoFukAX6AK98BakrFBAoBdCjVxWEr7RmpKb+W5N9EVC9LHR+a7ZT8le+94Rl3wCUHL2JXrYt2bJXsc8donzHuNfct6SntUQEOiugYMC0bDeXTAarMfcqI957EGEewpKbsJZ/ZTsdUyglJx9iR6O8TnmsUuc9xj/qG1J/2rGFBDoY849dGqBfj9XyU04K3RK9jomUHY5lBMAAAWrSURBVEouhiV6OMbnmMcucd5j/KO2Jf2rGVNAoI8599CpozfPkvqSBpZ4d79v3+hH0/te5Oz7dGLfvlnrRNc/5rwljy05V8knOdF5ZYXjMetEH1tiVWJe8pxS07eAQO97vimnKwnokvAtuXGV3OSP+f49GhDRoBToFwJZs47OKxqmJfONhmm0hxKraA8pT3yLnJyAQD+5ka3fsEC/3zzrJpy1TkkwRV9s7NYvEVIlL94E+oWAQF//vneKOwr0U5zayj0fczMveedeEhwlPURfeBzDeEw/JeG79A28pP+SuRxzlui8SnouqSmZ+zHrRB8brS/pX82YAgJ9zLmHTh294URv1CXBUdLDMfuGQO75SDn6FcAxIRjtM2uvEv+SOZbUHDPHaJ9ZPiXnWmKvrOvBOn0ICPQ+5rjoKaI3ySVuyCU9HLNvFPCYflq4sZf0v3RIRedV0nNJTcmsj1kn+thofUn/asYUEOhjzj106ugNJ3qjLgmO3TX3NV/yTrnk4CUfdx/Tz9KB3kJvJddMretk13/fV0LRa6zkGi6Ze9b1WbKOmv4EBHp/M00/UcnNueSGVrJOyU0+erMtCbh9N/ljbsIlLwxK3KIDLTnv0r1lzbrkRVo0lAV69IpSfyoCAv1UJlWxz5Kbc0kwlawj0C8ko4G7zz/64qfkBUxJb1mzFuj3T6TEv+LtwtYVBQR6RXxbEyBAgACBLAGBniVpHQIECBAgUFFAoFfEtzUBAgQIEMgSEOhZktYhQIAAAQIVBQR6RXxbEyBAgACBLAGBniVpHQIECBAgUFFAoFfEtzUBAgQIEMgSEOhZktYhQIAAAQIVBQR6RXxbEyBAgACBLAGBniVpHQIECBAgUFFAoFfEtzUBAgQIEMgSEOhZktYhQIAAAQIVBQR6RXxbEyBAgACBLAGBniVpHQIECBAgUFFAoFfEtzUBAgQIEMgSEOhZktYhQIAAAQIVBQR6RXxbEyBAgACBLAGBniVpHQIECBAgUFFAoFfEtzUBAgQIEMgSEOhZktYhQIAAAQIVBQR6RXxbEyBAgACBLAGBniVpHQIECBAgUFFAoFfEtzUBAgQIEMgSEOhZktYhQIAAAQIVBQR6RXxbEyBAgACBLAGBniVpHQIECBAgUFFAoFfEtzUBAgQIEMgSEOhZktYhQIAAAQIVBQR6RXxbEyBAgACBLAGBniVpHQIECBAgUFFAoFfEtzUBAgQIEMgSEOhZktYhQIAAAQIVBQR6RXxbEyBAgACBLAGBniVpHQIECBAgUFFAoFfEtzUBAgQIEMgSEOhZktYhQIAAAQIVBQR6RXxbEyBAgACBLAGBniVpHQIECBAgUFFAoFfEtzUBAgQIEMgSEOhZktYhQIAAAQIVBQR6RXxbEyBAgACBLAGBniVpHQIECBAgUFFAoFfEtzUBAgQIEMgSEOhZktYhQIAAAQIVBQR6RXxbEyBAgACBLAGBniVpHQIECBAgUFHgYKC/8OBTm3e//6Rii7YmQIAAAQIEJoHrMnlvoAtyFw8BAgQIEGhDoCSTrwz06YHT3/TO/I3HXtm89sVb5/93yYJtHF0XBAgQIECgD4HSTN77Dn03yPsgcQoCBAgQIHCaAiWZfGWg7z7w9tO3N7c+vnX5Tt279NO8GHRNgAABAqcpUJrJ1/4obgrz7d97H725ufn4c5cfv58mi64JECBAgMBpChzK5L2BvvvA7dG/+vqOQD/N60DXBAgQIHDCAiWZvPdHcdMP4nYXmD5290eAAAECBAisK7D9qvtQJt8X6Pd+8b79Dt135+sO0G4ECBAgQCCSydf+yn2ifPihs83Ln79OlQABAgQIEKgkMAX7oUwu/jfFbRfb/jPplc5kWwIECBAgMKzA9tPyqzL5YKAPq+bgBAgQIEDghAQE+gkNS6sECBAgQGCfgEB3bRAgQIAAgQ4E/g9X69tDIKSdLwAAAABJRU5ErkJggg==';
    tank_base2.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAABBklEQVRYR+1YMQ7DIAyELVueQDf6Avr/sbygbOUJ2bK1Im0kEgl8VYLocJlAOuLT2cY2WmWftfaV70MIOt+3WtfsLgSccxtieyLe+yZEEbtaAq1kzyaJ2tXGmKp6K8FnjKd6+GIM9D+d/D/PswjuQXAYBrW4eJomkeAjBBHzC+BqrQgfx/FDUEQqpe7eIzAYc3MOwv4/QSZJwZFIFjNJalnAJIHuiAoIicF0nJWkJCIrydEYhLOYlYSV5KsA261CKHAmKV1HSKlju8V2K1egx1zMdqsWg0i7tSjIwb0gI9utow0rK8lRBeEYRLO42xNwUkKaS2KMTR7REbsbw/sDrYjtw6Nm9w3oNO+CP3uxtwAAAABJRU5ErkJggg==';
    red_bullet.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAJElEQVQYV2NkgID/UBpEMTLCBFYzMDCEQmXAgjABGI1TJYaZAPKaCQPPgktjAAAAAElFTkSuQmCC';
    coins.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAACAklEQVRoQ+2ZTU7DQAyFkxV3QWLHnjNwUs7Avjuk3oVVq0F1cdOx/fyTkIawQsp05M/vje1MxmEjf+NGOIYdZG1K7opIipw+n06WWuPbd3kCSza8Cf751eIYhuPhuqYKKgVyBUCCl/AuUFmgEEgJwBQsCeQG+YHIKGAZ73gYIuq4QGAIdgbu4kaSEICBQUwIHvzLu5z3r4/fZxqUEwYCUSEIQAtewiIoCcgBY4KYEBGAKVgDSsLEQZoSloX48xastb4HA6qigohqWBAt49PALRD6TRDGD4JCaGXWqwygiggSVoNXpZ61pOcELp0XA8YHgqrRABBrSXbrwSwKQtlGQbSyPD0rEZCUrchOiCIekLZWgelaqwvisVW0anEwp73qQVowWWtJpXgxRSgAa8Kl54VluFaRqTW0zm41yD+1FgpiQazCWryxSYrsIPLbI35GLnVcnWCrrBUYU2pHFG4ZrSFa1nIe9Ja/ehCrj/AxxtPZIyNK2z81pvT6hDX1zjX9ijDIqCI1RuS1OGAr1VopVaKzVuCQk5D/451dVcVbjrX5a4lbFAiGKhU6LPKD3f5PXgWZZ4THtYmbRgIyYWjhmu9+3TBem7EkzH4bfwOjeTsCsfT3kbtzkwVKAsB9BEnuw39D7EE+9FddRLW515gjytwBVO2/g1RlsmqfXZGqTFbtcwaz8MRC/tSRIgAAAABJRU5ErkJggg==';
    button.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABkCAYAAAA8AQ3AAAAD4klEQVR4Xu3a223qABREUVwQfdCO26Ihl0NEpER5ACFjO+PIK993fMwC7a87jON4OfgjQIDAPxAYBOsffEtekQCBwzRNB8HyQyBAYPMC11hd/wRr81+VFySwb4G3WAnWvn8HPj2BzQt8jJVgbf7r8oIE9ivwNVaCtd/fgk9OYNMCt2IlWJv+yrwcgX0K3IuVYO3z9+BTE9iswKNYCdZmvzYvRmB/Aj/F6lfBOh6P+xP0iQkQ+BOB8/n81J2n/x+WYD3l6R8RIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAICFaAZkKAQEdAsDrurhIgEAgIVoBmQoBAR0CwOu6uEiAQCAhWgGZCgEBHQLA67q4SIBAILBqsaZqCVzAhQIDAsgLDOI6XR48Uq2XBPY0AgVzgYbDEKoe1JEBgeYG7wRKr5bE9kQCBeQI3gyVW81CtCRBYR+BbsMRqHWhPJUBgvsCnYInVfFBPIEBgPYH3YInVesieTIDAMgKvwRKrZTA9hQCBdQWG0+n08P9hrXve0wkQIPC8wAstHt7rfzpGygAAAABJRU5ErkJggg==';
    gameplay1.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAgAElEQVR4Xu29D9CmRXUneiY6iCOgguiggCI6q8gIjERAMDdMgiyZLNx1d0RwNbV6N4m47qUqu1xJOeTKWAtqaouNV2BjyK1dVyMXcZObfEECMTG3/LOokEgwRDAaIyFxYFBRhAGcW+f93v6+fvvr7tP9dPfzdPf3e6umBuZ5up8+fU6f3/nTp3vDjh079pPlt23bNrr99tttj2b/5nr+0pe+lC688EJ6+tOfvtL2iSeeoJtuuok++9nP0lve8hY67rjjFvq977776IMf/ODCvw39vjng1H7Gbr+0tERvf/vbnfPeyoNrrrmmCzq+9a1vWad8bLlIkevbbruN9uzZQzt37qTLLrusFRFaGOd73vMe+sQnPkGHH344vfrVr15DQ0v88DEAdCzPztjz0Ive3cCAbk5eyv8zoL/5zW+mAw44YEVun3zySfrDP/xDJ6A/9NBD9L73vW/l/aHfH9pOfbiG9tdffz197Wtfax4M//iP/7gLOhjQa5AL3biOHc/DDz9MRx11FH30ox+l888/n9797nc3BeqXX3453XDDDfSmN72J2Dh54xvfuOBsxM5HLe/XMo5U/dcDHb3o3Rmg57SIXB76nXfeSTxpLg+dvfchEYEUzyVnZMK1KGLHd88998yatA7qDOg90KE89LE9hli58Y2PZemTn/wkXXnllc2B+nvf+96Z3mAwf9e73kWvf/3racuWLaN7cDn5offVslz1REcvencF0HOBOnsDrpD7j370Izr++OOdIfdU4c4FqqnjSGmvBKt1MFSA3jodesg9ha+51tcQo1cBOo/hiiuuoI997GNNeOoKzFmfXHrppbPlrQB9yvnMpWdy9TO1XPZARy96dwHQcyySc845Z5bjcuXQ3/nOd9ILXvCCBYNXz6GnCmfrwqULVstgqAN6y3SYOfRU+ZyivQ7oOqjrQGmLVk35bzYwNwE9h76agh+2eZ16HKnfh96dcrWsftuaQ9+1cylodLtv2DF7TxcG5NBXNxIOyS1xeNH8tRh+NwG9VVDvJYf+oQ99aEGsXIAZtPALv6SiCDaDgz105NCXGTBEv6TsxXB9r5ZxuIyKkPH1onc37Nq1a78ZxlOAfsov2Ffu//yvy//OgG5OVq5d7rEWX6qFWUt700NX89AaqNsAvUVQf85znpO0t6MGufr4xz8+y6GbvxpB3QfmPP53vOMddPDBBztNihrmOyQt0so4XROtxt8LHb3o3TUhd2YgAzqDOQO3Cerq3/jvpa/vWqPscgJ6qrDEGgWS8I7x3CVYrYGhC9Bbo8NVtmbzkEz5SJXfXO3NkLs+TnPTWWHn29t9SH5fz6HXOt9D9USL+srH0FzyO3Q+Y77fi971AjpPpPLG1aQqgOd/VyF3fcJzAXoMM3oRKqbDJ1gtgaEP0Fuio4c6dB+gMy/0sjDeST7FL3QHvgvQU/XF1O0B5otSNzY/etG7zhy6DtzKE1eeuwJ65NAXD98JydVIOSxbLsdUsKygp/yFHHwjAboC9drp6DWHbs67AnVfOLskr7g65g1veAPt2rXL+xnk0NP26Ej6Z8jzHHpvyHddRtCQ8fSid6116Dbg5knacezulRC86aGrSbR56JIi4F3uqENfniXJUlRAyLW4Y/+UIZET0Guno6c6dElezjzzTHrwwQfpoIMOkl7N+vyxxx6jE088ka677jqxX70Offfu3Qvv79ixvEmXfyU9PD5VTP/p3w39fuh7JenIMU+90NGL3rXWoZuArguvL+TOwmerQ5dWqSpbSxXeHoSrVsFSm/JCj3QN9dDHBvRYOnqrQ/etxbPPPnv2+Oabb5aWbNbnV1999ay/iy66SOyXAf1FL3qR9z11SmWqPjHb79u3L+i7oXoo9L3cdJhEpPbfAx296F1rHbryxKVNcbYcuq0OXVqlqENfnaEaBUvfYd8yoA+ho8c6dNd6bAHQt27dKqmT2XPlNaeClWpveuWuQZjeuvT9HsBQnwuJ3lqf96J3vXXoIWVrZtjGVocurUCc5b48QyzsobmcsTxbs1yuVUAfSsd6yaGz/LUA6Oeddx4de+yxkkqhP7rlFvGdmBded9ZZQa9zdCA0hxv6ngv0a2lfyzhS5qkXveutQ5ckOLQOXerHdttarOVaq+UXS0dNlqKt9r1FQE+ho+c6dHNdtgDooR669QpJg+AfENHfERHfp2f7+zeJ6DVExNdMbZCU2Pw5b+pDHfrqZLWil3vRu9aytVgQcsn61MxskY5aBMt1kE1rgJ5KR+916PrabQHQYzx0H1gziDOgP5eIDtf+6P//CSI6j4h2EtEFER76UH3Yor7y2TlT6/+Y7/eid52AHjMZNqZO3b7VxVGDYPlOpWsJ0HPQsR7q0NVaaQHQQz30nyCiQx1AzQDOwP0sIuL3njL/W/9v/jcG/I/O+7gy0EO37Xg305K9gGBPdPSid7Pfh24T3rFyLGN9JyVXI9VbTp3LkY6YbQXQc9GBHHogkiW8FrPLXfLQOX239Wtfo7c88QRt9IC1Cd4M4OqPevYYET1ARJyN//1DD6X9xxxDhx7KZoL7hxx6PWfMx+jpXvRu9vvQh4abUj36Vj1yc75yWYpDD5+RaszHBvSp6VhPdeg9eOinEhGfdXeU5n2bQK2Dt++/Hyei7xDRHiL6cyLiKvkvCIZL6O76XvRVL3T0onez34cesiGkFOj3IFy5BStmN7zk1fL8TgXoU9GBOvQE1zuwaQ4P/Yc//CHtu/tu+qW9e+nVRHSY5nH7QFsa4hNzQGdQ/wYR/c5Tn0pfeMELyCWPqgYeYfa1M5vqtJVs34vezX4feslJXw+LJKdgsbfNABwKhrUC+pR0oA5dgrz05zGA7sqhcyD8fyeinyOiFxDR5oid6RIFvGP+74noH4noH7jG3dMAdeiLx2GbUzU1Pri+34veRQ799rrORs6VyzEPUQkB9ZoBXUUHxqYDOXQJ7tKfxwC6mUP/8Y9/TN/59rfp1X/91/Q2Inr+HNCfmj6shR4YyPkPe+oXEtHLX/taOvDAA9d8BTn09Z1Dn1rvWuvQdSmt1aIyw+utjFNKN+S2FNX3Qjz12gE9FNRz0oE69MzIaOkuBtBtHvp2Ino3ER05//P0AkNmIFeA/m+J6K8c30Ad+uLEtKKXe9G7qEMvfJFDSJpAF/pSgmWCIYOe6e3mBMJcZ7m7DoVRYy9NB+rQC6Cj0WUMoJseOl/q9BuPPEKvJKLnzT303N45D3fvHNB5k9yvEdFDW7fS5s0c2F/86Tn02HCz6aRIxn+tz1ukoxe9izr0bXXlfEoKlgJ1/tsWum4F0MekA3XodQG6zUO/goh+WgP0pxUY8vfmgM559A8SER86Y/uhDn15VlrxzBUPe9G7yKGvgxy6rng49A5AD9+tjxx6AXRM8NB/8id/ctb6sUcfnZ3wxvefvZWI/sUc0I8gok0FhsyHzNw/L2Pjw2b+4Kij6GUve5nVQw89DyP0PZfHW0v7WsaRMk8l9i5NoXdRh24syakty5KWol5yZsupt+Khj0kH6tALoGMCoP/K1q305vnpbXwAzEeIiHeh/6IG6AcXGPKP5oDOHvqniOhywUOX9EiLYWnftEr01v68F72LOnSLlE4pfCUFyyTVBPVWAH1MOlCHXgAdEwD9P2/dOgNvVVv+u0T0eSK6dH6cK3vofKRr7h9HAthDZ0C/jYh+9eCD6dRT+RibxR/q0Ferhsy5mVKv8lh83+9F76IO3bHypxK+MQWLSddBvVVAL0kH6tBzQ+Pa/mI2xf3W1q30Kxqg/xkR8Tnrvz732hnQ+VCZ3L8n54DOO93/hojOFzz0UA889L2p9FHo+ELfq5WOXvQucujrLIdu00MK1FsGdB3Uc9KBHHpuaEwD9Ou3bp3tMmcPna805fKxs4notzVA54tXSvzYQ1ela+cS0enbt9NTnsIjWf2hDh116NLx2UpaSuhd1KHPZ7cWy3FsS1EXLv5vSRinOPpVGpOuUNWmP6lNKB2oQy8BjYt9xnjon9i6lf5PrTlvVnsOEf2/c0DnQjL20kv8uGSNQZ1r0nkjHl/Pav5Qh744I7XoVZc8qPH1ondRh15ZiUVOwRqi1HIBYc469CnpQB36kNmPa5MC6PwlBvSriejF8yNfGdAX/ea48bjeflAD9H/PL23bRocdthjgRx366uy1AuY84l70LurQO6xDDwk5D1VxoZ5tDkCvgQ7UoQ+VlPB2qYC+be4x86UsfLgMA/oB4Z8PfvO7Wuka177fbGmJOvTlSWkJzHMBeg36Cjn0znLoJYWKBX8sQK+FDuTQg/Fu8IupgH4eEZ1IRP9UA/QSx78+rO10/00i+tyLX0zHHnvsAt3Ioa/PHHot+gp16IYamtqyTAn9lBaqsQC9JjpQhz4Yp4MbDgX0HxMR7z7nW9b49yatdO2g4K+Hv/iIBug3EtF/8njokh5RTaX3an/eCx296F3UoVsW5ZSLaKhgjQGCYwB6bXSgDj0c8Ia+GQPoH9m6lTh/zUCu/vDZh/cS0TvnHjpvjHvm0MF42j2mXaP6p0T0gUMPpVe96lVrPPRQkAt9b0p9FBI+74GOXvQu6tAdC3iqRTREsMYCwdKAXiMdqEMvgIxGlzGAfs3WrfRLRKS8c/6bT267lYjeM/fQGdD5fvTcvyc0QP8KEf1vHg89FORC35tKH4WOL/S9WunoRe8ih954Dn1MECwJ6LXSgRx6blhc218MoP/6/OhXBnIOgXMJ2ZeIiA+Y+b+00jU+5z33j49//fb8z3/8iZ+g727bRs9+9rPXeOihZ5uHvucCy1ra1zKOlHmKPcu9Vn2FOvS5FNRiOcZYimMLVSlAr5kO1KHnhsU0QD9n61Y6cA7kfAOa+nHp2sc0QF97sWkaHeyd3zf30K+bnyHPx8GaP9ShL85ILXrVxf0hdeg16yvUoVdWYhEK6CygUs14mgqzt869y712OtZbHfoDDzxAJ5xwQgnRcfZ533330emnn06XXXaZ+N2f/dmfpRe96EV04IEHkl7zfeutt9If7N9PDOTqXnQ+SS7Hjy9/4RPiGND/kK9OPfJIOuwlL6GNGzeu6R516KtT0gqY84h70buoQ2+0Dj2HohraR4ghEVqHPnQMOdqF0LFe6tCvu+46uuqqq+i4446jTZtKXEDq59gdd9xBV1xxBZ1zzjneF233oasGfIUq38R2zNxT57D7UwMERd9g5/pvPlSGL2Xh+nM+y931Qx368sy0BOaxgB4gUkVeCdFXyKE3mEMvIi0RnYaEnEIAPeKTRV4NoWM95NAVmF988cX0tre9rchcS53edNNNdOmll4qgft55562p/VZ9s6e/+ZvfpAseeWRWl86Azue6sx8dAtrqHbXhTv9/Pub1NzZtou+9/OV06KHuLXeoQ++3Dl2S4dLPQ/QV6tANLkxtWYaEfkoLTkj/knC1AOhMp0RH73XoNYC5krcQUPd56KofPgL2F4jorPmxsE+bA7oJ1AzY+m558//Vs8fnl79w7bn0Ux66pEdUP9J7tT/vhY5e9C7q0C0rdMpF1IpgSWDYCqBLdPRch14TmIeCus9DV3388Ic/pJ/63OdmO+D5sBk+ZMYF1gzafKQrX7zC76s/+v/vJaKjXvhC2rJli4TnC3l9SY/0AoY90NGL3kUdumOJSoux1POWBMsHhi0Buo+OXuvQawTzEFAP8dC5nweI6AYi+j0i+pcOoGbwZuBmwD+aQdvzd+gGOzOHLumJHsBQV6ESvbU+70XvIoeOHLrodUgv2MLWrQG6C9R7zKHXDOYSqId46NzHH91yC3FZ2eeI6BcFsA45JvZ1Z3EAX/4hh44cuiwled6w6V3Uoc/nthbLsTVLUYmmKVwtAroN1HurQ28BzH2gHuqhc6lZzl+oh4469MVZr0WvumQhpg49pzzl6svUu6hDr6zEolVAN8GwVUA36eipDr0lMHeBeqiHbqsHTwGXfftsx8isVcuoQ1+dk5T55l7GbN+L3kUdeoN16LmsuxL9KIuxZUDXQb2XOnTO7XKd+ZSlaUPlTd/9/uEPf1g8BMRWC54DHHbv3u0lwVWDHgJOquMc47xdSyOaAy7df6t0tAzour5CDh059KF61tmOQT1kR3D2D2fukOlgJd36WdX3338/sZJvEcx1T/1d73rX7HS2U045hZimJ5/kveurv6c85Smz/zniiCNmz9Uv9/+b3z3yyCOjvrd//37iVM5ZZ50144sLBGuXO67H/9SnPkUvf/nLo+jPzY/U/pgfjz766CQHKuVUWayv4KFX5qG7GFzasi7df6uWew/8ePDBB4kPXeHjUs2fqQxrfs7Rkscee4ze9KY3EYN7i78rr7ySPvrRj9Lhhx9Or371q9eQUHod5uz/tttuoz179oAfE0ZETH7CQ6/QQq7dMg8dX+h7tXsooGOZQ1PPw8MPP0xHHXXUDBDf/OY30yWXXNIUpr///e+nj3zkIzMAZDB84xvf2LSHDn5sq45/OCnOUAk5LVibtpH6hye7OGvSfJV+Dn7Uww8OKX7yk58k5eW2BOo6mHN04fWvf/0sLVVafkv2D36s1fAl59tmVJsjwElxFtSdmikAkXpARB/J1HKR+v3W5UoBCNPBF7l87GMfa8JTV2B+4YUXzs6r558C9BAlncr3Uu3BD3uAqNR8h6xfnBTnCNpNyRSASH2Wb8hiakE5t0yHDiCtgLoNzE1Ab0FubPoQ/Fjd0GhqrKnwAzl05NCL5Uanzrnm+n6uflxgOlb/Y32nFJ2cs/3Qhz60oDvf+9730vXXX1+lp+4CcwXoPeTQwY+6qhRwUtxcPUxlUZnKr5ZxOAIXwTk/0LE8g7XMQy3jSJGrj3/847McuvmrEdR9YM7jf8c73kEHH3ywazqqkRsfv8CP1dmpZX3hpLiKlK5zdVdmdAxVyi2He20017KI1ws/zBCvTndNoK7A/Pzzz6d3v/vdVvboOfRawrWx6xP8qMto59GgDh116KN4krHKYihIlQZZ0LHImdLzrffvAxAe1eWXX0433HDDpOH3EDBXIXfb4UtjzmeqkQp+1BOBU7xEDh05dOTQHREQpVxbzz33Qocth26CkgJ1XzhbioSlPOcxvuENbyC+pMX3Yw+9xxw6+DFtTh116IYETm0hwwOczgP0KeCp5SL1+1PI1dLS0sqUqnPOU+iQPEL1sTPPPJP4dLyDDgq5GDUFvhfb8il2J554IvElONKvpzp0iVbwY3WGUuSfe5Haow7dIo3SpJV+PoXyTQ2/AQzlxVZabqT+x5QrdZHJDUS0c/7hVFAPBfSzzz579sWbb75Zwpqsz6+++upZfxdddJHYb2916D6CwY/xnBTUoTskUVKOpZ+PqXxL3s4EOsZbzCEW/Fj8YO9cgbn6W7+NbMj66RXQQ/g2ZL50ySvRHvxAHfpMxkrlJEv161KCpb5Xql/QMezsZfBjWXJi5kGF23UPnfPKKbeLheTQeZyteITrIYcOfoybU0cd+hzlSliwQyzkWsbhCqGFji/0vdTvlG4POuxgbs67bZ70HLoJ5iHtzfXjqns2+2oB0HuuQwc/1mqlsfQI6tADNhqMxQwp8Tb1OFK/P1a4N3WcUnvQMU0aoecQ7xDjv7QxK/UPfgw3dkvxG3XoqEO3hlOHeFC+XDxAcBoQ7MlIXA8AIhmTNT0HP+rbCIs6dNShR+VGY3KpMTnXmH5dxkGp75XqF3TE7WlADj1uvkrLLfhRFz+Y36hDN1yYqS1geLLwZG1edapc9iBXPXqEqXydsj34MV2u3BV5Qx26ZWamXCSlcislQEKapx5ABPyoR2n1CCC2yFTudJe0Toc+Bz/ssDp0PnPoS9ShO0ydKZkCEKkHRFpRrqHKIPS9qeXf9v1eAaRVUAc/UIc+0yelcjul+kWuc1iuCPxYlpxa5qGWcQxdT8jZDluHQ+dbkhfwoy5+zHLou3bt2l/ypLCxLP2xvuPKXeT6fq5+So9T6h902MF8Ko+/B36gDn1VemrgJ/hRFz8Yx1GHjjr0FaksrSRaDvdOsQcB/Fg0inoO8baYZgM/6jLaeTSoQ0cdujUsnNuTBJgvzmhpsJb6b5Ef6wFAJL7V9Bz8QB160ZyilPNp5Xkr45Ryc6ADOfSUs9tN+UHOtq6cLfhRFz9mOfQdO3bsb3WXJcKg9e2yzO3ZT+2RtOjJuvY3tL7Oe/QIp5bvlO+DH2tXWsp85lifqEO3aL+pmQIQQXi6hLHaulz1CCA5lPhU+gr8sJvOU/HDmkOfcjA5hLt1pQUPt75F0uKGJZ+XnmOdTaEnegUQ8EOS1mHPr7766lnDiy66SOzg9a9/PW3ZsmXlvSnkO4eewVnuOMu9WJ00cujIoSOHbscSBhDchy7ibNILsYDeOj9Qh66JSy0WWS3jcK2k0PGFvpf6ndLtQYfdKJkikoS659VZr0EuwY+6+IE69Dk/algcuHq0vsUhuQe1y400vtbSUz2H3HOEW8fmJ/hRj7Gr8AN16KhDn0mlpPxTn4+tbEp57qBjcWZT5SKm/XoAkJj5sMn4mO3Bj/J6M5afyKEjh44cuiNSoxYT9gLUsRcAdc911T2DH3XxA3XoFhM31iLKnUuEBzidB+gLsU8tF6nf70GuevQIU/k6ZXvwY63GmJIfPBrUoQPUnTiWKpw9gEiLuc1ec/89AsgY6a7UdexqD37YV1qp+Q7Rp7gP3aH9pmQKQKQ+yzdkMbWgnFumo1cAaUFubPoQ/KjvpE7k0JFDRw4dOfTZDNS+VwA527pytuBHXfyY5dBL3Ie+b9++BRfrgAMOKBbW1TcufeELX3B+d2qPO/T7oe+5JrSW9rWMI3WeQId9Q1zuvSMh84y659VZD5kvXylsjvbgR138yF6HvrS05E3f7dixY+F5DqFiIqTv7tq1i0oLd8n+Ww6T2gQiF99TwXpoe/BjcebG4mdMiPeBBx6gE044QdpOkPX5fffdR6effjpddtllYr/mUaMtptnAj3qM3ex16KZX7pJo5a3nUgKx353Cs0gBe4DHNOAhaeRc8jvUqEj9fotyFQIg1113HV111VV03HHH0aZNmyQ2Zn9+xx130BVXXEHnnHOOt28XoKfydcz24EfHdei7d+8OWhyzu1ojfl8kok8T0Z1E9N8t7TYE9sXRgdpzhHr6IOcZ2FPRPdV3XWA1dDxD2+UeR2p/rdMh5WwVmF988cX0tre9LVAz5H3tpptuoksvvVQE9fVwljv4MX6OPdt96KGe8h/dcot3Bd07B3AGcf7zDCI6kYg4ePb/EBEHs04noqPmvbzurLOCVmTuyEApz6pFz8nHgDE9Bts4Ur8PftQTIfF5hDWAh5qpEFBXHnqqfE7ZHvxYq3Gm5AePJlsdupTHVqSbHvpeA8AfmAO4AvEjiGgjEfG2uu8T0e8R0dPm7/wCExAE50R6/n7qSZe+DxCpB0T0kUh8q/1563LlApCawDwU1PWQe+1y4xof+GEHnyn5ma0OPcZDV943/82VfOx9M4Dzn5fMAVyBOAO5+sPGwHeJ6K/n7Tgc/+RrXkPPeAb78f4fe+g//vGP6cknn6SNGzcWP7s8lamtK1+TG6nzMXV78GN6I8sGIDWCeQio93D/NvjRcR16aA79QCJ6sQbiDOYKvE0QZ0+cwVz9zf/9IBE9NAf2bxLRlUT0N0T0AwHQn/Oc59CDDz5IL37xi+llL3vZ7O1ac4q1jssFaq7xgo7lGatlHmoZR6wcqffNHHrNYC6Beo85dPBj/Jy5udcqWx26z0P/x7/5Gzr6619f8cKfZQFxHbT5v/X/N7H6YSL63hzYOWT/60T0tS1b6IUvfCE98sgjtHfv3pU/Rz7++EoOng2G9x1yCL32ta9dA/9Te4CuDXGterq1zKfLzgsdX+h7qd8p3b4HOvS65xbAwwfq73jHO+jggw92uiEt8Av8WGVfLfxyXp9q8yx84OLLob+SiH5DA3E9jK48cN0bD8mL/5CI/oGI/pGIbiKi984H9xwjB7/ZMB5+mjfVve51s7B7K2HUVsYpCTXomD5sbUMQiW+1PFch3pbA3AXqPdWhgx+LnvmUTli2+9BdHvoXv/hFuvq736XjiYirQhWY6174U4Rwue3xE0R0/xzQ/5aIzieiDwo5eP72L3Kbk06i5z//+dZw6JTM8E1DLUp1qCcJMAeYpxoTDOi8uZXrzKcsTRugrmZN9N3vH/7wh2nLli3VRgpD1jn4sQ7r0P81Ef2beZnZs+eh9KcOXRFGO/bO+c8eIvp3RHQREb3GMBr0HDwbEVcT0QeOPHJ2ilStOcVax+UCZeTQp8+dhZxb0Lpc3X///bMTH1sEc9NTP/LII+ktb3nLwgmWrfEH/Khv3Rfz0B999FHa/9Wv0i8/+CBtI6IjicidMRqG8LzjncPu3yGia4noUCL61bnR4MrB/yURvfrpT6ft27fDQ9emPXcEgFMwb3/724cxtqJW3/rWt6yjyT1fY0eGWoyY8KZWPl71wAN5a+3i74gjjiAGGNevpucczeTxHHbYYU176ODHOvLQecmxd/7WOZhzLjv370fzsDsD+p8R0SeJ6PMBtekv5ND89u2zc5dDPJtYzzT1/dYsddt4r7/+euKQXOugzoDeAz9YzkHH8sqsZR5qGQf01bKn3QM/ip0U9/VPf5qufvJJOpaIGEBXt6Dlg/UntTw6H0hzHhH9/dxT932FDY0vHX/8bFf82J6R5Nm16DnZ5vqee+6Z/XProK48dIlvtT/vRa5Ax+Jqm1ruwI+6+FHspLhfI6Kziei5RMTeuXz0yzCg5/w5h93570uJ6D8Q0b8UuvoIv/e859HJJ59sfROLxO7JxBg/CtBbB3U95D61XKR+H8q3LuULfoAfNgBKWedFTor75je/Sa+45x66WAN03hBX4sfHwao8OgM175jnjW++H++Kf9mBB9LP/MzPOF9LmVTuNLV964tdB/SWQd3Moafyder2rctVjFLR8C4AACAASURBVFGZYx2W5hf4AVDPCeozQM+ROzBPinsREX2YiA4noufNvfQSgP6oVo/OB/H9ZyK62/gQh+Yf0/7sI6J/SkSnvfWt9I//yPvk7R5pjnkZkqOf6rs554Fz6OavxfA7cuirx1vWIpe1jCN1vYAO6N0h+OCTu2InxX3mM5+h39q3j/iQVQXoXEKW+8fnu6t6dM6jXzDfGMe3sSkQZwDn/+a/1Z//RERffulLV2pBS1viof2Hvueax1ramx66Gm9roM5HBqfcZ18LP2oZR6rcgg47CE4VuQA/6uJH0ZPifoWI/vk87M63ph2UG83n/fH57iqPzifGvY6IdhoAzkD++PzfON9+IxH96WGH0amnnpocHi8t1C2G5VyA3lr43VW21kI4V5LLFuXKp0Ikemt/Dn4g/J4afi9Wh/7tb3+bnv9Xf0XvnofdeWPc2qrLPAjPF7Owl87la/+DiO4kosvn4M0gzsfE/gUR/fn877/duHFWA3r44YfTeeedR3fccYdzIFMrgVYXuQ/QWwJ11KHXc6wlwDx9b05pfdaqvkqNHNXSvlgOnQlkAP8dIuLz1Tnszl56yDntsTDP3rc6153z5+8kovfMAVyB+KGHHUZbt24lPtTh2c9e3qJXaw6r1nG5FqttvLYcuslXDr9P+QupkUcOHTn0UuuxVL8x6zRHDhd02MPuU8xLsTp0JvFzn/scfeCHP5ydFKfy6GvPeMqj0hWgczidj5r9zqZNMw/80EMPnQH405/+dCuIm18vbcFK/fdi4UoeuvLSbedZ55EIdy/KkAgFdITXV+dSkt/Sz3tZH6AD4fXU8LqtfbE6dPUxvgzlzVr52iGFtDXfka7K164iot81vsOXOrSyiFoZp0951wroalPeNddcE3SKHerQ1y7Y0qAt9d/D+tBnVaK39ufgRz3GSZE6dEUel4Qd+JWv0BVaHp3L2Er81HWqnEe/mYh+84gj6Pjj+Y635d8BByzuscciWZ6XUvNQI6DrO+yHAHrJ+RpLKY71nVJyZeqO0t8p3T/4UQ8Y9mBkFc2h8wTxJSm/Z+TRh1yXKhkBvPmND4z5OhH90vy/9TbsoU+R0xiSo2plnC5lxOMPzaGPFXI3y+ViAL0HfvRyVjXowJ6GUuuxVL8+PTkEH3z9FatDVx+97bbb6Fe/9z36Ka18bTmbnffH4fZvE9FvE9Ftr3gFHTq/71x9hcvTWqgnLu0RjNV/TR66rfY9FNBRh142khProY4lvy7tlOv7ufopPU6pf9BR1/ooWoeuhIEPe/llLY/+rLxYTg/PwZxtV65DN0+K48/pOfTWwnaxSk9ahGM8rwXQXQfZhAI66tDLpWVyyXWufqYGJ9CB8LtNN8fIZbE6dDWwvXv30o++/GX6De0YWN7xnuP3xPwEON4Q93dE9H9v3Eif27KFnm945/wtM4feyuJpZZym0NUA6L5T6VIBPWaRpS7SEu1blasxjNES8y3JC/gBMM8hd8Vz6GqQn5hfo6rq0Z86ANH5THZ1jKt+lOsjRPSXRHShp0/k0MfNvU2dQ5eOmI0B9B5ya8g9jyv/IblRyJU9XD3VvEz1XZcxN2Q8RevQ1UD5JLZffuABOkerRw+5TpXPaVfArZ/Lrp/Jro50/Xd8tOxrXkPPeIa9Z+WhS5by1M97sdRzeehDD5+RasxjAJ15MrVcpH6/F7kCHfBkc3iyvUZ6itehq4n7eSL690R0KBHxOW3PJKKDLbOqwNoEcAXs6jx2/vu72mlwf0pEfASs64c69LUzkwoSvva5AT1mN7zknfNMxAI6QH1VfkrKTcg8A9QB6gD11YiTPhdF69DVh370ox/R9++/n+jrX6d3ERFfrcob4xjY+XhYHcRtN6PpIK6fyf437JU/5zmz0+Ce+9znrpwGZ2M26tDtpk4p5ZwT0NnbZgAOBfVSgB4CNqXmMxeI5epnajpBB0AdoL4W1EfLoavJfy0R/SSHMInon8yBnc93t4XRGcjvNc5k53bbtT+hZ8Mjhz5uDjFXDt08DCYE1HMD+pBcVkgO1QVKpb5Xql/QsXiBTeg8h7439vzGfg90LM9YDfNQvA7dtKL+6JZb6L/OQZrD6OcRER8Hq3LhfGua8sL5YhW+2EUHcA7Z67/XnXWWK8q+8O+oQ7cLnTl5uTyv3B66GmeIp54T0FGHPq7cuBazkstc8il9p/Rz0AG50mUslzyMUoeuD5w3uvGPy8w+O7/m9A0aiPMRrjqAv0SA6xgPvfQiLdW/y2IuBcY56CgF6Dw2HdQZvE2vPSegow69/g2BLa4Pn1rLpdxzrGNbH9L4wI/FWZPmK+fz4nXopkCwh27+/hURbZ0DOYfjY36hHjrq0Me917okoCtQ579tIfgxAD3nIhyiNFO/D6U7ndIFmMNILLX+Rs+h585lLy0tBeF/7u/yR0vlTEr16xKiEt8rkUPXGc1e+liAXmJ+psixg45lCaplHmoZR6peAB31yFW2OnRm6he+8AUvuLq8ZNsiMzvyeST79nEG3v3Tv5vq2ZRuX8pyi5nPVH5w+5Ieul5yZsupl/DQS/O9dP+9yBXoQGShRESrF7nKVoeug4DpNasa8NJKy/XdscEslc4ehKskoJv8NEG9BKDnMHJS5SK1fQ9ypfM+dT6mbg9+wDjJbZxkq0OvRThrGUeqsmidjjEBnefK3CiX+6S41vnRmlErrR/wA2CYGwx7MBaz5dCnyAmaixq5nOUZqWEeSufQbYtZgXpuD72G+cyxvkBHPesDZ+uPey5GyPrpYX1kq0O3Kdgxw5SSRd/K81bGKfF7bA9djUdtlsvloaMO3Q6CU3n8vawP0AG5KhERyFaHXiL8kSr0CMtNF5bLCegu48H377kAHXXoKDEaS4+M9Z1UvSq1Bx3T6d1sdehTWeySspeEr/bnrS6OHIAeEjqX+O96PuRylhIWtRTpKPW8VbkqNR9T6wHwYzoQ9OmQqeUi9vvIod+OXE6J3FFqDr0kmPMCjgH0EvMTktNzKfmh4xnaLvc4UvsDHfZw9VTzMtV3U+XIbN8DHVnr0HUlNbbHDgu3Lgs3xUMvDeaxgM7vx1rKY8u/ND6sj7rWB/gBftgiA9I6lp4XqUOfKiyGRVLPIhkK6GOA+RBAB6ivypakVEo/xzqvZ53rIynN99L99yBXqEN3WB+lhUfqv3XhGgLoY4H5UEAHqAPUc0deWl/nuedD0ouln7fOD+TQkUMvUrcem0MfE8xjAb2H3BrqnrFXppQcl+rXBa6lvleq3zHpyFaHHnpJiiscX/rfd+3aRVPm+EMty9D3pkprhI4vxkMfG8xjAB116MuSFsr30nJZyzhS6QQdkKsS6YpsdegK0K/ae1VpbI7q/+JDL569r86TtzWufXG1GAYKBXSmTaoZj2J44Msxu9xTlXet7VuUKx97a1/H0vjAj0XuSvNV+nmL/MhWh94qoJcWitT+WxQqHnMMoAdicPbXQgwJ18EyqXydun2rclWrcZTKT/ADYJ7D2cyWQ9+9e/dsPDV76K3kSFoZp0sJ8fhDcujZETqyw5BQPwN6D/xADh059FJyXKpfn34pcY5DD3Rkq0OvPYc+1hWusNSXl2GIhx6Jv0Vel0BdeeipfJ26PTxAeIA5PEBESLZVvRcLdegWCYXyXZ6UlHloBdCZTh+o6yH3lPlInc8c7QHqAHWA+mqkpkfjBHXoDq5OrbxbV74tAboP1M0c+tRykfr91uXKXK6p8zF1e/ADRlZOIytbDr1ETiM2h9JDDqSXXGcLOXRzIdk8deTQkXsupVdK9RurN1PfBx32iOYU85KtDn3q8MXUlnau7+fqZ2p+tOahq/kyQR116OnplxzpArUuelkfoANypevoXPKQrQ49Z9ggVxgqVz+5JnsoyLZIR6uAbobfcR962l6KnGDuWj8trg8fLVPrm9Tvgx/TpRGy1aHXltuCUE0nVPzllgFdB3XUode9qxfrfNp1PtRJSTUaSrdvVa6QQ8dZ7kXqrFvModty6lzuOEUurMSeFNBRT66zl70yoKOuPSYbiGg/7dolRbPqf84H24COevgEftTDCx4J+AF+lJiBjuQq5OTIElOYs88NtHPnfrrhhvbB8O67CXTkFI3EvsCPxAnM3Bz8yDyhid2BH4kTmLf5luuv955Hkfdr5XpbBnT+tQ7qvEBARzlJie0Z/IidsbLvgx9l5ze2d/AjdsaKvs+Azj/p5Miig8jQ+Sqgtw6GaoGAjgxikaEL8CPDJGbsAvzIOJkZugI/Mkxivi4UoLcO6ouA3jIY6gsEdOST9KE9gR9DZ65MO/CjzLwO7RX8GDpzRdrpgN4yqK8F9FbB0FwgoKOI4Ad3Cn4ET9UoL4Ifo0xz8EfAj+CpGuNFE9BbBXU7oLcIhrYFAjrGWAv2b4Af08297cvgB/hRYgY6kSsboLcI6m5Abw0MXYIFOkosY7lP8EOeozHfAD/GnG35W+CHPEcjvuEC9NZA3Q/oLYGhb4GAjhGXxvxT4Mf4c+77IvgBfpSYgU7kygfoLYG6DOgKDEsIQ2ifIQfGSIIFOkJnW34P/FicIy75nPIHfoAfJeRvHcmVBOgK1EtMc2ifIQffhAP6zp2h3833nlKUOQULdAznD/hhnzueF8gV5Arro9n1EQroW7ZsGS7nA1tybTz/2gZ0ddBN6NGCoR762IoXdLjFeAogBD/Aj4GK1dsMctW0XNUK6Oqgm2uuuaZhQNdPrWsZ0EGHX3WODejgB/hREsy5b+irtTPcwDqvEdD1U+vaBXTzCNpWFwjokFXnmAsd/AA/5BmIfwNyJc9ZA+u8NkA3j6BtE9Bt58m3COigg6imFAj4AX7IsBP/BuSqG7mqCdBt58m3B+iuy2FaA3TQsawYawF08AP8iIdquQXkqiu5qgXQXZfDtAXovpveWgJ00LGqCGsAdPAD/JChOf4NyFV3clUDoPtuemsH0H2Lo6VNJqBjUTFODejgB/gRD9VyC8hVl3I1NaBL17aOD+gs6EN+Uo352B466PBzEfwYtosXcgW50mcgl8ELucoiV7kAXdWMx0KhVGM+HaDH1HlL1u4UHrpaIKDDLpNTATr4AX7k3JuBdZ4FCLPtlZmYH7kBPeYAGsk7Z0ZNA+jsbbPCD1W+tQI66HAv9ikAHfwAP9QM5PRsIVeQq/kM5AR09rYZgENBvW5AV151CKjXDOigox6PUKVlQo1FyNVa3uUGQqwPrI/cEZMJ13luQFdedQio1w/ooYu9dsULOtYqrak8dDWSEFCHXI0D6FgfWB8lDMUJ5KoEoIeCehuAbjLFdlpQC4oXdCwqrakBHfwAP/zZX6KQk8lch8KoyCL0FZG0YbmUh24z3gvzoxSgm6DO4G167e0AulK+/LctBN8KoIOOVRVaA6CDH+CHD9SHAjrkat3KVUlAV6DOf9tC8AB022IuFfrRrUUYJuNfPuE7kQv8AD9sugCAviwXWB/B6wOAbi4kVwjLt9GhFQ9d90ptOVzQsVatljSwwI+6PCnwA/woETEZUa5KArpecmbb/d6Wh64z2gTDVoDQFFbQIWUx853lHiojekontM3UOULIVV1ACH6sW36UBHRTrExQbxfQVY5K33giKdVacra+xd4qgIAfdRkm4Af4oc9ASuoA+mp5BgLxY0xA52HpoN42oOtKq2UgBB1+5Vsy5O76soqcQK7GTYGAH3V7tmp0WB9OnTU2oOug3j6gKzDkv1v10PVFAjrqABDIldvImsLAAj/AD56BnJGGQnp3CkBXoM5/13uWuxw0W3yjRkCPpaFWQAcd7hkIDMVlP6s6lidYH4szltMwieUF1nlZw31CfuQE9CFk1AXoISHOIVQqC19SavxejoUOOmQujQmE4Af4Yc4A1vnqjGB9ZFsfOQA9JHQuD9j+xniXs5QUqjEBHXSEydpYgA5+gB+2GUgFdMgV5MoyA6mAXhLMVWhe8uL5vQ20c+d+kcOuHEjpxTEWoIMOUQRWXhgD0MEP8MM1AymADrmCXDlmIAXQS4P5OIA+xuIYA9BBR/giBz/sczX0nOm4mbe/XdrAwvqI4xL4sXa+GlgfQwF9DDAvD+hjLfLSAAI64pQV+BEG6JAryJU0A0MiDZAraVbXPg80sIYA+lhgXhbQxxSqkgACOuIXB/ghAzrkCnJVYhMv5KqoXMUC+phgXg7QxxaqUgACOoYtDvDDD+iQK8hVoEcYVZUDuSouVzGAPjaYlwF07jXE8hw+9ePkCEFHGodyKyzwA/woYShCriBXEXIVCujcZchu87TJX9s6f9la7hHG9BdiSITmpGK+m/td0LE4o+x5TPkDP8CPEvIHuWpOrmIAvYTIhPQZYkiEla2FfK3kOyEhpxBALznGkL5BR8gsjfcO+DHeXId8CfwImaXx3llH/AgB9PEm3v6lkFB/G4DO9EnC1QKgg46p18Ta70Ou6uIJ+AF+lJgBQa5aAHSeFgnU2wF0CQxbAXTQUWK5pvXpW+yQq7S5HdIa/Bgya+XarAN+tALoEqi3Beg+MGxJ8YKOcspnaM8upQW5Gjqjae3Aj7T5y926c360BOg+UG8P0F1g2JriBR25VU56fzalBblKn9ehPYAfQ2euTLuO+dEaoLtAvU1At4Fhi4oXdJRRPCm9mkoLcpUym+ltwY/0OczZQ6f8aBHQbaDeLqCbYNiq4gUdOdVNnr50pQW5yjOnKb2AHymzl79th/xoFdBNUG8b0HUwbFnxgo78Sie1R6W0IFepM5mnPfiRZx5z9dIZP1oGdB3U2wd0BYY7d+YS1en64UUCOqabf/PL4Ec9vMA6r4sXnfFjy5Yt9c1v5Ii4pG3Djh07rPehb9u2jW6//XZnl7U/VwOvfZzS+FqlY2lpaZqjgiMXgfT6jvka2LdvH91yyy309A0b6Nf376f/j4g+TkSnnXYaff7zn6ezzjpr9nysv3ft2pW0PiFXEufLPldyZX5F0ge1P29Vrlzcrn2+zfHNAN38x1b/v9Vxm4ugBzp2c6haOiSkrM7M0jsrXjZOTj/9dPrsZz9L1xDRyUT0fSL6H0R07VOfSk888cRoQK4bDAcccMCMxlB5CX3PpZRraN+TXNUwn7rTNnQ8Q9vVJmc90LHiobdmifRiUfVKx9KmTcukNQ7qCtA3bdpE73/kEbqAiL47/8Og/hkieh8RHXPccfTVr351VGBXgG4DdchVFnuuWCfKQ4feXZ7iqeehl8jCQsh96klN/X4vTOmBjhVAbxzUf/bzn6dbb72VLiWi/zhnzPeI6CEi4r8Z1L9CRFcS0beJVgD9hBNOoL/4i7+gV73ylfTlr3yFjjzySHrs29+mZx9+OH1tzx467ZnPpM9/73u0fft2+vSnPz3YENixY8cK6EjrB3JVDJ+jO9ZD7hLfan/eg1zpDKx9vn3jW5NDb5mYXpjSAx0LgN4wqD//uuvoZ/7+7+m/GSr7YcNTZzBnUN/zghfQfffdR8cT0T8josOI6EdEdCgRvZSI9hLRRiI6mIj+CxHdcsgh9P3vfz8LoId4Oq0r317kysyhQ+/CU8+h95FD1zb+1ZJDqWUcKTmuWa7T/DUYft++ezf9scP/ekTz1JW3/itE9CARfYCIjiWig4joJ4joKdrfT2rePV8gy8D+qle9ir785S9HAzt76KHyEvpeCt9z5GR93+9FrhjQe+AH8xt02I2RKeZlw65du/a3vJtdTVrrFm5vdKzxpJSWbgzU/2H3bnqeJ6D62BzUOa/OwH/xHMx549yz5545/82lJAzs7J1zuF7l4dnTv3Weh39CC9mH7pYP3e3ey/roRa52ESVVKdTCz1rG4VqioeMLfS/1O6XbO8vWQsJ3tU9C6+FFk/m1z7c+PqfibSz8/uDu3TNg3uABdQbiL85D7L9MRK/TwJxD7gda2ipA5785D88Fory57qCXvITuvffeIE/91FNPTQKFFtdHL3LlKluD3l1dLFPruxbXB+rQK6+3b1GoeMxexdsQqH9t92561hygn+oA9R8S0U8R0U8S0RuJZu9zzvw5RDTf629tyd652lzHIftvzkH9LwM9dX2Xe6zxB7nyWGgjPEIdet3nnLS6PpBDRw69SA7Mmuu05dRHUJ7OT+ziwKf/94Xdu+mZc5BmT/1pltfPm4fS3z5/j71y/sMb36SfysMrT52BnT31uw4/nPbs2eP01H272125uylyerlz6r3IFXLoq4eW1SKXtYzDZUyEjA916IbGRZhneUJS50H00JWXPsVRt5zH518AoH9q9246hGgG6vyHQV33ut9BRF8lol/VPHMGc/bSQ3+PWjbX8Ua5bxDRx4jonxgd/bxWqhbqSYS+l8r30u17kSvUoS8KdWm5kfrvZX2gDt2idSXml37eg3BVq3jVprzdu4MA/cTdu+ldRHQk0QzYGaj5D3vfXJf+O/NNcAz0/Ed556Fgrt57XNssp3bMs9nxu3NQ3651aAP0ECMMchXLlYj3I+UKdehr57a0XpX672F9oA7dsWYl5pd+3rpwVQno+g77QEB/6cc/Tj+6554ZqL9yDursqX+KiN6jlaepHe2cN+fd7EN+P/bsgL+OiN4y79QF6OsB1HuRK9Sh21dIab0q9d+63kUOHTn0aXPoY4XczXK5QEBnxbthwwb64z/4A/o/iOh/IaK/JqJ/aylPYzDnsrTUn2sH/Dvnof3LjItZQnJrvdQLB+fQG5CrUL65QKaW9rWMI3WeeqADdehzKZAst7Gej/UdF+jk+n5VnpSt9j0Q0FW98He+8x2654tfpJ/nHfxEdFFgedpQcHftgOeowG9HHPeq+JmLr6XlRuq/N7mS6K39eS9y1QsdqEPPsAGstDC0GAaqRvG6DrIJBHQ9NLpraYnOnIe+Y8rThoK6uQP+94noFCL68BzQU+UOcjWUM55LhwbIlTmKVL5O3b5FufJJwtTzGfN91KGjDn0myzFCYxN+s30VgO47lW6A4n3K0tIspD6kPG0odKgd8A8Q0R8R0UvmgJ7Kr1aVbo9ypctGKl+nbt+qXNUeCQkdH3LoyKH3mUOXjpiNAHSlJDcvLdH5CeVpKaD+9fn96wzwP4Ecun8qmfelcugF5Co19zt1+x5yz73sMUEduqEaYOEuT0jqPGTzpFTNeCwaSjXmEYDOn37FK15B//3976cPEs1uUhtanhZLBr/Pt7X9LRF9mojuJKK/Qx16OqBXIlep62zq9vDIF0Vxan6gDt2iGqZmSg+LJDugx3hckhfFExwJ6NzksKUl+jki4otXjkkoT4sFdb4AhgGd/9xGRJ+3AHqIEQa50mZeAXoFcjW1vkn9fg9y1UvaA3XoDu2aKuSp7VtfJFkBnb1tBuBQ5VsI0E9bWqJXE9EL539sx8DGgnXI+1yfzqfGfYmI/pCvZ3UA+noA9R7lKoRvqfqkdPvW9ZW5DkvPV6n+kUNHDr3uHLp5GEwIqGcGdLX4jlpaoq1ExKe2Mag/PQSNM7zD96vzH760hWvR/9Ull9Bdd9210nNoDjP0PZdyrqF9tjr0iuSq5vkOOYu/BrkIGac0zz3QgTr0OZdLWUyxlmst43DhUOj4sntSakAhnnpGQNfvrT5xaWl2Feo/J6JjHdeiZsDvhS74kBkG873zO9SvJ6J/QB26f5pDNsXZDhqSjMVCcmUjJnSdpa7T1PatjFOisxc6UIeeYQNYaWGINQok4R3jeTFA58HroG5T3hkVr16H/m+Wluje+YEyfCocH/dqu+s8F6jz4TLKO2dgv4aI+Mz3J1GHnh/QJ5SrXsK9oMMulqXxQe8fdeioQ59JYW6hKwroSvny3zavqiCg/08i+mfaRS3m7Wu5wJwPleHac+WZf5yIvkhEf0ZE5+/YkcyvFo1EHnOPcqXLTO51ODbItipXYzg5Y0RikENHDr2dHLq+IthLHwnQlZJ969IS/RrR7Ez3FxlXqobcfR4K9lxrrsLsD80PlPktIvrM/CpVnOUuzOSQkLvqcgK5coFgKzndVsYpzXMPdKAO3dANU1vIvVi4RT0pveTMllMv4KGzXFy+e/fsylSuRWdQ32ZcqRpzB7oLkjikrjxzBnPe2f4fiOjm+WY8bof70AsB+kRypW/oGtujTtV3veirXuhAHbpFN6QKeWr7HoSrKKCbPDNBvQCg8yf/YImvZSH6b0T0tjmo/+z8bnR1TzqH4FOuT9XBnE+HYzB/n3Z1qgvQ+d8luYNcaYITKiN6Sie0jXSoERHhPvS1ileS39LPe1gfqEN3GPulhUfqv3XhGhXQebLMjXKSUh1wsIwCdP4cn9p2IRH9r5zG1+5JZ2BnUB9yjaraAMeeOf9hML9gfmWqLqa4D72Qh27rdgS5CjHGJH0x9fPW9VVrkREXv5FDRw69zRy6T/lm9qTU4tEBnT/P96IzqPOpcb+kgfozB+yAN8vTOLR/HBF9yEIncugjArpuLBaSKxcYtpLTbWWc0jz3QAfq0OdcntrCVd+vZRwulRk6vtE9dDVgtakpk4eu6tD37dtHt9xyC5111lkLf2/fvp0+/elP00/PQ/AM5ofML3AJ3QFvK0/7s8MPpz179qz5nvr+jsjStVC+pfK9dPve5Kr0fJXuvxe56oUO1KEH5B6nZnaL4aysildwyKyPMwE65zqXlpacwKoA9qijjqJn/t3fRe+At5Wn/RfeADc3FEwDQv//U089lVI2VEGuBghWRrkqDbZT9d+iXPkkYWr9H/N91KGjDn0myzFCYxN+s30WQA8JcQ7QybMmETn0EEBXQLs5Yge8rTztAwccQBwN8AG5ihQccMABTuolfraqdHuSq5B1ZL4j8XXq563K1VTGT25+IYeOHHqdOfSSYB4B6Gd94QvWULsLcI8//ni6+y//cuap23bAc1kaX7ayYb7xjQ+O0cvTTnjVq+jLX/5yEKAzsHPoPTT3F/qelGuc8nnyWe6VyBVHfnrgRy/3iPdCB+rQDdMst8UUa2H3YuEmeVKlx9hU3QAAIABJREFUlW5BQFdAz97zv963b2EH/FP42NY5oKu/f0BEqjztH4iCgTw2lw65IqKK5EqVrU2tb1K/34tc9UIH6tAtsZZUIU9t34NwDQb0MZTuCID+mte8hj73uc/Rv5jvgOeNb+ydH0pE98yPdOXb2rhU7feJ6NGXvITuvffewYAekjaBXPEWx8K/iFROD/zQZzNV703dvgd+oA7dsb4hXMsTM3QeBgH6WGA+AqArD/rwww+nw/fsoa8/7Wm06bHH6MVHH01f/ta36JhjjqFvf+Mb9IoTT6Q///M/jwZy00MPVUah7w3le+n+e5Er/WCZlHVWer5D+w99r1a5io2k1koHcujIoRfJ5UXnOscE8whAD93lLm1iO/PMM+lP/uRPBgO3rX+1KS40Fxv6nks519C+J7mqYT5xj/iq09IDP1CHPtdetVhctYzDFZgMHV+UJzU2mEcAOgdpd+/enRWIbfXskkHgAvRQfoS+l8r30u17kquUksNa+FnLOFLlrhc6UIeeEFYeK8w01ndyCnWw4mXipNreEmnPyFyn62CZIUCcA9Btu9tjw4aQqwKCFSlXthHkXIdT9N+iXPkkoSV+oA4ddehJuXLX4o0C9AJ6NajLAEPCzHVCWS3O7NjKDnK1LekwodL8wvqYdn0gh44c+nQ59CDULfhSQKgf9cK3rzCghhxjUA69oMgEdQ25WjA6apCbkL0CrYzTt8cFdejGCi1twUr992LhBnlSQdqx8EuC8kW98LQeh8l9yFVd/OhFX/VCB+rQLXghgW7p5z0IVzOKlyfbA+q4t3rtAikt/77+IVd18UMfzZRyweNI/X4Pehd16A4HMFU4Utu3LlxNKV4PqKNe2L5AUuV7aHvIVV38MEczlK+16LtaxjF0HpFDRw59/ebQTW1k8dSRQ0cOPTnpA7lqIqfeRQ59165d+1EPmR6uGWpRmRZhrn5cSmis/pvzpNSEGcpX3Yc+9Xymfn8svqeOU2oPuVqeoVr4Wcs4JLmRnvdCB+rQK1ockifQktA1q3iN8LurbK0mpSopK8iVNAMjPdeMRchVPUaJxP2W9C7q0FGHXsTibxrQNVBHHXpddc+Qq7r4MdSYrB0kW82lI4eOHDpy6C6tdMMNxJervPzlL6f7779/5a0jjjiiqf9/5JFH6Ac/+AGdcMIJTY3bnOf9+/fTvS94AdFznys5VXU/v+GGqHvsXeBSS863lnGkzlMPdMBDh4dexkNfWprmSNecqvx3fofo3nvpTW96E73rXe/K2fNofV155ZX00Y9+dPa9XuiY5KjgzBxD5AeRhhJpO3jo8NDLeegBJ2Zl1pN5u/vMZ+hNRx01A8Q3v/nNdMkll+Ttv3Bv73//++kjH/kIbd26lV75ylc2TwcbJB/dt897bkDhKc3WPaon6qqe4I3hXXnotec0pPG1mvPoNQe1kutsGdQ/8xm680MfIuXltgTqCswZBA855BC66KKLmqeDoyRbL798ecm0LFdEhBMIFzWfpN9LP+8FP3BSnAVRSwuP1H8PwrWwealV5TsHdObHFVdcQR/72Mea8NQVmF944YV06aWX0tVXXz0D9Nbp4PGvAHrjoI4TCNcqXkkvln7eg97FSXEOF7m08Ej9ty5ca3YjtwjqGqC3AoYmmPO4dUBvmY41gN4wqOMEQrvilfRi6eet613k0JFDL5I7st6K1RqoG4DOi/29730vXX/99VV66jYwtwF6q3RYAb1RUEcOHTn0Ejn7DTgpbtkmK235hfYf+l7tuXdnvXBLoG4B9FrB0AXmLkBvkQ4noDcI6jiBEHpX1+G59D5OiqsIzF0g3WIYyHsASCug7gD02sBQgfn5559P7373u9eIkRly11+oKeIg0eEF9MZAHSfF1eNE9aR3UYeOOvQiEQrxRK8WQN0D6Dxpl19+Od1www2Tht9DQNAH6C3RIQJ6Q6COOnTUoZeIDCOHjhz6eDl00xRmUJ/yt4sDn56fAOg6GB588MGTUPLwww/TG97wBtrloUUC9FboCAJ0BeqTcGP+UUmu5mVrJXKo+kVbY/U/1ndckcpc38/VT+lx+vpf8dBzxfBd66h0/y2GpX06p/R8le5f9NCV4t25c3zVqwwJSfEGADoP/swzz6QHH3yQDjrooFFpeeyxx+jEE0+k6667zvvdEEBvgY4oQK9ZrlCHvkZeS+sjqf9e8AN16BZVKDG/9PMehKtaQFeh/t275aNpAwH97LPPnrHs5ptvHhXQGaj5p2rMXR8PBfTa6aga0GPkSgN0pqm0Pindfw/6Sl87peerZP+oQ3dowZKTHrKIW18kVQK6nrcHoK+RfAD6QHssVq4MQA/RB1PrI+n7resrk/MSvbU+Rw4dOfRpc+hjhUbNTXgAdAD6QPxeaDZErpBDpyly/S6jQ4FzFzl01KEvs7kWi6uWcbh0Xej4qvLQbTvqAegA9FRAHypXRIQ6dOjdEmF+1KFXBOaSfgkF01QwztG+GkB3lccB0AHo0oLzPU+RK0vIvYRyz7GObX1Iegjh98VZk+Yr53PUoaMOvUiEogpA99W6A9AB6EMBPVWuPICeU7kPAePU7wPMpwNz/jJy6Mih95lDlw6uAaAD0IcAeg65Qg4dOXRLZDhHDh916MaiTrVQU9v3YuFm89CHHj4j1ZgD0Nc3oE8pV6hDXyN7qXoztX0vehd16BYrPVU4Utv3IFzZAT1mN7zkRfEEA9AB6DwDU8gV6tCtsZFUvZnavge9izp0R9gtVThS27cuXFkBnb1tBuBQ5QtAX5BqHCxjWeT6QTBjyxXq0J3JjlS9mdq+db2LHDpy6HXn0M1DO0KULwAdgC7JyZRyhRw6cuilcuioQ1/WfamWXa72ufpxmcBj9Z/dQ1cEhXjqAHQAegygqxRMbBvbIgtJ5aAOfWXmxtJHkj6sZRzSOKXnqEOvCMylTbctCV0xQDeVL4O3qYgB6AD0IeCsG4sl5Qp16FU5UT3pXdShow69yOIqCugK1F2bmgDoAPQhgD6WXKEOvZqIaE9gzrQgh44cejs5dH31sTcFQJ9NAW5bM9Syzbs2NbfvpLfScoUcOnLopXLoO3bs2F9TDlnKEfRmUQ2lt/bwe1EPXc9T2nLq8NDhoQ/x0MeSK9Shr1F7U+szNaCpx5H6fdShWxA1dVJT2/cgXEUB3eSZCeoAdAD6EEAfS65Qh271Y1L1Zmr7HvQu6tAdLnKqcKS2b124RgV0lftUShyADkDPAeil5Ap16M5Aa6reTG3fut5FDh059DZz6DaVoDx1ADoAPReg66CeS66QQ0cOvVQOHXXoy7ov1bLL1T5XP1Pn5kf30BXBarMcznJfEQGcFGdZDSHgrDfLKVeoQ1+Z2Vr0XS3jSNXbqEOvCMx72vCXFdClibE9B6AD0H1yU8nlLLYhtg4urYetTZ60xA/UoaMOvUiEIgugx3pRMcAfcqLXZz5Dd37oQ2KvZ5999uydm2++WXw35wvsefMPZWvGrEpla1PLFerQq4mISuuxJTBnWpBDRw69zhx6SaWr8qKSFw9Al/Rdluehhgl/bOvll8vf9AF6DXKFHDpy6KVy6KhDX9QPU1tkvYSrkjz00koXgG4FxdojDcmAXotcoQ59jfxB7y5PSeo8oA7dotpSJzW1fQ+gPhjQx1C6APT1B+g1yRXq0K3yl6o3U9v3oHdRh+4I4KUKR2r71oVrEKCPpXQB6OsL0GuTK9ShO9MmqXoztX3rehc5dOTQ68ihj6l0AejrB9BrlCvk0JFDL5VDRx16ntxFqmWo2ufqx2UCj9V/lIc+ttIFoK8PQK9VrlCHviJ/Y+kjSR/WMg5pnNJz1KFn2IhQWhhaDAMFAzoTJ+02dwboEh5kLlt74IEH6IQTTkgYUHzT++67j04//XS67LLLvI1jDpapmQ4mMniXe81yhfvQZ/JaWm+m9t+i3kUdOurQiyyuKECPx7I8LSRDIqBs7brrrqOrrrqKjjvuONq0aVOecUX0cscdd9AVV1xB55xzjrNVCKC3QEc0oEfMY9ZXJblCHTrAfC5wqUaH2R45dOTQp8uhZ9WiAzqTQrICoCsQvPjii+ltb3vbgAGkN7npppvo0ksv9YK6BOit0BEM6OnTmtaDJFfIoSOHXiqHjjr0xbWb22IyNYPUf4thHpv2C/LQ09RmntY+5esB9BpAUE2ABOo+QG+JjmYAnQcqgPqOuSMh6YPan/eir3qhA3XoFliYehH1IFzNALpP+ToAvSYQDAF1F6C3RkdTgC6AugJ0fm1qfZP6/R70lQ4DqfMxZXvUoTv8vCmZ0oNwNQXoLuVrAfQaQVACdRugt0hHc4DuAXUd0AHqqxoPend5LobOA3LoyKGv3xy6acyZYVID0GsGQR+om4DeKh1NAroD1BnQTaXd6v+3Om4zstADHRtQh55mEeUONw21zHKPwxG4CLYcm/PQFcE6qGuA3gIIukBdB/SW6WgW0C2gvotoYWOYud5a0QOtjFPSZ73QgTr0hPDGWCA61ndyCnWzgK4r3zmgtwSCNlD/xje+MbtitXU6uDQvqA7dpb2n/nfNWDRD7j2k2Vo1SiSxyKkXbd/K2T/q0FGHnpSzcRkbTQO6AvWf+im6eNu2WZ35lKVpksJxPVe737dv306veMUrmqeD6+0v+eIXh05HHe3moO4C9JzKvTR4+CYUdEwT+UUOHTn0Irm83XffXYcCTRkFK1+iJsFc99QvueSSLujgevsnX//6FI7W0faGG2jHjh1F1t3tE+izHnLPPG890AEPHR56EQ9dylm18PzBBx8kPl71wAMPXDPcI444gu6//34nQNT0/Pvf/z7t37+fnvnMZzZNx759+4jn9bDDDltDBzzCaTzCFtbxkEiFK/JYe1oBHvoEFq1LWJRS6sFS7MXiBR23r+iwWuSylnFI61h6DjrsRshU8zLVdyU5iXm+4qHDwoWFq1ufqfLQqoULj2Nb1buvIVeLEpq6TlPbgx918QMnxVk0eKqQp7bHIqlrkYAf4MeQsK2kByBXkKvccoWT4hwumbQYSz/HYsdiz73Yc0ZgSsu/1D/WB9YH1sdqOkzNBXLoyKEX293ZQ04KOXTk0EvJcal+Y3KuOXbFg4569gLgpLi59EsewVjPx/pO6Vwx6KhjT4Zro6XJ/1b41co4pfUFOrA+SkTMcFIcTopbkavSSgZhUoRJESZdGyZt1bhyGS1Y59Otc9Show59Jn0A8zCPAcpqOmXlA5DS8lu6f8gV5CqHsYscOnLoyKE70i6ucHWrOcNWx22CHeioJ2eLPSZ17TFBHbphFpW2xKX+YanDUs9hqUs53FafY31gfWB9uNM2qEO3SIcEuqWfQ2lBaUFpIdfcih5oZZyS3u6BDtShO1wVifmln/cgXPrUlp6v0v2DHzCyYGTByKpdDyCHjhw6cujIoc9moJXcdCvjdCl/7M1YPF64Fn7WMg5JbnzPUYfuUOamNV7aA0S9cF0eIPgBfpTwyCFXkKuScoU69BFKtlKNgdrDPKHjC30vdb5KtwcddSll8AP8KAGSLcoV6tBRh24Nt+aOULS4OBzbK0aZLxgly7MvzQPkCmAOMF/d24AcOnLoxXKnPeSkUGdbV50t+AF+lNIrpfpNyYnHnrWPOnTDvJM8gtLP4XHA44DHgd3UreiBVsYp6e1e6EAdukV7Sswv/bwX4QIdME5gnMA4aUUPtDJOH/6gDt2RKC0N2lL/PQiXPrUSvbU/Bz9gnMA4gXFSux5ADh05dOTQ56vUlUPrIbeG3DNyz6XkuFS/Y+aee1kfqEN3KHPTGh/LgxzrO47AhLirOHR8oe+VHkdq/6BjeQZrmYdaxgG5Wj4cBvyoa32gDr0iZeVSErWHeULHF/pe7UoCdCD8jvA7wu816gHUoaMOfRQPrEbhH6KUQQfAfIjcSEYq5ApylUOukENHDh05dOTQrUZdrbnRWscVm/MFHfZw9VTzMtV3Y+XG9z7q0A2zSLKkSz+HpQ5LPYelnprjrbU91gfWB9YH7kNfkIHSoJzaP5QWlBaUFnK0reiBVsYp6eUe6EAdusMVkZhf+nkPwqVPben5Kt0/+AEjC0YWjKza9QBy6MihI4eOHDpy6BPogR5ytr3Ub/dCB+rQHcrctMZLe4Cq/7G+UzpHCjrsG34gV3bJC5WX0PdKy3dq/6AD66NEBBN16KhDX5Gr0kqm9nBV6PhC3ys9n6n9gw6kEZBG6CuNgDp01KFbw625PUmAB8AD4NEXeLiiFPzvqcbm1O1b1VfIoU+QO3MJiyvs3mqurdVxm/wBHfbw6FTzMtV3pXUb+xx0QK5i7zuX3kcdumFmwjLMk9tq1cJNzY3W2h78QIQEEZL+IyTd3Id+zDHH0Mknn0wbN26kI444gu6//3768Y9/THfddRfdeeedK7LM75177rm0Z8+elX8z36sN1HXa1KB5zI8//jjdeOONtH37dtq8efPCev3e9743m4OHHnpoZV70tjwvPFfPetaz1rRV76l5dIHUIYccQtdeey0973nPo9NOO402bdpkfdXXj0QHd+hqr/jGdOiWK9IFvmAowqG1GDe1jCNV34GOeozFburQTdBjELjvvvusgM7Af/TRR88Aj3824E8V8tT2+iIZAsoM6EtLS/RzP/dz9IxnPGMG3iags6Hz1re+lR599FEnAvjAWBkNPM8+QA8F5VjjQudb6nxL7aG06lFa+kgkvtX+HHIFucoZOekmh86gZ3re7DneeuutKx46L24dHE1P3vT0pspxmd81Qdk0VkxQ5ud33333DNBtEQl9Xti7P+mkk1aMGxv4mqCu/l8ZDdwHe+vKQ1fPXe30CIBudNnoUEaXNC5lXEg5Jt1oMOlw8XsqOXAp+6HjGdou9zhS+wMdyzNYyzzUMg7I1Tbqpg5d8kSV0Enha5erOpalb/uOlE7QQ+4mSEmRCzNcL4XZ9ecK0G3jC+1H97JtdLj4YfavxiJ5POobNjpsnt9YfC8td6DDDoLmvI81T2N9B3K1fG+7NA+98KObOnQXUKtcsWKq7T3Tk59qkbuETo3ZlSbwgdQQYyAWRG3jc64gIyduA/RQY0D/hg7ovsVpi0iEGgO1yUWqEpKMn1aetzJOiV+gA+H31PB7N3XooZ635LHWprR5PNJGPh9IpRgD+lzYcu26hy5tNAxRVqFga8u1q7FIStNHhy2MKVn2rT4P4UfIfEjzXfo56AAIpoKgz/koLb+5+0cOXdsNjxz6smir3eu6svTlnl05frUL36V0TWH27QXQwcUVkeB8O3Lo7twqcp3LkljLPNQyjtD1iT0mi+H7GvnXTR265IkqoZVy7RdccAFt2LDBabSZ4WA9ZHzKKafQGWecsbDBzOxIb2/bXW9bXFKawFa2pkBZilz4St50cLTl2vWNd6pkUI1/SEmZjw59Hn17BqQIS+ieAeXxq5K8Y489NpivZgjf5emm7BmwyZXih6+M0JXOSKmK0Odq586dtHfv3uD1g3RHWI631kgQIiR1RUhQh26UraWAMrdlxR+aA/YBug4CKaCcUvKmW6BDjAZ1BkBoWCmHcSGF32zfsPHLBHTewR/KVxug20A91LhQNEnfTzkXIDR94kuLlDR+JL62+BxgWBcY9sAP1KFbAH0oKCtAZ8GQlK+rTt7mYaaAshSRCM0pDz28Rh3qEwLqobnxUE9en0v1fVtbG79sgB7KV+UpSyATalzo/ZQ6F8CsWBiyAVOPDITIP6dIQo2fWj3UELn27bLuAURs66xVfrXOD+TQjRy6LVfs8pBC6sF9ddS2Onnbvby116Hbxsd056hD18HRBGWz9GxoDt3GX/2kPXPDn0SXbQ+CLbdv2wtgMxqk75mlikP4YTtZ0HZOQ+xeCukcARt/XUpV8b/G3KW0d8P2HHQsc7qWeahlHJL8+56jDn3uofuOUfV5ZnquOMdJZ6hDXz7C1gQp3eIP3Q1v84xcIGp6FAqUbREOyWNOKaEzxxHq6XK7lHMBcuy5sEWSXMaw+vfQ6gQXmNsiWiF1x1N7kKmefS3taxlHKj97oQN16ESzs8jViXK5w7mhZ6r7lJC04a/nOnRbSDY0TWBb5LHGgHS+QWiawDaWlFLAHKWKOtj6NjiGro+UvR4pINx6mLRVo8QFouDH4syMaSygDt048923m9smwKkbx8zSLts3xq5Dt43Bt3HMNj7XYvdVCYSCbSkQtYX/SoKUj46UvQ8h8qIu88lxsmAOY2CohwXwmA48fIA+JohJennI81blCjn0gBy6KywYU0ctnamug4nZL3LoS7P1VSqH7uOvdEeAzQhJyaGXymXzmflmWsm2B0G66yA2hy6d1Y8c+uqxpK3mcFsdtwnaPdABD93w0PWd6sxwKYcphSN97fWrQ33WbojHZV6wkmPXsj6mlDC36ieFDn3xwUO335IXU3o2locurR/T+IkNP7fqSQ2NSNTu+YIf00ZM4KEbHjqfFsdlazYwt+02ljyQFne5x54UF3qCVEqkAR76/SvX/Jr32Ifscp/KQ8cud5zc14rn28o4XUYTjx8nxRl16CknxdnCr6Yl3uNJcaG5tJQT7/Rv5D4pzlc3nrIhUR+zzbPy0SHdOZByct+YuXuf/PMznBSHk+Jse1diIzWpkYteIgs4Kc4AdJ9wlVTuvo0bKRuzUg6lCd3w5wN0fT5T6Ag9htY3j/wstophyG11oYelxIKylN6RSs/UyX1T7a7PpTRz9ZMKAqntQce04eke0x44Kc4C6C5QL6ncfcKVAsopu6VtYW59nGoDV+hJWCl0pBoX5oa6UDpSPOUU4yLF+PHNM5/cl6PkzXafvLQ+pIhFzHOAIcBQWl9DnrcuV13n0ENP2HIdDhPSfqocupSbDM3dh+7Sjt3dnOukuFA6bcaFLScWehLgVCfFldrlLvHD912l5Mx0kVly6Tqhztae/437wy537HKvJXddyzhcRkXI+Lo+KU630EJ3m4ee9a36nuqkOJdHL+0qNo+rDa27jz28JmQcDDL6BTUpt4+F5mJrPykuR3WCzVOW+OEL1yvw1Y0r82IhVjY4KW7tBjhzneLEu7oiC73xo+uT4lyegbnIpNzkGMZA7ElxNkCXlLZ5NrcKv7pKmPRvxIZpXQaHCQ42QA+lwwybLy0t1qvbxhC6cdF3WIokV6HHmY6Zy5b44aPXxg+db0op2tIENmMgNN0RGv4MfS815126PeioC+xb5EfXdeiSMlHP9bB57JWWuYwBn8INOYktBgQlEI09bjVkfC4QNMdi1tOb8yJVCUhK1waitkiO7Vz00IhPyN4CHueUu81NfrjoNeXKFwmxAbokl6hDrwvEpPUjPW8RBH26V6K3tufIoQecFOcCIzN8bYKFlINPuW0tNLdsM2pMI8QE0dg69CG3e9mMqVzz51IqJoj6+IMc+kbrbXm+nDdy6MuSF5LrtN2qGNrOJ99Dbn1L7a/VcZt090BHN3XosAzrsvTBD/DD5vmkejSQK8gV5Gp1I6U5F93UoeuEpSqNqdtDaUFpQWm5lRbWB9YH1od9fXRTh24yeGpQTv0+lBaUFpQWQL0VPdDKOCW93Dod3eTQkZOqr561h5wU5ApyVUqOS/WbmhOPbQ864vZOxM5vzPvd1KFLllcrz1sZp8175H/rra4T/LArq6kiYuAH+FEivdqLXK3rOvTQeuGpma1baPptcKZSnfo4TmmeWg9nTQVikvE09Dn4gbQO0jp9pXXWdR16aL2w79AXCcRSn5tK17yvXT131QtLF8qMpdTH+k7qfEvtQQdAECDYFwi6DGI94jjUaJb0Se7n3eTQ+djJc889l/bs2bMy91IduK3e2lbH6Tr2lGvBS9R9+nJSDOhnnHEGSXXo+slf5ry46t9jcjUhdCO3Vk9uDXsBsBeg1Hos1W9ufST11wMd3dSh2zxRm1VlC0tLOZmU+7djw7SSBxh7Frl0a1cpy1Oio5XnrYxTsvRBByILiCz0H1nopg7ddiGFC6wUqOe4t3rsRRJ6gYnt9iw1Vv1CmZLpBIAIQGTs9SEZ5zU9x/rA+si9PrqpQ5furY71lHWPx3YDW+oZ1EM9qtDb0WpRFrWMQ5pv6TnogPLNrXxrMi4k+ZeeY33UsT66zqHnyhXXlEMPvb88JMct5ZRSn/eQk0LuGbnnUnJcqt/UdRvbHnQsz1gN89BNHbp5MQRPsO2KR1cY3meBxl65yYbEaaedRps2bVr5XOitU5IlnHpbWMh96PqGO0WASk/wXHB0QqdNn1NF55C9CjV6LBI/Wnneyjil9Qk67OCREoFMiTyAH3Xxo+s69Fy54lgQNQFdAnN+rt9i5VNqtis31ftD6tBjc/J81axprNi+r9Pgu3IzRZlIyr/Uc4QX6wgvTgVikKttC9U9peZjamOhxXXedR06A/rjjz9ON954o0vm1oRJbEoiFkR1QA8Bc/aIXRv0TKGy5fP5naF16LG75m3RB9v3XYA+9SJN/X6Li9wp/JYwYWsgCX7AuCrhFLQqV13n0EvWodtATG2Uc5XQhdy/LeVsbeF/W7+hdeg5+nPRZVYT1JBjGrK3oNVxm0oJdNjDo1PNy1TfdYHV0PEMbZd7HKn99UAH6tCXlhYMPJsHFxuW5kNuTj75ZOJjWs2fy2MPDUvHetRSHXoOj9/nAUrphFSPuXT7Vi11hEH9YeHSciP1D7lCZKFEZAF16Aag8ySbizG2VEzaoGfLydtC7qHGha+e3mZc6HsLbOkE3xn3tnp/ffPhFCV+kvJMfQ7lC+VbQvlCriBXueWq6zp0225tNYExnmMsSDHomcetmqB30kknLRzfWuqQm1TjwgRDqd7fNlctnJkvgT6UL5RvbuWr9yfJX+3PsT7qWB/IoV977QonXDmU2Dp014UoaoOe1J8vlxNbh24zLvT6/JL9IYeOnO2QPQtSLrSHXKe0V8YWKayV7lrHJcmR+bwHOlCHPueqzwK2gZ4v523LW5tBcgYyAAAgAElEQVQgunnz5gWTTm2okyxxKVyf6lFLkQubx68Tgjr0Oix1SVmZ3qYkd7U8r2UcqXsUQIfd2J1KLnvhB+rQA0p3pgZRXTmXzHmHnHFfcsNfK2G7VsYpKSnQUZfxBX6AH6lpHdShb5N3w04JouYij81RS/ehu8L/S47d/7boA4/RV2+vRzMkkKn9OZQulG6q0nV597Yw+1Qe69AIBNbHtOuj6xz6VGe5r+ccuq6UeshJIdeJs9xLyXGpfmNzx6nvgw57+mCKeem6Dl3fWZ5iOcbeh55SKiaNM4dHrZ+gl9vjb82jkCICEj9aed7KOMGPaT282PULuaqLX13XodsAfUhYy1aH7rvAJHTjmC4KLdeh+0KI/Kz0VbMSCKQ+h9KqS2mBH+AH0h6rkTN9LrquQ+eLRO666y6688471/A/RsnbvFhbzliBsq1UzByA6+x1yUIe+5CbkF3zvnp/pgd16KtcjZE7KC270tLnZer5TP0+jBMYJznXOXLot8s5QtdxqyYoT3WWu+kB60pmjDp0nOW+uLFyitxZSL13reOKzeGCjnpytthjIuNHrHynvI869Pnsxdah2zxu/UIU8yx36dY11KHXZakreUj1wGppX8s4XOmZ0PGFvpf6ndLtQYfdKJEilKWe98IP1KEPrEPXBcus37Ydj5pyDK1usU1ZQsdCjzr0esLnqUoI4d66jEjwA/xIDb93U4de2qKeqn8scizy1EXukl3+91SjYOr2WB9YH1gfq2H/bnLoyOXUlcsBP8CPUrnuUv2m5C5D9jCY/YMO7AUYIjc+Oe2mDh2WOix1WOrYFd6KHmhlnFIEBnTUpXe7qUPXp1USwtqfY5HUtUjAD/ADxiKMxRb0QDd16OaCqx20pfG1IDwhOVjQATAEGAIMW9EDrYzThR/d5NB59/W5555Le/bsWdEftrPc9V3aane6OlFu48aNlDunMaS/9ZBbi72H3WY8jDVPQ78jnek/Vc7WdXwwV2L0uD7M2xL1K4KlcxpKyV2ovhqiP8aUqxQ6al0fY85fbv6uuzp0221h+tnmNm8ixBOVPO7Q56HvTT1O6fsSHbFX0k4VgZHo8D23gYV5HHFK/zFyqX/HdvKheSgSG7fq1/r68N3FYCsxzXXCpC8yYTseWpeNseRCWsfS8xQ6al0fJSJKY/Fz3dWh2wDd9OSnAg/X4mk9DGSbz9h6er2PsRZHKj9SlF0MWEtK13zuOz64x/Xhu9BIp9eM2JlHRueUO5djwUdVm5HCnPooN4im0FHr+sg53yXXsQ0XuqlDD/W8p7LIY5Vuy0LlA0KlFGNveavFqIkZh3Trnh5uG5PfNXqsJdeHLb2j37vApzoeffTRpA5/ynVLo88IDdVXueUiN4im0FHr+qjJeYg1IpFDv/9+Qg69zFnkvtwzcug3ruiNoTl6l3Hh6s80pvTjiNUFOsihE9n23uTOdabknmP5rr+fm78pdCCHnl/vwkM/4ggaI2dW0gMZO6wzJMdkKiF46IsbMHN7Yj7LHh768g2AS0tL5AIV85bGWE9J4meKZ5sS2YGHvswZiZ8xkbgUfkjjiH0ODx0e+hrhzuUxwkM/hmzh3Mcff5xuvBEe+lA5i22HXe6rIAYPfS2YK3mKlasa3+/mpLhQS1uyUC+44ALasGGDMw1s3pqm59ty5OpsliGHAE877TTatGnTyrhCb2+zeQLcia+9LfxqToirvfJ8JAvXdSWt6zvmPG/evHn2aggdHIEx5099R6LDJlc24TDD17r3x5ucbBuufJ6y5OHZNrb5LgDS00q+DYnS+kjZ1Z+yPmLp1cPmKbv6TznlFDr22GPXsNwlN670ndnBVHIVenFUi3TokVaTb5K+DKU31mOW1nHu592cFBe6W1Xa5cmCcMYZZ6xskJGUtw1ohih3XxjbBuihYJZyO5pNuevjtC0SE9Bd4S0bmLmsKP6OvlhDlbsaC7/PZVm6QRRKh02ufONkUDXDuaoELIQOdQWvTx74mQ2kfEpLl1NfyaC0PnzhSsmoTtltPoReFQnxpXekjVnMOxugu9ZfaOnZVHJlM/BtctMiHboRZzPEQtdH7aDtG183J8WF7l6XlI4SBMmii/W4Qsdns9hSPMwUULYpd8lTtwG6DQRCQVl9T/K4fMYFt925cyft3bs3OPLiA+XQq3ClEjCf5+gaqK9KwGfk5Y4k2ZSKZFSn5O5tcxVCLwNySkQiJWIXYvzoxt4YchUasQs9LyHU49cjYspoTVkf0h6EoZEVVarYKqh3nUM3lbzr/13hyJD2uueYK1dnCpMJyiHj0heNeYKe1N48ZEQv6bEpUdtuaem2NVv41Tcu/XATV7jebJ+DDtsu3pT5k+hQ6Q5pV7UOUtJ4VGTA9Fhz73K3yWmu9aHLSyi9amNbyrpMkVMFYrZc61RyZcuh29JPtsOEaqcjVY9PtbfFJyeSHjCfd31SnBRWVc9tSlbyRPW26jCIFE/AtTGDv5M7PJfiCUiRCwWikoVrC/v6+CV5mDZ++TyBUDpyRzgkOkrtQZAiHDlOipPC1ynrQ8mLxLfckbOUSJIvbTKVXIXm7kMP26qJDin9aeOHkpdQeqXI2dTPuz4pzmZ52iY8l7JLydX5Sh+kNIEvN6nTawPZnGFQXhy+HLBuieZQ7jptOhCadErpDpvSHpMO20EmvnCty6JX/x4qL0PCoD6QksKgNa6PlDSBOReSEas/d81VDueg1B4Jm9zVSkfKBswUvvrWxxhg33Udui08bPPkpA1XY+R8XMxWHroZNi/p6dnyx7GejyS8Kco99tjYnErbVGqxdNiMzNwnk0kb21KMOJ+yK5kTLbU+Qo0f20ZXyVj2PS9p/KQYyzFGidJNXJrpOvs/dn3kqsZINWoko7nW58ihG3XottykAriSOULdIwvJoYfmJm2ent6/zZL15Z5jcsBD69BdSjYkB2zLOaX2l5MOm5Hp2oPgUhpS/atLTkPmL6VOucX1YTN+zIjdSSedtFD1otbHUP4w/6QT1kqtyxT+hubQQ+cvZV1KdAzR413k0Hfs2LE/JbxXi6USGg6SciUpll2pMI/k+cSGQaXIha8OPdSCluQiVwmTAkPf7vqU0r3QnKPNg0vZ+yCF/WJzu6F8Qx06zY6CVsp96G5p6YKVlMhASsRJSj9Jcmc+r5WOlPVR08lvsfzopg7dFt6Swlo2IEwB5RSQigVl2y5UG/NTwExS7tIVqD5QTwn7xua8UzfuqJBi6IasHCVvPnngZ0Pqsl27vrk/fcy+9I7P+B9LudvSFuZ8SRtdQ43WMevQpcOTcpxvkOIc2GSypHHhW+cSHSnrIzbtEAu6Jfvvpg49FMykE4FSQDkFpGyLRSnP3LuH9W/56rdDcrFmODKlDj3HXgWX0jFBynzPnIccoGxTOpJRYIZzXcpiaF0219mmGK0+I21M5S6Beq6IxJh16DXv5XGBkOTx5wbl0GqMlPXRcsS6mxy6re5ZyunZckJDci856l11IcqZQ0/JEabkuIbUoefK3ZugI9HhAqkpc51S/ekQOW05h26rQx97b0sPciXlniW5M5/XuhfAtz5ce1CkvSktPO+mDn3IyVW2MP1UuzJj69BrCM/Z5iqlDj007OvzMG3gLIXnfOHtlLRDjgiHyzOKjSTlKs0cM5JkO642dK9CrvXhC6e2KFdSWsQXgbHpy6nWh0SHb33Y6CgZBh/T4++mDt2X39FPOpNKhIbkXnxnRvO4XGFWKfyvaJpqY1ZKOM2l+JVw20prfGHU0LPIbYvHRkfK4TpSuNflOXK7FDr0ObUZNb40ggvglHyGpjt8xo8N4HzyL82jFDaPpTeXMeCSbXUin3n1aqiRKc2HT670McWm0WzzGHqltIvnPr6nrA8fnVIpb6i8+Iw4n/z7jN2xNtp1U4fuAvTYDT6xuyMlz8eXMw3Z2MZ02Tw9n3DyM999z7a5yqXsXDl0U6kNzXFJZ3OHRmp8Stl3wYqkdNXccx8pex8kjyH1pD3ztrrQDVc+ZTdkz0Do+nDJi4sfknIPvQ/d5un51k/oIUFDjJ8ctyCGbhCdio5QI84mN5KxHGoM6O9J67C258ih3377Cv+YOUPOyPbl0HVP0BRCs4TOFXav6Sz3knXoKWe5+85Ad0U4fN/TlWfOs/Bj5s/m2fG/DZXTKXLoPvmXzmR31TNL7UxPeT2c5W7ziEPP6rcZRTZnI7QOXeJPyl4ZH50uY0Aaj5IX1KFrpszUlkpoWGtIHXqoZZcSBs3t+fh2g/rCYbk8TBcYqX9P8Xx81QS2eUwp3cud7gjN7Uphv9hIkuT5hIZBfet8SCTJF2mS0gS+tIkUOSu9W3pIHXpK5GxoxKm2dKC0Pnx0SlUCPnkJTX9OjXPS91GHbkhICiingJRNUBXzagrPSWGtqe5D9529Hhpm1HngO/42RXlKSqe3+9AlkAoNX+fe2xJah56y10MyfqY43yA0LaKv8zHpkNZHihEnpXdUpFUCzZqfd12HPtZGBB8Yu5S/JBSSh9vK81bGCX4sSqo0H1M/h1zVxS/wow5+dJ1Db6Fu0Fb32eq4zUUNOpZnpJZ5qGUcLuUfOr7Q91K/U7o96MD6iK37l97vug5dt5mm9ihCvx/63lDPf6z+x/pO6XkAHXala877WPM01ncgV9soJMIJftS1PrquQ0cYqI4w0FTKv5RShlxBrpBmW60OKrXOpjYWWlznXdeh28KdrYFLi0LlWuDgx+rMQFnl8WywPmBcwbhaNa6QQzfq0KUcxRjPkVtDbq2EnEGuIFeQK/eelh7Wx4qHPrXHkPp9WOqw1GGpIwzaih5oZZySXgYddendruvQW83tYJHUtUjAD/ADxiKMxRb0AOrQHagvWaaln7cgPCE5cdABMAQYAgxb0QOtjNOFP8ihI4derE66h5yUdK+7zaiple5ax+VSoq7xgg7sBcBeAPteANShz7VJaY87tP/Q92pPJ4AOu9I1+TbWPI31ndJyCTogV7qM5ZKHXP2Uln+pf9ShW07ymkrpupjVehiotvlMXbzgB9IISCMgjVCjHkAd+rawE5Eky6jU8xqFZogyAx0AwSFyIxlfkCvIFeQKdegzGag1F1fruJDrrCt3CX6AHyVyyZCrduUKdeiGeSd5BKWfw+OAxwGPA+HcVvRAK+OU9HYvdKAO3aI9JeaXft6LcIGO9WWcqLu2jz76aFL3Vuv3aqvZsN1PH3r/tj6jpddh6f6Hrg/zTnPznm++631paYls/LAZi3p71TZmnofSYY4lZL5T7qd/61vfSo8++qhtChb+Tc2HTXZtkd0hdIRcfOMaqG+eUIfumLUQ4SrFlJjFNPU4pe+PudjBj7VppCFKYajS0oHGpxT5vXPPPZf27NmzMjxTeUKu3MagCej8pg2UbfxwyYNqbwP0EHkYa51LoGwaN7pcbd++nU466aQVY9OH7NzPfffdR3fddRfdeeeda16V5HOq56hDRx16sb0E2AuwrAdqmYfS4zA9b1Mpqu+7PPnHH3+cbrzxxhXl6RpvaTrG6n/od1yetwnKNn6oyIlpBKj/v/vuu2fefYzcDqXDZQT4+rOBsgni+v8zoCu54rabN29eMH70eTD7ed7znke33nrrCqCPSWfM/OvziDr0+WxMZVGZQl3LOFI9O9BhB/OpwnNj8OOhhx6ik08+mTZu3LjG8+Z/UxEUm4dpKk/XPI1BR8lIjxp/Ch22CIc+X+xlM3Db+KHeM8FL/bvy0EPHF/peqj5R7RUoS3So57pcmW19YM7PYtJAY8+Da32gDh116E6PKDf4uCzy3N8pvbhAxyLHTM/bBHTT87blgFPCm+uNH7Y9CDpHDjnkELr22mtXcug6P3QQs4GsMgZSjJqS/NBB2WWU6GCvy1WsMaAAXTdGbXNWWt/E9I86dNShz2Q0RmiGCHXJRa6PB3SERQZK8CPU8w7NtYfIZQk6hsh3qtwNpcMGUipsLvHD5rEqY2CoRz2UjlCj3hY2NzcBMgDb9nDYjAFbWzUWPVw/dD5S5SK2PXLoyKEXy/G2kHMKqeMFHXYjwZwX5NDD5skVdh8iZ/qu75Acuu6xmhvMuH2LOXQzImFWWfhy6Hp6wtyo2WQOfceOHftDLOFYSyHU4spl0eXqZ2o6QYc9nDuVhQx+hPMjdPe6lGtnD+vYY491sXz27+bGJ7UbObcHZxtEaFjaVmLlKymzlUlJ+si261v/hq+awLbBzNzlLn3ftj5KRgZsUQXfngE9bD4GP6T5Kv0cdeiWFVt60qX+ASLhIDKGMQp+hPEjVx36BRdcQBs2bPACug7qZmkS72TWn7vCquYu/NAca+jGMQls1QbClLpnHyhL/PAZP/rkx+orG6Dbdpvr38hhJEnnG4zBjzH0kY8fqEN3qA1JiEs/B4iEgchY8zTWd1qWq9DcuOTJn3LKKXTGGWcMqhfOnVO2bbzSvVgfv0LAVm1YG1r3nDsikaMOXYrA+DxlSf4lUB4akci9UVOio9Rz5NCRQ0cOfY7WrhzmkNxmSG7eZSSU+l6pfhUdQ+qe9RPlctQL20A0Jcdqhvf5/82NY655dYGtKinz5Wx9dOhyE5tDH6MOXdpL4cvd24w9s/TMPBwGdejLEsFyiDp0hzI3HfdSFpWp1Mf6jiuemev7ufopPU6pf9CxqiykUqYhdc9q/nPVC+f24Ez5MDeO+fSEbxd5SnhYD+vG5pTVeEvWoUuRmpjd5kPTIrnlqhU9gDr0EUq2UoUB4V6E322GR21yZcud2jxcG0hK9cK+UqzQjU+5QFQyAtXz0Jy8ej+UDp3vPqNhCD9Cc9k+4y4ldz/EGHDxI9UYQB26NrOpyia1PUAQINgCCIaCQ+6IUYn1IZ0xbqM1d71wrMc6BERD5SrmEBTuc8jGsdg9A9JhLDnq0KU9EiF7C1wX/LiOb7UZiZxekDZMutafyY+p1mksDiKHjhw6cujz1Yoc+raV41n1sK4L/M35KplDD60Xzp1Dt3msoXsRUs4ORw599eIe5NCXV2CI3OGkOJwUZxWWFjzCUE9Jfy/W4m3FMo/lVwkP3TZXUpjbxo+YHGuMZ1ty97VPrkLOXnddiCLxNdZDl/YqTO2hS3JZale/VGrXih6Ahw4PPcjyG7JrO8SiHNJvqMeY6/u5+hl73Ob3pqBDB1EzvG6eka3GV2p3uHTLm++7KXLqilyYJ7sN4U/sLvcxTopL2eUueaKlIjAspzgpTjNbUj2f1PaSZdfK81bGKfELdCza9NJ8lX4+FT+G1PemnOhVyoOTPGXbcxsd5nuuE+9C+ZV7Vz9Oijt65fyDISf3lV7HUv84Kc4SS5EmrfTz0MVcehyp/YMOgLp0DKgtlCmBlO+EtZhwfcguaP1s81hQt9Fhozd2Y6DeR+4NZraDZSQ9YK5zKXxdauOilN5JkSt9zqX5mPI5TopzJEemZEorwmMLj7WSa4pVzjBOhhknknK3yUsISLlOWKspp2yjw7U+hhoXtUQk9PWRslchFZRxUtyOHfuH5G5SckqlvleqX5cyL/W9Uv2CjmG7uMGPZckZMg+5cuj8feXJmqVXJXdB597lnmO3vr6OY3PooSfFSSe26fJgysWQagf9nIHYE/RwUtzq+sRJcfPVMbVHrr5fyzhSPW3QYQfB2MhArnnM1U+sXEiHjNiMTFtI1gXm3F7PdUphblP5m3XK5rGisTllfZ5bPilO8pR9+sp2OIwvMsFGRu4LdNT3pF39oXI11fqJjQzipDicFLey1koLbaxwxoLHWP2P9Z0e+CEdMmLz/HOfsKb4ZTtURTIGQi9iCaUjx4l3oUZDyklxUtg89qQ421rW+WED9NC9BSlpDD1iYfaDk+K0GSmtjKT+oXQXxVOar9LPwY/1yY9QT0+fndwnrOnhep/STrkFzLZ+YjboqUiDuqAm1JiN3TMQelLckL0PasxS3b2NH6GREJwU59cjqENHHfqg3GjIHoohOdeQfl3GQanvlep3PdCBHPrmldw/8zs0h27z+G1y2FsOXaLbttEQOfRlTcLysRJyL+2hle4fHuD69AAhV6uL2RcGxfrA+rB5/a71E7r3YYhc+fYWhEYmzPdS9cAQOmLmc6z+UYdu4UqqcKS2H4v5qeOU2oMOgEgJpQe5Ki9X0q1nKXz11aFLV/SWfN6DXKEO3WESSmBV+nkPwqVPben5Kt0/+FEeRGLkBfwoy4/YE/5i+JF7r0KM3Eh6IoaOksaFNE7Xc+TQkUNHDn2+il25cuTQ7WH9qeZlqu+6lP3Q8Qxtl3sctv5S6tDZA+dLXjZt2jTr2pfjVs/1E/mmmpepvpuTn6hDdyhz03EfajHFWnxjfccRmFgD7kPnAXTYQXDofKbyC/wAP2I82dDqBJtcce33aaedNgN0aVe9eUEN1od9pYeuX9Show59RYJChWYouMQaN0O/AzrCwCs3P0JLnWLDuRI/c9ORS+58JWWub/C/S/SWfO47djd0nhWgH3vssXTggQfSQQcdtEDuY489Rl/72tfW7P5XV8iGfsc3DyHH7rouxok9ByH08Bobz7nWPbZU0Wf04D503Ic+k4+SSiLGMyg9jtT+cyibMeZborMEHaEXscRsuJqCDpvilcZhe+6rQ89lNOT2aH0XmITILb/DALdz507au3cvMa9tgP7www+v3GrGbULr0EPlNvXEwM2bN88+FRJh0K+kTTEGcsgdcujIoSOHPl9JyKEPO+teKSLpHmw1v9K95C6lrdq3kuuU7levkY7YunYGM9PD1HPozGsGRQZw9Xva055GX/rSl1b+n5/nzqHb6FAftOX0dTpch9e49gLoJ8oN+e5dd91Fd955ZxY9jDp0wywaYonn9EBDLdCpxyl9H3QsCpY0X6Wfj8GP0GNeU44VHYOOEE80hF++3dy10hF78px0PKrvNricetPkR6yn7AJl02secnyw3ofvLoEccoc6dEucI2SxlixZqHWxx4b3QMf6AvXQw0hCN1zZQpAlQSBWviU9IYWva1wfvkNfXHskXB4m0xeTdpDmM+a574Ifm1xJZ/qHgnJK7j6HPKAO3aE1YoTH1kVq+xzMzWHxgY66QLlmfoTmxkM9+dZPvgvZYFbbOvcd+uKLrHDI2KZvYjcGpsp3SHuJDh/fJKM15X76HMYqcujIoWfJ3djOYK8xRzjkrHjQsaxqpHlADn1xntZrDl2XExs4co36tddeu4Jfklzlfu6SU7UXwMc36W6CIXsQsubQd+3atb9k+DjEYsrx/bG+4woD5vp+rn5Kj1PqH3TYQTB3WDd0nkPfk/jqex6aG5d2w59yyinEJU+2MOeQHKZ+Nee5555Le/bsWelaug/dpNfcwOXjpy/sK9FxwQUX0IYNG1zTvWb39RA61NWhZlve4R16talER8pu81IRRlskSSo9U0aIVJo5NMKxceNGyoGDqEMfoWQrVZnWFpYbqvRBR13h+9z8kJSd+p5Uh24CulQ6JG1oUoAu5e5DwccssXLNY8rGLFbwplGjvlMSRGPDzTZdIPEjlI5UvelqHxo218fpu8c+lzEwVK/qdKIOHXXo1nBqbk8yN3jkEH5bH5ISAR1uo0TyvG2ArsBJ9xJ1QJfAnPvUD+dI8ZBsu9Jt4GMDdJvc2DaEOV1ugw5blILbuuYjF4j6NvKF3HMewo9QOnJ4rLb5lvZw+DbyScZobJWAbgwM0Uem3CGHjhy6mBsdmsMa2s4FmlP1N9V3c89DaTpsuUkV1rUpcfOkLl8OU29vgprpIZ100kkLh5aY4dKjjz565bnNGJDOHg/NAUv1zLF05Lr32zRSpDpqV4RDjWdMOtR98tJemCH14F3k0Hfs2LGfGSx5JrU/h+fUdzgXHvnioS+xEZQx1keIB5fqYdo85pQcsK2tz4PkZzZQsfHD5/EPocO2BmwRjpjIALeXTjrzhZvHpmOMdIfv/IAhaZvQPRw5cBh16JZVMrXxMobyzSE80jyBjvVlZNlykz4Q0p9JdcC6Ujz55JOJc8yhYGZrq0A7FMyGhEOl8HUpOnzGnpQWGbMOPSV8LfEjdx16KCjH7kHQ5T6HvkQdusP1k8Cq9PMczB0DtKV5AB3rB9Rtu4ddkRXfiVkhSpEBPRaUU8BsSIRoKjp8YCdtXBy6B2FIHXpJftjo8EVepCoB0yg00zaq9Ax16AH1raG5v9D3XCBTS/taxpE6T6BjeQZrmYfS4yiZQ1dhbi6NM0vPSubQQ3O2tpyuVM9cig7fui1RR22e5a7Lma8OXTrTX6rj99G5bnPoqEO3K11f2EoK96Q8lzzeVp63Mk7J8wIdaetDKhGyKWVf2LdkDjPkZLcYechNhy6rsREOU86lq0ND69ClXdpS2sF3LkBKhNHnKetzofhpGiHqtjX1LurQK9hoh3Dv+gn32hapBNZDn0OuwuVKKhGyKe3Y0h+pZCulDl3fhBUD5kxXbjrUrNdUh27LAevrI2WDWex861KZYpzF8g116COAPZRuuNJNsYRD5zn0vZRFDDpWeS7N41j8kDxq2zhj6oCZjpJ16ArQpfkcgw6mNbVKYIgxELohzGZUD83Jp56cJl2M4+NnjPylVglIEY5YuUMdOurQi+V4S+dsx+p/rO+4QDbX93P1EzNOKWdbU+45JWc7Jh3S/Eu5e9fuegbRIWeRt5JDdxlnZth97PnLepY76tDhydpCz7GW4dDwdep3Srcfy5PtlQ6pNMmUG54HX5hWysmn7DL2eWa2cUonmZWiQ0ovjelhptyHLt3Ol8IPX9jcpatUhC8lTTDVngFFE+rQLdwtrVyl/gEiMLJ6MbKk0iQbndKZ6mYuecjhMCp83VIduguIQsK++ol9Zj8pOWC9r5rOBchdh26jU/2bVPJmzrdrQ2IOvY86dMcqkUC39PMczFUWp+RJlHwOOta3cSLVPduWn21D09RgNiQClbsOPQWUfWfip+xBsIFVqJEkGXu+KgGJH7nr0ENB2RYhskFAG5kAAAI4SURBVI3VZvxIkZeQ58ihI4eOHPp8pbhyzFPknqWzqoc8n4KOXDl05VEzqNV4dvgYOfReznJPOVs/9FyA3HXouvy5IkSuPQjm+/r/68ZPjvW5AXXoy9Nb2uMO7T/0PclCnfo56IBc8QxIOW9bBMcXLh2ySzslXGqeHR4j1+uhDn0IP2J2zcfMt87nWE9ZSjukRDhsxoAaq7QHITbCifvQKwJzFwjHMnVqMAcddYH5lPzIVYfuCzdLdeguD4n/PfQM+SFGf2w9c0k6chg1oWf1S3SEngswFMyZ1pC0jXS4jk9u9PnMZQzk0Nu4Dx33oY8SoYBRsrhcU5TVEHDJoSxsfYCOPMYb1gfWR471hRw6cujIoc9XEnLoi1e05sjpDcn1u8DNrBeW3qv9ea3zGztvoMNu1E0xLyse+tSWdur3YeHCws1h4cKTTbt3PXUdS+2xzrHOsc5vd2bRUIdumRpJqZR+DqUFpQWl5VZaWB9YH1gf9vWBOnSHrVMatKX+obSgtKC0AOqt6IFWxtm73kUOHTl05NCRQ5/NwBQ5vyE59lbGKeWiQUc9uWeWwx74gTp0hzI3vSPJssv1PFc/U+eCQYddWUGu7JIZKi+h700t/9L3QQfWhy4jueQBdeioQ1+Rq1xC5dyxUZnxJCld0FGX0gU/wI8SINiTXKEOHXXo1nBrbk8SOTbsCcCeAOwJaEUPtDJO0wlDDh059GK5ox5yUr3k1kDHKpjWIpe1jEPK9UvPQUc9ewH+f84NPv2naKbwAAAAAElFTkSuQmCC';
    toolkit.src = '/assets/images/tanks/toolkit.png';
    scaffolding.src = '/assets/images/tanks/scaffolding.png';
    boost.src = '/assets/images/tanks/boost.png';
    flashbang.src = '/assets/images/tanks/flashbang.png'
    iron_tank_base.src = 'https://' + window.location.hostname + '/iron-tank-base.png';
    iron_tank_base2.src = 'https://' + window.location.hostname + '/iron-tank-base2.png';
    iron_tank_top.src = 'https://' + window.location.hostname + '/iron-tank-top.png';
    diamond_tank_top.src = 'https://' + window.location.hostname + '/diamond-tank-top.png';
    diamond_tank_base.src = 'https://' + window.location.hostname + '/diamond-tank-base.png';
    diamond_tank_base2.src = 'https://' + window.location.hostname + '/diamond-tank-base2.png';
  }

  function updateBootProgress() {
    draw.clearRect(0, 0, 500, 500)
    draw.fillStyle = '#000000';
    draw.fillRect(0, 0, 500, 500);
    draw.textAlign = 'center';
    draw.fillStyle = '#ffffff';
    draw.fillText('Loading ' + Math.round(new Number(bootPercent.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0])) + '%', 250, 250);
  }
  var startM = new Menu(function() {
    draw.textAlign = 'left';
    draw.drawImage(gameplay1, 0, 0);
    draw.drawImage(button, 100, 300);
    draw.font = '40px starfont';
    draw.fillStyle = '#000000';
    draw.fillText('pixel', 110, 60);
    draw.fillText('Tanks', 170, 120);
    draw.font = '30px starfont';
    draw.fillStyle = '#000000';
    draw.fillText('Play', 200, 370);
  }, {
    click: function(e) {
      x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      x -= canvas.offsetLeft;
      y -= canvas.offsetTop;
      x /= window.resizer;
      y /= window.resizer;
      if (x > 100 && x < 400 && y > 300 && y < 400) {
        this.removeListeners();
        loading();
      }
    }
  }, document);

  function startScreen() {
    startM.draw();
    startM.addListeners();
  } // startScreen
  var userData;

  function loading() {
    if (sessionStorage.username == undefined) {
      var t = confirm('Do you want to play as Guest?');
      if (t) {
        userData = {
          username: 'Guest' + JSON.stringify(Math.random()),
          health: 200,
          coins: 0,
          level: 1,
          boosts: 0,
          blocks: 0,
          flashbangs: 0,
          toolkits: 0,
          class: 'normal',
          stealth: false,
          builder: false,
          tactical: false,
          summoner: false,
          steel: false,
          crystal: false,
          dark: false,
          light: false,
          fire1: true,
          fire2: false,
          fire3: false,
          cosmetic: '',
          cosmetics: [],
          xp: 0,
          rank: 1,
          crates: 0,
        };
        user = {};
        user.username = userData.username;
        mainMenu();
      } else {
        startScreen();
      }
    } else {
      user = {};
      user.username = sessionStorage.username;
      get(sessionStorage.username, function() {
        if (userData == undefined) {
          userData = {
            username: sessionStorage.username,
            health: 200,
            coins: 0,
            level: 1,
            blocks: 0,
            flashbangs: 0,
            boosts: 0,
            toolkits: 0,
            class: 'normal',
            stealth: false,
            builder: false,
            tactical: false,
            summoner: false,
            steel: false,
            crystal: false,
            dark: false,
            light: false,
            fire1: true,
            fire2: false,
            fire3: false,
            cosmetic: '',
            cosmetics: [],
            xp: 0,
            rank: 1,
            crates: 0,
          };
        }
        if (userData.cosmetics == undefined) {
          alert('Cosmetics are now enabled for your account!');
          userData.cosmetics = [];
          userData.crates = 0;
        }
        user.level = userData.level;
        user.coins = userData.coins;
        if (userData.xp == undefined || userData.rank == undefined) {
          saveStatus.innerHTML = 'Ranks and Xp [BETA] have been enabled for your account';
          userData.rank = 1;
          userData.xp = 0;
        }
        if (userData.class == undefined) {
          userData.class = 'normal';
        }
        mainMenu();
      });
    }
  }

  function levelReader(array) {
    Game.map = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
    Game.borders = [0, 1500, 0, 1500];
    for (l = 0; l < array.length; l++) {
      for (q = 0; q < array[l].split('').length; q++) {
        var p = array[l].split(''); // Block key: # = invincible, 1 = weak, 2 = strong, @ = player, A = Area, C = Condensed, W = Weak Tanks, I = normal T = turret, B = Invis Area, D = Invis Condensed, X = Invis Weak, L = loot create, U = PowerBullet Turret, V = Mega Bullet Turret
        if (p[q] == '#') {
          Game.map[l] += '1';
          wall(q, l);
        } else if (p[q] == '1') {
          Game.map[l] += '1';
          weak(q, l);
        } else if (p[q] == 'G') {
          Game.map[l] += '1';
          gold(q, l);
        } else if (p[q] == '2') {
          Game.map[l] += '1';
          strong(q, l);
        } else if (p[q] == '=') {
          Game.map[l] += '1';
          barrier(q, l);
        } else if (p[q] == 'I') {
          Game.map[l] += '0';
          createAi(q * 50, l * 50, false, 1, false, null, 'Eval');
        } else if (p[q] == 'W') {
          Game.map[l] += '0';
          createAi(q * 50, l * 50, false, 0, false, null, 'Eval');
        } else if (p[q] == 'A') {
          Game.map[l] += '0';
          createAi(q * 50, l * 50, false, 2, false, null, 'Eval');
        } else if (p[q] == 'C') {
          Game.map[l] += '0';
          createAi(q * 50, l * 50, false, 3, false, null, 'Eval');
        } else if (p[q] == 'T') {
          createAi(q * 50, l * 50, 'turret', 1, false, null, 'Eval');
        } else if (p[q] == '@') {
          Game.map[l] += '0';
          spawn(q, l);
        } else if (p[q] == ' ') {
          Game.map[l] += '0';
        }
      }
    }
  }

  function level(num, mo, m) {
    // remove event listener from all levelSelect screens
    if (num >= 10000) {
      if (m) {
        Game.borders = [0, 1500, 0, 1500];
        Game.map = [];
        var l = 0;
        while (l < 30) {
          var q = 0;
          while (q < 30) {
            Game.map[l] += '0';
            q++;
          }
          l++;
        }
        endlessRunner = window.setInterval(function() {
          if (Math.round(Math.random() * 3) >= 1) {
            var x = Math.round(Math.random() * 30);
            var y = Math.round(Math.random() * 30);
            weak(x, y, true);
            var map = Game.map[y].split('');
            map[x] = '1';
            Game.map[y] = map.join('');
            user.tank.scaffolding.push({
              x: x,
              y: y,
              type: 'weak'
            });
          } else {
            createAi((Math.round(Math.random() * 29)) * 50, (Math.round(Math.random() * 29)) * 50, false, Math.floor(Math.random() * 2) + 1, false, null, 'Eval');
          }
        }, 400);
      }
    }
    document.removeEventListener('keydown', levelSelectKeyDown);
    document.removeEventListener('click', levelSelectSupport);
    // levelSelect2();
    document.removeEventListener('keydown', levelSelectSupport2KeyDown);
    document.removeEventListener('click', levelSelect2Support);
    //levelSelect3();
    document.removeEventListener('keydown', levelSelect3KeyDown);
    document.removeEventListener('click', levelSelect3Support);
    //levelSelect4();
    document.removeEventListener('keydown', levelSelect4KeyDown);
    document.removeEventListener('click', levelSelect4Support);
    //multiplayer();
    document.removeEventListener('click', multiplayer2);
    // make viewport follow tank
    draw.setTransform(window.resizer, 0, 0, window.resizer, window.resizer * (-user.tank.x + 230), window.resizer * (-user.tank.y + 230));
    draw.fillStyle = '#000000';
    draw.fillRect(-1000, -1000, 3500, 3500);
    draw.clearRect(0, 0, 500, 500);
    draw.drawImage(floor, 0, 0, 1500, 1500);
    draw.setTransform(window.resizer, 0, 0, window.resizer, window.resizer * (-user.tank.x + 230), window.resizer * (-user.tank.y + 230));
    if (mo != 'n') {
      Game.coins = 0;
      Game.xp = 0;
      Game.foes = 0;
      Game.control();
      Game.i();
      user.tank.initialize();
      s = [];
      b = [];
      document.addEventListener('visibilitychange', Game.tabHandler);
      window.addEventListener('blur', Game.leaveWindow);
    }
    if (num == 1) {
      if (m) {
        levelReader(['        1                     ', '                  W        1W ', '  @   1   1   1            1  ', '                  W        1W ', '        1                     ', '==============================', '==============================', '==============================', '==============================', '==============================', '==============================', '==============================', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ']);
      }
    }
    if (num == 2) {
      if (m) {
        levelReader(['       I       ======         ', '               ======         ', '      111      ======         ', '               ======         ', '               ======         ', '     2   2     ======         ', '  2         2  ======         ', 'W 2    @    2 W======         ', '  2         2  ======         ', '     2   2     =======        ', '               =======        ', '               =======        ', '      111      =======        ', '               =======        ', '       I       =======        ', '======================        ', '======================        ', '======================        ', '======================        ', '======================        ', '======================        ', '            ====              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ']);
      }
    }
    if (num == 3) {
      if (m) {
        levelReader(['==============================', '===============================', '==============================', '===============================', '==============================', '=========           ==========', '=========           ==========', '=========           ==========', '=========   #   #   ==========', '=========   #   #   ==========', '====        #   #        =====', '====      W #   # W      =====', '====        #1 1#        =====', '====   ######   ######   =====', '====                     =====', '==== W    1   @   1    W =====', '====                     =====', '====   ######   ######   =====', '====        #1 1#        =====', '====      W #   # W      =====', '====        #   #        =====', '=========   #   #   ==========', '=========   #   #   ==========', '=========           ==========', '=========           ==========', '=========           ==========', '==============================', '==============================', '==============================', '==============================']);
      }
    }
    if (num == 4) {
      if (m) {
        levelReader(['2222==========================', '2  2    W          11    W    ', '2  2              1        111', '2222=========   ===========   ', '=============   ===========   ', '============= W ===========   ', '=============   ===========   ', '=============   =========== 1W', '=============   =========== 11', '=============   ===========   ', '=============   ===========   ', '=============   ===========   ', '=============###===========  1', '     2111       1       ===1  ', '@    2          1   WTTI===   ', '     2111       1       ===1  ', '=============111===========   ', '=============   =========== W ', '=============   ===========11 ', '=============   ===========   ', '=============   ===========   ', '#############  W===========   ', ' 2    2    2#1  =========== 1 ', '   W    2 W # 1 ===========   ', '2           #W  ===========W11', '   2 2    2 #   ===========11 ', '            #   =========== 1 ', ' 2    W 2 2     11     11    1', '                    1   1     ', ' 2  2              11         ']);
      }
    }
    if (num == 5) {
      if (m) {
        levelReader(['                              ', '                              ', '==========================    ', '==========================    ', '==========================    ', '==========================    ', '==========================    ', '==========================    ', '==========================    ', '=====   1   1   ==========    ', '=====     1  TT ==========    ', '=====  #######  ==========    ', '       #1   1#   T T======    ', '          #      I T======    ', ' #@       #      I#T======    ', '          #      IT ======    ', '       #1   1#     T======    ', '=====  #######  ==========    ', '=====     1     ==========    ', '=====   1   1   ==========    ', '==========================    ', '==========================    ', '==========================    ', '==========================    ', '==========================    ', '==========================    ', '==========================    ', '                              ', '                              ', '                              ']);
      }
    }
    if (num == 6) {
      if (m) {
        levelReader(['==============================', '==============================', '==============================', '==============================', '==============================', '==============================', '==============================', '=========11    #    ##========', '=========1  1        #========', '=========  I 1     A  ========', '========= 1  #   1    ========', '=========  1##   11   ========', '=========             ========', '=========1     @     1========', '=========             ========', '=========   11   ##1  ========', '=========    1   #  1 ========', '=========  A     1 I  ========', '=========#        1  1========', '=========##    #    11========', '==============================', '==============================', '===============================', '==============================', '===============================', '===============================', '===============================', '==============================', '==============================', '===============================']);
      }
    }
    if (num == 7) {
      if (m) {
        levelReader(['===========     ==============', '=========== A A ==============', '===========     ==============', '=========== ### ==============', '===========     ==============', '==========       =============', '========== #   # =============', '========== #   # =============', '==========       =============', '===========     ==============', '=========== ### ==============', '===========     ==============', '===========  @  ==============', '===========     ==============', '==============================', '==============================', '===============================', '===============================', '==============================', '==============================', '==============================', '==============================', '==============================', '==============================', '===============================', '==============================', '==============================', '==============================', '==============================', '==============================']);
      }
    }
    if (num == 8) {
      if (m) {
        levelReader(['==============================', '===============================', '==============================', '===============================', '==============================', '======               =========', '======    1     1    =========', '======    == I ==    =========', '======     =W1W=     =========', '====== 1= 111#111 =1 =========', '======  ==1     1==  ==========', '======   W1     1W   =========', '======  I1#  @  #1   =========', '======   W1     1W   =========', '======  ==1     1==  =========', '====== 1= 111#111 =1 =========', '======     =W1W=     ==========', '======    == I ==    =========', '======    1     1    =========', '======               =========', '==============================', '===============================', '===============================', '==============================', '===============================', '===============================', '===============================', '==============================', '===============================', '===============================']);
      }
    }
    if (num == 9) {
      if (m) {
        levelReader(['==============================', '===========       ============', '===========       ============', '===========### ###============', '============I   I=============', '=============# #==============', '=============   ==============', '=============   ==============', '=============   ==============', '=============   ===============', '=============   ===============', '=  I=======       =======I  ===', '= #  ======       ======  # ==', '=                           ==', '=   #         @         #   ==', '=                           ==', '= #  ======       ======  # ===', '=  I=======       =======I  ==', '=============   ==============', '=============   ==============', '=============   ==============', '=============   ==============', '=============   ==============', '=============# #==============', '============I   I=============', '===========### ###============', '===========       ============', '===========       ============', '==============================', '==============================']);
      }
    }
    if (num == 10) {
      if (m) {
        levelReader(['                             =', ' W W   W     W W     W   W W =', '              W              =', ' W  ========     ========  W =', '   ==========   ==========   =', '   ==========   ==========   =', '   ==========   ==========   =', ' W ====W ====   ==== W==== W =', '   = ===  ====   ====  ====   =', '   ====  ====   ====  ====   =', '   ====  ====   ====  ====   =', '   ====  ==== 1 ====  ====   =', '    ===11===     ===11===    =', ' W                         W =', '  W        1  @  1        W  =', ' W                         W =', '    ===11===     ===11===    =', '   ====  ==== 1 ====  ====   =', '   ====  ====   ====  ====   =', '   ====  ====   ====  ====   =', '   ====  ====   ====  ====   =', ' W ====W ====   ==== W==== W =', '   ==========   ==========   =', '   ==========   ==========   =', '   ==========   ==========   =', ' W  ========     ========  W =', '              W              =', ' W W   W     W W     W   W W =', '                             =', '===============================']);
      }
    }
    if (num == 11) {
      if (m) {
        levelReader(['                              ', '              =               ', '  =========================   ', '  =========================   ', ' ==========================   ', ' ==========================   ', ' ==========================   ', '===========================   ', '========W           W======   ', '======== #2#2#2#2#2# ======   ', ' ======= 2A       A2 =======  ', ' ======= # # # # # # =======  ', '======== 2         2 =======  ', '======== # # # # # # =======  ', '======== 2    @    2 =======  ', '======== # # # # # # =======  ', '======== 2         2 =======  ', '======== # # # # # # ======   ', '======== 2A       A2 ======   ', '======== #2#2#2#2#2# ======   ', '========W           W======   ', '===========================   ', '===========================   ', '===========================   ', '===========================   ', '===========================   ', '===========================   ', '                              ', '                              ', '                              ']);
      }
    }
    if (num == 12) {
      if (m) {
        levelReader(['                              ', '                              ', '     ==================       ', '    ====================      ', ' ========================     ', ' ==========================   ', '===========================   ', '===========     ===========   ', ' ========    C    =========   ', ' =======    222    ========   ', ' ======  22     22  ========  ', ' ======  22     22  ========  ', ' =====               =======  ', '======  2         2  =======  ', '====== C2    @    2C =======  ', ' =====  2         2  =======  ', ' =====               =======  ', ' ======  22     22  ========  ', ' ======  22     22  ========  ', ' =======    222    =========  ', '=========    C    ==========  ', '===========     ===========   ', ' = ========================   ', '  ========================    ', '   =======================    ', '     ===================      ', '     ==============           ', '                              ', '                              ', '                              ']);
      }
    }
    if (num == 13) {
      if (m) {
        levelReader([' ==                           ', '============                  ', '============                  ', '==============                ', '===============               ', '================              ', '=================             ', '        ==========            ', '         ==========           ', ' #  WC    =========           ', ' # W W     =========          ', ' #  W       ========          ', '      #      ========         ', '      # W     ========        ', '@     #W W  C ========        ', '      # W     ========        ', '      #      ========         ', ' #  W       =========         ', ' # W W     =========          ', ' #  WC    ==========          ', '         ==========           ', '        ===========           ', '==================            ', '================              ', '=================             ', '==================            ', '==================            ', '================              ', '====                          ', '                              ']);
      }
    }
    if (num == 14) {
      if (m) {
        levelReader(['                              ', '                              ', '                              ', '   =======================    ', '   =======================    ', '   =======================    ', '   =======================    ', '   =======================    ', '   =====     #C#     =====    ', '   =====     # #     =====    ', '   =====  #  # #  #  =====    ', '   =====     # #     =====    ', '   =====             =====    ', '   =====####     ####=====    ', '   =====C     @     C=====    ', '   =====####     ####=====    ', '   =====             =====    ', '   =====     # #     =====    ', '   =====  #  # #  #  =====    ', '   =====     # #     =====    ', '   =====     #C#     =====    ', '   =======================    ', '   =======================    ', '   =======================    ', '   =======================    ', '   =======================    ', '                              ', '                              ', '                              ', '                              ']);
      }
    }
    if (num == 15) {
      if (m) {
        levelReader(['CCCWW                         ', 'CCCWW     @                   ', 'CCCWW                         ', '######################   #####', '######################   #####', '######################   # # ##', '               #         #####', '               #         # # #', '               #         #   #', '   #####111#   #   ###########', '       #   #   #   ###########', '       #   #   #   1          ', '       #   #   #   1          ', '   #   #   #   #   1          ', '   #   #   #   ############   ', '   #   #   #   1              ', '   #   #   # W 1              ', '   #   #   #   1              ', '111#111#   #   ###############', '                   #          ', '                   # #      # ', '                   #    ##    ', '   #################          ', '   1               #          ', '   1               # #  ##  # ', '   1               #          ', '   #################          ', '                   1    ##    ', '                   1 #      # ', '                   1          ']);
      }
    }
    if (num == 16) {
      if (m) {
        levelReader(['==============================', '===============================', '===============================', '==============================', '====        =        =========', '====I## ### = ### ##I==========', '==== ## ### = ### ## ==========', '====        W        =========', '==== ## # ##### # ## =========', '====W   #   #   #   W=========', '======= ### # ### ============', '======= #       # ============', '======= # ## ## # ============', '===A      # C #      A========', '===A      #CCC#      A========', '======= # ##### # ============', '======= #   W   # ============', '======= # ##### # ============', '====   W    #    W   =========', '==== ## ### # ### ## =========', '====I #     @       I=========', '===== # # ##### # # ==========', '====    #   #   #    ==========', '==== ###### # ###### ==========', '====        W        =========', '==============================', '==============================', '==============================', '===============================', '==============================']);
      }
    }
    if (num == 17) {
      if (m) {
        levelReader(['                              ', ' =========================    ', ' ==========================   ', ' =========================    ', ' =========================    ', ' =========================    ', ' =========================    ', ' ======C           C======    ', ' ================== ======    ', ' ======C         C= ======    ', ' ====== ===== === = ======    ', ' ====== =C      = = ======    ', ' ====== = ===== = = ======    ', ' ====== = =  @  = = ======    ', '======= = = ===== = ======    ', ' ====== = =      C= ======    ', ' ====== = === ===== ======    ', ' ====== =C         C======    ', ' ====== ===== ============    ', ' ======C           C======    ', ' =========================    ', ' =========================    ', '==========================    ', '===========================   ', ' ==========================   ', '  =========================   ', '                              ', '                              ', '                              ', '                              ']);
      }
    }
    if (num == 18) {
      if (m) {
        levelReader(['       =================      ', '    =====================     ', '    ======================    ', '    ======================    ', '   ========       =========   ', '= ========         ========   ', '=========           =======   ', '========   ##AWA##   =======  ', '=======   #  #1#  #   ======  ', '======   #A       A#   =====  ', '=====   #    1#1    #   ======', '=====   #   #   #   #   ======', '=====   A# 1     1 #A   ======', '=====   W1 #  @  # 1W   ======', '=====   A# 1     1 #A   ======', '=====   #   #   #   #   ======', '=====   #    1#1    #   ======', '======   #A       A#   =======', '=======   #  #1#  #   ========', '========   ##AWA##   =========', ' ========           ==========', ' =========         ========== ', ' ==========       =========== ', '  =========================== ', '   ========================   ', '    ======================    ', '     ====================     ', '      ==================      ', '      =================       ', '                              ']);
      }
    }
    if (num == 19) {
      if (m) {
        levelReader(['                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '=========================     ', '=========================     ', '=========================     ', '=========================     ', '=========================     ', '=========================     ', '=========================     ', '           # CCC=========     ', ' ##   ##   # CWC=========     ', ' #     #     CWC=========     ', '    #      # CWC=========     ', '    #      # CCC=========     ', '    #    ===== ==========     ', '    #    ===== ==========     ', '    #    ===== ==========     ', '    #    ===== ==========     ', ' #     # ====   =========     ', ' ##   ##  222 @ =========     ', '         ====   =========     ']);
      }
    }
    if (num == 20) {
      if (m) {
        levelReader(['==============================', '==============================', '==============================', '========                     C', '========          ##        ##', '========                      =', '========   =======   ##22##   ', '========   ======= ###    ### ', '========WCW=======##        ##', '========   =======     ##     ', '========   =======  AA ## AA  ', '========121=======     ##     ', '========212=======     ##     ', 'A          ======= ###2  2### ', 'A    @     ======= C   ##   C ', 'A          =======  C      C  ', '================== # # ## # # ', '==================            ', '==================            ', '================== A A    A A ', '================== ####22#### ', '================== #        # ', '================== # ###### # ', '================== #   C    # =', '================== #C######C# ', '================== #    C   # ', '================== # ###### # ', '================== #        # ', '==================2####22####2', '================== C        C ']);
      }
    }
    if (num == 21) {
      if (m) {
        levelReader(['                              ', ' ====       = ===   A   === = ', ' =  = A = C = = =       = = = ', ' =  =   =   =I= = =====   = =A', ' ==== ===== =I= =   C    == =W', '    =   =   =I= = ===== ==  =A', '  @ = A = C = = =       =   = ', '    =       = ===   A   === = ', '                              ', '==============================', '==============================', '==============================', '==============================', '===============================', '===============================', '==============================', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ']);
      }
    }
    if (num == 22) {
      if (m) {
        levelReader(['                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '=======================       ', '=======================       ', '=======================       ', '=======================       ', '=======================       ', '=======================       ', 'A # C # A # C # A======       ', '    # # # # #    ======       ', '# ### # # # ### #======       ', '  #   # # #   #  ======       ', 'C## #       # ##C======       ', '     ### ###     ======       ', '#### #     # ####======       ', '     # # # #     ======       ', 'A###    @    ###A======       ', '     # # # #     ======       ', '#### #     # ####======       ', '     ### ###     ======       ', 'C## #       # ##C======       ', '  #   # # #   #  ======       ', '# ### # # # ### #======       ', '    # # # # #    ======       ', 'A # C # A # C # A======       ']);
      }
    }
    if (num == 23) {
      if (m) {
        levelReader(['               ===============', '               ===============', '  ###  #  ###  ===============', '  #         #  ===============', '  #         #  ================', '     # # #     AAAAA==========', '               AAAAA==========', '  #  # @ #  #  AAAAA==========', '               AAAAA==========', '     # # #     AAAAA==========', '  #         #  ===============', '  #         #  ================', '  ###  #  ###  ===============', '               ===============', '               ===============', '==============================', '===============================', '==============================', '===============================', '===============================', '==============================', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ']);
      }
    }
    if (num == 24) {
      if (m) {
        levelReader(['                              ', '    ========                  ', '   =======================    ', '   =======================    ', '   =======================    ', '   =======================    ', '   =======================    ', '   =======================    ', '   ======I I I I I I======    ', '   ====== #1#1#1#1# ======    ', '   ======I1       1I======    ', '   ====== # # # # # ======    ', '   ======I1       1I======    ', '   ====== # # # # # ======    ', ' ========I1   @   1I======    ', '========= # # # # # ======    ', '   ======I1       1I======    ', '   ====== # # # # # ======    ', '   ======I1       1I======    ', '   ====== #1#1#1#1# ======    ', '   ======I I I I I I======    ', '   =======================    ', '   ========================   ', '   ========================   ', '   =======================    ', '   =======================    ', '   =======================    ', '       =                      ', '                              ', '                              ']);
      }
    }
    if (num == 25) {
      if (m) {
        levelReader(['===============================', '==============================', '==============================', '===============================', '==============================', '==============================', '==============================', '===============================', '==============================', '==============================', '==============================', '==============================', '==============================', '==============================', '==============================', '=======    1WIIIW1    ========', '======= # ##     ## # ========', '=======       #       ========', '======= #           # ========', '=======1#  #     #  #1========', '=======W             W========', '=======I             I========', '=======I #    @    # I========', '=======I             I========', '=======W             W========', '=======1#  #     #  #1========', '======= #           # ========', '=======       #       ========', '======= # ##     ## # ========', '=======    1WIIIW1    ========']);
      }
    }
    if (num == 26) {
      if (m) {
        levelReader(['                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '=======================       ', '=======================       ', '=======================       ', '=======================       ', '=======================       ', '=======================       ', '       1W1       ======       ', ' ===   ===   === ======       ', ' =             = ======       ', ' = =         = = ======       ', '                 ======       ', '     C I C I     ======       ', '                 ======       ', '1=   I C I C   =1======       ', 'W=             =W======       ', '1=   I C I C   =1======       ', '                 ======       ', '     C I C I     ======       ', '                 ======       ', ' = =         = = ======       ', ' =             = ======       ', ' ===   ===   === ======       ', '       1@1       ======       ']);
      }
    }
    if (num == 27) {
      if (m) {
        levelReader(['               ======         ', ' @   W   W     ======         ', '               ======         ', '   #   #   #   ======         ', '               ======         ', ' W   W   W     ======         ', '             I ======         ', '   #   #   #   ======         ', '               ======         ', ' I   I   I   I ======         ', '               ======         ', '   #   #   #   ======         ', '            I  ======         ', '     W   W    =======         ', '             ========         ', '=====================         ', '=====================         ', '=====================         ', '=====================         ', '=====================         ', '=====================         ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              '], m);
      }
    }
    if (num == 28) {
      if (m) {
        levelReader(['                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '     ===                      ', '======  ==    =               ', '================              ', '================              ', '================              ', '================              ', '================              ', '==================            ', '          A=======            ', '           ======             ', '  ====     ======             ', '           ======             ', '           ======             ', 'AAA  =  =  ======             ', '        =  ======             ', '        =  ======             ', '     A  =  ======             ', '     A     ======             ', '@    A     ======             '], m);
      }
    }
    if (num == 29) {
      if (m) {
        levelReader(['==============================', '==============================', '==============================', '==============================', '===============================', '==============================', '==========WWWWWWWWW===========', '=========           ==========', '========             =========', '======= I#  #   #  #I ========', '======  #           #  =======', '=====I                 I=======', '=====I     #  #  #     I======', '=====I  #           #  I======', '=====I                 I======', '=====I     #  @  #     I======', '=====I                 I======', '=====I  #           #  I======', '=====I     #  #  #     I======', '=====I                 I======', '======  #           #  =======', '======= I#  #   #  #I ========', '========             =========', '=========           ==========', '==========WWWWWWWWW===========', '==============================', '==============================', '==============================', '==============================', '==============================']);
      }
    }
    if (num == 30) {
      if (m) {
        levelReader(['                   =          ', ' @                 =          ', '                   =          ', '   ============    =   ====   ', '             C=           =   ', '             C=      C    =   ', '             C=           =CCC', '   =================   =======', '                              ', '        C            C        ', '                              ', '=================   ===========', '                          =CCC', '        C               C =   ', '                          =   ', '   ====================   =   ', '            =C            =   ', '        C   =C            =   ', '            =C            =   ', '=========   =======   =       ', 'C           =         = C     ', 'C         C =         =       ', 'C           =      =  =====   ', '====   ======   =             ', 'CCC=   =        =             ', '   =   =        =             ', '   =   =        =====   =   ==', '            =   =C      =    C', '     C      =   =C      =    C', '            =CCC=C      =    C']);
      }
    }
    if (num == 31) {
      if (m) {
        levelReader(['===============================', '===============================', '==============================', '==============================', '==============================', '==========       =============', '=======             ==========', '======     I # C     =========', '=====    #       #    ========', '====   C           I   =======', '====         W         =======', '====  #             #  =======', '===       W  W  W       ======', '===  I               C  ======', '===                     ======', '===  #  W W  @  W W  #  ======', '===                     ======', '===  C               I  ======', '===       W  W  W       ======', '====  #             #  =======', '====         W         =======', '====   I           C   =======', '=====    #       #    ========', '======     C # I     =========', '=======             ==========', '=========         =============', '===============================', '==============================', '==============================', '==============================']);
      }
    }
    if (num == 32) {
      if (m) {
        levelReader(['                              ', ' @11=11A11=11A11=11A11=11A11= ', '    1                       1 ', '    1                       1 ', ' =  C  =  C  =11C11=  C11=  C ', ' 1     1  1     1        1  1 ', ' 1     1  1     1        1  1 ', ' A11=11A11=11A11=11A11=11A11= ', '                              ', '==============================', '==============================', '==============================', '==============================', '==============================', '==============================', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ']);
      }
    }
    if (num == 33) {
      if (m) {
        levelReader(['         # #####  #          I', ' @    #          #          I ', '   #    ## #####           I =', '  #      #                I ==', '    #  #   ####          I ===', '     #   # #            I ====', ' #         # #         I =====', '    #  ### #          I ======', '  #    #             I =======', '# ## # # #          I ========', '                   I =========', '# # ####          I ==========', '# # #            I ===========', '# # # #         I ============', '# # #          I ============ ', '# #           I ============= ', '             I =============  ', ' #          I =============   ', '#          I ============     ', '          I ============      ', '         I ============       ', '        I ============        ', '       I ============         ', '      I ============          ', '     I ============           ', '    I ============            ', '   I ============             ', '  I ============              ', ' I =============              ', 'I ============                ']);
      }
    }
    if (num == 34) {
      if (m) {
        levelReader(['                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '   =====       = ===          ', '====================          ', '====================          ', '====================          ', '====================          ', '====================          ', '====================          ', 'C   A   A   C=======          ', ' =  =   =  = =======          ', '             =======          ', '   I     I   =======          ', 'A=  =   =  =A=======          ', '             =======          ', '      @      =======          ', '             =======          ', 'A=  =   =  =A========         ', '   I     I   ========         ', '             ========         ', ' =  =   =  = ========         ', 'C   A   A   C=======          ']);
      }
    }
    if (num == 35) {
      if (m) {
        levelReader(['                              ', '                              ', '                =======       ', '              ==      ==      ', '            ==      U  ==     ', '            =           =     ', '           =     I      =     ', '          =          22# =    ', '          =   W      22## =   ', '       ==== #     ### 2T# =   ', '     ===    ## #### ##22# =   ', '    ==       ###       2#  =  ', '    =              @    #  =  ', '   ==      ####    W #     =  ', '   =           #########   =  ', '   =    U                  =  ', '   =                       =  ', '   =       I      I        =  ', '   =                       =  ', '   =   ####                =  ', '   =  ##  2   I      I     =  ', '   =  #  A2                =  ', '    = #2222  WWWWWW       =   ', '    =                     =   ', '    =======================   ', '     =                        ', '                              ', '                  =           ', '                              ', '                              ']);
      }
    }
    if (num == 36) {
      if (m) {
        levelReader(['=         =======             ', ' =   T   = ======             ', '    ===    ======             ', '           ======             ', '  =     =  ======             ', ' T=  @  =T ======             ', '  =     =  ======             ', '           ======             ', '    ===    ======             ', ' =   T   = ======             ', '=         =======             ', '=================             ', '=================             ', '=================             ', '=================             ', '=================             ', '=================             ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ']);
      }
    }
    if (num == 37) {
      if (m) {
        levelReader(['                              ', ' @  #     T     I     T     I ', '    #                         ', '   ===   ===   ===   ===   ===', ' ##===## ===## ===## === ##===', '   ===   ===   ===   ===   ===', '                              ', ' #  T  #  A  #  T  #  A     T ', ' #     #     #     #          ', '===   ===   ===   ===   ===   ', '===## ===## ===## === ##===## ', '===   ===   ===   ===   ===   ', '       #     #     #     #    ', ' I  #  #  T  #  I  #  T  #  I ', '    #                         ', '==============================', '==============================', '==============================', '==============================', '===============================', '==============================', '      =====                   ', '       ==                     ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              '], m);
      }
    }
    if (num == 38) {
      if (m) {
        levelReader(['                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '====================          ', '====================          ', '====================          ', '====================          ', '====================          ', '====================          ', '     =       =======          ', '     =     = =======          ', '     =T=T=T= =======          ', '  @          =======          ', '     =T=T=T= =======          ', '     =     = =======          ', '     =       =======          '], m);
      }
    }
    if (num == 39) {
      if (m) {
        levelReader(['                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '=====================         ', '=====================         ', '=====================         ', '=====================         ', '=====================         ', '=====================         ', '1 1 1 1 1 1 1 1======         ', ' # C # I # C # ======         ', '1 1 1 1 1 1 1 1======         ', ' C # I # C # I ======         ', '1 1 1 1 1 1 1 1======         ', ' # I # C # I # ======         ', '1 1 1 1 1 1 1 1======         ', ' @ # C # I # C ======         ', '  1 1 1 1 1 1  ======         '], m);
      }
    }
    if (num == 40) {
      if (m) {
        levelReader(['                 ======       ', '                 ======       ', '  =    AAA    =  ======       ', ' A=           =A ======       ', ' A=   22222   =A ======       ', '  =           =  ======       ', '                 ======       ', '                 ======       ', '========2==============       ', '                 ======       ', ' C   C     C   C ======       ', ' W  W  W W  W  W ======       ', '                 ======       ', '                 ======       ', '======22222============       ', '                 ======       ', '                 ======       ', ' I      I      I ======       ', '                 ======       ', '                 ======       ', '   ===========   ======       ', '   2         2   ======       ', ' I 2         2 I ======       ', '   2  W   W  2   ======       ', '====         ==========       ', '                 ======       ', '  W           W  ======       ', '      =====      ======       ', '        @        ======       ', '                 ======       ']);
      }
    }
    if (num == 41) {
      if (m) {
        levelReader(['===#=======#      #=======#===', '=W ##======# IIII #======## W=', '= W ##=====#  TT  #=====## W =', '#    ##====#      #====##    #', '##    ##===#      #===##    ##', '=##    #####      #####    ##=', '==##                      ##==', '===##                    ##===', '====#                    #====', '====#                    #====', '====#                    #====', '#####                    #####', '                              ', ' I                          I ', '  C                        C  ', '  C           @            C  ', ' I                          I ', '                              ', '#####                    #####', '====#                    #====', '====#                    #====', '====#                    #====', '===##                    ##===', '==##                      ##==', '=##    #####      #####    ##=', '##    ##===#      #===##    ##', '#    ##====#      #====##    #', '= W ##=====#  TT  #=====## W =', '=W ##======# IIII #======## W=', '===#=======#      #=======#===']);
      }
    }
    if (num == 42) {
      if (m) {
        levelReader(['CCCC        =   =             ', 'CCCC        =   =             ', 'CCCC               =========  ', 'CCCC          =               ', '              =               ', '           ======   =  =   =  ', '           =======  =  = = =  ', '           ======   =  =   =  ', '        @  ======  ==  = = =  ', '           ======   =  =   =  ', '            =====   =  = = =  ', '     =====   ====   =  =   =  ', '==   ======   ===   =  = = =  ', '     =======   ==   =  =   =  ', '   ==========   =   =  = = =  ', '     =========      =  =   =  ', '==   ==========     =  = = =  ', '      =             =  =   =  ', '                    =  = = =  ', '  =     =              =   =  ', '  =  ==============    = = =  ', '  =                    =   =  ', '  =                        =  ', '  =  =================     =  ', '  =                        =  ', '  =   = = = = = = = =      =  ', '  =                        =  ', '  =  =======================  ', '                              ', '                              '], m);
      }
    }
    if (num == 43) {
      if (m) {
        levelReader(['=II                        1CA', '==I                        1 C', '===                        111', '===                T          ', '===                           ', '===                           ', '==2                           ', '=W2     === = ==== =  = ===   ', '222     =   = =    =  =  =    ', '        ==  = = == ====  = 222', '        =   = =  = =  =  = 2W=', '        =   = ==== =  =  = 2==', '                           ===', '                           ===', '               @           ===', '                            ==', '                             =', '  === === =   = === === ===   ', '  =   = = =   = = = = = =  =  ', '  =   = = = = = === === =  =  ', '  =   = = = = = = = ==  =  =  ', '  === === ===== = = = = ===   ', '                              ', '                              ', '                              ', '                             T', '                              ', 'II====222                  111', 'I======W2                  1 C', '========2                  1CA']);
      }
    }
    if (num == 44) {
      if (m) {
        levelReader(['=II                        1CA', '==I                        1 C', '===                        111', '===                T          ', '===                           ', '===                           ', '==2                           ', '=W2     === = ==== =  = ===   ', '222     =   = =    =  =  =    ', '        ==  = = == ====  = 222', '        =   = =  = =  =  = 2W=', '        =   = ==== =  =  = 2==', '                           ===', '                           ===', '               @           ===', '                            ==', '                             =', '  === === =   = === === ===   ', '  =   = = =   = = = = = =  =  ', '  =   = = = = = === === =  =  ', '  =   = = = = = = = ==  =  =  ', '  === === ===== = = = = ===   ', '                              ', '                              ', '                              ', '                             T', '                              ', 'II====222                  111', 'I======W2                  1 C', '========2                  1CA']);
      }
    }
    if (num == 45) {
      if (m) {
        levelReader(['##############################', '#                            #', '#                            #', '#  #########        #######  #', '#  #===#  2          2  #=#  #', '#  #==# @ 2          2 A ##  #', '#  #=#2   211      112 A 2#  #', '#  ##122222W1      1W22222#  #', '#  #I1   1WW1      1WW1   #  #', '#  #11   1111      1111   #  #', '#  #                      #  #', '#                            #', '#                            #', '#                            #', '#                            #', '#                            #', '#                            #', '#                            #', '#                            #', '#  #                      #  #', '#  #11                    #  #', '#  #I1    1111    1111    #  #', '#  ##1    1WW1    1WW1    #  #', '#  #=# 22222W1    1W22222 #  #', '#  #==#2 A 211    112 A 2##  ##', '#  #===# A 2        2 A #=#  ##', '#  ########2        2######  #', '#                            #', '#                            #', '##############################']);
      }
    }
    if (num == 46) {
      if (m) {
        levelReader(['C C C C C C C C C C C C C C C ', ' C C C C C C C C C C C C C C C', 'C                           C ', ' C   ========    ========    C', 'C                           C ', ' C =                      =  C', 'C  =          ==          = C ', '222=   =    22  22    =   =  C', '222=   =    2    2    =   = C ', '222=   =   =      =   =   =  C', '222=   =   =      =   =   = C ', '222=   =   =      =   =   =  C', '222=   =   =      =   =   = C ', '      2=2  =222222=  2=2     C', '     222222=22AA22=222222   C ', '     222222=22AA22=222222    C', '      2=2  =222222=  2=2    C ', '   =   =   =      =   =   =  C', '   =   =   =      =   =   = C ', '   =   =   =      =   =   =  C', '   =   =   =      =   =   = C ', '   =   =    2    2    =   =  C', '   =   =    22  22    =   = C ', '   =          ==          =  C', '   =                      = C ', '                             C', '     ========    ========   C ', '                 222222      C', '                 222222 C C C ', '@                222222C C C C'], m);
      }
    }
    if (num == 47) {
      if (m) {
        levelReader(['==============================', '==============================', '==============================', '==============================', '==============================', '==============================', '==============================', '==============================', '========           A==========', '=======  ===========  ========', '======= A    ===    A  ========', '======A=      A      = =======', '====== =    A   A    = =======', '====== =             = ========', '====== =  A       A  = ========', '====== ==           == ========', '====== ==A    @    A== ========', '====== ==           == =======', '====== =  A       A  = =======', '====== =             = =======', '====== =    A   A    = =======', '====== =      A      =A=======', '======  A    ===    A ========', '=======  ===========  ========', '=========A           =========', '==============================', '==============================', '==============================', '==============================', '=============================='], m);
      }
    }
    if (num == 48) {
      if (m) {
        levelReader(['IIIIIIIIIIIIIIIIIIIIIIIIIIIIII', 'I                            I', 'I                            I', 'I                            I', 'I                            I', 'I             ##             I', 'I         #        #         I', 'I                            I', 'I           ##  ##           I', 'I        #          #        I', 'I     #                #     I', 'I          #  ##  #          I', 'I       #            #       I', 'I       #            #       I', 'I    #     #  ##  #     #    I', 'I    #     #  ##  #     #    I', 'I       #     @      #       I', 'I       #            #       I', 'I          #  ##  #          I', 'I     #                #     I', 'I        #          #        I', 'I           ##  ##           I', 'I                            I', 'I         #        #         I', 'I             ##             I', 'I                            I', 'I                            I', 'I                            I', 'I                            I', 'IIIIIIIIIIIIIIIIIIIIIIIIIIIIII'], m);
      }
    }
    if (num == 'beta-level') {
      if (m) {
        eval(betaLevel);
      }
    }
    if (num == 'multiplayer') {
      if (mo != 'n') {
        if (user.tank.team == 'red') {
          spawn(1, -10);
        } else {
          spawn(-2, 9);
        }
      }
      var l = 0;
      while (l < pt.length) {
        if (pt[l].ded != true) {
          if (ai_check(pt[l].x, pt[l].y)) {
            if (pt[l].shields > 0) {
              pt[l].shields -= 1;
            } else if (pt[l].immune) {} else {
              draw.fillStyle = '#FF0000';
              draw.fillRect(pt[l].x, pt[l].y, 40, 40);
              pt[l].health -= 20;
              if (pt[l].health <= 0) {
                pt[l].ded = true;
                if (pt[l].team == 'red') {
                  pt[l].x = 50;
                  pt[l].y = -500;
                } else {
                  pt[l].x = -100;
                  pt[l].y = -500;
                }
                window.setTimeout(function(l) {
                  pt[l].ded = false;
                  pt[l].health = pt[l].maxHealth;
                }, 10000, l);
              }
            }
          }
          if (pt[l].invis) {
            if (pt[l].username != user.username) {
              draw.globalAlpha = 0;
            } else {
              draw.globalAlpha = .5;
            }
          }
          if (pt[l].leftright == true) {
            draw.translate(pt[l].x + 20, pt[l].y + 20)
            draw.rotate(90 * Math.PI / 180);
            if (pt[l].base > 2) {
              if (pt[l].material == 'normal') {
                draw.drawImage(tank_base_png, -20, -20);
              } else if (pt[l].material == 'iron') {
                draw.drawImage(iron_tank_base, -20, -20);
              } else if (pt[l].material == 'diamond') {
                draw.drawImage(diamond_tank_base, -20, -20)
              }
            } else {
              if (pt[l].material == 'normal') {
                draw.drawImage(tank_base2, -20, -20);
              } else if (pt[l].material == 'iron') {
                draw.drawImae(iron_tank_base2, -20, -20);
              } else if (pt[l].material == 'diamond') {
                draw.drawImage(diamond_tank_base2, -20, -20);
              }
            }
            draw.rotate(-90 * Math.PI / 180);
            draw.translate((-pt[l].x - 20), (-pt[l].y - 20));
          } else {
            if (pt[l].base > 2) {
              if (pt[l].material == 'normal') {
                draw.drawImage(tank_base_png, pt[l].x, pt[l].y);
              } else if (pt[l].material == 'iron') {
                draw.drawImage(iron_tank_base, pt[l].x, pt[l].y);
              } else if (pt[l].material == 'diamond') {
                draw.drawImage(diamond_tank_base, pt[l].x, pt[l].y);
              }
            } else {
              if (pt[l].material == 'normal') {
                draw.drawImage(tank_base2, pt[l].x, pt[l].y);
              } else if (pt[l].material == 'iron') {
                draw.drawImage(iron_tank_base2, pt[l].x, pt[l].y);
              } else if (pt[l].material == 'diamond') {
                draw.drawImage(diamond_tank_base2, pt[l].x, pt[l].y);
              }
            }
          }
          draw.translate(pt[l].x + 20, pt[l].y + 20);
          draw.rotate(pt[l].rotation * Math.PI / 180);
          if (pt[l].material == 'normal') {
            draw.drawImage(tank_top_png, -20, -20 + pt[l].pushback);
          } else if (pt[l].material == 'iron') {
            draw.drawImage(iron_tank_top, -20, -20 + pt[l].pushback)
          } else if (pt[l].material == 'diamond') {
            draw.drawImage(diamond_tank_top, -20, -20 + pt[l].pushback);
          }
          if (pt[l].pushback != 0) {
            pt[l].pushback += 1;
          }
          draw.rotate(-(pt[l].rotation * Math.PI / 180));
          draw.translate(-pt[l].x - 20, -pt[l].y - 20);
          // Health Bar!
          draw.fillStyle = '#000000';
          draw.fillRect(pt[l].x, pt[l].y + 50, 40, 5);
          draw.fillStyle = '#90EE90';
          draw.fillRect(pt[l].x + 2, pt[l].y + 51, 36 * pt[l].health / userData.health, 3);
          draw.font = '20px starfont';
          draw.fillStyle = '#FFDF00';
          draw.fillText(pt[l].username, pt[l].x + 20 - pt[l].username.length / 2 * 13, pt[l].y - 25);
          if (pt[l].shields != 0) {
            draw.strokeStyle = '#7DF9FF';
            draw.globalAlpha = .2;
            draw.lineWidth = 5;
            draw.beginPath();
            draw.arc(pt[l].x + 20, pt[l].y + 20, 33, 0, Math.PI * 2);
            draw.fill();
            draw.globalAlpha = 1;
          }
          if (pt[l].invis) {
            draw.globalAlpha = 1;
          }
        }
        l++;
      }
      levelReader([' 2    2  2  2  222B#', '   2     2  2  #####', ' 2  22  2###########', '        2#2   2   2#', '  2  2 2 #  2   2  #', '2  2 2   #11111#1 1#', '        2#11111#1 1#', '  2 2    211 11#1 1#', '2    2  2#11111#1 1#', '22222#####11111#1 1#', '#1 1#11111#####22222', '#1 1#11111#2  2    2', '#1 1#11 112    2 2  ', '#1 1#11111#2        ', '#1 1#11111#   2 2  2', '#  2   2  # 2 2  2  ', '#2   2   2#2        ', '###########2  22  2 ', '#####  2  2     2   ', '#R222  2  2  2    2 '], m, true, [-500, 500, -500, 500]);
      user.host.send();
    }
    draw.globalAlpha = .5;
    draw.font = '20px starfont';
    draw.globalAlpha = 1;
    if (user.tank != undefined) {
      user.tank.draw();
      user.tank.check();
    }
    var l = 0;
    while (l < ai.length) {
      ai[l].draw();
      ai[l].check();
      l++;
    }
    var l = 0;
    while (l < b.length) {
      b[l].draw();
      var results = ai_check(b[l].x * 50, b[l].y * 50, true);
      if (b[l].type != 'void' && b[l].type != 'wall') {
        if (results[0]) {
          draw.fillStyle = '#FF0000';
          draw.fillRect(b[l].x * 50, b[l].y * 50, 50, 50);
          b[l].health -= results[1]
          if (b[l].health <= 0) {
            if (Game.level == 'multiplayer') {
              let isScaffolding = false;
              var t = 0;
              while (t < b.length) {
                var q = 0;
                while (q < user.tank.scaffolding.length) {
                  if (b[t].x == user.tank.scaffolding[q].x) {
                    if (b[l].y == user.tank.scaffolding[q].y) {
                      isScaffolding = true;
                    }
                  }
                  q++;
                }
                t++;
              }
              if (!isScaffolding) {
                var strand = user.host.blockData[b[l].y + 10].split('');
                strand[b[l].x + 10] = ' ';
                user.host.blockData[b[l].y + 10] = strand.join('');
              } else {
                var q = 0;
                while (q < user.tank.scaffolding.length) {
                  if (b[l].y == user.tank.scaffolding[q].y) {
                    if (b[l].x == user.tank.scaffolding[q].x) {
                      user.tank.scaffolding.splice(q, 1);
                    }
                  }
                  q++;
                }
              }
            }
            var q = 0;
            while (q < user.tank.scaffolding.length) {
              if (b[l].y == user.tank.scaffolding[q].y) {
                if (b[l].x == user.tank.scaffolding[q].x) {
                  user.tank.scaffolding.splice(q, 1);
                }
              }
              q++;
            }
            b[l].destroy(l);
            l--; // class Block.destroy function removes a block. This prevents the next block from glitching out during this frame.
          }
        }
      }
      l++;
    }
    var l = 0;
    while (l < s.length) {
      s[l].draw();
      l++;
    }
    // Additional Things
    draw.globalAlpha = .4;
    draw.font = '15px starfont'
    draw.setTransform(window.resizer, 0, 0, window.resizer, 0, 0);
    draw.drawImage(tank_ui['tank_main_singleplayer'].image, 0, 0);
    draw.fillStyle = '#ffffff';
    if (user.tank.CanBoost) draw.globalAlpha = .8;
    draw.fillRect(100, 450, 50, 50);
    draw.globalAlpha = .4;
    if (user.tank.CanToolkit) draw.globalAlpha = .8;
    draw.fillRect(183, 450, 50, 50);
    draw.globalAlpha = .4;
    if (user.tank.canPlaceScaffolding) draw.globalAlpha = .8;
    draw.fillRect(266, 450, 50, 50);
    draw.globalAlpha = .4;
    if (user.tank.canFireFlashbang) draw.globalAlpha = .8;
    draw.fillRect(349, 450, 50, 50);
    draw.globalAlpha = 1;
    draw.drawImage(boost, 100, 450);
    draw.drawImage(toolkit, 183, 450);
    draw.drawImage(scaffolding, 266, 450);
    draw.drawImage(flashbang, 349, 450);
    draw.fillStyle = '#ffffff';
    draw.fillText(userData.boosts, 135, 470);
    draw.fillText(userData.toolkits, 183 + 35, 470);
    draw.fillText(userData.blocks, 266 + 35, 470);
    draw.fillText(userData.flashbangs, 349 + 35, 470);
    draw.drawImage(coins, 0, 0);
    draw.fillStyle = '#ffffff';
    draw.fillText(Game.coins, 70, 40);
    if (user.tank.toolkitAnimation) {
      draw.globalAlpha = .5;
      toolkitAnimation.drawFrame();
      draw.globalAlpha = 1;
    }
    draw.setTransform(window.resizer, 0, 0, window.resizer, window.resizer * (-user.tank.x + 230), window.resizer * (-user.tank.y + 230));
    draw.font = '20px starfont';
    draw.fillStyle = '#FFDF00';
    draw.fillText(user.username, user.tank.x + 20 - user.username.length / 2 * 13, user.tank.y - 25);
  }
  var defeatSupport = false;

  function defeat() {
    draw.setTransform(window.resizer, 0, 0, window.resizer, 0, 0);
    draw.fillStyle = '#556B2F';
    draw.fillRect(50, 50, 400, 400);
    draw.font = '60px starfont';
    draw.fillStyle = '#ffffff';
    draw.fillText('DEFEAT!', 120, 120);
    draw.font = '20px starfont';
    draw.fillText('No Stats Updated', 140, 250);
    document.addEventListener('keydown', defeatHelper);
    setTimeout(function() {
      document.removeEventListener('keydown', defeatHelper);
      if (!defeatSupport) {
        mainMenu();
      } else {
        window.setTimeout(function() {
          defeatSupport = false;
        }, 20);
      }
    }, 3000);
    Game.endGame();
  }

  function defeatHelper(event) {
    if (event.keyCode == 13) {
      defeatSupport = true;
      user.tank = new Tank();
      window.clearInterval(interval2);
      document.removeEventListener('keydown', defeatHelper);
      level(Game.level, null, true);
    }
  }
  var victorySupport = false;

  function victory() {
    Game.endGame();
    draw.setTransform(window.resizer, 0, 0, window.resizer, 0, 0);
    draw.drawImage(tank_ui['victory'].image, 0, 0);
    draw.fillStyle = '#ffffff';
    draw.font = '20px starfont';
    draw.fillText('Coins Gained: ' + Game.coins, 160, 250);
    if (user.tank.fullHealthBonus) {
      draw.fillText('No Damage: x2 Coins', 160, 290);
      Game.coins *= 2;
    } else {
      draw.fillText('No Damage: No Bonus', 160, 290);
    }
    draw.fillText('Coins: ' + Game.coins + ' Xp: ' + Game.xp, 160, 330);
    userData.coins += Game.coins;
    userData.xp += Game.xp;
    if (userData.level == Game.level) {
      userData.xp += (Math.floor(Game.level % 5) + 1) * 5;
      userData.level++;
    }
    var t = new Object();
    t['increedible-tanks'] = userData;
    update(user.username, 'playerdata', JSON.stringify(t));
    draw.fillText('Total Coins: ' + userData.coins, 160, 370);
    document.addEventListener('keydown', victoryHelper);
    setTimeout(function() {
      document.removeEventListener('keydown', victoryHelper);
      if (!victorySupport) {
        mainMenu();
      } else {
        window.setTimeout(function() {
          victorySupport = false;
        }, 20);
      }
    }, 5000);
  }

  function victoryHelper(event) {
    if (event.keyCode == 13) {
      victorySupport = true;
      user.tank = new Tank();
      window.clearInterval(interval2);
      document.removeEventListener('keydown', victoryHelper);
      Game.level++;
      level(Game.level, null, true);
    }
  }

  var shopKitsM = new Menu(function() {
    draw.drawImage(shopKitsMenu, 0, 0);
    draw.fillStyle = '#ffffff';
    draw.font = '10px starfont';
    draw.fillText('Coins: ' + userData.coins, 360, 100);
    if (userData.kit == 'cooldown') draw.fillText('Equiped', 345, 170);
    if (userData.kit == 'autoheal') draw.fillText('Equiped', 345, 240);
    if (userData.kit == 'thermal') draw.fillText('Equiped', 345, 310);
    if (userData.kit == 'scavenger') draw.fillText('Equiped', 345, 380);
  }, {
    click: function(e) {
      if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
      } else {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }
      x -= canvas.offsetLeft;
      y -= canvas.offsetTop;
      x /= window.resizer;
      y /= window.resizer;
      if (x > 60) {
        if (x < 140) {
          if (y > 60) {
            if (y < 90) {
              this.removeListeners();
              mainMenu();
            }
          }
        }
      }
      if (x > 100) {
        if (x < 200) {
          if (y > 140) {
            if (y < 190) {
              if (userData.cooldownKit) {
                userData.kit = 'cooldown';
                setTimeout(function() {
                  this.removeListeners();
                  shopKits();
                }.bind(this), 20);
              } else if (userData.coins >= 40000) {
                userData.kit = 'cooldown';
                userData.cooldownKit = true;
                userData.coins -= 40000;
                try {
                  var t = playerData;
                  t['increedible-tanks'] = userData;
                } catch (e) {}
                setTimeout(function() {
                  this.removeListeners();
                  shopKits();
                }.bind(this), 20);
                update(sessionStorage.username, 'playerdata', JSON.stringify(t));
              } else {
                alert('Not Enough Money! You are ' + JSON.stringify(40000 - userData.coins) + ' coins short!');
              }
            }
          }
        }
      }
      if (x > 100) {
        if (x < 200) {
          if (y > 210) {
            if (y < 260) {
              if (userData.autohealKit) {
                userData.kit = 'autoheal';
                setTimeout(function() {
                  this.removeListeners();
                  shopKits();
                }.bind(this), 20);
              } else if (userData.coins >= 40000) {
                userData.kit = 'autoheal';
                userData.autohealKit = true;
                userData.coins -= 40000;
                try {
                  var t = playerData;
                  t['increedible-tanks'] = userData;
                } catch (e) {}
                setTimeout(function() {
                  this.removeListeners();
                  shopKits();
                }.bind(this), 20);
                update(sessionStorage.username, 'playerdata', JSON.stringify(t));
              } else {
                alert('Not Enough Money! You are ' + JSON.stringify(40000 - userData.coins) + ' coins short!');
              }
            }
          }
        }
      }
      if (x > 100) {
        if (x < 200) {
          if (y > 280) {
            if (y < 330) {
              if (userData.thermalKit) {
                userData.kit = 'thermal';
                setTimeout(function() {
                  this.removeListener(this);
                  shopKits();
                }.bind(this), 20);
              } else if (userData.coins >= 40000) {
                userData.kit = 'thermal';
                userData.thermalKit = true;
                userData.coins -= 40000;
                try {
                  var t = playerData;
                  t['increedible-tanks'] = userData;
                } catch (e) {}
                setTimeout(function() {
                  this.removeListener();
                  shopKits();
                }.bind(this), 20);
                update(sessionStorage.username, 'playerdata', JSON.stringify(t));
              } else {
                alert('Not Enough Money! You are ' + JSON.stringify(40000 - userData.coins) + ' coins short!');
              }
            }
          }
        }
      }
      if (x > 100) {
        if (x < 200) {
          if (y > 350) {
            if (y < 400) {
              if (userData.scavengerKit) {
                userData.kit = 'scavenger';
                setTimeout(function() {
                  this.removeListener();
                  shopKits();
                }.bind(this), 20);
              } else if (userData.coins >= 40000) {
                userData.kit = 'scavenger';
                userData.scavengerKit = true;
                userData.coins -= 40000;
                try {
                  var t = playerData;
                  t['increedible-tanks'] = userData;
                } catch (e) {}
                setTimeout(function() {
                  this.removeListener();
                  shopKits();
                }.bind(this), 20);
                update(sessionStorage.username, 'playerdata', JSON.stringify(t));
              } else {
                alert('Not Enough Money! You are ' + JSON.stringify(40000 - userData.coins) + ' coins short!');
              }
            }
          }
        }
      }
    },
    keydown: function(e) {
      if (e.keyCode == '39') {
        document.removeEventListener('keydown', shopKitsKeyDown);
        document.removeEventListener('click', shopKitsSupport);
        shopItems();
      }
    }
  });

  function shopKits() {
    shopKitsM.draw();
    shopKitsM.addListeners();
  }

  var shopItemsM = new Menu(function() {
    draw.drawImage(shopItemsMenu, 0, 0);
    draw.fillStyle = '#ffffff';
    draw.font = '10px starfont';
    draw.fillText('Coins: ' + userData.coins, 360, 100);
    draw.fillText('Boosts: ' + userData.boosts, 345, 170);
    draw.fillText('Blocks: ' + userData.blocks, 345, 240);
    draw.fillText('Toolkits: ' + userData.toolkits, 345, 310);
    draw.fillText('Flashbangs: ' + userData.flashbangs, 345, 380);
  }, {
    click: function shopItemsSupport(e) {
      if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
      } else {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }
      x -= canvas.offsetLeft;
      y -= canvas.offsetTop;
      x /= window.resizer;
      y /= window.resizer;
      if (x > 60) {
        if (x < 140) {
          if (y > 60) {
            if (y < 90) {
              this.removeListeners();
              mainMenu();
            }
          }
        }
      }
      if (x > 100) {
        if (x < 200) {
          if (y > 140) {
            if (y < 190) {
              if (userData.coins >= 50) {
                userData.coins -= 50;
                userData.boosts += 1;
                try {
                  var t = playerData;
                  t['increedible-tanks'] = userData;
                } catch (e) {}
                this.draw();
                update(sessionStorage.username, 'playerdata', JSON.stringify(t));
              } else {
                clearInterval(shopItemsLooper);
                alert('Not Enough Money! You are ' + JSON.stringify(50 - userData.coins) + ' coins short!');
              }
            }
          }
        }
      }
      if (x > 100) {
        if (x < 200) {
          if (y > 210) {
            if (y < 260) {
              if (userData.coins >= 200) {
                userData.coins -= 200;
                userData.blocks += 1;
                try {
                  var t = playerData;
                  t['increedible-tanks'] = userData;
                } catch (e) {}
                this.draw();
                update(sessionStorage.username, 'playerdata', JSON.stringify(t));
              } else {
                clearInterval(shopItemsLooper);
                alert('Not Enough Money! You are ' + JSON.stringify(200 - userData.coins) + ' coins short!');
              }
            }
          }
        }
      }
      if (x > 100) {
        if (x < 200) {
          if (y > 280) {
            if (y < 330) {
              if (userData.coins >= 500) {
                userData.coins -= 500;
                userData.toolkits += 1;
                try {
                  var t = playerData;
                  t['increedible-tanks'] = userData;
                } catch (e) {}
                this.draw();
                update(sessionStorage.username, 'playerdata', JSON.stringify(t));
              } else {
                clearInterval(shopItemsLooper);
                alert('Not Enough Money! You are ' + JSON.stringify(500 - userData.coins) + ' coins short!');
              }
            }
          }
        }
      }
      if (x > 100) {
        if (x < 200) {
          if (y > 350) {
            if (y < 400) {
              if (userData.coins >= 1000) {
                userData.coins -= 1000;
                userData.flashbangs += 1;
                try {
                  var t = playerData;
                  t['increedible-tanks'] = userData;
                } catch (e) {}
                this.draw();
                update(sessionStorage.username, 'playerdata', JSON.stringify(t));
              } else {
                clearInterval(shopItemsLooper);
                alert('Not Enough Money! You are ' + JSON.stringify(1000 - userData.coins) + ' coins short!');
              }
            }
          }
        }
      }
    },
    mousedown: function(e) {
      shopItemsLooper = setInterval(shopItemsSupport, 20, e);
    },
    mouseup: function shopItemsSupport3(e) {
      clearInterval(shopItemsLooper);
    },
    keydown: function(e) {
      if (e.keyCode == '39') {
        clearInterval(shopItemsLooper);
        this.removeListeners();
        shopTanks();
      } else if (e.keyCode == '37') {
        clearInterval(shopItemsLooper);
        this.removeListeners();
        shopKits();
      }
    },
  }, document);

  var shopItemsLooper;
  function shopItems() {
    shopItemsM.draw();
    shopItemsM.addListeners();
  }

  var shopClassesM = new Menu(function() {
    draw.drawImage(shopClassesMenu, 0, 0);
    draw.font = '10px starfont';
    draw.fillText('Coins: ' + userData.coins, 360, 100);
    if (userData.class == 'stealth') draw.fillText('Equiped', 345, 170);
    if (userData.class == 'tactical') draw.fillText('Equiped', 345, 240);
    if (userData.class == 'builder') draw.fillText('Equiped', 345, 310);
    if (userData.class == 'summoner') draw.fillText('Equiped', 345, 380);
  }, {
    click: function shopClassesSupport(e) {
      if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
      } else {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }
      x -= canvas.offsetLeft;
      y -= canvas.offsetTop;
      x /= window.resizer;
      y /= window.resizer;
      if (x > 60) {
        if (x < 140) {
          if (y > 60) {
            if (y < 90) {
              this.removeListeners();
              mainMenu();
            }
          }
        }
      }
      if (x > 100) {
        if (x < 200) {
          if (y > 140) {
            if (y < 190) {
              if (userData.stealth) {
                userData.class = 'stealth';
                setTimeout(function() {
                  this.removeListeners();
                  shopClasses();
                }.bind(this), 20);
              } else if (userData.coins >= 50000) {
                if (userData.class == 'stealth') {
                  alert('You already bought this!');
                  return;
                }
                userData.coins -= 50000;
                userData.class = 'stealth';
                userData.stealth = true;
                try {
                  var t = playerData;
                  t['increedible-tanks'] = userData;
                } catch (e) {}
                setTimeout(function() {
                  this.removeListeners();
                  shopClasses();
                }.bind(this), 20);
                update(sessionStorage.username, 'playerdata', JSON.stringify(t));
              } else {
                alert('Not Enough Money! You are ' + JSON.stringfy(50000 - userData.coins) + 'coins short!');
              }
            }
          }
        }
      }
      if (x > 100) {
        if (x < 200) {
          if (y > 210) {
            if (y < 260) {
              if (userData.tactical) {
                userData.class = 'tactical';
                setTimeout(function() {
                  this.removeListeners();
                  shopClasses();
                }.bind(this), 20);
              } else if (userData.coins >= 50000) {
                if (userData.class == 'tactical') {
                  alert('You already bought this!');
                  return;
                }
                userData.coins -= 50000;
                userData.class = 'tactical';
                userData.tactical = true;
                try {
                  var t = playerData;
                  t['increedible-tanks'] = userData;
                } catch (e) {}
                setTimeout(function() {
                  this.removeListeners();
                  shopClasses();
                }.bind(this), 20);
                update(sessionStorage.username, 'playerdata', JSON.stringify(t));
              } else {
                alert('Not Enough Money! You are' + JSON.stringify(50000 - userData.coins) + 'coins short!')
              }
            }
          }
        }
      }
      if (x > 100) {
        if (x < 200) {
          if (y > 280) {
            if (y < 330) {
              if (userData.builder) {
                userData.class = 'builder';
                setTimeout(function() {
                  this.removeListeners();
                  shopClasses();
                }.bind(this), 20);
              } else if (userData.coins >= 50000) {
                if (userData.class == 'builder') {
                  alert('You already bought this!');
                  return;
                }
                userData.coins -= 50000;
                userData.class = 'builder';
                userData.builder = true;
                try {
                  var t = playerData;
                  t['increedible-tanks'] = userData;
                } catch (e) {}
                setTimeout(function() {
                  this.removeListeners();
                  shopClasses();
                }.bind(this), 20);
                update(sessionStorage.username, 'playerdata', JSON.stringify(t));
              } else {
                alert('Not Enough Money! You are' + JSON.stringify(50000 - userData.coins) + 'coins short!')
              }
            }
          }
        }
      }
      if (x > 100) {
        if (x < 200) {
          if (y > 350) {
            if (y < 400) {
            /*if (userData.coins >= 1000) {
              userData.coins -= 1000;
              userData.flashbangs += 1;
              var t = playerData;
              t['increedible-tanks'] = userData;
              update(sessionStorage.username, 'playerdata', JSON.stringify(t));
              }*/
            }
          }
        }
      }
    },
    keydown: function(e) {
      if (e.keyCode == '37') {
        document.removeEventListener('click', shopClassesSupport);
        document.removeEventListener('keydown', shopClassesKeyDown);
        shopTanks();
      }
    },
  }, document);
  function shopClasses() {
    shopClassesM.draw();
    shopClassesM.addListeners();
  }

  function shopTanks() {
    document.removeEventListener('click', mainMenuSupport);
    document.removeEventListener('keydown', mainMenuSupport2);
    draw.drawImage(shopTanksMenu, 0, 0);
    draw.fillStyle = '#ffffff';
    draw.font = '10px starfont';
    draw.fillText('Coins: ' + userData.coins, 360, 100);
    if (userData.health == 300) draw.fillText('Equiped', 345, 170);
    if (userData.health == 400) draw.fillText('Equiped', 345, 240);
    if (userData.health == 500) draw.fillText('Equiped', 345, 310);
    if (userData.health == 600) draw.fillText('Equiped', 345, 380);
    document.addEventListener('click', shopTanksSupport);
    document.addEventListener('keydown', shopTanksKeyDown);
  }

  function shopTanksKeyDown(e) {
    if (e.keyCode == '37') {
      document.removeEventListener('click', shopTanksSupport);
      document.removeEventListener('keydown', shopTanksKeyDown);
      shopItems();
    }
    if (e.keyCode == '39') {
      document.removeEventListener('click', shopTanksSupport);
      document.removeEventListener('keydown', shopTanksKeyDown);
      shopClasses();
    }
  }

  function shopTanksSupport(e) {
    if (e.pageX || e.pageY) {
      x = e.pageX;
      y = e.pageY;
    } else {
      x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    x /= window.resizer;
    y /= window.resizer;
    if (x > 60) {
      if (x < 140) {
        if (y > 60) {
          if (y < 90) {
            mainMenu();
          }
        }
      }
    }
    if (x > 100) {
      if (x < 200) {
        if (y > 140) {
          if (y < 190) {
            if (userData.steel) {
              userData.health = 300;
              setTimeout(function() {
                document.removeEventListener('keydown', shopTanksKeyDown);
                document.removeEventListener('click', shopTanksSupport);
                shopTanks();
              }, 20);
            } else if (userData.coins >= 20000) {
              if (userData.health == 200) {
                userData.health = 300;
                userData.steel = true;
              } else {
                alert('You already bought a better upgrade!');
                return;
              }
              userData.coins -= 20000;
              try {
                var t = playerData;
                t['increedible-tanks'] = userData;
              } catch (e) {}
              setTimeout(function() {
                document.removeEventListener('keydown', shopTanksKeyDown);
                document.removeEventListener('click', shopTanksSupport);
                shopTanks();
              }, 20);
              update(sessionStorage.username, 'playerdata', JSON.stringify(t));
            } else {
              alert('Not Enough Money! You are ' + JSON.stringify(20000 - userData.coins) + ' coins short!');
            }
          }
        }
      }
    }
    if (x > 100) {
      if (x < 200) {
        if (y > 210) {
          if (y < 260) {
            if (userData.crystal) {
              userData.health = 400;
              setTimeout(function() {
                document.removeEventListener('keydown', shopTanksKeyDown);
                document.removeEventListener('click', shopTanksSupport);
                shopTanks();
              }, 20);
            } else if (userData.coins >= 40000) {
              if (userData.health < 300) {
                alert('You need to buy Steel Tank first!');
                return;
              }
              if (userData.health > 300) {
                alert('You already bought a better upgrade!');
                return;
              }
              if (userData.health == 300) {
                userData.health = 400;
                userData.crystal = true;
              }
              userData.coins -= 40000;
              try {
                var t = playerData;
                t['increedible-tanks'] = userData;
              } catch (e) {}
              setTimeout(function() {
                document.removeEventListener('keydown', shopTanksKeyDown);
                document.removeEventListener('click', shopTanksSupport);
                shopTanks();
              }, 20);
              update(sessionStorage.username, 'playerdata', JSON.stringify(t));
            } else {
              alert('Not Enough Money! You are ' + JSON.stringify(40000 - userData.coins) + ' coins short!');
            }
          }
        }
      }
    }
    if (x > 100) {
      if (x < 200) {
        if (y > 280) {
          if (y < 330) {
            if (userData.dark) {
              userData.health = 500;
              setTimeout(function() {
                document.removeEventListener('keydown', shopTanksKeyDown);
                document.removeEventListener('click', shopTanksSupport);
                shopTanks();
              }, 20);
            } else if (userData.coins >= 50000) {
              if (userData.health < 400) {
                alert('You need to buy Crystal Tank first!');
                return;
              }
              if (userData.health > 400) {
                alert('You already bought a better upgrade!');
                return;
              }
              if (userData.health == 400) {
                userData.health = 500;
                userData.dark = true;
              }
              userData.coins -= 50000;
              try {
                var t = playerData;
                t['increedible-tanks'] = userData;
              } catch (e) {}
              setTimeout(function() {
                document.removeEventListener('keydown', shopTanksKeyDown);
                document.removeEventListener('click', shopTanksSupport);
                shopTanks();
              }, 20);
              update(sessionStorage.username, 'playerdata', JSON.stringify(t));
            } else {
              alert('Not Enough Money! You are ' + JSON.stringify(50000 - userData.coins) + ' coins short!');
            }
          }
        }
      }
    }
    if (x > 100) {
      if (x < 200) {
        if (y > 350) {
          if (y < 400) {
            if (userData.light) {
              userData.health = 600;
              setTimeout(function() {
                document.removeEventListener('keydown', shopTanksKeyDown);
                document.removeEventListener('click', shopTanksSupport);
                shopTanks();
              }, 20);
            } else if (userData.coins >= 75000) {
              if (userData.health < 500) {
                alert('You need to buy Dark Tank first!');
                return;
              }
              if (userData.health > 500) {
                alert('You already bought a better upgrade!');
                return;
              }
              if (userData.health == 500) {
                userData.health = 600;
                userData.light = true;
              }
              userData.coins -= 75000;
              try {
                var t = playerData;
                t['increedible-tanks'] = userData;
              } catch (e) {}
              setTimeout(function() {
                document.removeEventListener('keydown', shopTanksKeyDown);
                document.removeEventListener('click', shopTanksSupport);
                shopTanks();
              }, 20);
              update(sessionStorage.username, 'playerdata', JSON.stringify(t));
            } else {
              alert('Not Enough Money! You are ' + JSON.stringify(75000 - userData.coins) + ' coins short!');
            }
          }
        }
      }
    }
  }

  function shopGuns() {}

  function shopGunsKeyDown(e) {}
  var mainM = new Menu(function() {
    clearInterval(endlessRunner); // backup
    Game.flashbanged = false;
    document.getElementById('server').style.display = 'none';
    clearInterval(shopItemsLooper);
    user.tank = new Tank();
    draw.clearRect(0, 0, 500, 500);
    draw.drawImage(tank_ui['main_menu'].image, 0, 0);
    if (userData.heath == 200) {
      draw.drawImage(tank_base_png, 90, 120);
      draw.drawImage(tank_top_png, 90, 120);
    } else if (userData.health == 300) {
      draw.drawImage(iron_tank_base, 90, 120);
      draw.drawImage(iron_tank_top, 90, 120);
    } else if (userData.health == 400) {
      draw.drawImage(diamond_tank_base, 90, 120);
      draw.drawImage(diamond_tank_top, 90, 120)
    } else if (userData.health == 500) {
      draw.drawImage(dark_tank_base, 90, 120);
      draw.drawImage(dark_tank_top, 90, 120);
    } else if (userData.health == 600) {
      draw.drawImage(light_tank_base, 90, 120);
      draw.drawImage(light_tank_top, 90, 120);
    }
    if (userData.cosmetic) {
      var l = 0;
      while (l < cosmetics.length) {
        if (userData.cosmetic == cosmetics[l].name) {
          draw.drawImage(cosmetics[l].image, 90, 120);
          break;
        }
        l++;
      }
    }
    draw.fillStyle = '#ffffff';
    draw.font = '15px starfont';
    draw.fillText(user.username, 150, 120);
    var xpToLevelUp = Math.ceil(Math.pow(1.6, userData.rank - 1) + 20 * (userData.rank - 1));
    while (userData.xp > xpToLevelUp) {
      if (userData.xp >= xpToLevelUp) {
        userData.xp -= xpToLevelUp;
        userData.rank += 1;
      }
      xpToLevelUp = Math.ceil(Math.pow(1.6, userData.rank - 1) + 20 * (userData.rank - 1));
    }
    draw.fillText('Rank: ' + userData.rank, 150, 150);
    draw.fillText('XP - ' + userData.xp + '/' + xpToLevelUp, 150, 180);
    draw.drawImage(coins, 300, 100);
    draw.fillText(userData.coins, 350, 130);
    window.clearInterval(interval2);
  }, {
    click: function mainMenuSupport(e) {
      x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      x -= canvas.offsetLeft;
      y -= canvas.offsetTop;
      x /= window.resizer;
      y /= window.resizer;
      if (x > 307) {
        if (x < 495) {
          if (y > 412) {
            if (y < 495) {
              this.removeListeners();
              multiplayer();
            }
          }
        }
      }
      if (x > 5) {
        if (x < 193) {
          if (y > 412) {
            if (y < 495) {
              this.removeListeners();
              levelSelect();
            }
          }
        }
      }
      if (x > 6) {
        if (x < 59) {
          if (y > 6) {
            if (y < 59) {
              this.removeListeners();
              shopItems();
            }
          }
        }
      }
      if (x > 442) {
        if (x < 495) {
          if (y > 6) {
            if (y < 59) {
              this.removeListeners();
              mainCosmetics();
            }
          }
        }
      }
    },
    keydown: function(e) {
      e = e || event;
      endlessLogger += e.key;
      if (endlessLogger.includes('endless')) {
        this.removeListeners();
        endlessLogger = '';
        Game.level = 10000;
        level(10000, null, true);
      }
    }
  }, document);
  var endlessLogger = '';

  function mainMenu() {
    mainM.draw();
    mainM.addListeners();
  }

  function mainCosmetics() {
    document.removeEventListener('keydown', myCosmeticSupport);
    document.removeEventListener('mousedown', myCosmeticSupport2);
    document.removeEventListener('click', mainMenuSupport);
    document.removeEventListener('keydown', mainMenuSupport2)
    draw.drawImage(tank_ui['cosmetics_main'].image, 0, 0);
    draw.textAlign = 'center';
    draw.fillText('Crates: ' + userData.crates, 250, 130);
    draw.textAlign = 'left';
    document.addEventListener('click', mainCosmeticSupport);
  }

  function mainCosmeticSupport(e) {
    if (e.pageX || e.pageY) {
      x = e.pageX;
      y = e.pageY;
    } else {
      x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    x /= window.resizer;
    y /= window.resizer;
    if (x > 388) {
      if (x < 411) {
        if (y > 57) {
          if (y < 110) {
            myCosmetics();
          }
        }
      }
    }
    if (x > 59) {
      if (x < 112) {
        if (y > 56) {
          if (y < 109) {
            mainMenu();
          }
        }
      }
    }
    // Red Crate (69,303)(122,356)
    if (x > 69) {
      if (x < 122) {
        if (y > 303) {
          if (y < 356) {
            openCrate('red');
          }
        }
      }
    }
    // Orange Crate (323,145)(376,199)
    if (x > 323) {
      if (x < 376) {
        if (y > 145) {
          if (y < 199) {
            openCrate('orange');
          }
        }
      }
    }
    // Yellow Crate (322,223)(375,276)
    if (x > 323) {
      if (x < 375) {
        if (y > 223) {
          if (y < 276) {
            openCrate('yellow');
          }
        }
      }
    }
    // Green Crate (323,306)(376,359)
    if (x > 323) {
      if (x < 376) {
        if (y > 306) {
          if (y < 359) {
            openCrate('green');
          }
        }
      }
    }
    // Blue Crate (198,305)(251,358)
    if (x > 198) {
      if (x < 251) {
        if (y > 305) {
          if (y < 358) {
            openCrate('blue');
          }
        }
      }
    }
    // Purple Crate (197,222)(250,275)
    if (x > 197) {
      if (x < 250) {
        if (y > 222) {
          if (y < 275) {
            openCrate('purple');
          }
        }
      }
    }
    // Mark Crate (69,143)(122,196)
    if (x > 69) {
      if (x < 122) {
        if (y > 143) {
          if (y < 196) {
            openCrate('mark');
          }
        }
      }
    }
    // Grey Crate (323,383)(376,436)
    if (x > 323) {
      if (x < 376) {
        if (y > 383) {
          if (y < 436) {
            openCrate('grey');
          }
        }
      }
    }
    // Christmas Crate (68,220)(121,273)
    if (x > 68) {
      if (x < 121) {
        if (y > 220) {
          if (y < 273) {
            openCrate('christmas');
          }
        }
      }
    }
    // Haloween (198,382)(251,435)
    if (x > 198) {
      if (x < 251) {
        if (y > 382) {
          if (y < 435) {
            openCrate('halloween');
          }
        }
      }
    }
    // Misc Crate (69,380)(122,433)
    if (x > 69) {
      if (x < 122) {
        if (y > 380) {
          if (y < 433) {
            openCrate('misc');
          }
        }
      }
    }
    // Pizza Crate (198,145)(251,198)
    if (x > 198) {
      if (x < 251) {
        if (y > 145) {
          if (y < 198) {
            openCrate('pizza');
          }
        }
      }
    }
  }

  function openCrate(crate) {
    if (userData.crates != 0) {
      userData.crates--;
    } else {
      alert('You need to get crates to open them. You can get crates by winning Duels matches!');
      return;
    }
    var raritySelector = 0;
    // 1 -> Common
    // 2 -> Uncommon
    // 3 -> Rare
    // 4 -> Epic
    // 5 -> Legendary
    // 6 -> Mythic
    if (Math.floor(Math.random() * (1000 - 0 + 1)) < 1) {
      raritySelector = 6;
    } else if (Math.floor(Math.random() * (1001)) < 10) {
      raritySelector = 5;
    } else if (Math.floor(Math.random() * (1001)) < 50) {
      raritySelector = 4;
    } else if (Math.floor(Math.random() * (1001)) < 150) {
      raritySelector = 3;
    } else if (Math.floor(Math.random() * (1000 - 0 + 1)) < 300) {
      raritySelector = 2;
    } else {
      raritySelector = 1;
    }
    var done = false;
    while (!done) {
      var l = 0,
        availableCosmetics = [],
        rarity;
      if (raritySelector == 6) {
        rarity = 'mythic';
      } else if (raritySelector == 5) {
        rarity = 'legendary'
      } else if (raritySelector == 4) {
        rarity = 'epic';
      } else if (raritySelector == 3) {
        rarity = 'rare';
      } else if (raritySelector == 2) {
        rarity = 'uncommon'
      } else {
        rarity = 'common';
      }
      while (l < crates[crate].length) {
        if (crates[crate][l].rarity == rarity) {
          availableCosmetics.push(crates[crate][l].name);
        }
        l++;
      }
      if (availableCosmetics.length == 0) {
        done = false;
        raritySelector--;
      } else {
        done = true;
      }
    }
    var number = Math.floor(Math.random() * (availableCosmetics.length));
    var c;
    var l = 0;
    while (l < cosmetics.length) {
      if (cosmetics[l].name == availableCosmetics[number]) {
        c = cosmetics[l];
      }
      l++;
    }
    draw.clearRect(0, 0, 500, 500);
    document.removeEventListener('click', mainCosmeticSupport);
    var boxAnimation = new Animation(boxOpenOpt);
    boxAnimation.loopRunSpriteSheet(function(c) {
      draw.drawImage(c.image, 150, 200, 200, 200);
      draw.textAlign = 'center';
      draw.fillStyle = '#ffffff';
      draw.fillText(c.name, 250, 400);
      draw.textAlign = 'left';
      setTimeout(mainCosmetics, 2000);
      userData.cosmetics.push(c.name);
      saveGame();
    }, c);
  }

  function myCosmetics() {
    document.removeEventListener('click', mainCosmeticSupport);
    document.addEventListener('keydown', myCosmeticSupport);
    document.addEventListener('mousedown', myCosmeticSupport2);
    drawCosmetics();
  }
  var cosmeticMenuStage = 0;

  function drawCosmetics() {
    draw.clearRect(0, 0, 500, 500);
    var totalStages = (userData.cosmetics.length - (userData.cosmetics.length % 30)) / 30;
    if (cosmeticMenuStage == 0) {
      draw.drawImage(tank_ui['cosmetics_left'].image, 0, 0);
    } else if (cosmeticMenuStage == totalStages) {
      draw.drawImage(tank_ui['cosmetics_right'].image, 0, 0);
    } else {
      draw.drawImage(tank_ui['cosmetics_middle'].image, 0, 0);
    }
    var start = cosmeticMenuStage * 30;
    if (userData.cosmetics.length - start < 30) {
      end = userData.cosmetics.length;
    } else {
      end = start + 30;
    }
    var l = start;
    while (l < end) {
      if (userData.cosmetic == userData.cosmetics[l]) {
        draw.lineWidth = 5;
        draw.strokeStyle = '#FFA500';
        draw.strokeRect(((l - start) % 6) * 59 + 78 - 2, (((l - start) - ((l - start) % 6)) / 6) * 59 + 140 - 2, 50, 50);
      }
      var q = 0;
      while (q < cosmetics.length) {
        if (cosmetics[q].name == userData.cosmetics[l]) {
          draw.drawImage(cosmetics[q].image, ((l - start) % 6) * 59 + 78, (((l - start) - ((l - start) % 6)) / 6) * 59 + 140);
        }
        q++;
      }
      l++;
    }
  }

  function myCosmeticSupport2(e) {
    if (e.pageX || e.pageY) {
      x = e.pageX;
      y = e.pageY;
    } else {
      x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    x /= window.resizer;
    y /= window.resizer;
    if (x > 59) {
      if (x < 112) {
        if (y > 56) {
          if (y < 109) {
            mainCosmetics();
          }
        }
      }
    }
    var mode;
    if (e.button == 0) {
      mode = 'select';
    } else {
      mode = 'delete';
    }
    var y2 = (y - ((y - 140) % 59)) / 59;
    var x2 = (x - ((x - 78) % 59)) / 59;
    var y3, x3;
    if (((y - 140) % 59 < y2 + 50) && y > 140) {
      y3 = Math.floor(y2 - 2);
    }
    if (((x - 78) % 59 < x2 + 50) && x > 78) {
      x3 = Math.floor(x2);
    }
    if (x3 != undefined && y3 != undefined) {
      if (mode == 'select') {
        userData.cosmetic = userData.cosmetics[cosmeticMenuStage * 30 + y3 * 6 + x3 - 1];
        drawCosmetics();
      } else if (mode == 'delete') {
        userData.cosmetics.splice(cosmeticMenuStage * 30 + y3 * 6 + x3 - 1, 1);
        drawCosmetics();
      }
    }
  }

  function myCosmeticSupport(e) {
    if (e.keyCode == 39) {
      if (cosmeticMenuStage != (userData.cosmetics.length - (userData.cosmetics.length % 30)) / 30) {
        cosmeticMenuStage++;
      }
    } else if (e.keyCode == 37) {
      if (cosmeticMenuStage != 0) {
        cosmeticMenuStage--;
      }
    }
    drawCosmetics();
  }
  var levelSelectM = new Menu(function() {
    draw.fillStyle = '#556B2f';
    draw.fillRect(50, 50, 400, 400);
    draw.fillStyle = '#ffffff';
    draw.fillRect(139, 94, 44, 44);
    if (userData.level < 2) draw.fillStyle = '#000000';
    draw.fillRect(228, 94, 44, 44);
    if (userData.level < 3) draw.fillStyle = '#000000';
    draw.fillRect(317, 94, 44, 44);
    if (userData.level < 4) draw.fillStyle = '#000000';
    draw.fillRect(139, 94 + 44 * 2, 44, 44);
    if (userData.level < 5) draw.fillStyle = '#000000';
    draw.fillRect(228, 94 + 44 * 2, 44, 44);
    if (userData.level < 6) draw.fillStyle = '#000000';
    draw.fillRect(317, 94 + 44 * 2, 44, 44);
    if (userData.level < 7) draw.fillStyle = '#000000';
    draw.fillRect(139, 94 + 44 * 4, 44, 44);
    if (userData.level < 8) draw.fillStyle = '#000000';
    draw.fillRect(228, 94 + 44 * 4, 44, 44);
    if (userData.level < 9) draw.fillStyle = '#000000';
    draw.fillRect(317, 94 + 44 * 4, 44, 44);
    if (userData.level < 10) draw.fillStyle = '#000000';
    draw.fillRect(139, 94 + 44 * 6, 44, 44);
    if (userData.level < 11) draw.fillStyle = '#000000';
    draw.fillRect(228, 94 + 44 * 6, 44, 44);
    if (userData.level < 12) draw.fillStyle = '#000000';
    draw.fillRect(317, 94 + 44 * 6, 44, 44);
    draw.fillStyle = '#000000';
    draw.font = '20px starfont';
    draw.fillText('1', 139 + 15, 94 + 30);
    if (userData.level < 2) draw.fillStyle = '#A9A9A9';
    draw.fillText('2', 228 + 15, 94 + 30);
    if (userData.level < 3) draw.fillStyle = '#A9A9A9';
    draw.fillText('3', 317 + 15, 94 + 30);
    if (userData.level < 4) draw.fillStyle = '#A9A9A9';
    draw.fillText('4', 139 + 15, 94 + 44 * 2 + 30);
    if (userData.level < 5) draw.fillStyle = '#A9A9A9';
    draw.fillText('5', 228 + 15, 94 + 44 * 2 + 30);
    if (userData.level < 6) draw.fillStyle = '#A9A9A9';
    draw.fillText('6', 317 + 15, 94 + 44 * 2 + 30);
    if (userData.level < 7) draw.fillStyle = '#A9A9A9';
    draw.fillText('7', 139 + 15, 94 + 44 * 4 + 30);
    if (userData.level < 8) draw.fillStyle = '#A9A9A9';
    draw.fillText('8', 228 + 15, 94 + 44 * 4 + 30);
    if (userData.level < 9) draw.fillStyle = '#A9A9A9';
    draw.fillText('9', 317 + 15, 94 + 44 * 4 + 30);
    if (userData.level < 10) draw.fillStyle = '#A9A9A9';
    draw.fillText('10', 139 + 15, 94 + 44 * 6 + 30);
    if (userData.level < 11) draw.fillStyle = '#A9A9A9';
    draw.fillText('11', 228 + 15, 94 + 44 * 6 + 30);
    if (userData.level < 12) draw.fillStyle = '#A9A9A9';
    draw.fillText('12', 317 + 15, 94 + 44 * 6 + 30);
  }, {
    click: function levelSelectSupport(e) {
      if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
      } else {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }
      x -= canvas.offsetLeft;
      y -= canvas.offsetTop;
      x /= window.resizer;
      y /= window.resizer;
      if (x > 139) {
        if (x < 139 + 44) {
          if (y > 94) {
            if (y < 94 + 44) {
              this.removeListeners();
              Game.level = 1;
              level(1, null, true);
            }
          }
        }
      }
      if (x > 228) {
        if (x < 228 + 44) {
          if (y > 94) {
            if (y < 94 + 44 && userData.level > 1) {
              this.removeListeners();
              Game.level = 2;
              level(2, null, true);
            }
          }
        }
      }
      if (x > 317) {
        if (x < 317 + 44) {
          if (y > 94) {
            if (y < 94 + 44 && userData.level > 2) {
              this.removeListeners();
              Game.level = 3;
              level(3, null, true);
            }
          }
        }
      }
      if (x > 139) {
        if (x < 139 + 44) {
          if (y > 94 + 44 * 2) {
            if (y < 94 + 44 * 2 + 44 && userData.level > 3) {
              this.removeListeners();
              Game.level = 4;
              level(4, null, true);
            }
          }
        }
      }
      if (x > 228) {
        if (x < 228 + 44) {
          if (y > 94 + 44 * 2) {
            if (y < 94 + 44 * 2 + 44 && userData.level > 4) {
              this.removeListeners();
              Game.level = 5;
              level(5, null, true);
            }
          }
        }
      }
      if (x > 317) {
        if (x < 317 + 44) {
          if (y > 94 + 44 * 2) {
            if (y < 94 + 44 * 2 + 44 && userData.level > 5) {
              this.removeListeners();
              Game.level = 6;
              level(6, null, true);
            }
          }
        }
      }
      if (x > 139) {
        if (x < 139 + 44) {
          if (y > 94 + 44 * 4) {
            if (y < 94 + 44 * 4 + 44 && userData.level > 6) {
              this.removeListeners();
              Game.level = 7;
              level(7, null, true);
            }
          }
        }
      }
      if (x > 228) {
        if (x < 228 + 44) {
          if (y > 94 + 44 * 4) {
            if (y < 94 + 44 * 4 + 44 && userData.level > 7) {
              this.removeListeners();
              Game.level = 8;
              level(8, null, true);
            }
          }
        }
      }
      if (x > 317) {
        if (x < 317 + 44) {
          if (y > 94 + 44 * 4) {
            if (y < 94 + 44 * 4 + 44 && userData.level > 8) {
              this.removeListeners();
              Game.level = 9;
              level(9, null, true);
            }
          }
        }
      }
      if (x > 139) {
        if (x < 139 + 44) {
          if (y > 94 + 44 * 6) {
            if (y < 94 + 44 * 6 + 44 && userData.level > 9) {
              this.removeListeners();
              Game.level = 10;
              level(10, null, true);
            }
          }
        }
      }
      if (x > 228) {
        if (x < 228 + 44) {
          if (y > 94 + 44 * 6) {
            if (y < 94 + 44 * 6 + 44 && userData.level > 10) {
              this.removeListeners();
              Game.level = 11;
              level(11, null, true);
            }
          }
        }
      }
      if (x > 317) {
        if (x < 317 + 44) {
          if (y > 94 + 44 * 6) {
            if (y < 94 + 44 * 6 + 44 && userData.level > 11) {
              this.removeListeners();
              Game.level = 12;
              level(12, null, true);
            }
          }
        }
      }
    },
    keydown: function(e) {
      if (e.keyCode == 39) {
        this.removeListeners();
        levelSelect2();
      }
    },
  }, document);

  function levelSelect() {
    levelSelectM.draw();
    levelSelectM.addListeners();
  }

  function levelSelect2() {
    draw.fillStyle = '#556B2f';
    draw.fillRect(50, 50, 400, 400);
    draw.fillStyle = '#ffffff';
    if (userData.level < 13) draw.fillStyle = '#000000';
    draw.fillRect(139, 94, 44, 44);
    if (userData.level < 14) draw.fillStyle = '#000000';
    draw.fillRect(228, 94, 44, 44);
    if (userData.level < 15) draw.fillStyle = '#000000';
    draw.fillRect(317, 94, 44, 44);
    if (userData.level < 16) draw.fillStyle = '#000000';
    draw.fillRect(139, 94 + 44 * 2, 44, 44);
    if (userData.level < 17) draw.fillStyle = '#000000';
    draw.fillRect(228, 94 + 44 * 2, 44, 44);
    if (userData.level < 18) draw.fillStyle = '#000000';
    draw.fillRect(317, 94 + 44 * 2, 44, 44);
    if (userData.level < 19) draw.fillStyle = '#000000';
    draw.fillRect(139, 94 + 44 * 4, 44, 44);
    if (userData.level < 20) draw.fillStyle = '#000000';
    draw.fillRect(228, 94 + 44 * 4, 44, 44);
    if (userData.level < 21) draw.fillStyle = '#000000';
    draw.fillRect(317, 94 + 44 * 4, 44, 44);
    if (userData.level < 22) draw.fillStyle = '#000000';
    draw.fillRect(139, 94 + 44 * 6, 44, 44);
    if (userData.level < 23) draw.fillStyle = '#000000';
    draw.fillRect(228, 94 + 44 * 6, 44, 44);
    if (userData.level < 24) draw.fillStyle = '#000000';
    draw.fillRect(317, 94 + 44 * 6, 44, 44);
    draw.fillStyle = '#000000';
    draw.font = '20px starfont';
    if (userData.level < 13) draw.fillStyle = '#A9A9A9';
    draw.fillText('13', 139 + 15, 94 + 30);
    if (userData.level < 14) draw.fillStyle = '#A9A9A9';
    draw.fillText('14', 228 + 15, 94 + 30);
    if (userData.level < 15) draw.fillStyle = '#A9A9A9';
    draw.fillText('15', 317 + 15, 94 + 30);
    if (userData.level < 16) draw.fillStyle = '#A9A9A9';
    draw.fillText('16', 139 + 15, 94 + 44 * 2 + 30);
    if (userData.level < 17) draw.fillStyle = '#A9A9A9';
    draw.fillText('17', 228 + 15, 94 + 44 * 2 + 30);
    if (userData.level < 18) draw.fillStyle = '#A9A9A9';
    draw.fillText('18', 317 + 15, 94 + 44 * 2 + 30);
    if (userData.level < 19) draw.fillStyle = '#A9A9A9';
    draw.fillText('19', 139 + 15, 94 + 44 * 4 + 30);
    if (userData.level < 20) draw.fillStyle = '#A9A9A9';
    draw.fillText('20', 228 + 15, 94 + 44 * 4 + 30);
    if (userData.level < 21) draw.fillStyle = '#A9A9A9';
    draw.fillText('21', 317 + 15, 94 + 44 * 4 + 30);
    if (userData.level < 22) draw.fillStyle = '#A9A9A9';
    draw.fillText('22', 139 + 15, 94 + 44 * 6 + 30);
    if (userData.level < 23) draw.fillStyle = '#A9A9A9';
    draw.fillText('23', 228 + 15, 94 + 44 * 6 + 30);
    if (userData.level < 24) draw.fillStyle = '#A9A9A9';
    draw.fillText('24', 317 + 15, 94 + 44 * 6 + 30);
    document.addEventListener('click', levelSelect2Support);
    document.addEventListener('keydown', levelSelectSupport2KeyDown);
  }

  function levelSelect2Support(e) {
    if (e.pageX || e.pageY) {
      x = e.pageX;
      y = e.pageY;
    } else {
      x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    x /= window.resizer;
    y /= window.resizer;
    if (x > 139) {
      if (x < 139 + 44) {
        if (y > 94) {
          if (y < 94 + 44 && userData.level > 12) {
            document.removeEventListener('click', levelSelectSupport);
            Game.level = 13;
            level(13, null, true);
          }
        }
      }
    }
    if (x > 228) {
      if (x < 228 + 44) {
        if (y > 94) {
          if (y < 94 + 44 && userData.level > 13) {
            document.removeEventListener('click', levelSelect2Support);
            document.removeEventListener('keydown', levelSelectSupport2KeyDown);
            Game.level = 14;
            level(14, null, true);
          }
        }
      }
    }
    if (x > 317) {
      if (x < 317 + 44) {
        if (y > 94) {
          if (y < 94 + 44 && userData.level > 14) {
            document.removeEventListener('click', levelSelect2Support);
            document.removeEventListener('keydown', levelSelectSupport2KeyDown);
            Game.level = 15;
            level(15, null, true);
          }
        }
      }
    }
    if (x > 139) {
      if (x < 139 + 44) {
        if (y > 94 + 44 * 2) {
          if (y < 94 + 44 * 2 + 44 && userData.level > 15) {
            document.removeEventListener('click', levelSelect2Support);
            document.removeEventListener('keydown', levelSelectSupport2KeyDown);
            Game.level = 16;
            level(16, null, true);
          }
        }
      }
    }
    if (x > 228) {
      if (x < 228 + 44) {
        if (y > 94 + 44 * 2) {
          if (y < 94 + 44 * 2 + 44 && userData.level > 16) {
            document.removeEventListener('click', levelSelect2Support);
            document.removeEventListener('keydown', levelSelectSupport2KeyDown);
            Game.level = 17;
            level(17, null, true);
          }
        }
      }
    }
    if (x > 317) {
      if (x < 317 + 44) {
        if (y > 94 + 44 * 2) {
          if (y < 94 + 44 * 2 + 44 && userData.level > 17) {
            document.removeEventListener('click', levelSelect2Support);
            document.removeEventListener('keydown', levelSelectSupport2KeyDown);
            Game.level = 18;
            level(18, null, true);
          }
        }
      }
    }
    if (x > 139) {
      if (x < 139 + 44) {
        if (y > 94 + 44 * 4) {
          if (y < 94 + 44 * 4 + 44 && userData.level > 18) {
            document.removeEventListener('click', levelSelect2Support);
            document.removeEventListener('keydown', levelSelectSupport2KeyDown);
            Game.level = 19;
            level(19, null, true);
          }
        }
      }
    }
    if (x > 228) {
      if (x < 228 + 44) {
        if (y > 94 + 44 * 4) {
          if (y < 94 + 44 * 4 + 44 && userData.level > 19) {
            document.removeEventListener('click', levelSelect2Support);
            document.removeEventListener('keydown', levelSelectSupport2KeyDown);
            Game.level = 20;
            level(20, null, true);
          }
        }
      }
    }
    if (x > 317) {
      if (x < 317 + 44) {
        if (y > 94 + 44 * 4) {
          if (y < 94 + 44 * 4 + 44 && userData.level > 20) {
            document.removeEventListener('click', levelSelect2Support);
            document.removeEventListener('keydown', levelSelectSupport2KeyDown);
            Game.level = 21;
            level(21, null, true);
          }
        }
      }
    }
    if (x > 139) {
      if (x < 139 + 44) {
        if (y > 94 + 44 * 6) {
          if (y < 94 + 44 * 6 + 44 && userData.level > 21) {
            document.removeEventListener('click', levelSelect2Support);
            document.removeEventListener('keydown', levelSelectSupport2KeyDown);
            Game.level = 22;
            level(22, null, true);
          }
        }
      }
    }
    if (x > 228) {
      if (x < 228 + 44) {
        if (y > 94 + 44 * 6) {
          if (y < 94 + 44 * 6 + 44 && userData.level > 22) {
            document.removeEventListener('click', levelSelect2Support);
            document.removeEventListener('keydown', levelSelectSupport2KeyDown);
            Game.level = 23;
            level(23, null, true);
          }
        }
      }
    }
    if (x > 317) {
      if (x < 317 + 44) {
        if (y > 94 + 44 * 6) {
          if (y < 94 + 44 * 6 + 44 && userData.level > 23) {
            document.removeEventListener('click', levelSelect2Support);
            document.removeEventListener('keydown', levelSelectSupport2KeyDown);
            Game.level = 24;
            level(24, null, true);
          }
        }
      }
    }
  }

  function levelSelectSupport2KeyDown(e) {
    if (e.keyCode == 39) {
      document.removeEventListener('keydown', levelSelectSupport2KeyDown);
      document.removeEventListener('click', levelSelect2Support)
      levelSelect3();
    }
    if (e.keyCode == 37) {
      document.removeEventListener('keydown', levelSelectSupport2KeyDown);
      document.removeEventListener('click', levelSelect2Support);
      levelSelect();
    }
  }

  function levelSelect3() {
    document.removeEventListener('click', mainMenuSupport);
    document.removeEventListener('keydown', mainMenuSupport2);
    draw.fillStyle = '#556B2f';
    draw.fillRect(50, 50, 400, 400);
    draw.fillStyle = '#ffffff';
    if (userData.level < 25) draw.fillStyle = '#000000';
    draw.fillRect(139, 94, 44, 44);
    if (userData.level < 26) draw.fillStyle = '#000000';
    draw.fillRect(228, 94, 44, 44);
    if (userData.level < 27) draw.fillStyle = '#000000';
    draw.fillRect(317, 94, 44, 44);
    if (userData.level < 28) draw.fillStyle = '#000000';
    draw.fillRect(139, 94 + 44 * 2, 44, 44);
    if (userData.level < 29) draw.fillStyle = '#000000';
    draw.fillRect(228, 94 + 44 * 2, 44, 44);
    if (userData.level < 30) draw.fillStyle = '#000000';
    draw.fillRect(317, 94 + 44 * 2, 44, 44);
    if (userData.level < 31) draw.fillStyle = '#000000';
    draw.fillRect(139, 94 + 44 * 4, 44, 44);
    if (userData.level < 32) draw.fillStyle = '#000000';
    draw.fillRect(228, 94 + 44 * 4, 44, 44);
    if (userData.level < 33) draw.fillStyle = '#000000';
    draw.fillRect(317, 94 + 44 * 4, 44, 44);
    if (userData.level < 34) draw.fillStyle = '#000000';
    draw.fillRect(139, 94 + 44 * 6, 44, 44);
    if (userData.level < 35) draw.fillStyle = '#000000';
    draw.fillRect(228, 94 + 44 * 6, 44, 44);
    if (userData.level < 36) draw.fillStyle = '#000000';
    draw.fillRect(317, 94 + 44 * 6, 44, 44);
    draw.fillStyle = '#000000';
    draw.font = '20px starfont';
    if (userData.level < 25) draw.fillStyle = '#A9A9A9';
    draw.fillText('25', 139 + 15, 94 + 30);
    if (userData.level < 26) draw.fillStyle = '#A9A9A9';
    draw.fillText('26', 228 + 15, 94 + 30);
    if (userData.level < 27) draw.fillStyle = '#A9A9A9';
    draw.fillText('27', 317 + 15, 94 + 30);
    if (userData.level < 28) draw.fillStyle = '#A9A9A9';
    draw.fillText('28', 139 + 15, 94 + 44 * 2 + 30);
    if (userData.level < 29) draw.fillStyle = '#A9A9A9';
    draw.fillText('29', 228 + 15, 94 + 44 * 2 + 30);
    if (userData.level < 30) draw.fillStyle = '#A9A9A9';
    draw.fillText('30', 317 + 15, 94 + 44 * 2 + 30);
    if (userData.level < 31) draw.fillStyle = '#A9A9A9';
    draw.fillText('31', 139 + 15, 94 + 44 * 4 + 30);
    if (userData.level < 32) draw.fillStyle = '#A9A9A9';
    draw.fillText('32', 228 + 15, 94 + 44 * 4 + 30);
    if (userData.level < 33) draw.fillStyle = '#A9A9A9';
    draw.fillText('33', 317 + 15, 94 + 44 * 4 + 30);
    if (userData.level < 34) draw.fillStyle = '#A9A9A9';
    draw.fillText('34', 139 + 15, 94 + 44 * 6 + 30);
    if (userData.level < 35) draw.fillStyle = '#A9A9A9';
    draw.fillText('35', 228 + 15, 94 + 44 * 6 + 30);
    if (userData.level < 36) draw.fillStyle = '#A9A9A9';
    draw.fillText('36', 317 + 15, 94 + 44 * 6 + 30);
    document.addEventListener('click', levelSelect3Support);
    document.addEventListener('keydown', levelSelect3KeyDown);
  }

  function levelSelect3Support(e) {
    if (e.pageX || e.pageY) {
      x = e.pageX;
      y = e.pageY;
    } else {
      x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    x /= window.resizer;
    y /= window.resizer;
    if (x > 139) {
      if (x < 139 + 44) {
        if (y > 94) {
          if (y < 94 + 44 && userData.level > 24) {
            document.removeEventListener('click', levelSelect3Support);
            document.removeEventListener('keydown', levelSelect3KeyDown);
            Game.level = 25;
            level(25, null, true);
          }
        }
      }
    }
    if (x > 228) {
      if (x < 228 + 44) {
        if (y > 94) {
          if (y < 94 + 44 && userData.level > 25) {
            document.removeEventListener('click', levelSelect3Support);
            document.removeEventListener('keydown', levelSelect3KeyDown);
            Game.level = 26;
            level(26, null, true);
          }
        }
      }
    }
    if (x > 317) {
      if (x < 317 + 44) {
        if (y > 94) {
          if (y < 94 + 44 && userData.level > 26) {
            document.removeEventListener('click', levelSelect3Support);
            document.removeEventListener('keydown', levelSelect3KeyDown);
            Game.level = 27;
            level(27, null, true);
          }
        }
      }
    }
    if (x > 139) {
      if (x < 139 + 44) {
        if (y > 94 + 44 * 2) {
          if (y < 94 + 44 * 2 + 44 && userData.level > 27) {
            document.removeEventListener('click', levelSelect3Support);
            document.removeEventListener('keydown', levelSelect3KeyDown);
            Game.level = 28;
            level(28, null, true);
          }
        }
      }
    }
    if (x > 228) {
      if (x < 228 + 44) {
        if (y > 94 + 44 * 2) {
          if (y < 94 + 44 * 2 + 44 && userData.level > 28) {
            document.removeEventListener('click', levelSelect3Support);
            document.removeEventListener('keydown', levelSelect3KeyDown);
            Game.level = 29;
            level(29, null, true);
          }
        }
      }
    }
    if (x > 317) {
      if (x < 317 + 44) {
        if (y > 94 + 44 * 2) {
          if (y < 94 + 44 * 2 + 44 && userData.level > 29) {
            document.removeEventListener('click', levelSelect3Support);
            document.removeEventListener('keydown', levelSelect3KeyDown);
            Game.level = 30;
            level(30, null, true);
          }
        }
      }
    }
    if (x > 139) {
      if (x < 139 + 44) {
        if (y > 94 + 44 * 4) {
          if (y < 94 + 44 * 4 + 44 && userData.level > 30) {
            document.removeEventListener('click', levelSelect3Support);
            document.removeEventListener('keydown', levelSelect3KeyDown);
            Game.level = 31;
            level(31, null, true);
          }
        }
      }
    }
    if (x > 228) {
      if (x < 228 + 44) {
        if (y > 94 + 44 * 4) {
          if (y < 94 + 44 * 4 + 44 && userData.level > 31) {
            document.removeEventListener('click', levelSelect3Support);
            document.removeEventListener('keydown', levelSelect3KeyDown);
            Game.level = 32;
            level(32, null, true);
          }
        }
      }
    }
    if (x > 317) {
      if (x < 317 + 44) {
        if (y > 94 + 44 * 4) {
          if (y < 94 + 44 * 4 + 44 && userData.level > 32) {
            document.removeEventListener('click', levelSelect3Support);
            document.removeEventListener('keydown', levelSelect3KeyDown);
            Game.level = 33;
            level(33, null, true);
          }
        }
      }
    }
    if (x > 139) {
      if (x < 139 + 44) {
        if (y > 94 + 44 * 6) {
          if (y < 94 + 44 * 6 + 44 && userData.level > 33) {
            document.removeEventListener('click', levelSelect3Support);
            document.removeEventListener('keydown', levelSelect3KeyDown);
            Game.level = 34;
            level(34, null, true);
          }
        }
      }
    }
    if (x > 228) {
      if (x < 228 + 44) {
        if (y > 94 + 44 * 6) {
          if (y < 94 + 44 * 6 + 44 && userData.level > 34) {
            document.removeEventListener('click', levelSelect3Support);
            document.removeEventListener('keydown', levelSelect3KeyDown);
            Game.level = 35;
            level(35, null, true);
          }
        }
      }
    }
    if (x > 317) {
      if (x < 317 + 44) {
        if (y > 94 + 44 * 6) {
          if (y < 94 + 44 * 6 + 44 && userData.level > 35) {
            document.removeEventListener('click', levelSelect3Support);
            document.removeEventListener('keydown', levelSelect3KeyDown);
            Game.level = 36;
            level(36, null, true);
          }
        }
      }
    }
  }

  function levelSelect3KeyDown(e) {
    if (e.keyCode == 37) {
      document.removeEventListener('click', levelSelect3Support);
      document.removeEventListener('keydown', levelSelect3KeyDown);
      levelSelect2();
    }
    if (e.keyCode == 39) {
      document.removeEventListener('click', levelSelect3Support);
      document.removeEventListener('keydown', levelSelect3KeyDown);
      levelSelect4();
    }
  }

  function levelSelect4() {
    document.removeEventListener('click', mainMenuSupport);
    document.removeEventListener('keydown', mainMenuSupport2);
    draw.fillStyle = '#556B2f';
    draw.fillRect(50, 50, 400, 400);
    draw.fillStyle = '#ffffff';
    if (userData.level < 37) draw.fillStyle = '#000000';
    draw.fillRect(139, 94, 44, 44);
    if (userData.level < 38) draw.fillStyle = '#000000';
    draw.fillRect(228, 94, 44, 44);
    if (userData.level < 39) draw.fillStyle = '#000000';
    draw.fillRect(317, 94, 44, 44);
    if (userData.level < 40) draw.fillStyle = '#000000';
    draw.fillRect(139, 94 + 44 * 2, 44, 44);
    if (userData.level < 41) draw.fillStyle = '#000000';
    draw.fillRect(228, 94 + 44 * 2, 44, 44);
    if (userData.level < 42) draw.fillStyle = '#000000';
    draw.fillRect(317, 94 + 44 * 2, 44, 44);
    if (userData.level < 43) draw.fillStyle = '#000000';
    draw.fillRect(139, 94 + 44 * 4, 44, 44);
    if (userData.level < 44) draw.fillStyle = '#000000';
    draw.fillRect(228, 94 + 44 * 4, 44, 44);
    if (userData.level < 45) draw.fillStyle = '#000000';
    draw.fillRect(317, 94 + 44 * 4, 44, 44);
    if (userData.level < 46) draw.fillStyle = '#000000';
    draw.fillRect(139, 94 + 44 * 6, 44, 44);
    if (userData.level < 47) draw.fillStyle = '#000000';
    draw.fillRect(228, 94 + 44 * 6, 44, 44);
    if (userData.level < 48) draw.fillStyle = '#000000';
    draw.fillRect(317, 94 + 44 * 6, 44, 44);
    draw.fillStyle = '#000000';
    draw.font = '20px starfont';
    if (userData.level < 37) draw.fillStyle = '#A9A9A9';
    draw.fillText('37', 139 + 15, 94 + 30);
    if (userData.level < 38) draw.fillStyle = '#A9A9A9';
    draw.fillText('38', 228 + 15, 94 + 30);
    if (userData.level < 39) draw.fillStyle = '#A9A9A9';
    draw.fillText('39', 317 + 15, 94 + 30);
    if (userData.level < 40) draw.fillStyle = '#A9A9A9';
    draw.fillText('40', 139 + 15, 94 + 44 * 2 + 30);
    if (userData.level < 41) draw.fillStyle = '#A9A9A9';
    draw.fillText('41', 228 + 15, 94 + 44 * 2 + 30);
    if (userData.level < 42) draw.fillStyle = '#A9A9A9';
    draw.fillText('42', 317 + 15, 94 + 44 * 2 + 30);
    if (userData.level < 43) draw.fillStyle = '#A9A9A9';
    draw.fillText('43', 139 + 15, 94 + 44 * 4 + 30);
    if (userData.level < 44) draw.fillStyle = '#A9A9A9';
    draw.fillText('44', 228 + 15, 94 + 44 * 4 + 30);
    if (userData.level < 45) draw.fillStyle = '#A9A9A9';
    draw.fillText('45', 317 + 15, 94 + 44 * 4 + 30);
    if (userData.level < 46) draw.fillStyle = '#A9A9A9';
    draw.fillText('46', 139 + 15, 94 + 44 * 6 + 30);
    if (userData.level < 47) draw.fillStyle = '#A9A9A9';
    draw.fillText('47', 228 + 15, 94 + 44 * 6 + 30);
    if (userData.level < 48) draw.fillStyle = '#A9A9A9';
    draw.fillText('48', 317 + 15, 94 + 44 * 6 + 30);
    document.addEventListener('click', levelSelect4Support);
    document.addEventListener('keydown', levelSelect4KeyDown);
  }

  function levelSelect4Support(e) {
    if (e.pageX || e.pageY) {
      x = e.pageX;
      y = e.pageY;
    } else {
      x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    x /= window.resizer;
    y /= window.resizer;
    if (x > 139) {
      if (x < 139 + 44) {
        if (y > 94) {
          if (y < 94 + 44 && userData.level > 36) {
            document.removeEventListener('click', levelSelect4Support);
            document.removeEventListener('keydown', levelSelect4KeyDown);
            Game.level = 37;
            level(37, null, true);
          }
        }
      }
    }
    if (x > 228) {
      if (x < 228 + 44) {
        if (y > 94) {
          if (y < 94 + 44 && userData.level > 37) {
            document.removeEventListener('click', levelSelect4Support);
            document.removeEventListener('keydown', levelSelect4KeyDown);
            Game.level = 38;
            level(38, null, true);
          }
        }
      }
    }
    if (x > 317) {
      if (x < 317 + 44) {
        if (y > 94) {
          if (y < 94 + 44 && userData.level > 38) {
            document.removeEventListener('click', levelSelect4Support);
            document.removeEventListener('keydown', levelSelect4KeyDown);
            Game.level = 39;
            level(39, null, true);
          }
        }
      }
    }
    if (x > 139) {
      if (x < 139 + 44) {
        if (y > 94 + 44 * 2) {
          if (y < 94 + 44 * 2 + 44 && userData.level > 39) {
            document.removeEventListener('click', levelSelect4Support);
            document.removeEventListener('keydown', levelSelect4KeyDown);
            Game.level = 40;
            level(40, null, true);
          }
        }
      }
    }
    if (x > 228) {
      if (x < 228 + 44) {
        if (y > 94 + 44 * 2) {
          if (y < 94 + 44 * 2 + 44 && userData.level > 40) {
            document.removeEventListener('click', levelSelect4Support);
            document.removeEventListener('keydown', levelSelect4KeyDown);
            Game.level = 41;
            level(41, null, true);
          }
        }
      }
    }
    if (x > 317) {
      if (x < 317 + 44) {
        if (y > 94 + 44 * 2) {
          if (y < 94 + 44 * 2 + 44 && userData.level > 41) {
            document.removeEventListener('click', levelSelect4Support);
            document.removeEventListener('keydown', levelSelect4KeyDown);
            Game.level = 42;
            level(42, null, true);
          }
        }
      }
    }
    if (x > 139) {
      if (x < 139 + 44) {
        if (y > 94 + 44 * 4) {
          if (y < 94 + 44 * 4 + 44 && userData.level > 42) {
            document.removeEventListener('click', levelSelect4Support);
            document.removeEventListener('keydown', levelSelect4KeyDown);
            Game.level = 43;
            level(43, null, true);
          }
        }
      }
    }
    if (x > 228) {
      if (x < 228 + 44) {
        if (y > 94 + 44 * 4) {
          if (y < 94 + 44 * 4 + 44 && userData.level > 43) {
            document.removeEventListener('click', levelSelect4Support);
            document.removeEventListener('keydown', levelSelect4KeyDown);
            Game.level = 44;
            level(44, null, true);
          }
        }
      }
    }
    if (x > 317) {
      if (x < 317 + 44) {
        if (y > 94 + 44 * 4) {
          if (y < 94 + 44 * 4 + 44 && userData.level > 44) {
            document.removeEventListener('click', levelSelect4Support);
            document.removeEventListener('keydown', levelSelect4KeyDown);
            Game.level = 45;
            level(45, null, true);
          }
        }
      }
    }
    if (x > 139) {
      if (x < 139 + 44) {
        if (y > 94 + 44 * 6) {
          if (y < 94 + 44 * 6 + 44 && userData.level > 45) {
            document.removeEventListener('click', levelSelect4Support);
            document.removeEventListener('keydown', levelSelect4KeyDown);
            Game.level = 46;
            level(46, null, true);
          }
        }
      }
    }
    if (x > 228) {
      if (x < 228 + 44) {
        if (y > 94 + 44 * 6) {
          if (y < 94 + 44 * 6 + 44 && userData.level > 46) {
            document.removeEventListener('click', levelSelect4Support);
            document.removeEventListener('keydown', levelSelect4KeyDown);
            Game.level = 47;
            level(47, null, true);
          }
        }
      }
    }
    if (x > 317) {
      if (x < 317 + 44) {
        if (y > 94 + 44 * 6) {
          if (y < 94 + 44 * 6 + 44 && userData.level > 47) {
            document.removeEventListener('click', levelSelect4Support);
            document.removeEventListener('keydown', levelSelect4KeyDown);
            Game.level = 48;
            level(48, null, true);
          }
        }
      }
    }
  }

  function levelSelect4KeyDown(e) {
    if (e.keyCode == 37) {
      document.removeEventListener('click', levelSelect4Support);
      document.removeEventListener('keydown', levelSelect4KeyDown);
      levelSelect3();
    }
    if (e.keyCode == 39) {
      //document.removeEventListener('click', levelSelect4Support);
      //document.removeEventListener('keydown', levelSelect4KeyDown);
      //levelSelect4();
    }
  }

  function levelSelect5() {}
  var multiplayerM = new Menu(function() {
    draw.drawImage(tank_ui['multiplayer'].image, 0, 0);
  }, {
    click: function(e) {
      if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
      } else {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }
      x -= canvas.offsetLeft;
      y -= canvas.offsetTop;
      x /= window.resizer;
      y /= window.resizer;
      if (x > 58) {
        if (x < 246) {
          if (y > 361) {
            if (y < 444) {
              if (confirm('This section is currently being added. Only proceed if you know the consequences')) {
                this.removeListeners();
                co_op();
              }
            }
          }
        }
      }
      if (x > 255) {
        if (x < 443) {
          if (y > 361) {
            if (y < 444) {
              this.removeListeners();
              pvp();
            }
          }
        }
      }
      if (x > 59) {
        if (x < 112) {
          if (y > 56) {
            if (y < 109) {
              this.removeListeners();
              mainMenu();
            }
          }
        }
      }
    }
  }, document);

  function multiplayer() {
    multiplayerM.draw();
    multiplayerM.addListeners();
  }
  var co_opM = new Menu(function() {
    document.getElementById('server').style.display = 'none';
    draw.drawImage(tank_ui['co_op'].image, 0, 0);
  }, {
    click: function co_op_onclick(e) {
      if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
      } else {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }
      x -= canvas.offsetLeft;
      y -= canvas.offsetTop;
      x /= window.resizer;
      y /= window.resizer;
      if (true) {
        if (true) {
          if (true) {
            if (true) {
              this.removeListeners();
              load = new Animation(loadOpt);
              load.start();
              document.getElementById('server').style.display = 'none';
              setTimeout(function() {
                var server = document.getElementById('server');
                if (server.value != '') {
                  user.joiner = new Joiner();
                  user.joiner.control(server.value, 'defense');
                } else {
                  user.joiner = new Joiner();
                  user.joiner.control('quick-join', 'defense');
                }
              }, 6000);
            }
          }
        }
      }
    }
  }, document);

  function co_op() {
    co_opM.draw();
    co_opM.addListeners();
  }
  var pvpM = new Menu(function() {
    document.getElementById('server').style.display = 'none';
    draw.drawImage(tank_ui['pvp'].image, 0, 0);
  }, {
    click: function pvp_onclick(e) {
      if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
      } else {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }
      x -= canvas.offsetLeft;
      y -= canvas.offsetTop;
      x /= window.resizer;
      y /= window.resizer;
      if (x > 58) {
        if (y > 289) {
          if (x < 246) {
            if (y < 344) {
              this.removeListeners();
              load = new Animation(loadOpt);
              load.start();
              document.getElementById('server').style.display = 'none';
              setTimeout(function() {
                var server = document.getElementById('server');
                if (server.value != '') {
                  user.joiner = new Joiner();
                  user.joiner.control(server.value, 'tdm');
                } else {
                  user.joiner = new Joiner();
                  user.joiner.control('quick-join', 'tdm');
                }
              }, 6000);
            }
          }
        }
      }
      if (x > 255) {
        if (y > 389) {
          if (x < 443) {
            if (y < 444) {
              this.removeListeners();
              duels();
            }
          }
        }
      }
      if (x > 59) {
        if (x < 112) {
          if (y > 56) {
            if (y < 109) {
              this.removeListeners();
              multiplayer();
            }
          }
        }
      }
      if (x > 255) {
        if (y > 389) {
          if (x < 443) {
            if (y < 444) {
              this.removeListeners();
              duels();
            }
          }
        }
      }
      if (x > 58) {
        if (y > 389) {
          if (x < 246) {
            if (y < 444) {
              this.removeListeners();
              ffa();
            }
          }
        }
      }
    }
  }, document);

  function pvp() {
    pvpM.draw();
    pvpM.addListeners();
  }
  var ffaM = new Menu(function() {
    document.getElementById('server').style.display = 'block';
    document.getElementById('server').style.marginBottom = '-250px';
    draw.drawImage(tank_ui['ffa'].image, 0, 0);
  }, {
    click: function(e) {
      if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
      } else {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }
      x -= canvas.offsetLeft;
      y -= canvas.offsetTop;
      x /= window.resizer;
      y /= window.resizer;
      if (x > 59) {
        if (x < 112) {
          if (y > 56) {
            if (y < 109) {
              this.removeListeners();
              pvp();
            }
          }
        }
      }
      if (x > 158) {
        if (x < 346) {
          if (y > 233) {
            if (y < 288) {
              this.removeListeners();
              load = new Animation(loadOpt);
              load.start();
              document.getElementById('server').style.display = 'none';
              setTimeout(function() {
                var server = document.getElementById('server');
                if (server.value != '') {
                  user.joiner = new Joiner();
                  user.joiner.control(server.value, 'ffa');
                } else {
                  user.joiner = new Joiner();
                  user.joiner.control('quick-join', 'ffa');
                }
              }, 6000);
            }
          }
        }
      }
    }
  }, document);

  function ffa() {
    ffaM.draw();
    ffaM.addListeners();
  }
  var duelsM = new Menu(function() {
    document.getElementById('server').style.display = 'block';
    draw.drawImage(tank_ui['duels'].image, 0, 0);
  }, {
    click: function duels_onclick(e) {
      if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
      } else {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }
      x -= canvas.offsetLeft;
      y -= canvas.offsetTop;
      x /= window.resizer;
      y /= window.resizer;
      if (x > 59) {
        if (x < 112) {
          if (y > 56) {
            if (y < 109) {
              pvp();
            }
          }
        }
      }
      if (x > 158) {
        if (x < 346) {
          if (y > 233) {
            if (y < 288) {
              document.removeEventListener('click', duels_onclick);
              document.getElementById('server').style.display = 'none';
              load = new Animation(loadOpt);
              load.start();
              setTimeout(function() {
                var server = document.getElementById('server');
                if (server.value != '') {
                  user.joiner = new Joiner();
                  user.joiner.control(server.value, 'duels');
                } else {
                  user.joiner = new Joiner();
                  user.joiner.control('quick-join', 'duels');
                }
              }, 6000);
            }
          }
        }
      }
    }
  }, document);

  function duels() {
    duelsM.draw();
    duelsM.addListeners();
  }

  function barrier(x, y) {
    b.push(new Block(Infinity, x, y, true, false, false, true, 'void'));
  }

  function wall(x, y) {
    b.push(new Block(Infinity, x, y, true, false, false, true, 'wall'));
  }

  function gold(x, y, isScaffolding) {
    b.push(new Block(600, x, y, false, false, isScaffolding, true, 'gold'));
  }

  function strong(x, y, isScaffolding) {
    b.push(new Block(120, x, y, false, false, isScaffolding, true, 'strong'));
  }

  function weak(x, y, isScaffolding) {
    b.push(new Block(80, x, y, false, false, isScaffolding, true, 'weak'));
  }

  function spawn(x, y) {
    user.tank.x = x * 50;
    user.tank.y = y * 50;
    user.tank.spawn = [x, y];
  }

  function chat_setup() {
    chatSubmit.addEventListener('click', chat_submit);
  }

  function chat_submit() {
    if (message.value != '') {
      if (user.username != undefined) {
        var newMessage = document.createElement('SPAN');
        newMessage.innerHTML = user.username + ': ' + message.value + ' to: ' + to.value;
        sessionStorage.setItem('messages', [user.username, message.value, to.value].concat(sessionStorage.getItem('messages')));
        messages.appendChild(newMessage);
        newMessage.classList.add('message');
        a('Message Sent', 'green');
      } else {
        a('Start Game First', 'red');
      }
    } else {
      a('Enter Text to Submit', 'red');
    }
  }

  function createAi(x, y, isTurret, fireType, isInvis, turretFireType, team) {
    Game.foes++; // add Eval Tank to Game to detect victory when all destroyed!
    ai.push(new Ai(isTurret, fireType, isInvis, turretFireType, team)); // add Eval Tank to array
    ai[ai.length - 1].id = ai.length - 1;
    ai[ai.length - 1].x = x;
    ai[ai.length - 1].y = y;
    /*ai[ai.length - 1].master = team;
    ai[ai.length - 1].type = t;
    ai[ai.length - 1].control();*/
  }
})();
