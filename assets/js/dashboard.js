var b_home, b_game_stats, b_store, b_servers, b_account_settings, b_admin_dashboard, home, game_stats, store, servers, account_settings, admin_dashboard;
var b_load_game_data, load_game;
var b_create_server, b_share_server, chat_server_name, chat_server_response;
var admin_kick_text, admin_kick_button, admin_ban_text, admin_ban_button;
var currentslide = 1; 
var preferences, settings;
var socket;
var logSocket;
var onlineSocket;
var statSocket;
setInterval(function() {
  socket.emit('message', {
    username: sessionStorage.username,
    token: sessionStorage.token,
    operation: 'status',
    task: 'set',
    status: 'online',
    message: 'On Dashboard',
  })
}, 1000);
// 1 -> home
// 2 -> game_stats
// 3 -> store
// 4 -> servers
// 5 -> account_settings
window.onload = function() {
  // Load User Data
  document.getElementById('user-greeting').innerHTML = 'Welcome, '+sessionStorage.username+'!';
  // Get Dashboard Nav Buttons
  b_home = document.getElementById('button-home');
  b_game_stats = document.getElementById('button-game-stats');
  b_store = document.getElementById('button-store');
  b_servers = document.getElementById('button-servers');
  b_account_settings = document.getElementById('button-account-settings');
  b_admin_dashboard = document.getElementById('button-admin-dashboard');
  if (sessionStorage.username == 'admin' || sessionStorage.username == 'Celestial' || sessionStorage.username == 'bradley' || sessionStorage.username == 'sullivan' || sessionStorage.username == 'Carterlin26' || sessionStorage.username == '3foe') {
    b_admin_dashboard.style.display = 'inline-block';
  }
  // Get Dashboard Options
  home = document.getElementById('home');
  game_stats = document.getElementById('game-stats');
  store = document.getElementById('store');
  servers = document.getElementById('servers');
  account_settings = document.getElementById('account-settings');
  admin_dashboard = document.getElementById('admin-dashboard');
  // Get Game Data Button
  b_load_game_data = document.getElementById('game-data-load');
  load_game = document.getElementById('load-game');
  b_load_game_data.addEventListener('click', function() {
    var game = load_game.value;
    statSocket = new io({
      path: '/server',
    });
    statSocket.on('connect', function() {
      statSocket.on('message', function(data) {
        if (game == 'pixel-tanks') {
          var loadData = JSON.parse(data.data[0].playerdata)['increedible-tanks'];
          document.getElementById('game-data').innerHTML = '<h3>Rank/Level: '+loadData.rank+'</h3><h3>Coins: '+loadData.coins+'</h3><h3>Items</h3><h4>Boosts: '+loadData.boosts+'<h4>Blocks'+loadData.blocks+'<h4>Toolkits: '+loadData.toolkits+'</h4><h4>Flashbangs: '+loadData.flashbangs;
        }
      });
      statSocket.emit('message', {
        username: sessionStorage.username,
        token: sessionStorage.token,
        operation: 'database',
        task: 'get',
      });
    });
  })
  // Get Chat Server Button
  b_create_server = document.getElementById('create-server-submit');
  b_chat_server = document.getElementById('chat-server-submit');
  // Get Chat Server UI
  chat_server_name = document.getElementById('server-name');
  chat_server_select = document.getElementById('chat-server-select')
  chat_server_ouput = document.getElementById('chat-server-output');
  // Chat Server Listeners
  b_create_server.addEventListener('click', function() {
    socket.emit('message', {
      username: sessionStorage.username,
      token: sessionStorage.token,
      operation: 'chat-servers',
      task: 'new',
      name: chat_server_name.value,
    });
    socket.on('message', function(data) {
      socket.removeAllListeners('message');
    });
  });
  document.getElementById('leave-server').addEventListener('click', function() {
    if (!confirm('Are you sure you want to do this? It will delete your access to this server...')) return;
    socket.emit('message', {
      username: sessionStorage.username,
      token: sessionStorage.token,
      operation: 'chat-servers',
      task: 'unshare',
      name: server,
      remove_member: sessionStorage.username,
    });
  });
  document.getElementById('chat-server-remove-member').addEventListener('click', function() {
    socket.emit('message', {
      username: sessionStorage.username,
      token: sessionStorage.token,
      operation: 'chat-servers',
      task: 'unshare',
      name: server,
      remove_member: document.getElementById('chat-server-members').value,
    });
  });
  document.getElementById('chat-server-add-member').addEventListener('click', function() {
    socket.emit('message', {
      username: sessionStorage.username,
      token: sessionStorage.token,
      operation: 'chat-servers',
      task: 'share',
      name: server,
      new_member: document.getElementById('chat-server-add').value,
    });
  });
  //b_share_server.addEventListener('click', function() {

  //})
  // Get Admin Dashboard UI
  admin_ban_text = document.getElementById('admin-ban-text');
  admin_ban_button = document.getElementById('admin-ban-button');
  admin_kick_text = document.getElementById('admin-kick-text');
  admin_kick_button = document.getElementById('admin-kick-button');
  // Load Default Dashboard
  swapDashboard();
  // Setup Event Listeners
  b_home.addEventListener('click', function() {
    currentslide = 1;
    swapDashboard();
  });
  b_game_stats.addEventListener('click', function() {
    currentslide = 2;
    swapDashboard();
  });
  b_store.addEventListener('click', function() {
    currentslide = 3;
    swapDashboard();
  });
  b_servers.addEventListener('click', function() {
    currentslide = 4;
    swapDashboard();
  });
  b_account_settings.addEventListener('click', function() {
    currentslide = 5;
    swapDashboard();
  });
  b_admin_dashboard.addEventListener('click', function() {
    currentslide = 6;
    swapDashboard();
  });
  // Load User Data
  socket = new io({
    path: '/server',
  });
  var userData;
  socket.on('connect', function() {
    get(sessionStorage.username, function(data) {
      userData = data;
      document.getElementById('lunar-coins').innerText = JSON.parse(data.userdata).coins;
      document.getElementById('username').innerText = sessionStorage.username;
      var chatServers = JSON.parse(userData.chat);
      chat_server_select.innerHTML = '';
      var l = 0;
      while (l<chatServers.length) {
        chat_server_select.innerHTML += '<option value="'+chatServers[l]+'">'+chatServers[l]+'</option>';
        l++;
      }
    });
  });
  // Chat Server Event Listeners
  var chatSocket = new io({
    path: '/server',
  });
  var members = [];
  var elite = [];
  var owner = '';
  var userrank = 'n/a';
  var server = null;
  b_chat_server.addEventListener('click', function() {
    server = chat_server_select.value;
    if (server != '' && server != undefined) {
      chatSocket.on('message', function(data) {
        members = JSON.parse(data.data.members);
        elite = JSON.parse(data.data.elite);
        owner = data.data.owner;
        if (members.includes(sessionStorage.username) || members.includes('*')) {
          userrank = 'member';
        } else if (elite.includes(sessionStorage.username)) {
          userrank = 'elite';
        } else if (owner == sessionStorage.username) {
          userrank = 'owner';
        }
        var l = 0, memberString = '', memberOptions = '';
        while (l<members.length) {
          memberString += ', '+members[l];
          memberOptions += '<option value="'+members[l]+'">'+members[l]+'</option>';
          l++;
        }
        var l = 0, eliteString = '', eliteOptions = '';
        while (l<elite.length) {
          eliteString += ', '+elite[l];
          eliteOptions += '<option value="'+elite[l]+'">'+elite[l]+'</option>';
          l++;
        }
        document.getElementById('chat-server-current-members').innerHTML = '<br><b>Members: '+memberString+'</b><br><b>Elite: '+elite.toString()+'</b><br><b>Owner: '+owner+'</br>';
        document.getElementById('chat-server-output').innerHTML = 'Selected Server: '+server;
        if (userrank == 'elite' || userrank == 'owner') {
          document.getElementById('chat-server-members').innerHTML = memberOptions;
        }
      });
      chatSocket.emit('message', {
        username: sessionStorage.username,
        operation: 'chat-servers',
        token: sessionStorage.token,
        name: server,
        task: 'get-people',
      });
    } else {
      alert('Invalid Server');
    }
  });
  document.getElementById('')
  // Admin Event Listeners
  admin_kick_button.addEventListener('click', function() {
    var password = prompt('In order to do this action, you must enter the password for it: ');
    socket.emit('message', {
      username: sessionStorage.username,
      password: password,
      token: sessionStorage.token,
      operation: 'status',
      task: 'admin-kick',
      victim: admin_kick_text.value,
    });
  })
  admin_ban_button.addEventListener('click', function() {
    var password = prompt('In order to do this action, you must enter the password for it: ');
    socket.emit('message', {
      username: sessionStorage.username,
      password: password,
      token: sessionStorage.token,
      operation: 'status',
      task: 'admin-ban',
      victim: admin_ban_text.value,
    });
  });
  // Load Log Data
  logSocket = new io({
    path: '/server',
  });
  logSocket.on('connect', function() {
    logSocket.on('message', function(data) {
      logs = data.data.logs;
      var log_container = document.getElementById('log-container')
      var l = 0;
      while (l<logs.length) {
        log_container.innerHTML += '<span>'+logs[l]+'</span>';
        l++;
      }
      logSocket.on('message', function(data) {
        var log = data.data.log;
        document.getElementById('log-container').innerHTML += '<span>'+log+'</span>';
      });
    });
    logSocket.emit('message', {
      username: sessionStorage.username,
      token: sessionStorage.token,
      operation: 'logs',
      task: 'get',
    });
  });
  document.getElementById('reboot-button').addEventListener('click', function() {
    logSocket.emit('message', {
      username: sessionStorage.username,
      token: sessionStorage.token,
      operation: 'logs',
      task: 'reboot',
    });
  })
}
function swapDashboard() {
  if (currentslide == 1) {
    b_home.style.backgroundColor = 'orangered';
    b_home.style.color = 'white';
    b_game_stats.style.backgroundColor = 'white';
    b_game_stats.style.color = 'black';
    b_store.style.backgroundColor = 'white';
    b_store.style.color = 'black';
    b_servers.style.backgroundColor = 'white';
    b_servers.style.color = 'black';
    b_account_settings.style.backgroundColor = 'white';
    b_account_settings.style.color = 'black';
    b_admin_dashboard.style.backgroundColor = 'white';
    b_admin_dashboard.style.color = 'black';
    home.style.display = 'block';
    game_stats.style.display = 'none';
    store.style.display = 'none';
    servers.style.display = 'none';
    account_settings.style.display = 'none';
    admin_dashboard.style.display = 'none';
  } else if (currentslide == 2) {
    b_home.style.backgroundColor = 'white';
    b_home.style.color = 'black';
    b_game_stats.style.backgroundColor = 'orangered';
    b_game_stats.style.color = 'white';
    b_store.style.backgroundColor = 'white';
    b_store.style.color = 'black';
    b_servers.style.backgroundColor = 'white';
    b_servers.style.color = 'black';
    b_account_settings.style.backgroundColor = 'white';
    b_account_settings.style.color = 'black';
    b_admin_dashboard.style.backgroundColor = 'white';
    b_admin_dashboard.style.color = 'black';
    home.style.display = 'none';
    game_stats.style.display = 'block';
    store.style.display = 'none';
    servers.style.display = 'none';
    account_settings.style.display = 'none';
    admin_dashboard.style.display = 'none';
  } else if (currentslide == 3) {
    b_home.style.backgroundColor = 'white';
    b_home.style.color = 'black';
    b_game_stats.style.backgroundColor = 'white';
    b_game_stats.style.color = 'black';
    b_store.style.backgroundColor = 'orangered';
    b_store.style.color = 'white';
    b_servers.style.backgroundColor = 'white';
    b_servers.style.color = 'black';
    b_account_settings.style.backgroundColor = 'white';
    b_account_settings.style.color = 'black';
    b_admin_dashboard.style.backgroundColor = 'white';
    b_admin_dashboard.style.color = 'black';
    home.style.display = 'none';
    game_stats.style.display = 'none';
    store.style.display = 'block';
    servers.style.display = 'none';
    account_settings.style.display = 'none';
    admin_dashboard.style.display = 'none';
  } else if (currentslide == 4) {
    b_home.style.backgroundColor = 'white';
    b_home.style.color = 'black';
    b_game_stats.style.backgroundColor = 'white';
    b_game_stats.style.color = 'black';
    b_store.style.backgroundColor = 'white';
    b_store.style.color = 'black';
    b_servers.style.backgroundColor = 'orangered';
    b_servers.style.color = 'white';
    b_account_settings.style.backgroundColor = 'white';
    b_account_settings.style.color = 'black';
    b_admin_dashboard.style.backgroundColor = 'white';
    b_admin_dashboard.style.color = 'black';
    home.style.display = 'none';
    game_stats.style.display = 'none';
    store.style.display = 'none';
    servers.style.display = 'block';
    account_settings.style.display = 'none';
    admin_dashboard.style.display = 'none';
  } else if (currentslide == 5) {
    b_home.style.backgroundColor = 'white';
    b_home.style.color = 'black';
    b_game_stats.style.backgroundColor = 'white';
    b_game_stats.style.color = 'black';
    b_store.style.backgroundColor = 'white';
    b_store.style.color = 'black';
    b_servers.style.backgroundColor = 'white';
    b_servers.style.color = 'black';
    b_account_settings.style.backgroundColor = 'orangered';
    b_account_settings.style.color = 'white';
    b_admin_dashboard.style.backgroundColor = 'white';
    b_admin_dashboard.style.color = 'black';
    home.style.display = 'none';
    game_stats.style.display = 'none';
    store.style.display = 'none';
    servers.style.display = 'none';
    account_settings.style.display = 'block';
    admin_dashboard.style.display = 'none';
  } else if (currentslide == 6) {
    b_home.style.backgroundColor = 'white';
    b_home.style.color = 'black';
    b_game_stats.style.backgroundColor = 'white';
    b_game_stats.style.color = 'black';
    b_store.style.backgroundColor = 'white';
    b_store.style.color = 'black';
    b_servers.style.backgroundColor = 'white';
    b_servers.style.color = 'black';
    b_account_settings.style.backgroundColor = 'white';
    b_account_settings.style.color = 'black';
    b_admin_dashboard.style.backgroundColor = 'orangered';
    b_admin_dashboard.style.color = 'white';
    home.style.display = 'none';
    game_stats.style.display = 'none';
    store.style.display = 'none';
    servers.style.display = 'none';
    account_settings.style.display = 'none';
    admin_dashboard.style.display = 'block';
  }
}
function get(username, callback) {
  socket.emit('message', {
    operation: 'database',
    task: 'get',
    username: username,
    token: sessionStorage.token,
  });
  socket.on('message', function(data) {
    data = data.data[0];
    socket.removeAllListeners('message');
    callback(data);
  });
}
