var socket = new WebSocket('wss://'+window.location.hostname+'/server');

function auth(username, password, callback, authType) {
  socket.send(JSON.stringify({
    task: 'auth',
    operation: 'database',
    username: username,
    password: password,
    authType: authType,
  }));
  socket.onmessage = function(data) {
    data = JSON.parse(data.data);
    callback(data);
  };
}

var submit = document.getElementById('submit');
var newaccount = document.getElementById('newaccount');
var usernameInput = document.getElementById('usernameForm');
var passwordInput = document.getElementById('passwordForm');
var output = document.getElementById('output');
submit.addEventListener('click', function() {
  auth(usernameInput.value, passwordInput.value, function(data) {
    if (data.authencated) {
      sessionStorage.username = usernameInput.value;
      sessionStorage.token = data.token;
      window.location.href = '/dashboard';
    }
    output.innerText = data.message;
  }, 'login');
});
newaccount.addEventListener('click', function() {
  newaccount.disabled = true;
  setTimeout(function() {
    newaccount.disabled = false;
  }, 3000);
  var usernameInput = document.getElementById('usernameForm');
  var passwordInput = document.getElementById('passwordForm');
  auth(usernameInput.value, passwordInput.value, function(data) {
    if (data.status == 200) {
      socket.on('message', function() {});
      sessionStorage.username = usernameInput.value;
      sessionStorage.token = data.token;
      window.location.href = '/dashboard';
    } else if (data.status == 400 || data.status == 403 || data.status == 404) {
      usernameInput.value = '';
      passwordInput.value = '';
    }
    output.innerText = data.message;
  }, 'new-account');
});
